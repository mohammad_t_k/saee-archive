<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */


///////////////////////////////////////////Routes Created by @Ahmed///////////////////////////////////////////////////////
//the create new contract form

Route::when('warehouse/login', 'csrf', array('post'));

Route::get('admin/newcontract', 'CustomerServiceController@CreateMonthlyContract');

Route::get('admin/newcontractcustomer', 'CustomerServiceController@CreateMonthlyContractCustomer');

//post new contract form to the database, the logic is inside the controller method.
Route::post('admin/newcontract/createmonthlycontract', 'CustomerServiceController@insertNewContract');
//view contracts page with the available functions:
/*//contracts page functions:
 *
 * change contract status to one of the following:
 *    1- paid.
 *    2- unreachable.
 *    3- service started.
 *    4- called.
 *    5- Contract sent.
 *    6- find driver.
 */
//this route must return a list of all available contracts.
Route::get('admin/contracts', 'CustomerServiceController@ViewContracts');
//download contract as .docs, it must includes all Monthly related and driver info.
Route::post('admin/contracts/downloadcontract', 'CustomerServiceController@downloadContract');
//return make view all drivers. can be one route only.
Route::get('admin/drivers', 'CustomerServiceController@ViewDrivers');
//route to function to get all drivers.
Route::get('admin/drivers/getdrivers', 'CustomerServiceController@getDrivers');
//Find drivers given the contract id. no need to include {id} as a third segment of the uri, since I call this function using ajax.
Route::post('admin/drivers/finddrivers', 'CustomerServiceController@findDrivers');
//Match two contracts.
Route::post('admin/drivers/matchtwocontracts', 'CustomerServiceController@matchTwoContracts');





//Captain new simple registration form...
/*Route::get("/provider/signupsimple","WebProviderController@view_simple_form");*/

//generic route for uploading any photo must send image/doc type with the request
Route::get('/provider/analyzeimage', array('as' => 'test', 'uses' => 'OCRController@test'));
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////Customer Service 2.0/////////////////////////////////////////////////////////////


//Customer service main view:
Route::get('/customerservice/main','CustomerServiceController@main');



//walker administrative functions page
Route::get('/customerservice/walkers', array('as' => 'showWalkers', 'uses' => 'CustomerServiceController@walkers'));

//contracts administrative functions page
Route::get('/customerservice/contracts', array('as' => 'showContracts', 'uses' => 'CustomerServiceController@contracts'));

//when the webpage is already in the route customerservice, making call to this route will only need: /findrivers/
Route::post('/customerservice/finddrivers', 'CustomerServiceController@findDrivers');

Route::post('customerservice/notifydriver','CustomerServiceController@notifydriver');

Route::post('customerservice/downloadcontract', 'CustomerServiceController@downloadContract');

Route::get('customerservice/onlinedrivers', 'CustomerServiceController@getOnlineDrivers');

Route::get('customerservice/monthlyonlinedrivers', 'CustomerServiceController@getMonthlyActiveDrivers');

Route::get('customerservice/ontripdrivers', 'CustomerServiceController@getOnTripDrivers');



//notify walkers of new monthly contract "in parallel" and get response.
//.................




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////Delivery Service Routes://///////////////////////////////////////////////////////



//return app/main page
Route::get('/delivery/main', 'DeliveryController@Main');

//create new user
Route::post('/delivery/signup', 'CompanyController@signup');

Route::get('/company', 'CompanyController@loginRedirect');
Route::get('/company/login', array('as' => 'CompanyLogin','uses' => 'CompanyController@companyLogin'));
Route::post('/company/verify', array('as' => 'CompanyVerify','uses' => 'CompanyController@companyVerify'));
Route::get('/company/dashboard', array('as' => 'companyDashboard','uses' => 'CompanyController@companyDashboard'));
Route::get('/company/logout', array('as' => 'CompanyLogout','uses' => 'CompanyController@companyLogout'));
Route::get('/company/delete/{id}','CompanyController@deleteRecord');
Route::get('/company/shipments/{shipmentdate}', 'CompanyController@shipmentsDetails');
Route::post('/company/shipments/{shipmentdate}', 'CompanyController@shipmentsDetails');
Route::get('/company/shipmentsExport', 'CompanyController@shipmentsExport');
Route::post('/company/shipmentsIdExport', 'CompanyController@shipmentsIdExport');
Route::post('/company/shipmentsIdsExport', 'CompanyController@shipmentsIdsExport');
Route::get('/company/totalNewShipments', 'CompanyController@totalNewShipments');
Route::get('/company/getdistrictsbycity','CompanyController@getdistrictsbycity');
Route::get('/company/test', 'CompanyController@test');

Route::get('/company/package/{packageID}', 'CompanyController@packageDetails');

Route::get('/company/sticker/{packageID}', 'CompanyController@sticker');
Route::post('/company/stickers', 'CompanyController@stickers');
Route::get('/company/admintracking', 'CompanyController@admintracking');
Route::get('/company/admintrackingbulk', 'CompanyController@admintrackingbulk');

Route::get('/company/companydetail', array('as' => 'ViewCompany', 'uses' => 'CompanyController@companyDetail'));

Route::get('/company/customerservice', 'CompanyController@customerservice');

Route::get('/company/sameDaySuccessRate', 'CompanyController@sameDaySuccessRate');
Route::post('/company/sameDaySuccessRate/filter', 'CompanyController@sameDaySuccessRate');

Route::get('/company/orderimportxml', 'CompanyController@orderimportxml');
Route::post('/company/xmlorderimportinDB', 'CompanyController@xmlorderimportinDB');
Route::get('/company/orderimport/download', 'CompanyController@orderimport_template');

///////////////
/// company tickets
Route::get('/company/ticket/view/{waybill}', 'CompanyController@view_ticket');
Route::get('/company/ticket/create', 'CompanyController@create_ticket');
Route::post('/company/ticket/create/post', 'CompanyController@create_ticket');
Route::get('/company/ticket/export', 'CompanyController@export_ticket');
Route::post('/company/ticket/export/bulk', 'CompanyController@export_bulk_ticket');
Route::get('/company/ticket/export/filter', 'CompanyController@export_filter_ticket');
///////////////

///////////////
/// company reports
Route::get('/company/kpiReport', 'CompanyController@kpiReport');
///////////////

///////////////
///// sub companies

Route::get('/company/subcompanies', 'CompanyController@subcompanies');

Route::post('/company/subcompanies/remove', 'CompanyController@subcompanyremove');
///////////////

//do login
Route::get('/delivery/login',   array('as' => 'CompanyLogin', 'uses' => 'CompanyController@login'));

//request new delivery
Route::post('/delivery/request','DeliveryController@request_delivery');

//returns all active requests
Route::get('/delivery/activerequests','CompanyController@getActiveRequests');

//returns all requests
Route::get('/delivery/allrequests', 'CompanyController@getAllRequests');

//return paid requests
Route::get('/delivery/deliveredrequests' , 'CompanyController@getDeliveredRequests');

//captain will call this url from android app to responses to delivery request
Route::post('/provider/responddeliveryrequest', 'DeliveryController@respond_delivery_request');

//Call this url from parcel delivery order status page to check the status of captain's acceptance
Route::post('/delivery/getdeliveryrequest', 'DeliveryController@get_delivery_request');

Route::post('/delivery/getdeliveryrequeststatus','DeliveryController@get_delivery_request_status');

//accept delivery request.
Route::post('delivery/walker/respond','DeliveryController@respond_delivery_request');

//package picked from sender.
Route::post('delivery/walker/arrived','DeliveryController@delivery_request_arrived');

//package picked from sender.
Route::post('delivery/walker/picked','DeliveryController@delivery_request_picked');

//package delivered and cash posted.
Route::post('delivery/walker/posted','DeliveryController@delivery_request_cash_posted');

//captain started towards delivery.
Route::post('delivery/walker/started','DeliveryController@started_delivery_request');

Route::post('delivery/resubmitrequest','DeliveryController@resubmit_delivery_request');


Route::get('delivery/customerservice','CustomerServiceController@delivery_orders');

Route::get('delivery/issupplier','CompanyController@isSupplier');

Route::get('delivery/iscustomer','CompanyController@isCustomer');

//send sms given the mobile number and content:
Route::post('delivery/sendsms','DeliveryController@send_sms');

//send link to customer to get its location live..
Route::get('delivery/getcustomerlocation/{randomnumber}','DeliveryController@get_customer_location');

//retrieve location, merely latitude and longitude pairs:
Route::get('delivery/retrievecustomerlocation','DeliveryController@retrieve_customer_location');


//send link to customer to get location, time and date of delivery..
Route::get('delivery/getcustomerinformation/{randomnumber}','DeliveryController@get_customer_information');

//retrieve date, time and location:
Route::get('delivery/retrievecustomerinformation','DeliveryController@retrieve_customer_information');


//Delivery web app API integration khowdhat:

//////////////////////////////////////Kapser Business APIs//////////////////////////

Route::get('deliveryrequest/getallcities','DeliveryReqController@getcities');

Route::get('deliveryrequest/getalldistricts','DeliveryReqController@getalldistricts');

Route::get('deliveryrequest/getdistrictsbycity','DeliveryReqController@getdistrictsbycity');

Route::get('deliveryrequest/printsticker/param','DeliveryReqController@printsticker');
Route::get('deliveryrequest/printsticker/{packageID}','DeliveryReqController@printsticker');

Route::get('deliveryrequest/generatestickerpdf/{packageID}','DeliveryReqController@generatestickerpdf'); 
Route::get('deliveryrequest/printsticker/pdf/param','DeliveryReqController@printstickerpdf');
Route::get('deliveryrequest/printsticker/pdf/{packageID}','DeliveryReqController@printstickerpdf');

Route::post('deliveryrequest/pickupshipment', 'DeliveryReqController@pickupshipment');

Route::post('deliveryrequest/activatecompany', 'DeliveryReqController@activatecompany');

Route::post('deliveryrequest/new/aramex', 'DeliveryReqController@newaramex');

//////////////
/// Hub Api
Route::post('/hub/undeliveredReasons', 'HubAPIController@undelivered_reasons');
Route::post('/hub/orderStatuses', 'HubAPIController@order_statuses');
Route::post('/hub/companies', 'HubAPIController@companies');
Route::post('/hub/cities', 'HubAPIController@cities');
Route::post('/hub/admin/getAllAdmins', 'HubAPIController@get_all_admins');
Route::post('/hub/admin/getAllHubs', 'HubAPIController@get_all_hubs');
Route::post('/hub/shipment/shipmentsExport', 'HubAPIController@shipmentsExport');
Route::post('/hub/captain/getShipments', 'HubAPIController@get_captain_shipments');
//////////////
Route::post('/hub/getCaptainAccountHistory', 'HubAPIController@getCaptainAccountHistory');
Route::post('/hub/captainaccounting', 'HubAPIController@captainAccounting');
Route::post('/hub/receivedDeliverdMoneyFromCaptain', 'HubAPIController@receivedDeliverdMoneyFromCaptain');

Route::post('/hub/getBanksName', 'HubAPIController@getBanksName');
Route::post('/hub/captainAccountReport', 'HubAPIController@captainAccountReport');
Route::post('/hub/bankDepositReport', 'HubAPIController@bankDepositReport');
//Route::post('/hub/paytosupplier', 'HubAPIController@paytosupplier');

Route::post('/hub/captainpaymentreport', 'HubAPIController@captainPaymentReport');
//////////////////////////////////JollyChic Service Routes://///////////////////////////////////////////////////////


Route::get('jollychic/sendpincode/','JollyChicController@sendpincode');
Route::get('jollychic/tests/','JollyChicController@liveTests');

Route::post('jollychic/login','JollyChicController@login');
Route::post('jollychic/getroles','JollyChicController@getRoles');

Route::post('user/uploadsignature','JollyChicController@uploadSignature');
Route::get('jollychic/getundeliveredreasons','JollyChicController@getUndeliveredReasons');


Route::post('jollychic/cardSelectionCaptain', 'PayFortAPIController@cardSelectionCaptain');

Route::post('jollychic/chargePaymentCaptain', 'PayFortAPIController@chargePaymentCaptain');

Route::get('jollychic/getcaptaindeliverystatus','JollyChicController@getcaptaindeliverystatus');

Route::get('jollychic/getitemstatus/{id}','JollyChicController@getitemstatus'); // used by JC as well 

Route::get('jollychic/getitempincode/{id}','JollyChicController@getitempincode');

Route::get('jollychic/isvalidpincode/{id}/{pincode}','JollyChicController@isvalidpincode');

Route::post('jollychic/getplanneddeliveryrequests','JollyChicController@getPlannedDeliveryRequests');

Route::post('jollychic/getscanneddeliveryrequests','JollyChicController@getScannedDeliveryRequests');

Route::post('jollychic/getconfirmeddeliveryrequests','JollyChicController@getConfirmedDeliveryRequests');

Route::get('/jollychic/getappversion', 'JollyChicController@get_app_version');

Route::get('jollychic/getitem/{itemid}','JollyChicController@get_item');
Route::get('jollychic/getitemdetails/{itemid}','JollyChicController@getitemDetails');

Route::get('jollychic/getcompanies','JollyChicController@getCompanies');

Route::get('jollychic/getcitiesbyadmin','JollyChicController@getCitiesbAdmin');

////////////////
// merchants && pickup 
Route::post('/jollychic/merchant/login', 'JollyChicController@merchantlogin');
Route::post('/jollychic/getorders', 'JollyChicController@getorders');
Route::post('/jollychic/ordersPool', 'JollyChicController@ordersPool');
Route::get('/jollychic/getPickupTimeIntervals', 'JollyChicController@pickup_time_intervals'); 
/////////////////

Route::get('/jollychic/maroof_merchants', 'JollyChicController@maroof_merchants');

Route::post('/sendsms','JollyChicController@send_sms');
Route::post('jollychic/getCoveredDeliveryRequests','JollyChicController@getCoveredDeliveryRequests');

Route::get('/jollychic/test', 'JollyChicController@test');


////////////////////////
// Warehouse 
// Tariq Alturkestani
// December 20 2017


Route::get('/warehouse/login', array('as' => 'WarehouseAdminLogin', 'uses' => 'WarehouseController@login'));
Route::get('/warehouse/login/passwd', 'WarehouseController@passwd');
Route::post('/warehouse/login/passwd/verify', 'WarehouseController@passwd_verify');

Route::post('/warehouse/verify', array('as' => 'WarehouseAdminVerify', 'uses' => 'WarehouseController@verify'));

Route::get('/warehouse/logout', array('as' => 'WarehouseAdminLogout', 'uses' => 'WarehouseController@logout'));

Route::get('/warehouse/test', 'WarehouseController@test');

Route::get('/warehouse', 'WarehouseController@newshipments');
Route::post('/warehousepost', 'WarehouseController@indexpost');
Route::get('/warehouse/warehouse', 'WarehouseController@index');
Route::get('/warehouse/jc', 'WarehouseController@jchome');
Route::get('/warehouse/admintracking', 'WarehouseController@admintracking');
Route::get('/warehouse/admintrackingbulk', 'WarehouseController@admintrackingbulk');


Route::get('/warehouse/shipmentstatusbymonth', 'WarehouseController@homebymonth');


Route::get('/warehouse/hovertracking', 'WarehouseController@hovertracking');


Route::get('/warehouse/customerservice', 'WarehouseController@customerservice');

Route::get('/warehouse/404', 'WarehouseController@page404');
Route::get('/warehouse/403', 'WarehouseController@page403');
Route::get('/warehouse/500', 'WarehouseController@page500');


Route::post('/warehouse/getnewshipmentscountbydate','WarehouseController@getnewshipmentscountbydate');

Route::post('/warehouse/getnewshipmentscountbyscheduledate','WarehouseController@getnewshipmentscountbyscheduledate');

Route::get('/warehouse/captains', 'WarehouseController@captains');
Route::post('/warehouse/captainspost', 'WarehouseController@captainspost');

Route::get('/warehouse/captainaccounting', 'AccountingController@captainAccounting');

Route::post('/warehouse/codreportredirect', 'AccountingController@codReportRedirect');
Route::post('/warehouse/captaincodreport', 'AccountingController@captaincodreport');
Route::post('/warehouse/checkcodviewstatus', 'AccountingController@checkcodviewstatus');
Route::get('/downloadcodReportDirect', 'AccountingController@downloadcodReportDirect');
Route::post('/warehouse/checkcaptaincodviewstatus', 'AccountingController@checkcaptaincodviewstatus');
Route::get('/downloadcaptaincodReportDirect', 'AccountingController@downloadcaptaincodReportDirect');

Route::get('/warehouse/companies', 'WarehouseController@companies');
Route::get('/warehouse/companydetail', array('as' => 'AdminViewCompany', 'uses' => 'WarehouseController@companyDetail'));
Route::get('/warehouse/pendingmerchants', 'WarehouseController@pendingmerchants');

Route::get('/warehouse/payments','WarehouseController@allpayments');
Route::post('/warehouse/payments/checkcodreportviewstatus','WarehouseController@checkcodreportviewstatus');
Route::post('/warehouse/payments/downloadcaptaincodReportDirect','WarehouseController@downloadcaptaincodReportDirect');
Route::get('/warehouse/payments/captainaccount/{captainid}', 'WarehouseController@captainaccount');
Route::post('/warehouse/payments/insertpayment','WarehouseController@insertpayment');

Route::get('/warehouse/shipments/{shipmentdate}', 'WarehouseController@shipmentsDetails');

Route::get('/warehouse/shipmentsV2/{shipmentdate}', 'WarehouseController@shipmentsDetailsV2');
Route::get('/warehouse/shipmentsExport', 'WarehouseController@shipmentsExport');
Route::post('/warehouse/checkshipmentsExportviewstatus', 'WarehouseController@checkshipmentsExportviewstatus');
Route::get('/downloadshipmentsExportReport', 'WarehouseController@downloadshipmentsExportReport');
Route::get('/warehouse/durationAnalysis', 'WarehouseController@durationAnalysis');
Route::get('/warehouse/totalNewShipments', 'WarehouseController@totalNewShipments');
Route::post('/warehouse/shipmentsIdsExport', 'WarehouseController@shipmentsIdsExport');
Route::post('/warehouse/checkshipmentsIdsviewstatus', 'WarehouseController@checkshipmentsIdsviewstatus');
Route::post('/downloadshipmentsIdsReport', 'WarehouseController@downloadshipmentsIdsReport');
Route::post('/warehouse/shipmentsIdExport', 'WarehouseController@shipmentsIdExport');
Route::get('/warehouse/sticker/{packageID}', 'WarehouseController@sticker');
Route::get('/warehouse/unCoveredShipments', 'WarehouseController@unCoveredShipments');
Route::post('/warehouse/unCoveredShipmentspost', 'WarehouseController@unCoveredShipmentspost');

Route::get('/warehouse/shipmentsExportV2/{shipmentdate}', 'WarehouseController@shipmentsExportV2');

Route::get('/warehouse/package/{packageID}', 'WarehouseController@packageDetails');

Route::get('/warehouse/jcpackage/{packageID}', 'WarehouseController@jcPackageDetails');


Route::post('/warehouse/sendwelcomesms','WarehouseController@sendwelcomesms');

Route::post('/warehouse/checkplannedcaptain','WarehouseController@checkplannedcaptain');

Route::post('/warehouse/getcaptaininfo','WarehouseController@getcaptaininfo');
Route::post('/warehouse/captaininfo','WarehouseController@captaininfo');
Route::post('/warehouse/getcaptainscanshipment','WarehouseController@getcaptainscanshipment');

Route::get('/warehouse/convertshipmentstatus', 'WarehouseController@convertshipmentstatus');
Route::post('/warehouse/convertshipmentstatuspost', 'WarehouseController@convertshipmentstatuspost');

///////////////
/// hubs
Route::get('/warehouse/hub/view', 'WarehouseController@hub_view');
Route::get('/warehouse/hub/view/{id}/details', 'WarehouseController@hub_details');
///////////////

///////////////
/// porta hubs
Route::get('/warehouse/portahub/view', 'WarehouseController@portahub_view');
Route::get('/warehouse/portahub/view/{id}/details', 'WarehouseController@portahub_details');

///////////////
/// reports
// Route::get('/warehouse/successratereport', 'WarehouseController@successratereport');
// Route::post('/warehouse/successratereportpost', 'WarehouseController@successratereport');

Route::get('/warehouse/successratereport', 'WarehouseController@successratereport');
Route::get('/warehouse/successratereport/export', 'WarehouseController@successratereport_export');

Route::get('/warehouse/shipmentsperbranch', 'WarehouseController@shipmentsperbranch');
Route::get('/warehouse/shipmentsperbranchpost', 'WarehouseController@shipmentsperbranch');

Route::get('/warehouse/branchperformance', 'WarehouseController@branchperformance');
Route::post('/warehouse/branchperformancepost', 'WarehouseController@branchperformance');

Route::get('/warehouse/branchperformanceen', 'WarehouseController@branchperformanceen');
Route::post('/warehouse/branchperformanceenpost', 'WarehouseController@branchperformanceen');


Route::get('/warehouse/cashflow', 'WarehouseController@cashflow');
Route::post('/warehouse/cashflowpost', 'WarehouseController@cashflow');

Route::get('/warehouse/inventoryreport', 'WarehouseController@inventoryreport');
Route::post('/warehouse/inventoryreportpost', 'WarehouseController@inventoryreport');

Route::get('/warehouse/ticketsreport', 'WarehouseController@ticketsreport');
Route::post('/warehouse/ticketsreportpost', 'WarehouseController@ticketsreport');
Route::post('/warehouse/ticket/calls/insert', 'WarehouseController@insertcallsrecord');

Route::get('/warehouse/returnsreport', 'WarehouseController@returnsreport');
Route::post('/warehouse/returnsreportpost', 'WarehouseController@returnsreport');

Route::get('/warehouse/sameDaySuccessRate', 'WarehouseController@sameDaySuccessRate');
Route::post('/warehouse/sameDaySuccessRate/filter', 'WarehouseController@sameDaySuccessRate');

Route::get('/warehouse/pincodereport', 'WarehouseController@pincodereport');

Route::get('/warehouse/kpireport', 'WarehouseController@kpi_report');
/////////////////

////////////////////////
/// ticketing

Route::get('/warehouse/ticket/handle/{waybill}', 'WarehouseController@handleticket');
Route::post('/warehouse/ticket/inquiry', 'WarehouseController@inquiry');
Route::get('/warehouse/ticket/view/{waybill}', 'WarehouseController@viewticket');

Route::get('/warehouse/ticket/export', 'WarehouseController@ticketexport');
Route::get('/warehouse/ticket/export/single', 'WarehouseController@exportsingle');
Route::post('/warehouse/ticket/export/bulk', 'WarehouseController@exportbulk');
Route::get('/warehouse/ticket/export/filter', 'WarehouseController@exportfilter');
////////////////////////

Route::get('/warehouse/sendpushtocaptain', 'WarehouseController@send_push_to_captain');
Route::post('/warehouse/SendPushToCaptainPost', 'WarehouseController@send_push_to_captain_post');

Route::get('/warehouse/sendnotificationtocaptain', 'WarehouseController@sendnotificationtocaptain');
Route::post('/warehouse/SendNotificationToCaptainPost', 'WarehouseController@SendNotificationToCaptainPost');

////////////////////////
/// merchants

Route::get('/merchant', ['as' => 'Merchant', 'uses' => 'MerchantController@index']);
Route::get('/merchant/login', ['as' => 'MerchantLogin', 'uses' => 'MerchantController@login']);
Route::post('/merchant/verify', 'MerchantController@verify');
Route::get('/merchant/forgetPassword', 'MerchantController@forgetPassword');
Route::get('/merchant/logout', 'MerchantController@logout');
Route::get('/merchant/welcome', 'MerchantController@welcome');

Route::get('/merchant/dashboard', 'MerchantController@dashboard');

Route::get('/merchant/orders', 'MerchantController@getorders');
Route::get('/merchant/exportShipments', 'MerchantController@exportshipments');
Route::get('/merchant/exportSingle', 'MerchantController@exportsingle');
Route::get('/merchant/exportFilter', 'MerchantController@shipmentsExport');
Route::get('/merchant/shipmentDetails/{waybill}', 'MerchantController@shipmentdetails');
// Route::get('/company/sticker/{packageID}', 'CompanyController@sticker');
Route::get('/merchant/sticker/{packageID}', 'MerchantController@sticker');

Route::get('/merchant/pickupInfo/{waybill}', 'MerchantController@pickupInfo');
Route::post('/merchant/pickupInfoPost/', 'MerchantController@pickupInfoPost');

Route::get('/merchant/trackShipment', 'MerchantController@trackShipment');
Route::post('/merchant/trackShipmentPost', 'MerchantController@trackShipmentPost');

Route::get('/merchant/bulkTrackShipment', 'MerchantController@bulkTrackShipment');
Route::post('/merchant/bulkTrackShipmentPost', 'MerchantController@bulkTrackShipmentPost');

Route::post('/test/test', 'MerchantController@test');
////////////////////////


// tracking 
Route::get('/tracking/test', 'TrackingController@test');

Route::get('/tracking/', 'TrackingController@tracking');
Route::get('/trackingpage', 'TrackingController@trackingpage');
Route::post('deliveryrequest/shipmenttracking', 'TrackingController@shipmenttracking');
Route::post('deliveryrequest/shipmenttrackingv2', 'TrackingController@shipmenttrackingv2');


// tariq private:
Route::get('/private/', 'TariqController@home');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Route::get('/dist', 'HelloController@index');
Route::get('/test', 'HelloController@test');

Route::get('/payms', 'HelloController@payms');

Route::post('/walk/walksummary', 'WalkerController@walk_summary');

Route::post('/walk/photo', 'WalkerController@upload_photo');

Route::post('/walk/video', 'WalkerController@upload_video');

Route::get('/walker/walks', 'WalkerController@get_walks');

Route::get('/walker/details', 'WalkerController@get_details');

Route::post('/walker/getwalk', 'WalkerController@walk_details');

Route::post('/walker/getschedule', 'WalkerController@get_schedule');


// On Demand API's
// Owner APIs

Route::post('/user/mapdistance', 'OwnerController@cost_calculation');

Route::post('/user/log', 'OwnerController@login');

Route::post('/user/captaindetails', 'OwnerController@captain_details');

Route::post('/user/monthlypaymentmode', 'OwnerController@monthly_payment_mode');

Route::any('/user/details', 'OwnerController@details');

Route::get('/user/braintreekey', 'OwnerController@get_braintree_token');

Route::post('/user/deletecardtoken', 'OwnerController@deletecardtoken');

Route::post('/user/paydebt', 'OwnerController@pay_debt');

Route::post('/user/selectcard', 'OwnerController@select_card');

Route::post('/user/card_selection', 'OwnerController@card_selection');

Route::post('/user/chargepayment', 'OwnerController@chargepayment');

Route::get('/user', 'OwnerController@getProfile');

Route::post('/user/payment_type', 'OwnerController@payment_type');

Route::post('/user/freezemonthly', 'OwnerController@freeze_monthly');

Route::post('/user/changecaptainmonthly', 'OwnerController@change_captain_monthly');

Route::post('/user/respondmonthlysubscription', 'OwnerController@respond_monthly_subscription');

Route::get('/provider/requestpath', 'WalkerController@get_walk_location');

Route::get('/user/referral', 'OwnerController@get_referral_code');

Route::post('/user/apply-referral', 'OwnerController@apply_referral_code');

Route::post('/user/apply-promo', 'OwnerController@apply_promo_code');

Route::get('/user/cards', 'OwnerController@get_cards');

Route::get('/user/history', 'OwnerController@get_completed_requests');

Route::post('/user/payfortfeedback', 'OwnerController@payfort_feedback');


Route::post('/user/paybypaypal', 'OwnerController@paybypaypal');

Route::post('/user/paybybitcoin', 'OwnerController@paybybitcoin');

Route::post('/user/acceptbitcoin', 'OwnerController@acceptbitcoin');

Route::get('/user/send_eta', 'OwnerController@send_eta');

Route::get('/user/payment_options', array('as' => '/user/payment_options', 'uses' => 'OwnerController@payment_options_allowed'));

Route::post('/user/logout', 'OwnerController@logout');

Route::get('/provider/check_banking', 'WalkerController@check_banking');

Route::post('/user/monthlyconfirmationrespond', 'OwnerController@monthly_confirmation_respond');

Route::post('/user/userongoingsubcsription', 'OwnerController@user_ongoing_subscription');

Route::post('/user/confirmcashamount', 'OwnerController@confirm_cash_amount');

Route::post('/user/invitefriend', 'OwnerController@invite_friend');

Route::get('/user/getinvites', 'OwnerController@get_invites');

Route::post('/user/respondinvite', 'OwnerController@respond_invite');

Route::get('/user/inviteinprogress', 'OwnerController@invite_in_progress');

Route::post('/user/asktojoin', 'OwnerController@ask_to_join');

Route::post('/user/getjoin', 'OwnerController@get_join');

Route::post('/user/acceptmonthlyasprivate', 'OwnerController@accept_monthly_as_private');

Route::post('/user/getcostestimate', 'OwnerController@get_cost_estimate');

Route::get('/user/getappversion', 'OwnerController@get_app_version');

Route::post('/user/deletefavoritelocation', 'OwnerController@delete_favorite_location');

Route::get('/user/getlocations', 'OwnerController@get_locations');

Route::post('/user/requestmonthlywalker', 'OwnerController@request_monthly_walker');

Route::post('/user/confirmmonthlywalker', 'OwnerController@confirm_monthly_walker');

Route::post('/user/refreshmonthlywalkers', 'OwnerController@refresh_monthly_walkers');


// Walker APIs

Route::get('/provider/getdeliveryrequest', 'WalkerController@get_delivery_request');

Route::get('/provider/getcompleteddeliveryrequests', 'WalkerController@get_completed_delivery_requests');

Route::post('/provider/walklocationqueue', 'WalkerController@walk_location_queue');

Route::get('/provider/viewprofile', 'WalkerController@view_profile');

Route::post('/provider/joininprogress', 'WalkerController@join_in_progress');

Route::post('/provider/respondjoin', 'WalkerController@respond_join');

Route::get('/provider/getappversion', 'WalkerController@get_app_version');

Route::post('/provider/heartbeat', 'WalkerController@heart_beat');

Route::post('/provider/confirminvoice', 'WalkerController@confirm_invoice');

Route::post('/provider/postcashamount', 'WalkerController@post_cash_amount');

Route::get('/provider/callScheduler', 'WalkerController@call_scheduler');

Route::get('/provider/confirmavailabilityOfsubscription', 'WalkerController@confirm_availability_Of_subscription');

Route::post('/provider/respondavailabilityOfsubscription', 'WalkerController@respond_availability_Of_subscription');

Route::post('/provider/monthlypaymentconfirm', 'WalkerController@monthly_payment_confirm');

Route::get('/provider/getrequests', 'WalkerController@get_requests');

Route::get('/provider/getrequest', 'WalkerController@get_request');

Route::post('/provider/respondrequest', 'WalkerController@respond_request');

Route::post('/provider/monthlyrespondrequest', 'WalkerController@monthly_respond_request');

Route::any('/provider/ongoingsubcsription', 'WalkerController@ongoing_subscription');

Route::any('/provider/ongoingsubcsriptionsingle', 'WalkerController@ongoing_subscription_single');

Route::post('/provider/location', 'WalkerController@walker_location');

Route::post('/provider/requestwalkerstarted', 'WalkerController@request_walker_started');

Route::post('/provider/requestwalkerarrived', 'WalkerController@request_walker_arrived');

Route::post('/provider/requestwalkstarted', 'WalkerController@request_walk_started');

Route::post('/request/location', 'WalkerController@walk_location');

Route::post('/provider/requestwalkcompleted', 'WalkerController@request_walk_completed');

Route::post('/provider/prepayment', 'WalkerController@pre_payment');

Route::post('/provider/paymentselection', 'WalkerController@payment_selection');

Route::post('/provider/log', 'WalkerController@login');

Route::post('/provider/login', 'WalkerController@login');

Route::post('provider/deleteCardTokenCaptain', 'WalkerController@deleteCardTokenCaptain');

Route::post('provider/getCardsCaptain', 'WalkerController@getCardsCaptain');

Route::get('/provider/services_details', 'WalkerController@services_details');

Route::get('/provider/requestinprogress', 'WalkerController@request_in_progress');

Route::get('/provider/checkstate', 'WalkerController@check_state');

Route::post('/provider/togglestate', 'WalkerController@toggle_state');

Route::post('/provider/togglestatemonthly', 'WalkerController@toggle_state_monthly');

Route::get('/provider/history', 'WalkerController@get_completed_requests');

Route::post('panic', array('as' => 'panic', 'uses' => 'WalkerController@panic'));
Route::post('provider/getcaptainshipmentsforstatus','WalkerController@getcaptainshipmentsforstatus');

Route::post('/provider/logout', 'WalkerController@logout');

// Info Page API
Route::get('/application/pages', 'ApplicationController@pages');

Route::get('/application/types', 'ApplicationController@types');

Route::post('/user/fetchavailablecars', 'ApplicationController@available_types');

Route::get('/application/page/{id}', 'ApplicationController@get_page');

Route::post('/application/forgot-password', 'ApplicationController@forgot_password');

// Admin Panel

Route::get('/admin/report_bug', array('as' => 'AdminReportBug', 'uses' => 'AdminController@report_bug'));

Route::post('/admin/report_bug_post', array('as' => 'AdminReportBugPost', 'uses' => 'AdminController@report_bug_post'));

Route::get('/admin/report', array('as' => 'AdminReport', 'uses' => 'AdminController@report'));

Route::get('/admin/reportrequestcount', array('as' => 'AdminReportRequestCount', 'uses' => 'AdminController@report_request_count'));

Route::get('/admin/reportonlinehours', array('as' => 'AdminReportOnlineHours', 'uses' => 'AdminController@report_online_hours'));

Route::get('/admin/feedbackprovider', array('as' => 'AdminFeedbacksProvider', 'uses' => 'AdminController@walker_feedbacks'));

Route::get('/admin/feedbackuser', array('as' => 'AdminFeedbacksUser', 'uses' => 'AdminController@owner_feedbacks'));

Route::get('/admin/payprovider/{id}', array('as' => 'AdminPayProvider', 'uses' => 'AdminController@pay_provider'));

Route::get('/admin/chargeuser/{id}', array('as' => 'AdminChargeUser', 'uses' => 'AdminController@charge_user'));

Route::post('/admin/transfer_amount', array('as' => 'AdminProviderPay', 'uses' => 'AdminController@transfer_amount'));

Route::get('/admin/map_view', array('as' => 'AdminMapview', 'uses' => 'AdminController@map_view'));

Route::get('/admin/providers', array('as' => 'AdminProviders', 'uses' => 'AdminController@walkers'));

Route::get('/admin/users', array('as' => 'AdminUsers', 'uses' => 'AdminController@owners'));

Route::get('/admin/requests', array('as' => 'AdminRequests', 'uses' => 'AdminController@walks'));

Route::get('/admin/monthly_subscription', array('as' => 'AdminMonthlySubscription', 'uses' => 'AdminController@monthly_subscription'));

Route::get('/admin/monthly_request/{id}', array('as' => 'AdminMonthlyRequest', 'uses' => 'AdminController@create_monthly_request'));

Route::get('/admin/monthly_subscription_view/{id}', array('as' => 'AdminMonthlySubscriptionView', 'uses' => 'AdminController@view_monthly_subscription'));

Route::get('/admin/monthly_cancle/{id}', array('as' => 'AdminCancleMonthlyRequest', 'uses' => 'AdminController@cancle_monthly_request'));

Route::get('/admin/reviews', array('as' => 'AdminReviews', 'uses' => 'AdminController@reviews'));

Route::get('/admin/reviews/delete/{id}', array('as' => 'AdminReviewsDelete', 'uses' => 'AdminController@delete_review'));

Route::get('/admin/reviews/delete_client/{id}', array('as' => 'AdminReviewsDeleteDog', 'uses' => 'AdminController@delete_review_owner'));

Route::get('/admin/search', array('as' => 'AdminSearch', 'uses' => 'AdminController@search'));

Route::get('/admin/login', array('as' => 'AdminLogin', 'uses' => 'AdminController@login'));

Route::post('/admin/verify', array('as' => 'AdminVerify', 'uses' => 'AdminController@verify'));

Route::get('/admin/logout', array('as' => 'AdminLogout', 'uses' => 'AdminController@logout'));

Route::get('/admin/cities', array('as' => 'AdminCities', 'uses' => 'AdminController@cities'));

Route::get('/admin/city/delete/{id}', array('as' => 'AdminCityDelete', 'uses' => 'AdminController@delete_city'));

Route::get('/admin/admin_city_privilege/{id}', array('as' => 'AdminCityPrivilege', 'uses' => 'AdminController@admin_city_privilege'));

Route::post('/admin/admin_city_privilege_post', array('as' => 'AdminCityPrivilegePost', 'uses' => 'AdminController@admin_city_privilege_post'));

Route::get('/admin/admins', array('as' => 'AdminAdmins', 'uses' => 'AdminController@admins'));

Route::get('/admin/user/referral/{id}', array('as' => 'AdminUserReferral', 'uses' => 'AdminController@referral_details'));

Route::get('/admin/admins/delete/{id}', array('as' => 'AdminAdminsDelete', 'uses' => 'AdminController@delete_admin'));

Route::get('/admin', array('as' => 'admin', 'uses' => 'AdminController@index'));

Route::get('/admin/provider/view/{id}', array('as' => 'AdminProviderView', 'uses' => 'AdminController@view_walker'));

Route::get('/admin/promo_code/deactivate/{id}', array('as' => 'AdminPromoCodeDeactivate', 'uses' => 'AdminController@deactivate_promo_code'));

Route::get('/admin/promo_code/activate/{id}', array('as' => 'AdminPromoCodeActivate', 'uses' => 'AdminController@activate_promo_code'));

Route::get('/admin/provider/history/{id}', array('as' => 'AdminProviderHistory', 'uses' => 'AdminController@walker_history'));

Route::get('/admin/provider/documents/{id}', array('as' => 'AdminProviderDocuments', 'uses' => 'AdminController@walker_documents'));

Route::get('/admin/provider/requests/{id}', array('as' => 'AdminProviderRequests', 'uses' => 'AdminController@walker_upcoming_walks'));

Route::get('/admin/provider/decline/{id}', array('as' => 'AdminProviderDecline', 'uses' => 'AdminController@decline_walker'));

//Route::get('/admin/provider/delete/{id}', array('as' => 'AdminProviderDelete', 'uses' => 'AdminController@delete_walker'));

Route::get('/admin/provider/approve/{id}', array('as' => 'AdminProviderApprove', 'uses' => 'AdminController@approve_walker'));

Route::get('/admin/provider/online/{id}', array('as' => 'AdminProviderOnline', 'uses' => 'AdminController@online_walker'));

Route::get('/admin/provider/active/{id}', array('as' => 'AdminProviderActive', 'uses' => 'AdminController@active_walker'));

Route::get('/admin/provider/pending/{id}', array('as' => 'AdminProviderPending', 'uses' => 'AdminController@move_to_pending_walker'));

Route::get('/admin/providers_xml', array('as' => 'AdminProviderXml', 'uses' => 'AdminController@walkers_xml'));


Route::get('/admin/user/delete/{id}', array('as' => 'AdminDeleteUser', 'uses' => 'AdminController@delete_owner'));

Route::get('/admin/user/history/{id}', array('as' => 'AdminUserHistory', 'uses' => 'AdminController@owner_history'));

Route::get('/admin/user/requests/{id}', array('as' => 'AdminUserRequests', 'uses' => 'AdminController@owner_upcoming_walks'));

Route::get('/admin/request/decline/{id}', array('as' => 'AdminUserDecline', 'uses' => 'AdminController@decline_walk'));

Route::get('/admin/request/approve/{id}', array('as' => 'AdminUserApprove', 'uses' => 'AdminController@approve_walk'));

Route::get('/admin/request/map/{id}', array('as' => 'AdminRequestsMap', 'uses' => 'AdminController@view_map'));

Route::get('/admin/request/correct_invoice/{id}', array('as' => 'AdminRequestsCorrectInvoice', 'uses' => 'AdminController@correct_invoice'));

Route::get('/admin/request/change_provider/{id}', array('as' => 'AdminRequestChangeProvider', 'uses' => 'AdminController@change_walker'));

Route::get('/admin/request/alternative_providers_xml/{id}', array('as' => 'AdminRequestsAlternative', 'uses' => 'AdminController@alternative_walkers_xml'));

Route::post('/admin/request/pay_provider', array('as' => 'AdminRequestPay', 'uses' => 'AdminController@pay_walker'));

Route::post('/admin/install', array('as' => 'AdminInstallFinish', 'uses' => 'AdminController@finish_install'));

Route::post('/admin/theme', array('as' => 'AdminTheme', 'uses' => 'AdminController@theme'));

Route::get('/admin/informations', array('as' => 'AdminInformations', 'uses' => 'AdminController@get_info_pages'));

Route::get('/admin/information/delete/{id}', array('as' => 'AdminInformationDelete', 'uses' => 'AdminController@delete_info_page'));

Route::get('/admin/provider-types', array('as' => 'AdminProviderTypes', 'uses' => 'AdminController@get_provider_types'));

Route::get('/admin/provider-type/delete/{id}', array('as' => 'AdminProviderTypeDelete', 'uses' => 'AdminController@delete_provider_type'));

Route::get('/admin/document-types', array('as' => 'AdminDocumentTypes', 'uses' => 'AdminController@get_document_types'));

Route::get('/admin/promo_code', array('as' => 'AdminPromoCodes', 'uses' => 'AdminController@get_promo_codes'));

Route::get('/admin/document-type/delete/{id}', array('as' => 'AdminDocumentTypesDelete', 'uses' => 'AdminController@delete_document_type'));

Route::post('/admin/adminCurrency', array('as' => 'adminCurrency', 'uses' => 'AdminController@adminCurrency'));

Route::get('/admin/details_payment', array('as' => 'AdminPayment', 'uses' => 'AdminController@payment_details'));


Route::get('/admin/send_push_to_customer', array('as' => 'SendPushToCustomerPost', 'uses' => 'AdminController@send_push_to_customer'));

Route::post('/admin/send_push_to_customer_post', array('as' => 'SendPushToCustomerPost', 'uses' => 'AdminController@send_push_to_customer_post'));

Route::get('/admin/send_push_to_captain', array('as' => 'SendPushToCaptainPost', 'uses' => 'AdminController@send_push_to_captain'));

Route::post('/admin/send_push_to_captain_post', array('as' => 'SendPushToCaptainPost', 'uses' => 'AdminController@send_push_to_captain_post'));



Route::get('/admin/provider/banking/{id}', array('as' => 'AdminProviderBanking', 'uses' => 'AdminController@banking_provider'));

Route::post('/admin/provider/providerB_bankingSubmit', array('as' => 'AdminProviderBBanking', 'uses' => 'AdminController@providerB_bankingSubmit'));

Route::post('/admin/provider/providerS_bankingSubmit', array('as' => 'AdminProviderSBanking', 'uses' => 'AdminController@providerS_bankingSubmit'));

//Admin Panel Sorting 

Route::get('/admin/sortur', array('as' => '/admin/sortur', 'uses' => 'AdminController@sortur'));

Route::get('/admin/sortpv', array('as' => '/admin/sortpv', 'uses' => 'AdminController@sortpv'));

Route::get('/admin/sortpvtype', array('as' => '/admin/sortpvtype', 'uses' => 'AdminController@sortpvtype'));

Route::get('/admin/sortreq', array('as' => '/admin/sortreq', 'uses' => 'AdminController@sortreq'));

Route::get('/admin/sortpromo', array('as' => '/admin/sortpromo', 'uses' => 'AdminController@sortpromo'));

//Provider Availability

Route::get('/admin/provider/allow_availability', array('as' => 'AdminProviderAllowAvailability', 'uses' => 'AdminController@allow_availability'));

Route::get('/admin/provider/disable_availability', array('as' => 'AdminProviderDisableAvailability', 'uses' => 'AdminController@disable_availability'));

Route::get('/admin/provider/availability/{id}', array('as' => 'AdminProviderAvailability', 'uses' => 'AdminController@availability_provider'));

Route::post('/admin/provider/availabilitySubmit/{id}', array('as' => 'AdminProviderAvailabilitySubmit', 'uses' => 'AdminController@provideravailabilitySubmit'));

Route::get('/admin/provider/view_documents/{id}', array('as' => 'AdminViewProviderDoc', 'uses' => 'AdminController@view_documents_provider'));

//Providers Who currently walking

Route::get('/admin/provider/current', array('as' => 'AdminProviderCurrent', 'uses' => 'AdminController@current'));

Route::get('/admin/provider/blocked', array('as' => 'AdminProviderBlocked', 'uses' => 'AdminController@blocked_providers'));


// Web User

Route::get('/oldwebsite', 'WebController@index');

Route::get('/oldwebsitear', 'WebController@indexar');

Route::get('/user/signin', array('as' => '/user/signin', 'uses' => 'WebUserController@userLogin'));

Route::post('/user/forgot-password', array('as' => '/user/forgot-password', 'uses' => 'WebUserController@userForgotPassword'));

Route::get('/user/logout', array('as' => '/user/logout', 'uses' => 'WebUserController@userLogout'));

Route::post('/user/verify', array('as' => '/user/verify', 'uses' => 'WebUserController@userVerify'));

Route::get('/user/trips', array('as' => '/user/trips', 'uses' => 'WebUserController@userTrips'));

Route::get('/user/trip/status/{id}', array('as' => '/user/trip/status', 'uses' => 'WebUserController@userTripStatus'));


Route::get('/find', array('as' => '/find', 'uses' => 'WebUserController@surroundingCars'));


Route::get('user/paybypaypal/{id}', array('as' => 'user/paybypaypal', 'uses' => 'WebUserController@webpaybypaypal'));

Route::get('user/paybypalweb/{id}', array('as' => 'user/paybypalweb', 'uses' => 'WebUserController@paybypalwebSubmit'));

Route::get('userpaypalstatus', array('as' => 'userpaypalstatus', 'uses' => 'WebUserController@paypalstatus'));

Route::get('userpaypalipn', array('as' => 'userpaypalipn', 'uses' => 'WebUserController@userpaypalipn'));


Route::get('/user/request-trip', array('as' => 'userrequestTrip', 'uses' => 'WebUserController@userRequestTrip'));

Route::get('/user/skipReview/{id}', array('as' => 'userSkipReview', 'uses' => 'WebUserController@userSkipReview'));


Route::post('/user/eta', array('as' => 'etaweb', 'uses' => 'WebUserController@send_eta_web'));

Route::get('/user/request-fare', array('as' => 'userrequestFare', 'uses' => 'WebUserController@request_fare'));

Route::get('/user/requesteta', array('as' => 'userrequestETA', 'uses' => 'WebUserController@request_eta'));

Route::get('/user/profile', array('as' => '/user/profile', 'uses' => 'WebUserController@userProfile'));

Route::get('/user/payments', array('as' => 'userPayment', 'uses' => 'WebUserController@userPayments'));


Route::get('termsncondition', array('as' => 'termsncondition', 'uses' => 'WebController@termsncondition'));

Route::get('banking_provider_mobile/{id}', array('as' => 'banking_provider_mobile', 'uses' => 'WebController@banking_provider_mobile'));

Route::post('provider/provider_braintree_banking', array('as' => 'ProviderBBanking', 'uses' => 'WebController@providerB_bankingSubmit'));

Route::post('provider/provider_stripe_banking', array('as' => 'ProviderSBanking', 'uses' => 'WebController@providerS_bankingSubmit'));


Route::get('page/{title}', array('as' => 'page', 'uses' => 'WebController@page'));

Route::get('track/{id}', array('as' => 'track', 'uses' => 'WebController@track_ride'));

Route::get('get_track_loc/{id}', array('as' => 'getTrackLoc', 'uses' => 'WebController@get_track_loc'));


Route::get('/user/payment/delete/{id}', array('as' => '/user/payment/delete', 'uses' => 'WebUserController@deleteUserPayment'));

Route::get('/user/trip/{id}', array('as' => '/user/trip', 'uses' => 'WebUserController@userTripDetail'));


// Search Admin Panel
Route::get('/admin/searchpv', array('as' => '/admin/searchpv', 'uses' => 'AdminController@searchpv'));
Route::get('/admin/searchur', array('as' => '/admin/searchur', 'uses' => 'AdminController@searchur'));
Route::get('/admin/searchreq', array('as' => '/admin/searchreq', 'uses' => 'AdminController@searchreq'));
Route::get('/admin/searchrev', array('as' => '/admin/searchrev', 'uses' => 'AdminController@searchrev'));
Route::get('/admin/searchinfo', array('as' => '/admin/searchinfo', 'uses' => 'AdminController@searchinfo'));
Route::get('/admin/searchpvtype', array('as' => '/admin/searchpvtype', 'uses' => 'AdminController@searchpvtype'));
Route::get('/admin/searchdoc', array('as' => '/admin/searchdoc', 'uses' => 'AdminController@searchdoc'));
Route::get('/admin/searchpromo', array('as' => '/admin/searchpromo', 'uses' => 'AdminController@searchpromo'));


// Web Provider


Route::get('/changelanguage', array(
    'as' => 'ChangeLanguage',
    'uses' => 'WebProviderController@changelanguage'
));
Route::get('/provider/signin', array(
    'as' => 'ProviderSignin',
    'uses' => 'WebProviderController@providerLogin'
));

Route::get('/provider/activation/{act}', array(
    'as' => '/provider/activation',
    'uses' => 'WebProviderController@providerActivation'
));


Route::get('/captain/getlatesttransactions','CaptainController@getlatesttransactions');
Route::get('/captain/getnationalities','CaptainController@getnationalities');
Route::get('/captain/getregistrationparams','CaptainController@getregistrationparams');
Route::post('/captain/getrandomdeliveryrequests','CaptainController@randomdeliveryrequests');

Route::get('/provider/availability', array(
    'as' => 'ProviderAvail',
    'uses' => 'WebProviderController@provideravailability'
));

Route::post('/provider/availabilitysubmit', array(
    'as' => 'provideravailabilitySubmit',
    'uses' => 'WebProviderController@provideravailabilitysubmit'
));



Route::post('/provider/forgot-password', array(
    'as' => 'providerForgotPassword',
    'uses' => 'WebProviderController@providerForgotPassword'));

Route::get('/provider/logout', array(
    'as' => 'ProviderLogout',
    'uses' => 'WebProviderController@providerLogout'
));


Route::post('/provider/verify', array(
    'as' => 'ProviderVerify',
    'uses' => 'WebProviderController@providerVerify'
));



Route::get('/provider/trips', array(
    'as' => 'ProviderTrips',
    'uses' => 'WebProviderController@providerTrips'
));


Route::get('/provider/trip/{id}', array(
    'as' => 'ProviderTripDetail',
    'uses' => 'WebProviderController@providerTripDetail'
));


Route::get('/provider/trip/changestate/{id}', array(
    'as' => 'providerTripChangeState',
    'uses' => 'WebProviderController@providerTripChangeState'
));


Route::get('/provider/tripinprogress', array(
    'as' => 'providerTripInProgress',
    'uses' => 'WebProviderController@providerTripInProgress'));

Route::get('/provider/skipReview', array(
    'as' => 'providerSkipReview',
    'uses' => 'WebProviderController@providerSkipReview'));

Route::get('/provider/profile', array(
    'as' => 'providerProfile',
    'uses' => 'WebProviderController@providerProfile'));

Route::get('/provider/vehicle', array(
    'as' => 'providerVehicle',
    'uses' => 'WebProviderController@providerVehicle'));

Route::get('/provider/request', array(
    'as' => 'providerRequestPing',
    'uses' => 'WebProviderController@providerRequestPing'));

Route::post('user/request', array('as' => 'manualrequest', 'uses' => 'WebProviderController@create_manual_request'));

Route::get('/provider/request/decline/{id}', 'WebProviderController@decline_request');

Route::get('/provider/request/accept/{id}', 'WebProviderController@approve_request');

Route::post('provider/get-nearby', array('as' => 'nearby', 'uses' => 'WebProviderController@get_nearby'));

Route::post('admin/get-nearby', array('as' => 'nearby', 'uses' => 'AdminController@get_nearby'));

Route::any('/provider/availability/toggle', array(
    'as' => 'toggle_availability',
    'uses' => 'WebProviderController@toggle_availability'));

Route::any('install', array('as' => 'install', 'uses' => 'InstallerController@install'))->before('new_installation');

Route::get('/install/complete', 'InstallerController@finish_install');

Route::get('token_braintree', array('as' => 'token_braintree', 'uses' => 'ApplicationController@token_braintree'));

// Charts Routes

Route::get('/warehouse/cockpit', 'GoogleChartsController@index');
Route::get('/warehouse/overall_chart', 'GoogleChartsController@overall_chart');
Route::get('/warehouse/overall_last_shipments', 'GoogleChartsController@overall_last_shipments');
Route::get('/warehouse/city_last_shipments', 'GoogleChartsController@city_last_shipments');
Route::get('/warehouse/per_city', 'GoogleChartsController@per_city');

Route::post('/warehouse/captainsforunplanshipment', 'AutoPlanningController@captainsforunplanshipment');

Route::get('/warehouse/gethubsbycity', 'AutoPlanningController@gethubsbycity');
Route::get('/warehouse/isHubMissionEnabled', 'AutoPlanningController@isHubMissionEnabled');
Route::post('/warehouse/missionsrollback', 'AutoPlanningController@missionsrollback');

Route::post('/warehouse/admincreatemission', 'AutoPlanningController@admincreatemission');
Route::post('/warehouse/missioninfo', 'AutoPlanningController@missioninfo');
Route::get('/warehouse/missionsticker/{packageID}', 'AutoPlanningController@sticker');
Route::post('/warehouse/planmissiontocaptain', 'AutoPlanningController@planmissiontocaptain');
Route::post('/warehouse/unplanmissiontocaptain', 'AutoPlanningController@unplanmissiontocaptain');
Route::post('/warehouse/missionshipmentsrollback', 'AutoPlanningController@missionshipmentsrollback');
Route::get('/warehouse/copymissionwaybill', 'AutoPlanningController@copymissionwaybill');

Route::post('/warehouse/reservecaptain', 'AutoPlanningController@reservecaptain');
Route::post('/warehouse/unreservecaptain', 'AutoPlanningController@unreservecaptain');
Route::post('/warehouse/unplancaptain', 'AutoPlanningController@unplancaptain');
Route::post('/warehouse/sendMissionNotificationToActiveCaptains', 'AutoPlanningController@sendMissionNotificationToActiveCaptains');

// New Website
Route::get('/', 'WebController@newweb');
Route::get('/ar', 'WebController@newweb');

Route::post('/sendemailpost','WebController@sendemailpost');

// System Configuration Routes
// Cities routes
Route::get('/warehouse/system/cities', 'WarehouseController@cities');

// Roles routes
Route::get('/warehouse/system/roles', 'WarehouseController@roles');

//Users Routes

Route::get('/warehouse/system/users', 'WarehouseController@users');

// captain rating routes

Route::post('/tracking/ratecaptain', array('as' => 'RateCaptain', 'uses' => 'TrackingController@rate_captain'));

Route::post('/tracking/ratecustomer', array('as' => 'RateCustomer', 'uses' => 'TrackingController@rate_customer'));

Route::post('/captain/captainrating/', array('as' => 'CaptainRating', 'uses' => 'WalkerController@captain_rating_comments'));

// captain ranking

Route::get('/warehouse/rankcaptain', array('as' => 'RankCaptain', 'uses' => 'WalkerController@rank_captain'));
Route::post('/warehouse/rankcaptainpost', array('as' => 'RateCaptainPost', 'uses' => 'WalkerController@rank_captain_post'));

Route::post('/captain/recentmissions', 'CaptainController@recent_missions');
Route::post('/captain/accomplishedmissions', 'CaptainController@accomplished_missions');

Route::post('/captain/getnotifications', 'CaptainController@getnotifications');
Route::post('/captain/clearAllNotifications', 'CaptainController@clearAllNotifications');

//Roll back shipments api call
Route::post('/captain/statusrollback', 'CaptainController@statusRollback');

Route::get('/warehouse/captaindetailratings', 'WarehouseController@captainDetailRatings');
Route::post('/warehouse/SendNotificationToActiveCaptains', 'WarehouseController@SendNotificationToActiveCaptains');

//Missions API calls
Route::post('/captain/getmissionsforcaptain', 'CaptainController@getmissionsforcaptain');
Route::post('/captain/acceptmission', 'CaptainController@acceptmission');
Route::post('/captain/rejectmission', 'CaptainController@rejectmission');
Route::post('/captain/getmissionshipments', 'CaptainController@getmissionshipments');

//Called to Consignee
Route::post('/captain/customercontacted', 'CaptainController@customercontacted');
Route::post('/captain/logout', 'CaptainController@logout');

Route::get('blackbox/allusers', 'BbChatController@bbAllUsers');

Route::get('/blackbox/user/delete/{id}', array('as' => 'bBDeleteUser', 'uses' => 'BbChatController@delete_user'));

Route::get('/blackbox/alias/delete/{id}', array('as' => 'bBDeleteAlias', 'uses' => 'BbChatController@delete_alias'));

Route::post('/warehouse/SendNotificationToActiveCaptains', 'WarehouseController@SendNotificationToActiveCaptains');

Route::get('/blackbo/user/delete/{id}', array('as' => 'bBDeleteUser', 'uses' => 'BbChatController@delete_user'));


Route::get('/blackbox', array('as' => 'bbChat', 'uses' => 'BbChatController@blackbox'));

Route::get('/blackbox/login', array('as' => 'BlackBoxLogin','uses' => 'BbChatController@bbLogin'));
Route::post('/blackbox/verify', array('as' => 'CompanyBlackBoxVerify','uses' => 'BbChatController@bbVerify'));

Route::get('/blackbox/{waybill}', 'BbChatController@bbpinscreen');
Route::post('/blackbox/pinverify', array('as' => 'CompanyBlackBoxPinVerify','uses' => 'BbChatController@bbPinVerify'));

//shipments details for Blackbox api call
Route::post('/blackbox/shipmentsdetails', array('as' => 'shipmentDetailsBB', 'uses' => 'JollyChicController@shipmentDetailsBb'));
//Saee agent login
Route::post('saeeagent/login','JollyChicController@login');

// delete company shipments with status = 0
Route::post('/company/deleteselected', 'CompanyController@delete_selected');

Route::post('/company/deletesingleselected', 'CompanyController@delete_single_selected');

//////////////
// archive APIs
Route::post('/prod/admin/login', 'ArchiveController@admin_login');
//////////////

Route::post('/blackbox/comingfrom', 'JollyChicController@comingFrom');


Route::get('/mail/branchSuccessRateReport', 'MailController@branchSuccessRateReport');

Route::get('/scheduledTasks/preparedRTO', 'ScheduledTasksController@prepared_rto');

Route::get('/warehouse/sub_statuses', 'WarehouseController@sub_statuses_legends');

//Inventory Stock
Route::get('/warehouse/stockreport', 'StockController@stockreport');
Route::post('/warehouse/stockreportpost', 'StockController@stockreportpost');


Route::post('/archive/paste/shipments/delivered', 'ArchiveController@paste_shipments_delivered');
Route::post('/archive/paste/shipments/created_by_origin', 'ArchiveController@paste_shipments_created_by_origin');
Route::post('/archive/delete/check/shipments/delivered', 'ArchiveController@delete_check_shipments_delivered');
Route::post('/archive/delete/check/shipments/created_by_origin', 'ArchiveController@delete_check_shipments_created_by_origin');

