@if( Auth::user()->role < 2)

    <script>window.location = "/warehouse/403";</script>

    @endif
<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 2)
    $layout = 'warehouse.jclayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captain Ranking</h3>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form id="rank_captain_form" method="post" action="/warehouse/rankcaptainpost">
                            <div class="row">
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">From<span class="required">*</span>
                                    </label>
                 
                                       <div class="col-md-6 col-sm-6 col-xs-12">
										 <input type="date" name="from" id="from" value="" class="form-control">
										 </div>
                                    
                                </div>
                                
                                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                 	<label class="control-label col-md-3 col-sm-3 col-xs-12">To<span class="required">*</span>
                                    </label>
                                 	<div class="col-md-6 col-sm-6 col-xs-12">
                                 	
                                 	<input type="date" name="to" id="to" value="" class="form-control">

                                 </div>
                                 </div>
                                
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Calculate Rank</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <table id="datatable_rankings" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Walker ID</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Points</th>
                                    <th>Rank</th>
                                    <th>Approve</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Walker ID</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Points</th>
                                    <th>Rank</th>
                                    <th>Approve</th>
                                </tr>
                                </tfoot>
                            </table>
                    </div>
                </div>
            </div>

           

        </div>
    </div>
  
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>


    <script>

        <?php


        $mydata_rankings = '';


       foreach ($eachCaptainRankingData as $astep)
        {

            $captain_name = $astep['first_name'] . " " . $astep['last_name'];

            $captainId = $astep['captainId'];
            $phone = $astep['phone'];
            $email = $astep['email'];
            $points = $astep['totalpoints'];
            $rank = $astep['rank'];
            $mydata_rankings .= "[  \"$captainId\" ,\"$captain_name\", \"$phone\" ,\"$email\", \"$points\", \"$rank\", \"<button href='#'>Approve</button>\"], ";

        }


        ?>



        $(document).ready(function () {


                    var table = $("#datatable_rankings").DataTable({



                  


                        "data": [
                            <?php echo $mydata_rankings ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                         buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],

                        responsive: true,
                        'order': [[4, 'desc']]
                    });


                      $('#datatable_rankings tbody').on( 'click','button', function () {

        var data = table.row( $(this).parents('tr') ).data();
        
        var captainId = data[0];
        var points = data[4];
        var rank = data [5];

        $.ajax({
        type: "GET",
        url: "/warehouse/updaterank/",
        data: { 
            captainId: captainId,
            points: points,
            rank: rank
        },
        success: function(result) {
            alert('Rank Updated in Database');
        },
        error: function(result) {
            alert('error something went wrong !');
        }
    });

    } );

                });

    </script>



@stop

