<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Confirm User Ride</title>
        <style type="text/css" media="screen">

            .ExternalClass * {line-height: 100%}

            /* Début style responsive (via media queries) */

            @media only screen and (max-width: 480px) {
                *[id=email-penrose-conteneur] {width: 100% !important;}
                table[class=resp-full-table] {width: 100%!important; clear: both;}
                td[class=resp-full-td] {width: 100%!important; clear: both;}
                img[class="email-penrose-img-header"] {width:100% !important; max-width: 340px !important;}
            }

            /* Fin style responsive */

        </style>

    </head>
    <body style="background-color:#ecf0f1">
        <div align="center" style="background-color:#ecf0f1;">

            <!-- Début en-tête -->

            <table id="email-penrose-conteneur" width="660" align="center" style="padding:20px 0px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" style="text-align:left;">
                                    <a href="{{ web_url() }}" style="text-decoration:none;"><h3 style="font-size: 25px;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: bold;color: #6B6B6B;margin: 0;"><?php /* echo "App Name"; */ echo ucwords(Config::get('app.website_title')); ?></h3></a>
                                </td>
                                <td width="50%" style="text-align:right;">
                                    <table align="right" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                        <h5 style="font-size: 20px;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: bold;color: #6B6B6B;margin: 0;"><?php echo date("d-m-Y"); ?></h5>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- Fin en-tête -->

<table id="email-penrose-conteneur" width="660" align="center" style="border-right:1px solid #e2e8ea; border-bottom:1px solid #e2e8ea; border-left:1px solid #e2e8ea; background-color:#ffffff;" border="0" cellspacing="0" cellpadding="0">

    <!-- Début bloc "mise en avant" -->

    <tr>
        <td style="background-color:#ff9d3d">
            <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="resp-full-td" valign="top" style="padding:20px; text-align:center;">
                        <span style="font-size:25px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#ffffff"><a href="{{ web_url() }}" style="color:#ffffff; outline:none; text-decoration:none;">Confirm your ride</a></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

   <!-- Début article 1 -->

    <tr>
        <td style="border-bottom: 1px solid #e2e8ea">
            <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0" style="padding:20px;">
                <tr>
                    <td width="100%">

                        <table width="100%" align="right" class="resp-full-table" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" class="resp-full-td" valign="top" style="text-align : justify;">

                                    <div style="padding: 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;text-align:left;">Hello  
                                    <strong style="font-weight: bold;">{{ $mail_body['owner_name'] }}</strong>,
                                        <br>
                                        Following Driver is ready to server you.
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="100%">

                        <table style="border-collapse:collapse" width="100%" border="0" cellpadding="0" cellspacing="0" align="right">
                            <tbody>
                            <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">
                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        <strong style="font-weight: bold;"><?php echo 'Driver Name'; /* echo $email_data['password']; */ ?></strong> 
                                    </div>
                                </td>
                                <td colspan="2" style="text-align:justify" valign="top" width="50%">
                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        {{ $mail_body['driver_name'] }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        <b style="font-weight:bold">Contact Number</b> 
                                    </div>
                                    
                                </td>
                                <td colspan="2" style="text-align:justify" valign="top" width="50%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                       {{ $mail_body['driver_contact'] }}
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">

                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        <strong style="font-weight: bold;"><?php echo 'Car Model'; ?></strong> 
                                    </div>
                                </td>
                                 <td colspan="2" style="text-align:justify" valign="top" width="50%">
                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        {{ $mail_body['driver_car_model'] }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">
                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        <strong style="font-weight: bold;"><?php echo 'Licence Plate Number'; /* echo $email_data['password']; */ ?></strong> 
                                    </div>
                                </td>
                                <td colspan="2" style="text-align:justify" valign="top" width="50%">
                                    <div style="padding: 5px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;">
                                        {{ $mail_body['driver_licence'] }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        <b style="font-weight:bold">Journey Date </b> 
                                    </div>
                                    
                                </td>
                                <td colspan="2" style="text-align:justify" valign="top" width="50%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        {{ $mail_body['journey_date'] }}
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:justify" valign="top" width="20%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        <b style="font-weight:bold">Trip Detail</b> 
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align:justify" valign="top" width="50%">
                                    <table style="border-collapse:collapse" width="100%" border="1" cellpadding="0" cellspacing="0" align="right">
                                        <tbody>
                                        <tr>
                                            <td colspan="1" style=" text-align:justify" valign="top" width="20%">

                                                <div style="padding:10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454;text-align:left">
                                                </div>
                                                
                                            </td>
                                            <td colspan="1" style="width:30%;">  
                                              <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     From                                                 </b> 
                                               </div>  
                                            </td>
                                            <td colspan="1" style="width:30%;">  
                                              <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     To                                                 </b> 
                                               </div>  
                                            </td>
                                            <td colspan="1" style="width:20%;">  
                                              <div style=" padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     Pickup Time                                                 </b> 
                                               </div>  
                                            </td>
                                            
                                        </tr>
                                        
                                       <tr>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     Trip One                                                 </b> 
                                               </div>  
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                    {{ $mail_body['trip_address']['trip_one_from_address']; }}                                               </b> 
                                               </div>  
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                {{ $mail_body['trip_address']['trip_one_to_address']; }}                                         </b> 
                                               </div> 
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     <span class="aBn" data-term="goog_78028473" tabindex="0"><span class="aQJ"> 
                                                     {{ $mail_body['trip_address']['trip_one_pickup_time']; }}</span></span>                                                </b> 
                                               </div> 
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     Trip Two                                                 </b> 
                                               </div>  
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                    {{ $mail_body['trip_address']['trip_two_from_address']; }} 
                                                </b> 
                                               </div>  
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold"> 
                                                {{ $mail_body['trip_address']['trip_two_to_address']; }}                                                                                 </b> 
                                               </div> 
                                            </td>
                                            <td>
                                                <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                                <b style="font-weight:bold">
                                                     <span class="aBn" data-term="goog_78028474" tabindex="0"><span class="aQJ">
                                                     {{ $mail_body['trip_address']['trip_two_pickup_time']; }}
                                                     </span></span>                                                </b> 
                                               </div> 
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" colspan="4" style="text-align:justify" valign="top" width="20%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        <b style="font-weight:bold">Cost</b> 
                                    </div>
                                    
                                </td>
                                <td colspan="2" style="text-align:justify" valign="top" width="50%">

                                    <div style="padding:5px 10px;font-size:12px;font-family:'Helvetica Neue',helvetica,arial,sans-serif;font-weight:100;color:#545454">
                                        {{ $mail_body['cost'] }}
                                    </div>
                                    
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="resp-full-td" valign="top" style="text-align : justify;">

                        <div style="padding: 30px 10px 10px 10px;font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100; color:#545454;text-align:center;">
                            For any query or consolation {{ $mail_body['admin_eamil'] }}
                        </div>
                        <!--<div style="text-align:center"><a href="#" style="padding: 10px;font-size:18px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:100;background-color: #ff9d3d;color:#fff;text-align:center;  text-decoration: none;">Login</a></div>


                        <br>-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- Fin article 1 -->
</table>
<!-- Début footer -->


<!-- Fin footer -->

</div>
</body>
</html>