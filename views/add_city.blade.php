@extends('layout')

@section('content')

    <div class="box box-success">
        <br/>
        <br/>
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif
        <br/>


        <form method="post" action="{{ URL::Route('AdminAddCityPost') }}">
            <div class="box-body ">
                <div class="form-group">
                    <label>City</label><input class="form-control" type="text" name="city"
                                              placeholder="Add City">
                </div>
            </div>
            <div class="box-footer">

                <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Add City</button>

            </div>
        </form>
    </div>
    </div>


@stop