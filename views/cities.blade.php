@extends('layout')

@section('content')
    <a id="addinfo" href="{{ URL::Route('AdminAddCity') }}"><input type="button" class="btn btn-info btn-flat btn-block"
                                                                   value="Add City"></a>
    <br>
    <div class="box box-success">
        <div align="left" id="paglink"><?php echo $cities->links(); ?></div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>City ID</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cities as $city) { ?>
            <tr>
                <td>{{$city->id}}</td>
                <td>{{$city->name}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                       href="{{ URL::Route('AdminCityDelete', $city->id) }}">Delete
                                    City</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>


    </div>

    <div align="left" id="paglink"><?php echo $cities->links(); ?></div>
    </div>
@stop