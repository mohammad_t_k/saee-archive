@extends('layout')

@section('content')

    <div class="box box-primary">
        @if(Session::has('error'))
            <div class="box box-error">
                <h4 class="alert alert-error"><?php
                    echo Session::get('error');
                    Session::put('error', NULL);
                    ?>
                </h4></div>
        @endif
        <div class="box-body ">
            <form method="post" action="{{ URL::Route('AdminUserUpdatePassword') }}">
                <input class="form-control" type="hidden" name="id" value="<?= $owner->id ?>">

                <div class="form-group">
                    <label>Password</label><input class="form-control" type="text" name="password"
                                                  placeholder="Reset User Password">
                </div>
                <div class="box-footer">
                    <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Reset User
                        Password
                    </button>
                </div>
            </form>
        </div>
    </div>


@stop