
@extends('layout')

@section('content')

<div class="box box-primary">
    <form method="post" id="main-form" action="{{ URL::Route('AdminUserUpdateCredit') }}"  enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $owner->id ?>">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Current Credit : <?= $owner->credit.'/- SAR' ?></label>
                                        </div>


                                        <div class="form-group">
                                            <label>Credit</label>
                                            <input type="text" class="form-control" name="credit" value="<?= $owner->credit ?>" placeholder="Credit" >

                                        </div>
                                   
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">

                                      <button type="submit" id="edit" class="btn btn-primary btn-flat btn-block">Update Credit</button>
                                    </div>
                                </form>
                            </div>



<?php
if($success == 1) { ?>
<script type="text/javascript">
    alert('Customer Credit Updated Successfully');
</script>
<?php } ?>
<?php
if($success == 2) { ?>
<script type="text/javascript">
    alert('Sorry Something went Wrong');
</script>
<?php } ?>

<script type="text/javascript">
$("#main-form").validate({
  rules: {
      credit: {
        required: true,
        float: true
    }
  }
});
</script>

@stop