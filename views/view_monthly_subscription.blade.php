@extends('layout')

@section('content')

<?php $counter = 1; ?>
<div class="box box-primary">
    <div class="box-header">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" style="margin-top: 15px;" >  
   
          <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Monthly Subscription Details</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @if(isset($walker))
                        <div class="col-md-6 col-sm-12">
                        <h3 class="panel-title"> Captain Info.:  <?php echo $walker->first_name." ".$walker->last_name ?></h3>
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td rowspan="2" colspan="1">
                                    <div class="row">
                                        <div class=" col-md-12 col-lg-12 ">
                                            <img alt="Captain Photo" src="<?= $walker->picture; ?>" class="img-circle img-responsive" style=" height:75px;width:75px;margin: auto;border-radius: 100%; border: 5px solid ">
                                        </div>
                                    </div>
                                </td>
                                <td>Email</td>
                                <td colspan="2">
                                    <?php echo $walker->email?>
                                </td>

                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td colspan="2"><?php echo $walker->phone?></td>
                            </tr>
                            <tr>
                                <td>Car Model</td>
                                <td><?php if($walker->car_model!="0") echo $walker->car_model ?></td>
                                <td>Plate Type</td>
                                <td><?php
                                    if ($walker->plate_type == '1') {
                                        echo "Private";
                                    }else{
                                        echo "Public";
                                    }
                                    ?></td>
                            </tr>
                            <tr>
                                <td>Vehicle Number Plate</td>
                                <td><?php echo ($walker->car_number_plate_letter_1 . " " .  $walker->car_number_plate_letter_2 . " " .$walker->car_number_plate_letter_3 . " " .$walker->car_number_plate_number) ?></td>
                                <td>License Expiry Date</td>
                                <td><?php echo $walker->license_expiry_date ?></td>
                            </tr>

                            </tbody>
                        </table>

                    </div>
                    @endif
                    <div class="col-md-6  col-sm-12">
                        <h3 class="panel-title">Customer Info.: <?php echo $owner->first_name." ".$owner->last_name ?></h3>
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td rowspan="2" colspan="1">
                                    <div class="row">
                                        <div class=" col-md-12 col-lg-12 ">
                                            <img alt="User" src="<?= $owner->picture; ?>" class="img-circle img-responsive" style=" height:75px;width:75px;margin: auto;border-radius: 100%; border: 5px solid ">
                                        </div>
                                    </div>
                                </td>
                                <td>Email</td>
                                <td colspan="2">
                                    <?php echo $owner->email?>
                                </td>

                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td colspan="2"><?php echo $owner->phone?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="panel-title">Subscription Details</h3>
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Trip Type</td>
                                    <td><?php  if($monthly->trip_type==1)echo'One Side Trip' ;else echo 'Round Trip';?></td>
                                    <td>Private/Shared</td>
                                    <td><?php  if($monthly->subscription_type==1)echo'Private' ;else echo 'Shared';?></td>

                                </tr>

                                <tr>
                                    <td>Trip 1 Pick Up Time</td>
                                    <td><?php echo $monthly->trip_one_pickup_time?></td>
                                    <td>Trip 2 Pick Up Time</td>
                                    <td><?php if($monthly->trip_type==2)echo $monthly->trip_two_pickup_time ?></td>
                                </tr>
                                <tr>
                                    <td>Trip 1 Pick Up Address</td>
                                    <td><?php echo $monthly->trip_one_from_address?></td>
                                    <td>Trip 2 Pick Up Address</td>
                                    <td><?php echo $monthly->trip_two_from_address?></td>
                                </tr>
                               <tr>
                                    <td>Trip 1 Drop Address</td>
                                    <td><?php echo $monthly->trip_one_to_address?></td>
                                    <td>Trip 2 Drop Address</td>
                                    <td><?php echo $monthly->trip_two_to_address ?></td>
                                </tr>
                                <tr>
                                    <td>Start Date</td>
                                    <td><?php echo $monthly->starting_date?></td>
                                    <td>Subscription Type</td>
                                    <td><?php  if($monthly->is_full_month==1)echo'Full Month' ;else echo 'Specific Days';?></td>

                                </tr>
                                @if(isset($request))
                                    <tr>
                                        <td>Cost</td>
                                        <td><?php echo $request->total?></td>
                                        <td>Distance</td>
                                        <td><?php echo $request->distance?></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">

                    <div class="row">
                            <div class=" col-md-12 col-lg-12 ">
                                <div id="map_canvas" style="height: 250px;width: 100%;margin: 10;"></div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=homeMarkerMap" async defer></script>
<script type="text/javascript">
function homeMarkerMap() {
    var walker = JSON.parse('<?php echo $walkerjs ?>');
   var mo='<?php echo $monthly?>';
    mo = mo.replace(/\n|\r/g, "");
    var monthly = JSON.parse(mo);
    if(walker.address_longitude!=0&&walker.address_latitude!=0){
        var homeLatLng = {lat: walker.address_latitude, lng: walker.address_longitude};
        var pickUp1 = {lat: parseFloat(monthly.trip_one_from_lat), lng: parseFloat(monthly.trip_one_from_long)};
        var drop1 = {lat: parseFloat(monthly.trip_one_to_lat), lng: parseFloat(monthly.trip_one_to_long)};
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 12,
            center: pickUp1
        });
        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        var marker_home ="https://kasper-cab.com/uploads/markerhome.png"
        var marker_pick ="https://kasper-cab.com/uploads/markerpickup.png"
        var marker_drop ="https://kasper-cab.com/uploads/markerdrop.png"
        var markerCaptainHome = new google.maps.Marker({
            position: homeLatLng,
            map: map,
            icon:marker_home,
            title: 'Captain Home Location'
        });
        var markerPickOne = new google.maps.Marker({
            position: pickUp1,
            map: map,
            icon:marker_pick,
            title: 'Trip One Pick Up'
        });
        var markerDropOne = new google.maps.Marker({
            position: drop1,
            map: map,
            icon:marker_drop,
            title: 'Trip One Drop'
        });
        if(monthly.trip_type==2){
            var pickUp2 = {lat: parseFloat(monthly.trip_two_from_lat), lng: parseFloat(monthly.trip_two_from_long)};
            var drop2 = {lat: parseFloat(monthly.trip_two_to_lat), lng: parseFloat(monthly.trip_two_to_long)};
            var markerPickTwo = new google.maps.Marker({
                position: pickUp2,
                map: map,
                title: 'Trip Two Pick Up'
            });
            var markerDropTwo = new google.maps.Marker({
                position: drop2,
                map: map,
                title: 'Trip Two Drop'
            });
        }
        var legendDiv = document.createElement('DIV');
        var legend = new Legend(legendDiv, map);
        legendDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legendDiv);
    }else{
        document.getElementById('map_canvas').style="display: none;";
    }
}
function Legend(controlDiv, map) {
    // Set CSS styles for the DIV containing the control
    // Setting padding to 5 px will offset the control
    // from the edge of the map
    controlDiv.style.padding = '5px';

    // Set CSS for the control border
    var controlUI = document.createElement('DIV');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.title = 'Legend';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control text
    var controlText = document.createElement('DIV');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';

    // Add the text
    controlText.innerHTML = '<b>Legends</b><br />' +
            '<img src="<?php echo asset_url(); ?>/uploads/markerhome.png" style="height:25px;"/> Captain Home<br />' +
            '<img src="<?php echo asset_url(); ?>/uploads/markerpickup.png" style="height:25px;"/> Pickup<br />' +
            '<img src="<?php echo asset_url(); ?>/uploads/markerdrop.png" style="height:25px;"/> Drop<br />'
    controlUI.appendChild(controlText);
}
</script>

</div>
@stop