@extends('layout')

@section('content')

    <div class="box box-success">
        <br/>
        <br/>
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif
        <br/>


        <form method="post" action="{{ URL::Route('AdminCityPrivilegePost') }}">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>
                    <span id="spanAdminName"></span>
                </h2>

                <div id="citiesListCheckBoxesList">
                    <h4>
                        <span>Select Cities</span>
                    </h4>
                </div>

            </div>

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="form-group">
                    <input type="button" value="Update City For Admin" class="btn btn-xl"
                           onclick="postCitiesEdit()"/> <i class="icon-hand-right"></i>
                </div>
            </div>
        </form>
    </div>
    </div>


    <script type="text/javascript">
        function loadCitiesListCheckBoxes() {
            var cityList = <?php echo $cities?>;
            var adminDetails = <?php echo $admin ?>;
            var adminCitiesSelected = <?php echo $adminCities?>;
            var cityListDiv = document.getElementById('citiesListCheckBoxesList');
            document.getElementById('spanAdminName').innerHtml = adminDetails.username;
            for (var i = 0; i < cityList.length; i++) {
                var cityDivRow = document.createElement('div');
                cityDivRow.className = "row";
                var checkBoxDiv = document.createElement('div');

                checkBoxDiv.className = 'col-lg-4';
                var checked = "";
                for (var j = 0; j < adminCitiesSelected.length; j++) {
                    if (adminCitiesSelected[j] != null)
                        if (adminCitiesSelected[j].city == cityList[i].name) {
                            checked = "checked";
                            break;
                        }
                }
                checkBoxDiv.innerHTML = "<input type='checkbox' id='checkbox" + cityList[i].id + "' " + checked + "/>";
                var cityDiv = document.createElement('div');
                cityDiv.className = 'col-lg-8';
                cityDiv.innerHTML = "<span>" + cityList[i].name + "</span>";
                cityDivRow.appendChild(checkBoxDiv);
                cityDivRow.appendChild(cityDiv);
                cityListDiv.appendChild(cityDivRow);
            }
        }
        function postCitiesEdit() {
            var cityList = <?php echo $cities; ?>;
            var adminDetails =  <?php echo $admin; ?>;
            var result = true;
            var citiesListSelected = [];
            for (var i = 0; i < cityList.length; i++) {
                if ((document.getElementById("checkbox" + cityList[i].id)).checked == true) {
                    citiesListSelected.push(cityList[i].name);
                }
            }
            path = "/admin/admin_city_privilege_post";
            var method = "post";
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);
            var params = {citiesSelected: JSON.stringify(citiesListSelected), admin_id: adminDetails.id};
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }
            document.body.appendChild(form);
            form.submit();
            return result;
        }
        loadCitiesListCheckBoxes();
    </script>
@stop