@extends('merchants.layout')

@section('track')

    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/web/css/custom.css"> <!-- for box animation style -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <div class="">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div style="margin-top: 50px;">
                    <form action="trackShipmentPost" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-5 col-md-offset-2 col-xs-12 custom-input">
                            <input type="text" name="waybill" id="waybill" class="form-control col-xs-12" value="{{ $waybill }}" required="required" />
                            <label>Shipment Tracking Number </label>
                        </div>
                        <div class="col-md-2 col-xs-12 custom-input">
                            <input type="submit" value="Search" class="btn btn-primary form-control" style="height: 40px;" />
                        </div>
                    </form>
                </div>   
            </div>
        </div>
                
        <div class="row" style="margin-top: 30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="text" style="font-size: 40px;">{{ !isset($progress) ? '' : $progress['text'] }}</div>
                <div class="progress col-xs-12">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ !isset($progress) ? '0' : $progress['percent'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ !isset($progress) ? '0' : $progress['percent'] }}%">
                    {{ !isset($progress) ? '0' : $progress['percent'] }}%
                  </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table id="datatable_details" class="table table-stripped table-bordered">
                    <thead style="background-color: #ccccff; color: blue;">
                        <tr>
                            <th style="text-align: center;">Date and Time</th>
                            <th style="text-align: center;">Waybill</th>
                            <th style="text-align: center;">Order Number</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;">Captain Name</th>
                            <th style="text-align: center;">Mobile</th>
                            <th style="text-align: center;">Customer Name</th>
                            <th style="text-align: center;">City</th>
                            <th style="text-align: center;">Remarks</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if(count($shipment) == 0)
                        <tr>
                            <td colspan="9" style="background-color: light-grey; text-align: center;">No Data</td>
                        </tr>
                        @else
                        <tr>
                            <?php 
                            $captain_name = '';
                            if($shipment->confirmed_walker2 > 0) {
                                $captain = Walker::find($shipment->confirmed_walker2);
                                $captain_name = $captain->first_name . ' ' . $captain->last_name;
                            }
                            else if($shipment->planned_walker2 > 0) {
                                $captain = Walker::find($shipment->planned_walker2);
                                $captain_name = $captain->first_name . ' ' . $captain->last_name;
                            }
                            else if($shipment->confirmed_walker1 > 0) {
                                $captain = Walker::find($shipment->confirmed_walker1);
                                $captain_name = $captain->first_name . ' ' . $captain->last_name;
                            }
                            else if($shipment->planned_walker1 > 0) {
                                $captain = Walker::find($shipment->planned_walker1);
                                $captain_name = $captain->first_name . ' ' . $captain->last_name; 
                            }
                             ?>
                            <td style="text-align: center;">{{ $shipment->new_shipment_date }}</td>
                            <td style="text-align: center;">{{ $shipment->waybill }}</td>
                            <td style="text-align: center;">{{ $shipment->order_number }}</td>
                            <td style="text-align: center;">{{ $progress['text'] }}</td>
                            <td style="text-align: center;">{{ $captain_name }}</td>
                            <td style="text-align: center;">{{ $shipment->customer_mobile1 }} - {{ $shipment->customer_mobile2 }}</td>
                            <td style="text-align: center;">{{ $shipment->receiver_name }}</td>
                            <td style="text-align: center;">{{ $shipment->d_city }}</td>
                            <td style="text-align: center;">{{ $shipment->remarks }}</td>
                        @endif
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
<!--     <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script> -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <style>
        /* Start of Floating Label CSS */
        :focus {
            outline: none;
        }
        .custom-input {
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 35px;
            top: 45px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input input {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input input:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input textarea {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input textarea:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input ._emptyoption {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input ._emptyoption:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        /* End of Floating Label CSS */
    </style>
    <script>
        /* Start of Floating Label Jquery */
        $(document).ready(function() { // to activate floating text if there is a default value for text box
            if(<?php echo $waybill != '' ? 'true' : 'false' ?>) {
                $('.custom-input input').parent().addClass('has-data');
            }
        });
        $(function() {
            'use strict';
            $('.custom-input input').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input textarea').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                    console.log('here');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input ._emptyoption').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');

                    console.log('hello world');
                    $('.custom-input ._district').on('focusout', function(){
                        if($(this).val() != '') {
                            $(this).parent().addClass('has-data');
                        } else {
                            $(this).parent().removeClass('has-data');
                        }
                    });

                } else {
                    $(this).parent().removeClass('has-data');
                }

            });
        });
        /* End of Floating Label Jquery */
    </script>


    <style>
        .bs-wizard {margin-top: 40px;}
        /*Form Wizard*/
        .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
        .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        /*END Form Wizard*/
    </style>


@stop