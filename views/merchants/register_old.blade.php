<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>:. Saee :.</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        #map_canvas {
            width: 100%;
        }

        #map_canvas .centerMarker {
            position: absolute;
            /*url of the marker*/
            background: url(../web/images/marker.png) no-repeat;
            /*center the marker*/
            top: 50%;
            left: 50%;
            z-index: 1;
            /*fix offset when needed*/
            margin-left: -10px;
            margin-top: -34px;
            /*size of the image*/
            height: 34px;
            width: 20px;
            cursor: pointer;
        }

        /* Start of Floating Label CSS */
        :focus {
            outline: none;
        }
        .custom-input {
            margin: 30px auto;
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 35px;
            top: 45px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input input {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input input:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input textarea {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input textarea:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input ._emptyoption {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input ._emptyoption:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        /* End of Floating Label CSS */

        .col-md-6 {
            height: 100px;
        }

    </style>
    <script>
        /* Start of Floating Label Jquery */
        $(function() {
            'use strict';
            $('.custom-input input').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input textarea').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                    console.log('here');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input ._emptyoption').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');

                    console.log('hello world');
                    $('.custom-input ._district').on('focusout', function(){
                        if($(this).val() != '') {
                            $(this).parent().addClass('has-data');
                        } else {
                            $(this).parent().removeClass('has-data');
                        }
                    });

                } else {
                    $(this).parent().removeClass('has-data');
                }

            });
        });
        /* End of Floating Label Jquery */
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>

<!-- header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="../images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling-->
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        {{--<li><a href="#">عربى</a></li>--}}
                        <li class="border"><a href="login">SIGN IN</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- header end-->

<br/><br/>
<form action="/merchant/registerMerchantPost" method="post" enctype="multipart/form-data"> <!-- enctype="multipart/form-data" for sending images -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="clearfix"></div>
                <label class="control-label col-xs-12">
                    <h3><span class="fa fa-user"></span> &nbsp; <b>Merchant Information</b> </h3>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="name" id="name" class="form-control" required="required" />
                            <label>English Name <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="name_ar" id="name_ar" class="form-control" required="required" />
                            <label>Arabic Name <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="email" name="email" id="email" class="form-control" required="required" />
                            <label>Email <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="username" id="username" class="form-control" required="required" />
                            <label>Username <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="password" name="password" id="password" class="form-control" required="required" />
                            <label>Password <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="number" name="mobile" id="mobile" class="form-control" required="required" />
                            <label>Mobile Number <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <select name="city" id="city" class="form-control _emptyoption _district" required="required" onchange="getDistricts()">
                                <option id="city_empty_option"></option>
                            </select>
                            <label>City <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <select name="district" id="district" class="form-control _emptyoption" required="required">
                                <option  id="district_empty_option" disabled>Choose City First</option>
                            </select>
                            <label>District <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <textarea name="address" id="address" class="form-control" required="required" style="height: 60px;"></textarea>
                            <label>Full Address <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="number" name="telegram" id="telegram" class="form-control" />
                            <label>Telegram Number</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="number" name="whatsapp" id="whatsapp" class="form-control" />
                            <label>Whatsapp Number</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="instagram" id="instagram" class="form-control" />
                            <label>Instagram Link</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="twitter" id="twitter" class="form-control" />
                            <label>Twitter Link</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="googleplus" id="googleplus" class="form-control" />
                            <label>Google+ Link</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="facebook" id="facebook" class="form-control" />
                            <label>Facebook Link</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="text" name="snapchat" id="snapchat" class="form-control" />
                            <label>Snapchat Link</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="custom-input">
                            <input type="number" name="idnumber" id="idnumber" class="form-control" required="required" />
                            <label>ID Number <span class="required">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12" style="margin-top: 4%;">
                <div class="col-xs-12">
                    <div class="col-md-12 col-xs-12">
                        <input id="uploadimage" name="idphoto" type="file" class="form-control" required="required">
                    </div>
                </div>
            </div>
        </div>

        <br/><hr/><br/>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="clearfix"></div>
                <label class="control-label col-xs-12">
                    <h3><span class="fa fa-map-marker"></span> &nbsp; <b>Update Location</b> </h3>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <br/>
                <div id="map_canvas" style="margin: 10px;"></div>
                <input id="address_latitude" name="address_latitude" hidden />
                <input id="address_longitude" name="address_longitude" hidden />
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
                <script>
                    function initMap() {
                        try {
                            var address_latitude = 21.5912954629946;
                            var address_longitude = 39.175945799537544;
                            var trackingnum = '';
                            var mapOptions = {
                                zoom: 15,
                                center: new google.maps.LatLng(address_latitude, address_longitude),
                            };
                            document.getElementById('map_canvas').style = 'height:300px';
                            var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                            infoWindow = new google.maps.InfoWindow;
                            google.maps.event.addListener(map, 'center_changed', function () {
                                address_latitude= document.getElementById('address_latitude').value = map.getCenter().lat();
                                address_longitude= document.getElementById('address_longitude').value = map.getCenter().lng();
                            });
                            $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                            //do something onclick
                                .click(function () {
                                    var that = $(this);
                                    if (!that.data('win')) {
                                        that.data('win', new google.maps.InfoWindow({
                                            content: 'this is the center'
                                        }));
                                        that.data('win').bindTo('position', map, 'center');
                                    }
                                    that.data('win').open(map);
                                });
                        }
                        catch (e) {
                            alert(e);
                        }
                    }
                </script>
                <script>google.maps.event.addDomListener(window, 'load', initMap);</script>
            </div>
        </div>

        <br/>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <input type="submit" class="btn btn-primary" />
            </div>
        </div>

        <div class="row">
            <!--footer start-->
            <div class="container" style="text-align: center;">
                <div class="footertext1">© All Rights Reserved Kasper Business</div>
            </div>
            <!--footer end-->
        </div>
    </div>
</form>



    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
      rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <script>
        $(document).ready(function(){
            console.log('here');
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text',
            }).done(function (response) {
                console.log(response);
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for (var i = 0; i < cities.length; i += 1) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }
                }
            });
        });
        function getDistricts() {
            $("#district").empty();
            $('.custom-input ._emptyoption').parent().addClass('has-data');
            var city = document.getElementById('city').value;
            console.log('city', city);
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: city}
            }).done(function (response) {
                // $("#district").addClass('._emptyoption:focus + label');
                $('#district').on('focusout', function(){
                    if($(this).val() != '') {
                        $(this).parent().addClass('has-data');
                    } else {
                        $(this).parent().removeClass('has-data');
                    }
                });
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    for (var i = 0; i < districts.length; i += 1) {
                        if(districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        option.appendChild(document.createTextNode(districts[i].district + ' - ' + districts[i].district_ar));
                        select.appendChild(option);
                    }
                }
            });
        }
    </script>

</body>
</html>




