@extends('merchants.layout')

@section('track')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <style>
    table tr th {
        text-align: center;
    }
    </style>

    <div class="container">

        <div class="col-md-12 col-lg-12 col-xs-12" style="margin-top: 2%; margin-bottom: 2%;">
            <div class="clearfix"></div>
            <label class="control-label col-xs-12">
                <h3><span class="fa fa-cube"></span> &nbsp; <b>Drop Off Information </b> </h3>
            </label>
        </div>
    
        <table class="table table-bordered text text-center">
            <tr>
                <th>Name</th>
                <td>{{ $shipment->receiver_name }}</td>
                <th>Email</th>
                <td>{{ $shipment->receiver_email }}</td>
            </tr>
            <tr>
                <th>First Mobile</th>
                <td>{{ $shipment->receiver_phone }}</td>
                <th>Second Mobile</th>
                <td>{{ $shipment->receiver_phone2 }}</td>
            </tr>
            <tr>
                <th>City</th>
                <td>{{ $shipment->d_city }}</td>
                <th>District</th>
                <td>{{ $shipment->d_district }}</td>
            </tr>
            <tr>
                <th>Shipment Description</th>
                <td>{{ $shipment->d_description }}</td>
                <th>Street Address</th>
                <td>{{ $shipment->d_address2 }}</td>
            </tr>
            <tr>
                <th>Full Address</th>
                <td>{{ $shipment->d_address }}</td>
                <th>Quantity</th>
                <td>{{ $shipment->d_quantity }}</td>
            </tr>
            <tr>
                <th>Order Number</th>
                <td>{{ $shipment->order_number }}</td>
                <th>Weight</th>
                <td>{{ $shipment->d_weight }}</td>
            </tr>
            <tr>
                <td colspan='4'>
                    <div id="map_canvas" style="margin: 10px;"></div>
                    <input id="address_latitude" name="address_latitude" hidden />
                    <input id="address_longitude" name="address_longitude" hidden />
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
                    <script>
                        function initMap() {
                            try {
                                var address_latitude = {{ $shipment->D_latitude }};
                                var address_longitude = {{ $shipment->D_longitude }};
                                var trackingnum = '';
                                var mapOptions = {
                                    zoom: 15,
                                    center: new google.maps.LatLng(address_latitude, address_longitude),
                                };
                                document.getElementById('map_canvas').style = 'height:300px';
                                var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                                var location = {lat: address_latitude,lng: address_longitude};
                                var marker = new google.maps.Marker({
                                    position: location,
                                    map: map,
                                    title: 'Drop Off Location'
                                });
                                infoWindow = new google.maps.InfoWindow;
                                $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                                //do something onclick
                                    .click(function () {
                                        var that = $(this);
                                        if (!that.data('win')) {
                                            that.data('win', new google.maps.InfoWindow({
                                                content: 'this is the center'
                                            }));
                                            that.data('win').bindTo('position', map, 'center');
                                        }
                                        that.data('win').open(map);
                                    });
                            }
                            catch (e) {
                                alert(e);
                            }
                        }
                    </script>
                    <script>google.maps.event.addDomListener(window, 'load', initMap);</script>
                </td>
            </tr>
        </table>
    </div>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
<!--     <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script> -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

@stop