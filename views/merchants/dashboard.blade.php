@extends('merchants.layout')

@section('dashboard')

    <style>
    
    </style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="height: 100px;">
                <form action="/merchant/dashboard" method="get">
                    <div class="col-xs-1 form-group">
                        <label class="col-xs-2">City</label>
                    </div>
                    <div class="col-xs-4">
                        <select name="city" id="city" class="form-control">
                            @foreach($adminCities as $adcity)
                                <option value="{{ $adcity->city }}" {{ isset($city) && $city == $adcity->city ? 'selected' : ''}}>{{ $adcity->city }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <input type="submit" value="Filter" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Total Packages
                <h3><?php echo $allitems ?></h3>
        
            </div>

            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> To Be Picked-up
                <h3><?php echo $tobepickedup ?></h3>

            </div>

            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Reserved To Pick-up
                <h3><?php echo $reservedtopickup ?></h3>

            </div>

            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Picked-up
                <h3><?php echo $pickedup ?></h3>

            </div>
        </div>

        <div class="row">

            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Returned to Supplier
                <h3><?php echo $returnedtocitems ?></h3>

            </div>

            <div class="col-md-3 package">

                <i class="fa fa-truck" aria-hidden="true"></i> In Route
                <h3><?php echo $onwayitems ?></h3>

            </div>

            <div class="col-md-3 package">
        
                <i class="fa fa-home" aria-hidden="true"></i> In Warehouse
                <h3><?php echo $arriveditems; ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-car" aria-hidden="true"></i> With Captains
                <h3><?php echo $withcaptains ?></h3>
        
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 package">
        
                <i class="fa fa-truck" aria-hidden="true"></i> Delivered
                <h3><?php echo $delivreditems ?></h3>

            </div>
        </div>
    </div>

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <script>
        $(document).ready(function() {
        })
    </script>

@stop
