@extends('merchants.layout')

@section('track')

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


      <div class="">
            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet"> <!-- for tabs -->
            <div class="row">
                  <div class="tab_container">
                        
                        <input id="tab1" type="radio" name="tabs" {{ isset($tabFilter) && $tabFilter == '1' ? 'checked' : '' }}>
                        <label for="tab1" class='label2'><i class="fa fa-car"></i><span>Export Single Shipment</span></label>
                                    
                        <!-- <input id="tab2" type="radio" name="tabs" {{ isset($tabFilter) && $tabFilter == '2' ? 'checked' : '' }}>
                        <label for="tab2" class='label2'><i class="fa fa-car"></i><span>Export Multiple Shipments</span></label> -->

                        <input id="tab3" type="radio" name="tabs" {{ !isset($tabFilter) || $tabFilter == '3' ? 'checked' : '' }}>
                        <label for="tab3" class='label2'><i class="fa fa-car"></i><span>Filter Shipments</span></label>

                        <section id="content1" class="tab-content">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                    <div class="x_title">
                                        <h3>Export Single Shipment</h3>
                                          <ul class="nav navbar-right panel_toolbox">
                                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                          </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form action="/merchant/exportSingle" method="get">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="1">

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-offset-2 col-md-1 col-sm-3 col-xs-12" style="margin-top: 1.5%;">Waybill <span class="required">*</span></label>
                                                
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <input type="text" id="waybill" name="waybill" required="required" class="form-control">
                                                    </div>
                                                    <div class="theCount">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 10px;">
                                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                                <p>
                                                                    <input type="submit" id="submitForm" value="Search" class="btn btn-success" />
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>

                                        <table id="datatable_single" class="table table-striped table-bordered " style="text-align: center; magin: auto;">
                                            <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Order No</th>
                                                <th>City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Details</th>

                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Order No</th>
                                                <th>City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                              </div>
                           </div>
                        </section>

                        <!-- <section id="content2" class="tab-content">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                          <div class="x_title">
                                            <h3>Export Shipments</h3>
                                          </div>
                                    </div>
                              </div>
                        </section> -->

                        <section id="content3" class="tab-content">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <form action="/merchant/exportFilter" method="get">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value="3">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">City
                                                    </label>

                                                    <div class="col-md-8 col-sm-6  col-xs-12 input-css">
                                                        <select name="city" id="city" class="form-control">
                                                            @foreach($adminCities as $adcity)
                                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Status 
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="all">All</option>
                                                            <option value="-3"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                                                To Be Picked Up
                                                            </option>
                                                            <option value="-1"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                                                Reserved To Pick Up
                                                            </option>
                                                            <option value="-2"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                                                Picked Up
                                                            </option>
                                                            <option value="3/new"<?php if (isset($status) && $status == '3' && isset($inwarehouse) && $inwarehouse == 3) echo 'selected';?> >
                                                                In Warehouse/New
                                                            </option>
                                                            <option value="3/R"<?php if (isset($status) && $status == '3' && isset($inwarehouse) && $inwarehouse == 1) echo 'selected';?> >
                                                                In Warehouse/Return
                                                            </option>
                                                            <option value="3/Res"<?php if (isset($status) && $status == '3' && isset($inwarehouse) && $inwarehouse == 2) echo 'selected';?> >
                                                                In Warehouse/Rescheduled
                                                            </option>
                                                            <option value="3"<?php if (isset($status) && $status == '3' && isset($inwarehouse) && $inwarehouse == 0) echo 'selected';?> >
                                                                In Warehouse
                                                            </option>
                                                            <option value="4"<?php if (isset($status) && $status == '4') echo 'selected';?> >
                                                                With Captains
                                                            </option>
                                                            <option value="5"<?php if (isset($status) && $status == '5') echo 'selected';?> >
                                                                Delivered
                                                            </option>
                                                            <option value="6"<?php if (isset($status) && $status == '6') echo 'selected';?> >
                                                                UnDelivered
                                                            </option>
                                                            <option value="7"<?php if (isset($status) && $status == '7') echo 'selected';?> >
                                                                Returned to Supplier
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Scheduled Shipment
                                                        Date
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                        <select name="shipment_date" id="shipment_date"
                                                                class="form-control">
                                                            <option value="all">All</option>
                                                            <?php

                                                            $shipmentdates = DeliveryRequest::distinct()->where('company_id', '=', $merchant->id)->orderBy('scheduled_shipment_date', 'desc')->get(['scheduled_shipment_date']);

                                                            $total_num_of_shipments = count($shipmentdates);

                                                            foreach ($shipmentdates as $ashipmentdates) {
                                                                if ($ashipmentdates->scheduled_shipment_date != null) {
                                                                    $selected = '';
                                                                    $date = $ashipmentdates->scheduled_shipment_date;
                                                                    if (isset($shipmentDate) && $shipmentDate == $date)
                                                                        $selected = 'selected';
                                                                    echo "<option value='$date' $selected>$date</option>";
                                                                }
                                                            }

                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">from
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="start_date" name="start_date" value="{{ isset($from) ? $from : '' }}"
                                                            class="date-picker form-control col-md-7 col-xs-12"
                                                            type="date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">To
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                                            value="{{ isset($to) ? $to : '' }}"
                                                            type="date">
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 btn-to form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                                        <p>
                                                            <button id="submitForm" class="btn btn-success">Filter
                                                            </button>
                                                        </p>
                                                    </div>
                                                </div>

                                            </form>

                                            <table id="datatable_filtered" class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Order No</th>
                                                    <th>City</th>
                                                    <th>Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Status</th>
                                                    <th>Details</th>

                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Order No</th>
                                                    <th>City</th>
                                                    <th>Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Status</th>
                                                    <th>Details</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                              </div>
                        </section>
                        
                  </div>
            </div>
      </div>
    

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>
        function opensticker(waybill){
            window.open('/merchant/sticker/'+waybill, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }
    </script>

    <script>
        <?php 
        $filter_data = '';
        foreach($filtershipments as $order) {
            if($order->num_faild_delivery_attempts == 0)
                $order->undelivered_reason = '';
            if($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status = 'Warehouse/Res';
            else if($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status = 'Warehouse/R';
            else if($order->status == 3)
                $status = 'Warehouse/New';
            else
                $status = $statusDef[$order->status];
            $sticker = "<a onclick=opensticker(\'$order->waybill\')>$order->waybill</a>";
            $details = "<a href='/merchant/shipmentDetails/$order->waybill'>Details</a>";
            $filter_data .= "[ \"$sticker\",\"$order->order_number\",\"$order->d_city\",\"$order->num_faild_delivery_attempts\",\"$order->undelivered_reason\",\"$order->receiver_name\", \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"$status\", \"$details\" ], ";
        }
        ?>
        $("#datatable_filtered").DataTable({

            "data": [
                <?php echo $filter_data ?>
            ],
            "autoWidth": false,

            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[0, 'desc']]
        });
    </script>


    <script>
        <?php 
        $single_data = '';
        foreach($singleshipment as $order) {
            if($order->num_faild_delivery_attempts == 0)
                $order->undelivered_reason = '';
            if($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status = 'Warehouse/Res';
            else if($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status = 'Warehouse/R';
            else if($order->status == 3)
                $status = 'Warehouse/New';
            else
                $status = $statusDef[$order->status];
            $sticker = "<a onclick=opensticker(\'$order->waybill\')>$order->waybill</a>";
            $details = "<a href='/merchant/shipmentDetails/$order->waybill'>Details</a>";
            $single_data .= "[ \"$sticker\",\"$order->order_number\",\"$order->d_city\",\"$order->num_faild_delivery_attempts\",\"$order->undelivered_reason\",\"$order->receiver_name\", \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"$status\", \"$details\" ], ";
        }
        ?>
        $("#datatable_single").DataTable({

            "data": [
                <?php echo $single_data ?>
            ],
            "autoWidth": false,

            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[0, 'desc']]
        });
    </script>

@stop