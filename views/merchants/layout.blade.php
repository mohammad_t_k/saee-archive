<!DOCTYPE html>
<html lang="en">
<head>
    <title>:. KASPER BUSINESS :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/companiesdashboard/css/font-awesome.css">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/bootstrap.min.css">
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/badge.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">


</head>


<body>

<!--header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/merchant/dashboard"><img src="<?php echo asset_url(); ?>/companiesdashboard/images/logo-light.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="column margintop20 rs-show">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#" style="background-color: #337ab7;">Welcome,<br> <?php echo $merchant->company_name; ?></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span>Edit My Info</a></li>
                            <li><a href="/merchant/generateShipment"><span class="glyphicon glyphicon-chevron-right"></span>Generate Shipment</a></li>
                            <li><a href="/merchant/editShipment"><span class="glyphicon glyphicon-chevron-right"></span>Edit Shipment Info</a></li>
                            <li><a href="/merchant/editPickup"><span class="glyphicon glyphicon-chevron-right"></span>Edit Pickup Info</a></li>
                            <!-- <li><a href="/merchant/orders"><span class="glyphicon glyphicon-chevron-right"></span>Orders</a></li> -->
                            <li><a href="/merchant/exportShipments"><span class="glyphicon glyphicon-chevron-right"></span>Export Shipments</a></li>
                            <li><a href="/merchant/trackShipment"><span class="glyphicon glyphicon-chevron-right"></span>Track Shipment</a></li>
                            <!-- <li><a href="bulkTrackShipment"><span class="glyphicon glyphicon-chevron-right"></span>Multiple Shipment Tracking</a></li> -->
                            <li><a href="/merchant/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<!--header end-->


<div class="col-md-12 body-con">
    <div class="col-md-2 column margintop20 fix rs-none">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="/merchant/dashboard" style="background-color: #337ab7;">Welcome,<br> <?php echo $merchant->company_name; ?></a></li>
            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span>Edit My Info</a></li>
            <li><a href="/merchant/generateShipment"><span class="glyphicon glyphicon-chevron-right"></span>Generate Shipment</a></li>
            <li><a href="/merchant/editShipment"><span class="glyphicon glyphicon-chevron-right"></span>Edit Shipment Info</a></li>
            <li><a href="/merchant/editPickup"><span class="glyphicon glyphicon-chevron-right"></span>Edit Pickup Info</a></li>
            <!-- <li><a href="/merchant/orders"><span class="glyphicon glyphicon-chevron-right"></span>Orders</a></li> -->
            <li><a href="/merchant/exportShipments"><span class="glyphicon glyphicon-chevron-right"></span>Export Shipments</a></li>
            <li><a href="/merchant/trackShipment"><span class="glyphicon glyphicon-chevron-right"></span>Single Shipment Tracking</a></li>
            <!-- <li><a href="bulkTrackShipment"><span class="glyphicon glyphicon-chevron-right"></span>Multiple Shipment Tracking</a></li> -->
            <li><a href="/merchant/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a></li>
        </ul>
    </div>


    <div class="col-md-10">

        @yield('dashboard')
        @yield('shipments')
        @yield('packageDetails')
        @yield('companydetails')
        @yield('customerservice')
        @yield('content')
        @yield('insert')
        @yield('track')
        @yield('editcompany')


    </div>
</div>

<div class="container">
    <div class="col-md-12">
        @yield('histogram')
    </div>
</div>

<!--footer start-->
<div class="container" style="text-align: center;">
    <div class="footertext1">© All Rights Reserved Saee</div>
</div>
<!--footer end-->

@yield('dashboard-script')

<!-- Bootstrap -->
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
<!-- ECharts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/custom.js"></script>
<script src="<?php echo asset_url(); ?>/companiesdashboard/js/scroll.js"></script>

<!--footer end-->


</body>
</html>
