@extends('merchants.layout')

@section('track')
    
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/web/css/custom.css"> <!-- for box animation style -->

    <style>
        /* Start of Floating Label CSS */
        :focus {
            outline: none;
        }
        .custom-input {
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 35px;
            top: 45px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input input {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input input:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input textarea {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input textarea:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input ._emptyoption {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input ._emptyoption:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        /* End of Floating Label CSS */
    </style>
    <script>
        /* Start of Floating Label Jquery */
        $(function() {
            'use strict';
            $('.custom-input input').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input textarea').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                    console.log('here');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input ._emptyoption').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');

                    console.log('hello world');
                    $('.custom-input ._district').on('focusout', function(){
                        if($(this).val() != '') {
                            $(this).parent().addClass('has-data');
                        } else {
                            $(this).parent().removeClass('has-data');
                        }
                    });

                } else {
                    $(this).parent().removeClass('has-data');
                }

            });
        });
        /* End of Floating Label Jquery */
    </script>

    <div class=container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div style="margin-top: 50px;">
                    <form aciton="trachShipmentPost" method="POST">
                        <div class="col-md-5 col-md-offset-2 col-xs-12 custom-input">
                            <input type="text" name="waybill" id="waybill" class="form-control col-xs-12" required="required" />
                            <label>Shipment Tracking Number </label>




                        </div>
                        <div class="col-md-2 col-xs-12 custom-input">
                            <button type="submit" value="Submit" class="btn btn-primary form-control" style="height: 40px;">Search</button>
                        </div>
                    </form>
                </div>   

            </div>
        </div>

        <div class="row" style="margin-top: ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="progress col-md-8 col-md-offset-1 col-xs-12">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    40%
                  </div>
                </div>
            </div>
        </div>        
    </div>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


@stop