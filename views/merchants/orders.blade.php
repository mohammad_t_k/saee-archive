@extends('merchants.layout')

@section('track')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <table class="table table-striped" style="width: 70%;">
                    <thead>
                        <tr>
                            <th>Waybill</th>
                            <th>Order Number</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($shipments as $shipment)
                        <tr class="multi-cols">
                            <td style="width: 5%;">{{ $shipment->jollychic }}</td>
                            <td style="width: 80%;">{{ $shipment->order_number }}</td>
                            <td style="width: 5%;">
                                <div title="Edit">
                                    <a  href="/company/subcompanies/edit/{{ $shipment->id }}"> <!-- edit -->
                                        <label style="float: right;"><i class="fa fa-pencil-square" style="font-size: 20px;"></i></label>
                                    </a>
                                </div>
                                <div style="clear: both;"></div>
                            </td>
                            <td style="width: 5%;">
                                <div title="Delete">
                                    <label style="float: right;"><i onclick="remove({{ $shipment->id }})" class="fa fa-times" style="font-size: 20px; color: red;"></i></label> <!-- remove -->
                                </div>
                                <div style="clear: both;"></div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="/merchant/generateShipment"><label class="btn btn-primary">Add Shipment</label></a>
                <style>
                    i:hover {
                        cursor: pointer;
                    }
                </style>
                <script>
                    function remove(subcompany_id){
                        if(confirm('Are you sure you want to remove this sub company?') == true) {
                            console.log('in');
                            $.ajax({
                                type: 'POST',
                                url: '/company/subcompanies/remove',
                                dataType: 'json',
                                data: {id: subcompany_id}
                            }).done(function (response) {
                                console.log(response);
                                return;
                                if(response.success)
                                    location.reload();
                                else
                                    alert('Error!');
                            });
                        }
                    }
                </script>
                     
            </div>
        </div>
    </div>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


@stop