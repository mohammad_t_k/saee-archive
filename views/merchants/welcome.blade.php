<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>:. Saee :.</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        #map_canvas {
            width: 100%;
        }

        #map_canvas .centerMarker {
            position: absolute;
            /*url of the marker*/
            background: url(../web/images/marker.png) no-repeat;
            /*center the marker*/
            top: 50%;
            left: 50%;
            z-index: 1;
            /*fix offset when needed*/
            margin-left: -10px;
            margin-top: -34px;
            /*size of the image*/
            height: 34px;
            width: 20px;
            cursor: pointer;
        }

        /* Start of Floating Label CSS */
        :focus {
            outline: none;
        }
        .custom-input {
            margin: 30px auto;
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 35px;
            top: 45px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input input {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input input:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input textarea {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input textarea:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }

        .custom-input ._emptyoption {
            height: 40px;
            padding: 5px 10px;
            background: transparent;
            border: 1px solid #dfdfdf;
        }
        .custom-input ._emptyoption:focus + label {
            left: 20px;
            top: 4px;
            font-size: 15px;
            color: #337ab7;
            transition: .3s ease-in-out;
        }
        /* End of Floating Label CSS */

        .col-md-6 {
            height: 100px;
        }

    </style>
    <script>
        /* Start of Floating Label Jquery */
        $(function() {
            'use strict';
            $('.custom-input input').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input textarea').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                    console.log('here');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });

            $('.custom-input ._emptyoption').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');

                    console.log('hello world');
                    $('.custom-input ._district').on('focusout', function(){
                        if($(this).val() != '') {
                            $(this).parent().addClass('has-data');
                        } else {
                            $(this).parent().removeClass('has-data');
                        }
                    });

                } else {
                    $(this).parent().removeClass('has-data');
                }

            });
        });
        /* End of Floating Label Jquery */
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>

<!-- header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="../images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling-->
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        {{--<li><a href="#">عربى</a></li>--}}
                        <li class="border"><a href="login">SIGN IN</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- header end-->

<br/><br/>

        <div class="container">
            <div class="row">
                <!-- <h3>Welcome $user->name }}, we will contact you soon.</h3> -->
                <h3>Thank you for your registration, we will contact you soon.</h3>
            </div>
        </div>
        <br/>
        <br/>
        <div class="container">
            <div class="row">
                <h3><a href="/merchant/login">Go back</a></h3>
            </div>
        </div>
                


    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
      rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

</body>
</html>




