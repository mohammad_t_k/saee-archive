@extends('layout')

@section('content')

    @if (Session::has('msg'))
        <h4 class="alert alert-info">
            {{{ Session::get('msg') }}}
            {{{Session::put('msg',NULL)}}}
        </h4>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Report Bug</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" method="post" action="{{ URL::Route('AdminReportBugPost') }}"  enctype="multipart/form-data">
            <input type="hidden" name="id" value="0>">
            <div class="box-body">
                <div class="form-group col-md-6 col-sm-6">
                    <label>Bug Label</label>
                    <input type="text" class="form-control" name="label" value="" placeholder="Bug Label" >
                </div>
                <div class="form-group col-md-6 col-sm-6">
                    <label>Bug URL</label>
                    <input class="form-control" type="text" name="url" value="" placeholder="Bug URL">
                </div>
                <div class="form-group col-md-6 col-sm-6">
                    <label>Bug Details</label>
                    <input class="form-control" type="text" name="details" value="" placeholder="Bug Complete Details">
                </div>
                <div class="form-group col-md-6 col-sm-6">
                    <label>Message</label>
                    <input class="form-control" type="text" name="message" value="" placeholder="Message for Admin">
                </div>
                <div class="form-group">
                    <label>Screen Shot</label>
                    <input class="form-control" type="file" name="screenshot" >
                    <p class="help-block">Please Upload Screen Shot of Bugs in jpg, png format.</p>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat btn-block">Report Bug</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $("#form").validate({
            rules: {
                label: "required",
                url: "required",
                details: "required",
            }
        });

    </script>
@stop