@extends('layout')

@section('content')

<div class="box box-primary">
    <div class="box box-error">
        <br/>
        @if (Session::has('error'))
        <h4 class="alert alert-error">
            {{{ Session::get('error') }}}
        </h4>
        @endif
    </div>
    <div class="box-body ">
        <form method="post" action="{{ URL::Route('AdminProviderUpdatePassword') }}">
            <input class="form-control" type="hidden" name="id" value="<?= $walker->id ?>">
            <div class="form-group">
                <label>Password</label><input class="form-control" type="text" name="password"
                                           placeholder="Reset Captain Password">
            </div>
    </div>
    <div class="box-footer">
        <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Reset Captain Password</button>
    </div>
    </form>
</div>
</div>


@stop