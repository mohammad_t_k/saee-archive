
@extends('layout')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- form start -->
        <div>
            <h4><?if(isset($role)){echo $role;} ?></h4>
        </div>
        <form method="post" id="basic" action="{{ URL::Route('AdminAddRole') }}">
            <div class="form-group col-md-12 col-sm-12">
                <label>Role Title</label>
                <input type="text" class="form-control" name="title" placeholder="Type Role Name" id="title" value="" required>
            </div>
            <div class="box-footer">
                <button type="submit" id="add" class="btn btn-primary btn-flat btn-block">Add Role</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $("#message").validate({
            rules: {
                name: "required",
            }
        });
    </script>
@stop