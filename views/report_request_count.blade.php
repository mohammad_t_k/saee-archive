@extends('layout')

@section('content')


        <!-- filter start -->

<div class="box box-danger">
    <div class="box-header">
        <h3 class="box-title">All Captain Performance Count Report</h3>
    </div>
    <div class="box-body">

        <form role="form" method="get" action="{{ URL::Route('AdminReportRequestCount') }}">

            <div class="col-md-6 col-sm-6 col-lg-6">
                <input type="text" class="form-control" style="overflow:hidden;" id="start-date" name="start_date"
                       value="{{ Input::get('start_date') }}" placeholder="Start Date">
                <br>
            </div>

            <div class="col-md-6 col-sm-6 col-lg-6">
                <input type="text" class="form-control" style="overflow:hidden;" id="end-date" name="end_date"
                       placeholder="End Date" value="{{ Input::get('end_date') }}">
                <br>
            </div>

            <div class="col-md-6 col-sm-6 col-lg-6">

                <select name="report_type" class="form-control">
                    <option value="1" <?php echo Input::get('report_type') == 1 ? "selected" : "" ?>>Paid Trips Count
                    </option>
                    v
                    <option value="2" <?php echo Input::get('report_type') == 2 ? "selected" : "" ?>>Accepted Trips
                        Count
                    </option>
                    <option value="3" <?php echo Input::get('report_type') == 3 ? "selected" : "" ?>>Completed Trips
                        Count
                    </option>
                    <option value="4" <?php echo Input::get('report_type') == 4 ? "selected" : "" ?>>Total Requests
                        Count
                    </option>
                    <option value="5" <?php echo Input::get('report_type') == 5 ? "selected" : "" ?>>Accepted Requests
                        Count
                    </option>
                    <option value="6" <?php echo Input::get('report_type') == 6 ? "selected" : "" ?>>Missed/Rejected
                        Requests Count
                    </option>
                </select>
                <br>
            </div>

            <div class="col-md-6 col-sm-6 col-lg-6">
                <button type="submit" name="submit" class="btn btn-primary">Report</button>
            </div>

        </form>

    </div>
</div>

<!-- filter end-->


<div class="box box-info tbl-box">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Serial No.</th>
            <th>Captain Id</th>
            <th>Captain Name</th>
            <th>Count</th>
        </tr>

        <?php $i=1;
        foreach ($walkers as $walk) { ?>

        <tr>
            <td><?php echo $i++; ?></td>
            <td><?= $walk->id ?></td>

            <td><?php echo $walk->first_name . " " . $walk->last_name; ?> </td>

            <td><?php echo $walk->counting; ?></td>
        </tr>
        <?php } ?>

        </tbody>
    </table>
    <!--</form>-->
</div>
<script>
    $(function () {
        $("#start-date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#end-date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end-date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#start-date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myModal").modal('show');
    });
</script>

@stop