@extends('layout')

@section('content')

<?php $counter = 1; ?>
<div class="box box-primary">
    <div class="box-header">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" style="margin-top: 15px;" >  
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $walker->first_name." ".$walker->last_name ?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">
                    <div class="row">
                        <div class=" col-md-12 col-lg-12 ">  
                            <img alt="Captain Photo" src="<?= $walker->picture; ?>" class="img-circle img-responsive" style="margin: auto;border-radius: 100%; border: 5px solid #d9edf7;"> 
                        </div>
                    </div>                    
                </div>
                
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                        <tr>
                            <td>WASL Captain Number</td>
                            <td><?php if($walker->waslReferenceNumber!=0) echo $walker->waslReferenceNumber?></td>
                            <td>WASL Vehicle Number</td>
                            <td><?php if($walker->vehicleReferenceNumber!="0") echo $walker->vehicleReferenceNumber?></td>
                        </tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $walker->email?></td>
                        <td>Phone</td>
                        <td><?php echo $walker->phone?></td>
                      </tr>
                      <tr>
                         <td>Date of Birth</td>
                        <td><?php echo $walker->date_of_birth?></td>
                        <td>Government Id Number</td>
                        <td><?php if($walker->government_id_number!=0) echo $walker->government_id_number?></td>
                      </tr>
                      <tr>
                        <td>License Expiry Date</td>
                        <td><?php echo $walker->license_expiry_date ?></td>
                        <td>Vehicle Sequence Number</td>
                        <td><?php if($walker->vehicle_sequence_number!=0) echo $walker->vehicle_sequence_number ?></td>
                      </tr>
                      <tr>
                        <td>Vehicle Registeration Expiry</td>
                        <td><?php echo $walker->car_registration_expiry ?></td>
                        <td>Vehicle Number Plate</td>
                        <td><?php echo ($walker->car_number_plate_letter_1 . " " .  $walker->car_number_plate_letter_2 . " " .$walker->car_number_plate_letter_3 . " " .$walker->car_number_plate_number) ?></td>
                      </tr>
                      <tr>
                      <td>Car Model</td>
                      <td><?php if($walker->car_model!="0") echo $walker->car_model ?></td>
                      <td>Plate Type</td>
                      <td><?php
                        if ($walker->plate_type == '1') {
                            echo "Private";
                        }else{
                            echo "Public";
                        }
                        ?></td>
                      </tr>                      
                      <tr>
                        <td>Is Currently Providing</td>
                        <td><?php
                            $walk = DB::table('walk')
                                    ->select('id')
                                    ->where('walk.is_started', 1)
                                    ->where('walk.is_completed', 0)
                                    ->where('walker_id', $walker->id);
                            $count = $walk->count();
                            if ($count > 0) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?></td>
                        <td>Is Provider Active</td>
                        <td><?php
                            $walk = DB::table('walker')
                                    ->select('id')
                                    ->where('walker.is_active', 1)
                                    ->where('walker.id', $walker->id);
                            $count = $walk->count();
                            if ($count > 0) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?></td>
                      </tr>
                      <tr>
                          <td colspan="2">Service Type</td>
                          <td colspan="2">
                          @foreach($type as $types)
                                    <?php
                                    $ar = array();
                                    foreach ($ps as $pss) {
                                        $ser = ProviderType::where('id', $pss->type)->first();
                                        if ($ser)
                                            $ar[] = $ser->name;
                                    }
                                    $servname = $types->name;
                                     if (!empty($ar)) {
                                        if (in_array($servname, $ar))
                                            echo $types->name;
                                    }
                                    ?>
                            @endforeach</td>
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  <!--<a href="#" class="btn btn-primary">Approve</a>
                  <a href="#" class="btn btn-primary">Decline</a>-->
                </div>
                <div class="row">
                        <div class=" col-md-12 col-lg-12 ">
                            <div id="map_canvas" style="height: 250px;width: 100%;margin: 10;"></div>
                        </div>
                    </div>
              </div>
            </div>
                <!-- <div class="panel-footer">
                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
            
          </div>-->
        </div>
    </div><!-- /.box-header -->
</div>

<!--

<?php if ($success == 1) { ?>
    <script type="text/javascript">
        alert('Walker Profile Updated Successfully');
    </script>
<?php } ?>
<?php if ($success == 2) { ?>
    <script type="text/javascript">
        alert('Sorry Something went Wrong');
    </script>
<?php } ?>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=homeMarkerMap" async defer></script>
<script type="text/javascript">
function homeMarkerMap() {
    var walker = JSON.parse('<?php echo $walker?>');
    var  address_latitude=walker.address_latitude;
    var  address_longitude=walker.address_longitude;
    if(address_longitude!=0&&address_latitude!=0){
        var homeLatLng = {lat: address_latitude, lng: address_longitude};
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 12,
            center: homeLatLng
        });
        var marker = new google.maps.Marker({
            position: homeLatLng,
            map: map,
            title: 'Captain Home Location'
        });
    }else{
        document.getElementById('map_canvas').style="display: none;";
    }
}
  /*  $("#main-form").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            country: "required",
            email: {
                required: true,
                email: true
            },
            state: "required",
            address: "required",
            bio: "required",
            zipcode: {
                required: true,
                digits: true,
            },
            phone: {
                required: true,
                digits: true,
            }


        }
    });*/
</script>


@stop