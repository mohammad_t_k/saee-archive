@extends('layout')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $title ?></h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form method="post" id="main-form" action="{{ URL::Route('AdminRequestsCorrectInvoiceUpdate') }}"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $request->id ?>">

        <div class="box-body">
            <div class="form-group">
                <label>Bill of On Demand</label>
                <input type="text" class="form-control" name="bill" value="<?= $request->bill ?>" placeholder="bill" >
            </div>

            <div class="form-group">
                <label>Net Total Payable by Customer</label>
                <input class="form-control" type="text" name="total" value="<?= $request->total ?>" placeholder="Total">
            </div>

            <div class="form-group">
                <label>Total Distance Covered</label>
                <input class="form-control" type="distance" name="distance" value="<?= $request->distance ?>" placeholder="Distance">
            </div>

            <div class="form-group">
                <label>Total Time of Trip</label>
                <input class="form-control" type="text" name="time" value="<?= $request->time ?>" placeholder="Time">
            </div>

            <div class="form-group">
                <label>Promo Payment</label>
                <input class="form-control" type="text" name="promo_payment" value="<?= $request->promo_payment ?>" placeholder="Promo payment">
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">

            <button type="submit" class="btn btn-primary btn-flat btn-block">Update Invoice</button>
        </div>
    </form>
</div>
@stop