@extends('layout')

@section('content')


        <!-- filter start -->

<div class="box box-danger">
    <div class="box-header">
        <h3 class="box-title">Captain Daily Online Hours Report</h3>
    </div>
    <div class="box-body">

        <form role="form" method="get" action="{{ URL::Route('AdminReportOnlineHours') }}">

            <div class="col-md-6 col-sm-6 col-lg-6">
                <input type="text" class="form-control" style="overflow:hidden;" id="start-date" name="start_date"
                       value="{{ Input::get('start_date') }}" placeholder="Start Date">
                <br>
            </div>

            <div class="col-md-6 col-sm-6 col-lg-6">
                <input type="text" class="form-control" style="overflow:hidden;" id="end-date" name="end_date"
                       placeholder="End Date" value="{{ Input::get('end_date') }}">
                <br>
            </div>

            <div class="box-body row">

                <div class="col-md-6 col-sm-12">
                    <select class="form-control" id="sortdrop" name="type">
                        <option value="provid" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provid') {
                            echo 'selected="selected"';
                        }
                        ?> id="provid"> Captain ID</option>
                        <option value="pvname" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvname') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvname">Captain Name</option>
                        <option value="pvemail" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvemail') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvemail">Captain Email</option>
                        <option value="pvphone" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvphone') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvemail">Captain Phone</option>
                    </select>
                    <br>
                </div>
                <div class="col-md-6 col-sm-12">
                    <input class="form-control" type="text" name="valu" value="<?php
                    if (Session::has('valu')) {
                        echo Session::get('valu');
                    }
                    ?>" id="insearch" placeholder="keyword"/>
                    <br>
                </div>

            </div>

            <div class="col-md-6 col-sm-6 col-lg-6">

                <select name="walker_id" style="overflow:hidden;" class="form-control">
                    <option value="0">Select Captain</option>
                    <?php foreach ($walkers as $walker) { ?>
                    <option value="<?= $walker->id ?>" <?php echo Input::get('walker_id') == $walker->id ? "selected" : "" ?>><?= $walker->first_name; ?> <?= $walker->last_name ?></option>
                    <?php } ?>
                </select>
                <br>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6">
                <button type="submit" name="submit" class="btn btn-primary">View Online Hours Report</button>
            </div>

        </form>

    </div>
</div>

<!-- filter end-->


<div class="box box-info tbl-box">
    <div class="box-header">
        <h3 class="box-title">Daily Online Hours Details Captain : </h3>
    </div>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Serial No.</th>
            <th>Date</th>
            <th>Online Hours</th>
        </tr>

        <?php $i=1;
        foreach ($days as $day) { ?>

        <tr>
            <td><?php echo $i++; ?></td>
            <td><?= $day->day ?></td>
            <td><?php echo round($day->total_online_hours,2); ?> </td>
        </tr>
        <?php } ?>

        </tbody>
    </table>
    <!--</form>-->
</div>
<script>
    $(function () {
        $("#start-date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#end-date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end-date").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                $("#start-date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myModal").modal('show');
    });
</script>

@stop