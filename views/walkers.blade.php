@extends('layout')

@section('content')

<!--<div class="row">
    <div class="col-md-12 col-sm-12">
        <a id="addpro" href="{{ URL::Route('AdminProviderAdd') }}"><button class="btn btn-flat btn-block btn-info" type="button">Add Provider</button></a>
        <br/>
    </div>
</div>-->

<div class="row">

    @if(isset($countCaptainPending)&&isset($countCaptainApproved))
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>
                    <?= $countCaptainPending +$countCaptainApproved?>
                </h3>
                <p>
                    Total Captains
                </p>
            </div>

        </div>
    </div><!-- ./col -->
    @endif
    @if(isset($countCaptainApproved))
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    <?= $countCaptainApproved?>
                </h3>
                <p>
                    Approved Captains
                </p>
            </div>

        </div>
    </div><!-- ./col -->
        @endif
        @if(isset($countCaptainPending))
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    <?= $countCaptainPending ?>
                </h3>
                <p>
                    Pending Captains
                </p>
            </div>

        </div>
    </div><!-- ./col -->
        @endif

</div>
<div class="col-md-6 col-sm-12">
    <div class="box box-danger">
        <form method="get" action="{{ URL::Route('/admin/sortpv') }}">
            <div class="box-header">
                <h3 class="box-title">Sort</h3>
            </div>
            <div class="box-body row">
                <div class="col-md-6 col-sm-12">
                    <select class="form-control" id="sortdrop" name="type">
                        <option value="provid" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provid') {
                            echo 'selected="selected"';
                        }
                        ?> id="provid">Captain ID</option>
                        <option value="pvname" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvname') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvname">Captain Name</option>
                        <option value="pvemail" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvemail') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvemail">Captain Email</option>

                         <option value="provonof" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provonof') {
                            echo 'selected="selected"';
                        }
                        ?> id="provonof">Online/Offline Captains</option>

                        <option value="provact" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provact') {
                            echo 'selected="selected"';
                        }
                        ?> id="provact">Email Activation/Deativation</option>

                        <option value="provcity" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provcity') {
                            echo 'selected="selected"';
                        }
                        ?> id="provcity">City</option>
                    </select>
                    <br>
                </div>
                <div class="col-md-6 col-sm-12">
                    <select class="form-control" id="sortdroporder" name="valu">
                        <option value="asc" <?php
                        if (isset($_GET['valu']) && $_GET['valu'] == 'asc') {
                            echo 'selected="selected"';
                        }
                        ?> selected id="asc">Ascending</option>
                        <option value="desc" <?php
                        if (isset($_GET['valu']) && $_GET['valu'] == 'desc') {
                            echo 'selected="selected"';
                        }
                        ?> id="desc">Descending</option>
                    </select>
                    <br>
                </div>
            </div>
            <div class="box-footer">    
                <button type="submit" id="btnsort" class="btn btn-flat btn-block btn-success">Sort</button>
            </div>
        </form>
    </div>
</div>

<div class="col-md-6 col-sm-12">

    <div class="box box-danger">

        <form method="get" action="{{ URL::Route('/admin/searchpv') }}">
            <div class="box-header">
                <h3 class="box-title">Filter</h3>
            </div>
            <div class="box-body row">

                <div class="col-md-6 col-sm-12">
                    <select class="form-control" id="sortdrop" name="type">
                        <option value="provid" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provid') {
                            echo 'selected="selected"';
                        }
                        ?> id="provid"> Captain ID</option>
                        <option value="pvname" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvname') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvname">Captain Name</option>
                        <option value="pvemail" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvemail') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvemail">Captain Email</option>
                        <option value="pvphone" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'pvphone') {
                            echo 'selected="selected"';
                        }
                        ?> id="pvemail">Captain Phone</option>

                        <option value="provonof" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provonof') {
                            echo 'selected="selected"';
                        }
                        ?> id="provonof">Online/Offline Captains</option>

                        <option value="provact" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provact') {
                            echo 'selected="selected"';
                        }
                        ?> id="provact">Email Activation/Deativation</option>

                        <option value="provcity" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'provcity') {
                            echo 'selected="selected"';
                        }
                        ?> id="provcity">City</option>
                    </select>
                    <br>
                </div>
                <div class="col-md-6 col-sm-12">
                    <input class="form-control" type="text" name="valu" value="<?php
                    if (Session::has('valu')) {
                        echo Session::get('valu');
                    }
                    ?>" id="insearch" placeholder="keyword"/>
                    <br>
                </div>

            </div>

            <div class="box-footer">

                <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Search</button>


            </div>
        </form>

    </div>
</div>

<div class="col-md-12 col-sm-12">
    <?php if (Session::get('che')) { ?>
        <a id="providers" href="{{ URL::Route('AdminProviders') }}"><button class="col-md-6 col-sm-12 btn btn-warning" type="button">All {{ trans('customize.Provider');}}s</button></a><br/>
        <a id="currently" href="{{ URL::Route('AdminProviderBlocked') }}"><button class="col-md-6 col-sm-12 btn btn-warning"  type="button">Blocked</button></a><br/>
        <?php } else if (Session::get('bec')) { ?>
        <a id="providers" href="{{ URL::Route('AdminProviders') }}"><button class="col-md-6 col-sm-12 btn btn-warning" type="button">All {{ trans('customize.Provider');}}s</button></a><br/>
        <a id="currently" href="{{ URL::Route('AdminProviderCurrent') }}"><button class="col-md-6 col-sm-12 btn btn-warning"  type="button">Currently Providing</button></a><br/>
    <?php } else { ?>
        <a id="currently" href="{{ URL::Route('AdminProviderCurrent') }}"><button class="col-md-6 col-sm-12 btn btn-warning"  type="button">Currently Providing</button></a><br/>
        <a id="currently" href="{{ URL::Route('AdminProviderBlocked') }}"><button class="col-md-6 col-sm-12 btn btn-warning"  type="button">Blocked</button></a><br/>
        <?php } ?>
    <br><br>
</div>

@if(Session::has('error'))
<div class="col-md-12 col-sm-12">
    <div class="alert alert-danger">
        <b>{{ Session::get('error') }}</b> 
    </div>
</div>
@endif
@if(Session::has('success'))
<div class="col-md-12 col-sm-12">
    <div class="alert alert-success">
        <b>{{ Session::get('success') }}</b> 
    </div>
</div>
@endif

<div class="box box-info tbl-box">
    <div class="pagecounts">Showing <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getFrom(); ?> to <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getTo(); ?> of <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getTotal(); ?> entries </div>
    <div align="left" id="paglink"><?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>
    <table class="table table-bordered">
        <tbody><tr>
                <th>ID</th>
                <th>Name</th>
                <th>WASL Captain Ref. Number</th>
                <th>WASL Vehicle Ref. Number</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Photo</th>
                <th>Online/Offline</th>
                <th>Email Activation</th>
                <th>City</th>
                <?php if (!Session::get('bec')) { ?>
                <!--<th>Total Online Hours</th>
                <th>Total Requests</th>
                <th>Total OnDemand Trips</th>
                <th>Total Monthly Contracts</th>
                <th>Acceptance Rate</th> -->
                <th>Status</th>

            <?php } ?>
                <th>Actions</th>
            </tr>

            <?php foreach ($walkers as $walker) { ?>
                <tr>
                    <td><?= $walker->id ?></td>
                    <td><?php echo $walker->first_name . " " . $walker->last_name; ?> </td>
                    <td><?php if ($walker->waslReferenceNumber==0) echo "Not Registered With WASL"; else echo $walker->waslReferenceNumber;?> </td>
                    <td><?php if ($walker->vehicleReferenceNumber==""||$walker->vehicleReferenceNumber=="0") echo "Not Registered With WASL"; else echo $walker->vehicleReferenceNumber; ?> </td>
                    <td><?= $walker->email ?></td>
                    <td><?= $walker->phone ?></td>
                    <td><a href="<?php echo $walker->picture; ?> target="_blank" onclick="window.open('<?php echo $walker->picture; ?>', 'popup', 'height=500px, width=400px'); return false;"">View Photo</a></td>
                    <td><?php if ($walker->isJolly == 1) {
                                echo "<span class='badge bg-green'>Online</span>";
                            } else {
                                echo "<span class='badge bg-red'>Offline</span>";
                            } ?></td>

                            <td><?php if ($walker->is_active == 1) {
                                echo "<span class='badge bg-green'>Active</span>";
                            } else {
                                echo "<span class='badge bg-red'>De Active</span>";
                            } ?></td>

                            <td><?php echo $walker->city; ?></td>
                   <!--<td>
                        <?php
                        if ($walker->bio) {
                            echo $walker->bio;
                        } else {
                            echo "<span class='badge bg-red'>" . Config::get('app.blank_fiend_val') . "</span>";
                        }
                        ?>
                    </td>-->
                    <?php if (!Session::get('bec')) { /* ?>

                    <td><?= $walker->total_online_hours ?></td>
                    <td><?= $walker->total_requests ?></td>
                    <td><?= $walker->totalOnDemandRequests ?></td>
                    <td><?= $walker->totalMonthlyRequests ?></td> -->
                   <!-- <td><?php
                        if ($walker->total_requests != 0) {
                            echo round(($walker->accepted_requests / $walker->total_requests) * 100, 2);
                        } else {
                            echo 0;
                        }
                        ?> %</td> */
                   ?>
                    <td><?php
                            if ($walker->is_approved == 1) {
                                echo "<span class='badge bg-green'>Approved</span>";
                            } else {
                                echo "<span class='badge bg-red'>Pending</span>";
                            }
                            ?>
                        </td>

                    <?php } ?>


                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1" name="action" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ URL::Route('AdminProviderView', $walker->id) }}">View Details</a></li>
                                <li role="presentation"><a role="menuitem" id="resetpassword" tabindex="-1" href="{{ URL::Route('AdminProviderResetPassword', $walker->id) }}">Reset Password</a></li>
                                <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ URL::Route('AdminProviderEdit', $walker->id) }}">Edit Details</a></li>-->
                                <?php if ($walker->merchant_id == NULL && (Config::get('app.generic_keywords.Currency') == '$' || Config::get('app.default_payment') != 'stripe')) {
                                    ?>
                                    <li role="presentation"><a id="addbank" role="menuitem" tabindex="-1" href="{{ URL::Route('AdminProviderBanking', $walker->id) }}">Add Banking Details</a></li>
                                <?php } ?>

                                <?php if ($walker->is_approved == 0) { ?>
                                <li role="presentation"><a role="menuitem" id="approve" tabindex="-1" href="{{ URL::Route('AdminProviderApprove', $walker->id) }}">Approve</a></li>

                                <li role="presentation"><a role="menuitem" id="online" tabindex="-1" href="{{ URL::Route('AdminProviderOnline', $walker->id) }}">Make Online/Offline</a></li>

                                <li role="presentation"><a role="menuitem" id="online" tabindex="-1" href="{{ URL::Route('AdminProviderActive', $walker->id) }}">Active / Deactive Email</a></li>

                                <li role="presentation"><a role="menuitem" id="approve" tabindex="-1" href="{{ URL::Route('AdminProviderCancel', $walker->id) }}">Cancel</a></li>
                                <?php } else if($walker->is_approved == 1){ ?>
                                    <li role="presentation"><a role="menuitem" id="history" tabindex="-1" href="{{ URL::Route('AdminProviderHistory', $walker->id) }}">View History</a></li>
                                     <li role="presentation"><a role="menuitem" id="online" tabindex="-1" href="{{ URL::Route('AdminProviderOnline', $walker->id) }}">Make Online/Offline</a></li>
                                      <li role="presentation"><a role="menuitem" id="online" tabindex="-1" href="{{ URL::Route('AdminProviderActive', $walker->id) }}">Active / Deactive Email</a></li>
                                    <li role="presentation"><a role="menuitem" id="decline" tabindex="-1" href="{{ URL::Route('AdminProviderDecline', $walker->id) }}">Decline</a></li>


                                <?php } else{?>
                                <li role="presentation"><a role="menuitem" id="decline" tabindex="-1" href="{{ URL::Route('AdminProviderPending', $walker->id) }}">Move to Pending</a></li>


                                <?php }?>
                                <?php
                                /* $settng = Settings::where('key', 'allow_calendar')->first();
                                  if ($settng->value == 1) { */
                                ?>
                                <!--<li role="presentation"><a role="menuitem" id="avail" tabindex="-1" href="{{ URL::Route('AdminProviderAvailability', $walker->id) }}">View Calendar</a></li>-->
                                <?php /* } */ ?>
                                <?php
                                $walker_doc = WalkerDocument::where('walker_id', $walker->id)->first();
                                if ($walker_doc != NULL) {
                                    ?>
                                    <li role="presentation"><a id="view_walker_doc" role="menuitem" tabindex="-1" href="{{ URL::Route('AdminViewProviderDoc', $walker->id) }}">View Documents</a></li>
                                <?php } else { ?>
                                    <li role="presentation"><a id="view_walker_doc" role="menuitem" tabindex="-1" href="#"><span class='badge bg-red'>No Documents</span></a></li>
                                <?php } ?>
                                <!--<li role="presentation"><a role="menuitem" id="history" tabindex="-1" href="{{ web_url().'/admin/provider/documents/'.$walker->id }}">View Documents</a></li>-->
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody></table>
    <div align="left" id="paglink"><?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>

    <div class="pagecounts">Showing <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getFrom(); ?> to <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getTo(); ?> of <?php echo $walkers->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->getTotal(); ?> entries </div>
</div>



@stop