<?php
//        echo "this is the admin page, the admin uses this page to control and edit the orders";

//echo sizeof($orders);
//echo $orders;
//echo sizeof($walkers);
//echo sizeof($statuses);
//echo $walkers;
//echo $statuses;
//echo sizeof($availablecaptains);
//echo $availablecaptains;


// foreach ($walkers as $walker){
//     echo $walker;
// }


?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>


    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    {{--<title>KasperCab</title>--}}


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <!--<link rel="stylesheet" href="/kc/app/views/web/DeliveryApp/assets-2/css/bootstrap.min.css" type="text/css">-->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  



   <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css"/>
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles2.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/stylesarticle.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">






</head>

<head>
<title>JollyChic Orders Panel</title>
</head>

<body>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <h3 style="color: orange">JollyChic Orders Panel</h3>
        </div>
    </div>

    <main class="mdl-layout__content mdl-color--grey-100">
        <div style="padding: 0px 10px;" class="mdl-grid demo-content">

            <div id="contractform" style="height:1800px; width: 1800px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                <br>
                <br>

                
               <table id='header' class='display' cellspacing='0' width='100%'>  <thead>
                <tr>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="allitem"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                All Orders:   <?php echo $allitems ?>
                            </button>
                        </div>
                    </td>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="allitem"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" disabled>
                            Waiting in Riyadh:   <?php echo $penddingitems ?>
                            </button>
                        </div>
                    </td>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="delivereditems"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                On the way to OneSar:   <?php echo $onwayitems ?>
                            </button>
                        </div>
                    </td>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="delivereditems"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                Arrived at OneSar:   <?php echo $arriveditems ?>
                            </button>
                        </div>
                    </td>
                </tr>
               
                <tr> 
                    <td>
                        <div align="center">
                                <button style="color: darkgoldenrod" id="undelivereditems"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                Out for delivery:   <?php echo $outfordeliveryitems ?>
                            </button>
                        </div>
                    </td>
                
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="delivereditems"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                Delivered Orders:   <?php echo $delivreditems ?>
                            </button>
                        </div>
                    </td>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="undelivereditems"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                Un-Delivered Orders:   <?php echo $undelivreditems ?>
                            </button>
                        </div>
                    </td>
                    <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="allitem"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                 returned to jollychic:   <?php echo $returnedtojcitems ?>
                            </button>
                        </div>
                    </td>
                </tr>
               
                <tr>
                
                <td>
                <div align="center">
                <button style="color: darkgoldenrod" id="cashcollected"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                        type="submit" disabled>
                    Total Cash Collected:   <?php echo number_format($cashcollected, 2) ?>
                </button>
                </div>
                </td>
                <td>
                <div align="center">
                <button style="color: darkgoldenrod" id="cashtobecollected"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                        type="submit" disabled>
                    Total Cash To be Collected:   <?php echo number_format($cashtobecollected, 2) ?>
                </button>
                </div>
                </td>
                
                 <td>
                        <div align="center">
                            <button style="color: darkgoldenrod" id="allitem"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                                type="submit" disabled>
                                 success rate:   <?php echo number_format($successrate, 2)  ?> %
                            </button>
                        </div>
                    </td>
                </tr>
              
               </thead>
            </table>
                
               <br>
                <br> 

                
                

                <!-- Echos to table structure-->
                <?php

                //array_multisort($data, SORT_ASC);
                echo " <table id='example' class='display' cellspacing='0' width='100%'>  <thead>
            <tr>

                <th>ID</th> <!-- 0-->
               
                <th>Jollychic ID</th>  <!-- 1-->
                <th>Date</th>  <!-- 2 -->
                <th>Customer Name</th>  <!-- 3-->
                <th>Customer Mobile</th> <!-- 4-->
                <th>Customer Mobile2</th> <!-- 5-->
                
                <th>Address</th>  <!-- 6-->
                <th>District</th>  <!-- 7-->
                <th>Cash</th> <!--   8-->
                <th>Captain</th> <!--  9-->
                <th>Status</th>  <!-- 10 -->
                <!-- 
                <th>Select</th> 
                <th>SMS Status</th> 
                -->

              </tr>
                </thead>
                ";

                $i = 0;
                $j = 1;
                foreach ($orders as $order) {
                    $customer_name= str_replace(' ',PHP_EOL,$order->receiver_name );   //$customer_name = substr($order->receiver_name , 0, 10);
                    
                    echo "<tr>";
                    echo "<td>" . $order->id  . "</td>";
                   
                    echo "<td>" . $order->jollychic . "</td>";
                    echo "<td>" . $order->created_at . "</td>";
                    echo "<td>" . $customer_name. "</td>";
                    echo "<td>" . $order->receiver_phone . "</td>";
                    echo "<td>" . $order->receiver_phone2 . "</td>";
                    echo "<td>" . $order->d_address. "</td>";
                    echo "<td>" . $order->d_district. "</td>";
                    echo "<td>" . $order->cash_on_delivery . "</td>";
                    echo "<td>" . $walkers[$i] . "</td>";
                    echo "<td>" . $statuses[$i] . "</td>";
                    
                   // echo "<td>" . '<input type="checkbox" name="orders" value="' . $order->id . '" ><br>' . "</td>";
                   
                   
                    /*if ($order->sms_status == -1){

                        echo "<td>" . "SMS not sent". "</td>";

                    }else if($order->sms_status == 0){

                        echo "<td>" . "SMS Sent". "</td>";

                    }else if ($order->sms_status == 1){
                        echo "<td>" . "Customer Information Retrieved". "</td>";
                    }*/

                    echo "</tr>";
                     
                    $i++;
                    $j++;
                }
                echo "</table>";
                ?>
                <div>


                    <br> <br>
                    
                
                    <button style="color: blue"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(1)">
                        Picked from JollyChic Warehouse
                    </button>
                    &nbsp; &nbsp;  
                      <button style="color: blue"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(2)">
                        Arrived at One Sar Service Station
                    </button>
                    
                    &nbsp; &nbsp;  
                    
                     <button style="color: blue"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(3)">
                        Arrived at Customer City
                    </button>
                    <br> <br>
                    <br> <br>
                    <button style="color: blue"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(4)">
                        Attemped Delivery/Scheduled
                    </button>
                 &nbsp; &nbsp; &nbsp; &nbsp;
                    
                    <button style="color: blue"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(5)">
                        Delivered/Paid
                    </button>
&nbsp; &nbsp;  
                  
                    <button style="color: orange"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(6)">
                        refused or stop delivering
                    </button>
                    <br> <br>
                    <br> <br>
                    
                       <button style="color: red"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(7)">
                        Returned to Jollychic
                    </button>

&nbsp; &nbsp; 
                    <button style="color: yellowgreen"  
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="cashCollected(8)">
                        Cash collected
                    </button>



                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    <button style="color: darkred" id="sendtocaptain"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="assignToSelectedCaptain()">
                        Assign orders to selected captains
                    </button>



                    &nbsp; &nbsp;
                    <!-- Ugly but it works -->

                    <?php

                    echo '<select id="captains">';
                    echo '<option value="default">Captains</option>';

                    foreach ($availablecaptains as $ac) {

                        echo '<option value="' . $ac->id . '">' . $ac->first_name . ' ' . $ac->last_name . '     ' . $ac->phone . '</option>';

                    }
                    echo '</select>';

                    ?>

                    <br>
                    <br>
                    <br>
                    <br>

                </div>
    </main>
</div>


<script>

    //FUCK THIS FUNCTION
function cashCollected(status) 
{

    var data = table.row('.selected').data();
    // if captain is pending then
    if(data[10] != "delivered/paid")
    {
        alert("please assign captain before updating the item status, and send sms to customer ");
         return;
    }
    else if (data[10] == "delivered/paid"){
    
    $.ajax({
        type: 'POST',
        url: 'updatestatus',
        dataType: 'text',
        data: {id: data[0], status: status},
    }).done(function (response) {
        alert(response + " Status Updated");
        console.log(response);
    }); // Ajax Call
    
    
    }  
}   
      
    
 


    function updateStatus(status) {

        var data = table.row('.selected').data();
        console.log("ordeID " + data[0]);
        table.row('.selected').data(data);


        //if captain is pending then
        // if(data[12] == "pending"){
            // alert("please assign captain before updating the item status");
            // return;
        // }
        //otherwise:



         if(status == 1){
            
             data[10] = "picked from jollychic warehouse";
            table.row('.selected').data(data);
            
        }else if(status == 2){
            
             data[10] = "arrived to Jeddah";
            table.row('.selected').data(data);
            
        }else if(status ==3){
            
             data[10] = "arrived at customer city";
            table.row('.selected').data(data);
            
        }else if(status ==4){
            
             data[10] = "shipments attempts delivery";
            table.row('.selected').data(data);
            
            //sendSMS($order->id);
            
        }
        else if(status ==5)
        {
            
            data[10] = "delivered/paid";
            table.row('.selected').data(data);
        
            console.log(data);
           /* if(data[8] == "shipments attempts delivery")
            {
                 if(data[7] == "pending")
                 {
                       alert("please assign captain before updating the item status");
                     return;
                 }
                 else 
                 {
                  data[8] = "delivered/paid";
                  table.row('.selected').data(data);
                 }
                
            }
            else 
            {
                 alert("please send sms to customer before updating the status ");
                return;
            }*/
            
            
        }else if(status ==6){
            
             data[10] = "undelivered";
            table.row('.selected').data(data);
            
        }else if(status ==7){
            
             data[10] = "returned";
            table.row('.selected').data(data);
        }
                
        $.ajax({
            type: 'POST',
            url: 'updatestatus',
            dataType: 'text',
            data: {id: data[0], status: status},
        }).done(function (response) {
           // alert(response + " Status Updated");
            console.log(response);

        }); // Ajax Call

    }

    function showItemsByDistrict() {

        console.log("show items by district");

    }

    function assignToSelectedCaptain() {


        /*
         1. Get value of selected checkboxes.
         2. Get selected captain from the drop down list.
         */

        var ordersIDs = $('input[name=orders]:checked').map(function() {
            return $(this).val();
        }).get();


        if(ordersIDs.length == 0) {
            alert("Please select an order to send to the captain");
            return;
        }else if($("#captains option:selected").val()=='default'){
            alert("Please choose a captain from the dropdown list");
            return;
        }


        console.log(ordersIDs);
        console.log($("#captains option:selected").val());
        console.log("this is the selected text of the captain dropdown list: "+$("#captains option:selected").text());
        console.log("here we send the link to the captain it must include ids of the request + captain id");


        //get the selected row and inject the captain information:
//        var data = table.row('.selected').data();
//        data[12] =  $("#captains option:selected").text();
//        table.row('.selected').data(data);



        smsLink = "http://kasper-cab.com/jollychic/walker/getupdatelink/";

        for (var i=0; i<ordersIDs.length; i++){
            if (i == 0){
                smsLink = smsLink+ordersIDs[i];
            }else {
                smsLink = smsLink+'s'+ordersIDs[i];
            }

        }

        console.log("this is your link: "+smsLink);
        console.log("captain ID: "+$("#captains option:selected").val());


        $.ajax({
            type: 'POST',
            url: 'sendorderssms',
            dataType: 'text',
            data: {
                   sms: smsLink,
                   captainid: $("#captains option:selected").val()
                  },
        }).done(function (response) {

            if (response == 1){
                alert("request assigned");
            }else {
                alert("something went wrong, unable to send sms to the captain");
            }
        }); // Ajax Call




    }


function updateInfo(orderID){
     window.open('https://kasper-cab.com/jollychic/updateitempage/'+orderID, '_blank');
    
} 

function sendSMS(orderID) 
{


    
    //1. customer mobile.
    //2. sms link.
    //3. request ID.
    


    var data = table.row('.selected').data();
    console.log("ordeID" + data[0]);
    console.log("customer mobile" + data[3])
    console.log("sms status" + data[12])
    smsLink = "https://kasper-cab.com/jollychic/getcustomerinformation/"+data[0].toString();
    console.log("this is the sms link: "+smsLink);




    $.ajax({
        type: 'POST',
        url: 'http://kasper-cab.com/jollychic/sendcustomersms',
        dataType: 'text',
        data: {
            requestid: data[0],
            mobile: +966505694459,//data[3],
            sms: smsLink
        },
    }).done(function (response) {

             console.log(response);
           
        if(response == 1){
            alert("sms sent");
        }else {
            alert("something wrong, please make sure you selected the row before cliking on send button ");
        }

    }); 




}



</script>



<!-- DataTables Library -->
<script src="/Dev-Server-Resources/tableassets/jquery.js"></script>
<script src="/Dev-Server-Resources/tableassets/jquery.datatables.js"></script>
<!-- DataTables Search Script  -->
<script src="/Dev-Server-Resources/tableassets/fnFindCellRowIndexes.js"></script>


<!-- jstables script -->
<script>

    //datatables methods
    $(document).ready(function () {
        //#1 method hide columns

        $('#example').DataTable({

            dom: 'Bfrtip',
            colReorder: true,
        
           buttons: [
               'excel'
            ],
            "iDisplayLength": 20,
            "order": [[ 1, "asc" ]]
        });


        //#2 Method: Row Selections.
        table = $('#example').DataTable();


        $('#example tbody').on('click', 'tr', function () {

            var data = table.row(this).data();

          
        });

        //logic for row selection.
        $('#example tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });


    });


</script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- PDF file generater -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>



<!-- MDL Linbraries -->
<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>

<!--<script src="/kc/app/views/web/DeliveryApp/assets-2/js/bootstrap.min.js"></script>-->

</body>

</html>


