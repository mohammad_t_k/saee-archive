<?php

// echo $order;

?>
<html dir="ltr" lang="en">
<head>
    <!--Resources-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places"></script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <!-- change this -->
    
     
    
    
     <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/styles2.css">
     <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/styles.css">
    
    
    
    
    <!-- UI resources -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico"/>
    <!-- Font -->
     
    <link href="/DeliveryApp2/assets-2/css/font-awesome.min.css" rel="stylesheet">

    <!-- Latest compiled and minified Bootstap CSS -->
    <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/bootstrap.min.css">

    
    
    <!-- CSS Styles -->
    
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<div layout="column">
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <figure class="logo"><a><img src="/DeliveryApp2/assets-2/images/logo.svg"
                                                 alt="Khod w Haat" class="img-responsive"></a></figure>
                </div>
                <!--put jollychic logo here-->
                <div class="col-xs-6">
                    <!--<nav class="navbar navbar-default nav_menu">-->
                    <!--</nav>-->
                </div>
            </div>
        </div>
    </header>


    <section class="content-wrapper internal-page">
        <div class="container">
            <div class="row">
                <!-- new request start-->
                <div id="lat" hidden><?php echo $order->D_latitude ?> </div>
                <div id="lng" hidden><?php echo $order->D_longitude ?> </div>
                <div id="dropofftime" hidden><?php echo $order->scheduled_dropoff_time ?></div>
                <div class="col-sm-9">
                    <form id="RequestForm" name="RequestForm" layout="column">
                        <!-- push this to the map-->
                        <input id="pac-input2" class="controls" type="text" placeholder="Search DropOff">
                        <div style="width:570px " id="requestformhint" class="alert alert-danger" role="alert" hidden></div>
                        <div style="width:570px " id="successmsg" class="alert alert-success" role="alert" hidden></div>


                        <h4 class="btmmargin topmargin">Item Information:</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-barcode"></span>
                                    
                                 <?php
                              echo  '<input id="jollychicid" name="jollychicid" type="text" class="form-control" placeholder="JollyChic Item ID" value="' . $order->jollychic .'" disabled />'
                                 ?>
                                           
                                </div>
                            </div>
                        </div>

                        <h4 class="btmmargin topmargin">Customer Information:</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-user"></span>
                                    <!--<input id="dropoffname" type="text"-->
                                    <!--       class="form-control" placeholder="Name">-->
                                           
                                                   <?php
                               
                              
                              echo         '<input id="dropoffname" type="text"  class="form-control" placeholder="Name" value="'. $order->receiver_name .'">'
                              
                               
                                 ?>
                                           
                                           
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-mobile"></span>
                                    <!--<input id="dropoffmobile" type="text" class="form-control"-->
                                    <!--       placeholder="Mobile 9665########">-->
                                           
                                           
                                            <?php
                               
                              
                              echo         '<input id="dropoffmobile" type="text" class="form-control"   placeholder="Mobile 9665########" value="'. $order->receiver_phone .'">'
                              
                               
                                 ?>
                                           
                                           
                                </div>
                            </div>
                        </div>

                        <div id="dropoffapproach" class="form-group">
                            <input type="radio" name="dropoffapproach" value="0" checked> Immediate Drop-off<br>
                            <input type="radio" name="dropoffapproach" value="1"> Scheduled Drop-off<br>
                        </div>

                        <div id="scheduleddropoff" class="row" hidden>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <!--<input type="text" class="form-control" id="dropoffdate"-->
                                        <!--       placeholder="Select Date">-->
                                               
                                               <?php
                                               
                         echo  ' <input type="text" class="form-control" id="dropoffdate"  placeholder="Select Date" value="'. $order->scheduled_dropoff_date .'">'

                                               
                                               ?>
                                               
                                               
                                               
                                        <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-7 col-sm-8">
                                <label>Drop-off time</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot1" onclick="setdropofftime(1)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">10 am - 12 pm</a></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot2" onclick="setdropofftime(2)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">2 - 4 pm</a></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot3" onclick="setdropofftime(3)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">7 - 9 pm</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <label>Drop-off Location</label>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div id="map_canvas2"
                                         style="width:100%; height:300px"></div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>
                        <div style="width:570px " id="cashshint" class="alert alert-danger" role="alert"
                             hidden></div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <label>Items Cost:</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-money"></span>
                                    
                                    
                                    <!--<input id="cashondelivery" type="number" class="form-control" placeholder="Cash Amount" value="0">-->
                                    <?php
                                    
                                    echo ' <input id="cashondelivery" type="number" class="form-control" placeholder="Cash Amount" value="'. $order->cash_on_delivery.'">'
                                    
                                    ?>
                                           
                                           
                                </div>
                            </div>
                        </div>
                    </form>
                    <button style="float: right;" id="submit" class="btn btn-main" onclick="submit()">Update Item
                    </button>
                </div>
            </div>


        </div>
    </section>


    <!-- Angular Material requires Angular.js Libraries -->

    <!-- Angular Material Library -->

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    
    <script src="/DeliveryApp2/assets-2/js/bootstrap.min.js"></script>
    
    <link href="/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.min.css"
          rel="stylesheet">
    <script src="/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>

    <script>

        //disable submit on click to avoid confusing. add to other forms later...
        $('#RequestForm').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $.datetimepicker.setLocale('en');
        jQuery('#dropoffdate').datetimepicker({
            timepicker: false,
            format: 'd.m.Y'
        });


        //GLOBAL VARS
        var immediateDropoff = true;
        var dropoffTime = "";
        var dropoffDate = "";



        /*Map Script*/
        //add window before the object it will make accesable by all controllers...

        window.map2;//will contain second map object
        var dropoffAddress; //willl contain the string address of dropoff location.
        window.dropoff = false; //dropoff location marker


         console.log($("#lat").text());
         console.log($("#lng").text());
         console.log($("#dropofftime").text());
         
          setdropofftime($("#dropofftime").text());

        geocoder = new google.maps.Geocoder;

        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(21.551343, 39.174972);

        var options2 = {
            center: centerOfMap,
            zoom: 10,
            mapTypeControl: false,
            disableDefaultUI: false
        }

        //Create the map object.
        map2 = new google.maps.Map(document.getElementById('map_canvas2'), options2);

        var input2 = document.getElementById('pac-input2');
        var searchBox2 = new google.maps.places.SearchBox(input2);
        map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input2);


        // Bias the SearchBox results towards current map's viewport.
        map2.addListener('bounds_changed', function () {
            searchBox2.setBounds(map2.getBounds());
        });


var myLatlng = new google.maps.LatLng($("#lat").text(),$("#lng").text());


            dropoff = new google.maps.Marker({
                position: myLatlng,
                map: map2,
                draggable: true //make it draggable
            });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox2.addListener('places_changed', function () {


            var places = searchBox2.getPlaces();
            if (places.length == 0) {
                return;
            }
            clearHomeMarker2();



            dropoff = new google.maps.Marker({
                position: places[0].geometry.location,
                map: map2,
                draggable: true //make it draggable
            });
            setAddress("dropoff");

            map2.setCenter(places[0].geometry.location);
            map2.setZoom(15);
            //  console.log(places[0].geometry.location);
            //clear marker1 and push the new one.

        });
        //Listen for any clicks on the map.
        google.maps.event.addListener(map2, 'click', function (event) {
            //Get the location that the user clicked.
            var clickedLocation = event.latLng;
            //If the marker hasn't been added.
            if (dropoff === false) {
                //Create the marker.
                dropoff = new google.maps.Marker({
                    position: clickedLocation,
                    map: map2,
                    draggable: true //make it draggable
                });
                setAddress("dropoff");

                //Listen for drag events!
                google.maps.event.addListener(dropoff, 'dragend', function (event) {
                    setAddress("dropoff");
                });
            } else {
                //Marker has already been added, so just change its location.
                dropoff.setPosition(clickedLocation);
                console.log("Company Location" + dropoff.getPosition());
                setAddress("dropoff");
            }
        });


        function clearHomeMarker2() {

            try {
                console.log("Company Location" + dropoff.getPosition());

                //  marker2.setMap(null);
            } catch (err) {
                console.log("Markers are not initialized ");
            }

            try {
                dropoff.setMap(null);
                dropoff = false;
            } catch (err) {
                console.log("Marker is not initialized ");
            }

        }

        //        this function also is not properly written, must be general, inject address to the view it does include latitude and longitude.
        function setAddress(marker) {

            if (marker == "pickup") {
                geocodePosition(new google.maps.LatLng(pickup.getPosition().lat(), pickup.getPosition().lng()),
                    //anonymous function
                    function (location) {
                        pickupAddress = location;
                        console.log(pickupAddress + "for marker" + pickup);
                        //now update the address shwown in the HTML tag
                        $("#defaultlocation").text(pickupAddress);
                        $("#pickuplocationlatitude").val(pickup.getPosition().lat());
                        $("#pickuplocationlongitude").val(pickup.getPosition().lng());
                    }
                );

            } else if (marker == "dropoff") {
                geocodePosition(new google.maps.LatLng(dropoff.getPosition().lat(), dropoff.getPosition().lng()),
                    //anonymous function
                    function (location) {
                        dropoffAddress = location;
                    }
                );
            } else {
                console.log("something wrong");
            }

        }

        //returns will assighn address to its parm "location' or respones[0].formatted_address;
        function geocodePosition(pos, assign) {
            geocoder.geocode({latLng: pos}, function (responses) {
                if (responses && responses.length > 0) {
                    // call anonymous function with parm  :returns(location) location = respones[0].formatted_address;
                    assign(responses[0].formatted_address);
                } else {
                    assign("none");
                }
            });
        }

        <!-- change map width when loaded from mobile-->
        var useragent = navigator.userAgent;
        var dropoffmap = document.getElementById("map_canvas2");

        if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1) {

            dropoffmap.style.width = '100%';

        } else {
            /* pickupMap.style.width = '600px';
             pickupMap.style.height = '800px';

             dropoffmap.style.height = '600px';
             dropoffmap.style.height = '800px';*/
        }

        /*Map Script ends here*/


        $("input[name='dropoffapproach']").on("change", function () {
            dropoffapproachChanged(this.value);
        });


        function setdropofftime(spotNumber) {

            //clear all time spots and add highlight the new one..
            for (x = 0; x <= 3; x++) {
                $("#dropoffspot" + x).css("background-color", "white");
                $("#dropoffspot" + x).css("color", "black");
            }

            //Now highlight the selected one only:
            $("#dropoffspot" + spotNumber).css("background-color", "#e6e6e6");
            $("#dropoffspot" + spotNumber).css("color", "#333");

            dropoffTime = spotNumber;

        }
        function dropoffapproachChanged(value) {

            if (value == 0) {
                $("#scheduleddropoff").hide();
                immediateDropoff = true;

            } else if (value == 1) {
                $("#scheduleddropoff").show();
                immediateDropoff = false;
            }
        }


        function submit() {

            if(immediateDropoff){
                immediateDropoff = 1;
                dropoffDate = '';
                dropoffTime = '';
            }else {
                immediateDropoff = 0;
            }



            /*
             1. Jollychic ID.
             2. name.
             3. mobile.
             4. immediate dropoff.
             5. scheduled dropoff time and date.
             6. dropoff marker on the map.
             7. cash amount. default as = 0.
             */



            //assuming the jollychic id is => 10
            // if ($("#jollychicid").val().length != 10) {
            //     //show error:
            //     $("#requestformhint").show();
            //     $("#requestformhint").text("Please a valid jollychic id number");
            //     $('html, body').animate({ scrollTop: 0 }, 'fast');

            //     return;

            // } else {
            //     //hide error:
            //     $("#requestformhint").hide();
            //     jollychicid = $("#jollychicid").val();
            // }


            if ($("#dropoffname").val() == "") {
                $("#requestformhint").show();
                $("#requestformhint").text("Please enter the name of the customer");
                $('html, body').animate({ scrollTop: 0 }, 'fast');

                return;
            } else {
                $("#requestformhint").hide();
            }

            //validate dropoff name and number.
            if ($("#dropoffmobile").val().length != 12) {
                $("#requestformhint").show();
                $("#requestformhint").text("Please enter a correct Receiver/Customer mobile number 966#########");
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                return;
            } else {
                $("#requestformhint").hide();
            }


            //validate dropoff approach.
            if (!immediateDropoff) {
                if (dropoffTime == "") {
                    $("#requestformhint").show();
                    $("#requestformhint").text("Please choose dropoff time");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                } else {
                    $("#requestformhint").hide();
                    console.log(dropoffTime);
                }
                if ($("#dropoffdate").val() == "") {
                    $("#requestformhint").show();
                    $("#requestformhint").text("Please choose dropoff date");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                } else {
                    $("#requestformhint").hide();
                    dropoffDate = $("#dropoffdate").val();
                    console.log(dropoffDate);
                }
            }

            //Dropoff Location Validation.
            if (!dropoff) {
                $("#requestformhint").show();
                $("#requestformhint").text("Please choose dropoff location from the second map");
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                return;
            } else {
                $("#requestformhint").hide();
            }

            // console.log("all validated, here is the data below:")
            // console.log("jollychic ID:"+$("#jollychicid").val());
            console.log("customer name:"+$("#dropoffname").val());
            console.log("customer mobile:"+$("#dropoffmobile").val());
            console.log("dropoff date:"+$("#dropoffdate").val());
            console.log("dropoff time:"+dropoffTime);
            console.log("is it immediate dropoff?"+immediateDropoff);
            console.log($("#cashondelivery").val());
            console.log(dropoff.getPosition().lat());
            console.log(dropoff.getPosition().lng());
            console.log(dropoff);
            console.log(dropoffAddress);


            //save it:
            var request = $.ajax({
                type: 'POST',
                url: '/jollychic/updateitem',
                dataType: 'text',
                data: {
                    jollychic_id: $("#jollychicid").val(),
                    d_latitude: dropoff.getPosition().lat(),
                    d_longitude: dropoff.getPosition().lng(),
                    d_address: dropoffAddress,
                    dropoffname: $("#dropoffname").val(),
                    dropoffmobile: $("#dropoffmobile").val(),
                    immediatedropoff: immediateDropoff,
                    dropoffdate: dropoffDate,
                    dropofftime: dropoffTime,
                    cash_on_delivery: $("#cashondelivery").val(),
                    transpiration_fees:  25
                }
            });
            request.done(function (response) {

                console.log(response);
                if (response ==1){
                    //go to error msg and show success msg that the request is created...
                    console.log("right on");

                }else if (response ==0){
                    //show something wrong error msg...
                }

                $("#successmsg").show();
                $("#successmsg").text("Item successfully Updated");
                $('html, body').animate({ scrollTop: 0 }, 'fast');

                // setTimeout(function(){
                //     $("#successmsg").hide();
                //     //clear the form here:
                //     clearForm();
                // }, 4000);

                //clear form:
                console.log("clear the form now and hide the success msg");

            }); // Ajax Call*!/
            request.fail(function (jqXHR, textStatus) {
                console.log("request fail for the reason" + textStatus);
                //show erroe msg with
            });
        }

        function clearForm() {

            //clear ID field...
            $("#jollychicid").val("");
            //clear the map..
            clearHomeMarker2();
            //seletc the first radio button for immediate dropoff...
            $("input[name=dropoffapproach][value=0]").prop('checked', true);
            //clear the selected spots
            for (x = 0; x <= 3; x++) {
                $("#dropoffspot" + x).css("background-color", "white");
                $("#dropoffspot" + x).css("color", "black");
            }
            //clear other fields
            $("#dropoffname").val("");
            //clear customer mobile feild...
            $("#dropoffmobile").val("");
            immediateDropoff = true;
            $("#cashondelivery").val(0)
            $("#dropoffdate").val("");
            //hide it:
            $("#scheduleddropoff").hide();

        }



    </script>


    </body>
</html>