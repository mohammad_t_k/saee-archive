<?php


/*
 * Important: must include an IF condition to check if the request is delivered and accordingly disable the update button:
 */


//latitude and longitude of the One Sar company location:
$lat = 21.575767;
$lng =    39.149122;
//echo sizeof($requests);

//good job!!1
//foreach ($requests as $request){
//    echo $request;
//}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Khod W Haat | Home Page</title>
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="/Dev-Server-Resources/css/bootstrap.min.css" type="text/css">

    <link href="font-awesome.min.css" rel="stylesheet">
    <style type="text/css">
        *{padding: 0; margin: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;}
        body{color: #333; border-top: 5px solid rgb(244, 170, 47); font-family: '29LT Bukra', sans-serif; font-weight: normal; font-size: 13px;}
        p{margin-bottom: 7px;}
        p span{color: #666; font-size: 12px;}
        a:hover{color: rgb(244, 170, 47); text-decoration: none !important;}
        a:focus{outline: none !important;}
        .btn{border-radius: 0; font-weight: 500; font-size: 13px; padding: 15px; text-align: center; text-decoration: none;}
        .btn-main{background: rgb(244, 170, 47); color: #fff;}
        .btn-main:hover{color: #fff; background: rgb(109, 109, 109);}
        .btn-block{display: block;}
        .btmmargin-sm{margin-bottom: 15px;}
        .header{padding: 10px 0; background: #fafafa; margin-bottom: 20px; border-bottom: 1px solid #eee;}
        .header img{width: 100px; margin: 0 auto;}
        .shadow-box{padding: 15px; box-shadow: 0 0 2px #ddd; -moz-box-shadow: 0 0 2px #ddd; -webkit-box-shadow: 0 0 2px #ddd; margin-bottom: 15px;}
        .form-group{position: relative; padding-left: 30px;}
        .form-control{width: 100%; padding: 5px; border:1px solid #ccc;}
        .form-group i{position: absolute; left: 0; top: 3px; font-size: 20px;}

    </style>

</head>
<body>
<div style="max-width: 500px; margin: 0 auto;">
    <header class="header" style="text-align: center;">
        <figure class="logo"><img src="/kc/app/views/web/DeliveryApp/assets-2/images/logo.svg" alt="Khod w Haat" class="img-responsive"></figure>
    </header>


    <!--elements list-->
    <div id="accordion" role="tablist" aria-multiselectable="true">

        <!--loop over requests here:-->

        <?php

        foreach ($requests as $request) {
            echo '<br>';

            echo '
        <div class="card">
            <div class="card-header" role="tab" id="' . $request->id . '">
                <h5 class="mb-0">
                <div align="center">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="' . "#" . "collapse" . $request->id . '" aria-expanded="false" aria-controls="' . "collapse" . $request->id . '">
                ' . "Request number: ".$request->id . '
                    </a>
                    </div>
                </h5>
            </div>
            <div id="' . "collapse" . $request->id . '" class="collapse" role="tabpanel" aria-labelledby="' . $request->id . '">
                <div class="card-block">
                
         

    <section style="padding: 0 20px;">
        <div class="shadow-box">
            <p id="requestid" hidden><' . $request->id . '</p>
            <p align="center" class="btmmargin-sm">' . " رقم الطلب" . $request->id . '</p>
<!-- 
//            <p class="btmmargin-sm"><strong>Pick Up Info:</strong></p>
//            <p><b>Name:</b> <span>' . $request->sender_name . '</span></p>
//            <p><b>Mobile:</b> <span>' . $request->sender_phone . '</span></p>
//           <p><b>Scheduled:</b>--> ';
//            if ($request->immediate_pickup == 0) {
//                echo '<i class="fa fa-check" style="color: green;"></i>';
//                echo "YES";
//                echo '<p><b>Date:</b> <span>' . $request->scheduled_pickup_date . '</span></p>';
//
//                if ($request->scheduled_pickup_time == 1) {
//                    echo '<p><b>Time:</b> <span>' . "10 am - 12 pm" . '</span></p>';
//                } else if ($request->scheduled_pickup_time == 2) {
//                    echo '<p><b>Time:</b> <span>' . "2 - 4 pm" . '</span></p>';
//                } else if ($request->scheduled_pickup_time == 3) {
//                    echo '<p><b>Time:</b> <span>' . "7 -9 pm" . '</span></p>';
//                }
//
//            } else {
//                echo '<p><b> Immediate Pickup </b></p>';
//            }

            echo '
                   <p><b>PickUp Link Address:</b>   <a href=" ' . "http://www.google.com/maps/place/" . $lat . "," . $lng. '">Click here for pickup address</a> </p>
        </div> 
        
            
    
        <div class="shadow-box">
            <p class="btmmargin-sm"><strong>Drop Off Info:</strong></p>
            <p><b>Name:</b> <span>' . $request->receiver_name . '</span></p>
            <p><b>Mobile:</b> <span>' . $request->receiver_phone . '</p>
            <p><b>Scheduled:</b>
            
            ';
            if ($request->immediate_dropoff == 0) {
                echo '<i class="fa fa-check" style="color: green;"></i>';
                echo "YES";
                echo '<p><b>Date:</b> <span>' . $request->scheduled_dropoff_date . '</span></p>';

                if ($request->scheduled_dropoff_time == 1) {
                    echo '<p><b>Time:</b> <span>' . "10 am - 12 pm" . '</span></p>';
                } else if ($request->scheduled_dropoff_time == 2) {
                    echo '<p><b>Time:</b> <span>' . "2 - 4 pm" . '</span></p>';
                } else if ($request->scheduled_dropoff_time == 3) {
                    echo '<p><b>Time:</b> <span>' . "7 -9 pm" . '</span></p>';
                }
            } else {
                echo '<p><b>Immediate Dropoff </b>';
            }

            echo '
            </p>
           <p><b>Link Address:</b>   <a href=" ' . "http://www.google.com/maps/place/" . $request->D_latitude . "," . $request->D_longitude . '">Click here for dropoff address</a> </p>
          </div>       
               ';



            echo '
            <div  class="shadow-box">
                     </span></p>
            <p><b>Cash On Delivery:</b> <span>' . $request->cash_on_delivery . ' SAR' . '</span></p>    
                    
                    </span></p>
        </div>
        <div id="updatebutton" class="btmmargin-sm" onclick="updateStatus('.$request->id.')"><a class="btn btn-main btn-block">تم توصيل الطلب</a></div>
    </section>
                
                
                </div>
            </div>
    </div>
        ';
        }
        ?>



</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="/Dev-Server-Resources/js/bootstrap.min.js"></script>

</body>
</html>
<script>
    function updateStatus(requestID) {

        $.ajax({
            type: 'POST',
            url: 'http://kasper-cab.com/jollychic/walker/updatestatus',
            dataType: 'text',
            data: {id: requestID, status:5}
        }).done(function (response) {
            alert("تم تحديث حالة الطلب");
        }); // Ajax Call
    }
</script>
