<?php ?>
<html dir="ltr" lang="en">
<head>
    <!--Resources-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places"></script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <!-- change this -->
    
     
    
    
     <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/styles2.css">
     <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/styles.css">
    
    
    
    
    <!-- UI resources -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico"/>
    <!-- Font -->
     
    <link href="/DeliveryApp2/assets-2/css/font-awesome.min.css" rel="stylesheet">

    <!-- Latest compiled and minified Bootstap CSS -->
    <link rel="stylesheet" href="/DeliveryApp2/assets-2/css/bootstrap.min.css">

    
    
    <!-- CSS Styles -->
    
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<head>
<title>Kasper & JollyChic</title>
</head>

<div layout="column">
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <!--put jollychic logo here
                    <figure class="logo"><a><img src="/DeliveryApp2/assets-2/images/logo.svg"
                                                 alt="Jolly" class="img-responsive"></a></figure>
                                                 -->
                </div>
                <!--put jollychic logo here-->
                <div class="col-xs-6">
                    <!--<nav class="navbar navbar-default nav_menu">-->
                    <!--</nav>-->
                </div>
            </div>
        </div>
    </header>


    <section class="content-wrapper internal-page">
        <div class="container">
            <div class="row">
                <!-- new request start-->
                <div class="col-sm-9">
                    <form id="RequestForm" name="RequestForm" layout="column">
                        <div style="width:570px " id="requestformhint" class="alert alert-danger" role="alert" hidden></div>
                        <div style="width:570px " id="successmsg" class="alert alert-success" role="alert" hidden></div>

                        <h4 class="btmmargin topmargin">Item Information:</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-barcode"></span>
                                    <input id="jollychicid" name="jollychicid" type="text"
                                           class="form-control" placeholder="JollyChic Item ID">
                                </div>
                            </div>
                        </div>

                        <h4 class="btmmargin topmargin">Customer Information:</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-user"></span>
                                    <input id="dropoffname" type="text"
                                           class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-mobile"></span>
                                    <input id="dropoffmobile" type="text" class="form-control"
                                           placeholder="Mobile 9665########">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-30 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-map-marker"></span>
                                    <input id="dropoffAddress" type="text"
                                           class="form-control" placeholder="address(unit no, street), district, city, province">
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        <div id="dropoffapproach" class="form-group">
                            <input type="radio" name="dropoffapproach" value="0" checked> Immediate Drop-off<br>
                            <input type="radio" name="dropoffapproach" value="1"> Scheduled Drop-off<br>
                        </div>

                        <div id="scheduleddropoff" class="row" hidden>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="dropoffdate"
                                               placeholder="Select Date">
                                        <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-7 col-sm-8">
                                <label>Drop-off time</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot1" onclick="setdropofftime(1)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">10 am - 12 pm</a></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot2" onclick="setdropofftime(2)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">2 - 4 pm</a></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group"><a id="dropoffspot3" onclick="setdropofftime(3)"
                                                                   class="btn btn-default btn-block"
                                                                   style="padding: 10px;">7 - 9 pm</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <br>
                        <br>
                        <div style="width:570px " id="cashshint" class="alert alert-danger" role="alert"
                             hidden></div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <label>Items Cost:</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group input-icon">
                                    <span class="fa fa-money"></span>
                                    <input id="cashondelivery" type="number" class="form-control"
                                           placeholder="Cash Amount" value="0">
                                </div>
                            </div>
                        </div>
                    </form>
                    <button style="float: right;" id="submit" class="btn btn-main" onclick="submit()">Save Item
                    </button>
                </div>
            </div>


        </div>
    </section>


    <!-- Angular Material requires Angular.js Libraries -->

    <!-- Angular Material Library -->

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    
    <script src="/DeliveryApp2/assets-2/js/bootstrap.min.js"></script>
    
    <link href="/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.min.css"
          rel="stylesheet">
    <script src="/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>

    <script>

        //disable submit on click to avoid confusing. add to other forms later...
        $('#RequestForm').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $.datetimepicker.setLocale('en');
        jQuery('#dropoffdate').datetimepicker({
            timepicker: false,
            format: 'd.m.Y'
        });


        //GLOBAL VARS
        var immediateDropoff = true;
        var dropoffTime = "";
        var dropoffDate = "";



        $("input[name='dropoffapproach']").on("change", function () {
            dropoffapproachChanged(this.value);
        });


        function setdropofftime(spotNumber) {

            //clear all time spots and add highlight the new one..
            for (x = 0; x <= 3; x++) {
                $("#dropoffspot" + x).css("background-color", "white");
                $("#dropoffspot" + x).css("color", "black");
            }

            //Now highlight the selected one only:
            $("#dropoffspot" + spotNumber).css("background-color", "#e6e6e6");
            $("#dropoffspot" + spotNumber).css("color", "#333");

            dropoffTime = spotNumber;

        }
        function dropoffapproachChanged(value) {

            if (value == 0) {
                $("#scheduleddropoff").hide();
                immediateDropoff = true;

            } else if (value == 1) {
                $("#scheduleddropoff").show();
                immediateDropoff = false;
            }
        }


        function submit() {
             console.log("submitting:")
            if(immediateDropoff){
                immediateDropoff = 1;
                dropoffDate = '';
                dropoffTime = '';
            }else {
                immediateDropoff = 0;
            }


            /*
             1. Jollychic ID.
             2. name.
             3. mobile.
             4. immediate dropoff.
             5. scheduled dropoff time and date.
             6. dropoff marker on the map.
             7. cash amount. default as = 0.
             */

console.log("submitting 1:")
            //assuming the jollychic id is => 10
           
            //hide error:
            $("#requestformhint").show();
            if($("#jollychicid").val().match(/^JC[0-9]{8}KS$/))
            {
                    jollychicid = $("#jollychicid").val();
            }
            else
            {
                //show error:
                $("#requestformhint").show();
                $("#requestformhint").text("Please a valid jollychic id number i.e. JC00000000KS");
                console.log("Please a valid jollychic id number i.e. JC00000000KS")
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                return;
            }
            

console.log("submitting 2:")
            if ($("#dropoffname").val() == "") {
                $("#requestformhint").show();
                $("#requestformhint").text("Please enter the name of the customer");
                $('html, body').animate({ scrollTop: 0 }, 'fast');

                return;
            } else {
                $("#requestformhint").hide();
            }
console.log("submitting 3:")
            //validate dropoff name and number.
            if ($("#dropoffmobile").val().length != 12) {
                $("#requestformhint").show();
                $("#requestformhint").text("Please enter a correct Receiver/Customer mobile number 966#########");
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                return;
            } else {
                $("#requestformhint").hide();
            }

console.log("submitting 4:")
            //validate dropoff approach.
            if (!immediateDropoff) {
                if (dropoffTime == "") {
                    $("#requestformhint").show();
                    $("#requestformhint").text("Please choose dropoff time");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                } else {
                    $("#requestformhint").hide();
                    console.log(dropoffTime);
                }
                if ($("#dropoffdate").val() == "") {
                    $("#requestformhint").show();
                    $("#requestformhint").text("Please choose dropoff date");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                } else {
                    $("#requestformhint").hide();
                    dropoffDate = $("#dropoffdate").val();
                    console.log(dropoffDate);
                }
            }
console.log("submitting 5:")
            
            console.log("all validated, here is the data below:")
            console.log("jollychic ID:"+$("#jollychicid").val());
            console.log("customer name:"+$("#dropoffname").val());
            console.log("customer mobile:"+$("#dropoffmobile").val());
             console.log("dropoff address:"+$("dropoffAddress").val());
            console.log("dropoff date:"+$("#dropoffdate").val());
            console.log("dropoff time:"+dropoffTime);
            console.log("is it immediate dropoff?"+immediateDropoff);
            console.log($("#cashondelivery").val());
            
            console.log();


            //save it:
            var request = $.ajax({
                type: 'POST',
                url: 'saveitem',
                dataType: 'text',
                data: {
                    jollychic_id: $("#jollychicid").val(),
                    d_latitude: 21.551343,
                    d_longitude: 39.174972,
                    d_address: $("#dropoffAddress").val(),
                    dropoffname: $("#dropoffname").val(),
                    dropoffmobile: $("#dropoffmobile").val(),
                    immediatedropoff: immediateDropoff,
                    dropoffdate: dropoffDate,
                    dropofftime: dropoffTime,
                    cash_on_delivery: $("#cashondelivery").val(),
                    transpiration_fees:  25
                }
            });
            request.done(function (response) {
                alert(response);
                console.log(response);
                if (response ==1){
                    //go to error msg and show success msg that the request is created...
                    console.log("right on");

                }else if (response ==0){
                    //show something wrong error msg...
                    $("#requestformhint").show();
                    $("#requestformhint").text("Please enter a valid Jolly Chic Id number i.e. JC00000000KS");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                }else if (response ==2){
                    //show something wrong error msg...
                    $("#requestformhint").show();
                    $("#requestformhint").text("Following Jolly Chic Id Already Exists, Please Try Again");
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    return;
                }

                $("#successmsg").show();
                $("#successmsg").text("Item successfully saved in the database");
                $('html, body').animate({ scrollTop: 0 }, 'fast');

                setTimeout(function(){
                    $("#successmsg").hide();
                    //clear the form here:
                    clearForm();
                }, 4000);



                //clear form:
                console.log("clear the form now and hide the success msg");

            }); // Ajax Call*!/
            request.fail(function (jqXHR, textStatus) {
                console.log("request fail for the reason" + textStatus);
                //show erroe msg with
            });
        }

        function clearForm() {

            //clear ID field...
            $("#jollychicid").val("");
           
            //seletc the first radio button for immediate dropoff...
            $("input[name=dropoffapproach][value=0]").prop('checked', true);
            //clear the selected spots
            for (x = 0; x <= 3; x++) {
                $("#dropoffspot" + x).css("background-color", "white");
                $("#dropoffspot" + x).css("color", "black");
            }
            //clear other fields
            $("#dropoffname").val("");
            //clear customer mobile feild...
            $("#dropoffmobile").val("");
            immediateDropoff = true;
            $("#cashondelivery").val(0)
            $("#dropoffdate").val("");
            $("#dropoffAddress").val("");
            
            //hide it:
            $("#scheduleddropoff").hide();

        }



    </script>


    </body>
</html>