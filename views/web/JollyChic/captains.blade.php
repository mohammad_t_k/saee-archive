<?php

//echo "the map is fixed and it will show percentage'delivered 'orders by each district in city of Jeddah";
//echo '<br>';
//echo 'or a button to show items delivered distributed over districts by percentages';


// printed below for testing only:
//
//foreach ($walkers as $walker) {
//    echo $walker->first_name;
//    echo $walker->phone;
//}
//
//foreach ($cashToGive as $cash) {
//    echo $cash;
//}
//
//
//foreach ($rate as $r) {
//    echo $r;
//}
//
//foreach ($overallCashOnDelivery as $overall) {
//    echo $overall;
//}


?>

        <!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>


    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="stylesheet" href="/kc/app/views/web/DeliveryApp/assets-2/css/bootstrap.min.css" type="text/css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

   


   <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css"/>
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles2.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/stylesarticle.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">




    <style>
        #map {
            width: 100%;
            height: 25%;
        }
    </style>


</head>
<body>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <h3 style="color: orange">JollyChic Captains Panel</h3>
        </div>
    </div>

    <main class="mdl-layout__content mdl-color--grey-100">
        <div style="padding: 0px 10px;" class="mdl-grid demo-content">

            <div id="contractform" style="height:1600px; width: 1800px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                <br>
                <br>

            {{--<table>--}}
            {{--</table>--}}
            <!--Two maps for picking Home ans Work locations-->
                <div align="center" id="map"></div>
                <br>

                <!-- Echos to table structure-->
                <?php

                //array_multisort($data, SORT_ASC);
                echo " <table id='example' class='display' cellspacing='0' width='100%'>  <thead>
            <tr>

                <th>ID</th> <!-- 0-->
                <th>Captain Name</th>  <!-- 1-->
                <th>Captain Mobile</th> <!-- 2-->
                <th>lat</th>  <!-- 3-->
                <th>lng</th>  <!-- 4-->
                <th>Overall Cash On Delivery</th> <!--   5-->
                <th>Rate</th> <!--  6-->
                <th>Cash to Collect</th>  <!-- 7 -->
                <th>Cash to Give</th> <!-- 8 -->

              </tr>
        </thead>
        ";

                $i = 0;
                foreach ($walkers as $walker) {

                    echo "<tr>";
                    echo "<td>" . $walker->id . "</td>";
                    echo "<td>" . $walker->first_name . " " . $walker->last_name . "</td>";
                    echo "<td>" . $walker->phone . "</td>";
                    echo "<td>" . $walker->address_latitude . "</td>";
                    echo "<td>" . $walker->address_longitude . "</td>";
                    echo "<td>" . $overallCashOnDelivery[$i] . "</td>";
                    echo "<td>" . $rate[$i] . "</td>";
                    echo "<td>" . $walker->credit . "</td>";
                    echo "<td>" . $cashToGive[$i] . "</td>";
                    echo "</tr>";

                    $i++;
                }
                echo "</table>";
                ?>
                <div>


                    <br> <br>
                    <button style="color: blue" id="delivered"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="clearCash()">
                        Clear Credit for Captain
                    </button>

<!--
                    <button style="color: red" id="delivered"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="showItemsByDistrict()">
                        Show Delivered Items By District
                    </button>
-->

                    <br>
                    <br>
                    <br>
                    <br>


                </div>
    </main>
</div>


<script>

    function clearCash() {

        var data = table.row('.selected').data();
        console.log("captain ID" + data[0]);
        data[7] = 0;
        table.row('.selected').data(data);

        console.log(data);

        $.ajax({
            type: 'POST',
            url: 'clearcaptaincredit',
            dataType: 'text',
            data: {captain_id: data[0]},
        }).done(function (response) {
            console.log(response);
            alert("Credit Cleared");
        }); // Ajax Call


    }

    function showItemsByDistrict() {

        console.log("show items by district");

    }


</script>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
<!-- DataTables Library -->
<!--<script src="/kc/public/Dev-Server-Resources/tableassets/jquery.js"></script>-->
<!--<script src="/kc/public/Dev-Server-Resources/tableassets/jquery.datatables.js"></script>-->
<!-- DataTables Search Script  -->
<!--<script src="/kc/public/Dev-Server-Resources/tableassets/fnFindCellRowIndexes.js"></script>-->




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- DataTables Library -->
<script src="/Dev-Server-Resources/tableassets/jquery.js"></script>
<script src="/Dev-Server-Resources/tableassets/jquery.datatables.js"></script>
<!-- DataTables Search Script  -->
<script src="/Dev-Server-Resources/tableassets/fnFindCellRowIndexes.js"></script>






<!-- jstables script -->
<script>

    //datatables methods
    $(document).ready(function () {
        //#1 method hide columns

        $('#example').DataTable({

            "columnDefs": [
                
                {
                    "targets": [3],
                    "visible": true,
                    "searchable": false
                },
                {
                    "targets": [4],
                    "visible": true,
                    "searchable": false
                }
                ,
                {
                    "targets": [5],
                    "visible": true,
                    "searchable": false
                },
                {
                    "targets": [6],
                    "visible": true,
                    "searchable": false
                },
                {
                    "targets": [7],
                    "visible": true,
                    "searchable": false
                }
            ],
            dom: 'Bfrtip',
            colReorder: true,

            buttons: [
                'excel'
            ],
            "iDisplayLength": 20
        });


        //#2 Method: Row Selections.
        table = $('#example').DataTable();


        $('#example tbody').on('click', 'tr', function () {

            var data = table.row(this).data();

            //call display route here:
            var StartLocation = new google.maps.LatLng(data[6], data[7]);
            var EndLocation = new google.maps.LatLng(data[4], data[5]);

            displayRoute(StartLocation, EndLocation, directionsService,
                directionsDisplay);

        });

        //logic for row selection.
        $('#example tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });


    });


</script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- PDF file generater -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>


<!-- map script -->
<script>

    var map;
    var FromAddress;
    var ToAddress;
    var doc;
    var directionsService;
    var directionsDisplay;
    var driverMarkers = [];

    function initMap() {
        var destination_place_id = null;
        var travel_mode = 'DRIVING';
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: 21.529, lng: 39.179}, //Jeddah
            // center: {lat: 24.713, lng: 46.675}, //Capital City
            zoom: 5
        });

        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: false,
            map: map
        });


        directionsDisplay.addListener('directions_changed', function () {


            var changedorigin = directionsDisplay.directions.routes[0].legs[0].end_location;

            console.log(changedorigin.lat());
            console.log(changedorigin.lng());


        });


        var StartLocation = new google.maps.LatLng(24.789095, 46.733349);
        var EndLocation = new google.maps.LatLng(24.789095, 46.733349);

        //      var EndLocation = new google.maps.LatLng(21.548977, 39.149628);

        //displayRoute(StartLocation, EndLocation , directionsService,
        //  directionsDisplay);


    }

    function displayRoute(origin, destination, service, display) {
        clearMarkers();

        service.route({
            origin: origin,
            destination: destination,
            travelMode: 'DRIVING',
            avoidTolls: true
        }, function (response, status) {
            if (status === 'OK') {
                display.setDirections(response);

                // console.log(response.routes[0].legs[0].start_address);

                FromAddress = response.routes[0].legs[0].start_address;


                // console.log(response.routes[0].legs[0].end_address);

                ToAddress = response.routes[0].legs[0].end_address


            } else {
                alert('Could not display directions due to: ' + status);
            }
        });
    }


    //clear/delete markers from the map.
    function clearMarkers() {

        for (var x = 0; x < driverMarkers.length; x++) {

            driverMarkers[x].setMap(null);
        }
        driverMarkers = [];


    }


</script>
<!-- MDL Linbraries -->
<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
<!-- load google API-->
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places&callback=initMap"
        async defer>
</script>

<script src="/kc/app/views/web/DeliveryApp/assets-2/js/bootstrap.min.js"></script>

</body>


</html>


