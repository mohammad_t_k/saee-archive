<?php

//echo "this is the request id"." ".$requestid;


?>
<!DOCTYPE html>
<html lang="en">
<head>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <!--<script src="/kc/app/views/web/DeliveryApp/assets-2/js/bootstrap.min.js"></script>-->
    <!--<link href="/kc/app/views/web/DeliveryApp/assets-2/datetimepicker-master/build/jquery.datetimepicker.min.css" rel="stylesheet">-->
    <!--<script src="/kc/app/views/web/DeliveryApp/assets-2/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>-->





  


<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="/DeliveryApp2/assets-2/js/bootstrap.min.js"></script> 
<script src="/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" href= "/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.min.css">





    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Khod W Haat | Home Page</title>
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--    <link href="font-awesome.min.css" rel="stylesheet">-->
    <style type="text/css">
        *{padding: 0; margin: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;}
        body{color: #666666; border-top: 5px solid rgb(244, 170, 47); font-family: '29LT Bukra', sans-serif; font-weight: normal; font-size: 13px;}
        p{margin-bottom: 5px;}
        a:hover{color: rgb(244, 170, 47); text-decoration: none !important;}
        a:focus{outline: none !important;}
        .btn{border-radius: 0; font-weight: 500; font-size: 13px; padding: 15px; text-align: center; text-decoration: none;}
        .btn-main{background: rgb(244, 170, 47); color: #fff;}
        .btn-main:hover{color: #fff; background: rgb(109, 109, 109);}
        .btn-block{display: block;}
        .btmmargin-sm{margin-bottom: 15px;}
        .header{padding: 10px 0; background: #fafafa; margin-bottom: 20px; border-bottom: 1px solid #eee;}
        .header img{width: 100px; margin: 0 auto;}
        .shadow-box{padding: 15px; box-shadow: 0 0 2px #ddd; -moz-box-shadow: 0 0 2px #ddd; -webkit-box-shadow: 0 0 2px #ddd;}
        .form-group{position: relative; padding-left: 30px;}
        .form-control{width: 100%; padding: 5px; border:1px solid #ccc;}
        .form-group i{position: absolute; left: 0; top: 3px; font-size: 20px;}
    </style>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div style="max-width: 500px; margin: 0 auto;">
    <header class="header" style="text-align: center;">
        <figure class="logo"><img src="/DeliveryApp2/assets-2/images/logo.svg" alt="Khod w Haat" class="img-responsive"></figure>
    </header>
    <div style="width:570px " id="requestformhint" class="alert alert-danger" role="alert" hidden></div>
    <section style="padding: 0 20px;">
        <p id="requestid" hidden><?php echo $requestid ?></p>
        <div class="shadow-box">
            <p class="btmmargin-sm">Please select date-time and location from the map below:</p>
            <div id="date" class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date</label>
                        <div class="input-group">
                            <input id="dropoffdate" type="text" class="form-control" id="datetimepicker" placeholder="Select Date">
                            <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="btmmargin-sm form-group">
                    <label>Time</label>
                    <i class="fa fa-clock-o"></i>
                    <select id="dropofftime" class="form-control">
                        <option selected>Select Time</option>
                        <option>10 AM - 12 PM</option>
                        <option>2 - 4 PM</option>
                        <option>7 - 9 PM</option>
                    </select>
                </div>
                <div id="map"></div>
                <br>
                <br>
                <div id="confirmbutton" class="btmmargin-sm" onclick="confirmInformation()"><a class="btn btn-main btn-block">Submit</a></div>

            </div>
    </section>
</div>
<script>
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 21.551343, lng: 39.174972},
            zoom: 8,
            mapTypeControl: false,
            disableDefaultUI: false
        });


        dropoff = false; //dropoff location marker
        geocoder = new google.maps.Geocoder;


        var centerControlDiv = document.createElement('span');
        var centerControl = new CenterControl(centerControlDiv, map);
        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);


//script for get current location
        function CenterControl(controlDiv, map) {

            // Set CSS for the control border.
            var controlUI = document.createElement('span');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '6px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to pick your current location';
            controlDiv.appendChild(controlUI);

            //Set CSS for the control interior.
            var controlText = document.createElement('span');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '14px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '7px';
            controlText.style.paddingRight = '7px';
            controlText.innerHTML = 'My Location';
            controlUI.appendChild(controlText);
            controlUI.addEventListener('click'
                , function() {
                    GetUserLocation(); // add map as a parm
                });
        }
        function GetUserLocation() {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {

                    OriginOfUserLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(OriginOfUserLocation);

                    clearMarker();
                    dropoff = new google.maps.Marker({
                        position: {lat: Number(position.coords.latitude),
                            lng: Number(position.coords.longitude)},
                        title: 'dropoff',
                        map: map,
                        drggable: false
                    });
                    setAddress("dropoff");

                });
            }
        }
        //Listen for any clicks on the map.
        google.maps.event.addListener(map, 'click', function(event) {
            //Get the location that the user clicked.
            var clickedLocation = event.latLng;
            //If the marker hasn't been added.
            if(dropoff === false){
                //Create the marker.
                dropoff = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true //make it draggable
                });

                setAddress("dropoff");
                //Listen for dragging events!
                google.maps.event.addListener(dropoff, 'dragend', function(event){
                    setAddress("dropoff");

                });
            } else{
                //Marker has already been added, so just change its location.
                dropoff.setPosition(clickedLocation);
                console.log("Company Location"+dropoff.getPosition());
                setAddress("dropoff");
            }
        });
        //clear markers from the map and replace them with new one
        function clearMarker() {

            try {
                console.log("Company Location"+dropoff.getPosition());

                //  marker2.setMap(null);
            } catch (err) {
                console.log("Markers are not initialized ");
            }
            try {
                dropoff.setMap(null);
                dropoff = false ;
            } catch (err) {
                console.log("Marker is not initialized ");
            }

        }
        //this function also is not properly written, must be general, inject address to the view it does include latitude and longitude.
        function setAddress(marker) {

            if(marker == "dropoff"){
                geocodePosition(new google.maps.LatLng(dropoff.getPosition().lat(), dropoff.getPosition().lng()),
                    //anonymous function
                    function (location) {
                        pickupAddress = location;
                    }
                );
            }
        }

//returns will assighn address to its parm "location' or respones[0].formatted_address;
        function geocodePosition(pos, assign) {
            geocoder.geocode({latLng: pos}, function (responses) {
                if (responses && responses.length > 0) {
                    // call anonymous function with parm  :returns(location) location = respones[0].formatted_address;
                    assign(responses[0].formatted_address);
                } else {
                    assign("none");
                }
            });
        }


        <!-- change map width when loaded from mobile ;) -->
        var useragent = navigator.userAgent;
        var dropoffmap = document.getElementById("map");

        if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 ) {

            dropoffmap.style.width = '340px';
            dropoffmap.style.height = '340px';

//            dropoffmap.style.width = '70%';
//            dropoffmap.style.height = '60%';

        } else {
            dropoffmap.style.height = '600px';
            dropoffmap.style.height = '340px';
        }

    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&callback=initMap"
        async defer></script>
</body>
</html>
<script>
    $.datetimepicker.setLocale('en');
    jQuery('#dropoffdate').datetimepicker({
        timepicker:false,
        format:'d.m.Y'
    });

    //use confirmation route:
    function confirmInformation() {


        var dropofftime;
        var dropoffdate;

        //validate date:
        if ($("#dropoffdate").val() == "") {
            $("#requestformhint").show();
            $("#requestformhint").text("Please choose dropoff date");
            return;
        } else {
            $("#requestformhint").hide();
            dropoffdate = $("#dropoffdate").val();
        }

        if ($('#dropofftime').val() == 'Select Time'){
            $("#requestformhint").show();
            $("#requestformhint").text("Please select appropriate time to deliver the product");
            return;
        }else {
            $("#requestformhint").hide();

            if($('#dropofftime').val() == '10 AM - 12 PM'){
                dropofftime =1;
            }else if ($('#dropofftime').val() == '2 - 4 PM'){
                dropofftime = 2;
            }else if ($('#dropofftime').val() == '7 - 9 PM'){
                dropofftime =3;
            }
        }

        //Marker/customer location Validation.
        if(!dropoff){
            $('#requestformhint').show();
            $("#requestformhint").text("Please choose a your location from the map");
            return;
        }else {
            $('#requestformhint').hide();
        }



        console.log($("#requestid").text());




        confirmInofrmation = $.ajax({
            type: 'post',
            url: 'https://kasper-cab.com/jollychic/confirmcustomerinformation',
            data: {
                requestid: $("#requestid").text(),
                latitude: dropoff.getPosition().lat(),
                longitude: dropoff.getPosition().lng(),
                time: dropofftime ,
                date: dropoffdate
            }
        })
        confirmInofrmation.done(function (response) {
            console.log('this is the response:  ' +response);
            alert("information updated, thanks !!");
        })
        confirmInofrmation.fail(function (jqxtem, reason) {
            console.log('something wrong we can not confirm your request');
            console.log(jqxtem);
            console.log(reason);
        })


    }
</script>
