@extends('web.providerLayout')

@section('content')
<?php $counter = 1; ?>
<div class="col-md-12 mt">
    @if(Session::has('error'))
    <div class="alert alert-danger">
        <b>{{ Session::get('error') }}</b> 
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success">
        <b>{{ Session::get('success') }}</b> 
    </div>
    @endif
    @if(isset($error))
    <div class="alert alert-danger">
        <b>{{ $error }}</b> 
    </div>
    @endif
    @if(isset($success))
    <div class="alert alert-success">
        <b>{{ $success }}</b> 
    </div>
    @endif
<link rel="stylesheet" href="../web/css/jquery.calendars.picker.css"/>

    <div class="content-panel">

        <br><h4>Update Vehcile(تحديث الملف)</h4><br>
        <form class="form-horizontal style-form" method="post" action="{{ URL::Route('updateProviderVehicle') }}" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Car Model<br>(طراز السيارة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="car_model" name="car_model"  value="{{ $user->car_model }}">
					<p id="car_modelErr" class="text-danger"></p>
                </div>
            </div>
			<div class="form-group"> 
                <label class="col-sm-2 col-sm-2 control-label">Vehicle Sequence Number<br>(الرقم التسلسلي للسيارة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="vehicle_sequence_number" name="vehicle_sequence_number"  onkeypress="return isNumber(event)" value="{{ $user->vehicle_sequence_number }}" placeholder='0000000000'>
					<p id="vehicle_sequence_numberErr" class="text-danger"></p>
                </div>
            </div>
			
			<div class="form-group"> 
                <label class="col-sm-2 col-sm-2 control-label">Car Registration Expiry<br>(تاريخ إنتهاء الإستمارة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="car_registration_expiry" name="car_registration_expiry"  value="{{$user->car_registration_expiry }}"
                    placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>
					<p id="car_registration_expiryErr" class="text-danger"></p>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Car Number Plate<br>(رقم لوحة السيارة)</label>
                <div class="col-sm-10"> 
					<div class="row">
                        <div class="col-sm-2   col-sm-2">
                            <!-- <h2 id="car_number_plate_numberexample" class="text-info">‍‍‍‎‎9110</h2> -->
                            <input type="text" class="form-control" id="car_number_plate_number" name="car_number_plate_number"  onkeypress="return isNumber(event)" value="{{$user->car_number_plate_number}}" maxlength="4" placeholder='0000'>
                            <p id="car_number_plate_numberErr" class="text-danger"></p>
                        </div>
                        <div class="col-sm-2  col-sm-2">
                            <!-- <h2 id="car_number_plate_letter_3example" class="text-info">‍‍‍‎‎ح</h2> -->
                            <select id="car_number_plate_letter_3" name="car_number_plate_letter_3" class="form-control" value="{{$user->car_number_plate_letter_3 }}"></select>
                        </div>
                        <div class="col-sm-2  col-sm-2">
                            <!-- <h2 id="car_number_plate_letter_2example" class="text-info">ب</h2> -->
                            <select id="car_number_plate_letter_2" name="car_number_plate_letter_2" class="form-control" value="{{$user->car_number_plate_letter_2 }}"></select>
                        </div>
						<div class="col-sm-2  col-sm-2">
                            <!-- <h2 id="car_number_plate_letter_1example" class="text-info">ا</h2> -->
							<select id="car_number_plate_letter_1" name="car_number_plate_letter_1" class="form-control" value="{{$user->car_number_plate_letter_1 }}"></select>
						</div>
                        <div class="col-sm-2  col-sm-2">
                            <img id="car_number_plate_example" src="../web/rsanumberplate.jpg" class="img img-responsive">
                        </div>
					</div>
				</div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Plate Type<br>(نوع اللوحة)</label>
                <div class="col-sm-6">
                    <select class="form-control" id="car_type" name="plate_type">
						<option value="1" <?php
                        if ($user->plate_type == '1') {
                            echo "selected";
                        }
                        ?>>Private</option>
						<option value="2" <?php
                        if ($user->plate_type == '2') {
                            echo "selected";
                        }
                        ?>>Taxi</option>
			 </select>
                </div>
            </div>
			<span style="display:none;">{{$i = 0;}}</span>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Car type<br>(نوع السيارة)</label>
                <div class="col-sm-10">
                    @foreach($type as $types)
                    <div class="col-sm-4">
                        <?php
                        foreach ($ps as $pss) {
                            $ser = ProviderType::where('id', $pss->type)->first();
                            $ar[] = $ser->name;
                        }
                        $servname = $types->name;
                        ?>
                        <input id="service" name="service[]" type="radio" value="{{$types->id}}" <?php
                        if (!empty($ar)) {
                            if (in_array($servname, $ar))
                                echo "checked='checked'";
                        }
                        ?>>{{$types->name}}
                        <br>
                    </div>
                    <?php $counter++; ?>
                    @endforeach
                </div>
            </div>
			
            <span class="col-sm-2"></span>
            <button id="update" type="submit" class="btn btn-info" onclick="if(!validateAllInputs()) return false;">Update Vehicle<br>(تحديث الملف)</button>
            <button type="reset" class="btn btn-info">Reset إعادة تعيين</button>

        </form>
    </div>
</div>

<script type="text/javascript">
var status = "ابدرسصطعقكلمحنهـوى";
var user = JSON.parse('<?php echo $user?>');
for(var i=0;i<status.length;i++){
		if(user.car_number_plate_letter_1==status[i]){
		$('#car_number_plate_letter_1').append($("<option selected ></option>").attr("value",status[i]).text(status[i]));
		}
		else
		$('#car_number_plate_letter_1').append($("<option ></option>").attr("value",status[i]).text(status[i]));
		if(user.car_number_plate_letter_2==status[i])
		$('#car_number_plate_letter_2').append($("<option selected ></option>").attr("value",status[i]).text(status[i]));
		else
		$('#car_number_plate_letter_2').append($("<option ></option>").attr("value",status[i]).text(status[i]));
		if(user.car_number_plate_letter_3==status[i])
		$('#car_number_plate_letter_3').append($("<option selected ></option>").attr("value",status[i]).text(status[i]));
		else
		$('#car_number_plate_letter_3').append($("<option ></option>").attr("value",status[i]).text(status[i]));
	}
</script> 
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<script src="../web/js/hijricalender/jquery.plugin.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>

<script>
$(function() {
	var calendar = $.calendars.instance('ummalqura');
	$('#car_registration_expiry').calendarsPicker({calendar: calendar,onSelect: function(date) {  $('#car_registration_expiry').val( date[0].formatDate('dd-mm-yyyy')); }});
    var carModel  = document.getElementById("car_model");
    var vehicleSequenceNumber = document.getElementById("vehicle_sequence_number");
	if (carModel.value=="0"){
            carModel.value="";
    }
    if (vehicleSequenceNumber.value=="0"){
            vehicleSequenceNumber.value="";
    }
    var waslresultCode = '<?php if(Session::has('waslresultCode')) echo Session::get('waslresultCode');?>';
    var waslError = '<?php if(Session::has('error')) echo Session::get('error');?>';
    switch(waslresultCode){
        case "421":
        if(waslError.search("Captain Identity Number")!=-1)
            document.getElementById("government_id_numberErr").innerHTML="Invalid values at Captain Government Identity Number<br>قيم غير صالحة في الكابتن الحكومة رقم الهوية";
        else if(waslError.search("Mobile Number")!=-1)            
            document.getElementById("phoneErr").innerHTML="Invalid value of Phone Number, Should be like this format 9665xxxxxxxxxx<br>قيمة غير صالحة لرقم الهاتف، ينبغي أن يكون مثل هذا الشكل 9665xxxxxxxxxx";
        else if(waslError.search("Date of Birth")!=-1)          
            document.getElementById("date_of_birthErr").innerHTML="Invalid values for Date of Birth, Should be like this format dd-MM-YYYY<br>قيم غير صالحة لتاريخ الميلاد، وينبغي أن يكون مثل هذا الشكل DD-MM-YYYY";
        break;
        case "503":
            document.getElementById("date_of_birthErr").innerHTML="Yakeen Failure: Captain Date Of Birth does not match Government Identity Number records<br>يقين الفشل: الكابتن تاريخ الميلاد لا تطابق سجلات الحكومة رقم الهوية";
        break;
        default:
            break;
    }
});
</script>
<script type="text/javascript">
function validateAllInputs(){
    var result = true;
    // Validating Vehicle Info
    var carModel  = document.getElementById("car_model");
	var carRegisterationExpiry  = document.getElementById("car_registration_expiry");
	var vehicleSequenceNumber = document.getElementById("vehicle_sequence_number");
	var carNumberPlateNumber = document.getElementById("car_number_plate_number");
    vehicleSequenceNumber.focus();
    if (carRegisterationExpiry.value==""){
		document.getElementById("car_registration_expiryErr").innerHTML = "*Please select Car Registeration Expiry Date.<br>(يرجى اختيار تسجيل السيارة تاريخ الانتهاء)";
		result = false;
    }else {
        document.getElementById("car_registration_expiryErr").innerHTML = "";
    }
    if (vehicleSequenceNumber.value==""){
		document.getElementById("vehicle_sequence_numberErr").innerHTML = "*Please enter Vehicle Sequence Number.<br>(الرجاء إدخال سيارة رقم تسلسل)";
		result = false;
    } else if (isNaN(vehicleSequenceNumber.value) ) {
		document.getElementById("vehicle_sequence_numberErr").innerHTML = "*Please enter Vehicle Sequence Number correctly.<br>(الرجاء إدخال سيارة رقم تسلسل بشكل صحيح)";
		result = false;
    } else {
		document.getElementById("vehicle_sequence_numberErr").innerHTML = "";
    }
    if (carNumberPlateNumber.value==""){
		document.getElementById("car_number_plate_numberErr").innerHTML = "*Please enter Car Plate Number.<br>(الرجاء إدخال رقم اللوحة السيارات)";
		result = false;
    } else if (isNaN(carNumberPlateNumber.value) ) {
		document.getElementById("car_number_plate_numberErr").innerHTML = "*Please enter Car Plate Number correctly(الرجاء إدخال رقم اللوحة السيارة بشكل صحيح)";
        result = false;
    } else {
		document.getElementById("car_number_plate_numberErr").innerHTML = "";
    }
	return result;
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@stop