<?php

$user = new Walker ();
$minimaldata = $userdata;
$names = explode(" ", $minimaldata[0]);



?>


        <!DOCTYPE html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">


    <!------------------------ CSSs and JSs ---------------------------------------------------------------------------------->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    {{--
        <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
    --}}

    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-amber.min.css"/>


    <link rel="stylesheet" href="/Dev-Server-Resources/mdl-stepper-master/stepper.min.css">
    <script src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <script src="/Dev-Server-Resources/mdl-stepper-master/stepper.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <link rel="stylesheet" href="../web/css/jquery.calendars.picker.css"/>


    <!---------------------------------------------------Styles-------------------------------->
    <style>
        .mdl-stepper-demo .mdl-stepper {
            margin: auto;
        }
    </style>

</head>


<script>
    //check if the user is browing form mobile device.....

  /*
    window.mobilecheck = function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    };*/



  function checkmobile() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
  }


/*
     if(!checkmobile ()){


         document.getElementById("stepper2_align").setAttribute("align","left");
         document.getElementById("stepper3_align").setAttribute("align","left");
         document.getElementById("stepper4_align").setAttribute("align","left");
         document.getElementById("stepper5_align").setAttribute("align","left");
         document.getElementById("stepper6_align").setAttribute("align","left");


     }

*/






</script>








<script>
    //global variables
    var firstname;
    var lastname;
    var mobile;
    var email;
    var password;
    var city;
    var govID;
    var BirthDate;
    var lExpiryDate;
    var carmodel;
    var RegisterationExpiry;
    var SequenceNumber;
    var PlateNumber;
    var PlateLetter1;
    var PlateLetter2;
    var PlateLetter3;
    /*
     image files are with this order:
     Personal Photo.
     ID photo copy.
     license copy.
     registry copy.
     car photo.
     */
    var docFiles = new Array();
    var docIDs = new Array("3","2","5","6","4");

</script>


<body>


<!------------------------------------ Layout --------------------------------------------------->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <div align="center"><h3 style="color: orange">Captain Registration Form</h3></div>
        </div>
    </div>


    <!--------------------------------- Layout-content --------------------------------------------------->
    <main class="mdl-layout__content mdl-color--grey-100">
        <div style="padding: 0px 10px;" class="mdl-grid demo-content">
            <div id="contractform" style="height:1100px; width: 1900px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">


                <!------------------------------------  Steppers  ---------------------------------------------------------------------------------->
                <section class="mdl-stepper-demo">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--12-col">
                            <!-- markup -->
                            {{-- non linear stepper--}}
                            {{--  <ul style="width: 1550px; height: 450px;"
                                  class="mdl-stepper mdl-stepper--feedback mdl-stepper--horizontal"
                                  id="demo-stepper-horizontal-linear-feedback">--}}

                            {{--linear stepper--}}
                            <ul style="width: 1800px; height: 800px; max-width: 1340px;"
                                class="mdl-stepper  mdl-stepper--horizontal"
                                id="demo-stepper-horizontal-linear-feedback">
                            <!--
                                <li class="mdl-step" data-step-transient-message="Step 2 looks great! Step 3 is coming up.">
            <span class="mdl-step__label">
          <span class="mdl-step__title">
            <span class="mdl-step__title-text">الشروط والاحكام</span>
            </span>
            </span>
                                    <div class="mdl-step__content">

                                        <div dir="rtl" lang="he" align="center">

                                           <b><h1 style="color: orange">شروط التعاقد مع الأفراد السعودين المؤهلين </h1></b>
                                            <p style="font-size: x-large; color: darkred">                                          - السائق
                                            <br>رخصة قيادة سارية المفعول
                                            <br>     الا يقل العمل عن 20 عاما و لا يزيد عن 60 عاما
                                            <br>    خلو صفحة الأدلة الجنائية من السوابق
                                            <br>              اجتياز اختبار فحص السموم
                                            <br>                ان يكون السائق الفعلي للمركبة
                                            <br>     اجتياز الدورة التدريبية على التعامل مع النظام
                                            <br> رخصة قيادة سارية المفعول
                                            </p>
                                            <p style="font-size: x-large; color: darkred">
                                                - المركبة
                                                <br>رخصة سير باسم السائق و مسجلة بنوع خصوصي
                                                <br>     وثيقة تأمين تغطي المسئولية المدنية تجاه الغير الخاصة بسيارات الأجرة
                                                <br>    الا يزيد العمل تلافتراضي عن خمس سنوات من بداية شهر يناير لسنة الصنع
                                                <br>              الا تقلسعة المحرك عن 2000 سم مكعب
                                                <br>               الا تقل المسافة بين مركزي المحورين عن 2,75 م
                                                <br>    فحص فني دوري ساري المفعول
                                            </p>
                                             <br>
                                            <b><h1 style="color: orange">شروط التعاقد مع المقيمين المؤهلين</h1></b>
                                            <p style="font-size: x-large; color: darkred">
العمل تحت كفالة شركة نقل خاص                                            </p>


                                        </div>
                                    </div>
                                    <div class="mdl-step__actions">
                                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                {{--data-stepper-next--}} >
                                            Continue
                                        </button>

                                    </div>
                                </li>
                                        -->

                                <li class="mdl-step"
                                    data-step-transient-message="Step 1 looks great! Step 2 is coming up.">
                                     <span class="mdl-step__label">
                                     <span class="mdl-step__title">
                                     <span class="mdl-step__title-text">معلومات شخصية</span>
                                     </span>
                                     </span>
                                    <!--Stepper Content-->
                                    <div class="mdl-step__content" id="demo-error-state-content">
                                        <!-- Form below -->

                                        <div class="content-panel">
                                            <form class="form-horizontal style-form" enctype="multipart/form-data">


                                                <input type="hidden" id="firstanme" value="{{ $names[0] }}"/>
                                                <input type="hidden" id="lastname" value="{{$names[1]}}"/>
                                                <input type="hidden" id="mobile" value="{{$minimaldata[1]}}"/>
                                                <input type="hidden" id="password" value="{{$minimaldata[3]}}"/>
                                                <input type="hidden" id="email" value="{{$minimaldata[2]}}"/>
                                                <input type="hidden" id="city" value="{{$minimaldata[4]}}"/>


                                                <div class="form-group">
                                                    <label class="col-sm-2 col-sm-2 control-label">Government ID
                                                        Number<br>(رقم الهوية الحكومة)</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="government_id_number"
                                                               name="government_id_number"
                                                               value="{{ $user->government_id_number }}"
                                                               maxlength="10"
                                                               placeholder='0000000000'>
                                                        <p id="government_id_numberErr" class="text-danger"></p>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 col-sm-2 control-label">Date of Birth<br>(تاريخ
                                                        الولادة)</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="date_of_birth" name="date_of_birth"
                                                               value="{{ $user->date_of_birth }}"
                                                               placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>
                                                        <p id="date_of_birthErr" class="text-danger"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 col-sm-2 control-label">License Expiry
                                                        Date<br>(تاريخ إنتهاء الرخصة)</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="license_expiry_date"
                                                               name="license_expiry_date"
                                                               value="{{ $user->license_expiry_date }}"
                                                               placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>
                                                        <p id="license_expiry_dateErr" class="text-danger"></p>
                                                    </div>
                                                </div>
                                                <span class="col-sm-2"></span>
                                            </form>
                                        </div>
                                    </div>


                                    <div class="mdl-step__actions">
                                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored" onclick="validateSteps(validateStepperOne()); resize(); ">
                                            Continue
                                        </button>


                                    </div>
                                </li>
                                <li class="mdl-step"
                                    data-step-transient-message="Step 2 looks great! Step 3 is coming up.">
                                      <span class="mdl-step__label">
                                      <span class="mdl-step__title">
                                     <span class="mdl-step__title-text">صورة شخصية</span>
                                      </span>
                                      </span>
                                    <div class="mdl-step__content">

                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                        <div id="stepper2_align" align="center">
                                            <form id="stepper2_form" action="" method="post"
                                                  enctype="multipart/form-data">
                                                <div id="stepper2_image_preview"><img id="stepper2_image"
                                                                                      src="/web/default_profile.png"/>
                                                </div>
                                                <hr id="stepper2_line">
                                                <div id="stepper2_selectImage">
                                                    <label>Select Your Photo</label><br/>
                                                    <input type="file" name="stepper2_file" id="stepper2_file" required
                                                           onchange="if(validateandPreview('2')) {$('#stepper2').prop('disabled',false);}else{$('#stepper2').prop('disabled',true);}"/>


                                                </div>
                                            </form>

                                            <div id="stepper2_message"></div>
                                        </div>
                                    </div>
                                    <div class="mdl-step__actions">
                                        <button id="stepper2"
                                                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                disabled onclick="validateSteps(true);"
                                        >
                                            Continue
                                        </button>

                                    </div>
                                </li>

                                <li class="mdl-step" data-step-transient-message="Step 3 looks great! Thanks.">
                                   <span class="mdl-step__label">
                                    <span class="mdl-step__title">
                                   <span class="mdl-step__title-text">الاقامة/الهوية</span>
                                   </span>
                                    </span>
                                    <div class="mdl-step__content">

                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                        <div id="stepper3_align" align="center">


                                            <form id="stepper3_form" action="" method="post"
                                                  enctype="multipart/form-data">
                                                <div id="stepper3_image_preview"><img id="stepper3_image"
                                                                                      src="/web/Doc-Examples/ahwal-card.jpg"/>
                                                </div>
                                                <hr id="stepper3_line">
                                                <div id="stepper3_selectImage">
                                                    <label>Select Your Photo/label><br/>
                                                        <input type="file" name="file" id="stepper3_file" required
                                                               onchange="if(validateandPreview('3')) {$('#stepper3').prop('disabled',false);}else{$('#stepper2').prop('disabled',true);}"/>
                                                    </label>
                                                </div>
                                            </form>


                                            <div id="stepper3_message"></div>
                                        </div>
                                    </div>
                                    <div class="mdl-step__actions">
                                        <button id="stepper3"
                                                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                disabled onclick="validateSteps(true);"
                                        >
                                            Continue
                                        </button>

                                    </div>
                                </li>

                                <li class="mdl-step" data-step-transient-message="Step 4 looks great! Thanks.">
                                   <span class="mdl-step__label">
                                   <span class="mdl-step__title">
                                   <span class="mdl-step__title-text">الرخصة</span>
                                   </span>
                                   </span>
                                    <div class="mdl-step__content">

                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                        <div id="stepper4_align" align="center">


                                            <form id="stepper4_form" action="" method="post"
                                                  enctype="multipart/form-data">
                                                <div id="stepper4_image_preview"><img id="stepper4_image"
                                                                                      src="/web/Doc-Examples/Saudi_licene_front-copy.jpg"/>
                                                </div>
                                                <hr id="stepper4_line">
                                                <div id="stepper4_selectImage">
                                                    <label>Select Your Photo</label><br/>
                                                    <input type="file" name="file" id="stepper4_file" required
                                                           onchange="if(validateandPreview('4')) {$('#stepper4').prop('disabled',false);}else{$('#stepper2').prop('disabled',true);}"/>
                                                </div>
                                            </form>


                                            <div id="stepper4_message"></div>
                                        </div>
                                    </div>
                                    <div class="mdl-step__actions">
                                        <button id="stepper4"
                                                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                disabled onclick="validateSteps(true);"
                                        >
                                            Continue
                                        </button>

                                    </div>
                                </li>

                                <li class="mdl-step" data-step-transient-message="Step 5 looks great! Thanks.">
                                      <span class="mdl-step__label">
                                      <span class="mdl-step__title">
                                      <span class="mdl-step__title-text">الاستمارة</span>
                                      </span>
                                      </span>


                                    <style>
                                        label {
                                            display: inline-block;
                                            width: 150px;
                                            text-align: right;
                                            font-size: small;
                                        }
                                    </style>


                                    <div class="mdl-step__content">

                                        <div id="stepper5_align" align="center">

                                            <div class="form-group">
                                                <label>Car Model<br>(طراز المركبة)</label>
                                                <input type="text" id="car_model"
                                                       name="car_model">

                                                <p id="car_modelErr" class="text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Vehicle Sequence Number<br>(الرقم التسلسلي للسيارة)</label>
                                                <input type="text"
                                                       id="vehicle_sequence_number"
                                                       name="vehicle_sequence_number"
                                                       placeholder='0000000000' maxlength="10">
                                                <p id="vehicle_sequence_numberErr" class="text-danger"></p>
                                            </div>

                                            <div class="form-group">
                                                <label>Car Registration Expiry<br>(تاريخ إنتهاء الإستمارة)</label>
                                                <input type="text"
                                                       id="car_registration_expiry"
                                                       name="car_registration_expiry"
                                                       placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>
                                                <p id="car_registration_expiryErr" class="text-danger"></p>
                                            </div>


                                            <div class="form-group">
                                                <label>Car Number Plate<br>(رقم لوحة السيارة)</label>
                                                <input type="text"
                                                       id="vehicle_plate_number"
                                                       name="vehicle_plate_number" placeholder='0000ابج' maxlength="7">
                                                <p id="car_number_plate_numberErr" class="text-danger"></p>
                                            </div>


                                            <form id="stepper5_form" action="" method="post"
                                                  enctype="multipart/form-data">
                                                <div id="stepper5_image_preview"><img id="stepper5_image"
                                                                                      src="/web/Doc-Examples/Vehicle-license1.jpg"/>
                                                </div>
                                                <hr id="stepper5_line">
                                                <div id="stepper5_selectImage">
                                                    <label>Select Your Photo</label><br/>
                                                    <input type="file" name="file" id="stepper5_file" required
                                                           onchange="if(validateandPreview('5')) {$('#stepper5').prop('disabled',false);}else{$('#stepper2').prop('disabled',true);}"/>
                                                </div>
                                            </form>


                                            <div id="stepper5_message"></div>
                                        </div>
                                    </div>

                                    <div class="mdl-step__actions">
                                        <button id="stepper5"
                                                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                onclick="validateSteps(validateStepperFive())" disabled
                                        >
                                            Continue
                                        </button>

                                    </div>
                                </li>
                                <li class="mdl-step" data-step-transient-message="Step 5 looks great! Thanks.">
            <span class="mdl-step__label">
          <span class="mdl-step__title">
            <span class="mdl-step__title-text">السيارة</span>
            </span>
            </span>
                                    <div class="mdl-step__content">


                                        <br>
                                        <br>
                                        <br>
                                        <br>


                                        <div id="stepper6_align" align="center">


                                            <form id="stepper6_form" action="" method="post"
                                                  enctype="multipart/form-data">
                                                <div id="stepper6_image_preview"><img id="stepper6_image"
                                                                                      src="/web/car.png"/>
                                                </div>
                                                <hr id="stepper6_line">
                                                <div id="stepper6_selectImage">
                                                    <label>Select Your Image</label><br/>
                                                    <input type="file" name="file" id="stepper6_file" required
                                                           onchange="if(validateandPreview('6')) {$('#stepper6').prop('disabled',false);}else{$('#stepper6').prop('disabled',true);}"/>
                                                </div>
                                            </form>


                                            <div id="stepper6_message"></div>
                                        </div>
                                    </div>
                                    <div class="mdl-step__actions">
                                        <button id="stepper6"
                                                class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                disabled onclick="validateSteps(true); finalize();"
                                        >
                                            Continue
                                        </button>

                                    </div>
                                </li>

                                               <li class="mdl-step" data-step-transient-message="Step 5 looks great! Thanks.">
                           <span class="mdl-step__label">
                         <span class="mdl-step__title">
                           <span class="mdl-step__title-text">المقابلة الشخصية</span>
                           </span>
                           </span>
                                                   <div class="mdl-step__content">

                                                       <div id="stepper7_align" align="center">

                                                           <h5 style="color: orange">لقد تم عملية التسجيل بنجاح الرجاء مراجعة المسئول</h5>
                                                               <h5 style="color: orange">+966 536767272</h5>
                                                       </div>
                                                       </div>
                                                   <div class="mdl-step__actions">
                                                       <!--<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
                                                               data-stepper-next>
                                                           Continue
                                                       </button>
 -->
                                                   </div>
                                               </li>

                            </ul>
                        </div>
                    </div>
                </section>
                <!------------------------------------  Steppers  ---------------------------------------------------------------------------------->
            </div>
    </main>
</div>

<!--  photo upload, validate, preview and function -->
<script>




    function resize() {



        if(checkmobile()){

            document.getElementById("stepper2_align").setAttribute("align","left");
            document.getElementById("stepper3_align").setAttribute("align","left");
            document.getElementById("stepper4_align").setAttribute("align","left");
            document.getElementById("stepper5_align").setAttribute("align","left");
            document.getElementById("stepper6_align").setAttribute("align","left");
            document.getElementById("stepper7_align").setAttribute("align","left");

        }

    }





    //OCR stuff goes here
    function analyzePhoto() {


    }

             /*
             here we have ajax post request "/provider/uploadphoto"   must send type and the doc url.
             */
    //parm doc/photo type general function.
    function populateDoc(doc,docid,walkerid) {


      var formData = new FormData();
        formData.append('file', doc);
        formData.append('docid', docid);
        formData.append('walkerid', walkerid);


        $.ajax({
            url: "uploaddoc",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                console.log(walkerid);

            }
        });
    }


    function validateandPreview(id) {


        $("#stepper" + id + "_message").empty();

        var file = document.getElementById("stepper" + id + "_file").files[0];

        var imagefile = file.type;


        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            $('#stepper' + id + '_image_preview').attr('src', 'noimage.png');
            $("#stepper" + id + "_message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        }/* else if (file.size > 50000) {
            $("#stepper" + id + "_message").html("<p id='error'>Please Select A valid Image Size</p>" + "<h4>Note</h4>" + "<span id='error_message'>Maximum 10mg allowed</span>");
            return false;
        }*/
        else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            docFiles.push(file);
            reader.readAsDataURL(file);
            return true;
        }

        function imageIsLoaded(e) {
            $('#stepper' + id + '_file').css("color", "green");
            $('#stepper' + id + '_image_preview').css("display", "block");
            $('#stepper' + id + '_image').attr('src', e.target.result);
            $('#stepper' + id + '_image').attr('width', '250px');
            $('#stepper' + id + '_image').attr('height', '230px');


            return true;


        };

    }
</script>


<!--- Stepper Validation Test -->
<script>
    function validateSteps(result) {

        var selector = '.mdl-stepper#demo-stepper-horizontal-linear-feedback';
        // Select stepper container element
        var stepperElement = document.querySelector(selector);

        // Get the MaterialStepper instance of element to control it.
        Stepper = stepperElement.MaterialStepper;

        if (result) {
            Stepper.next();
        }
    }

</script>


<!-- Steppers Script  "change needed below" -->
<script>
    (function () {
        var stepperHorizontal = function () {
            var selector = '.mdl-stepper#demo-stepper-horizontal-linear-feedback';
            // Select stepper container element
            var stepperElement = document.querySelector(selector);

            var steps;
            var inputTransientMessage /** @type {HTMLElement} */;
            if (!stepperElement) return;

            // Get the MaterialStepper instance of element to control it.
            Stepper = stepperElement.MaterialStepper;

            if (!Stepper) {
                console.error('MaterialStepper instance is not available for selector: ' + selector + '.');
                return;
            }
            steps = stepperElement.querySelectorAll('.mdl-step');
            for (var i = 0; i < steps.length; i++) {

                // When user clicks on [data-stepper-next] button of step.
                steps[i].addEventListener('onstepnext', (function (e, step) {
                    return function () {
                        // {element}.MaterialStepper.next() change the state of current step to "completed"
                        // and move one step forward.
                        inputTransientMessage = step.querySelector('#stepper-transient-message');

                        if (inputTransientMessage && inputTransientMessage.value.length) {
                            step.setAttribute('data-step-transient-message', inputTransientMessage.value);
                        }
                        setTimeout(function () {
                            Stepper.next();
                        }, 3000);
                    };
                })(event, steps[i]));
            }
            // When all steps are completed this event is dispatched.
            stepperElement.addEventListener('onsteppercomplete', function (e) {
                var toast = document.querySelector('#snackbar-stepper-complete');
                if (!toast) return;
                toast.MaterialSnackbar.showSnackbar({
                    message: 'Stepper horizontal + feedback are completed',
                    timeout: 4000,
                    actionText: 'Ok'
                });
            });
        };
        window.addEventListener('load', stepperHorizontal)
    })();
</script>

<script src="../web/js/hijricalender/jquery.plugin.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>

<script>
    $(function () {
        var calendar = $.calendars.instance('ummalqura');
        $('#date_of_birth').calendarsPicker({
            calendar: calendar, onSelect: function (date) {
                $('#date_of_birth').val(date[0].formatDate('dd-mm-yyyy'));
            }
        });
        $('#license_expiry_date').calendarsPicker({
            calendar: calendar, onSelect: function (date) {
                $('#license_expiry_date').val(date[0].formatDate('dd-mm-yyyy'));
            }
        });
        var governmentIdNumber = document.getElementById("government_id_number");
        var phone = document.getElementById("phone");
        if (governmentIdNumber.value == "0") {
            governmentIdNumber.value = "";
        }


        var waslresultCode = '<?php if (Session::has('waslresultCode')) echo Session::get('waslresultCode');?>';
        var waslError = '<?php if (Session::has('error')) echo Session::get('error');?>';
        switch (waslresultCode) {
            case "421":
                if (waslError.search("Captain Identity Number") != -1)
                    document.getElementById("government_id_numberErr").innerHTML = "Invalid values at Captain Government Identity Number<br>قيم غير صالحة في الكابتن الحكومة رقم الهوية";
                else if (waslError.search("Mobile Number") != -1)
                    document.getElementById("phoneErr").innerHTML = "Invalid value of Phone Number, Should be like this format 9665xxxxxxxxxx<br>قيمة غير صالحة لرقم الهاتف، ينبغي أن يكون مثل هذا الشكل 9665xxxxxxxxxx";
                else if (waslError.search("Date of Birth") != -1)
                    document.getElementById("date_of_birthErr").innerHTML = "Invalid values for Date of Birth, Should be like this format dd-MM-YYYY<br>قيم غير صالحة لتاريخ الميلاد، وينبغي أن يكون مثل هذا الشكل DD-MM-YYYY";
                break;
            case "503":
                document.getElementById("date_of_birthErr").innerHTML = "Yakeen Failure: Captain Date Of Birth does not match Government Identity Number records<br>يقين الفشل: الكابتن تاريخ الميلاد لا تطابق سجلات الحكومة رقم الهوية";
                break;
            default:
                break;
        }
    });
</script>
<script type="text/javascript">


    function validateStepperOne() {
        var result = true;
        var governmentIdNumber = document.getElementById("government_id_number");
        var dateOfBirth = document.getElementById("date_of_birth");
        var licenseExpiryDate = document.getElementById("license_expiry_date");


        if (governmentIdNumber.value == "") {
            document.getElementById("government_id_numberErr").innerHTML = "*Please enter your Government Id Number.<br>(الرجاء إدخال حكومتكم إيد عدد)";
            result = false;
        } else if (isNaN(governmentIdNumber.value)) {
            document.getElementById("government_id_numberErr").innerHTML = "*Please enter your Government Id Number correctly.<br>(الرجاء أدخل الحكومة عدد رقم صحيح)";
            result = false;
        } else if (governmentIdNumber.value.length < 10) {
            document.getElementById("government_id_numberErr").innerHTML = "*Please enter your Government Id Number correctly.<br>(الرجاء أدخل الحكومة عدد رقم صحيح)";
            result = false;
        }
        else {
            document.getElementById("government_id_numberErr").innerHTML = "";
        }
        if (dateOfBirth.value == "") {
            document.getElementById("date_of_birthErr").innerHTML = "*Please select Date of Birth.<br>(يرجى تحديد تاريخ الميلاد)";
            result = false;
        } else {
            document.getElementById("date_of_birthErr").innerHTML = "";
        }

        if (licenseExpiryDate.value == "") {
            document.getElementById("license_expiry_dateErr").innerHTML = "*Please select License Expiry Date.<br>(يرجى تحديد الترخيص تاريخ انتهاء الصلاحية)";
            result = false;
        } else {
            document.getElementById("license_expiry_dateErr").innerHTML = "";
        }


        if (result) {

            firstname = $("#firstanme").val();
            lastname = $("#lastname").val();
            mobile = $("#mobile").val();
            email = $("#email").val();
            password = $("#password").val();
            city = $("#city").val();
            govID = governmentIdNumber.value;
            BirthDate = dateOfBirth.value;
            lExpiryDate = licenseExpiryDate.value;


            console.log("fisrt name " + firstname);
            console.log("lastname" + lastname);
            console.log("mobile" + mobile);
            console.log("email" + email);
            console.log("password" + password);
            console.log("city" + city);
            console.log("gov id" + govID);
            console.log("birth date" + BirthDate);
            console.log("expiry date" + lExpiryDate);


        }


        return result;
    }

    function validateStepperFive() {

        var result = true;


        var carModel = document.getElementById("car_model");
        var carRegisterationExpiry = document.getElementById("car_registration_expiry");
        var vehicleSequenceNumber = document.getElementById("vehicle_sequence_number");
        var carNumberPlateNumber = document.getElementById("vehicle_plate_number");


        if (carModel.value == "") {

            document.getElementById("car_modelErr").innerHTML = "*Please select Car Registeration Expiry Date.<br>(يرجى اختيار تسجيل السيارة تاريخ الانتهاء)";
            result = false;

        } else {
            document.getElementById("car_modelErr").innerHTML = "";
        }


        if (carRegisterationExpiry.value == "") {
            document.getElementById("car_registration_expiryErr").innerHTML = "*Please select Car Registeration Expiry Date.<br>(يرجى اختيار تسجيل السيارة تاريخ الانتهاء)";
            result = false;
        } else {
            document.getElementById("car_registration_expiryErr").innerHTML = "";
        }
        if (vehicleSequenceNumber.value == "") {
            document.getElementById("vehicle_sequence_numberErr").innerHTML = "*Please enter Vehicle Sequence Number.<br>(الرجاء إدخال سيارة رقم تسلسل)";
            result = false;
        } else if (isNaN(vehicleSequenceNumber.value)) {
            document.getElementById("vehicle_sequence_numberErr").innerHTML = "*Please enter Vehicle Sequence Number correctly.<br>(الرجاء إدخال سيارة رقم تسلسل بشكل صحيح)";
            result = false;
        } else if (vehicleSequenceNumber.value.length != 9) {
            document.getElementById("vehicle_sequence_numberErr").innerHTML = "*Please enter Vehicle Sequence Number correctly.<br>(الرجاء إدخال سيارة رقم تسلسل بشكل صحيح)";
            return false;
        }
        else {
            document.getElementById("vehicle_sequence_numberErr").innerHTML = "";
        }
        if (carNumberPlateNumber.value == "") {
            document.getElementById("car_number_plate_numberErr").innerHTML = "*Please enter Car Plate Number.<br>(الرجاء إدخال رقم اللوحة السيارات)";
            result = false;
        } /*else if (isNaN(carNumberPlateNumber.value) ) {
         document.getElementById("car_number_plate_numberErr").innerHTML = "*Please enter Car Plate Number correctly(الرجاء إدخال رقم اللوحة السيارة بشكل صحيح)";
         result = false;
         }*/ else if (carNumberPlateNumber.value.length != 7) {
            document.getElementById("car_number_plate_numberErr").innerHTML = "*Please enter Car Plate Number correctly(الرجاء إدخال رقم اللوحة السيارة بشكل صحيح)";
            return false;
        }
        else {
            document.getElementById("car_number_plate_numberErr").innerHTML = "";
        }

        if (result) {
            carmodel = carModel.value;
            RegisterationExpiry = carRegisterationExpiry.value;
            SequenceNumber = vehicleSequenceNumber.value;
            PlateNumber = carNumberPlateNumber.value;

            console.log("Registration expiray: " + RegisterationExpiry);
            console.log("SequenceNumber: " + SequenceNumber);
            console.log("PlateNumber: " + PlateNumber);




            var splits= PlateNumber.split("");
             PlateNumber = splits[0]+splits[1]+splits[2]+splits[3];
             PlateLetter1 = splits[4];
             PlateLetter2 = splits[5];
             PlateLetter3 = splits[6];
        }

        return result;


    }


    function finalize() {

        /*
         1- create user.
         2- get user id.
         2- loop over docs and use ajax to upload them given the walker id.
         */

        console.log("finalizing.......");

        $.ajax({
            type: 'POST',
            url: 'newwalker',
            dataType: 'text',
            data: {
                firstname: firstname,
                lastname: lastname,
                mobile: mobile,
                email: email,
                password: password,
                city: city,
                govID: govID,
                BirthDate: BirthDate,
                lExpiryDate: lExpiryDate,
                carmodel: carmodel,
                RegisterationExpiry: RegisterationExpiry,
                SequenceNumber: SequenceNumber,
                PlateNumber: PlateNumber,
                PlateLetter1: PlateLetter1,
                PlateLetter2: PlateLetter2,
                PlateLetter3: PlateLetter3
            }

        }).done(function (returns) {
           //what is wrong with this request?
            //alert(returns);

            for(var x=0; x<5; x++){
                populateDoc(docFiles[x],docIDs[x],returns);
            }

            validateSteps(true);


        });// Ajax Call

    }


</script>


</body>
</html>