<?php
//echo "to do for admin panel";

/*
 1. Get all orders from the database.
 2. Databases. can sort by date.
 3. Ability to change order status to: cancel, delivered, 'cash collected'. -1, 2,3. need to check for integers.
 4. show on the map when click order route.
 5.
*/

/*
 1. Add tracking service only after testing it on live server.
 2. Try to add auto-refresh status, instead of button.
 3. Include cash collected status in wallet view.
 4. Test with captain the new app update after asking for it from Umar.
 5.
 */

//
//$data = null;
//echo "size of orders" . sizeof($orders);
//echo "\r\n";
//echo "size of companies" . sizeof($companies);
//echo "\r\n";
//echo "size of statuses" . sizeof($statuses);
//echo "\r\n";
//echo "size of walkers" . sizeof($walkers);

//print_r ($statuses);
//print_r ($companies);
//print_r ($orders);

?>




<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>


    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">


    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.cyan-light_blue.min.css">
      -->
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css"/>
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles2.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/stylesarticle.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">


    <style>
        #map {
            width: 100%;
            height: 25%;
        }
    </style>


</head>
<body>


<script>


</script>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <h3 style="color: orange">Delivery Orders</h3>
        </div>
    </div>

    <main class="mdl-layout__content mdl-color--grey-100">
        <div style="padding: 0px 10px;" class="mdl-grid demo-content">

            <div id="contractform" style="height:1600px; width: 1800px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                <br>
                <br>

                <table>
                </table>
                <!--Two maps for picking Home ans Work locations-->
                <div align="center" id="map"></div>

                <br>


                <!-- Echos to table structure-->
                <?php

                //array_multisort($data, SORT_ASC);
                echo " <table id='example' class='display' cellspacing='0' width='100%'>  <thead>
            <tr>

                <th>ID</th>  <!-- 0-->
                <th>Company</th>   <!-- 1-->
                <th>Mobile</th>  <!-- 2-->
                <th>d_lat</th>  <!-- 3-->
                <th>d_lng </th>  <!-- 4-->
                <th>o_lat</th>  <!-- 5-->
                <th>o_lng</th>  <!-- 6-->
                <th>Customer Name</th> <!--7-->
                <th>Customer Mobile</th> <!--8-->
                <th>Cash On Delivery</th> <!--9-->
                <th>Fees Payed By</th> <!--10-->
                <th>Captain</th>  <!-- 11-->
                <th>Status</th>  <!-- 12-->

              </tr>
        </thead>
        ";


                $i = 0;
                foreach ($orders as $order) {


                    echo "<tr>";


                    //contract id
                    echo "<td>" . $order->id . "</td>";


                      if($companies[$i]){
                    
                     echo "<td>" . $companies[$i]->name ."</td>";
                     echo "<td>" . $companies[$i]->phone ."</td>";

                    } else 
                    {
                     echo "<td>" ."none"."</td>";
                     echo "<td>" ."none"."</td>";;
                     
                     }


                    
                    //pickup
                    echo "<td>" . $order->D_latitude . "</td>";
                    echo "<td>" . $order->D_longitude . "</td>";

                    //dropoff
                    echo "<td>" . $order->origin_latitude . "</td>";
                    echo "<td>" . $order->origin_longitude . "</td>";

                    //customer info
                    echo "<td>" . $order->receiver_name . "</td>";
                    echo "<td>" . $order->receiver_phone . "</td>";

                    //cash on delivery
                    echo "<td>" . $order->cash_on_delivery . "</td>";

                    //fees payed by
                    echo "<td>" . $order->transportation_fees_payed_by . "</td>";

                    echo "<td>" . $walkers[$i] . "</td>";
                    echo "<td>" . $statuses[$i] . "</td>";


                    echo "</tr>";
                    $i++;
                }
                echo "</table>";

                ?>
                <div>



                    <br> <br>
                    <button style="color: blue" id="delivered"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(3)">
                        Delivered
                    </button>

                    <button style="color: yellowgreen" id="cashcollected"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(4)">
                        Cash Collected
                    </button>

                    <button style="color: orange" id="canceled"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus(-1)">
                        Canceled
                    </button>

                </div>
    </main>
</div>


<script>

    function updateStatus(status) {

        var data = table.row('.selected').data();
        console.log("TripID " + data[0]);
        table.row('.selected').data(data);



        if(status == -1){

            data[12] = "Canceled";
            table.row('.selected').data(data);

        }else if(status == 3){

            data[12] = "Delivered";
            table.row('.selected').data(data);

        }else if(status == 4){

            data[12] = "Cash Collected";
            table.row('.selected').data(data);

        }


        console.log("Status"+ status);


        $.ajax({
            type: 'POST',
            url: 'customerservice/updatestatus',
            dataType: 'text',
            data: {id: data[0], status: status},
        }).done(function (response) {
            alert(response + " Status Updated");
        }); // Ajax Call

    }

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- DataTables Library -->
<script src="/Dev-Server-Resources/tableassets/jquery.js"></script>
<script src="/Dev-Server-Resources/tableassets/jquery.datatables.js"></script>
<!-- DataTables Search Script  -->
<script src="/Dev-Server-Resources/tableassets/fnFindCellRowIndexes.js"></script>



<!-- jstables script -->
<script>

    //datatables methods
    $(document).ready(function () {
        //#1 method hide columns

        $('#example').DataTable({

            "columnDefs": [

                {
                    "targets": [3],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [4],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [5],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [6],
                    "visible": false,
                    "searchable": false
                }
            ],
            dom: 'Bfrtip',
            colReorder: true,

            buttons: [
                'excel'
            ],
            "iDisplayLength": 20
        });


        //#2 Method: Row Selections.
        table = $('#example').DataTable();


        $('#example tbody').on('click', 'tr', function () {


            var data = table.row(this).data();


            //call display route here:

            var StartLocation = new google.maps.LatLng(data[3], data[4]);
            var EndLocation = new google.maps.LatLng(data[5], data[6]);


            displayRoute(StartLocation, EndLocation, directionsService,
                directionsDisplay);


        });

        //logic for row selection.
        $('#example tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });


    });


</script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- PDF file generater -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>




<!-- map script -->
<script>

    var map;
    var FromAddress;
    var ToAddress;
    var doc;
    var directionsService;
    var directionsDisplay;
    var driverMarkers = [];

    function initMap() {
        var destination_place_id = null;
        var travel_mode = 'DRIVING';
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: 21.529, lng: 39.179}, //Jeddah
            // center: {lat: 24.713, lng: 46.675}, //Capital City
            zoom: 5
        });

        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: false,
            map: map
        });


        directionsDisplay.addListener('directions_changed', function () {


            var changedorigin = directionsDisplay.directions.routes[0].legs[0].end_location;

            console.log(changedorigin.lat());
            console.log(changedorigin.lng());


        });


        var StartLocation = new google.maps.LatLng(24.789095, 46.733349);
        var EndLocation = new google.maps.LatLng(24.789095, 46.733349);

        //      var EndLocation = new google.maps.LatLng(21.548977, 39.149628);

        //displayRoute(StartLocation, EndLocation , directionsService,
        //  directionsDisplay);


    }

    function displayRoute(origin, destination, service, display) {
        clearMarkers();

        service.route({
            origin: origin,
            destination: destination,
            travelMode: 'DRIVING',
            avoidTolls: true
        }, function (response, status) {
            if (status === 'OK') {
                display.setDirections(response);

                // console.log(response.routes[0].legs[0].start_address);

                FromAddress = response.routes[0].legs[0].start_address;


                // console.log(response.routes[0].legs[0].end_address);

                ToAddress = response.routes[0].legs[0].end_address


            } else {
                alert('Could not display directions due to: ' + status);
            }
        });
    }








    //clear/delete markers from the map.
    function clearMarkers() {

        for (var x = 0; x < driverMarkers.length; x++) {

            driverMarkers[x].setMap(null);
        }
        driverMarkers = [];


    }


</script>
<!-- MDL Linbraries -->
<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>

<!-- load google API-->
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places&callback=initMap"
        async defer>
</script>


</body>


</html>


