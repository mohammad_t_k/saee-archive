<?php


?>


<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>


    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">




    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css"/>
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles2.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/stylesarticle.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles.css">



</head>
<body>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <div align="center">
                <h3 style="color: orange">Customer Service Panel</h3>
            </div>
        </div>
    </div>

    <main class="mdl-layout__content mdl-color--grey-100">
        <div  style="padding: 0px 10px;" class="mdl-grid demo-content">

            <div id="contractform" style="height:600px; width: 1800px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">


                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>



                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <button style="width: 150px; height: 150px; border: thin; border-color:orange;" id="unreachable"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            onclick="window.location.href = '{{ URL::Route('showWalkers')}}'" >
                        <span style="color: orange; font-size: x-large">الكباتن</span>
                        <i style="color: orange" class="material-icons">people</i>

                    </button>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button style="width: 150px; height: 150px;  border: thin; border-color:dodgerblue;   " id="contractsent"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" disabled onclick="window.location.href = '{{ URL::Route('showContracts')}}'">
                         <span style="font-size: x-large; color: dodgerblue">العقود</span>
                        <i style="color: dodgerblue" class="material-icons">content_copy</i>
                    </button>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button style="width: 150px; height: 150px;  border: thin; border-color:yellowgreen;   color: orange" id="contractsent"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" disabled onclick="window.location.href='{{URL::Route('newContract')}}'">
                         <span style="font-size: x-large; color: yellowgreen">عقد جديد</span>
                        <i style="color: yellowgreen" class="material-icons">add</i>
                    </button>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <button style="width: 150px; height: 150px;   border: thin; border-color:red;" id="contractsent"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                        type="submit" disabled onclick="route()">
                    <span style="font-size: large; color: red">توصيل</span>
                    <i style="color: saddlebrown" class="material-icons">local_shipping</i>
                </button>
            </div>
    </main>
</div>



<!-- MDL Linbraries -->
<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
</body>
</html>
