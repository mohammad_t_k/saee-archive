<?php

echo "this page is under developement not all functions are working properly";


/* 1. view all contracts from a specific date.
2. view all approved walkers with a label specificity online/offline status.
3. view monthly contract status. after notifying the contract to walkers in case of accepted or rejected.
*/

//echo "this page is under development";
$data = null;

echo "size of contracts" . sizeof($contracts);
echo "\r\n";
echo "size of users" . sizeof($users);
echo "\r\n";
echo "size of statuses" . sizeof($statuses);

print_r ($statuses);


?>


<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>


    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>


    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">


    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.cyan-light_blue.min.css">
      -->
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css"/>
    <link rel="stylesheet" href="/kc/public/Dev-Server-Resources/new-css/styles2.css">
    <link rel="stylesheet" href="/kc/public/Dev-Server-Resources/new-css/stylesarticle.css">
    <link rel="stylesheet" href="/kc/public/Dev-Server-Resources/new-css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <style>
        #map {
            width: 100%;
            height: 25%;
        }
    </style>


</head>
<body>


<script>


</script>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <div class=" mdl-layout__header mdl-layout__header--waterfall">
        <div style="background-color: white" class="mdl-layout__header-row">
            <h3 style="color: orange">Contracts</h3>
        </div>
    </div>

    <main class="mdl-layout__content mdl-color--grey-100">
        <div style="padding: 0px 10px;" class="mdl-grid demo-content">

            <div id="contractform" style="height:1600px; width: 1800px; padding: 0px 10px;"
                 class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">

                <br>
                <br>

                <table>
                </table>
                <!--Two maps for picking Home ans Work locations-->
                <div align="center" id="map"></div>

                <br>


                <?php

                //array_multisort($data, SORT_ASC);
                echo " <table id='example' class='display' cellspacing='0' width='100%'>  <thead>
            <tr>

                <th>ID</th>  <!-- 0-->
                <th>Name</th>   <!-- 1-->
                <th>Email</th>  <!-- 2-->
                <th>Mobile</th>  <!-- 3-->

                <th>home_lat</th>  <!-- 4-->
                <th>home_lng </th>  <!-- 5-->
                <th>home address</th><!-- 6-->


                <th>work_lat</th>  <!-- 7-->
                <th>work_lng</th>  <!-- 8-->
                <th>work address</th> <!--9-->



                <th>Going Time</th>  <!-- 10-->
                <th>Return Time</th>  <!-- 11-->



                <th>extra_lat</th>  <!-- 12-->
                <th>extra_lng</th>  <!-- 13-->
                <th>extra address</th>> <!-- 14-->


                <th>KM</th>  <!-- 15-->
                <th>Min</th>   <!-- 16-->


                <th>Days</th>  <!-- 17-->
                <th>Cost</th>  <!-- 18-->
                <th>Request Date</th>  <!-- 19-->
                <th>Starting Date</th>   <!-- 20-->
                <th>Employer</th>   <!-- 21-->
                <th>Type</th>  <!-- 22-->

                <th>Service Status</th>  <!-- 23-->
                <th>Contract Status</th>  <!-- 23-->

              </tr>
        </thead>
        ";



                $i = 0;
                foreach ($contracts as $contract) {


                    echo "<tr>";


                    //contract id
                    echo "<td>" . $contract->id . "</td>";
                    echo "<td>" . $users[$i]->first_name . " " . $users[$i]->last_name . "</td>";

                    echo "<td>" . $users[$i]->email . "</td>";
                    echo "<td>" . $users[$i]->phone . "</td>";

                    //Trip one.
                    echo "<td>" . $contract->trip_one_from_lat . "</td>";
                    echo "<td>" . $contract->trip_one_from_long . "</td>";
                    echo "<td>" . $contract->trip_one_from_address . "</td>";


                    echo "<td>" . $contract->trip_one_to_lat . "</td>";
                    echo "<td>" . $contract->trip_one_to_long . "</td>";
                    echo "<td>" . $contract->trip_one_to_address . "</td>";


                    echo "<td>" . $contract->trip_one_pickup_time . "</td>";
                    echo "<td>" . $contract->trip_two_pickup_time . "</td>";


                    echo "<td>" . $contract->extra_lat . "</td>";
                    echo "<td>" . $contract->extra_lng . "</td>";
                    echo "<td>" . $contract->extra_address . "</td>";

                    echo "<td>" . $contract->distance . "</td>";
                    echo "<td>" . $contract->duration . "</td>";

                    echo "<td>" . $contract->days . "</td>";
                    echo "<td>" . $contract->cost . "</td>";


                    echo "<td>" . $contract->created_at . "</td>";
                    echo "<td>" . $contract->starting_date . "</td>";

                    echo "<td>" . $contract->employer . "</td>";

                    if ($contract->subscription_type == 1) {

                        echo "<td>" . "Private" . "</td>";
                    } else if ($contract->subscription_type == 2) {
                        echo "<td>" . "Shared" . "</td>";

                    } else {
                        echo "<td>" . "Unknown Subscription " . "</td>";
                    }

                    echo "<td>" . $contract->cs_status . "</td>";
                    echo "<td>" . $statuses[$i] . "</td>";


                    echo "</tr>";
                    $i++;
                }
                echo "</table>";

                ?>
                <div>


                    <button style="color: blue" id="finddriver"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="findDriver()">
                        Show Drivers on the map
                    </button>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <button style="color: orangered" id="notifynearestdrivers"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="findDriver()">
                       Notify Nearest Drivers
                    </button>
                      <br> <br>
                    <button style="color: blue" id="called"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Called')">
                        Called
                    </button>

                    <button style="color: yellowgreen" id="unreachable"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Unreachable')">
                        Unreachable
                    </button>

                    <button style="color: orange" id="contractsent"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Contract Sent')">
                        Contract Sent
                    </button>

                    <button style="color: blueviolet" id="servicestarted"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Service Started')">
                        Service Started
                    </button>


                    <button style="color: orangered" id="paid"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Paid')">
                        Paid
                    </button>

                    <button style="color: orangered" id="cancel"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect   "
                            type="submit" onclick="updateStatus('Cancel')">
                        Cancel
                    </button>
                </div>
    </main>
</div>


<script>

    var Drivers;

    //JSON Object fields below:
    /*
     address_latitude:33.6685471
     address_longitude:72.9966081
     car_model:"ccc"
     date_of_birth:"26-04-1438"
     email :"umar.com"
     first_name:"Umar"
     id:132
     is_active:0 7
     is_approved:1 8
     is_available:1 9
     last_name:"Usman"
     latitude:33.6686344
     longitude:72.996572
     phone: "966533153340"
     picture: "http://35.163.42.127/kasper-cab/public/uploads/b05c77ecb973a331ea8ff937328dbc1d00e4fd39.jpg"
     */


    function updateStatus(status) {

        var data = table.row('.selected').data();
        //get ID.
        //update data[status].

        console.log("TripID " + data[0]);
        table.row('.selected').data(data);


        data[23] = status;
        table.row('.selected').data(data);


        //Configure routes first.

        $.ajax({
            type: 'POST',
            url: 'updatestatus',
            dataType: 'text',
            data: {id: data[0], status: status},
        }).done(function (response) {
            alert(response + " Status Updated");
        }); // Ajax Call

    }


    function notifyDriver(driverID) {

        /*
         console.log("Driver ID "+driverID);
         console.log("Monthly ID" +table.row('.selected').data()[0]);*/

        var monthlyid = table.row('.selected').data()[0];

        /*driverID = 265;*/
        $.ajax({
            type: "POST",
            url: "notifydriver",
            data: {monthlyid: monthlyid, walkerid: driverID},
            cache: false,
            success: function (text) {
                alert(text);
                //do something here ;) :
            }
        });


        /*
         var data = table.row('.selected').data();
         */

        return;
    }


    function downloadContract(driver) {

        //driver name, phone, email, photo..
        var driverData = driver.split(" ");

        //Get selected row data/contract info
        var data = table.row('.selected').data();


        //Stringify contract+driver info.
        var jsonString = JSON.stringify([/*Contract info*/
            data[0], data[22], new Date(), data[1], data[0], data[20],
            data[6], data[9], data[10], data[11], data[18], data[3],
            /*driver data*/
            driverData[0] + " " + driverData[1], driverData[3], driverData[2], driverData[4]]);


        console.log(jsonString);

        $.ajax({
            type: "POST",
            url: "downloadcontract",
            data: {data: jsonString},
            cache: false,
            success: function (text) {
                alert(text);

                //download from the path of the saved .docs contract.
                window.open('/Dev-Server-Resources/ReadyContracts/Contract.docx', '_blank');
                //open link in new tab
            }
        });
    }


    function findDriver() {

        //get data from selected row.
        var data = table.row('.selected').data();

        //contract id
        console.log("Contract ID " + data[0]);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        console.log("Contract Id " + data[0]);

        //check how to call a route using ajax.

        $.ajax({
            type: 'POST',
            url: 'finddrivers',
            dataType: 'text',
            data: {id: data[0]},
        }).done(function (data) {
            //alert(data);

            Drivers = JSON.parse(data);
            // console.log(Drivers);
            displayDrivers(Drivers);

        }); // Ajax Call


    }


</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- DataTables Library -->
<script src="/kc/public/Dev-Server-Resources/tableassets/jquery.js"></script>
<script src="/kc/public/Dev-Server-Resources/tableassets/jquery.datatables.js"></script>

<!-- DataTables Search Script  -->
<script src="/kc/public/Dev-Server-Resources/tableassets/fnFindCellRowIndexes.js"></script>


<script>


    //datatables methods
    $(document).ready(function () {
        //#1 method hide columns

        $('#example').DataTable({

            "columnDefs": [

                {
                    "targets": [4],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [5],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [6],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [7],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [8],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [9],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [12],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [13],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [14],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [21],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [17],
                    "visible": false,
                    "searchable": false
                }
            ],
            dom: 'Bfrtip',
            colReorder: true,

            buttons: [
                'excel'
            ],
            "iDisplayLength": 20
        });


        //#2 Method: Row Selections.
        table = $('#example').DataTable();


        $('#example tbody').on('click', 'tr', function () {


            var data = table.row(this).data();


            //call display route here:
            console.log('You clicked on ' + "Starting Point:" + data[4] + " " + data[5] + " End Point:" + data[7] + " " + data[8] + '\'s ');

            var StartLocation = new google.maps.LatLng(data[4], data[5]);
            var EndLocation = new google.maps.LatLng(data[7], data[8]);


            displayRoute(StartLocation, EndLocation, directionsService,
                directionsDisplay);


        });

        //logic for row selection.
        $('#example tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });


    });


</script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- PDF file generater -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>


<script>

    var map;
    var FromAddress;
    var ToAddress;
    var doc;
    var directionsService;
    var directionsDisplay;
    var driverMarkers = [];

    function initMap() {
        var destination_place_id = null;
        var travel_mode = 'DRIVING';
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: 21.529, lng: 39.179}, //Jeddah
            // center: {lat: 24.713, lng: 46.675}, //Capital City
            zoom: 5
        });

        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: false,
            map: map,
        });


        directionsDisplay.addListener('directions_changed', function () {


            var changedorigin = directionsDisplay.directions.routes[0].legs[0].end_location

            console.log(changedorigin.lat());
            console.log(changedorigin.lng());


        });


        var StartLocation = new google.maps.LatLng(24.789095, 46.733349);
        var EndLocation = new google.maps.LatLng(24.789095, 46.733349);

        //      var EndLocation = new google.maps.LatLng(21.548977, 39.149628);

        //displayRoute(StartLocation, EndLocation , directionsService,
        //  directionsDisplay);


    }

    function displayRoute(origin, destination, service, display) {
        clearMarkers();

        service.route({
            origin: origin,
            destination: destination,
            travelMode: 'DRIVING',
            avoidTolls: true
        }, function (response, status) {
            if (status === 'OK') {
                display.setDirections(response);

                // console.log(response.routes[0].legs[0].start_address);

                FromAddress = response.routes[0].legs[0].start_address;


                // console.log(response.routes[0].legs[0].end_address);

                ToAddress = response.routes[0].legs[0].end_address


            } else {
                alert('Could not display directions due to: ' + status);
            }
        });
    }


    function statusToString(status) {
         if(status == 0){
             return 'offline';
         }else if (status==1){
             return 'online';
         }
    }

    function addMarker(Driver) {


        /*
         Driver[0] -> score relative to contract.home
         Driver[1] -> driver data.
         */
        var contentString = '<div id="content">' +
            '<div id="driverinfro">' +
            '</div>' +
            '<h3 id="firstHeading" class="firstHeading">Driver Info</h3>' +
            '<div id="bodyContent">' +
            '<p><b>Name:</b>, ' + Driver[1]['first_name'] + Driver[1]['last_name'] + '<br>' +
            '<b>Email:</b> ' + Driver[1]['email'] + '<br>' +
            '<b>Mobile:</b>  ' + Driver[1]['phone'] + '<br>' +
            '<b>Status:</b>  ' + statusToString(Driver[1]['is_active']) + '<br>' +

            '<img src="' + Driver[1]['picture'] + '"' + ' alt="Mountain View" style="width:250px;height:250px;">' +

            '</p>' +
            '<button style="color: orangered" id="assigncontract"' +
            'class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"' +
            'type="submit" value= "' + Driver[1]['first_name'] + " " + Driver[1]['last_name'] + " " + Driver[1]['email'] + " " + Driver[1]['phone'] + " " + Driver[1]['photo'] + '"' +
            ' onclick="downloadContract($(this).val())">' +
            'Download Contract' +
            '</button>' + '<br>' + '<br>' +

            '<button style="color: blueviolet" id="notifydriver"' +
            'class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"' +
            'type="submit" value= "' + Driver[1]['id'] + '"' +
            ' onclick="notifyDriver($(this).val())">' +
            'Assign Contract to Driver' +
            '</button>' + '<br>' + '<br>' +
            '</div>' +
            '</div>';


        //Note!: downloadContract must be defined globally to the whole php file in order to access it from "contentString"


        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(Driver[1]['address_latitude'], Driver[1]['address_longitude']),
            draggable: false,
            // icon: icons[feature.type].icon,
            map: map,
            label: Driver[1]['id'].toString()/*,
             zIndex: Math.round(latlng.lat()*-100000)<<5*/
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        driverMarkers.push(marker);
    }

    //display drivers as markers on the map
    function displayDrivers(Drivers) {
        console.log(Drivers.length);
        //Clear previously created markers.....
        clearMarkers();
        //show drivers marker on the map
        for (var i = 0; i < Drivers.length; i++) {


            addMarker(Drivers[i]);
        }
    }


    //clear/delete markers from the map.
    function clearMarkers() {

        for (var x = 0; x < driverMarkers.length; x++) {

            driverMarkers[x].setMap(null);
        }
        driverMarkers = [];


    }


</script>
<!-- MDL Linbraries -->
<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>

<!-- load google API-->
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places&callback=initMap"
        async defer>
</script>


</body>


</html>

