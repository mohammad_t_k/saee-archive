<!DOCTYPE html>
<html lang="en">
  <head>
    <title>:. Saee :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  </head>
  <body>
    <!-- header start-->
    <div class="container-fluid topclr">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="../images/logo.png"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling-->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="../changelanguage?language=en&amp;page=/provider/vehicle">English</a></li>
                <li><a href="../provider/profile" > الملف الشخصي</a></li>
                <li><a href="../provider/vehicle" style="color:#f7931e;">مركبة</a></li>
                <li><a href="../provider/documents" >مستندات</a></li>
                <li class="border"><a href="../provider/logout">الخروج</a></li>
              </ul>
            </div>
            <!-- /.navbar-collapse-->
          </div>
          <!-- /.container-fluid-->
        </nav>
      </div>
    </div>
    <!-- header end-->
    <!-- 1st section start-->
    <div class="container">
      <div class="row">
        <div class="col-md-12 heading">
          <h5>My Vehicle</h5>
          <h4>Our monthly contracts are headache free.</h4>
          <!-- 2nd section start-->
          <div class="container">
            <form action="../provider/update_vehicle" method="post" enctype="multipart/form-data" accept="image/x-png,image/gif,image/jpeg">
              <div class="container formwid2">
                <p>Update Vehicle</p>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3">
                      <label><b>MODEL</b></label>
                      <input type="text" placeholder="Model" name="car_model" required="" value="{{ $user->car_model }}">
                      <input type="hidden" name="id" value="">
                      <input type="hidden" name="token" value="">
                    </div>
                    <div class="col-md-3">
                      <label><b>الرقم التسلسلي للسيارة</b></label>
                      <input type="text" placeholder="000000000" name="vehicle_sequence_number" onkeypress="return isNumber(event)" maxlength="10" required="" value="{{ $user->vehicle_sequence_number }}">
                    </div>
                    <div class="col-md-3">
                      <label><b>تاريخ انتهاء الاستمارة</b></label>
                      <input type="text" placeholder="dd-mm-yy" name="car_registration_expiry" id="car_registration_expiry" required="" value="{{ $user->car_registration_expiry }}">
                    </div>
                    <div class="col-md-3">
                      <label><b>نوع اللوحة</b></label>
                      <select id="plate_type" name="plate_type" value="{{ $user->plate_type }}" class="form-control">
                        <option value="1">Private</option>
                        <option value="2">Taxi</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-2">
                      <select id="car_number_plate_letter1" name="car_number_plate_letter1" value="{{ $user->car_number_plate_letter1 }}" class="form-control">
                        <option>الرسالة الأولى</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select id="car_number_plate_letter2" name="car_number_plate_letter2" value="{{ $user->car_number_plate_letter2 }}" class="form-control">
                        <option>الرسالة الثانية</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select id="car_number_plate_letter3" name="car_number_plate_letter3" value="{{ $user->car_number_plate_letter3 }}" class="form-control">
                        <option>الرسالة الثالثة</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label><b>زقم اللوحة</b></label>
                      <input type="text" placeholder="PLATE NUMBER i.e. 0000" name="car_number_plate_number" onkeypress="return isNumber(event)" maxlength="4" value="{{ $user->car_number_plate_number }}" required="">
                    </div>
                    <div class="col-md-3">
                      <label></label><img src="../web/rsanumberplate.jpg" class="img-responsive">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                <?php $counter = 1; ?>
			    <span style="display:none;">{{$i = 0;}}</span>
                  <div class="row">
                    <div class="col-md-3">
                      <label><b>تاريخ انتهاء التأمين</b></label>
                      <input type="text" placeholder="dd-mm-yy" name="car_insurance_expiry" id="car_insurance_expiry" required="" value="{{ $user->car_insurance_expiry }}">
                    </div>
                    <div class="col-md-3">
                    <label><b>نوع السيارة</b></label>
                        @foreach($type as $types)
                        <div class="col-sm-12">
                            <?php
                            foreach ($ps as $pss) {
                                $ser = ProviderType::where('id', $pss->type)->first();
                                $ar[] = $ser->name;
                            }
                            $servname = $types->name;
                            ?>
                            <input id="service" name="service[]" type="radio" value="{{$types->id}}" <?php
                            if (!empty($ar)) {
                                if (in_array($servname, $ar))
                                    echo "checked='checked'";
                            }
                            ?>>{{$types->name}}
                            <br>    
                        </div>
                        <?php $counter++; ?>
                        @endforeach
                 </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <button type="submit" class="button3">UPDATE</button>
                </div>
              </div>
            </form>
          </div>
          <!-- 2nd section end-->
        </div>
      </div>
    </div>
    <!-- 1st section end-->
    <!-- footer start-->
    <div class="container">
      <div class="col-md-12 context">
        <div class="col-md-8">
          <h4>Customer Service</h4>
          <h5>920004954</h5>
        </div>
        <div class="col-md-4">
          <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="../#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-4 copuright">© 2017, KASPERCAB. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="../#">DRIVER REQUIREMENTS</a><a href="../#">TERMS OF SERVICE</a><a href="../#">PRIVACY POLICY</a></div>
      </div>
    </div>
    <!-- footer end-->
    <script type="text/javascript">
      var status = "ابدرسصطعقكلمحنهـوى";
      var user = <?php echo "".$user?>;
      for (var i = 0; i < status.length; i++) {
          if (user.plate_letter1 == status[i]) {
              $('#car_number_plate_letter1').append($("<option selected ></option>").attr("value", status[i]).text(status[i]));
          }
          else
              $('#car_number_plate_letter1').append($("<option ></option>").attr("value", status[i]).text(status[i]));
          if (user.plate_letter2 == status[i])
              $('#car_number_plate_letter2').append($("<option selected ></option>").attr("value", status[i]).text(status[i]));
          else
              $('#car_number_plate_letter2').append($("<option ></option>").attr("value", status[i]).text(status[i]));
          if (user.plate_letter3 == status[i])
              $('#car_number_plate_letter3').append($("<option selected ></option>").attr("value", status[i]).text(status[i]));
          else
              $('#car_number_plate_letter3').append($("<option ></option>").attr("value", status[i]).text(status[i]));
      }
      function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
          }
          return true;
      }
    </script>
    <script src="../web/js/hijricalender/jquery.plugin.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>
    <script>
      var calendar = $.calendars.instance('ummalqura');
      jQuery('#car_registration_expiry').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#car_registration_expiry').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
      jQuery('#car_insurance_expiry').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#car_insurance_expiry').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
    </script>
  </body>
</html>