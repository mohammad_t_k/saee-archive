<!DOCTYPE html>
<html lang="en">
  <head>
    <title>:. Saee :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
      #map_canvas {
          height: 300px;
          width: 100%;
          margin: 0;
      }
      #map_canvas .centerMarker {
          position: absolute;
          /*url of the marker*/
          background: url(../web/images/marker.png) no-repeat;
          /*center the marker*/
          top: 50%;
          left: 50%;
          z-index: 1;
          /*fix offset when needed*/
          margin-left: -10px;
          margin-top: -34px;
          /*size of the image*/
          height: 34px;
          width: 34px;
          cursor: pointer;
      }
    </style>
  </head>
  <body>
    <!-- header start-->
    <div class="container-fluid topclr">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#" class="navbar-brand"><img src="../images/logo.png"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling-->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="../changelanguage?language=ar&amp;page=/provider/profile">عربى</a></li>
                <li><a href="../provider/profile" style="color:#f7931e;"> PROFILE</a></li>
                <li><a href="../provider/vehicle">VEHICLE</a></li>
                <li><a href="../provider/documents" >Documents</a></li>
                <li class="border"><a href="../provider/logout">LOGOUT</a></li>
              </ul>
            </div>
            <!-- /.navbar-collapse-->
          </div>
          <!-- /.container-fluid-->
        </nav>
      </div>
    </div>
    <!-- header end-->
    <!-- 1st section start-->
    <div class="container">
      <div class="row">
        <div class="col-md-12 heading">
          <h5>My Profile</h5>
          <h4>Our monthly contracts are headache free.</h4>
          <!-- 2nd section start-->
          <div class="container">
            <form action="/provider/update_profile" method="post" enctype="multipart/form-data" accept="image/x-png,image/gif,image/jpeg">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="hidden" id="address_latitude" name="address_latitude" value="{{ $user->address_latitude }}">
                <input type="hidden" id="address_longitude" name="address_longitude" value="{{ $user->address_longitude }}">
              <div class="container formwid2">
                <p>Update Profile</p>
                  <div class="row">
                    <div class="col-md-3">
                        <label><b>FIRST NAME</b></label>
                        <input type="text" placeholder="First Name" name="first_name" required=""value="{{ $user->first_name }}">
                    </div>
                    <div class="col-md-3">
                        <label><b>LAST NAME</b></label>
                        <input type="text" placeholder="Last Name" name="last_name" required="" value="{{ $user->last_name }}">
                    </div>
                    <div class="col-md-3">
                      <label><b>GOVERNMENT ID NUMBER</b></label>
                      <input type="text" placeholder="1231231312" name="government_id_number" value="{{ $user->government_id_number }}" onkeypress="return isNumber(event)" maxlength="10" required="">
                    </div>
                    <div class="col-md-3">
                      <label><b>DATE OF BIRTH</b></label>
                      <input type="text" placeholder="Date of Birth" name="date_of_birth" id="date_of_birth" value="{{ $user->date_of_birth }}" required="">
                    </div>
                    
                  </div>
                <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-4">
                      <label><b>PAYMENT MODE</b></label>
                      <select id="plate_type" name="payment_mode" value="{{ $user->payment_mode }}" class="form-control">
                        <option value="1">CASH</option>
                        <option value="0">ONLINE</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label><b>LICENSE EXPIRY DATE</b></label>
                      <input type="text" placeholder="12-05-1438" name="license_expiry_date" id="license_expiry_date" value="{{ $user->license_expiry_date }}" required="">
                    </div>
                    <div class="col-md-4">
                      <label><b>PHONE</b></label>
                      <input type="text" placeholder="+966505694000" name="phone" id="phone" value="{{ $user->phone }}" required="">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-4">
                      <div class="form-group">
                          
                        <label for="picture">PHOTO</label>
                        <input id="picture" type="file" name="picture" class="form-control-file">
                      </div>
                    </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label><b>SELECT HOME LOCATION ON MAP</b></label>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div id="map_canvas"></div>
                  <p id="homeAddressErr" class="text-dangers"></p>
                </div>
                <div class="col-md-12">
                  <button type="submit" onclick="if(!validateAllInputs()) return false;" class="button3">UPDATE</button>
                </div>
              </div>
            </form>
          </div>
          <div class="container">
            <form action="/provider/update_password" method="post">
              <div class="container formwid2 col-md-12 border2">
                <p>CHANGE PASSWORD
                  <div class="col-md-3">
                    <label><b>CURRENT PASSWORD</b></label>
                    <input type="text" placeholder="Current passowrd" name="password" required="">
                  </div>
                  <div class="col-md-3">
                    <label><b>NEW PASSWORD</b></label>
                    <input type="text" placeholder="New passowrd" name="newpassword" required="">
                  </div>
                  <div class="col-md-3">
                    <label><b>CONFIRM PASSWORD</b></label>
                    <input type="text" placeholder="Confirm passowrd" name="confirm-pwd" required="">
                  </div>
                  <div class="col-md-12">
                    <button type="submit" class="button3">CHANGE PASSOWORD</button>
                  </div>
                </p>
              </div>
            </form>
          </div>
          <!-- 2nd section end-->
        </div>
      </div>
    </div>
    <!-- 1st section end-->
    <!-- footer start-->
    <div class="container">
      <div class="col-md-12 context">
        <div class="col-md-8">
          <h4>Customer Service</h4>
          <h5>920004954</h5>
        </div>
        <div class="col-md-4">
          <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="../#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-4 copuright">© 2017, KASPERCAB. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="../#">DRIVER REQUIREMENTS</a><a href="../#">TERMS OF SERVICE</a><a href="../#">PRIVACY POLICY</a></div>
      </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
    <script>
      function initMap() {
          try {
              var user = <?php echo "".$user?>;
              var address_latitude = 21.2854;
              var address_longitude = 39.2376;
              if (!isNaN(user.address_latitude)&&!isNaN(user.address_longitude)&&user.address_latitude != 0 && user.address_longitude != 0) {
                  address_latitude = user.address_latitude;
                  document.getElementById('address_latitude').value = address_latitude;
                  address_longitude = user.address_longitude;
                  document.getElementById('address_longitude').value = address_longitude;
              }
              var mapOptions = {
                  zoom: 14,
                  center: new google.maps.LatLng(address_latitude, address_longitude),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };
              document.getElementById('map_canvas').style = 'height:200px';
              var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
              google.maps.event.addListener(map, 'center_changed', function () {
                  document.getElementById('address_latitude').value = map.getCenter().lat();
                  document.getElementById('address_longitude').value = map.getCenter().lng();
              });
              $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
              //do something onclick
                  .click(function () {
                      var that = $(this);
                      if (!that.data('win')) {
                          that.data('win', new google.maps.InfoWindow({
                              content: 'this is the center'
                          }));
                          that.data('win').bindTo('position', map, 'center');
                      }
                      that.data('win').open(map);
                  });
          }
          catch (e) {
              alert(e);
          }
      }
    </script>
    <script>google.maps.event.addDomListener(window, 'load', initMap);</script>
    <script type="text/javascript">
      function validateAllInputs() {
          var result = true;
          // Validating Sign Up Captain
          var phone = document.getElementById("phone");
          if (phone.value.substring(0, 4) != "+966") {
              alert("*Phone number must start with +966 e.g.+966501234567.<br>(يجب أن يبدأ رقم الهاتف مع +966 مثلا +966501234567)");
              result = false;
          } else if (phone.value.length != 13) {
              alert("*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(يجب أن يكون رقم لهاتف 13 رقما تبدأ ب 966)");
              result = false;
          }
          return result;
      }
      function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
          }
          return true;
      }
    </script>
    <script src="../web/js/hijricalender/jquery.plugin.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>
    <script>
      var calendar = $.calendars.instance('ummalqura');
      jQuery('#date_of_birth').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#date_of_birth').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
      jQuery('#license_expiry_date').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#license_expiry_date').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
    </script>
  </body>
</html>