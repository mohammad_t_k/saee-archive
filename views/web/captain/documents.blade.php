<!DOCTYPE html>
<html lang="en">
  <head>
    <title>:. KASPER CAB :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  </head>
  <body>
    <!-- header start-->
    <div class="container-fluid topclr">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="../images/logo.png"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling-->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="../changelanguage?language=ar&amp;page=/provider/vehicle">عربى</a></li>
                <li><a href="../provider/profile"> PROFILE</a></li>
                <li><a href="../provider/vehicle" style="color:#f7931e;">VEHICLE</a></li>
                <li><a href="../provider/documents" >Documents/مستندات</a></li>
                <li class="border"><a href="../provider/logout">LOGOUT</a></li>
              </ul>
            </div>
            <!-- /.navbar-collapse-->
          </div>
          <!-- /.container-fluid-->
        </nav>
      </div>
    </div>
    <!-- header end-->
    <!-- 1st section start-->
    <div class="container">
      <div class="row">
        <div class="col-md-12 heading">
          <h5>My Documents</h5>
          <h4>Our monthly contracts are headache free.</h4>
          <!-- 2nd section start-->
          <div class="container">
            <form class="form-horizontal style-form" method="post" action="{{ URL::Route('providerUpdateDocuments') }}" enctype="multipart/form-data">
            <?php foreach ($documents as $document) { ?>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"><?= $document->name ?></label>
                    <div class="col-sm-1">
                        <?php
                        foreach ($walker_document as $walker_documents) {
                            if ($document->id == $walker_documents->document_id) {
                                ?>
                                <a href="<?= $walker_documents->url ?>" target="_blank">View File(استعراض الملف)</a>
                            <?php }
                        }
                        ?>
                    </div>
                    <div class="col-sm-5" style="">
                        <input id="doc" type="file" class="form-control" name="<?= $document->id ?>" >
                    </div>
                </div>

<?php } ?>

            <span class="col-sm-2"></span>
            <button id="upload" type="submit" class="btn btn-info">Upload Documents(وثائق تحميل)</button>

        </form>
          </div>
          <!-- 2nd section end-->
        </div>
      </div>
    </div>
    <!-- 1st section end-->
    <!-- footer start-->
    <div class="container">
      <div class="col-md-12 context">
        <div class="col-md-8">
          <h4>Customer Service</h4>
          <h5>920004954</h5>
        </div>
        <div class="col-md-4">
          <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="../#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-4 copuright">© 2017, KASPERCAB. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="../#">DRIVER REQUIREMENTS</a><a href="../#">TERMS OF SERVICE</a><a href="../#">PRIVACY POLICY</a></div>
      </div>
    </div>
    <!-- footer end-->
  </body>
</html>