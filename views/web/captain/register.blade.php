<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>:. Saee :.</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        #map_canvas {
          height: 300px;
          width: 100%;
          margin: 0;
      }
      #map_canvas .centerMarker {
          position: absolute;
          /*url of the marker*/
          background: url(../web/images/marker.png) no-repeat;
          /*center the marker*/
          top: 50%;
          left: 50%;
          z-index: 1;
          /*fix offset when needed*/
          margin-left: -10px;
          margin-top: -34px;
          /*size of the image*/
          height: 34px;
          width: 34px;
          cursor: pointer;
      }

    </style>
</head>
<body>
<!-- header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="<?php echo asset_url(); ?>/newweb/images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling-->
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../changelanguage?language=ar&amp;page=/provider/signup">عربى</a></li>
                        <li class="border"><a href="../provider/signin">SIGN IN</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse-->
            </div>
            <!-- /.container-fluid-->
        </nav>
    </div>
</div>
<!-- header end-->
<!-- 1st section start-->
<div class="container">
    <div class="row">
        <div class="col-md-12 heading">
            <h5>Register</h5>
            <h4>Dont have an account?<a href="#">Create your account</a>, it takes
                less than a minute
            </h4>
            <!-- 2nd section start-->
            <h4>
                @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                @endif
            </h4>
            <!-- 2nd section start-->
            <div class="container">
                <form action="/provider/save" method="post" role="form" enctype="multipart/form-data">
                    <div class="container formwid">
                        <?php  echo Form::token(); ?>
                        <input type="hidden" value="" name="address_latitude" id="address_latitude" required="">
                        <input type="hidden" value="" name="address_longitude" id="address_longitude" required="">
                        <input type="hidden" value="<?php echo $utm_source; ?>" name="utm_source" id="utm_source">

                        <label><b>FIRST NAME</b></label>
                        <input type="text" placeholder="First Name" name="first_name" value="{{ Session::get('first_name') }}" required="">
                        <label><b>LAST NAME</b></label>
                        <input type="text" placeholder="Last Name" name="last_name" required="" value="{{ Session::get('last_name') }}">
                        <label><b>PHONE #</b></label>
                        <input type="text" placeholder="Phone #" name="phone" id="phone" required="" value="{{ Session::get('phone') }}">
                        <label><b>EMAIL</b></label>
                        <input type="text" placeholder="Email" name="email" id="email" required="" value="{{ Session::get('email') }}">

                         <label><b>SEX</b></label>
                        <select required="" name="sex" id="sex" class="form-control">
                            <option value="">Select Gender</option>
                            <option value="Male" <?php if(Session::get('sex')=='Male'){echo "selected";} ?> >Male</option>
                            <option value="Female" <?php if(Session::get('sex')=='Female'){echo "selected";} ?> >Female</option>
                        </select>
                        <label><b>DISTRICT</b></label>
                        <input type="text" placeholder="District" name="district" id="district" required="" value="{{ Session::get('district') }}">
                         <!--<label><b>ADDRESS</b></label>
                        <textarea style="width:100%;margin-bottom: 15px;margin-top: 15px" name="address" id="address">Address</textarea> -->
                            <label><b>Car Type</b></label>
                            <select required="" name="car_type" id="car_type" class="form-control">
                                <option value="">Select Car Type</option>
                                @foreach($car_types as $cartype)
                                    <option value="{{$cartype->id}}"<?php if (isset($car_type) && $car_type == $cartype->id) {
                                        echo 'selected';
                                    } ?>>{{$cartype->english}}</option>
                                @endforeach
                            </select>

                        <label><b>VECHICLE REGISTRATION SCAN</b></label>

                       <input type="file" name="vehicle_registration_scan" class="form-control" id="vehicle_registration_scan" value="" required>

                       <label><b>VECHICLE REGISTRATION EXPIRY</b></label>
                        <input type="text" placeholder="VECHELE REGISTRATION EXPIRY" name="vehicle_registration_expiry" id="vehicle_registration_expiry" required="" value="{{ Session::get('vehicle_registration_expiry') }}">
                      
                      
                        <label><b>VECHICLE FRONT SCAN</b></label>
                        <input type="file" name="vehicle_front_scan" class="form-control" id="vehicle_front_scan" value="" required>

                        <label><b>CAPTAIN IQAMA SCAN</b></label>
                        <input type="file" name="captain_iqama_scan" class="form-control" id="captain_iqama_scan" value="" required>

          
                        <label><b>CAPTAIN LICENSE SCAN</b></label>
                        <input type="file" name="captain_license_scan" class="form-control" id="captain_license_scan" value="" required>

                        <label><b>CONFESSION OF JUDGEMENT</b></label>
                        <input type="file" name="captain_confession" class="form-control" id="captain_confession" value="" required>

                        <label><b>LICENSE EXPIRY DATE</b></label>
                        <input type="text" placeholder="LICENSE EXPIRY DATE" name="license_expiry_date" id="license_expiry_date" required="" value="{{ Session::get('license_expiry_date') }}">
                
                        <label><b>NATIONALITY</b></label>
                        <select required="" name="nationality" id="nationality" class="form-control">
                            <option value="">Select Nationality</option>
                            <option value="Saudi" <?php if(Session::get('nationality')=='Saudi'){echo "selected";} ?>>Saudi</option>
                            <option value="Egypt" <?php if(Session::get('nationality')=='Egypt'){echo "selected";} ?> >Egypt</option>
                            <option value="Syria" <?php if(Session::get('nationality')=='Syria'){echo "selected";} ?>>Syria</option>
                            <option value="Sudan" <?php if(Session::get('nationality')=='Sudan'){echo "selected";} ?>>Sudan</option>
                            <option value="Pakistan" <?php if(Session::get('nationality')=='Pakistan'){echo "selected";} ?>>Pakistan</option>
                            <option value="India" <?php if(Session::get('nationality')=='Indea'){echo "selected";} ?>>India</option>
                            <option value="Yemen" <?php if(Session::get('nationality')=='Yemen'){echo "selected";} ?>>Yemen</option>
                            <option value="Other" <?php if(Session::get('nationality')=='Other'){echo "selected";} ?>>Other</option>
                        </select>
                        <label><b>CITY</b></label>
                        <select required="" name="city" id="city" class="form-control">
                            <option value="">Select City</option>
                            @foreach($cities as $one_city)
                              <option value="{{ $one_city->id }}" <?php if(isset($city) && $city == '{{ $one_city->id }}'){echo "selected";} ?>>{{ $one_city->subcity }}</option>
                            @endforeach
                        </select>


                        <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label><b>SELECT HOME LOCATION ON MAP</b></label>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div id="map_canvas"></div>
                  <p id="homeAddressErr" class="text-dangers"></p>
                </div>

                        <label><b>PASSWORD</b></label>
                        <input type="password" placeholder="Enter Password" name="password" required="">
                        <label><b>REPEAT PASSWORD</b></label>
                        <input type="password" placeholder="Repeat Password" name="psw-repeat" required=""><br>
                        <p>Do you have an account already?<a href="../captain/signin">Sign in</a></p>
                        <div class="clearfix">
                            <button id="submit" type="submit" onclick="if(!validateAllInputs()) return false;" class="button1">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- 2nd section end-->
        </div>
    </div>
</div>
<div style="display: none" id="map"></div>

<!-- 1st section end-->
<!-- footer start-->
<div class="container">
    <div class="col-md-12 context">
        <div class="col-md-8">
            <h4>Customer Service</h4>
            <h5>920004954</h5>
        </div>
        <div class="col-md-4">
            <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4 copuright">© <?php echo date('Y') ?>, SAEE. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="#">CAPTAIN REQUIREMENTS</a><a href="#">TERMS OF SERVICE</a><a href="#">PRIVACY POLICY</a></div>
    </div>
</div>
<!-- footer end-->
<script src="../web/js/scroll.js"></script>
<script type="text/javascript">
    function validateAllInputs() {
        var result = true;
        // Validating Sign Up Captain
        var phone = document.getElementById("phone");
        if (phone.value.substring(0, 4) != "+966") {
            alert("*Phone number must start with +966 e.g.+966501234567.<br>(??? ?? ???? ??? ?????? ?? +966 ???? +966501234567)");
            result = false;
        } else if (phone.value.length != 13) {
            alert("*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(??? ?? ???? ??? ????? 13 ???? ???? ? 966)");
            result = false;
        }
        var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        var mailvalue = document.getElementById('email');
        if (!mailformat.test(mailvalue.value)) {
            alert("Invalid Email");
            result = false;
        }
        var car_type = document.getElementById("car_type").value;
        if (car_type == "") {
            alert('Please Select car type');
            result = false;
        }
        return result;
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key?>&callback=initMap">
    </script>
<script>


      function initMap() {
          try {
              //var user = <?php //echo "".$orderDetail[0]?>;
              var address_latitude = 21.2854;
              var address_longitude = 39.2376;
              var mapOptions = {
                  zoom: 15,
                  center: new google.maps.LatLng(address_latitude, address_longitude),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };
              document.getElementById('map_canvas').style = 'height:200px';
              var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
              infoWindow = new google.maps.InfoWindow;

               // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {

            var accuracy = position.coords.accuracy;
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);

            if(accuracy > 60){

              infoWindow.setContent('Please Update Location Accuracy ' + accuracy);
              
            }
            else{
              infoWindow.setContent('Current Location. Accuracy is ' + accuracy);
            }
            
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }


              google.maps.event.addListener(map, 'center_changed', function () {
                  address_latitude= document.getElementById('address_latitude').value = map.getCenter().lat();
                 address_longitude= document.getElementById('address_longitude').value = map.getCenter().lng();
                
              });
              $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
              //do something onclick
                  .click(function () {
                      var that = $(this);
                      if (!that.data('win')) {
                          that.data('win', new google.maps.InfoWindow({
                              content: 'this is the center'
                          }));
                          that.data('win').bindTo('position', map, 'center');
                      }
                      that.data('win').open(map);
                  });

          }
          catch (e) {
              alert(e);
          }
      }
    </script>
    <script>google.maps.event.addDomListener(window, 'load', initMap);</script>

 <script src="../web/js/hijricalender/jquery.plugin.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>
    <script>
      var calendar = $.calendars.instance('ummalqura');
      jQuery('#vehicle_registration_expiry').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#vehicle_registration_expiry').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
      jQuery('#license_expiry_date').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#license_expiry_date').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
    </script>

</body>
</html>