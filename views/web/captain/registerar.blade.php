<!DOCTYPE html>
<html lang="en">
<head>
    <title>:. Saee :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link rel="stylesheet" type="text/css" href="../web/css/jquery.calendars.picker.css">
    <link href="../https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        #map_canvas {
          height: 300px;
          width: 100%;
          margin: 0;
      }
      #map_canvas .centerMarker {
          position: absolute;
          /*url of the marker*/
          background: url(../web/images/marker.png) no-repeat;
          /*center the marker*/
          top: 50%;
          left: 50%;
          z-index: 1;
          /*fix offset when needed*/
          margin-left: -10px;
          margin-top: -34px;
          /*size of the image*/
          height: 34px;
          width: 34px;
          cursor: pointer;
      }

    </style>
</head>
<body>
<!-- header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="<?php echo asset_url(); ?>/newweb/images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling-->
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../changelanguage?language=en&amp;page=/provider/signup">English</a></li>
                        <li><a href="../provider/signin">تسجيل الدخول</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse-->
            </div>
            <!-- /.container-fluid-->
        </nav>
    </div>
</div>
<!-- header end-->
<!-- 1st section start-->
<div class="container">
    <div class="row">
        <div class="col-md-12 heading">
            <h5>تسجيل</h5>
            <h4>   ما سجلت لسه؟ سجل من هنا في دقايق</h4>
            <!-- 2nd section start-->
            <h4>
                @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                @endif
            </h4>
            <div class="container">
                <form action="/provider/save" method="post" role="form" enctype="multipart/form-data">
                    <div class="container formwid">
                        <?php  echo Form::token(); ?>
                        <input type="hidden" value="" name="address_latitude" id="address_latitude" required="">
                        <input type="hidden" value="" name="address_longitude" id="address_longitude" required="">
                        <input type="hidden" value="<?php echo $utm_source; ?>" name="utm_source" id="utm_source">

                        <label><b>الاسم الأول</b></label>
                        <input type="text" placeholder="الاسم الأول " name="first_name" id="first_name" required="" value="{{ Session::get('first_name') }}">
                        <label><b>الاسم الأخير</b></label>
                        <input type="text" placeholder="الاسم الأخير " name="last_name" id="last_name" required="" value="{{ Session::get('last_name') }}">
                        <label><b>الجنس</b></label>

                        <select required="" name="sex" id="sex" class="form-control">
                            <option value="">حدد نوع الجنس</option>
                            <option value="Male" <?php if(Session::get('sex')=='Male'){echo "selected";} ?> >ذكر</option>
                            <option value="Female" <?php if(Session::get('sex')=='Female'){echo "selected";} ?> >أنثى</option>
                        </select>
                        <label><b>الحي</b></label>
                        <input type="text" placeholder="الحي" name="district" id="district" required="" value="{{ Session::get('district') }}">
                         <!--<label><b>ADDRESS</b></label>
                        <textarea style="width:100%;margin-bottom: 15px;margin-top: 15px" name="address" id="address">Address</textarea> -->
                            <label><b>نوع السيارة</b></label>
                            <select required="" name="car_type" id="car_type" class="form-control">
                                <option value="">اختر نوع السيارة</option>
                                @foreach($car_types as $cartype)
                                    <option value="{{$cartype->id}}"<?php if (isset($car_type) && $car_type == $cartype->id) echo 'selected';?>>{{$cartype->arabic}}</option>
                                @endforeach
                            </select>

                        <label><b>.الرجاء اختيار صورة طبق الأصل من الاستمارة</b></label>

                       <input type="file" name="vehicle_registration_scan" class="form-control" id="vehicle_registration_scan" value="" required>
                      
                      <label><b>.الرجاء اختيار تاريخ انتهاء استمارة صحيح</b></label>
                        <input type="text" placeholder=".الرجاء اختيار تاريخ انتهاء استمارة صحيح" name="vehicle_registration_expiry" id="vehicle_registration_expiry" required="" value="{{ Session::get('vehicle_registration_expiry') }}">

                        <label><b>الرجاء اختيار صورة طبق الأصل من رخصة القيادة.</b></label>
                        <input type="file" name="captain_license_scan" class="form-control" id="captain_license_scan" value="" required>

                         <label><b>.الرجاء اختيار تاريخ انتهاء رخصة صحيح</b></label>
                        <input type="text" placeholder=".الرجاء اختيار تاريخ انتهاء رخصة صحيح" name="license_expiry_date" id="license_expiry_date" required="" value="{{ Session::get('license_expiry_date') }}">
                      
                        <label><b>.الرجاء اختيار صورة طبق الأصل من مقدمة السيارة</b></label>
                        <input type="file" name="vehicle_front_scan" class="form-control" id="vehicle_front_scan" value="" required>

                        <label><b>.الرجاء اختيار صورة طبق الأصل من الهوية</b></label>
                        <input type="file" name="captain_iqama_scan" class="form-control" id="captain_iqama_scan" value="" required>

                      <label><b>سند لأمر</b></label>
                        <input type="file" name="captain_confession" class="form-control" id="captain_confession" value="" required>


                

                        <input type="hidden" value="web" placeholder="DEVICE TYPE" name="device_type" id="device_type" required="">
                         <label><b>الجنسية</b></label>
                        <select required="" name="nationality" id="nationality" class="form-control">
                            <option value="">الجنسية</option>
                            <option value="Saudi" <?php if(Session::get('nationality')=='Saudi'){echo "selected";} ?>>السعودية</option>
                            <option value="Egypt" <?php if(Session::get('nationality')=='Egypt'){echo "selected";} ?>>مصر</option>
                            <option value="Syria" <?php if(Session::get('nationality')=='Syria'){echo "selected";} ?>>سوريا</option>
                            <option value="Sudan" <?php if(Session::get('nationality')=='Sudan'){echo "selected";} ?>>سودان</option>
                            <option value="Pakistan" <?php if(Session::get('nationality')=='Pakistan'){echo "selected";} ?>>باكستان</option>
                            <option value="India" <?php if(Session::get('nationality')=='Indea'){echo "selected";} ?>>الهند</option>
                            <option value="Yemen" <?php if(Session::get('nationality')=='Yemen'){echo "selected";} ?>>اليمن</option>
                            <option value="Other" <?php if(Session::get('nationality')=='Other'){echo "selected";} ?>>اخرى</option>
                        </select>
                        <label><b>المدينة</b></label>
                        <select required="" name="city" id="city" class="form-control">
                            <option value="">المدينة</option>
                            @foreach($cities as $one_city)
                              <option value="{{ $one_city->id }}" <?php if(isset($city) && $city == '{{ $one_city->id }}'){echo "selected";} ?>>{{ $one_city->subcity_ar }}</option>
                            @endforeach
                        </select>
                        <label><b>رقم الجوال</b></label>
                        <input type="text" placeholder="رقم الجوال" name="phone" id="phone" required="" value="{{ Session::get('phone') }}">
                        <label><b>البريد الالكتروني</b></label>
                        <input type="text" placeholder="البريد الالكتروني" name="email" id="email" required="" value="{{ Session::get('email') }}">

                        <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label><b>عنوان جوجل</b></label>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div id="map_canvas"></div>
                  <p id="homeAddressErr" class="text-dangers"></p>
                </div>

                        <label><b>كلمه السر</b></label>
                        <input type="password" placeholder="كلمه السر" name="password" id="password" required="">
                        <label><b>كرر الرقم السري</b></label>
                        <input type="password" placeholder="كرر الرقم السري" name="password-repeat" id="password-repeat" required=""><br>
                        <p>عندك حساب؟<a href="../#">سجل الدخول</a></p>
                        <div class="clearfix">
                            <button type="submit" onclick="if(!validateAllInputs()) return false;" class="button1">تسجيل</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- 2nd section end-->
        </div>
    </div>
</div>
<div style="display: none" id="map"></div>
<!-- 1st section end-->
<!-- footer start-->
<div class="container">
    <div class="col-md-12 context">
        <div class="col-md-8">
            <h4>خدمة الزبائن</h4>
            <h5>920004954</h5>
        </div>
        <div class="col-md-4">
            <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="../#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4 copuright">© <?php echo date('Y') ?>, SAEE. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="../#">DRIVER REQUIREMENTS</a><a href="../#">TERMS OF SERVICE</a><a href="../#">PRIVACY POLICY</a></div>
    </div>
</div>
<!-- footer end-->
<script src="../web/js/scroll.js"></script>
<script type="text/javascript">
    function validateAllInputs() {
        var result = true;
        // Validating Sign Up Captain
        var phone = document.getElementById("phone");
        if (phone.value.substring(0, 4) != "+966") {
            alert("*Phone number must start with +966 e.g.+966501234567.<br>(يجب أن يبدأ رقم الهاتف مع +966 مثلا +966501234567)");
            result = false;
        } else if (phone.value.length != 13) {
            alert("*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(يجب أن يكون رقم لهاتف 13 رقما تبدأ ب 966)");
            result = false;
        }
        var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        var mailvalue = document.getElementById('email');
        if (!mailformat.test(mailvalue.value)) {
            alert("Invalid Email");
            result = false;
        }
        var car_type = document.getElementById("car_type").value;
        if (car_type == "") {
            alert('Please Select car type');
            result = false;
        }
        return result;
    }
</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key?>&callback=initMap">
    </script>
<script>


      function initMap() {
          try {
              //var user = <?php //echo "".$orderDetail[0]?>;
              var address_latitude = 21.2854;
              var address_longitude = 39.2376;
              var mapOptions = {
                  zoom: 15,
                  center: new google.maps.LatLng(address_latitude, address_longitude),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };
              document.getElementById('map_canvas').style = 'height:200px';
              var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
              infoWindow = new google.maps.InfoWindow;

               // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {

            var accuracy = position.coords.accuracy;
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);

            if(accuracy > 60){

              infoWindow.setContent('Please Update Location Accuracy ' + accuracy);
              
            }
            else{
              infoWindow.setContent('Current Location. Accuracy is ' + accuracy);
            }
            
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }


              google.maps.event.addListener(map, 'center_changed', function () {
                  address_latitude= document.getElementById('address_latitude').value = map.getCenter().lat();
                 address_longitude= document.getElementById('address_longitude').value = map.getCenter().lng();
                
              });
              $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
              //do something onclick
                  .click(function () {
                      var that = $(this);
                      if (!that.data('win')) {
                          that.data('win', new google.maps.InfoWindow({
                              content: 'this is the center'
                          }));
                          that.data('win').bindTo('position', map, 'center');
                      }
                      that.data('win').open(map);
                  });

          }
          catch (e) {
              alert(e);
          }
      }
    </script>
    <script>google.maps.event.addDomListener(window, 'load', initMap);</script>

 <script src="../web/js/hijricalender/jquery.plugin.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
    <script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>
    <script>
      var calendar = $.calendars.instance('ummalqura');
      jQuery('#vehicle_registration_expiry').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#vehicle_registration_expiry').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
      jQuery('#license_expiry_date').calendarsPicker({
          calendar: calendar, onSelect: function (date) {
              jQuery('#license_expiry_date').val(date[0].formatDate('dd-mm-yyyy'));
          }
      });
    </script>
</body>
</html>