<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Saee</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../web/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../web/css/font-awesome.css">
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/bootstrap.min.js"></script>
    <script src="../web/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/css/custom.css">
    <link rel="stylesheet" type="text/css" href="../web/css/badge.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>
<!-- header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="/" class="navbar-brand"><img src="../images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling-->
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../changelanguage?language=ar&amp;page=/provider/signin">عربى</a></li>
                        <li class="border"><a href="../provider/signup">CAPTAIN SIGNUP</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse-->
            </div>
            <!-- /.container-fluid-->
        </nav>
    </div>
</div>
<!-- header end-->
<!-- 1st section start-->
<div class="container">
    <div class="row">
        <div class="col-md-12 heading">
            <h5>Become A Captain</h5>
            <h4>Our monthly contracts are headache free..</h4>
            <!-- 2nd section start-->
            <h4>
                @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                @endif
            </h4>
            <div class="container">
                <form action="../provider/verify" method="post">
                    <div class="container formwid">
                        <label><b>Username</b></label>
                        <input type="text" placeholder="Enter Email" name="email" id="email" required="">
                        <label><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="password" required="">
                        <button type="submit" onclick="if(!validateAllInputs()) return false;" class="button1">Login</button><br>
                        <input type="checkbox" checked="checked"> Remember me
                    </div>
                </form>
            </div>
            <form id="recoverform" action="../provider/forgot-password" method="post" class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you!</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text" name="email" required="" placeholder="Email" class="form-control">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light">Reset</button>
                    </div>
                </div>
            </form>
            <!-- 2nd section end-->
        </div>
    </div>
</div>
<!-- 1st section end-->
<!-- footer start-->
<div class="container">
    <div class="col-md-12 context">
        <div class="col-md-8">
            <h4>Customer Service</h4>
            <h5>920004954</h5>
        </div>
        <div class="col-md-4">
            <div class="social"><img src="../images/facb.png"><img src="../images/twi.png"><img src="../images/insta.png"><img src="../images/ink.png"></div><a id="back-to-top" href="#" title="Back to top"><img src="../images/arrow.jpg"></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4 copuright">© 2017, KASPERCAB. ALL RIGHTS RESERVED</div>
        <div class="col-md-6 footLinks"><a href="#">DRIVER REQUIREMENTS</a><a href="#">TERMS OF SERVICE</a><a href="#">PRIVACY POLICY</a></div>
    </div>
</div>
<!-- footer end-->
<script src="../js/scroll.js"></script>
<script type="text/javascript">
    function validateAllInputs() {
        var result = true;
        var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        var mailvalue = document.getElementById('email');
        if (!mailformat.test(mailvalue.value)) {
            alert("Invalid Email");
            result = false;
        }
        return result;
    }
</script>
</body>
</html>