<?php
//echo $request;
//echo $request->id;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Khod W Haat | Home Page</title>
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <link href="font-awesome.min.css" rel="stylesheet">
    <style type="text/css">
        *{padding: 0; margin: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;}
        body{color: #333; border-top: 5px solid rgb(244, 170, 47); font-family: '29LT Bukra', sans-serif; font-weight: normal; font-size: 13px;}
        p{margin-bottom: 7px;}
        p span{color: #666; font-size: 12px;}
        a:hover{color: rgb(244, 170, 47); text-decoration: none !important;}
        a:focus{outline: none !important;}
        .btn{border-radius: 0; font-weight: 500; font-size: 13px; padding: 15px; text-align: center; text-decoration: none;}
        .btn-main{background: rgb(244, 170, 47); color: #fff;}
        .btn-main:hover{color: #fff; background: rgb(109, 109, 109);}
        .btn-block{display: block;}
        .btmmargin-sm{margin-bottom: 15px;}
        .header{padding: 10px 0; background: #fafafa; margin-bottom: 20px; border-bottom: 1px solid #eee;}
        .header img{width: 100px; margin: 0 auto;}
        .shadow-box{padding: 15px; box-shadow: 0 0 2px #ddd; -moz-box-shadow: 0 0 2px #ddd; -webkit-box-shadow: 0 0 2px #ddd; margin-bottom: 15px;}
        .form-group{position: relative; padding-left: 30px;}
        .form-control{width: 100%; padding: 5px; border:1px solid #ccc;}
        .form-group i{position: absolute; left: 0; top: 3px; font-size: 20px;}

    </style>

</head>
<body>
<div style="max-width: 500px; margin: 0 auto;">
    <header class="header" style="text-align: center;">
        <figure class="logo"><img src="/DeliveryApp2/assets-2/images/logo.svg" alt="Khod w Haat" class="img-responsive"></figure>
    </header>

    <section style="padding: 0 20px;">
        <div class="shadow-box">
            <p id="requestid" hidden><?php echo $request->id ?></p>
            <p align="center" class="btmmargin-sm"><?php echo " رقم الطلب".$request->id ?></p>

            <p class="btmmargin-sm"><strong>Pick Up Info:</strong></p>
            <p><b>Name:</b> <span><?php echo $request->sender_name; ?></span></p>
            <p><b>Mobile:</b> <span><?php echo $request->sender_phone; ?></span></p>
            <p><b>Scheduled:</b>
                <?php 
                if($request->immediate_pickup == 0){
                    echo "YES";
                    echo '<p><b>Date:</b> <span>'.$request->scheduled_pickup_date.'</span></p>';

                    if ($request->scheduled_pickup_time == 1){
                        echo '<p><b>Time:</b> <span>'."10 am - 12 pm".'</span></p>';
                    }else if ($request->scheduled_pickup_time == 2){
                        echo '<p><b>Time:</b> <span>'."2 - 4 pm".'</span></p>';
                    }else if($request->scheduled_pickup_time == 3){
                        echo '<p><b>Time:</b> <span>'."7 -9 pm".'</span></p>';
                    }

                } else if($request->immediate_pickup == 1) {
                    echo '<p><b>Immediate Pickup</b>';
                }
                ?>
            </p>

            <p><b>Link Address:</b>   <a href="<?php echo "http://www.google.com/maps/place/".$request->origin_latitude.",".$request->origin_longitude ?>">Click here for pickup address</a> </p>

        </div>
        <div class="shadow-box">
            <p class="btmmargin-sm"><strong>Drop Off Info:</strong></p>
            <p><b>Name:</b> <span><?php echo $request->receiver_name; ?></span></p>
            <p><b>Mobile:</b> <span><?php echo $request->receiver_phone; ?></p>
            <p><b>Scheduled:</b>
                <?php if($request->immediate_dropoff == 0){
                    echo  '<i class="fa fa-check" style="color: green;"></i>';
                    echo "YES";
                    echo '<p><b>Date:</b> <span>'.$request->scheduled_dropoff_date.'</span></p>';

                    if ($request->scheduled_dropoff_time == 1){
                        echo '<p><b>Time:</b> <span>'."10 am - 12 pm".'</span></p>';
                    }else if ($request->scheduled_dropoff_time == 2){
                        echo '<p><b>Time:</b> <span>'."2 - 4 pm".'</span></p>';
                    }else if($request->scheduled_dropoff_time == 3){
                        echo '<p><b>Time:</b> <span>'."7 -9 pm".'</span></p>';
                    }
                } else if($request->immediate_dropoff == 1) {
                    echo '<p><b>Immediate Dropoff </b>';
                }
                
                
                  ?>
            </p>
            <p><b>Link Address:</b>   <a href="<?php echo "http://www.google.com/maps/place/".$request->D_latitude.",".$request->D_longitude ?>">Click here for pickup address</a> </p>
        </div>
        <div  class="shadow-box">
            <p><b>Item:</b> <span>  <?php
                        echo 'Small Items'.$request->small_items;
                        echo '<br>';
                        echo 'Medium Items'.$request->medium_items;
                        echo '<br>';
                        echo 'Large Items'.$request->large_items;
                    ?> </span></p>
            <p><b>Cash On Delivery:</b> <span><?php echo $request->cash_on_delivery." SAR"; ?> </span></p>
            <p><b>Transportation Fees:</b> <span> <?php echo $request->transportation_fees." SAR";  ?></span></p>
            <p><b>Payed By:</b> <span><?php
                       if ($request->transportation_fees_payed_by == 0){
                           echo "Sender will pay the fees";
                       }else if($request->transportation_fees_payed_by == 1){
                           echo "Customer will pay the fees";
                       }

                    ?></span></p>
        </div>


        <div id="updatebutton" class="btmmargin-sm" onclick="updateStatus()"><a class="btn btn-main btn-block">تم توصيل الطلب</a></div>



    </section>
</div>
</body>
</html>
<script>
    function updateStatus() {

        console.log("request ID is: "+$("#requestid").text());

        $.ajax({
            type: 'POST',
            url: 'http://kasper-cab.com/delivery/walker/updatestatus',
            dataType: 'text',
            data: {id: $("#requestid").text(), status: 3},
        }).done(function (response) {
            alert("تم تحديث حالة الطلب");
            $("#updatebutton").find("a").text("تم تحديث حالة الطلب");
        }); // Ajax Call

    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
























