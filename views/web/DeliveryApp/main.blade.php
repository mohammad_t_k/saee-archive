<html dir="ltr" lang="en" ng-app="DeliveryApp">
<head>
    <!--Resources-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <link rel="stylesheet" href= "{{asset('/DeliveryApp2/assets-2/css/styles2.css')}}">




    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- UI resources -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Khod W Haat | Login Page</title>
    <meta content="" name=keywords>
    <meta content="" name=description>
    <link rel="shortcut icon" href="favicon.ico" />

    <!-- Font -->
    <!--<link href="/kc/app/views/web/DeliveryApp/assets-2/css/font-awesome.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href= "{{asset('/DeliveryApp2/assets-2/css/font-awesome.min.css')}}" rel="stylesheet">

    
    <!-- Latest compiled and minified Bootstap CSS -->
    <!--<link rel="stylesheet" href="/kc/app/views/web/DeliveryApp/assets-2/css/bootstrap.min.css" type="text/css">-->
    <link rel="stylesheet" href= "{{asset('/DeliveryApp2/assets-2/css/bootstrap.min.css')}}" type="text/css">

    
    <!-- CSS Styles -->
   <!-- <link href="/kc/app/views/web/DeliveryApp/assets-2/css/styles.css" rel="stylesheet" type="text/css" />-->
    <link rel="stylesheet" href= "{{asset('/DeliveryApp2/assets-2/css/styles.css')}}">





    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- Inject views in the below tags-->
    <meta name="viewport" content="initial-scale=1"/>
    <script language="JavaScript">
    </script>
</head>

<!-- main controller vars acceptable by all controllers and views style="font-family: myFirstFont" layout="column" -->
<!-- <!--layout="column" class="relative" layout-fill role="main" -->
<body  class="bg-grey-2"  ng-controller="MainCtrl">
<div>
    <!-- inject views here-->
    <div ng-view></div>
</div>





<!-- Angular Material requires Angular.js Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.js"></script>

<!-- Angular Material Library -->
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.1/angular-material.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular-material-icons/0.7.1/angular-material-icons.min.js"></script>

<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>



<!-- load app main script for configuration and main controllers -->
<script src="{{asset('/DeliveryApp2/app.js') }}"></script>   
 <!-- Load Controllers-->

<script src="{{asset('/DeliveryApp2/controllers/logincontroller.js') }}"></script>  
<script src="{{asset('/DeliveryApp2/controllers/registercontroller.js') }}"></script>   
<script src="{{asset('/DeliveryApp2/controllers/requestcontroller.js') }}"></script>   
<script src="{{asset('/DeliveryApp2/controllers/profilecontroller.js') }}"></script>   



<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="{{asset('/DeliveryApp2/assets-2/js/bootstrap.min.js') }}"></script>   



<link rel="stylesheet" href= "{{asset('/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.min.css')}}">
<script src="{{asset('/DeliveryApp2/assets-2/datetimepicker-master/build/jquery.datetimepicker.full.min.js') }}"></script>   





</body>
</html>