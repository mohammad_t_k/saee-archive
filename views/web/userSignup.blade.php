<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">


        <title>{{Config::get('app.website_title')}}</title>

        <?php
        $theme = Theme::all();
        $active = '#000066';
        $logo = '/image/logo.png';
        $favicon = '/image/favicon.ico';
        foreach ($theme as $themes) {
            $active = $themes->active_color;
            $favicon = '/uploads/' . $themes->favicon;
            $logo = '/uploads/' . $themes->logo;
        }
        if ($logo == '') {
            $logo = '/image/logo.png';
        }
        if ($favicon == '') {
            $favicon = '/image/favicon.ico';
        }
        ?>

        <link rel="icon" type="image/ico" href="<?php echo asset_url(); ?><?php echo $favicon; ?>">


        <?php
        $theme = Theme::all();
        $active = '#000066';
        $logo = '/image/logo.png';
        $favicon = '/image/favicon.ico';
        foreach ($theme as $themes) {
            $active = $themes->active_color;
            $favicon = '/uploads/' . $themes->favicon;
            $logo = '/uploads/' . $themes->logo;
        }
        if ($logo == '/uploads/') {
            $logo = '/image/logo.png';
        }
        if ($favicon == '/uploads/') {
            $favicon = '/image/favicon.ico';
        }
        ?>

        <link rel="icon" type="image/ico" href="<?php echo asset_url(); ?><?php echo $favicon; ?>">

        <!-- Bootstrap core CSS -->
        <link href="<?php echo asset_url(); ?>/web/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo asset_url(); ?>/web/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="<?php echo asset_url(); ?>/web/css/style.css" rel="stylesheet">
        <link href="<?php echo asset_url(); ?>/web/css/style-responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="http://momentjs.com/downloads/moment.min.js"></script>
        <script src="http://momentjs.com/downloads/moment-timezone-with-data.min.js"></script>
        <script src="https://bitbucket.org/pellepim/jstimezonedetect/downloads/jstz-1.0.4.min.js"></script>
        <script src="<?php echo asset_url(); ?>/web/js/validation.js"></script>
        <script type="text/javascript" src="<?php echo asset_url(); ?>/website/scripts/timezone/jstz.js"></script>
        <script type="text/javascript" src="<?php echo asset_url(); ?>/website/scripts/timezone/prettify.js"></script>
        <script type="text/javascript" src="<?php echo asset_url(); ?>/website/scripts/timezone/jquery.js"></script>
        <script type="text/javascript">
$(document).ready(function () {
    var tz = jstz.determine();

    response_text = 'No timezone found';

    if (typeof (tz) === 'undefined') {
        response_text = 'No timezone found';
    }
    else {
        response_text = tz.name();
    }

    $('#tz_info').val(response_text);

    $('#code-example').html("> var timezone = jstz.determine();\n" +
            "> timezone.name(); \n" +
            "\"" + tz.name() + "\"\n\n");
    /*$('#tz_info').show();*/
    prettyPrint();
});
        </script>
    </head>

    <body>

        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->

        <div id="login-page">
            <div class="container">
                <a href="/" class="col-md-2 col-xs-12"><img class="imghome" src="<?php echo asset_url(); ?><?php echo $logo; ?>" alt="" style="width: 100%"></a>
                <form class="form-login" action="{{route('/user/save')}}" method="post">
                    <h2 class="form-login-heading">Register</h2>
                    <div class="login-wrap">
                        <input type="text" name="first_name" class="form-control" required placeholder="First Name(الاسم الاول)" autofocus
                               @if(Session::has('first_name'))
                               value ={{ Session::get('first_name') }}
                                @endif><br>
                        <input type="text" name="last_name" class="form-control" required placeholder="Last Name(الكنية)" @if(Session::has('last_name'))
                        value ={{ Session::get('last_name') }}
                                @endif><br>
                        <span id="no_mobile_error1" style="display: none"> </span>
                        <input type="text" id="phone" name="phone" required class="form-control" placeholder="Mobile Number(هاتف) +966501234567" maxlength="13"
                               @if(Session::has('phone'))
                               value ={{ Session::get('phone') }}
                                @endif>
                        <p id="phoneErr" class="text-danger"></p><br>


<!--<input type="text" name="phone" required class="form-control" placeholder="Mobile Number" ><br>-->
<!--<input type="text" name="email" required class="form-control" placeholder="Email Address" >-->
                        <span id="no_email_error1" style="display: none"> </span>
                        <input type="text" name="email" required class="form-control" placeholder="Email Address(البريد الإلكتروني)"  onblur="ValidateEmail(1)" id="email_check1" required="" @if(Session::has('email'))
                        value ={{ Session::get('email') }}
                                @endif>
                        <br>
                        <input type="password" name="password" required="" class="form-control" placeholder="Password(كلمه السر)"><br>
                        <input type="hidden" name="timezone" id="tz_info" value="">
                        <input type="text" name="referral_code" class="form-control" placeholder="Referral Code" >
                        <br>

                        @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                        @endif

                        <button class="btn btn-theme btn-block" type="submit" id="provider-signup"onclick="if(!validateAllInputs()) return false;"><i class="fa fa-lock"></i> Register(تسجيل)</button>
                        <hr>
                        <div class="registration">
                            Do you have an account already?<br>(هل لديك حساب بالفعل؟)<br/>
                            <a class="" href="/user/signin">
                                Sign in(تسجيل الدخول)
                            </a>
                        </div>

                    </div>
                </form>

            </div>
        </div>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo asset_url(); ?>/web/js/jquery.js"></script>
        <script src="<?php echo asset_url(); ?>/web/js/bootstrap.min.js"></script>

        <!--BACKSTRETCH-->
        <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
        <script type="text/javascript" src="<?php echo asset_url(); ?>/web/js/jquery.backstretch.min.js"></script>
        <script>
                            $.backstretch("<?php echo asset_url(); ?>/web/img/bg.jpg", {speed: 500});
        </script>

        <script src="<?php echo asset_url(); ?>/web/js/bootstrap-tour.js"></script>
        <script type="text/javascript">
                            var tour = new Tour(
                                    {
                                        name: "userappSignup",
                                    });

                            // Add your steps. Not too many, you don't really want to get your users sleepy
                            tour.addSteps([
                                {
                                    element: "#user-signup",
                                    title: "Sign up as a new User",
                                    content: "Please fill your details and Click on Register button",
                                },
                            ]);

                            // Initialize the tour
                            tour.init();

                            // Start the tour
                            tour.start();
                            var tz = jstz.determine();
                            console.log(tz.name());
                            $("#timezone").val(tz.name());
        </script>
        <script type="text/javascript">
            function validateAllInputs(){
                var result = true;
                // Validating Sign Up Captain
                var phone  = document.getElementById("phone");
                if(phone.value.substring(0, 4)!="+966"){
            document.getElementById("phoneErr").innerHTML = "*Phone number must start with +966 e.g.+966501234567.<br>(يجب أن يبدأ رقم الهاتف مع +966 مثلا +966501234567)";
            result = false;
        }else if(phone.value.length!=13){
                    document.getElementById("phoneErr").innerHTML = "*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(يجب أن يكون رقم لهاتف 13 رقما تبدأ ب 966)";
                result = false;
        }else{
            document.getElementById("phoneErr").innerHTML = "";
        }
                return result;
            }
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>



    </body>
</html>
