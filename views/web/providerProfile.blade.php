@extends('web.providerLayout')

@section('content')
<?php $counter = 1; ?>
<div class="col-md-12 mt">
    @if($user->picture == "")
    <div class="alert alert-danger">
        <b>{{ "Please Select Profile Picture(يرجى تحديد صورة الملف الشخصي ...)" }}</b> 
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger">
        <b>{{ Session::get('error') }}</b> 
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success">
        <b>{{ Session::get('success') }}</b> 
    </div>
    @endif
    @if(isset($error))
    <div class="alert alert-danger">
        <b>{{ $error }}</b> 
    </div>
    @endif
    @if(isset($success))
    <div class="alert alert-success">
        <b>{{ $success }}</b> 
    </div>
    @endif

  <!--  <div class="content-panel">
        <h4>Available to drive?(متاح للقيادة؟)</h4><br>
        <form>
            <div class="form-group" >
                <label class="col-sm-2 col-sm-2 control-label"  id="flow22">Availabity(توافر)</label>
                <div class="col-sm-6">
                    <input type="checkbox" data-toggle="switch" name="avaialbility" id="avaialability" <?= $user->is_active ? "checked" : "" ?>/>
                </div>
            </div>
        </form>
    </div>-->
<style>
#map_canvas {
  height: 300px;
  width: 100%;
  margin: 0;
}

#map_canvas .centerMarker {
  position: absolute;
  /*url of the marker*/
  background: url(../web/images/marker.png) no-repeat;
  /*center the marker*/
  top: 50%;
  left: 50%;
  z-index: 1;
  /*fix offset when needed*/
  margin-left: -10px;
  margin-top: -34px;
  /*size of the image*/
  height: 34px;
  width: 20px;
  cursor: pointer;
}
</style>

<link rel="stylesheet" href="../web/css/jquery.calendars.picker.css"/>

    <div class="content-panel">

        <br><h4>Update Profile(تحديث الملف)</h4><br>
        <form class="form-horizontal style-form" method="post" action="{{ URL::Route('updateProviderProfile') }}" enctype="multipart/form-data">
			<input type="hidden" name="email" value ="{{$user->email}}" />
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">First Name<br>(الاسم الاول)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="first_name"  name="first_name" value="{{ $user->first_name }}">
					<p id="first_nameErr" class="text-danger"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Last Name<br>(الكنية)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}">
					<p id="last_nameErr" class="text-danger"></p>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Government ID Number<br>(رقم الهوية الحكومة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="government_id_number" name="government_id_number" value="{{ $user->government_id_number }}" onkeypress="return isNumber(event)" maxlength="10" placeholder='0000000000'>
					<p id="government_id_numberErr" class="text-danger"></p>
                </div>
				
            </div>
			<div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Date of Birth<br>(تاريخ الولادة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" value="{{ $user->date_of_birth }}" 
                    placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>
					<p id="date_of_birthErr" class="text-danger"></p>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">License Expiry Date<br>(تاريخ إنتهاء الرخصة)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="license_expiry_date" name="license_expiry_date" value="{{ $user->license_expiry_date }}"
                    placeholder='dd-mm-yyyy(يوم-شهر-سنة)'>					
					<p id="license_expiry_dateErr" class="text-danger"></p>
                </div>
            </div>
            <div class="form-group">
                <span id="no_mobile_error1" style="display: none"> </span>
                <label class="col-sm-2 col-sm-2 control-label">Phone<br>(هاتف)</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="phone" name="phone"  value="{{ $user->phone }}"onkeypress="return Ismobile(event, 1);" maxlength="13" placeholder='+966xxxxxxxxx'>
					<p id="phoneErr" class="text-danger"></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Photo<br>(صورة فوتوغرافية)</label>
                <div class="col-sm-1">
                    <img src="<?php
                    if ($user->picture == "") {
                        echo asset_url() . "/web/default_profile.png";
                    } else {
                        echo $user->picture;
                    }
                    ?>" class="img-circle" width="60">
                </div>
                <div class="col-sm-5" style="position:relative;top:15px;">
                    <input type="file" class="form-control" name="picture" >
                </div>
            </div>
			<span style="display:none;">{{$i = 0;}}</span>

            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Select Home location on Map<br>(حدد موقع منزلك على الخريطة)</label>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1">
                        <label>Home Latitude</label>
                        <input type="text" id="address_latitude" name="address_latitude" value ="{{$user->address_latitude}} "/>
                    </div>
                    <div class="col-sm-3">
                        <label>Home Longitude</label>
                        <input type="text" id="address_longitude" name="address_longitude" value ="{{$user->address_longitude}}"/>
                    </div>
                    <div class="col-sm-8">
                        <div id="map_canvas"></div>
                        <p id="homeAddressErr" class="text-danger"></p>                        
                    </div>
                    
                </div>
            </div>
			
            <span class="col-sm-2"></span>
            <button id="update" type="submit" class="btn btn-info" onclick="if(!validateAllInputs()) return false;">Update Profile<br>(تحديث الملف)</button>
            <button type="reset" class="btn btn-info">Reset إعادة تعيين</button>

        </form>
    </div>

    <div class="content-panel">
        <h4>Change Password<br> (تغيير كلمة السر)</h4><br>
        <form class="form-horizontal style-form" method="post" action="{{ URL::Route('updateProviderPassword') }}">
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Current Password<br> (كلمة السر الحالية)</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" name="current_password" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">New Password<br> (كلمة السر الجديدة)</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" name="new_password" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Confirm Password<br> (تأكيد كلمة المرور)</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" name="confirm_password" value="">
                </div>
            </div>
            <span class="col-sm-2"></span>
            <button id="pass" type="submit" class="btn btn-info">Change Password<br> تغيير كلمة السر</button>
            <button type="reset" class="btn btn-info">Reset إعادة تعيين</button>

        </form>
    </div>
</div>

<script type="text/javascript">
    /*$(function () {
        $("#avaialability").change(function () {
            $.ajax({
                type: 'post',
                url: '{{ URL::Route('toggle_availability') }}',
                success: function (msg) {
                    console.log("state changed");
                },
                processData: false,
            });
        });
    });*/
</script>

<script type="text/javascript">
    /*var tour = new Tour(
            {
                name: "providerappProfile",
            });

    // Add your steps. Not too many, you don't really want to get your users sleepy
    tour.addSteps([
        {
            element: "#flow22",
            title: "Setting Availability",
            content: "Toggle your service availability here",
            placement: "top",
        },
    ]);

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.start();*/
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
<script>
function initMap() {
    try{
    var user = JSON.parse('<?php echo $user?>');
    var  address_latitude=21.2854;
    var  address_longitude=39.2376;
    if(user.address_latitude!=0&&user.address_longitude!=0){
        address_latitude=user.address_latitude;
        document.getElementById('address_latitude').value=address_latitude;
        address_longitude=user.address_longitude;
        document.getElementById('address_longitude').value=address_longitude;
    }
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(address_latitude, address_longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);
      google.maps.event.addListener(map,'center_changed', function() {
        document.getElementById('address_latitude').value = map.getCenter().lat();
        document.getElementById('address_longitude').value = map.getCenter().lng();
      });
      $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
        //do something onclick
        .click(function() {
          var that = $(this);
          if (!that.data('win')) {
            that.data('win', new google.maps.InfoWindow({
              content: 'this is the center'
            }));
            that.data('win').bindTo('position', map, 'center');
          }
          that.data('win').open(map);
        });}
    	catch(e){
    	alert(e);
    }
}

google.maps.event.addDomListener(window, 'load', initMap);</script>

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<script src="../web/js/hijricalender/jquery.plugin.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.plus.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.picker.js"></script>
<script src="../web/js/hijricalender/jquery.calendars.ummalqura.js"></script>

<script>
$(function() {
	var calendar = $.calendars.instance('ummalqura');
	$('#date_of_birth').calendarsPicker({calendar: calendar,onSelect: function(date) {  $('#date_of_birth').val( date[0].formatDate('dd-mm-yyyy')); }});
	$('#license_expiry_date').calendarsPicker({calendar: calendar,onSelect: function(date) {  $('#license_expiry_date').val( date[0].formatDate('dd-mm-yyyy')); }});
	var governmentIdNumber  = document.getElementById("government_id_number");
    var phone  = document.getElementById("phone");
	if (governmentIdNumber.value=="0"){
			governmentIdNumber.value="";
	}
    if (phone.value==""){
            phone.value="+966";
    }

    var waslresultCode = '<?php if(Session::has('waslresultCode')) echo Session::get('waslresultCode');?>';
    var waslError = '<?php if(Session::has('error')) echo Session::get('error');?>';
    switch(waslresultCode){
        case "421":
        if(waslError.search("Captain Identity Number")!=-1)
            document.getElementById("government_id_numberErr").innerHTML="Invalid values at Captain Government Identity Number<br>قيم غير صالحة في الكابتن الحكومة رقم الهوية";
        else if(waslError.search("Mobile Number")!=-1)            
            document.getElementById("phoneErr").innerHTML="Invalid value of Phone Number, Should be like this format +9665xxxxxxxxxx<br>+قيمة غير صالحة لرقم الهاتف، ينبغي أن يكون مثل هذا الشكل 9665xxxxxxxxxx";
        else if(waslError.search("Date of Birth")!=-1)          
            document.getElementById("date_of_birthErr").innerHTML="Invalid values for Date of Birth, Should be like this format dd-MM-YYYY<br>قيم غير صالحة لتاريخ الميلاد، وينبغي أن يكون مثل هذا الشكل DD-MM-YYYY";
        break;
        case "503":
        document.getElementById("date_of_birthErr").innerHTML="Yakeen Failure: Captain Date Of Birth does not match Government Identity Number records<br>يقين الفشل: الكابتن تاريخ الميلاد لا تطابق سجلات الحكومة رقم الهوية";
        break;
        default:
        break;
    }
});
</script>
<script type="text/javascript">
function validateAllInputs(){ 
		var result = true;
		// Validating Sign Up Captain   
		var firstName  = document.getElementById("first_name");
		var lastName  = document.getElementById("last_name");
		var governmentIdNumber  = document.getElementById("government_id_number");
		var dateOfBirth  = document.getElementById("date_of_birth");
		var phone  = document.getElementById("phone");
		var licenseExpiryDate  = document.getElementById("license_expiry_date");
        var address_latitude=document.getElementById('address_latitude').value;
        var address_longitude=document.getElementById('address_longitude').value;
        firstName.focus();
		if (firstName.value==""){
			document.getElementById("first_nameErr").innerHTML = "*Please enter your First Name.(الرجاء إدخال الاسم الأول)";
			result = false;
		}else {
				document.getElementById("first_nameErr").innerHTML = "";
			}
		if (lastName.value==""){
			document.getElementById("last_nameErr").innerHTML = "*Please enter your Last Name.<br>(رجاء ادخل اسمك الاخير)";
			result = false;
		}else {
				document.getElementById("last_nameErr").innerHTML = "";
			}
			if (phone.value==""){
    			document.getElementById("phoneErr").innerHTML = "*Please enter your Phone Number.<br>(يرجى إدخال رقم الهاتف الخاص بك)";
    			result = false;
		      }else if(phone.value.substring(0, 4)!="+966"){
                    document.getElementById("phoneErr").innerHTML = "*Phone number must start with +966 e.g.+966501234567.<br>(+يجب أن يبدأ رقم ا+لهاتف مع 966 مثلا 966501234567)";
                result = false;
                }else if(phone.value.length!=13){
                    document.getElementById("phoneErr").innerHTML = "*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>(* يجب أن يبدأ رقم الهاتف بالرقم +966 على سبيل المثال + 966501234567)";
                result = false;
                }else{
				    document.getElementById("phoneErr").innerHTML = "";
			 }
			
			
		if (governmentIdNumber.value==""){
			document.getElementById("government_id_numberErr").innerHTML = "*Please enter your Government Id Number.<br>(الرجاء إدخال حكومتكم إيد عدد)";
			result = false;
		} else if (isNaN(governmentIdNumber.value)){
				document.getElementById("government_id_numberErr").innerHTML = "*Please enter your Government Id Number correctly.<br>(الرجاء أدخل الحكومة عدد رقم صحيح)";
				result = false;
			} else {
				document.getElementById("government_id_numberErr").innerHTML = "";
			}			
			if (dateOfBirth.value==""){
			document.getElementById("date_of_birthErr").innerHTML = "*Please select Date of Birth.<br>(يرجى تحديد تاريخ الميلاد)";
			result = false;
		}else {
				document.getElementById("date_of_birthErr").innerHTML = "";
			}	
			
			if (licenseExpiryDate.value==""){
			document.getElementById("license_expiry_dateErr").innerHTML = "*Please select License Expiry Date.<br>(يرجى تحديد الترخيص تاريخ انتهاء الصلاحية)";
			result = false;
		}else {
				document.getElementById("license_expiry_dateErr").innerHTML = "";
			}
        if(address_latitude==0||address_longitude==0){
            document.getElementById("homeAddressErr").innerHTML = "*Please select your home location on Map(الرجاء تحديد موقع منزلك على الخريطة)";
            result = false;
        } else {
            document.getElementById("homeAddressErr").innerHTML = "";
        }
		return result;
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@stop