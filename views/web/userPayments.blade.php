@extends('web.layout')

@section('content')

<div class="col-md-12 mt">

    @if(Session::has('message'))
    <div class="alert alert-{{ Session::get('type') }}">
        <b>{{ Session::get('message') }}</b> 
    </div>
    @endif

    <div class="content-panel" >
        <div class="row">
            <div class="col-lg-6"><h4>User Credit: </h4> </div>
            <div class="col-lg-6"><h4><?php echo $credit ?> </h4> </div>
            <div class="col-lg-6"><h4> User Stored Cards </h4> </div>

            <?php if ($payments) { ?>
                @foreach ( $payments as $payment)
                <div class="col-lg-12" style="padding-left:20px;">
                    <div class="col-lg-3" style="padding:10px">
                        <img src="{{ asset_url() }}/web/credit_card.png">&nbsp;&nbsp;&nbsp;Credit Card
                    </div>
                    <div class="col-lg-3" style="padding:10px;padding-top:15px;">
                        **** {{ $payment->last_four }}
                    </div>
                    <div class="col-lg-3" style="padding:10px;padding-top:15px;">
                        <a href="{{route('/user/payment/delete',$payment->id)}}">Delete</a>
                    </div>
                </div>
                @endforeach
            <?php } else{?>
                <div class="col-lg-12"><h4> No Cards Found, Please use Kasper-Cab App to add new cards. </h4> </div>
            <?php }?>
            <div class="col-lg-7"  ></div>

            <div class="col-lg-12" style="padding:20px;" >
                <h4 >Referal Credits</h4><br>
                <div class="col-lg-6" >
                    <div class="col-lg-6"  style="padding-top:10px">
                        Total Referrals
                    </div>
                    <div class="col-lg-2"  style="padding-top:10px;text-align:right;">
                        {{ $ledger?$ledger->total_referrals:0 }}
                    </div>
                    <div class="col-lg-6"  style="padding-top:10px">
                        Credits Earned
                    </div>
                    <div class="col-lg-2"  style="padding-top:10px;text-align:right;">
                        {{ $ledger?round($ledger->amount_earned):0 }}
                    </div>
                    <div class="col-lg-6"  style="padding-top:10px;padding-bottom:10px; ">
                        Credits Spent
                    </div>
                    <div class="col-lg-2"  style="padding-top:10px;padding-bottom:10px;text-align:right;">
                        {{ $ledger?round($ledger->amount_spent):0 }}
                    </div>
                    <div class="col-lg-6"  style="padding-top:10px;border-top: #cccccc solid 1px;">
                        <b>Balance Credits</b>
                    </div>
                    <div class="col-lg-2"  style="padding-top:10px;text-align:right;border-top: #cccccc solid 1px;">
                        <b>{{ $ledger?round($ledger->amount_earned - $ledger->amount_spent):0 }}</b>
                    </div>
                </div>
            </div>

            <div class="col-lg-7" style="border-bottom: #cccccc solid 1px;" ></div>

            <div class="col-lg-12" style="padding:20px;">
                <h4>Personlize Referral Code</h4><br>
                <div class="col-lg-6" >
                    <form method="post" action="{{route('/user/update_code')}}">
                        <div class="col-lg-6"  style="padding-top:10px">
                            <input type="text" class="form-control" placeholder="Referral Code" name="code" value="{{ $ledger?$ledger->referral_code:'' }}">
                        </div>
                        <div class="col-lg-2"  style="padding-top:10px;text-align:right;">
                            <button type="submit" class="btn btn-theme">Change Code</button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>

</div>


<script type="text/javascript">

    $(function () {
        $("#add-new").click(function () {
            $("#add-card").toggle();
        });
    });
</script>



@stop 