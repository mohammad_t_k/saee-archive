<?php


?>


        <!doctype html>


<html lang="en">
<head>

    <meta name="csrf-token" content="{{ csrf_token() }}">


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="/kc/public/Dev-Server-Resources/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="/kc/public/Dev-Server-Resources/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png">


    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.grey-orange.min.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles3.css">
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles5.css">


    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1.11.1/jquery.min.js"></script>


</head>
<body>
<div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
    <main class="mdl-layout__content">
        <div class="demo-blog__posts mdl-grid">


            <div style="width: 450px">
                <div class="container text-center">

                    <div class="col-md-1"></div>
                    <div class="col-md-10">


                        <form class="form-horizontal style-form" method="post"
                              action="{{ URL::Route('registerSimple') }}" enctype="multipart/form-data">
                            <fieldset>

                                <div class="form-group">
                                    <div class="col-md-5 col-sm-5">
                                        <span class="input-group-addon">الاسم</span>
                                        <input id="name" name="name" class="form-control"
                                               value="" type="text">
                                        <p id="first_nameErr" class="text-danger"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="input-group">
                                            <span class="input-group-addon">966</span>
                                            <input id="mobile" name="mobile" maxlength="9" class="form-control"
                                                   placeholder="  رقم الجوال  "
                                                   value="" type="text">
                                        </div>
                                        <p id="phoneErr" class="text-danger"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-sm-5">
                                        <span class="input-group-addon"> البريد الإلكتروني</span>
                                        <input id="email" name="email"
                                               class="form-control input-md" type="email">
                                        <p id="emailErr" class="text-danger"></p>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-5 col-sm-5">
                                        <span class="input-group-addon">كلمة السر</span>
                                        <input id="password" name="password"
                                               class="form-control input-md" type="password">
                                        <p id="passwordErr" class="text-danger"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-5 col-sm-5">
                                        <span class="input-group-addon">المدينة</span>
                                        <select id="city" name="city" class="form-control" required="">
                                            <option value="1" style="text-align: center;">
                                                جدة
                                            </option>
                                            <option value="2">المدينة المنورة</option>
                                            <option value="3">الرياض</option>
                                            <option value="4">الدمام</option>
                                            <option value="5">مكة المكرمة</option>
                                        </select>
                                    </div>
                                </div>


                                <button style="background-color: rgb(204, 204, 204);
                                        padding: 8px 55px;
                                        font-size: 22px;
                                        border-radius: 0px;
                                        margin: 5px 0px 0px;
                                        display: inline-block;
                                        padding: 6px 12px;
                                        margin-bottom: 0px;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        cursor: pointer;
                                        -moz-user-select: none;
                                        background-image: none;
                                        border: 1px solid transparent;
                                        border-radius: 4px;
                                        color: rgb(255, 255, 255);
                                        background-color: rgb(204, 204, 204);
                                        border-color: rgb(204, 204, 204);" class="btn btn-info"
                                        onclick="if(!validateAllInputs()) return false;">
                                    Apply
                                </button>


                            </fieldset>


                        </form>
                    </div>
                </div>
            </div>
    </main>
    <div class="mdl-layout__obfuscator"></div>
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</body>


<script>
    function validateAllInputs() {


        var result = true;
        var firstName = document.getElementById("name");
        var phone = document.getElementById("mobile");
        var email = document.getElementById("email");
        var password = document.getElementById("password");



        if(!/^[\w ]{1,20}\ [\w ]{1,20}$/.test(firstName.value)){
            document.getElementById("first_nameErr").innerHTML = "*Please enter your First Name and last name.( الرجاء ادخال الاسم الاول ملحقا بالكنية مثال: احمد العتيبي)";
            result = false ;
        }

      /*  if (firstName.value == "") {
            document.getElementById("first_nameErr").innerHTML = "*Please enter your First Name and last name.( الرجاء ادخال الاسم الاول ملحقا بالكنية مثال: احمد العتيبي)";
            result = false;
        }*//*{
            document.getElementById("first_nameErr").innerHTML = "*Please enter your First Name and last name.( الرجاء ادخال الاسم الاول ملحقا بالكنية مثال: احمد العتيبي)";
             return false;
        }
*/

        else {
            document.getElementById("first_nameErr").innerHTML = "";
        }
        if (phone.value == "") {
            document.getElementById("phoneErr").innerHTML = "*Please enter your Phone Number.<br>(يرجى إدخال رقم الهاتف الخاص بك)";
            result = false;
        }/*else if(phone.value.substring(0, 3)!="966"){
         document.getElementById("phoneErr").innerHTML = "*Phone number must start with 966 e.g.966501234567.<br>(يجب أن يبدأ رقم الهاتف مع 966 مثلا 966501234567)";
         result = false;
         }*/ else if (phone.value.length != 9) {
            document.getElementById("phoneErr").innerHTML = "*Phone number must have 12 digits starting with 966 e.g.966501234567.<br>(يجب أن يكون رقم الهاتف 12 رقما تبدأ ب 966)";
            result = false;
        } else {
            document.getElementById("phoneErr").innerHTML = "";
        }

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value)) {
            document.getElementById("passwordErr").innerHTML = "";
        } else {
            document.getElementById("emailErr").innerHTML = "*Please enter a valid email ID.<br>(الرجاء ادخال ايميل صحيح)";
            result = false;
        }
        if (password.value == "") {
            document.getElementById("passwordErr").innerHTML = "Please pick a password.<br>(الرجاء اختيار كلمة مرور)";
            result = false

        } else {

            document.getElementById("passwordErr").innerHTML = "";
        }


        /* $.ajaxSetup({
         headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });

         $.ajax({
         type: 'get',
         url: 'http://34.200.125.97/kc/public/provider/registersimple',
         dataType: 'text',
         data: {
         name: $("#name").val(),
         mobile: "966"+$("#mobile").val(),
         email: $("#email").val(),
         password: $("#password").val(),
         city: $("#city option:selected").text()
         },

         }).done(function (text) {

         alert(text);
         console.log("done");

         });// Ajax Call

         }*/


        //must return result to the form above and call route of next page


        return result;


    }

</script>
</html>
