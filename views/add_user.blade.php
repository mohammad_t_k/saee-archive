@extends('layout')

@section('content')


    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Add {{ trans('customize.Provider');}}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" class="form" id="main-form" method="post" action="{{ URL::Route('AdminAddUser') }}"
              enctype="multipart/form-data">
            <input type="hidden" name="id" value="0>">

            <div class="box-body">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="first_name" value="" placeholder="First Name">

                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <input class="form-control" type="text" name="last_name" value="" placeholder="Last Name">

                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" value="" placeholder="Email">

                </div>

                <div class="form-group">
                    <label>Phone</label>
                    <input class="form-control" type="text" name="phone" value="" placeholder="Phone">

                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Add user password">
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->title }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label>Picture</label>
                    <input class="form-control" type="file" name="pic">

                    <p class="help-block">Please Upload image in jpg, png format.</p>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat btn-block">Add User</button>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $("#main-form").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                role_id: "required",
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    digits: true,
                }
            }
        });
    </script>


@stop