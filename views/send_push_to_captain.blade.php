
@extends('layout')

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- form start -->
        <div>
            <h4><?if(isset($message)){echo $message;} ?></h4>
        </div>
        <form method="post" id="basic" action="{{ URL::Route('SendPushToCaptainPost') }}">
            <div class="form-group col-md-12 col-sm-12">
                <label>Message</label>
                <input type="text" class="form-control" name="message" placeholder="Type Message" id="message" value="" required>
            </div>
            <div class="box-footer">
                <button type="submit" id="add" class="btn btn-primary btn-flat btn-block">Send Message to Captains</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $("#message").validate({
            rules: {
                name: "required",
            }
        });
    </script>
@stop