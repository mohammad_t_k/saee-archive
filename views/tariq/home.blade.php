@if( Auth::user()->role < 2)

  <script>window.location = "/warehouse/403";</script>
  
@endif
<?php

	$layout = 'warehouse.layout';
?>

@extends($layout)


@section('content')


 <!-- page content -->
 
                    
<div class="right_col" role="main">
    
    <!-- top tiles -->
    <div class="row tile_count">
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>
          <div class="count"><?php echo $allitems ?> </div>
          
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> Success Rate </span>
          <div class="count red"><?php echo number_format($successrate,2) ?>%</div>
        </div>
    
    </div>
    <div class="row tile_count">
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-truck"></i>Picked up not scaned</span>
            <div class="count "><?php echo $onwayitems ?></div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-home"></i> Ready to dispatch (New)</span>
          <div class="count green"><?php echo $arriveditems ?></div>
        </div>
        
    </div>
    
    <div class="row tile_count">
    
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-home"></i> Ready to dispatch (Returned)</span>
          <div class="count green"><?php echo $undelivreditems ?></div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-car"></i> With Captains ALL</span>
            <div class="count blue"><?php echo $outfordeliveryitems  ?></div>
        </div>
        
        
        
    </div>
    
    <div class="row tile_count">
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-thumbs-ub"></i> Delivered </span>
            <div class="count green"><?php echo $delivreditems ?></div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-thumbs-down"></i> Returned to JC</span>
          <div class="count blue"><?php echo $returnedToJC ?></div>
        </div>
    </div>
<!-- /top tiles -->



<br />

    
    </div>
</div>
        <!-- /page content -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
 <!-- ECharts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script> 


@stop
