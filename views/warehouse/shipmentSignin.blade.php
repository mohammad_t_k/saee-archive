@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif


@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')

@section('content')

    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Sign-In New Shipments</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Sign-In new Incoming Shipments to our Warehouse</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" required="required" class="form-control"
                                                type="text" onchange="onChangeCompanyCity()">
                                            @foreach($companies as $compan)
                                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control"
                                                onchange="onChangeCompanyCity()">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="shipmentIDs" required="required" class="form-control" type="text"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <h2>In Route Dates/Shipments
                                        Count </h2>
                                    <table name="newshipmentscount" id="newshipmentscount"
                                           class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Shipment Date</th>
                                            <th>Count</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Shipment Date</th>
                                            <th>Count</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h2>Shipments Status </h2>
                                    <table name="signedInshipments" id="signedInshipments"
                                           class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <script>


        $(document).ready(function () {
            $('#shipmentIDs').on('keypress', function(e) {
                if (e.which == 13) {
                    var shipID = document.getElementById("shipmentIDs").value.replace(/\s/g, "");
                    var company = document.getElementById("company").value;
                    if (/JC[0-9]{8}KS/.test(shipID) || /OS[0-9]{8}KS/.test(shipID) || /^[a-z0-9]+$/i.test(shipID)) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/singleshipmentsigninpost',
                            dataType: 'text',
                            data: {
                                to: 2,
                                id: shipID,
                                company: company
                            },
                        }).done(function (response) {
                            if (response) {
                                responseObj = JSON.parse(response);
                                var table = document.getElementById("signedInshipments");
                                var row = table.insertRow(1);
                                var cell1 = row.insertCell(0);
                                var cell2 = row.insertCell(1);
                                cell1.innerHTML = responseObj.waybill;
                                cell2.innerHTML = responseObj.success ? 'Success' : 'Failed';
                                var row = document.getElementById(responseObj.new_shipment_date);
                                if(row){
                                    var counttd = row.getElementsByTagName("td")[1];
                                    var counts = counttd.innerHTML.split('/');
                                    var done=parseInt(counts[0])+1;
                                    row.getElementsByTagName("td")[1].innerHTML=done+'/'+counts[1];
                                }else{
                                    var table = document.getElementById("newshipmentscount");
                                    var row = table.insertRow(1);
                                    row.id=responseObj.new_shipment_date;
                                    var cell1 = row.insertCell(0);
                                    var cell2 = row.insertCell(1);
                                    cell1.innerHTML = responseObj.new_shipment_date;
                                    cell2.innerHTML = '1/1';
                                }
                            }
                            else {
                                alert('Something went wrong!');
                            }
                        });
                    }else{
                        alert('Invalid input please scan again');
                    }
                    document.getElementById("shipmentIDs").value='';
                }
            });
            $("#submitForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var company = document.getElementById("company").value;

                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        /*

                           /^[a-z0-9]+$/i

                           ^         Start of string
                           [a-z0-9]  a or b or c or ... z or 0 or 1 or ... 9
                           +         one or more times (change to * to allow empty string)
                           $         end of string
                           /i        case-insensitive

                       */
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            /* if (/[0-9]{8}KS/.test(ids[i]))
                             {
                                 correctIDs.push('JC' + ids[i]);
                                 correctedIDs.push(ids[i]);
                             }
                             else if (/C[0-9]{8}KS/.test(ids[i]))
                             {
                                 correctIDs.push('J' + ids[i]);
                                 correctedIDs.push(ids[i]);
                             }
                            */
                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /*if (correctedIDs.length)
                    {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }*/

                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            for (var i = 0; i < finalArray.length; i++) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/singleshipmentsigninpost',
                                    dataType: 'text',
                                    data: {
                                        to: 2,
                                        id: finalArray[i],
                                        company: company
                                    },
                                }).done(function (response) {
                                    if (response) {
                                        responseObj = JSON.parse(response);
                                        var table = document.getElementById("signedInshipments");
                                        var row = table.insertRow(1);
                                        var cell1 = row.insertCell(0);
                                        var cell2 = row.insertCell(1);
                                        cell1.innerHTML = responseObj.waybill;
                                        cell2.innerHTML = responseObj.success ? 'Success' : 'Failed';
                                        var row = document.getElementById(responseObj.new_shipment_date);
                                        if(row){
                                            var counttd = row.getElementsByTagName("td")[1];
                                            var counts = counttd.innerHTML.split('/');
                                            var done=parseInt(counts[0])+1;
                                            row.getElementsByTagName("td")[1].innerHTML=done+'/'+counts[1];
                                        }else{
                                            var table = document.getElementById("newshipmentscount");
                                            var row = table.insertRow(1);
                                            row.id=responseObj.new_shipment_date;
                                            var cell1 = row.insertCell(0);
                                            var cell2 = row.insertCell(1);
                                            cell1.innerHTML = responseObj.new_shipment_date;
                                            cell2.innerHTML = '1/-';
                                        }
                                    }
                                    else {
                                        alert('Something went wrong!');
                                    }
                                });
                            }
                        }
                        else {
                            alert('You cancled!');
                        }

                    }
                    else {
                        alert('Nothing to signin!');

                    }

                }
            );
        });

    </script>
    <script>
        function onChangeCompanyCity() {
            $('#newshipmentscount tr').has('td').remove();
            $('#signedInshipments tr').has('td').remove();
            var city = document.getElementById('city').value;
            var company = document.getElementById('company').value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/getnewshipmentscountbydate',
                dataType: 'text',
                data: {city: city, company_id: company,},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                var newShipmentCountByDate = responseObj.newShipmentCountByDate;
                var table = document.getElementById("newshipmentscount");
                for (var i = 0; i < newShipmentCountByDate.length; i += 1) {
                    var row = table.insertRow(1);
                    row.id=newShipmentCountByDate[i].new_shipment_date;
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    cell1.innerHTML = newShipmentCountByDate[i].new_shipment_date;
                    cell2.innerHTML = '0/'+newShipmentCountByDate[i].count;
                }
            });
        }
    </script>
@stop

