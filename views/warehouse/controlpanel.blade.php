@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        #table_dashboard {
            color: #424242;
            font-size: 18px;
        }
        .operation {
            background-color: #e8e8e8 !important;
            color: black;
        }
        .returns {
            background-color: #cccccc !important;
            color: black;
        }
        #table_dashboard th {
            text-align: center;
        } 
        #table_dashboard td {
            text-align: center;
        } 
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
        label {
            color: #73879C
        }
        h3 {
            color: #73879C
        }
    </style>

    <style>
        .loader {
            border: 5px solid #f3f3f3;
            border-radius: 50%;
            border-top: 5px solid orange;
            border-bottom: 5px solid black;
            width: 60px;
            height: 60px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>

    <div class="right_col" role="main">
        

        <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
        <div class="row">

            <div class="tab_container">

                <input id="tab1" type="radio" name="tabs" {{ !isset($tabFilter) || $tabFilter == '1' ? 'checked' : '' }}>
                <label for="tab1" class='label2'><i class="fa fa-pencil-square-o"></i><span>Dashboard</span></label>
                
                <input id="tab2" type="radio" name="tabs" {{ isset($tabFilter) && $tabFilter == '2' ? 'checked' : '' }}>
                <label for="tab2" class='label2'><i class="fa fa-cog"></i><span>Set Configration</span></label>

                <input id="tab3" type="radio" name="tabs" {{ isset($tabFilter) && $tabFilter == '3' ? 'checked' : '' }}>
                <label for="tab3" class='label2'><i class="fa fa-sliders"></i><span>View Configration</span></label>

                <section id="content1" class="tab-content">
                    <div class="container">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <form method="get" action="/warehouse/controlpanel/dashboard">
                                    <input type="hidden" name="tabFilter" value="1" />
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                <select id="company" name="company[]" required="required" class="form-control" type="text" style="height: 130px;" multiple>
                                                    @foreach($adminCompanies as $compan)
                                                        <option value="{{$compan->id}}" {{ isset($company) && in_array($compan->id, $company) ? 'selected' : '' }}>{{$compan->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                <select name="city[]" id="city" class="form-control" style="height: 130px;" multiple>
                                                    @foreach($adminCities as $adcity)
                                                        <option value="{{$adcity->city}}" {{ isset($city) && in_array($adcity->city, $city) ? 'selected' : '' }}>{{$adcity->city}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                <input type="date" name="from" id="from" class="form-control" value="{{ isset($from) ? $from : '' }}" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                <input type="date" name="to" id="to" class="form-control" value="{{ isset($to) ? $to : '' }}" />
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button id="submitForm" class="btn btn-success">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="tab-content">
                            <div id="dataview" class="tab-pane fade in active" style="text-align: center;">
                                <h3 id="txt_live" style="color: green; display: none;" >Live ... </h3>
                                <div id="btn_load" class="btn btn-success">Start </div>
                                <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                                    <!-- Modal_of_COD -->
                                    <div class="modal fade" id="modal_csv_view" role="dialog" style="overflow: auto">
                                        <input type="hidden" id="modal_export_row_id" value="" />
                                        <div class="modal-dialog modal-lg">
                                            <!-- Modal content-->
                                            <div class="modal-content" style="color: #73879C;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title" style="text-align: left;">Select Option For Viewing <span class="modal_row_id" style="font-weight: bold;"></span> Report</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table id="table_csv_view" class="table table-striped table-bordered" style="text-align: left; margin-bottom: 0px;">
                                                        <caption><h4>Count: <span class="modal_row_count"></span></h4></caption>
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 25%;">Select</th>
                                                                <th>Option</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><input id="modal_radio_csv"  type="radio" name="radio_csv_view" class="radio-inline"></td>
                                                                <td>Download CSV</td>
                                                            </tr>
                                                            <tr>
                                                                <td><input id="modal_radio_view"  type="radio" name="radio_csv_view" class="radio-inline"></td>
                                                                <td>View Result</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div style="text-align: right; margin-right: 15px;">
                                                    <a class="btn btn-success" onclick="modal_submit(document.getElementById('modal_export_row_id').value)">Submit</a>
                                                </div>
                                                <br>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- -->
                                    <table class="table table-striped table-bordered sortable" id="table_dashboard">
                                        <thead>
                                            <tr>
                                                <th>#</i></th>
                                                <th>Status</i></th>
                                                <th>Count</i></th>
                                            </tr>
                                        </thead>
                                        <tbody id="row_container">
                                            <tr class="export-shipment" id="row_dw" style="background-color: white;">
                                                <td class="numbering"></td>
                                                <td>Duplicated Waybills</td>
                                                <td id="duplicate_waybills">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_atww" style="background-color: #b7f4f0">
                                                <td class="numbering"></td>
                                                <td>Arrived to wrong warehouse</td>
                                                <td id="arrived_to_wrong_warehouse">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_dc" style="background-color: #b7f4f0">
                                                <td class="numbering"></td>
                                                <td>Delayed Captains</td>
                                                <td id="delayed_captains">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_ir">
                                                <td class="numbering">6</td>
                                                <td>Delayed In route (Delayed Status)</td>
                                                <td id="in_route">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_crm">
                                                <td class="numbering"></td>
                                                <td>Delayed Shipments With Captains</td>
                                                <td id="cod_reception_money">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_rmib">
                                                <td class="numbering"></td>
                                                <td>Delayed Delivered With Captains</td>
                                                <td id="reception_money_in_branches">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_lar">
                                                <td class="numbering"></td>
                                                <td>Low Average Rate Captains</td>
                                                <td id="lowest_average_rate">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment operation" id="row_drs">
                                                <td class="numbering"></td>
                                                <td>Delayed Rescheduled Shipments</td>
                                                <td id="delayed_rescheduled_shipments">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment returns" id="row_cbc">
                                                <td class="numbering"></td>
                                                <td>Cancelled By Customer Care</td>
                                                <td id="cancelled_by_customer">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment returns" id="row_eda">
                                                <td class="numbering"></td>
                                                <td>Exceeded Maximum Delivery Attempts</td>
                                                <td id="exceeded_delivery_attempt">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment returns" id="row_rts">
                                                <td class="numbering"></td>
                                                <td>Exceeded Maximum Age</td>
                                                <td id="returned_to_supplier">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment returns" id="row_ptbr">
                                                <td class="numbering"></td>
                                                <td>Prepare To Be Returned To Supplier</td>
                                                <td id="prepared_to_be_rts">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_cbs">
                                                <td class="numbering"></td>
                                                <td>Created By Supplier</td>
                                                <td id="created_by_supplier">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_tbpu">
                                                <td class="numbering"></td>
                                                <td>Delayed To Be Picked Up (Delayed Status)</td>
                                                <td id="to_be_pickedup">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_rtpu">
                                                <td class="numbering"></td>
                                                <td>Delayed Reserved To Pick Up (Delayed Status)</td>
                                                <td id="reserved_to_pickup">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_pu">
                                                <td class="numbering"></td>
                                                <td>Delayed Picked Up (Delayed Status)</td>
                                                <td id="pickedup">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_dcbs">
                                                <td class="numbering"></td>
                                                <td>Delayed Created By Supplier (Delayed Status)</td>
                                                <td id="delayed_created_by_supplier">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_iw">
                                                <td class="numbering"></td>
                                                <td>Delayed In warehouse (Delayed Status)</td>
                                                <td id="in_warehouse">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_da">
                                                <td class="numbering"></td>
                                                <td>Delayed Delivery Attempt (Delayed Status)</td>
                                                <td id="delivery_attempt">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_rmfb">
                                                <td class="numbering"></td>
                                                <td>Delayed Reception of Money Not Locally Transfered</td>
                                                <td id="reception_money_from_branches">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_cps">
                                                <td class="numbering"></td>
                                                <td>Delayed Pay to Supplier COD</td>
                                                <td id="cod_paid_to_supplier">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_ot">
                                                <td class="numbering"></td>
                                                <td>Delayed Tickets Opened</td>
                                                <td id="opened_tickets">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                            <tr class="export-shipment" id="row_out">
                                                <td class="numbering"></td>
                                                <td>Delayed Urgent Tickets Opened</td>
                                                <td id="opened_urgent_tickets">
                                                    <i class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:15px; text-align: center; display: none;"></i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </section>

                <section id="content2" class="tab-content">
                    <form action="/warehouse/controlpanel/configrationpost" method="post" onsubmit="return configration_check()">
                        <input type="hidden" name="tabFilter" id="tabFilter" value="2" />
                        <input type="hidden" name="apply_all_h" id="apply_all_h" value="0" />
                        <div class="container">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <h3 class="col-md-2 col-xs-12" style="text-align: left; float: left;">Update For</h3><br/>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group" style="text-align: left; float: left;">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                            <div class="col-md-9 col-sm-6 col-xs-12">
                                                <select id="company" name="company[]" class="form-control" type="text" multiple>
                                                    @foreach($adminCompanies as $compan)
                                                        <option value="{{$compan->id}}" {{ isset($company) && in_array($compan->id, $company) ? 'selected' : '' }}>{{$compan->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group" style="text-align: left; float: left;">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                            <div class="col-md-9 col-sm-6 col-xs-12">
                                                <select name="city[]" id="city" class="form-control" multiple>
                                                    @foreach($adminCities as $adcity)
                                                        <option value="{{$adcity->city}}" {{ isset($city) && in_array($adcity->city, $city) ? 'selected' : '' }}>{{$adcity->city}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 form-group" style="text-align: left; float: left;">
                                            <input type="submit" name="filter" class="btn btn-success" value="Check" />
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <?php $admin_id = Session::get('admin_id'); ?>
                        <div class="container">
                            <div class="tab-content">
                                <div id="dataview" class="tab-pane fade in active">
                                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                                        <h3 style="text-align: left;">Cycle Limits</h3><br/>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">To Be Picked Up </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="to_be_pickedup" name="to_be_pickedup" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->to_be_pickedup : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Hours</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Reserved To Pickup </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="reserved_to_pickup" name="reserved_to_pickup" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->reserved_to_pickup : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Hours</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Picked Up </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="pickedup" name="pickedup" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->pickedup : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Hours</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Delayed Created By Supplier </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="delayed_created_by_supplier" name="delayed_created_by_supplier" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->delayed_created_by_supplier : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">In Route </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="in_route" name="in_route" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->in_route : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">In Warehouse </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="in_warehouse" name="in_warehouse" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->in_warehouse : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Max Delivery Attempt </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="max_delivery_attempt" name="max_delivery_attempt" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->max_delivery_attempts : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Attempts</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Delivery Attempt </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="delivery_attempt" name="delivery_attempt" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->delivery_attempt : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Shipments With Captains </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="cod_reception_money" name="cod_reception_money" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->cod_reception_money : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Delivered With Captains </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="reception_money_in_branches" name="reception_money_in_branches" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->reception_money_in_branches : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Reception of Money Not Locally Transferred </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="reception_money_from_branches" name="reception_money_from_branches" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->reception_money_from_branches : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Pay to Supplier </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="cod_paid_to_supplier" name="cod_paid_to_supplier" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->cod_paid_to_supplier : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Return to Supplier </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="returned_to_supplier" name="returned_to_supplier" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->returned_to_supplier : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Tickets Opened </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="opened_tickets" name="opened_tickets" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->opened_tickets : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Hours</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Urgent Tickets Opened </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="opened_urgent_tickets" name="opened_urgent_tickets" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->opened_urgent_tickets : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Hours</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Delayed Captains </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="delayed_captains" name="delayed_captains" class="form-control col-md-8" value="{{ isset($change) && $change == '1' ? $intervals[0]->delayed_captains : 'Leave as is' }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Days</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-6 col-sm-3 col-xs-12">Lowest Average Rate </label>
                                            <div class="input-group col-md-5 col-sm-6 col-xs-12">
                                                <input type="text" id="lowest_average_rate" name="lowest_average_rate" class="form-control col-md-8" value="{{ $lowest_average_rate }}" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                <label class="input-group-addon" style="color: grey;">Rate</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br/>
                                        <div class="col-md-12 col-md-offset-4 col-xs-12 form-group">
                                            <label class="col-md-3 col-xs-12">
                                                <input type="checkbox" name="apply_all" id="apply_all" class="checkbox-inline form-check" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                                Apply for all new companies and cities? 
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-md-offset-4 col-xs-12 form-group">
                                            <input type="submit" value="Submit" class="btn btn-success col-md-10" {{ $admin_id != '32' && $admin_id != '51' ? 'disabled' : '' }} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>

                <section id="content3" class="tab-content">
                    <div class="container">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <table id="datatable_view" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Company</th>
                                            <th>City</th>
                                            <th>To Be Picked Up (Hours)</th>
                                            <th>Reserved To Pick Up (Hours)</th>
                                            <th>Picked Up (Hours)</th>
                                            <th>Delayed Created By Supplier (Days)</th>
                                            <th>In Route (Days)</th>
                                            <th>In Warehouse (Days)</th>
                                            <th>Max Delivery Attempts (Attempts)</th>
                                            <th>Delayed Delivery Attempt (Days)</th>
                                            <th>Delayed Shipments With Captains (Days)</th>
                                            <th>Delayed Delivered With Captains (Days)</th>
                                            <th>Delayed Reception Money Not Locally Transferred (Days)</th>
                                            <th>Delayed Pay To Supplier (Days)</th>
                                            <th>Delayed Return To Supplier (Days)</th>
                                            <th>Tickets Opened (Hours)</th>
                                            <th>Urgent Tickets Opened (Hours)</th>
                                            <th>Delayed Captains (Days)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Company</th>
                                            <th>City</th>
                                            <th>To Be Picked Up (Hours)</th>
                                            <th>Reserved To Pick Up (Hours)</th>
                                            <th>Picked Up (Hours)</th>
                                            <th>Delayed Created By Supplier (Days)</th>
                                            <th>In Route (Days)</th>
                                            <th>In Warehouse (Days)</th>
                                            <th>Max Delivery Attempts (Attempts)</th>
                                            <th>Delayed Delivery Attempt (Days)</th>
                                            <th>Delayed Shipments With Captains (Days)</th>
                                            <th>Delayed Delivered With Captains (Days)</th>
                                            <th>Delayed Reception Money Not Locally Transferred (Days)</th>
                                            <th>Delayed Pay To Supplier (Days)</th>
                                            <th>Delayed Return To Supplier (Days)</th>
                                            <th>Tickets Opened (Hours)</th>
                                            <th>Urgent Tickets Opened (Hours)</th>
                                            <th>Delayed Captains (Days)</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/sorttable/sorttable.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>
        let rush_hours = [13,14,15,16];
        let page_in_action = 0;
        let total_time = 60; // minutes
        let request_cnt = 22;
        let holding_time = (total_time - request_cnt - 2) / request_cnt; // one minute for each request + waiting time (keep interval of 2 empty requests after last request)
        let dp = [];
        for(let i = 0; i < request_cnt; ++i)
            dp.push(i == 0 ? 0 : 1 + holding_time);
        for(let i = 1; i < request_cnt; ++i)
            dp[i] += dp[i-1];
        let prev_hour = [];
        for(let i = 0; i < request_cnt; ++i)
            prev_hour.push(-1);

        setInterval(function() {
            let hour = new Date().getHours();
            if(rush_hours.includes(hour)) {
                $("#txt_live").text("Suspended During Rush Hours (1 PM Until 5 PM)");
                $("#txt_live").css("color", "red");
            }
            else {
                $("#txt_live").text("Live ...");
                $("#txt_live").css("color", "green");
            }
        }, 1000);
    </script>

    <script>
        <?php
        $companies_comma = '';
        $flag = false;
        foreach($company as $comp) {
            if($flag)
                $companies_comma .= ",";
            $flag = true;
            $companies_comma .= "$comp";
        }
        ?>

        <?php
        $cities_comma = '';
        $flag = false;
        foreach($get_nongrouped_cities as $ci) {
            if($flag)
                $cities_comma .= ",";
            $flag = true;
            $cities_comma .= "$ci";
        }
        foreach($get_grouped_cities as $agc) {
            foreach($agc as $ci) {
                if($flag)
                    $cities_comma .= ",";
                $flag = true;
                $cities_comma .= "$ci";
            }
        }
        ?>

        var companies_array = "{{ $companies_comma }}";
        var cities_array = "{{ $cities_comma }}";
        var from = "{{ $from }}";
        var to = "{{ $to }}";
    </script>

<script>
        $(document).ready(function(){
            <?php
            $all_statuses = '';
            // except 8 because it will add condition  in export shipments (is_money_received = 0)
            $query = OrderStatuses::all()->except(8); 
            $flag = false;
            foreach($query as $one_st) {
                if($flag)
                    $all_statuses .= "&";
                $flag = true;
                $all_statuses .= "status%5B%5D=$one_st->id";
            }
            
            $statuses_rts = '';
            $query = OrderStatuses::whereIn('id', array(1,2,3,4,6))->get();
            $flag = false;
            foreach($query as $st) {
                if($flag)
                    $statuses_rts .= "&";
                $flag = true;
                $statuses_rts .= "status%5B%5D=$st->id";
            }
            ?>

            <?php
            $companies_export = '';
            $flag = false;
            foreach($company as $comp) {
                if($flag)
                    $companies_export .= "&";
                $flag = true;
                $companies_export .= "companies_filter%5B%5D=$comp";
            }
            ?>
            
            <?php
            $cities_export = '';
            $flag = false;
            foreach($get_nongrouped_cities as $ci) {
                if($flag)
                    $cities_export .= "&";
                $flag = true;
                $cities_export .= "cities_filter%5B%5D=$ci";
            }
            foreach($get_grouped_cities as $agc) {
                foreach($agc as $ci) {
                    if($flag)
                        $cities_export .= "&";
                    $flag = true;
                    $cities_export .= "cities_filter%5B%5D=$ci";
                }
            }
            ?>
            console.log("{{ $companies_export }}");
            console.log("{{ $cities_export }}");
        }); 
    </script>

    <script>
        // Modal_of_COD
        $(document).ready(function() {      
            // do something in the background
            // dialog.modal('hide');
            // $("#modal_csv_view").modal('show');
            // var COD_report_Information = '<caption><h4>COD Report</h4></caption><tr>' + '<th>Select</th>' + '<th>COD</th>' + '</tr>';
            // COD_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="codreportradio">' + '</td><td>' + 'Download COD Report' + '</td></tr><tr><td>'
            //         + '<input id="viewcod"  type="radio" name="codreportradio">' + '</td><td>' + 'View COD Report' + '</td></tr>';

            // $('#table_csv_view').html(COD_report_Information);
        });

        $(".export-shipment").click(function() {
            let div = $(this);
            let id = div.context.id; // row id (used to redirect to export shipments page)
            let title = div.context.cells[1].innerHTML; // row title in the view
            let value_id = div.context.cells[2].id;
            let value = $("#" + value_id).html(); // column name in DB
            value = parseInt(value);
            if(isNaN(value)) {
                let date = new Date();
                let hour = date.getHours();
                if(rush_hours.includes(hour)) {
                    bootbox.alert('Control Panel Suspended Right Now');
                    return false;
                } 
                var dialog_wait = bootbox.dialog({
                    message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Please wait while we fetch data...</p>',
                    closeButton: false
                });
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/modal/count',
                    dataType: 'text',
                    data: {key: value_id, company: companies_array, city: cities_array, from: from, to: to}
                }).done(response => { 
                    dialog_wait.modal('hide');
                    console.log(response);
                    responseObj = JSON.parse(response);
                    // clear all radio buttons
                    $("#modal_radio_view").removeAttr('checked');
                    $("#modal_radio_csv").removeAttr('checked');
                    $("#modal_radio_csv").prop('checked', true);
                    // force csv when count is >= 500
                    if(responseObj.value >= 500)
                        $("#modal_radio_view").attr('disabled', 'true');
                    else 
                        $("#modal_radio_view").removeAttr('disabled');
                    // pass values to modal and show it 
                    $(".modal_row_id").html(title);
                    $(".modal_row_count").html(responseObj.value);
                    $("#modal_export_row_id").val(id);
                    $("#modal_csv_view").modal('show');
                });
            }
            else {
                // clear all radio buttons
                $("#modal_radio_view").removeAttr('checked');
                $("#modal_radio_csv").removeAttr('checked');
                $("#modal_radio_csv").prop('checked', true);
                // force csv when count is >= 500
                if(value >= 500)
                    $("#modal_radio_view").attr('disabled', 'true');
                else 
                    $("#modal_radio_view").removeAttr('disabled');
                // pass values to modal and show it
                $(".modal_row_id").html(title);
                $(".modal_row_count").html(value);
                $("#modal_export_row_id").val(id);
                $("#modal_csv_view").modal('show');
            }
            console.log(div.context.cells[1].innerHTML)
        });

        function modal_submit(id) {
            let flag = $("#modal_radio_view").is(":checked") + $("#modal_radio_csv").is(":checked");
            if(flag != 1) {
                bootbox.alert("Please choose an option");
                return false;
            }
            let csv_parameter = 'download_csv=1&';
            if($("#modal_radio_view").is(":checked"))
                csv_parameter = '';
            if(id == 'row_dw') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=duplicate_waybills&company=all&city=All&{{ $all_statuses }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from=&to=&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_cbs') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=created_by_supplier&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=0&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_tbpu') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=to_be_pickedup&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=-3&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_dc') {
                window.open("/warehouse/captains?" + csv_parameter + "controlpanel=delayed_captains&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=4&status%5B%5D=5&status%5B%5D=6&shipment_date=all&new_shipment_date=all&last_update=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_rtpu') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=reserved_to_pickup&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=-2&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_pu') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=pickedup&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=-1&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_dcbs') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=delayed_created_by_supplier&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=0&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_ir') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=in_route&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=2&shipment_date=all&new_shipment_date=all&last_update=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_iw') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=in_warehouse&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_da') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=delivery_attempts&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_eda') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=exceed_delivery_attempts&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_crm') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=cod_reception_money&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=4&status%5B%5D=6&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_rmib') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=reception_money_in_branches&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_rmfb') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=reception_money_from_branches&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_cps') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=cod_paid_to_supplier&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_rts') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=returned_to_supplier&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&{{ $statuses_rts }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_ot') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=opened_tickets&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&{{ $all_statuses }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_out') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=opened_urgent_tickets&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&{{ $all_statuses }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_dc') {
                window.open("/warehouse/captains?" + csv_parameter + "controlpanel=delayed_captains&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=4&status%5B%5D=5&status%5B%5D=6&shipment_date=all&new_shipment_date=all&last_update=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_lar') {
                window.open("/warehouse/captains?" + csv_parameter + "controlpanel=lowest_average_rate&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_cbc') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=cancelled_by_customer&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from=&to=&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_ptbr') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=prepared_to_be_rts&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&{{ $statuses_rts }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_drs') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=delayed_rescheduled_shipments&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
            else if(id == 'row_atww') {
                window.open("/warehouse/shipmentsExport?" + csv_parameter + "tabFilter=3&controlpanel=arrived_to_wrong_warehouse&company=all&city=All&{{ $companies_export }}&{{ $cities_export }}&status%5B%5D=2&shipment_date=all&new_shipment_date=all&last_update=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&hub=0&datatable_filtered_length=10", csv_parameter == '' ? '' : '_blank');
            }
        }
    </script>

    <!-- Dashboard CSS & JS -->
    <style>
        .export-shipment:hover { 
            cursor: pointer;
        } 
    </style>

    <style>
        @-webkit-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @-moz-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @-o-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        .invalid {
            -webkit-animation: invalid 10s; /* infinite Safari 4+ */
            -moz-animation:    invalid 10s; /* infinite Fx 5+ */
            -o-animation:      invalid 10s; /* infinite Opera 12+ */
            animation:         invalid 10s; /* infinite IE 10+ */
        }

        td {
            padding: 1em;
        }
    </style>

    <script>

        var timer = 10200;

        // 17 duplicate waybills
        $.ajax({
            type: 'GET',
            url: '/warehouse/controlpanel/inquiry/duplicate_waybills',
            dataType: 'text',
            data: {company: companies_array, city: cities_array, from: from, to: to}
        }).done(function(response){
            console.log('duplicate_waybills: ', response);
            responseObj = JSON.parse(response);
            if(responseObj.success){
                var duplicate_waybills = responseObj.value;
                if(duplicate_waybills != 0)
                    document.getElementById('row_dw').style.color = 'red';
                $("#duplicate_waybills").html(duplicate_waybills);
                $("#row_dw").addClass("invalid");
                setTimeout(function() {
                    $("#row_dw").removeClass("invalid");
                }, timer);
            }
        });

        $("#btn_load").click(function() {
            let date = new Date();
            let hour = date.getHours();
            $("#txt_live").css('display', '');
            if(rush_hours.includes(hour)) {
                $("#txt_live").text("Suspended During Rush Hours (1 PM Until 5 PM)");
                $("#txt_live").css("color", "red");
                return;
            }   
            page_in_action = 1;
            $("#txt_live").text("Live ...");
            $("#txt_live").css("color", "green");
            $("#btn_load").css('display', 'none');
            $(".fa.fa-spinner.fa-spin").css("display", "");

            let cur_hour = new Date().getHours();
            for(let i = 0; i < request_cnt; ++i)
                prev_hour[i] = cur_hour;

            // 1 created by supplier
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/created_by_supplier',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('created_by_supplier: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success) {
                        var created_by_supplier = responseObj.value;
                        $("#created_by_supplier").html(created_by_supplier);
                        $("#row_cbs").addClass("invalid");
                        setTimeout(function() {
                            $("#row_cbs").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 2 delayed created by supplier
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/delayed_created_by_supplier',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('delayed_created_by_supplier: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var delayed_created_by_supplier = responseObj.value;
                        $("#delayed_created_by_supplier").html(delayed_created_by_supplier);
                        $("#row_dcbs").addClass("invalid");
                        setTimeout(function() {
                            $("#row_dcbs").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 3 in route
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/in_route',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('in_route: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var in_route = responseObj.value;
                        $("#in_route").html(in_route);
                        $("#row_ir").addClass("invalid");
                        setTimeout(function() {
                            $("#row_ir").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 4 in warehouse
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/in_warehouse',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('in_warehouse: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var in_warehouse = responseObj.value;
                        $("#in_warehouse").html(in_warehouse);
                        $("#row_iw").addClass("invalid");
                        setTimeout(function() {
                            $("#row_iw").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 5 delivery attempt
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/delivery_attempt',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('delivery_attempt: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var delivery_attempt = responseObj.value;
                        $("#delivery_attempt").html(delivery_attempt);
                        $("#row_da").addClass("invalid");
                        setTimeout(function() {
                            $("#row_da").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 6 exceeded delivery attempt
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/exceeded_delivery_attempt',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('exceeded_delivery_attempt: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var exceeded_delivery_attempt = responseObj.value;
                        $("#exceeded_delivery_attempt").html(exceeded_delivery_attempt);
                        $("#row_eda").addClass("invalid");
                        setTimeout(function() {
                            $("#row_eda").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 7 cod money reception
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/cod_reception_money',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('cod_reception_money: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var cod_reception_money = responseObj.value;
                        $("#cod_reception_money").html(cod_reception_money);
                        $("#row_crm").addClass("invalid");
                        setTimeout(function() {
                            $("#row_crm").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 8 reception money in branches
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/reception_money_in_branches',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('reception_money_in_branches: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var reception_money_in_branches = responseObj.value;
                        $("#reception_money_in_branches").html(reception_money_in_branches);
                        $("#row_rmib").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rmib").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 9 reception money from branches
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/reception_money_from_branches',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('reception_money_from_branches: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var reception_money_from_branches = responseObj.value;
                        $("#reception_money_from_branches").html(reception_money_from_branches);
                        $("#row_rmfb").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rmfb").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 10 cod paid to supplier
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/cod_paid_to_supplier',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('cod_paid_to_supplier: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var cod_paid_to_supplier = responseObj.value;
                        $("#cod_paid_to_supplier").html(cod_paid_to_supplier);
                        $("#row_cps").addClass("invalid");
                        setTimeout(function() {
                            $("#row_cps").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 11 returned to supplier
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/returned_to_supplier',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('returned_to_supplier: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var returned_to_supplier = responseObj.value;
                        $("#returned_to_supplier").html(returned_to_supplier);
                        $("#row_rts").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rts").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 12 opened tickets
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/opened_tickets',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('opened_tickets: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var opened_tickets = responseObj.value;
                        $("#opened_tickets").html(opened_tickets);
                        $("#row_ot").addClass("invalid");
                        setTimeout(function() {
                            $("#row_ot").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 13 opened urgent tickets
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/opened_urgent_tickets',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('opened_urgent_tickets: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var opened_urgent_tickets = responseObj.value;
                        $("#opened_urgent_tickets").html(opened_urgent_tickets);
                        $("#row_out").addClass("invalid");
                        setTimeout(function() {
                            $("#row_out").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 14 to be picked up
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/to_be_pickedup',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('to_be_pickedup: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var to_be_pickedup = responseObj.value;
                        $("#to_be_pickedup").html(to_be_pickedup);
                        $("#row_tbpu").addClass("invalid");
                        setTimeout(function() {
                            $("#row_tbpu").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 15 reserved to pick up
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/reserved_to_pickup',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('reserved_to_pickup: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var reserved_to_pickup = responseObj.value;
                        $("#reserved_to_pickup").html(reserved_to_pickup);
                        $("#row_rtpu").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rtpu").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 16 picked up
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/pickedup',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('pickedup: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var pickedup = responseObj.value;
                        $("#pickedup").html(pickedup);
                        $("#row_pu").addClass("invalid");
                        setTimeout(function() {
                            $("#row_pu").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 18 delayed captains
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/delayed_captains',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('delayed_captains: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var delayed_captains = responseObj.value;
                        $("#delayed_captains").html(delayed_captains);
                        $("#row_dc").addClass("invalid");
                        setTimeout(function() {
                            $("#row_dc").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 19 low captains rate
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/lowest_average_rate',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('lowest_average_rate: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var lowest_average_rate = responseObj.value;
                        $("#lowest_average_rate").html(lowest_average_rate);
                        $("#row_lar").addClass("invalid");
                        setTimeout(function() {
                            $("#row_lar").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 20 cancelled by customer care
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/cancelled_by_customer',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('cancelled_by_customer: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var cancelled_by_customer = responseObj.value;
                        $("#cancelled_by_customer").html(cancelled_by_customer);
                        $("#row_cbc").addClass("invalid");
                        setTimeout(function() {
                            $("#row_cbc").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 21 prepared to be returned to supplier
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/prepared_to_be_rts',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('prepared_to_be_rts: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var prepared_to_be_rts = responseObj.value;
                        $("#prepared_to_be_rts").html(prepared_to_be_rts);
                        $("#row_ptbr").addClass("invalid");
                        setTimeout(function() {
                            $("#row_ptbr").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 22 delayed scheduled shipments
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/delayed_rescheduled_shipments',
                    dataType: 'text',
                    data: {company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('delayed_rescheduled_shipments: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success){
                        var delayed_rescheduled_shipments = responseObj.value;
                        $("#delayed_rescheduled_shipments").html(delayed_rescheduled_shipments);
                        $("#row_drs").addClass("invalid");
                        setTimeout(function() {
                            $("#row_drs").removeClass("invalid");
                        }, timer);
                    }
                });
            }

            // 23 arrived to wrong warehouse
            if(page_in_action) {
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/controlpanel/inquiry/row',
                    dataType: 'text',
                    data: {key: 'arrived_to_wrong_warehouse', company: companies_array, city: cities_array, from: from, to: to}
                }).done(function(response){
                    console.log('arrived_to_wrong_warehouse: ', response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success) {
                        var arrived_to_wrong_warehouse = responseObj.value;
                        $("#arrived_to_wrong_warehouse").html(arrived_to_wrong_warehouse);
                        $("#row_atww").addClass("invalid");
                        setTimeout(function() {
                            $("#row_atww").removeClass("invalid");
                        }, timer);
                    }
                });
            }
        });
        // end of loading button click event

        let index = -1;

        // 1 created by supplier
        index = 0;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/created_by_supplier',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('created_by_supplier: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success) {
                                var created_by_supplier = responseObj.value;
                                $("#created_by_supplier").html(created_by_supplier);
                                $("#row_cbs").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_cbs").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 2 delayed created by supplier
        index = 1;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/delayed_created_by_supplier',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('delayed_created_by_supplier: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var delayed_created_by_supplier = responseObj.value;
                                $("#delayed_created_by_supplier").html(delayed_created_by_supplier);
                                $("#row_dcbs").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_dcbs").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 3 in route
        index = 2;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/in_route',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('in_route: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var in_route = responseObj.value;
                                $("#in_route").html(in_route);
                                $("#row_ir").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_ir").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 4 in warehouse
        index = 3;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/in_warehouse',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('in_warehouse: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var in_warehouse = responseObj.value;
                                $("#in_warehouse").html(in_warehouse);
                                $("#row_iw").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_iw").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 5 delivery attempt
        index = 4;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/delivery_attempt',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('delivery_attempt: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var delivery_attempt = responseObj.value;
                                $("#delivery_attempt").html(delivery_attempt);
                                $("#row_da").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_da").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 6 exceeded delivery attempt
        index = 5;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/exceeded_delivery_attempt',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('exceeded_delivery_attempt: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var exceeded_delivery_attempt = responseObj.value;
                                $("#exceeded_delivery_attempt").html(exceeded_delivery_attempt);
                                $("#row_eda").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_eda").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 7 cod money reception
        index = 6;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/cod_reception_money',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('cod_reception_money: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var cod_reception_money = responseObj.value;
                                $("#cod_reception_money").html(cod_reception_money);
                                $("#row_crm").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_crm").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 8 reception money in branches
        index = 7;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/reception_money_in_branches',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('reception_money_in_branches: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var reception_money_in_branches = responseObj.value;
                                $("#reception_money_in_branches").html(reception_money_in_branches);
                                $("#row_rmib").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_rmib").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 9 reception money from branches
        index = 8;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/reception_money_from_branches',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('reception_money_from_branches: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var reception_money_from_branches = responseObj.value;
                                $("#reception_money_from_branches").html(reception_money_from_branches);
                                $("#row_rmfb").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_rmfb").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 10 cod paid to supplier
        index = 9;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/cod_paid_to_supplier',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('cod_paid_to_supplier: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var cod_paid_to_supplier = responseObj.value;
                                $("#cod_paid_to_supplier").html(cod_paid_to_supplier);
                                $("#row_cps").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_cps").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 11 returned to supplier
        index = 10;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/returned_to_supplier',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('returned_to_supplier: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var returned_to_supplier = responseObj.value;
                                $("#returned_to_supplier").html(returned_to_supplier);
                                $("#row_rts").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_rts").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 12 opened tickets
        index = 11;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/opened_tickets',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('opened_tickets: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var opened_tickets = responseObj.value;
                                $("#opened_tickets").html(opened_tickets);
                                $("#row_ot").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_ot").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 13 opened urgent tickets
        index = 12;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/opened_urgent_tickets',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('opened_urgent_tickets: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var opened_urgent_tickets = responseObj.value;
                                $("#opened_urgent_tickets").html(opened_urgent_tickets);
                                $("#row_out").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_out").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 14 to be picked up
        index = 13;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/to_be_pickedup',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('to_be_pickedup: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var to_be_pickedup = responseObj.value;
                                $("#to_be_pickedup").html(to_be_pickedup);
                                $("#row_tbpu").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_tbpu").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 15 reserved to pick up
        index = 14;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/reserved_to_pickup',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('reserved_to_pickup: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var reserved_to_pickup = responseObj.value;
                                $("#reserved_to_pickup").html(reserved_to_pickup);
                                $("#row_rtpu").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_rtpu").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 16 picked up
        index = 15;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/pickedup',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('pickedup: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var pickedup = responseObj.value;
                                $("#pickedup").html(pickedup);
                                $("#row_pu").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_pu").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 17 duplicate waybills
        index = 16;
        setInterval(function(){
            let cur_hour = new Date().getHours();
            if(cur_hour != prev_hour[index]) {
                setTimeout(function(){
                    $.ajax({
                        type: 'GET',
                        url: '/warehouse/controlpanel/inquiry/duplicate_waybills',
                        dataType: 'text',
                        data: {company: companies_array, city: cities_array, from: from, to: to}
                    }).done(function(response){
                        console.log('duplicate_waybills: ', response);
                        responseObj = JSON.parse(response);
                        if(responseObj.success){
                            var duplicate_waybills = responseObj.value;
                            if(duplicate_waybills != 0)
                                document.getElementById('row_dw').style.color = 'red';
                            $("#duplicate_waybills").html(duplicate_waybills);
                            $("#row_dw").addClass("invalid");
                            setTimeout(function() {
                                $("#row_dw").removeClass("invalid");
                            }, timer);
                        }
                    });
                }, dp[index] * 1000);
                prev_hour[index] = cur_hour;
            }
        }, 1000);

        // 18 delayed captains
        index = 17;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/delayed_captains',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('delayed_captains: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var delayed_captains = responseObj.value;
                                $("#delayed_captains").html(delayed_captains);
                                $("#row_dc").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_dc").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 19 low captains rate
        index = 18;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/lowest_average_rate',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('lowest_average_rate: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var lowest_average_rate = responseObj.value;
                                $("#lowest_average_rate").html(lowest_average_rate);
                                $("#row_lar").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_lar").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 20 cancelled by customer care
        index = 19;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/cancelled_by_customer',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('cancelled_by_customer: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var cancelled_by_customer = responseObj.value;
                                $("#cancelled_by_customer").html(cancelled_by_customer);
                                $("#row_cbc").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_cbc").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 21 prepared to be returned to supplier
        index = 20;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/prepared_to_be_rts',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('prepared_to_be_rts: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var prepared_to_be_rts = responseObj.value;
                                $("#prepared_to_be_rts").html(prepared_to_be_rts);
                                $("#row_ptbr").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_ptbr").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 22 delayed rescheduled shipments
        index = 21;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/delayed_rescheduled_shipments',
                            dataType: 'text',
                            data: {company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('delayed_rescheduled_shipments: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var delayed_rescheduled_shipments = responseObj.value;
                                $("#delayed_rescheduled_shipments").html(delayed_rescheduled_shipments);
                                $("#row_drs").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_drs").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

        // 23 delayed rescheduled shipments
        index = 22;
        setInterval(function(){
            if(page_in_action) {
                let cur_hour = new Date().getHours();
                if(cur_hour != prev_hour[index]) {
                    setTimeout(function(){
                        $.ajax({
                            type: 'GET',
                            url: '/warehouse/controlpanel/inquiry/row',
                            dataType: 'text',
                            data: {key: 'arrived_to_wrong_warehouse', company: companies_array, city: cities_array, from: from, to: to}
                        }).done(function(response){
                            console.log('arrived_to_wrong_warehouse: ', response);
                            responseObj = JSON.parse(response);
                            if(responseObj.success){
                                var arrived_to_wrong_warehouse = responseObj.value;
                                $("#arrived_to_wrong_warehouse").html(arrived_to_wrong_warehouse);
                                $("#row_atww").addClass("invalid");
                                setTimeout(function() {
                                    $("#row_atww").removeClass("invalid");
                                }, timer);
                            }
                        });
                    }, dp[index] * 1000);
                    prev_hour[index] = cur_hour;
                }
            }
        }, 1000);

    </script>

    <script>
        function configration_check() {
            var checked = document.getElementById('apply_all').checked;
            if(checked && confirm("Are you sure you want to apply changes for all new companeis and cities? ")) {
                document.getElementById('apply_all_h').value=1;
                return true;
            }
            return !checked;
        }
    </script>

    <script>
        <?php
        $companies_map = [];
        $x = '';
        $companies_map['0'] = "All";
        foreach($adminCompanies as $compan) {
            $companies_map[$compan->id] = $compan->company_name;
        }
        $counter = 2;
        $data = '';
        foreach($all_intervals as $interval) {
            $company_name = $companies_map[$interval->company_id];
            $value = $company_name == 'All' ? '1' : $counter++;
            $data .= "[ \"$value\", \"$company_name\", \"$interval->city\", \"$interval->to_be_pickedup\", \"$interval->reserved_to_pickup\", \"$interval->pickedup\", \"$interval->delayed_created_by_supplier\", \"$interval->in_route\", \"$interval->in_warehouse\", \"$interval->max_delivery_attempts\", \"$interval->delivery_attempt\", \"$interval->cod_reception_money\", \"$interval->reception_money_in_branches\", \"$interval->reception_money_from_branches\", \"$interval->cod_paid_to_supplier\", \"$interval->returned_to_supplier\", \"$interval->opened_tickets\", \"$interval->opened_urgent_tickets\", \"$interval->delayed_captains\" ],"; 
        }
        ?>

        $("#datatable_view").dataTable({

            "data": [{{ $data }}],
            "autoWidth": false,

            "pageLength": 20,

            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[0, 'asc']]
        });
    </script>

    <script>
        // give rows numbers on dashboard table
        $(document).ready(function(){
            let td_elements = document.getElementsByClassName('numbering');
            for(i = 0; i < td_elements.length; ++i)
                td_elements[i].innerHTML = i + 1;
        });
    </script>

    <script>
        // old request style
        // setInterval(function() {
        //     $.ajax({
        //         type: 'GET',
        //         url: '/warehouse/controlpanel/inquiry',
        //         dataType: 'text',
        //         data: {company: companies_array, city: cities_array, from: from, to: to}
        //     }).done(function(response){
        //         <?php $to = date('Y-m-d'); ?>
        //         console.log('to variable: {{ $to }} | {{ date("Y-m-d H:i:s") }}');
        //         console.log(response);
        //         document.getElementById('to').value='{{ $to }}';
        //         responseObj = JSON.parse(response);
        //         if(responseObj.success == true) {
        //             var created_by_supplier = responseObj.created_by_supplier;
        //             var cur = document.getElementById('created_by_supplier').innerText;
        //             if(cur != created_by_supplier) {
        //                 $("#created_by_supplier").html(created_by_supplier);
        //                 $("#row_cbs").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_cbs").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var delayed_created_by_supplier = responseObj.delayed_created_by_supplier;
        //             cur = document.getElementById('delayed_created_by_supplier').innerText;
        //             if(cur != delayed_created_by_supplier) {
        //                 $("#delayed_created_by_supplier").html(delayed_created_by_supplier);
        //                 $("#row_dcbs").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_dcbs").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var in_route = responseObj.in_route;
        //             cur = document.getElementById('in_route').innerText;
        //             if(cur != in_route) {
        //                 $("#in_route").html(in_route);
        //                 $("#row_ir").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_ir").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var in_warehouse = responseObj.in_warehouse;
        //             cur = document.getElementById('in_warehouse').innerText;
        //             if(cur != in_warehouse) {
        //                 $("#in_warehouse").html(in_warehouse);
        //                 $("#row_iw").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_iw").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var delivery_attempt = responseObj.delivery_attempt;
        //             cur = document.getElementById('delivery_attempt').innerText;
        //             if(cur != delivery_attempt) {
        //                 $("#delivery_attempt").html(delivery_attempt);
        //                 $("#row_da").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_da").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var exceeded_delivery_attempt = responseObj.exceeded_delivery_attempt;
        //             cur = document.getElementById('exceeded_delivery_attempt').innerText;
        //             if(cur != exceeded_delivery_attempt) {
        //                 $("#exceeded_delivery_attempt").html(exceeded_delivery_attempt);
        //                 $("#row_eda").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_eda").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var cod_reception_money = responseObj.cod_reception_money;
        //             cur = document.getElementById('cod_reception_money').innerText;
        //             if(cur != cod_reception_money) {
        //                 $("#cod_reception_money").html(cod_reception_money);
        //                 $("#row_crm").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_crm").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var reception_money_in_branches = responseObj.reception_money_in_branches;
        //             cur = document.getElementById('reception_money_in_branches').innerText;
        //             if(cur != reception_money_in_branches) {
        //                 $("#reception_money_in_branches").html(reception_money_in_branches);
        //                 $("#row_rmib").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_rmib").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var reception_money_from_branches = responseObj.reception_money_from_branches;
        //             cur = document.getElementById('reception_money_from_branches').innerText;
        //             if(cur != reception_money_from_branches) {
        //                 $("#reception_money_from_branches").html(reception_money_from_branches);
        //                 $("#row_rmfb").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_rmfb").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var cod_paid_to_supplier = responseObj.cod_paid_to_supplier;
        //             cur = document.getElementById('cod_paid_to_supplier').innerText;
        //             if(cur != cod_paid_to_supplier) {
        //                 $("#cod_paid_to_supplier").html(cod_paid_to_supplier);
        //                 $("#row_cps").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_cps").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var returned_to_supplier = responseObj.returned_to_supplier;
        //             cur = document.getElementById('returned_to_supplier').innerText;
        //             if(cur != returned_to_supplier) {
        //                 $("#returned_to_supplier").html(returned_to_supplier);
        //                 $("#row_rts").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_rts").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var opened_tickets = responseObj.opened_tickets;
        //             cur = document.getElementById('opened_tickets').innerText;
        //             if(cur != opened_tickets) {
        //                 $("#opened_tickets").html(opened_tickets);
        //                 $("#row_ot").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_ot").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var opened_urgent_tickets = responseObj.opened_urgent_tickets;
        //             cur = document.getElementById('opened_urgent_tickets').innerText;
        //             if(cur != opened_urgent_tickets) {
        //                 $("#opened_urgent_tickets").html(opened_urgent_tickets);
        //                 $("#row_out").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_out").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var to_be_pickedup = responseObj.to_be_pickedup;
        //             cur = document.getElementById('to_be_pickedup').innerText;
        //             if(cur != to_be_pickedup) {
        //                 $("#to_be_pickedup").html(to_be_pickedup);
        //                 $("#row_tbpu").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_tbpu").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var reserved_to_pickup = responseObj.reserved_to_pickup;
        //             cur = document.getElementById('reserved_to_pickup').innerText;
        //             if(cur != reserved_to_pickup) {
        //                 $("#reserved_to_pickup").html(reserved_to_pickup);
        //                 $("#row_rtpu").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_rtpu").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var pickedup = responseObj.pickedup;
        //             cur = document.getElementById('pickedup').innerText;
        //             if(cur != pickedup) {
        //                 $("#pickedup").html(pickedup);
        //                 $("#row_pu").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_pu").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var duplicate_waybills = responseObj.duplicate_waybills;
        //             cur = document.getElementById('duplicate_waybills').innerText;
        //             if(cur != duplicate_waybills) {
        //                 if(duplicate_waybills != 0)
        //                     document.getElementById('row_dw').style.color = 'red';
        //                 else
        //                     document.getElementById('row_dw').style.color = '#999';
        //                 $("#duplicate_waybills").html(duplicate_waybills);
        //                 $("#row_dw").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_dw").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var delayed_captains = responseObj.delayed_captains;
        //             cur = document.getElementById('delayed_captains').innerText;
        //             if(cur != delayed_captains) {
        //                 $("#delayed_captains").html(delayed_captains);
        //                 $("#row_dc").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_dc").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var lowest_average_rate = responseObj.lowest_average_rate;
        //             cur = document.getElementById('lowest_average_rate').innerText;
        //             if(cur != lowest_average_rate) {
        //                 $("#lowest_average_rate").html(lowest_average_rate);
        //                 $("#row_lar").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_lar").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var cancelled_by_customer = responseObj.cancelled_by_customer;
        //             cur = document.getElementById('cancelled_by_customer').innerText;
        //             if(cur != cancelled_by_customer) {
        //                 $("#cancelled_by_customer").html(cancelled_by_customer);
        //                 $("#row_cbc").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_cbc").removeClass("invalid");
        //                 }, timer);
        //             }
        //             var prepared_to_be_rts = responseObj.prepared_to_be_rts;
        //             cur = document.getElementById('prepared_to_be_rts').innerText;
        //             if(cur != prepared_to_be_rts) {
        //                 $("#prepared_to_be_rts").html(prepared_to_be_rts);
        //                 $("#row_ptbr").addClass("invalid");
        //                 setTimeout(function() {
        //                     $("#row_ptbr").removeClass("invalid");
        //                 }, timer);
        //             }
        //         }
        //     });
        // }, 300000);
    </script>

@stop
