@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends('warehouse.layout')


    @section('content')

            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3> Register Product</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name <span
                                class="required">*</span></label>

                    <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                        <select required name="company" id="company" class="form-control">
                            <option value="">Select Company</option>
                            @foreach($companies as $company)
                                <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Product SKU <span
                                class="required">*</span></label>

                    <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">

                        <input id="sku" name="sku" class="form-control col-md-7 col-xs-12"
                               placeholder="stock keeping unit" type="text" maxlength="10">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Name<span
                                class="required">*</span></label>

                    <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                        <input id="name" name="name" class="form-control col-md-7 col-xs-12"
                               required="required" type="text" maxlength="50" placeholder="Product Name">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Weight</label>

                    <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                        <input id="product_weight" name="product_weight" class="form-control col-md-7 col-xs-12"
                               placeholder="Product Weight" type="text" onkeypress="return isNumberKey(event,this)"
                               maxlength="5" value="0">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span
                                class="required">*</span></label>

                    <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="3" cols="45" id="description" name="description"
                                      class="form-control col-md-7 col-xs-12" required="required"
                                      type="text"></textarea>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-md-offset-2 col-sm-12 col-xs-12 form-group" style="margin-top: 40px;">
                    <button type="submit" class="btn btn-primary btn-flat btn-block" onclick="registerShipments()">
                        Register Product
                    </button>
                </div>

            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>


        function isNumberKey(evt, obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function registerShipments() {

            var company_id = document.getElementById('company').value
            if (company_id == '') {
                alert('Please select company');
                return;
            }

            var product_weight = document.getElementById('product_weight').value;
            var name = document.getElementById('name').value;
            if (name == '') {
                alert('Please Enter Product name');
                return;
            }
            var sku = document.getElementById('sku').value;
            if (sku == '') {
                alert('Please Enter Product stock keeping unit');
                return;
            }

            var description = document.getElementById('description').value;
            if (description == '') {
                alert('Please Enter description');
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/warehouse/addstockproductpost',
                dataType: 'text',
                data: {
                    company_id: company_id,
                    productName: name,
                    sku: sku,
                    product_weight: product_weight,
                    description: description
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.error) {
                    alert(responseObj.error);
                } else if (responseObj.success == true) {
                    alert(responseObj.message);
                    window.location = '/warehouse/addstockproduct';
                } else {
                    alert('Failed: ' + responseObj.error);
                }
            })

        }

    </script>
    @stop

