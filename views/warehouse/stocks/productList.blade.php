@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif


    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Product List</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/productslistpost">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Companies <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="company" id="company" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">
                                <table id="stockProductsTable" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Product ID</th>
                                        <th>Company Name</th>
                                        <th>Product Name</th>
                                        <th>SKU</th>
                                        <th>Weight</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($orders as $item) {
                                    ?>

                                    <tr id='<?= 'tr' . $item['id'] ?>'>
                                        <td><?= $item['id'] ?></td>
                                        <td><?=  $item['company_name']  ?></td>
                                        <td><?= $item['produnt_name'] ?></td>
                                        <td><?= $item['sku'] ?></td>
                                        <td><?= $item['weight'] ?></td>
                                        <td><?= $item['description'] ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button id="dLabel" class="btn btn-success" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Action
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                    <li onclick="editProduct('<?= 'tr' . $item['id'] ?>')"><a>Edit
                                                            Product</a>
                                                    </li>
                                                    <li><a href="#">Detail</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php } ?>

                                    </tbody>


                                    <tfoot>
                                    <tr>
                                        <th>Product ID</th>
                                        <th>Company Name</th>
                                        <th>Product Name</th>
                                        <th>SKU</th>
                                        <th>Weight</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- Edit product modal start -->
                            <div class="modal fade" id="editProductModal" role="dialog" style="overflow: auto">
                                <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Company </label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12">
                                                        <input id="updatecompany" name="updatecompany"
                                                               class="form-control col-md-7 col-xs-12" type="text"
                                                               disabled
                                                               maxlength="10">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product
                                                        Name<span
                                                                class="required">*</span></label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                                        <input id="updatename" name="updatename"
                                                               class="form-control col-md-7 col-xs-12"
                                                               required="required" type="text" maxlength="50"
                                                               placeholder="Product Name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product SKU <span
                                                                class="required">*</span></label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">

                                                        <input id="updatesku" name="updatesku"
                                                               class="form-control col-md-7 col-xs-12"
                                                               placeholder="stock keeping unit" type="text"
                                                               maxlength="10">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product
                                                        Weight</label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                                        <input id="updateweight" name="updateweight"
                                                               class="form-control col-md-7 col-xs-12"
                                                               placeholder="Product Weight" type="text"
                                                               onkeypress="return isNumberKey(event,this)"
                                                               maxlength="5" value="0">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Description <span
                                                                class="required">*</span></label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="3" cols="45" id="updatedescription" name="updatedescription"
                                      class="form-control col-md-7 col-xs-12" required="required"
                                      type="text"></textarea>
                                                    </div>
                                                </div>
                                                <input id="SelectedproductId" name="SelectedproductId" type="hidden">
                                                <input id="tableSelectedRowId" name="tableSelectedRowId" type="hidden">

                                            </div>
                                            <div class="modal-footer">
                                                <span id="error_message"
                                                      style="color: red;margin: 100px;font-size: large;"></span>
                                                <button type="button" class="btn btn-success" onclick="updateProduct()">
                                                    Update
                                                </button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-----------End Edit product modal------------->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>


        <!-- Datatables -->
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

        <script>

            $(document).ready(function () {

                $("#stockProductsTable").DataTable({
                    "autoWidth": false,
                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,

                    'order': [[0, 'asc']]
                });
            })

            function editProduct(rowid) {
                var selectedRowCells = document.getElementById(rowid).cells;
                var productId = selectedRowCells.item(0).innerHTML;
                var productCompany = selectedRowCells.item(1).innerHTML
                var productName = selectedRowCells.item(2).innerHTML;
                var productSKU = selectedRowCells.item(3).innerHTML;
                var productweight = selectedRowCells.item(4).innerHTML;
                var productDescription = selectedRowCells.item(5).innerHTML;

                document.getElementById('updatecompany').value = productCompany;
                document.getElementById('updatename').value = productName;
                document.getElementById('updatesku').value = productSKU;
                document.getElementById('updateweight').value = productweight;
                document.getElementById('updatedescription').value = productDescription;
                document.getElementById('tableSelectedRowId').value = rowid;
                document.getElementById('SelectedproductId').value = productId;
                document.getElementById('error_message').innerHTML = ''
                $('#editProductModal').modal("show");
            }

            function isNumberKey(evt, obj) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                var value = obj.value;
                var dotcontains = value.indexOf(".") != -1;
                if (dotcontains)
                    if (charCode == 46) return false;
                if (charCode == 46) return true;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function updateProduct() {
                var productId = document.getElementById('SelectedproductId').value;
                var productName = document.getElementById('updatename').value;
                var updatedSKU = document.getElementById('updatesku').value;
                var productweight = document.getElementById('updateweight').value;
                var productDescription = document.getElementById('updatedescription').value;
                var rowid = document.getElementById('tableSelectedRowId').value;
                var modelErrorMessage = document.getElementById('error_message');

                modelErrorMessage.innerHTML = '';

                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatestockproduct',
                    dataType: 'text',
                    data: {
                        productId: productId,
                        productName: productName,
                        updatedSKU: updatedSKU,
                        productweight: productweight,
                        productDescription: productDescription,
                    },
                }).done(function (response) {
                            responseObj = JSON.parse(response);
                            if (responseObj.error) {
                                modelErrorMessage.innerHTML = responseObj.error;
                            } else if (responseObj.success) {
                                $('#editProductModal').modal('hide');
                                modelErrorMessage.innerHTML = '';
                                alert(responseObj.message);
                                var selectedRowCells = document.getElementById(rowid).cells;
                                selectedRowCells.item(2).innerHTML = productName;
                                selectedRowCells.item(3).innerHTML = updatedSKU;
                                selectedRowCells.item(4).innerHTML = productweight;
                                selectedRowCells.item(5).innerHTML = productDescription;
                            } else {
                                modelErrorMessage.innerHTML = 'Product not updated';
                            }
                        }
                )
            }

        </script>
    @stop