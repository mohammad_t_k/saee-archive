@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif


    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Stock Report</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/stockreportpost">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-5 col-sm-6 col-xs-12">Companies <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="company" id="company" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City<span
                                            class="required">*</span></label>

                                <div class="col-md-8 col-sm-6 col-xs-12 ">
                                    <select name="city" id="city" class="form-control" onchange="onChangeCity()">
                                        <option value="All">All</option>
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Hub <span
                                            class="required">*</span></label>

                                <div class="col-md-10 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($cityHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-1">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">
                                <table id="stockProductsTable" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>City</th>
                                        <th>Hub</th>
                                        <th>Product Name</th>
                                        <th>SKU</th>
                                        <th>Quantity</th>
                                        <th>Updated At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($orders as $item) {
                                    ?>

                                    <tr id='<?= 'tr' . $item['id'] ?>'>
                                        <td><?=  $item['company_name']  ?></td>
                                        <td><?=  $item['city']  ?></td>
                                        <td><?=  $item['hub_name']  ?></td>
                                        <td><?= $item['produnt_name'] ?></td>
                                        <td><?= $item['sku'] ?></td>
                                        <td><?= $item['total_quantity'] ?></td>
                                        <td><?= date('Y-m-d', strtotime($item['updated_at'])) ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button id="dLabel" class="btn btn-success" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Action
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                    <li onclick="updateStockShow('<?= 'tr' . $item['id'] ?>','<?=$item['id'] ?>')">
                                                        <a>Update Stock</a>
                                                    </li>
                                                    <li><a href="#">Detail</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php } ?>

                                    </tbody>


                                    <tfoot>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>City</th>
                                        <th>Hub</th>
                                        <th>Product Name</th>
                                        <th>SKU</th>
                                        <th>Quantity</th>
                                        <th>Updated At</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- Edit product modal start -->
                            <div class="modal fade" id="updateStockModal" role="dialog" style="overflow: auto">
                                <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Company </label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12">
                                                        <input id="updatecompany" name="updatecompany"
                                                               class="form-control col-md-7 col-xs-12" type="text"
                                                               disabled
                                                               maxlength="10">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">City </label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12">
                                                        <input id="updatecity" name="updatecity"
                                                               class="form-control col-md-7 col-xs-12" type="text"
                                                               disabled
                                                               maxlength="10">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Hub </label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12">
                                                        <input id="updatehub" name="updatehub"
                                                               class="form-control col-md-7 col-xs-12" type="text"
                                                               disabled
                                                               maxlength="10">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product
                                                        Name<span
                                                                class="required">*</span></label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                                        <input id="updatename" name="updatename"
                                                               class="form-control col-md-7 col-xs-12"
                                                               required="required" type="text" maxlength="50"
                                                               placeholder="Product Name" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product SKU <span
                                                                class="required">*</span></label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">

                                                        <input id="updatesku" name="updatesku"
                                                               class="form-control col-md-7 col-xs-12"
                                                               placeholder="stock keeping unit" type="text"
                                                               maxlength="10" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Add
                                                        Quantity</label>

                                                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                                        <input id="updatequanity" name="updatequanity"
                                                               class="form-control col-md-7 col-xs-12"
                                                               placeholder="Enter quantity" type="text"
                                                               onkeypress="return isNumber(event)"
                                                               maxlength="5">
                                                    </div>
                                                </div>

                                                <input id="SelectedstockId" name="SelectedstockId" type="hidden">
                                                <input id="tableSelectedRowId" name="tableSelectedRowId" type="hidden">
                                                <input id="currentQuantity" name="currentQuantity" type="hidden">

                                            </div>
                                            <div class="modal-footer">
                                                <span id="error_message"
                                                      style="color: red;margin: 100px;font-size: large;"></span>
                                                <button type="button" class="btn btn-success" onclick="updateStock()">
                                                    Update
                                                </button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-----------End Edit product modal------------->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>


        <!-- Datatables -->
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

        <script>

            $(document).ready(function () {

                $("#stockProductsTable").DataTable({
                    "autoWidth": false,
                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,

                    'order': [[0, 'asc']]
                });
            })

            function updateStockShow(rowid, stockId) {
                var selectedRowCells = document.getElementById(rowid).cells;
                var stockCompany = selectedRowCells.item(0).innerHTML;
                var stockCity = selectedRowCells.item(1).innerHTML
                var stockHub = selectedRowCells.item(2).innerHTML;
                var stockProductName = selectedRowCells.item(3).innerHTML;
                var stockSKU = selectedRowCells.item(4).innerHTML;
                var stockcurrentQuantity = selectedRowCells.item(5).innerHTML;


                document.getElementById('updatecompany').value = stockCompany;
                document.getElementById('updatecity').value = stockCity;
                document.getElementById('updatehub').value = stockHub;
                document.getElementById('updatename').value = stockProductName;
                document.getElementById('updatesku').value = stockSKU;
                document.getElementById('tableSelectedRowId').value = rowid;
                document.getElementById('SelectedstockId').value = stockId;
                document.getElementById('currentQuantity').value = stockcurrentQuantity;
                document.getElementById('error_message').innerHTML = '';
                document.getElementById('updatequanity').value = '';
                $('#updateStockModal').modal("show");
            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            function onChangeCity() {
                $("#hub").empty();
                var city = document.getElementById('city').value;
                $.ajax({
                    type: 'GET',
                    url: '/warehouse/gethubsbycity',
                    dataType: 'text',
                    data: {city: city},
                }).done(function (response) {
                    responseObj = JSON.parse(response);
                    if (responseObj.city == city) {
                        var hubs = responseObj.hubs;
                        var select = document.getElementById('hub');
                        for (var i = 0; i < hubs.length; i += 1) {
                            if (hubs[i].name == '')
                                continue;
                            option = document.createElement('option');
                            option.setAttribute('value', hubs[i].hub_id);
                            option.appendChild(document.createTextNode(hubs[i].name));
                            select.appendChild(option);
                        }
                    }
                });
            }

            function updateStock() {
                var stockId = document.getElementById('SelectedstockId').value;
                var stockCity = document.getElementById('updatecity').value;
                var rowid = document.getElementById('tableSelectedRowId').value;
                var currentQuantity = document.getElementById('currentQuantity').value;
                var updatequanity = document.getElementById('updatequanity').value;

                var modelErrorMessage = document.getElementById('error_message');
                modelErrorMessage.innerHTML = '';
                if (updatequanity == '') {
                    alert('Please Enter quantity');
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatestock',
                    dataType: 'text',
                    data: {
                        stockId: stockId,
                        city: stockCity,
                        updatequanity: updatequanity,
                        currentQuantity: currentQuantity
                    },
                }).done(function (response) {
                            responseObj = JSON.parse(response);
                            if (responseObj.error) {
                                modelErrorMessage.innerHTML = responseObj.error;
                            } else if (responseObj.success) {
                                $('#updateStockModal').modal('hide');
                                modelErrorMessage.innerHTML = '';
                                alert(responseObj.message);
                                var selectedRowCells = document.getElementById(rowid).cells;
                                selectedRowCells.item(5).innerHTML = responseObj.totalUpdatedQuantity;
                                selectedRowCells.item(6).innerHTML = responseObj.updatedAt;
                            } else {
                                modelErrorMessage.innerHTML = 'Stock not updated';
                            }
                        }
                )
            }

        </script>
    @stop