@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends('warehouse.layout')


    @section('content')

            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Stock</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Company Name <span
                                class="required">*</span></label>

                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                        <select required name="company" id="company" class="form-control" onchange="onChangeCompany()">
                            <option value="">Select Company</option>
                            @foreach($companies as $company)
                                <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product Name <span
                                class="required">*</span></label>

                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                        <select required name="productName" id="productName" class="form-control">
                            @foreach($productlists as $productlist)
                                <option value="{{$productlist->id}}"<?php if (isset($company_id) && $company_id == $productlist->id) echo 'selected';?>>{{$productlist->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">City<span
                                class="required">*</span></label>

                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                        <select name="city" id="city" class="form-control" onchange="onChangeCity()">
                            @foreach($adminCities as $adcity)
                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Hub <span
                                class="required">*</span></label>

                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12">
                        <select name="hub" id="hub" class="form-control">
                            @foreach($cityHubs as $adhub)
                                <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">Product Quantity <span
                                class="required">*</span></label>

                    <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                        <input id="product_quantity" name="product_quantity" class="form-control col-md-7 col-xs-12"
                               placeholder="Product Quantity" type="text" onkeypress="return isNumberKey(event,this)"
                               maxlength="5">
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-md-offset-2 col-sm-12 col-xs-12 form-group" style="margin-top: 20px;">
                    <button type="submit" class="btn btn-primary btn-flat btn-block" onclick="registerStock()">
                        Add Stock
                    </button>
                </div>

            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        function onChangeCompany() {
            $("#productName").empty();
            var company_id = document.getElementById("company").value;
            $.ajax({
                type: 'GET',
                url: '/warehouse/getproductsbycompany',
                dataType: 'text',
                data: {company_id: company_id},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.company_id == company_id) {
                    var products = responseObj.productslist;
                    var select = document.getElementById('productName');
                    for (var i = 0; i < products.length; i += 1) {
                        if (products[i].name == '')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', products[i].id);
                        option.appendChild(document.createTextNode(products[i].name));
                        select.appendChild(option);
                    }
                }
            });

        }
        function onChangeCity() {
            $("#hub").empty();
            var city = document.getElementById('city').value;
            $.ajax({
                type: 'GET',
                url: '/warehouse/gethubsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var hubs = responseObj.hubs;
                    var select = document.getElementById('hub');
                    for (var i = 0; i < hubs.length; i += 1) {
                        if (hubs[i].name == '')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', hubs[i].hub_id);
                        option.appendChild(document.createTextNode(hubs[i].name));
                        select.appendChild(option);
                    }
                }
            });
        }

        function isNumberKey(evt, obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function registerStock() {

            var company_id = document.getElementById('company').value;
            if (company_id == '') {
                alert('Please select company');
                return;
            }
            var productid = document.getElementById('productName').value;

            if (company_id == '') {
                alert('Please select product Name');
                return;
            }
            var city = document.getElementById('city').value;
            var hub_id = document.getElementById('hub').value;

            var product_quantity = document.getElementById('product_quantity').value;

            if (product_quantity == '') {
                alert('Please Enter Product Quantity');
                return;
            }

            $.ajax({
                type: 'POST',
                url: '/warehouse/addstockpost',
                dataType: 'text',
                data: {
                    company_id: company_id,
                    productid: productid,
                    city: city,
                    hub_id: hub_id,
                    product_quantity: product_quantity
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.error) {
                    alert(responseObj.error);
                } else if (responseObj.success == true) {
                    alert(responseObj.message);
                    window.location = '/warehouse/addstock';
                } else {
                    alert('Failed: ' + responseObj.error);
                }
            })

        }

    </script>
    @stop

