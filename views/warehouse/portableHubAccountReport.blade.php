@if( Auth::user()->role < 4 || Auth::user()->role == 14)

    <script>window.location = "/warehouse/403";</script>

@endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')

    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form method="post" action="/warehouse/portablehubaccountreportpost">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Portable Hub </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">

                                    <select name="portahub" id="portahub" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($portableHubs as $portaHub)
                                            <option value="{{$portaHub->id}}"<?php if (isset($portableHub) && $portableHub == $portaHub->id) echo 'selected';?>>{{$portaHub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                <p>
                                    <button class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_content">
                        <!--<p> <a class="setdistrict" ><button>Set District</button></a></p> -->
                        <table id="table_captain_account" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th><input id="selectallup" class="dt-checkboxes"
                                           onchange="onChangeAllCheckBox(this)"
                                           type="checkbox">Select
                                </th>
                                <th>WayBill</th>
                                <th>Hub</th>
                                <th>Porta Hub</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $balance = 0;
                            foreach ($orders as $order) { ?>
                            <tr id='<?= 'tr' . $order['jollychic'] ?>'>
                                <td><input id="ch<?= $order['jollychic'] ?>" class="dt-checkboxes"
                                           value="<?= $order['jollychic'] ?>"
                                           onchange="onChangeCreditCheckBox('<?= $order['jollychic'] ?>')"
                                           type="checkbox"></td>
                                <td><?= $order['jollychic'] ?></td>
                                <td><?php if ($order['hub_name'] == 'All') echo ''; else echo $order['hub_name']; ?></td>
                                <td><?=  $order['porta_hub'] ?></td>
                                <td><?= $order['cash_on_delivery']?></td>


                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2">Admin</td>
                                <td></td>
                                <td>Total Selected Amount</td>
                                <td id="selectedAmount">0</td>

                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                           onchange="onChangeAllCheckBox(this)"
                                           type="checkbox">Select
                                </th>
                                <th>WayBill</th>
                                <th>Hub</th>
                                <th>Porta Hub</th>
                                <th>Amount</th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12">Amount Received <span
                                    class="required">*</span>
                        </label>

                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input id="portaAmountReceived" required="required" class="form-control" type="text"/>
                        </div>

                    </div>
                    <div>
                        <button id=updatePortaMoneyReceived>Update Amount</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>


    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>
        var orders = <?php echo $orders; ?>;
        $("#updatePortaMoneyReceived").click(function (e) {
            var amountReceived = document.getElementById('portaAmountReceived').value;
            var ids = [];
            for (var i = 0; i < orders.length; i++) {
                if (document.getElementById('ch' + orders[i].jollychic).checked == true) {
                    ids.push(orders[i].jollychic);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want to update portable Hub amount since rolling back will be difficult?: ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updateMoneyReceivedFromPortableHub',
                    dataType: 'text',
                    data: {
                        ids: ids.join(','),
                        amountReceived: amountReceived,
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(parseFloat(responseObj.amount).toFixed(2) + ' SAR Received from Portable Hub');
                        window.location = '/warehouse/portablehubaccountreport'
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });
        var selectedAmo = parseFloat(selectedAmount.innerHTML);
        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < orders.length; i++) {
                if (document.getElementById('ch' + orders[i].jollychic).checked == checkbox.checked)
                    continue;
                document.getElementById('ch' + orders[i].jollychic).checked = checkbox.checked;
                onChangeCreditCheckBox(orders[i].jollychic);
            }
        }

        function onChangeCreditCheckBox(waybill) {
            var row = document.getElementById("tr" + waybill);
            var checkbox = document.getElementById("ch" + waybill);
            var amount = row.getElementsByTagName("td")[4];
            var selectedAmount = document.getElementById("selectedAmount");
            selectedAmo = parseFloat(selectedAmount.innerHTML);
            if (checkbox.checked) {
                if (amount.innerHTML != '') {
                    selectedAmo = selectedAmo + parseFloat(amount.innerHTML);
                }
            } else {
                if (amount.innerHTML != '') {
                    selectedAmo = selectedAmo - parseFloat(amount.innerHTML);
                }
            }
            selectedAmount.innerHTML = selectedAmo.toFixed(2);

        }
        $('input:checkbox').removeAttr('checked');

    </script>


    @stop

