@if( Auth::user()->role < 5)

  <script>window.location = "/warehouse/403";</script>
  
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Bulk Delivery</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mark items as delivered </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <div class="row">
                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company Old <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select id="company_id_old" name="company_id_old" required="required" class="form-control"
                                        type="text">
                                    @foreach($companies as $compan)
                                        <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company New <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select id="company_id_new" name="company_id_new" required="required" class="form-control"
                                        type="text">
                                    @foreach($companies as $compan)
                                        <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                      <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                          <label class="control-label col-md-4 col-sm-6 col-xs-12">New Transfer Code<span
                                      class="required">*</span>
                          </label>

                          <div class="col-md-8 col-sm-6 col-xs-12">
                              <input type="text" class="form-control" id="new_transfer_code"  placeholder="enter new transfer code">
                          </div>
                      </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20" ></textarea>
                        </div>
                    </div>
                    
                   
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <p><button id="submitForm"  class="btn btn-success">Update Company</button>  </p>
                        </div>
                    </div>
                    
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
     
            
            
          </div>
        </div>
        <!-- /page content -->
        
          <!-- jQuery k-w-h.com/app/views/warehouse/vendors --> 
   
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    
<script  >




$(document).ready(function() {
    
    $("#submitForm").click( function()
    {
        var shipIDs= document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
        var company_id_old= document.getElementById("company_id_old").value.replace(/\s/g, ",");
        var company_id_new= document.getElementById("company_id_new").value.replace(/\s/g, ",");
        var new_transfer_code= document.getElementById("new_transfer_code").value;

        if (new_transfer_code=='')
        {
            bootbox.alert('Please enter New Transfer code'   );
            return;
        }

        if (shipIDs=='')
        {
            bootbox.alert('Please enter IDs!'   );
            return;
        }
        
        var ids = shipIDs.split(',');
        for(i = 0; i < ids.length;++i){
            if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                ids[i] = ids[i].toUpperCase();
        }
        
        var wrongIDs = Array();
        var correctIDs = Array();
        var correctedIDs = Array();
        var finalArray = [];
        for (var i = 0; i <ids.length; i++ )
        {
            if(!ids[i].replace(/\s/g, '').length)
            {
                continue;
            }
            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]))
            {
                correctIDs.push(ids[i]);
            }
            else
            {
                if (/[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('JC' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
                else if (/C[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('J' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
               
                wrongIDs.push(ids[i]);
            }
        }
        
        $.each(correctIDs, function(i, el)
        {
            if($.inArray(el, finalArray) === -1) 
            {
                finalArray.push(el);
            }
        });

        
        
        if (wrongIDs.length)
        {
            bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
        }
        
        if (correctedIDs.length)
        {
            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
        }
    
        if (finalArray.length ) 
        {
            if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true)
            {
               $.ajax({
                    type: 'POST',
                    url: '/warehouse/work/bulkcompanychangepost',
                    dataType: 'text',
                   data: {
                       company_id_old: company_id_old,
                       company_id_new: company_id_new,
                       new_transfer_code: new_transfer_code, ids: finalArray.join(",")
                   },
               }).done(function (response) {
        
                    var responseObj = JSON.parse(response);
                    console.log(response);
                   if (responseObj.error) {
                       alert(responseObj.error)
                   } else if (responseObj.success == true) {
                       if (responseObj) {
                           alert('Company has been changed successfully');
                           location.reload();
                       }
                   }
                    else
                    {
                        alert('Something went wrong!'   );
                        //location.reload();
                    }
                });
            }
            else
            {
                alert('You cancled!' );
            }
            
        } 
        else 
        {
            alert('Nothing to mark as delivered!'   );
            location.reload();
        }    
        
       }
      );
});

</script  >
@stop

