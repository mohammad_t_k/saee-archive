@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif


@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captain Account Report</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/CaptainAccountReportpost">


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">From <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="fromdate" name="fromdate"
                                       value="{{$fromdate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">To <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="todate" name="todate"
                                       value="{{$todate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    <option value="All">All</option>
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Companies <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="company" id="company" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Status <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" <?php if (isset($status) && $status == 1) echo 'selected';?>>Deposited</option>
                                        <option value="0" <?php if (isset($status) && $status == 0) echo 'selected';?>>Not Deposited</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Transaction <span class="required">*</span></label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="transaction" id="transaction" class="form-control">
                                        <option value="All">All</option>
                                        <option value="1" <?php if (isset($transaction) && $transaction == 1) echo 'selected';?>>Cash</option>
                                        <option value="2" <?php if (isset($transaction) && $transaction == 2) echo 'selected';?>>ATM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Admin <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="admin" id="admin" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($admins as $admin)
                                            <option value="{{$admin->id}}"<?php if (isset($admin_id) && $admin_id == $admin->id) echo 'selected';?>>{{$admin->username}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Porta Hub</label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="chosen_porta_hub" id="chosen_porta_hub" class="form-control">
                                        @foreach($adminPortaHubs as $ad_porta_hub)
                                            <option value="{{$ad_porta_hub->porta_hub_id}}" <?php if (isset($chosen_porta_hub) && $ad_porta_hub->porta_hub_id == $chosen_porta_hub) echo 'selected';?>>{{$ad_porta_hub->name}}</option>
                                        @endforeach
                                            <option value="-1" <?php if (isset($chosen_porta_hub) && $chosen_porta_hub== '-1') echo 'selected';?>>
                                                No Porta Hub
                                            </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <button class="btn btn-success" onclick="captainexcelReport()">Export to Excel</button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">

                                <table id="datatable_cod" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th><input id="selectallup" class="dt-checkboxes"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Date</th>
                                        <th>Admin</th>
                                        <th>Paid to Captain Admin</th>
                                        <th>Company</th>
                                        <th>Hub</th>
                                        <th>Porta Hub</th>
                                        <th>City</th>
                                        <th>Captain</th>
                                        <th>Total</th>
                                        <th>Covered Amount</th>
                                        <th>Cash</th>
                                        <th>ATM</th>
                                        <th>Approval Code</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $balance = 0;
                                    $total = 0;
                                    foreach ($items as $item) {
                                    $balance += $item->debit;
                                    $total += $item->credit;
                                    ?>
                                    <tr id='<?= 'tr' . $item['id'] ?>'>
                                        <td><input id="ch<?= $item['id'] ?>" class="dt-checkboxes"
                                                   value="<?= $item['id'] ?>"
                                                   onchange="onChangeAccountHistoryCheckBox('<?= $item['id'] ?>')"
                                                   type="checkbox"></td>
                                        <td><?= date('Y-m-d', strtotime($item['date']))  ?></td>
                                        <td><?= $item['adminuser'] ?></td>
                                        <td><?php if ($item['paid_to_captain'] == 0) echo 'Not Paid to Captain'; else echo $item['paid_admin_name']; ?></td>
                                        <td><?= $item['company'] ?></td>
                                        <td><?php if ($item['hub_name'] == 'All') echo ''; else echo $item['hub_name']; ?></td>
                                        <td><?php if ($item['porta_hub_id'] == '0') echo 'No Porta Hub'; else echo $item['porta_hub_name']; ?></td>
                                        <td><?= $item['city'] ?></td>
                                        <td><?php echo $item->first_name . ' ' . $item->last_name . ' 0' . substr(str_replace(' ', '', $item->phone), -9);?></td>
                                        <td><?= $item['credit'] ?></td>
                                        <td><?php if ($item['covered_amount'] == 0) echo '0'; else echo $item['covered_amount']; ?></td>
                                        <td><?= $item['cash_credit'] ?></td>
                                        <td><?= $item['atm_credit'] ?></td>
                                        <td><?= $item['approval_code'] ?></td>
                                        <td><?php if ($item['banks_deposit_id'] == 0) echo 'Not Deposited'; else  echo 'Deposited';?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="4">Admin</td>
                                        <td colspan="3">Total Selected Amount</td>
                                        <td id="selectedAmount">0</td>
                                        <td>Total Cash</td>
                                        <td id="selectedcashAmount">0</td>
                                        <td>Total ATM</td>
                                        <td id="selectedatmAmount">0</td>
                                    </tr>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Date</th>
                                        <th>Admin</th>
                                        <th>Paid to Captain Admin</th>
                                        <th>Company</th>
                                        <th>Hub</th>
                                        <th>Porta Hub</th>
                                        <th>City</th>
                                        <th>Captain</th>
                                        <th>Total</th>
                                        <th>Covered Amount</th>
                                        <th>Cash</th>
                                        <th>ATM</th>
                                        <th>Approval Code</th>
                                        <th>Status</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount Deposited <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="amountDeposited" name="amountDeposited" required="required"
                                                   class="form-control" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="bankName" name="bankName" required="required"
                                                    class="form-control"
                                                    type="text">
                                                @foreach($banks as $bank)
                                                    <option value="{{$bank->name}}">{{$bank->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <button id="updateDepositAmount" class="btn btn-success">Update Deposit Amount
                                        </button>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        {{--<button id="codReportDirect" class="btn btn-success">Cash On Delivery Report</button>--}}
                                        <button type="button" class="btn btn-success" onclick="CODpopupOption();">Cash On Delivery Report</button>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- table start -->
            <div class="modal fade" id="Modal_of_COD" role="dialog" style="overflow: auto">
                <div class="modal-dialog modal-sm">{{--style="overflow-y: scroll;height: 500px;"--}}
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select Option For COD Report</h4>
                        </div>
                        <div class="modal-body">
                            <table id="cod_report_table" class="table table-striped table-bordered">
                            </table>
                        </div>
                        <div class="modal-footer">
                             <a id="download_codReportDirect" class="btn btn-success" onclick="submitselectedcod()">Submit</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- !table start -->


        </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        var status = document.getElementById('status').value;
        if (status == 1) {
            document.getElementById('updateDepositAmount').disabled = true;
        }
        var city = document.getElementById('city').value;
        if (city == 'All') {
            document.getElementById('updateDepositAmount').disabled = true;
        }
        var items = <?php echo $items; ?>;
        $("#updateDepositAmount").click(function (e) {
            var amountDeposited = document.getElementById('amountDeposited').value;
            var bankName = document.getElementById('bankName').value;
            var city = document.getElementById('city').value;
            var company = document.getElementById('company').value;
            $("#updateDepositAmount").attr("disabled", true);
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want update deposit amount since rolling back will be difficult?") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updateDepositMoneytoBank',
                    dataType: 'text',
                    data: {
                        ids: ids.join(','),
                        amountDeposited: amountDeposited,
                        bankName: bankName,
                        city: city,
                        company: company
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(amountDeposited + ' SAR deposited in Bank  ' + bankName);
                        window.location = '/warehouse/CaptainAccountReport';
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                    $("#updateDepositAmount").attr("disabled", false);
                });
            } else {
                $("#updateDepositAmount").attr("disabled", false);
            }


        });

        /*$("#codReportDirect").click(function (e) {
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0)
                alert('Please select records to view in COD Report');
            else {
                var form = document.createElement("form");
                var element1 = document.createElement("input");
                form.method = "POST";
                form.action = "/warehouse/captaincodreport";
                element1.value = ids.join(',');
                element1.name = "ids";
                form.appendChild(element1);

                document.body.appendChild(form);

                form.submit();
            }

        });*/

        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == checkbox.checked)
                    continue;
                document.getElementById('ch' + items[i].id).checked = checkbox.checked;
                onChangeAccountHistoryCheckBox(items[i].id);
            }
        }

        var selectedAmo = 0;

        function onChangeAccountHistoryCheckBox(history_id) {
            var row = document.getElementById("tr" + history_id);
            var checkbox = document.getElementById("ch" + history_id);
            var credit = row.getElementsByTagName("td")[9];
            var cash = row.getElementsByTagName("td")[11];
            var atm = row.getElementsByTagName("td")[12];
            var selectedAmount = document.getElementById("selectedAmount");
            var selectedcashAmount = document.getElementById("selectedcashAmount");
            var selectedatmAmount = document.getElementById("selectedatmAmount");
            selectedCashAmo = parseFloat(selectedcashAmount.innerHTML);
            selectedAtmAmo = parseFloat(selectedatmAmount.innerHTML);
            selectedAmo = parseFloat(selectedAmount.innerHTML);
            if (checkbox.checked) {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo + parseFloat(credit.innerHTML);
                    selectedCashAmo = selectedCashAmo + parseFloat(cash.innerHTML);
                    selectedAtmAmo = selectedAtmAmo + parseFloat(atm.innerHTML);
                }
            } else {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo - parseFloat(credit.innerHTML);
                    selectedCashAmo = selectedCashAmo - parseFloat(cash.innerHTML);
                    selectedAtmAmo = selectedAtmAmo - parseFloat(atm.innerHTML);
                }
            }

            selectedAmount.innerHTML = selectedAmo.toFixed(2);
            selectedcashAmount.innerHTML = selectedCashAmo.toFixed(2);
            selectedatmAmount.innerHTML = selectedAtmAmo.toFixed(2);
        }

        function captainexcelReport() {
            var i, j;
            var csv = "";

            var table = document.getElementById("datatable_cod");

            var table_headings = table.children[0].children[0].children;
            var table_body_rows = table.children[1].children;

            var heading;
            var headingsArray = [];
            for (i = 1; i < table_headings.length; i++) {
                heading = table_headings[i];
                headingsArray.push('"' + heading.innerHTML + '"');
            }

            csv += headingsArray.join(',') + ";\n";

            var row;
            var columns;
            var column;


            var columnsArray;
            for (i = 0; i < table_body_rows.length ; i++) {
                row = table_body_rows[i];
                columns = row.children;
                columnsArray = [];
                for (j = 1; j < columns.length; j++) {
                    var column = columns[j];
                    columnsArray.push('"' + column.innerHTML + '"');
                }
                csv += columnsArray.join(',') + ";\n";
            }

            download("Captain Account Report.csv", csv);

        }

        //From: http://stackoverflow.com/a/18197511/2265487
        function download(filename, text) {
            var universalBOM = "\uFEFF";
            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + text));
            pom.setAttribute('download', filename);

            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            }
            else {
                pom.click();
            }
        }

        function CODpopupOption() {

            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0) {
                alert('Please select records for COD Report');
                return;
            }
            else {

                function CODpopupOption(callback) {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/checkcaptaincodviewstatus',
                        dataType: 'text',
                        cache: false,
                        success: callback,
                        data: {
                            ids: ids.join(',')
                        }
                    })
                }

                CODpopupOption(function (res) {
                    var data_count = JSON.parse(res);
                    var COD_report_Information = '<caption><h4>COD Report</h4></caption><tr>' + '<th>Select</th>' + '<th>COD</th>' + '</tr>';
                    COD_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="codreportradio">' + '</td><td>' + 'Download COD Report' + '</td></tr><tr><td>'
                            + '<input id="viewcod"  type="radio" name="codreportradio">' + '</td><td>' + 'View COD Report' + '</td></tr>';

                    $('#cod_report_table').html(COD_report_Information);
                    $('#Modal_of_COD').modal("show");
                    if (data_count.count > 500) {
                        document.getElementById("viewcod").disabled = true;
                    }

                });
            }
            return;
        }


        function submitselectedcod() {
            var codreportradio = document.getElementsByName('codreportradio');
            var is_radio_check = '';

            for (var i = 0, length = codreportradio.length; i < length; i++) {
                if (codreportradio[i].checked) {
                    var cod_option = codreportradio[i].parentNode.parentNode.childNodes[1].innerHTML;
                    //if (confirm("Are you sure you want to " + cod_option) == true) {
                        if (cod_option == 'Download COD Report') {
                            var ids = [];
                            for (var i = 0; i < items.length; i++) {
                                if (document.getElementById('ch' + items[i].id).checked == true) {
                                    ids.push(items[i].id);
                                }
                            }
                            if (ids.length == 0) {
                                alert('Please select records to download COD Report');

                            } else {
                                var ids = ids.join(',');
                                var path = '/downloadcaptaincodReportDirect?ids=' + ids;
                                location.href = path;
                                $('#Modal_of_COD').modal('hide');
                            }
                            return;
                        } else if (cod_option == 'View COD Report') {
                            var ids = [];
                            for (var i = 0; i < items.length; i++) {
                                if (document.getElementById('ch' + items[i].id).checked == true) {
                                    ids.push(items[i].id);
                                }
                            }
                            if (ids.length == 0)
                                alert('Please select records to view in COD Report');
                            else {
                                var form = document.createElement("form");
                                var element1 = document.createElement("input");
                                form.method = "POST";
                                form.action = "/warehouse/captaincodreport";
                                element1.value = ids.join(',');
                                element1.name = "ids";
                                form.appendChild(element1);

                                document.body.appendChild(form);

                                form.submit();
                            }

                            $('#Modal_of_COD').modal('hide');
                        }
                    /*} else {
                        bootbox.alert('You cancled!');
                    }*/

                    var is_radio_check = 'ok';
                    break;
                }
            }

            if (is_radio_check == '') {
                bootbox.alert("please select option first");
            }
        }

        $('input:checkbox').removeAttr('checked');
    </script>
@stop