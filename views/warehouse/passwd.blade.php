<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <title>Saee</title>
        <!-- Bootstrap -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo asset_url(); ?>/warehouseadmin/build/css/custom.min.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body class="login">
        <style>
            .incorrect {
                border: 1px solid red !important;
            }
        </style>
        <div>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <form class="form" action="/warehouse/login/passwd/verify" method="post" onsubmit="return validate()">
                            <?php  echo Form::token(); ?>
                            <h1>Change Password</h1>
                            <div>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off" required=""  oninput="check_passwd1()" />
                            </div>
                            <div>
                                <input type="password" name="confirmed_password" id="confirmed_password" class="form-control" placeholder="Confirm Password" autocomplete="off" required="" oninput="check_passwd2()" />
                            </div>
                            <div class="form-actions">
                                <div>
                                    <button class="btn btn-default submit" type="submit" >Submit</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">
                                <div class="clearfix"></div>
                                <br />
                                <div class="pull-right">&copy; All rights reserved Saee</div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
        <script>
            @if(isset($_GET['error']) && $_GET['error'] == 'same')
            alert('You entered the same password');
            @endif
            function validate() {
                let passwd1 = document.getElementById('password').value;
                let passwd2 = document.getElementById('confirmed_password').value;
                if(passwd1 != passwd2) {
                    alert("Two Passwords does not match");
                    return false;
                }
                return true;
            }
            function check_passwd1() {
                let passwd1 = document.getElementById('password').value;
                let passwd2 = document.getElementById('confirmed_password').value;
                if(passwd1 != passwd2)
                    document.getElementById('confirmed_password').className = 'form-control incorrect';
                else 
                    document.getElementById('confirmed_password').className = 'form-control';
            }
            function check_passwd2() {
                let passwd1 = document.getElementById('password').value;
                let passwd2 = document.getElementById('confirmed_password').value;
                if(passwd1 != passwd2)
                    document.getElementById('confirmed_password').className = 'form-control incorrect';
                else 
                    document.getElementById('confirmed_password').className = 'form-control';
            }
        </script>
    </body>
</html>
