@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<?php

$cities = City::select(['id', 'name'])->get();
$roles = Role::select(['id', 'title'])->get();

$existingCities =  explode(",", $existingCities);


$i = 0;

 ?>

<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif
           <div class="box-body ">
                <form action="" method="POST" id="AdminForm">
                    <input type="hidden" id="id" name="id" value="{{ $owner->id }}">
                <div class="form-group">
                    <label>Username</label><input required="" value="{{ $owner->username }}" id="username" required="" class="form-control" type="text" name="username"
                                              placeholder="Add User Name">
                </div>
                <div class="form-group">
                    <label>Email</label><input value="{{ $owner->email }}" id="email" class="form-control" type="text" name="email"
                                              placeholder="Add User Email">
                </div>

                <div class="form-group">
                    <label>Cities</label>
                    <select multiple name="cities" id="cities" style="width: 100%;height: 100px;border: 1px solid #CCCCCC;padding-left: 10px;" >

                       @foreach($cities as $aCity)
                        <option <?php if (in_array($aCity->name, $allowed_cities))  echo 'selected = "selected"'; ?> value="{{$aCity->id}}">{{$aCity->name}}</option>
                        {{ $i++ }}
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Hubs</label>
                    <select multiple name="hubs" id="hubs" style="width: 100%;height: 100px;border: 1px solid #CCCCCC;padding-left: 10px;" >
                        @foreach($hubs as $hub)
                            <option <?php if(in_array($hub->id, $admin_hubs))  echo 'selected = "selected"'; ?> value="{{$hub->id}}">{{$hub->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Porta Hubs</label>
                    <select multiple name="porta_hubs" id="porta_hubs" style="width: 100%;height: 100px;border: 1px solid #CCCCCC;padding-left: 10px;" >
                        @foreach($porta_hubs as $porta_hub)
                            <option <?php if(in_array($porta_hub->id, $admin_porta_hubs))  echo 'selected'; ?> value="{{$porta_hub->id}}">{{$porta_hub->name}}</option>
                        @endforeach
                    </select>
                </div>

               <div class="form-group">
                    <label>Role</label>
                    <select name="role" id="role" required="" style="width: 100%;height: 35px;border: 1px solid #CCCCCC;padding-left: 10px;" >
                       @foreach($roles as $aRole)
                        <option <?php if (!empty($role) && $role == $aRole->id)  echo 'selected = "selected"'; ?> value="{{$aRole->id}}">{{$aRole->title}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                         
                                <label style="color: #73879C; font-size: 13px; top: 0px;">Is Blackbox User ?</label>
                                <select name="is_bb_user" id="is_bb_user" class="form-control col-md-8 col-xs-12" required="required" >
                                    
                                        <option <?php if($owner->is_bb_user == '0') echo 'selected = "selected"' ?> value="0">No</option>
                                        <option <?php if($owner->is_bb_user == '1') echo 'selected = "selected"' ?> value="1">Yes</option>
                                        
                                </select>
                                
                            
                </div>

            </div>
           

                <button  type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Update Admin</button>

            </form>
        

    </div>
</div>
</div>

<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>


        $(document).ready(function () {

            $("#AdminForm").submit(function (e) {
                
                        var username = document.getElementById("username").value;
                        var id = document.getElementById("id").value;
                        var email = document.getElementById("email").value;
                        var role = document.getElementById("role").value;
                        var is_bb_user = document.getElementById("is_bb_user").value;
                        var cities = new Array();//storing the selected values inside an array
                        var hubs = new Array();
                        var porta_hubs = new Array();
                       
                       	jabberusername = username.substring(0, username.indexOf("@"));

                         $('#cities :selected').each(function(i, selected) {
                            cities[i] = $(selected).val();
                        });

                        $('#hubs :selected').each(function(i, selected) {
                            hubs[i] = $(selected).val();
                        });

                        $('#porta_hubs :selected').each(function(i, selected) {
                            porta_hubs[i] = $(selected).val();
                        });

                        //var allowed_pages = allowed_pages.split(',');
                       // var allowed_actions = allowed_actions.split(',');


                        var wrongIDs = Array();
                        var correctAllowedPages = Array();
                        var correctAllowedActions = Array();
                        var correctedIDs = Array();
                        var finalArrayAllowedPages = [];
                        var finalArrayAllowedActions = [];
                        



                       /* if (correctedIDs.length) {
                            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                        } */

                        if (cities.length) {
                            if (confirm("Are you sure you want to update admin ? ") == true) {
                                 e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/system/edit_admin_post',
                  
                                    data: {username: username,email: email, role: role, cities: cities, hubs: hubs, porta_hubs: porta_hubs, id: id, is_bb_user: is_bb_user, jabberusername: jabberusername},
                                }).done(function (response) {
                                    

                                    console.log(response);
                                    var responseObj = JSON.parse(response);  


                                    if(responseObj.message == 'good' && responseObj.removed == 0){

                                       alert('Successfully registered your username is ' + responseObj.username);
                                       window.location='/warehouse/system/users?success=3';

                                   }  
                                   if(responseObj.message == 'good' && responseObj.removed == 1){

                                       alert('Successfully removed username for Blackbox ' + responseObj.username);
                                       window.location='/warehouse/system/users?success=3';

                                   }                         
                                    
                                    if(responseObj.message != undefined){
                                        // error exception here
                                        if(responseObj.message == 'good' || responseObj.message == 'bad'){

                                        }else{

                                        alert(responseObj.message);
                                        }

                                    }else{

                                    	alert(responseObj.message);
                                    }


                                }); // Ajax Call

                            }
                            else {
                                bootbox.alert('You cancled!');
                                return false;
                            }

                        }
                        else {
                            bootbox.alert('Nothing to signin!');
                            return false;
                        }


                    }
            );
        });

    </script>

@stop