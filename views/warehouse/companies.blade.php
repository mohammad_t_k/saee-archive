@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<style>
    table th {
        text-align: center;
    }
    table td {
        text-align: center;
    }
</style>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
    <div class="page-title">
        <div class="title_left">
        <h3>Captains data</h3>
        </div>
        
    </div>
        
            
        <div class="row">
           
          <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Companies information</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Companies data
                    </p>
                    <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->
                  
                    
                    <table id="datatable_captains" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>Company ID</th> 
                          <th>Compnay Name</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>City</th>
                          <th>Type</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      <tbody>
                                    <?php foreach ($companies as $company) { ?>
                                    <tr id='<?= 'tr'.$company['id'] ?>'>
                                        <td><?= $company['id'] ?></td>
                                        <td><?= $company['company_name'] ?></td>
                                        <td><?= $company['username'] ?></td>
                                        <td><?= $company['email'] ?></td>
                                        <td><?= $company['phone'] ?></td>
                                        <td><?= $company['city'] ?></td>
                                        <?php
                                        if($company['company_type'] == 0) $company['company_type'] = "Dependant Company";
                                        if($company['company_type'] == 1) $company['company_type'] = "Multi-Store";
                                        if($company['company_type'] == 2) $company['company_type'] = "Sub-Company";
                                        if($company['company_type'] == 3) $company['company_type'] = "Merchant";
                                        ?>
                                        <td><?= $company['company_type'] ?></td>
                                        <td><a href="/warehouse/companydetail?id=<?= $company['id']?>">View Details</a></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                      
                      
                      <tfoot>
                        <tr>
                          <th>Company ID</th> 
                          <th>Compnay Name</th>
                          <th>username</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>City</th>
                          <th>Type</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            
        <?php //echo $captainsdata ?>



        </div>
    </div>
</div>
<!-- /page content -->
                    

<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->

 <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
@stop
