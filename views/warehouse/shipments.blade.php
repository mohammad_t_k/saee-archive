@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

 <!-- page content -->
 
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        
        <div class="page-title">
        <div class="title_left">
        <h3><?php echo $shipmentDate ?> Shipments <small>Jolly Chick Shipments</small></h3>
        </div>
        
        <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
        </div>
        </div>
        <!-- top tiles -->
        <div class="row tile_count">
            <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-truck"></i> Shipment Date</span>
                <div class="count">< ?php echo $shipmentDate ?> </div>
                
            </div>
            -->    
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>
                <div class="count"><?php echo $allitems ?> </div>
                
            </div>
             
             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-thumbs-down"></i>  In our office </span>
                <div class="count red"><?php echo $arriveditems ?></div>
            </div>   
               
             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-car"></i> With Captains</span>
                <div class="count blue"><?php echo $outfordeliveryitems ?></div>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-thumbs-ub"></i> Delivered </span>
                <div class="count green"><?php echo $delivreditems ?></div>
            </div>
            
           <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-home"></i> In Riyadh</span>
                <div class="count ">< ?php //echo $penddingitems ?></div>
            </div> -->
                
            <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-truck"></i> In the way to Jeedah</span>
                <div class="count ">< ?php //echo $onwayitems ?></div>
            </div> -->
        </div> 
        <!--<div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-home"></i> In Jeedah</span>
                <div class="count green">< ?php //echo $arriveditems ?></div>
            </div> 
            
        </div> -->
        <div class="row tile_count">        
           <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Returned to JC </span>
                <div class="count black"><?php //echo $returnedtojcitems ?></div>
            </div> -->
                
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Cash collected </span>
                <div class="count green"><?php echo number_format($cashcollected,2) ?></div>
            </div>
                
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Cash to be collected </span>
                <div class="count red"><?php echo number_format($cashtobecollected,2) ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Success Rate </span>
                <div class="count red"><?php echo number_format($successrate,2) ?>%</div>
            </div>
        </div>
          <!-- /top tiles -->
  
    

        <div class="clearfix"></div>

        <div class="row">
            <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All statues <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    جدول بجميع الحالات
                    </p>

                    <table id="datatable_summary" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                     
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            <!-- table start
             <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Latest <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Latest shipments
                    </p>
                    
                    <table id="datatable_newshipments" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                     
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
           
            <!-- !table start -->
        
            <!-- table start 
             <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Older <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Older shipment
                    </p>
                    
                    <table id="datatable_oldshipments" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                     
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Status</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            <!-- table start -->
        <!--    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>In Riyadh <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items in Riyadh
                    </p>
                    
                    

                    <table id="datatable_inRiyadh" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            -->
            <!-- !table start -->
            <!-- table start -->
         <!--   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>In the way to Kasper Warehouse <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items that left Riyadh
                    </p>
                    <p id = moveToKasperWarehouse><button>Move selected to Kasper Warehouse</button></p>

                    <table id="datatable_leftRiyadh" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            -->
            <!-- !table start -->
            <!-- table start -->
         <!--   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>In Kasper Warehouse <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items in Kasper Warehouse
                    </p>
                    <p id = markAsDelivered><button>Mark selected as Delivered</button></p>
                    <table id="datatable_inWarehouse" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th></th>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>  
            
            -->
            <!-- !table start -->
            
             <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Dispatched | مع الكباتن <small>In delivery process</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items that are out for delivery
                    </p>
                  
                    <table id="datatable_inWithCaptains" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Pincode</th>
                          <th>Captain</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
    
            <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Delivered <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items that are delivered
                    </p>
                    

                    <table id="datatable_delivered" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Captain</th>
                          <th>Delivery Date</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Captain</th>
                          <th>Delivery Date</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            <!-- table start 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>UN Delivered | الرجيع<small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items that were attemted to deliver but failed. رجيع. 
                    </p>
                   

                    <table id="datatable_unDelivered" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Reason</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Reason</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            <!-- table start -->
         <!--   <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Returned to JollyChic <small>Shipments</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Items that were returned to JollyChic  
                    </p>
                    
                    <table id="datatable_returnedToJC" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Reason</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>JollyChicID</th>
                          <th>Name</th>
                          <th>Mobile1</th>
                          <th>Mobile2</th>
                          <th>Street Addrs</th>
                          <th>District</th>
                          <th>COD</th>
                          <th>Reason</th>
                          <th>Details</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            -->
            <!-- !table start -->
        <?php //echo $orders ?>



        </div>
    </div>
</div>
<!-- /page content -->
                    
        



<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->

 <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
 
 
 <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
   
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
<script  >

<?php
            
            
$mydata = '';
//$mydata_inRiyadh = '';
//$mydata_leftRiyadh = '';
$mydata_inWarehouse = ''; 
$mydata_inWithcaptains = '';
$mydata_inDelivered = '';
//$mydata_backToOffice = '';
//$mydata_returnedToJC = '';

//$mydata_oldshipments = '';
//$mydata_newshipments = '';

/*foreach ($oldorders as $order) {
    
  $mydata_oldshipments .= "[ \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"$order->status\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
}


foreach ($neworders as $order) {
    
  $mydata_newshipments .= "[ \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"$order->status\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
}*/

foreach ($withcaptainsorders as $order) {


    $mydata_inWithcaptains .= "[ \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\",  \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  
}
foreach ($deliveredorders as $order) {

    $mydata_inDelivered .= "[  \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->d_district\", \"$order->cash_on_delivery\",  \"$order->confirmed_walker\", \"$order->dropoff_timestamp\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  
}
foreach ($orders as $order) {
    
  $mydata .= "[ \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"$order->status\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  
  /*if ($order->status == 'In Riyadh' )
  {
      $mydata_inRiyadh .= "[ \"$order->id\", \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }
  
  
  if ($order->status == 'Left Riyadh' )
  {
      $mydata_leftRiyadh .= "[ \"$order->id\", \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }
  
  if ($order->status == 'In Jeddah' )
  {
      $mydata_inWarehouse .= "[ \"$order->id\", \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }
  
  if ($order->status == 'With Captain' )
  {
    $mydata_inWithcaptains .= "[ \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\",  \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \"$order->confirmed_walker\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }
  
  if ($order->status == 'Delivered' )
  {
        $mydata_inDelivered .= "[  \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->d_district\", \"$order->cash_on_delivery\",  \"$order->confirmed_walker\", \"$order->dropoff_timestamp\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }*/
  
  /*if ($order->status == 'Reschdule' )
  {
      $mydata_backToOffice .= "[  \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->undelivered_reason\",  \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }*/
  
  /* if ($order->status == 'Returned to JC' )
  {
      $mydata_returnedToJC .= "[ \"$order->id\", \"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->undelivered_reason\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
  }*/

}
?>
        
$(document).ready(function() {
    $("#datatable_summary").DataTable({
   
                
    "data": [
      <?php echo $mydata ?> 
       
    ],
    "autoWidth": false,
    
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
   
    'order': [[0, 'asc']]
    });
    
    
    /*var table_oldshipments = $("#datatable_oldshipments").DataTable({
    
    "data": [
      < ?php echo $mydata_oldshipments ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });*/
    
    /* var table_newshipments = $("#datatable_newshipments").DataTable({
    
    "data": [
      < ?php echo $mydata_newshipments ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });*/
    
   /*var table_inRiyadh = $("#datatable_inRiyadh").DataTable({
    
    "data": [
      < ?php echo $mydata_inRiyadh ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });
    */
     
     /*var table_leftRiyadh = $("#datatable_leftRiyadh").DataTable({
    
    "data": [
      < ?php echo $mydata_leftRiyadh ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });
    */
    
    var table_inWarehouse = $("#datatable_inWarehouse").DataTable({
    
    "data": [
      <?php echo $mydata_inWarehouse ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });
    
    
    var table_inWithCaptains = $("#datatable_inWithCaptains").DataTable({
    
    "data": [
      <?php echo $mydata_inWithcaptains ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true,
    'order': [[1, 'asc']]
    });
    
    
    var table_delivered  = $("#datatable_delivered").DataTable({
    
    "data": [
      <?php echo $mydata_inDelivered ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    
    'order': [[1, 'asc']]
    });
    
   /* var table_unDelivered  = $("#datatable_unDelivered").DataTable({
    
    "data": [
      < ?php echo $mydata_backToOffice ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });
    */
    

    /*var table_returnedToJC = $("#datatable_returnedToJC").DataTable({
    
    "data": [
      < ?php echo $mydata_returnedToJC ?> 
      
    ],
    "autoWidth": false,
   
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[1, 'asc']]
    });
    */

   /* $("#pickUpFromRiyadh").click( function()
           {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                 var rows_selected = table_inRiyadh.column(0).checkboxes.selected();
                 
                if (confirm("Are you sure you want to pick up selected from Riyadh?") == true) 
                {
                     console.log(rows_selected.join(','));
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/updatestatus',
                        dataType: 'text',
                        data: {to:'1',  ids:rows_selected.join(',')},
                    }).done(function (response) {
                        alert(rows_selected.count() + ' items have been moved'   );
                        console.log(response);
                        if (response==1)
                        {
                            location.reload();
                        }
                    }); // Ajax Call
            
                } else {
               
                }    
             
           }
      );
    
    $("#moveToKasperWarehouse").click( function()
           {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                 var rows_selected = table_leftRiyadh.column(0).checkboxes.selected();
                 
                if (confirm("Are you sure you want to move selcted items to Kasper Warehouse") == true) 
                {
                    console.log(rows_selected.join(','));
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/updatestatus',
                        dataType: 'text',
                        data: {to:'2',  ids:rows_selected.join(',')},
                    }).done(function (response) {
                        alert(rows_selected.count() + ' items have been moved'   );
                        console.log(response);
                        if (response==1)
                        {
                            location.reload();
                        }
                    }); // Ajax Call
            
                } else {
               
                }    
             
           }
      );*/
    
    
} );


</script>


@stop
