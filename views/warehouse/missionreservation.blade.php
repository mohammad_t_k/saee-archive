@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif
    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Bulk Mission Creation</h3>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form method="get" action="/warehouse/missionreservation">
                            <div class="row">

                                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control" onchange="onChangeCity()">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-6 col-xs-12">Hub </label>
                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                        <select name="hub" id="hub" class="form-control" onchange="onChangehub()">
                                            @foreach($cityHubs as $adhub)
                                                <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-5 col-sm-3 col-xs-12"> Scheduled Shipment Date
                                    </label>

                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <select name="scheduled_date" id="scheduled_date" class="form-control">
                                            <option value="all">All</option>
                                            <?php
                                            foreach ($scheduled_Dates as $scheduled_Date) {
                                                if ($scheduled_Date->scheduled_shipment_date != null) {
                                                    $selected = '';
                                                    $date = $scheduled_Date->scheduled_shipment_date;
                                                    if (isset($shipmentDate) && $shipmentDate == $date)
                                                        $selected = 'selected';
                                                    echo "<option value='$date' $selected>$date</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Max Shipments
                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                <input type="text" name="maxShipment" id="maxShipment" value="25">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                <p>
                                    <button id="missionCreate"  class="btn btn-success" onclick='create_mission()'>Create Missions</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="row">

                <!-- table start -->

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2> All Records group by districts and scheduled shipment Date</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table id="datatable_planningALL" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>District</th>
                                    <th>Number of shipments</th>
                                    <th>Scheduled Shipment Date</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>District</th>
                                    <th>Number of shipments</th>
                                    <th>Scheduled Shipment Date</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->


                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Mission Reservation
                                <small></small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row" style=" margin-bottom: 20px;">
                                <div class="col-md-5 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-5 col-sm-5 col-xs-12">Start Pickup Time<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <input id="start_pickup_time" name="start_pickup_time"
                                               value="13:00" class="time-picker form-control col-md-7 col-xs-12"
                                               type="time">
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-5 col-sm-5 col-xs-12">End Pickup Time<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <input id="end_pickup_time" name="end_pickup_time"
                                               value="16:00" class="time-picker form-control col-md-7 col-xs-12"
                                               type="time">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12"><button  class="btn btn-success" onclick='reservemission()'>Reserve</button></div>

                            </div>
                            <div class="row" style=" margin-bottom: 20px;">
                                <div class="col_md-2 col-sm-2 col-xs-12"><button  class="btn btn-success" onclick='unreservemission()'>Un Reserve</button></div>
                                <div class="col_md-2 col-sm-2 col-xs-12"><button  class="btn btn-success" onclick='unplanmission()'>Un Plan</button></div>
                                <div class="col_md-2 col-sm-2 col-xs-12"><button  class="btn btn-success" onclick='sendNotificationMission()'>Send Notification</button></div>
                                <div class="col_md-2 col-sm-2 col-xs-12"><button  class="btn btn-success" onclick='rollback()'>Rollback </button></div>
                                </div>
                            </div>
                                <table id="datatable_missionALL" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Name</th>
                                    <th>District</th>
                                    <th>Number of shipments</th>
                                    <th>Scheduled Shipment Date</th>
                                    <th>Mission</th>
                                    <th>Notify to Captain</th>
                                    <th>Reserve Captain</th>
                                    <th>Planned Captain</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Name</th>
                                    <th>District</th>
                                    <th>Number of shipments</th>
                                    <th>Scheduled Shipment Date</th>
                                    <th>Mission</th>
                                    <th>Notify to Captain</th>
                                    <th>Reserve Captain</th>
                                    <th>Planned Captain</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

            </div>
            <!-- table start -->
            <div class="modal fade" id="myModal" role="dialog" style="overflow: auto">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Manually Send Notificaion to Captain</h4>
                        </div>
                        <div class="modal-body">
                            <table id="firsttable" class="table table-striped table-bordered">
                            </table>
                            <table id="secondtable" class="table table-striped table-bordered">
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script>
        //\'$order->d_city\',\'$order->scheduled_shipment_date\',\'$order->d_district\'
        //encodeURIComponent(partId)
        <?php

        $mydata_allShipments = '';
        foreach ($allshipments as $order) {
            $mydata_allShipments .= "[  \"$order->d_district\" , \"$order->shipments\", \"$order->scheduled_shipment_date\"], ";
        }

        $mydata_allMissions = '';
        foreach ($Missions as $order) {

        $isNotify =$order->is_notify;
        if ($isNotify == 0){
                    $isNotify ='No';
                    } else{
                    $isNotify ='Yes';
                    }

        $reserve_captain = $order->first_name;
        if ($reserve_captain){
                $reserve_captain = $reserve_captain . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
                }
            else {
                $reserve_captain = 'Not Set';
                }

                $confirmed_captain = $order->confirst_name;
        if ($confirmed_captain){
                $confirmed_captain = $confirmed_captain . ' ' . $order->conlast_name . ' 0' . substr(str_replace(' ', '', $order->conphone), -9);
                }
            else {
                $confirmed_captain = 'Not Set';}

            $mydata_allMissions .= "[ \"$order->id\", \"$order->id\" ,\"<a onclick=opensticker(\'$order->mission_waybill\')>$order->mission_waybill</a>\" ,\"$order->mission_name\" ,  \"$order->district\",\"$order->total_shipments\",\"$order->scheduled_shipment_date\",
            \"$order->mission_status\",\"$isNotify\",\"$reserve_captain\",\"$confirmed_captain\"], ";


        }



        ?>



        $(document).ready(function () {

                    var table_planningALL = $("#datatable_planningALL").DataTable({

                        "data": [
                            <?php echo $mydata_allShipments ?>

                        ],
                        "pageLength": 25,
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        /*'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }
                        ],*/
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });

                    var table_mission = $("#datatable_missionALL").DataTable({

                        "data": [
                            <?php echo $mydata_allMissions ?>

                        ],
                        "pageLength": 25,
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                         {
                         'targets': 0,
                         'checkboxes': {
                         'selectRow': true
                         }
                         }
                         ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });

                });
        var enable_missions_creation = <?php echo $enableMissionCreation; ?>;
        if(enable_missions_creation == 0 ) {
            document.getElementById('missionCreate').disabled = true;
        }

        function onChangeCity() {
            $("#hub").empty();
            var city = document.getElementById('city').value;
            $.ajax({
                type: 'GET',
                url: '/warehouse/gethubsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var hubs = responseObj.hubs;
                    var select = document.getElementById('hub');
                    for (var i = 0; i < hubs.length; i += 1) {
                        if(hubs[i].name == '')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', hubs[i].hub_id);
                        option.appendChild(document.createTextNode(hubs[i].name));
                        select.appendChild(option);
                    }
                    if (hubs[0].enable_missions_creation == 0) {
                        document.getElementById('missionCreate').disabled = true;
                    } else {
                        document.getElementById('missionCreate').disabled = false;
                    }
                }
            });


        }

        function onChangehub() {
            var city = document.getElementById('city').value;
            var hub = document.getElementById('hub').value;

            $.ajax({
                type: 'GET',
                url: '/warehouse/isHubMissionEnabled',
                dataType: 'text',
                data: {city: city, hub:hub},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                var hubs = responseObj.hub;
                if (hubs[0].hub_id == hub) {
                    if (hubs[0].enable_missions_creation == 0) {
                        document.getElementById('missionCreate').disabled = true;
                    } else {
                        document.getElementById('missionCreate').disabled = false;
                    }
                }
            });


        }

        function create_mission(){
            var scheduled_date =document.getElementById('scheduled_date').value;
            var city = document.getElementById('city').value;
            var maxShipment = document.getElementById('maxShipment').value;
            var hub = document.getElementById('hub').value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/generatemissions',
                dataType: 'text',
                data: {city: city, scheduled_shipment_date: scheduled_date,maxShipment: maxShipment, hub:hub},
            }).done(function (responsef) {
                responseObjf = JSON.parse(responsef);

                if (responseObjf.error) {
                    bootbox.alert(responseObjf.error);
                    return;
                } else if (responseObjf.new_records) {
                    bootbox.alert(responseObjf.message);
                     $('#datatable_planningALL').DataTable().clear().draw();

                    var new_records = responseObjf.new_records;
                    var datatable_mission = $('#datatable_missionALL').DataTable();

                    $.each(new_records, function (i, item) {
                        var reserve_captain ='Not Set';
                        var confirmed_captain ='Not Set';

                        //var planning_button='<button onclick="planmission(this);">Plan</button>'
                        datatable_mission.row.add([
                                    item.id,
                                    item.id,
                                    '<a onclick=opensticker("' + item.mission_waybill + '")>' + item.mission_waybill + '</a>',
                                    item.mission_name,
                                    item.district,
                                    item.total_shipments,
                                    item.scheduled_shipment_date,
                                    'New Mission',
                                    item.is_notify,
                                    reserve_captain,
                                    confirmed_captain]
                        ).draw();
                    });
                } else {
                    bootbox.alert(responseObjf.message);
                }
            });
        }

        function reservemission(){

            var table_mission=$('#datatable_missionALL').DataTable();
            var rows_selected = table_mission.column(0).checkboxes.selected();
            var start_pickup_time = document.getElementById('start_pickup_time').value;
            var end_pickup_time = document.getElementById('end_pickup_time').value;
            var scheduled_date =document.getElementById('scheduled_date').value;
            if (rows_selected.count())
            {
                var captain_id = prompt("Please captain ID:");
                if (captain_id != null && captain_id != "")
                {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptaininfo',
                        dataType: 'text',
                        data: {captain_id:captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response==0)
                        {
                            bootbox.alert('No captain is registred with entered ID');
                            return;
                        }
                        else
                        {
                            responseObj = JSON.parse(response);
                            var captain_firstName = responseObj.first_name;
                            var captain_lastName = responseObj.last_name;
                            var captain_city = responseObj.city;

                            if (confirm("Are you sure you want to Resereve captain: " + captain_firstName + " "  + captain_lastName +" for selected missions" ) == true)
                            {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/reservecaptain',
                                    dataType: 'text',
                                    data: {captainid:responseObj.id,  ids:rows_selected.join(','), captain_city:captain_city, scheduled_shipment_date: scheduled_date,
                                        start_pickup_time:start_pickup_time, end_pickup_time:end_pickup_time },
                                }).done(function (response) {
                                    var responseObj2 = JSON.parse(response);
                                    console.log(response);
                                    if (responseObj2.success==true)
                                    {
                                        if(responseObj2.count != 0) {
                                            alert(responseObj2.count + ' missions have been Reserved for captain ' + captain_firstName  + " "  + captain_lastName  );
                                        location.reload();
                                        } else {
                                            alert( 'missions not Reserved for captain ' + captain_firstName  + " "  + captain_lastName +' somthing went wrong.');
                                        }
                                    }
                                    else
                                    {
                                        alert('error:' + responseObj2.error);
                                    }
                                });

                            } else {
                                bootbox.alert('You canceled!');
                            }
                        }
                    }); // Ajax Call

                }

            } else {
                bootbox.alert('Please select missions first');
                return;
            }

        }

        function unreservemission(){

            var table_mission=$('#datatable_missionALL').DataTable();
            var rows_selected = table_mission.column(0).checkboxes.selected();
            if (rows_selected.count())
            {
                var captain_id = prompt("Please captain ID:");
                if (captain_id != null && captain_id != "")
                {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptaininfo',
                        dataType: 'text',
                        data: {captain_id:captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response==0)
                        {
                            bootbox.alert('No captain is registred with entered ID');
                            return;
                        }
                        else
                        {
                            responseObj = JSON.parse(response);
                            var captain_firstName = responseObj.first_name;
                            var captain_lastName = responseObj.last_name;

                            if (confirm("Are you sure you want to Un Resereve captain: " + captain_firstName + " "  + captain_lastName +" for selected missions" ) == true)
                            {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/unreservecaptain',
                                    dataType: 'text',
                                    data: {captainid:responseObj.id,  ids:rows_selected.join(',')},
                                }).done(function (response) {
                                    var responseObj2 = JSON.parse(response);
                                    console.log(response);
                                    if (responseObj2.success==true)
                                    {
                                        if(responseObj2.count != 0) {
                                            alert(responseObj2.count + ' missions have been un Reserved for captain ' + captain_firstName  + " "  + captain_lastName  );
                                            location.reload();
                                        } else {
                                            alert( 'No mission un Reserved for captain ' + captain_firstName  + " "  + captain_lastName +' somthing went wrong.');
                                        }
                                    }
                                    else
                                    {
                                        alert('error:' + responseObj2.error);
                                    }
                                });

                            } else {
                                bootbox.alert('You canceled!');
                            }
                        }
                    }); // Ajax Call

                }

            } else {
                bootbox.alert('Please select missions first');
                return;
            }

        }

        function unplanmission(){

            var table_mission=$('#datatable_missionALL').DataTable();
            var rows_selected = table_mission.column(0).checkboxes.selected();
            var city = document.getElementById('city').value;
            if (rows_selected.count())
            {
                            if (confirm("Are you sure you want to Un plan selected missions") == true)
                            {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/unplancaptain',
                                    dataType: 'text',
                                    data: {  ids:rows_selected.join(','), city:city},
                                }).done(function (response) {
                                    var responseObj = JSON.parse(response);
                                    console.log(response);
                                    if (responseObj.success==true)
                                    {
                                        if(responseObj.count != 0) {
                                            alert(responseObj.count + ' missions have been Un Plan');
                                            location.reload();
                                        } else {
                                            alert( 'Missions not un plan  because captain already scanned items.');
                                        }
                                    }
                                    else
                                    {
                                        alert('error: ' + responseObj.error);
                                    }
                                });

                            } else {
                                bootbox.alert('You canceled!');
                            }

            } else {
                bootbox.alert('Please select missions first');
                return;
            }

        }

        function sendNotificationMission(){
            var city = document.getElementById('city').value;
            var start_pickup_time = document.getElementById('start_pickup_time').value;
            var end_pickup_time = document.getElementById('end_pickup_time').value;
            var scheduled_date =document.getElementById('scheduled_date').value;
            var table_mission=$('#datatable_missionALL').DataTable();
            var rows_selected = table_mission.column(0).checkboxes.selected();
            if (rows_selected.count())
            {
                if (confirm("Are you sure you want to send notification to "+ city +"'s captains for selected missions" ) == true)
                            {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/sendMissionNotificationToActiveCaptains',
                                    dataType: 'text',
                                    data: {city:city,  ids:rows_selected.join(','), scheduled_shipment_date: scheduled_date,
                                        start_pickup_time:start_pickup_time, end_pickup_time:end_pickup_time },
                                }).done(function (response) {
                                    var responseObj = JSON.parse(response);
                                    console.log(response);
                                    if (responseObj.success==true)
                                    {
                                        if(responseObj.count != 0) {
                                            alert(responseObj.count + ' missions notification sent to' + city + '  captains');
                                            location.reload();
                                        } else {
                                            alert( 'notification not sent, something went Worng');
                                        }
                                    }
                                    else
                                    {
                                        alert('error:' + responseObj.error);
                                    }
                                });

                            } else {
                                bootbox.alert('You canceled!');
                            }
                        } else {
                bootbox.alert('Please select missions first');
                return;
            }

        }

        function rollback() {
            var responseObj;
            var city = document.getElementById('city').value;
            var scheduled_shipment_date = document.getElementById('scheduled_date').value;
            var responseObj;
            var table_mission=$('#datatable_missionALL').DataTable();
            var rows_selected = table_mission.column(0).checkboxes.selected();
            if (rows_selected.count())
            {
            if (confirm("Are you sure you want Roll Back selected Missions for selected city and scheduled date" ) == false) {
                bootbox.alert('You canceled!');
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/warehouse/missionsrollback',
                dataType: 'text',
                data: {
                    city: city,
                    scheduled_shipment_date: scheduled_shipment_date,
                    ids:rows_selected.join(',')
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success==true)
                {
                    if(responseObj.count != 0) {
                        alert(responseObj.count + ' missions have been Rollback');
                        location.reload();
                    } else {
                        alert( 'missions not Rollback  somthing went wrong.');
                    }
                }
                else
                {
                    alert('error:' + responseObj.error);
                }

            }); // Ajax Call
            } else {
                bootbox.alert('Please select missions first');
                return;
            }
        };

        function opensticker(waybill) {
            window.open('/warehouse/missionsticker/' + waybill, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }

    </script>

    @stop

