@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <!-- page content -->
    <!-- page content -->
    <style>
        /* The Modal (background) */
        #modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 400px; /* Could be more or less, depending on screen size */
            height: 250px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Bulk Captain Assignment Shipments</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Assign a Group of Shipments to a Captain</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <textarea id="shipmentIDs" required="required" class="form-control" type="text"
                                                  rows="20"></textarea>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Assign</button>
                                            <button id="unAssignForm" class="btn btn-success">Un Assign</button>
                                            <button id="scanform" class="btn btn-success"
                                            <?php
                                                $admin_role = Session::get('admin_role');
                                                if ($admin_role == 9 || $admin_role == 12) {
                                                    echo 'style="visibility: hidden;" disabled';
                                                }
                                                ?>
                                            >Scan
                                            </button>
                                        </p>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Customer Name</th>
                                    <th>Ignore</th>
                                    <th>Mark As Delivered</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Customer Name</th>
                                    <th>Ignore</th>
                                    <th>Mark As Delivered</th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>
                    <div class="x_title">
                        <h2>Planned Shipments to a Captain</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">

                            <table id="datatable_plan" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Receiver Name</th>
                                    <th>Planned Captain</th>
                                    <th>Confirmed Captain</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Receiver Name</th>
                                    <th>Planned Captain</th>
                                    <th>Confirmed Captain</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- /page content -->
    <div id="modal" class="modal">
        <!-- Modal content -->
        <div id="modal-content" class="modal-content">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <input name="waybill_id" id="waybill_id" type="hidden"/>

                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label>Way Bill :</label>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label id="waybilllabel"></label>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                </div>
                <label class="control-label col-md-12 col-sm-12 col-xs-12">Pin <span class="required">*</span></label>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input id="pin" name="pin" class="form-control" type="text"/>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p>
                        <button id="markAsDeliveredButton" class="btn btn-success">Mark As Delivered</button>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>


        $(document).ready(function () {

            $("#submitForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            /*if (/[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('JC' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }
                            else if (/C[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('J' + ids[i]);
                                correctedIDs.push(ids[i]);
                            } */

                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /*if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }*/


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to assign selcted items to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/setplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (responsef) {
                                                responseObjf = JSON.parse(responsef);
                                                console.log(responseObjf);
                                                if (responseObjf.count) {
                                                    bootbox.alert(JSON.parse(responsef).count + ' items have been updated.');
                                                }

                                                if (responseObjf.items) {
                                                    $('#datatable td').parent().remove();
                                                    var items = responseObjf.items;
                                                    $.each(items, function (i, item) {
                                                        $("#datatable tbody").append(
                                                            "<tr id='" + item.jollychic + "'>"
                                                            + "<td>" + item.jollychic + "</td>"
                                                            + "<td>" + item.receiver_name + "</td>"
                                                            + "<td><button onclick='onClickIgnore(\"" + item.jollychic + "\")'>Ignore</button></td>"
                                                            + "<td><button onclick='onClickMarkAsDelivered(\"" + item.jollychic + "\")'>Refresh(" + item.pincode + ")</button></td>"
                                                            + "</tr>")
                                                    });
                                                    //location.reload();
                                                }
                                                else if (responseObjf.error) {
                                                    bootbox.alert(JSON.parse(responsef).error)
                                                }
                                                else {
                                                    bootbox.alert('Something went wrong!');
                                                    //location.reload();
                                                }
                                            });

                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );


            /////// Un Assign Captain Code ////////


            $("#unAssignForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            /*if (/[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('JC' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }
                            else if (/C[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('J' + ids[i]);
                                correctedIDs.push(ids[i]);
                            } */

                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /*if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }*/


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to un assign captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/unsetplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (responsef) {
                                                responseObjf = JSON.parse(responsef);
                                                console.log(responseObjf);
                                                bootbox.alert(JSON.parse(responsef).count + ' items have been updated.');
                                                if (responseObjf.items) {
                                                    $('#datatable td').parent().remove();
                                                    var items = responseObjf.items;
                                                    $.each(JSON.parse(items), function (i, item) {
                                                        $("#datatable tbody").append(
                                                            "<tr id='" + item.jollychic + "'>"
                                                            + "<td>" + item.jollychic + "</td>"
                                                            + "<td>" + item.receiver_name + "</td>"
                                                            + "<td><button onclick='onClickIgnore(\"" + item.jollychic + "\")'>Ignore</button></td>"
                                                            + "<td><button onclick='onClickMarkAsDelivered(\"" + item.jollychic + "\")'>Refresh(" + item.pincode + ")</button></td>"
                                                            + "</tr>")
                                                    });
                                                    //location.reload();
                                                }
                                                else {
                                                    bootbox.alert('Something went wrong!');
                                                    //location.reload();
                                                }
                                            });

                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );
            ///////////////////////////
            $("#datatable_plan").DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

            /////// Scan Code ///////

            $("#scanform").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to update with captain status for selcted items to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/scanplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (responsef) {
                                                responseObjf = JSON.parse(responsef);
                                                if (responseObjf.error) {
                                                    bootbox.alert(responseObjf.error);
                                                }

                                                else if (responseObjf.count) {
                                                    bootbox.alert(responseObjf.count + ' items have been updated to with captain status : '
                                                        + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city);
                                                    document.getElementById('shipmentIDs').value = '';
                                                    //location.reload();
                                                    var plannedItems = responseObjf.scannedItems;
                                                    var mydata_plan = []
                                                    var datatable_plan = $('#datatable_plan').DataTable();
                                                    datatable_plan.clear().draw();
                                                    $.each(plannedItems, function (i, item) {
                                                        /*$("#datatable_plan tbody").append(
                                                         "<tr>"
                                                         + "<td>" + item.jollychic + "</td>"
                                                         + "<td>" + item.order_number + "</td>"
                                                         + "<td>" + item.receiver_name + "</td>"
                                                         + "<td>" + item.plannedcaptain + "</td>"
                                                         + "<td>" +item.confirmedcaptain  + "</td>"
                                                         + "<td>" + item.d_quantity + "</td>"
                                                         + "<td>" + item.d_city + "</td>"
                                                         + "</tr>");*/

                                                        datatable_plan.row.add([
                                                            item.jollychic,
                                                            item.order_number,
                                                            item.receiver_name,
                                                            item.plannedcaptian,
                                                            item.confirmedcaptian,
                                                            item.d_quantity,
                                                            item.d_city]
                                                        ).draw();
                                                        //mydata_plan.push([ item.jollychic , item.order_number ,item.receiver_name, item.plannedcaptain,item.confirmedcaptain, item.d_quantity ,item.d_city]);
                                                    });
                                                    if (responseObjf.maximum_attempts_count > 0) {
                                                        bootbox.alert(responseObjf.maximum_attempts_count+' shipments '+responseObjf.shipments_exceed_max_attempts+' exceed its maximum attempts');
                                                    }
                                                    if (responseObjf.maximum_days_to_return_count > 0) {
                                                        bootbox.alert(responseObjf.maximum_days_to_return_count+' shipments '+responseObjf.shipments_exceed_max_days_to_return+' exceed its maximum days to return');
                                                    }


                                                } else if (responseObjf.maximum_days_to_return_count > 0 || responseObjf.maximum_attempts_count > 0) {
                                                    if (responseObjf.maximum_attempts_count > 0) {
                                                        bootbox.alert(responseObjf.maximum_attempts_count + ' shipments ' + responseObjf.shipments_exceed_max_attempts + ' exceed its maximum attempts');
                                                    }
                                                    if (responseObjf.maximum_days_to_return_count > 0) {
                                                        bootbox.alert(responseObjf.maximum_days_to_return_count + ' shipments ' + responseObjf.shipments_exceed_max_days_to_return + ' exceed its maximum days to return');
                                                    }
                                                } else {
                                                    bootbox.alert('Something went wrong!');
                                                    //location.reload();
                                                }
                                            });
                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );

        });

    </script>
    <script>
        // Get the modal
        var modal = document.getElementById('modal');
        var waybill_id = document.getElementById('waybill_id');
        var waybilllabel = document.getElementById('waybilllabel');
        var pin = document.getElementById('pin');

        // Get the button that opens the modal
        var markAsDeliveredButton = document.getElementById("markAsDeliveredButton");

        // When the user clicks on the button, open the modal

        function onClickMarkAsDelivered(waybill) {
            jQuery.ajax({
                type: "POST", url: "/warehouse/work/checkdeliverystatus",
                data: {
                    waybillid: waybill
                },
                success: function (json) {
                    if (json.success) {
                        deleteRow(document.getElementById(waybill));
                        modal.style.display = "none";
                    } else {
                        modal.style.display = "none";
                        alert('Order status is with captain');
                    }
                }
            });
            /*waybill_id.value = waybill;
            waybilllabel.innerHTML = waybill;
            modal.style.display = "block";*/
        }

        function onClickIgnore(waybill) {
            deleteRow(document.getElementById(waybill));
        }

        function deleteRow(r) {
            var i = r.rowIndex;
            document.getElementById("datatable").deleteRow(i);

        }

        // When the user clicks on <span> (x), close the modal
        markAsDeliveredButton.onclick = function () {
            //sending ajax request
            jQuery.ajax({
                type: "POST", url: "/warehouse/work/checkdeliverystatus",
                data: {
                    waybillid: waybill_id.value
                },
                success: function (json) {
                    if (json.success) {
                        deleteRow(document.getElementById(waybill_id.value));
                        modal.style.display = "none";
                    } else {
                        modal.style.display = "none";
                        alert('Order status is with captian.');
                    }
                }
            });
        }
    </script>
@stop

