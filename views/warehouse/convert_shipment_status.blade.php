@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table th {
            text-align: center;
        } 
        table td {
            text-align: center;
        } 
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
    </style>

    <div class="right_col" role="main">

        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <textarea id="shipmentIDs" name="ids" required="required" class="form-control" type="text" rows="10"></textarea>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <br/>       
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p> <button id="submitForm" class="btn btn-success"> Submit </button> </p>
                </div>
            </div>
        </div>

   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>
        $("#submitForm").click(function () {
                var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

                if (shipIDs == '') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');

                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i < ids.length; i++) {
                    if (!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    /*

                        /^[a-z0-9]+$/i

                        ^         Start of string
                        [a-z0-9]  a or b or c or ... z or 0 or 1 or ... 9
                        +         one or more times (change to * to allow empty string)
                        $         end of string
                        /i        case-insensitive

                    */
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function (i, el) {
                    if ($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });

                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }

                if (finalArray.length) {
                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                        for (var i = 0; i < finalArray.length; i++) {
                            $.ajax({
                                type: 'POST',
                                url: '/warehouse/convertshipmentstatuspost',
                                dataType: 'text',
                                data: {
                                    id: finalArray[i],
                                }
                            }).done(function (response) {
                                if (response) {
                                    responseObj = JSON.parse(response);
                                    if(responseObj.success) {
                                        ++count;
                                    }
                                }
                                else {
                                    alert('Something went wrong!');
                                }
                            });
                        }
                    }
                    else {
                        alert('You cancled!');
                    }

                }
                else {
                    alert('Nothing to signin!');
                }

            }
        );
    </script>

@stop
