<style>
    table {

        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #fbfbfb;
    }
</style>

<table cellpadding="7" cellspacing="0" style="width:650px;border: 1px solid black;">
    <tr>

        <td width="100" style="border: 1px solid black; text-align:center;"><span style="text-align: center"><img
                        src="/companiesdashboard/images/logo-d.png"></span></td>

        <td width="100" height="120" colspan="2" style="border: 1px solid black; text-align:center;"><span
                    style="text-align: center"><?php echo $barcode ?></span><br>

            <p style="padding-left:3px;text-align: center"><?php echo $jollychic ?></p>
            <p>Note: bold shipments are important to look after</p></td>
    </tr>


    <tr>
        <td colspan="3" style="border: 1px solid black;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="13%" align="center" bgcolor="#FFFFFF">
                        <strong>Date</strong><br>
                    <?php echo $date ?></td>

                    <td width="13%" align="center" bgcolor="#FFFFFF"><strong>Destination</strong>
                        <h4 style="font-size:20px; font-weight:bold; margin: 0px"><b><?php echo $destination ?></b></h4>
                    </td>

                    <td width="22%"><b style="font-size:25px;">Total COD<br>
                            <?php echo $cod ?> SAR</b></td>

                    <td width="13%" align="center" bgcolor="#FFFFFF"><strong>Shipments
                            #</strong><br>
                    <?php echo $pieces ?></td>
                    <td width="13%" align="center" bgcolor="#FFFFFF"><strong>Boxes #</strong><br>
                        <?php echo $shimpentsQuantity ?> </td>
                    <td width="13%" align="center" bgcolor="#FFFFFF"><strong>Estimated
                            Commission</strong><br>
                        <?php echo $estimatedDeliverfee ?> SAR</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan="3" valign="top" bgcolor="#FFFFFF" style="border: 1px solid black;"><p
                    style="margin: 5px 0 3px 5px; font-weight:bold; ">

            <table>
                <tr>
                    <th style="border: 1px solid #000000;">Waybill</th>
                    <th style="border: 1px solid #000000;">Receiver</th>
                    <th style="border: 1px solid #000000;">Mobile 1</th>
                    <th style="border: 1px solid #000000;">Mobile 2</th>
                    <th style="border: 1px solid #000000;">District</th>
                    <th style="border: 1px solid #000000;">COD</th>
                    <th style="border: 1px solid #000000;">Q</th>
                </tr>

                <?php
                foreach ($items as $item) {
                $mobile_1_last_nine = substr($item['receiver_phone'], -9);
                $mobile_2_last_nine = substr($item['receiver_phone2'], -9);
                $prefix = 0;

                //mobile phone $mobile_1_last_nine
                if ($mobile_1_last_nine != '') {
                    $receiver_mobile_1 = $prefix . $mobile_1_last_nine;
                } else {
                    $receiver_mobile_1 = '';
                }
                //mobile phone 2
                if ($mobile_2_last_nine != '') {
                    $receiver_mobile_2 = $prefix . $mobile_2_last_nine;
                } else {
                    $receiver_mobile_2 = '';
                }
                //get first part of name
                $nameParts = explode(' ', $item['receiver_name']);
                $firstName = $nameParts[0];
                if(array_key_exists(1,$nameParts) && !empty($nameParts[1])){
                    $firstName = $nameParts[0].' '.$nameParts[1];
                }
                $undelivered_reason = $item['undelivered_reason'];
                $shipment_reschedule = $item['shipment_reschedule'];

                ?>
                <tr <?php if($undelivered_reason == 6 || $undelivered_reason == 16 || $shipment_reschedule == 'Yes'){?>style="font-weight:bold" <?php }?>>
                    <td><?= $item['jollychic'] ?></td>
                    <td><?= $firstName ?></td>
                    <td><?=$receiver_mobile_1 ?></td>
                    <td><?= $receiver_mobile_2 ?></td>
                    <td><?= $item['d_district'] ?></td>
                    <td><?= $item['cash_on_delivery'] ?></td>
                    <td><?= $item['d_quantity'] ?></td>
                </tr>
                <?php } ?>
            </table>
    </tr>
</table>
