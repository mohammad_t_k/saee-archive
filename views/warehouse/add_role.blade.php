@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<?php

$allowed_pages = AllowedPages::select(['id', 'page_description'])->get();

$query = 'SELECT a.id PageID,a.page_description PageName, GROUP_CONCAT(b.action_name ORDER BY b.id) ActionName, a.allowed_actions ActionIDs FROM allowed_pages a INNER JOIN allowed_actions b ON FIND_IN_SET(b.id, a.allowed_actions) > 0 GROUP BY a.id '; 

$allowed_actions = DB::select($query);



 ?>

<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif
        
            <div class="box-body ">
                <form action="" method="POST" id="RoleForm">
                <div class="form-group">
                    <label>Role Name</label><input id="rolename" required="" class="form-control" type="text" name="rolename"
                                              placeholder="Add Role Name">
                </div>
                <div class="form-group">
                    <label>Role Description</label>
                    <textarea id="roledesc" required="" class="form-control" name="roledesc"
                                              placeholder="Add Role Description"></textarea>
                </div>

                <div class="form-group">

                     <label>Select Pages with their actions</label>
                   <ul>

                    <?php foreach($allowed_actions as $aAction){ ?>
                        <li style="display: block; width: 100%;float: left;">
                            <input name="page" value="<?php echo 'page-'.$aAction->PageID ?>" type="checkbox"><?php echo $aAction->PageName; ?>
                        <br>
                            <ul>
                               <?php 

                               $actions = explode(",", $aAction->ActionName);
                               $ActionIDs = explode(",", $aAction->ActionIDs);
                               $i=0;
                                    foreach($actions as $act){
                                ?>
                                            <li style="display: block;width: 20%;float:left;">
                                            <input name="action" value="<?php echo $aAction->PageID.'_'.$ActionIDs[$i]; ?>" type="checkbox"><?php echo $act ?>
                                                <br>
                                            </li>

                                            <?php $i++; } ?>
                         

                        </ul>

  </li>
  <?php } ?>
</ul>
                </div> 

                


            </div>
           
                <button  type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Add Role</button>

            </form>
        

    </div>
</div>
</div>

<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>


        $(document).ready(function () {

            $(function() {
  $("li:has(li) > input[type='checkbox']").change(function() {
    $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
  });
  $("input[type='checkbox'] ~ ul input[type='checkbox']").change(function() {
    $(this).closest("li:has(li)").children("input[type='checkbox']").prop('checked', $(this).closest('ul').find("input[type='checkbox']").is(':checked'));
  });
});




            $("#RoleForm").submit(function (e) {
                

                var actionObj = {};
$('input[name="action"]:checked').each(function() {
    var parts = this.value.split('_'),
        key = parts[0],
        value= parts[1];

    actionObj[key] = actionObj[key] || [];
    actionObj[key].push(value)
});

               

                        var rolename = document.getElementById("rolename").value;
                        var roledesc = document.getElementById("roledesc").value;


                        var allowed_actions = new Array();//storing the selected values inside an array
                        var allowed_pages = new Array();

                         $('#allowed_actions :selected').each(function(i, selected) {
                                allowed_actions[i] = $(selected).val();
                            });

                          $('#allowed_pages :selected').each(function(i, selected) {
                                allowed_pages[i] = $(selected).val();
                            });

                        /* var allowed_actions = new Array();//storing the selected values inside an array

                         $('#allowed_actions :selected').each(function(j, selected) {
                                allowed_actions[j] = $(selected).val();
                            }); */

                        //var allowed_pages = allowed_pages.split(',');
                       // var allowed_actions = allowed_actions.split(',');


                        var wrongIDs = Array();
                        var correctAllowedPages = Array();
                        var correctAllowedActions = Array();
                        var correctedIDs = Array();
                        var finalArrayAllowedPages = [];
                        var finalArrayAllowedActions = [];
                        



                       /* if (correctedIDs.length) {
                            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                        } */

                        if (actionObj) {
                            if (confirm("Are you sure Actions and pages are correct ? ") == true) {
                                 e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/system/add_role_post',
                                    dataType: 'text',
                                    data: {rolename: rolename,roledesc: roledesc, acdata: actionObj, allowedactions: allowed_actions, allowedpages: allowed_pages },
                                }).done(function (response) {

                                    console.log(response);

                                    window.location='/warehouse/system/roles?success=1';
                                    
                                }); // Ajax Call

                            }
                            else {
                                bootbox.alert('You cancled!');
                                return false;
                            }

                        }
                        else {
                            bootbox.alert('Nothing to signin!');
                            return false;
                        }


                    }
            );
        });

    </script>

@stop