@extends('warehouse.layout')
@section('content')
@if(in_array(Session::get('admin_id'), array(34, 32, 51)))
    <?php $allowed = 1; ?>
@else
    <?php $allowed = 0; ?>
@endif

    <link rel="stylesheet" type="text/css" href="/styles/spooky-goast.css">
    <link rel="stylesheet" type="text/css" href="/styles/spinner.css">

    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left" style="width: 100%;">
                <h3 class="text-center"> Hi, I am Lord Voldemort</h3>
                <div id="container" style="margin-left: auto; margin-right: auto;">
                    <div id="spooky">
                        <div id="body">
                            <div id="eyes"></div>
                            <div id="mouth"></div>
                            <div id="feet">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div id="shadow"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        @if(!$allowed)
            <h3 class="text-center">You are not authorized to use my magic :)</h3>
        @endif
        <div class="row">
            @if($allowed)
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <br>
                            <div style="float: left; margin-right: 20px;">
                                <h2>What do you want to use magic for? </h2>
                            </div>
                            <div style="float: left;" class="col-md-3">
                                <select name="task" id="task" class="form-control">
                                    <option value="empty"></option>
                                    @if($allowed)
                                        <option value="change_city">I want to change city of shipments</option>
                                        <option value="change_hub">I want to change hub of shipments</option>
                                        <option value="change_district">I want to change district of shipments</option>
                                        <option value="city_map">I want to add a new map for city</option>
                                        <option value="status_rollback">I want to change status of shipments</option>
                                        <option value="schedule_shipments">I want to schedule shipments</option>
                                        <option value="change_cod">I want to change COD of shipments</option>
                                        <option value="close_tickets">I want to close bulk tickets</option>
                                        <option value="fix_jollychic_waybills">I want to fix waybills of JollyChic shipments</option>
                                        <!-- <option value="911_to_946">I want to change JollyChic to JollyChic Express</option> -->
                                    @endif
                                </select>
                            </div>          
                            <div class="clearfix"></div>
                            <br>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div id="div_row" class="row" style="display: none;">
            <div style="display: {{ $allowed ? '' : 'none' }};">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <div class="row">
                            <div id="shipments_ids" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20" ></textarea>
                                    </div>
                                    <!-- <div class="col-md-7 col-sm-3 col-xs-12">
                                        <table>
                                            <td style="border-left: 1px solid grey; padding-left: 15px;">
                                                <h3><span style="color: red; font-weight: bold;"> Conditions: </span></h3><br/>
                                                <h3><span><span style="font-weight: bold;">1)</span> You have to use Saee's waybill (Starting with OS or JC).</span></h3><br/>
                                                <h3><span><span style="font-weight: bold;">2)</span> You can change cities for shipments only in the following statuses:</span></h3><br/>
                                                <h3><span style="margin-left: 30px;">- In Sorting Area.</span></h3><br/>
                                                <h3><span style="margin-left: 30px;">- Picked-Up.</span></h3><br/>
                                                <h3><span style="margin-left: 30px;">- In Warehouse.</span></h3><br/>
                                            </td>
                                        </table>
                                    </div> -->
                                </div>
                            </div>
                            <div id="div_citymap" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Wrong Spelling of City <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input type="text" name="citymap" id="citymap" class="form-control" requried />
                                    </div>
                                </div>
                            </div>
                            <div id="div_city" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">New City <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="new_city" id="new_city" class="form-control">
                                            <option value="empty" selected></option>
                                            @foreach($subcities as $subcity) 
                                                <option value="{{ $subcity->city }}+{{ $subcity->subcity }}">{{ $subcity->city }} | {{ $subcity->subcity }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-7 col-xs-12">
                                        <span id="new_city_note"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="div_hub" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">New Hub <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="new_hub" id="new_hub" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_district" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">New District <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="new_district" id="new_district" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_statuses" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Statuses <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="statuses[]" id="statuses" class="form-control" style="height: 180px;" multiple>
                                            @foreach($order_statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->english }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_old_status" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Old Status <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="old_status" id="old_status" class="form-control">
                                            @foreach($order_statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->english }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_new_status" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">New Status <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="new_status" id="new_status" class="form-control">
                                            @foreach($order_statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->english }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_undelivered_reasons" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Undelivered Reason <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="undelivered_reason" id="undelivered_reason" class="form-control">
                                            @foreach($undelivered_reasons as $undelivered_reason)
                                                <option value="{{ $undelivered_reason->id }}">{{ $undelivered_reason->id == '0' ? '' : $undelivered_reason->english }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="div_date" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Date <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input type="date" name="date" id="date" class="form-control" requried />
                                    </div>
                                </div>
                            </div>
                            <div id="div_text" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Text <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input type="text" name="text" id="text" class="form-control" requried />
                                    </div>
                                    <div class="col-md-7 col-xs-12">
                                        <span id="text_note"></span>
                                    </div>
                                </div>
                            </div>
                            <div id="div_number" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Number <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input type="number" name="number" id="number" class="form-control" requried />
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <span id="div_span_number"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="submit" style="display: none;">
                                <br/>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <p><button id="abracadabra" class="btn btn-success">Abracadabra <i class="fa fa-magic"></i></button></p>
                                </div>
                            </div>
                            <!-- spinner -->
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner" style="display: none;">
                                <br/>
                                <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-3">
                                    <div class="spinner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="unknown_failure_section" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                <h4 class="text-center"><span id="unknown_failure_msg"></span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="general_success" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                <h4 class="text-center"><span id="general_success_msg"></span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="failed_items_section" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li class="dropdown"></li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="datatable_faileditems" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Reason</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Reason</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="success_items_section" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li class="dropdown"></li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="datatable_successitems" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Company</th>
                                            <th>Order Number</th>
                                            <th>Order Reference</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>District</th>
                                            <th>Receiver Name</th>
                                            <th>Receiver Mobile</th>
                                            <th>Receiver Mobile 2</th>
                                            <th>Cash On Delivery</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>Company</th>
                                            <th>Order Number</th>
                                            <th>Order Reference</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>District</th>
                                            <th>Receiver Name</th>
                                            <th>Receiver Mobile</th>
                                            <th>Receiver Mobile 2</th>
                                            <th>Cash On Delivery</th>
                                            <th>Status</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>

        // how to add new task: 
        // 1- add the option in #task select
        // 2- add the necessary input div
        // 3.1- add the action on change event of #task select change to show the input
        // 3.2- set ids_flag to 1 in the 'if' statement if you want to show the ids box
        // 4- define variables to get your input on #abracadabra click
        // 5- add the conditions to validate the input 
        // 6- define data object

        // set it to 1 if you want to use ids box
        let ids_flag = 0;
        
        $("#task").change(function(e) {
            ids_flag = 0; 
            $("#task option[value='empty']").remove();
            $("#div_row").css('display', 'none'); // the container panel
            $("#shipments_ids").css('display', 'none'); // ids box
            $("#div_city").css('display', 'none'); // new city
            $("#div_hub").css('display', 'none'); // new hub
            $("#div_district").css('display', 'none'); // new district
            $("#div_citymap").css('display', 'none'); // new city map record
            $("#div_date").css('display', 'none'); // date
            $("#div_old_status").css('display', 'none'); // old status
            $("#div_new_status").css('display', 'none'); // new status 
            $("#div_undelivered_reasons").css('display', 'none'); // undelivered reasons
            $("#div_number").css('display', 'none'); // number
            $("#div_text").css('display', 'none'); // text 
            $("#new_city_note").text("");
            $("#text_note").text("");
            $("#submit").css('display', 'none');
            var value = this.value;
            if(value == 'change_city') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_city").css('display', '');
                $("#div_hub").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'change_hub') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_city").css('display', '');
                $("#div_hub").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'change_district') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_city").css('display', '');
                $("#div_district").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'city_map') {
                $("#div_row").css('display', '');
                $("#div_city").css('display', '');
                $("#div_citymap").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == '911_to_946') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'status_rollback') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_old_status").css('display', '');
                $("#div_new_status").css('display', '');
                $("#div_undelivered_reasons").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'schedule_shipments') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_date").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'change_cod') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_number").css('display', '');
                $("#submit").css('display', '');
                $("#div_span_number").html('This number will be rounded to two digits after the decimal point');
                $("#div_span_number").css('color', 'red');
                $("#div_span_number").css('font-weight', 'bold');
            }
            else if(value == 'close_tickets') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#submit").css('display', '');
            }
            else if(value == 'fix_jollychic_waybills') {
                ids_flag = 1;
                $("#div_row").css('display', '');
                $("#shipments_ids").css('display', '');
                $("#div_text").css('display', '');
                $("#text_note").text("JollyChic waybills only");
                $("#text_note").css('font-weight', 'bold');
                $("#text_note").css('color', 'red');
                $("#submit").css('display', '');
            }
        });

        $("#new_city").change(function(e) {
            $("#new_city option[value='empty']").remove();
            $("#new_hub").empty();
            $("#new_district").empty();
            var value = this.value;
            var city = value.split('+')[0];
            var subcity = value.split('+')[1];
            if(value != 'empty') {
                @foreach($hubs as $hub)
                    if(city == '{{ $hub->city }}') {
                        $("#new_hub").append($('<option>', {
                            value: "{{ $hub->id }}",
                            text: "{{ $hub->name }}"
                        }));
                    }
                @endforeach
                @foreach($districts as $district) 
                    if(subcity == "{{ $district->subcity }}") {
                        $("#new_district").append($('<option>', {
                            value: "{{ $district->district }}",
                            text: "{{ $district->district }}"
                        }));
                    }
                @endforeach 
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#abracadabra").click(function() {

                var task = $("#task").val();
                var citymap = $("#citymap").val();
                var new_city = $("#new_city").val().split("+")[1];
                var new_main_city = $("#new_city").val().split("+")[0];
                var new_hub = $("#new_hub").val();
                var new_district = $("#new_district").val();
                var statuses = $("#statuses").val();
                var old_status = $("#old_status").val();
                var new_status = $("#new_status").val();
                var undelivered_reason = $("#undelivered_reason").val();
                var date = $("#date").val();
                var number = $("#number").val();
                var text = $("#text").val();
                var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

                if(ids_flag) {

                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }
                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }
                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i <ids.length; i++ ) {
                        if(!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) /* || /^[a-z0-9]+$/i.test(ids[i]) */) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            wrongIDs.push(ids[i]);
                        }
                    }
                    $.each(correctIDs, function(i, el)
                    {
                        if($.inArray(el, finalArray) === -1) 
                        {
                            finalArray.push(el);
                        }
                    });
                    if (wrongIDs.length) {
                        alert("These items are filtered out:\n" + wrongIDs.toString());
                        if(!finalArray.length) {
                            alert("Nothing to submit!");
                            return;
                        }
                    }
                }

                var data = {};
                if(task == 'change_city') {
                    if($("#new_city").val() == 'empty' || !new_hub) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to choose city and hub");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","), 
                        new_city: new_city, 
                        new_main_city: new_main_city,
                        new_hub: new_hub
                    };
                    if(!confirm("This will not update shipments that have their money received! do you want to continue?"))
                        return;
                }
                else if(task == 'change_hub') {
                    if(!new_hub) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to choose hub");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","),
                        new_hub: new_hub
                    };
                    if(!confirm("This action will not update shipments that have their money received! do you want to continue?"))
                        return;
                }
                else if(task == 'change_district') {
                    if(!new_district) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to choose district");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","),
                        new_city: new_city,
                        new_district: new_district
                    };
                    if(!confirm("This action will not update shipments that have their money received! do you want to continue?"))
                        return;
                }
                else if(task == 'city_map') {
                    if(!citymap || citymap == '') {
                        bootbox.alert("<i class='fa fa-magic'></i> You can not map empty string");
                        return;
                    }
                    if(!new_city) {
                        bootbox.alert("<i class='fa fa-magic'></i> You can not map to an empty string");
                        return;
                    }
                    if(!confirm("This action in the future will update every '" + citymap + "' city in the system to be '" + new_city + "'. If you sure of what you are doing, click OK."))
                        return;
                    data = {
                        task: task, 
                        citymap: citymap,
                        new_city: new_city,
                        new_district: new_district
                    };
                }
                else if(task == '911_to_946') {
                    data = {
                        task: task, 
                        ids:finalArray.join(",")
                    };
                    if(!confirm("This action will update ONLY JollyChic records that did NOT have thier money received! do you want to continue?"))
                        return;
                }
                else if(task == 'status_rollback') {
                    data = {
                        task: task, 
                        ids: finalArray.join(","), 
                        old_status: old_status, 
                        new_status: new_status,
                        undelivered_reason: undelivered_reason,
                    };
                    if(!confirm("This action will update neither JollyChic, JollyChic Express, MarkaVIP and Dealy nor records that did have their money received! do you want to continue?"))
                        return;
                }
                else if(task == 'schedule_shipments') {
                    if(!date) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to choose date");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","), 
                        date: date, 
                    };
                }
                else if(task == 'change_cod') {
                    if(!number) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to choose date");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","), 
                        number: number, 
                    };
                }
                else if(task == 'close_tickets') {
                    data = {
                        task: task, 
                        ids: finalArray.join(",")
                    };
                }
                else if(task == 'fix_jollychic_waybills') {
                    if(!text) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to enter new waybill");
                        return;
                    }
                    if(ids.length != 1) {
                        bootbox.alert("<i class='fa fa-magic'></i> You have to insert exactly one waybill in the Shipment IDs box");
                        return;
                    }
                    if(/JC[0-9]{8}KS/.test(ids[0])) {
                        bootbox.alert("<i class='fa fa-magic'></i> You can not change JollyChic waybills");
                        return;
                    }
                    if(/JC[0-9]{8}KS/.test(text) == false) {
                        bootbox.alert("<i class='fa fa-magic'></i> This new waybill is not a correct JollyChic waybill");
                        return;
                    }
                    data = {
                        task: task, 
                        ids: finalArray.join(","),
                        text: text,
                    };
                }

                if (ids_flag == 0 || confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {

                    $("#success_items_section").css("display", "none");
                    $("#failed_items_section").css("display", "none");
                    $("#unknown_failure_section").css("display", "none");
                    $("#general_success").css("display", "none");
                    $("#submit").css('display', 'none');
                    $("#div_spinner").css('display', '');
                    
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/magic_force/post',
                        dataType: 'text',
                        data: data
                    }).done(function (response) {

                        console.log(response);
                        $("#submit").css('display', '');
                        $("#div_spinner").css('display', 'none');
                        responseObj = JSON.parse(response);
                        if(responseObj.error_message)
                            alert(responseObj.error_message);
                        responseObj = JSON.parse(response);
                        if (responseObj.success) {
                            if(responseObj.general_success_msg) {
                                $("#general_success_msg").text(responseObj.message);
                                $("#general_success").css('display', '');
                            }
                            else {
                                alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                                $("#datatable_successitems").DataTable().destroy();
                                if(responseObj.success_items.length) {
                                    let rows = [];
                                    responseObj.success_items.forEach(function(element) {
                                        if(!element.order_reference)
                                            element.order_reference = ''
                                        if(!element.d_city)
                                            element.d_city = '';
                                        if(!element.receiver_name)
                                            element.receiver_name = '';
                                        if(!element.receiver_phone)
                                            element.receiver_phone = '';
                                        if(!element.receiver_phone2)
                                            element.receiver_phone2 == '';
                                        if(element.hub_name == 'All')
                                            element.hub_name = '';
                                        rows.push([
                                            element.jollychic,
                                            element.company_name,
                                            element.order_number,
                                            element.order_reference,
                                            element.d_city,
                                            element.hub_name,
                                            element.d_district,
                                            element.receiver_name,
                                            element.receiver_phone,
                                            element.receiver_phone2,
                                            element.cash_on_delivery,
                                            element.status_name
                                        ]);
                                    });
                                    $("#datatable_successitems").DataTable({
                                        "data": rows,
                                        "autoWidth": false,
                                        dom: "Blfrtip",
                                        buttons: [
                                            {
                                                extend: "copy",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "csv",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "excel",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "pdfHtml5",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "print",
                                                className: "btn-sm"
                                            },
                                        ],
                                        responsive: true,
                                        'order': [[0, 'asc']]
                                    });
                                    $("#success_items_section").css("display", "");
                                }
                                if(responseObj.failed_items.length) {
                                    $("#datatable_faileditems").DataTable().destroy();
                                    let rows = [];
                                    responseObj.failed_items.forEach(function(element) {
                                        rows.push([element.waybill, element.error]);
                                    });
                                    $("#datatable_faileditems").DataTable({
                                        "data": rows,
                                        "autoWidth": false,
                                        dom: "Blfrtip",
                                        buttons: [
                                            {
                                                extend: "copy",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "csv",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "excel",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "pdfHtml5",
                                                className: "btn-sm"
                                            },
                                            {
                                                extend: "print",
                                                className: "btn-sm"
                                            },
                                        ],
                                        responsive: true,
                                        'order': [[0, 'asc']]
                                    });
                                    $("#failed_items_section").css("display", "");
                                }
                            }
                        }
                        else if(responseObj.error_code && responseObj.error_code == 2) {
                            $("#unknown_failure_section").css('display', '');
                            $("#unknown_failure_msg").html(responseObj.error);
                        }
                        else
                            alert('Something went wrong!');
                    });
                }
                else {
                    alert('You cancled!');
                }
            });
        });
    </script>

    <script>
        function onChangeCompanyCity() {
            $('#newshipmentscount tr').has('td').remove();
            $('#signedInshipments tr').has('td').remove();
            var city = document.getElementById('city').value;
            var company = document.getElementById('company').value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/getnewshipmentscountbydate',
                dataType: 'text',
                data: {city: city, company_id: company,},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                var newShipmentCountByDate = responseObj.newShipmentCountByDate;
                var table = document.getElementById("newshipmentscount");
                for (var i = 0; i < newShipmentCountByDate.length; i += 1) {
                    var row = table.insertRow(1);
                    row.id=newShipmentCountByDate[i].new_shipment_date;
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    cell1.innerHTML = newShipmentCountByDate[i].new_shipment_date;
                    cell2.innerHTML = '0/'+newShipmentCountByDate[i].count;
                }
            });
        }
    </script>
@stop

