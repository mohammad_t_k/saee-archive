@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <style>
        /* The Modal (background) */
        #modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 400px; /* Could be more or less, depending on screen size */
            height: 250px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .vertical-line {
            border-left: 1px solid black;
            height: 200px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/styles/spinner.css">
    
    <div class="right_col" role="main">
        <div class="container">
        
            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="tab_container">

                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class='label2'><i class="fa fa-indent"></i><span> Check in</span></label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2" class='label2'><i class="fa fa-outdent"></i> Check out</span></label>

                <section id="content1" class="tab-content" style="color: #7E90A4;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Sign-in shipments to returns warehouse</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Captain ID</label>
                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <input type="text" name="captainid" id="captainid" class="form-control">
                                            </div>
                                            <div class="col-md-1 col-sm-12 col-xs-12 form-group" id="div_spinner3" style="display: none;">
                                                <br/>
                                                <div class="col-md-1 col-sm-6 col-xs-12">
                                                    <div class="spinner"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-1" id="div_captain_name">
                                                <span id="captain_name"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>

                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <button id="captain_reset" class="btn btn-success">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>

                                    <div class="col-md-12 col-sm-12 col-xs-12" id="div_summary" style="display: none;">
                                        <div class="x_panel">
                                            <div class="row tile_count">
                                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-cubes"></i> Total Shipments With Captain</span>
                                                    <div class="count" id="captain_total"></div>
                                                </div>
                                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-cubes"></i> Total OFD Shipments</span>
                                                    <div class="count" id="captain_OFD"></div>
                                                </div>
                                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-cubes"></i> Total UnDelivered Shipments</span>
                                                    <div class="count" id="captain_undelivered"></div>
                                                </div>
                                                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-cubes"></i> Total Delivered Shipments</span>
                                                    <div class="count" id="captain_delivered"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="row" id="div_shipments" style="display: none;">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                        <div class="col-md-10 col-sm-6 col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="col-md-2">
                                                        <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="10" onkeypress="work()"></textarea>
                                                    </div>    
                                                    <div class="col-md-10">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div id="correct_ids" style="font-size: 10pt; text-align: center;"></div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="vertical-line" style="text-align: center;"></div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div id="wrong_ids" style="font-size: 10pt; text-align: center;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                                <p>
                                                    <button id="submitForm" class="btn btn-success">Return</button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner" style="display: none;">
                                            <br/>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-2">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="unknown_failure_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="unknown_failure_msg"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="general_success" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="general_success_msg"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="failed_items_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_faileditems" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="success_items_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_successitems" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Pieces</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Pieces</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content2" class="tab-content" style="color: #7E90A4;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Sign-in shipments to dispatching warehouse</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Scheduled Shipment Date</label>
                                            <div class="col-md-10 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-md-2">
                                                            <input type="date" name="scheduled_shipment_date" id="scheduled_shipment_date" class="form-control">
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span>
                                            </label>
                                            <div class="col-md-10 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-md-2">
                                                            <textarea id="shipmentIDs2" required="required" class="form-control" type="text" rows="10"></textarea>
                                                        </div>    
                                                        <div class="col-md-10" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div id="correct_ids" style="font-size: 10pt; text-align: center;">
                                                                        <span style='font-size: 25px; color: green;'>Accepted Items</span><br/><br/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <div class="vertical-line" style="text-align: center;"></div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div id="wrong_ids" style="font-size: 10pt; text-align: center;">
                                                                        <span style='font-size: 25px; color: red;'>Unaccepted Items</span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                                <p>
                                                    <button id="submitForm2" class="btn btn-success">Submit</button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner2" style="display: none;">
                                            <br/>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-2">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="unknown_failure_section2" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="unknown_failure_msg2"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="general_success2" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="general_success_msg2"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="failed_items_section2" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_faileditems2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="success_items_section2" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_successitems2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Pieces</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Pieces</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#captain_reset").click(function() {
            $("#div_captain_summary").css('display', 'none');
            $("#div_captain_name").css('display', 'none');
            $("#div_summary").css('display', 'none');
            $("#div_shipments").css('display', 'none');
            $("#success_items_section").css("display", "none");
            $("#failed_items_section").css("display", "none");
            $("#unknown_failure_section").css("display", "none");
            $("#general_success").css("display", "none");
            $("#captainid").val("");
            $("#shipmentIDs").val("");
            document.getElementById('correct_ids').innerHTML = "";
            document.getElementById('wrong_ids').innerHTML = "";
        });
    </script>

    <script>
        document.getElementById('scheduled_shipment_date').valueAsDate = new Date();
    </script>    

    <script>
        var captainid = 0;
        $("#captainid").keypress(function(e) {
            if(e.which == 13) {
                $("#div_spinner3").css('display', '');
                $("#unknown_failure_section").css('display', 'none');
                $("#div_captain_name").css('display', 'none');
                $("#div_summary").css('display', 'none');
                $("#div_shipments").css('display', 'none');
                $("#success_items_section").css("display", "none");
                $("#failed_items_section").css("display", "none");
                $("#unknown_failure_section").css("display", "none");
                $("#general_success").css("display", "none");
                captainid = $("#captainid").val();
                console.log(captainid);
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/work/returns/captain',
                    dataType: 'text', 
                    data: {captainid: captainid}
                }).done(function(response){
                    console.log(response);
                    responseObj = JSON.parse(response);
                    $("#div_spinner3").css('display', 'none');
                    if(responseObj.error_code) {
                        $("#unknown_failure_section").css('display', '');
                        $("#unknown_failure_msg").html(responseObj.error);
                    }
                    else {
                        $("#div_captain_summary").css('display', '');
                        $("#div_captain_name").css('display', '');
                        $("#div_summary").css('display', '');
                        $("#div_shipments").css('display', '');
                        $("#captain_name").html(responseObj.captain.first_name + ' ' + responseObj.captain.last_name);
                        $("#captain_total").html(responseObj.total);
                        $("#captain_OFD").html(responseObj.OFD);
                        $("#captain_delivered").html(responseObj.delivered);
                        $("#captain_undelivered").html(responseObj.undelivered);
                    }
                });
            }
        });
    </script>

    <script>


        $(document).ready(function () {
            $("#submitForm").click(function () {
                var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                
                if (shipIDs == '') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i){
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }

                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i < ids.length; i++) {
                    if (!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) ) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        /*if (/[0-9]{8}KS/.test(ids[i])) {
                            correctIDs.push('JC' + ids[i]);
                            correctedIDs.push(ids[i]);
                        }
                        else if (/C[0-9]{8}KS/.test(ids[i])) {
                            correctIDs.push('J' + ids[i]);
                            correctedIDs.push(ids[i]);
                        } */

                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function (i, el) {
                    if ($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });


                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }

                /*if (correctedIDs.length) {
                    bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                }*/

                if (finalArray.length) {
                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                        $("#success_items_section").css("display", "none");
                        $("#failed_items_section").css("display", "none");
                        $("#unknown_failure_section").css("display", "none");
                        $("#general_success").css("display", "none");
                        $("#submitForm").css('display', 'none');
                        $("#div_spinner").css('display', '');
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/updatestatus',
                            dataType: 'text',
                            data: {to: 3, ids: finalArray.join(','), captainid: captainid, request_source: 'returns_check-in'}
                        }).done(function(response){
                            console.log(response);
                            $("#submitForm").css('display', '');
                            $("#div_spinner").css('display', 'none');
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                $("#shipmentIDs").val("");
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/work/returns/captain',
                                    dataType: 'text', 
                                    data: {captainid: captainid}
                                }).done(function(response){
                                    console.log('here');
                                    console.log(response);
                                    responseObj = JSON.parse(response);
                                    $("#captain_total").html(responseObj.total);
                                    $("#captain_OFD").html(responseObj.OFD);
                                    $("#captain_delivered").html(responseObj.delivered);
                                    $("#captain_undelivered").html(responseObj.undelivered);
                                });
                                if(responseObj.general_success_msg) {
                                    $("#general_success_msg").text(responseObj.message);
                                    $("#general_success").css('display', '');
                                }
                                else {
                                    alert(responseObj.success_items.length + ' items have been updated');
                                    $("#datatable_successitems").DataTable().destroy();
                                    if(responseObj.success_items.length) {
                                        let rows = [];
                                        responseObj.success_items.forEach(function(element) {
                                            if(!element.order_reference)
                                                element.order_reference = ''
                                            if(!element.d_city)
                                                element.d_city = '';
                                            if(!element.receiver_name)
                                                element.receiver_name = '';
                                            if(!element.receiver_phone)
                                                element.receiver_phone = '';
                                            if(!element.receiver_phone2)
                                                element.receiver_phone2 == '';
                                            if(element.hub_name == 'All')
                                                element.hub_name = '';
                                            rows.push([
                                                element.jollychic,
                                                element.company_name,
                                                element.order_number,
                                                element.order_reference,
                                                element.d_city,
                                                element.hub_name,
                                                element.d_district,
                                                element.receiver_name,
                                                element.receiver_phone,
                                                element.receiver_phone2,
                                                element.cash_on_delivery,
                                                element.d_quantity,
                                                element.status_name
                                            ]);
                                        });
                                        $("#datatable_successitems").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#success_items_section").css("display", "");
                                    }
                                    if(responseObj.failed_items.length) {
                                        $("#datatable_faileditems").DataTable().destroy();
                                        let rows = [];
                                        responseObj.failed_items.forEach(function(element) {
                                            rows.push([element.waybill, element.error]);
                                        });
                                        $("#datatable_faileditems").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#failed_items_section").css("display", "");
                                    }
                                }
                            }
                            else if(responseObj.error_code && responseObj.error_code == 2) {
                                $("#unknown_failure_section").css('display', '');
                                $("#unknown_failure_msg").html(responseObj.error);
                            }
                            else
                                alert('Something went wrong!');
                        });
                        // window.location = "/warehouse/work/returns?ids=" + finalArray.join(",");
                    }

                }
                else {
                    bootbox.alert('Nothing to return!');
                    location.reload();
                }

            });

            $("#submitForm2").click(function () {
                var shipIDs = document.getElementById("shipmentIDs2").value.replace(/\s/g, ",");
                var scheduled_shipment_date = document.getElementById('scheduled_shipment_date').value;

                if (shipIDs == '') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i){
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }

                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i < ids.length; i++) {
                    if (!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) ) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        /*if (/[0-9]{8}KS/.test(ids[i])) {
                            correctIDs.push('JC' + ids[i]);
                            correctedIDs.push(ids[i]);
                        }
                        else if (/C[0-9]{8}KS/.test(ids[i])) {
                            correctIDs.push('J' + ids[i]);
                            correctedIDs.push(ids[i]);
                        } */

                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function (i, el) {
                    if ($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });

                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }

                /*if (correctedIDs.length) {
                    bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                }*/

                if (finalArray.length) {
                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                        $("#success_items_section2").css("display", "none");
                        $("#failed_items_section2").css("display", "none");
                        $("#unknown_failure_section2").css("display", "none");
                        $("#general_success2").css("display", "none");
                        $("#submitForm2").css('display', 'none');
                        $("#div_spinner2").css('display', '');
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/updatesubstatus',
                            dataType: 'text',
                            data: {
                                current_status: 3, 
                                current_sub_status: 1, 
                                to_sub_status: 2, 
                                ids: finalArray.join(','), 
                                scheduled_shipment_date: scheduled_shipment_date, 
                                request_source: 'returns_check-out'
                            }
                        }).done(function(response){
                            console.log('check-out');
                            console.log(response);
                            $("#submitForm2").css('display', '');
                            $("#div_spinner2").css('display', 'none');
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                $("#shipmentIDs2").val("");
                                if(responseObj.general_success_msg) {
                                    $("#general_success_msg2").text(responseObj.message);
                                    $("#general_success2").css('display', '');
                                }
                                else {
                                    alert(responseObj.success_items.length + ' items have been updated');
                                    $("#datatable_successitems2").DataTable().destroy();
                                    if(responseObj.success_items.length) {
                                        let rows = [];
                                        responseObj.success_items.forEach(function(element) {
                                            if(!element.order_reference)
                                                element.order_reference = ''
                                            if(!element.d_city)
                                                element.d_city = '';
                                            if(!element.receiver_name)
                                                element.receiver_name = '';
                                            if(!element.receiver_phone)
                                                element.receiver_phone = '';
                                            if(!element.receiver_phone2)
                                                element.receiver_phone2 == '';
                                            if(element.hub_name == 'All')
                                                element.hub_name = '';
                                            rows.push([
                                                element.jollychic,
                                                element.company_name,
                                                element.order_number,
                                                element.order_reference,
                                                element.scheduled_shipment_date,
                                                element.d_city,
                                                element.hub_name,
                                                element.d_district,
                                                element.receiver_name,
                                                element.receiver_phone,
                                                element.receiver_phone2,
                                                element.cash_on_delivery,
                                                element.d_quantity,
                                                element.status_name
                                            ]);
                                        });
                                        $("#datatable_successitems2").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#success_items_section2").css("display", "");
                                    }
                                    if(responseObj.failed_items.length) {
                                        $("#datatable_faileditems2").DataTable().destroy();
                                        let rows = [];
                                        responseObj.failed_items.forEach(function(element) {
                                            rows.push([element.waybill, element.error]);
                                        });
                                        $("#datatable_faileditems2").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#failed_items_section2").css("display", "");
                                    }
                                }
                            }
                            else if(responseObj.error_code && responseObj.error_code == 2) {
                                $("#unknown_failure_section2").css('display', '');
                                $("#unknown_failure_msg2").html(responseObj.error);
                            }
                            else
                                alert('Something went wrong!');
                        });
                    }
                }
            });
        });
        

    </script>

    <script>
        function work() {
            var key = window.event.keyCode;
            if (key === 13) {
                var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                if (shipIDs == '') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }
                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i) {
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }
                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var print_wrong = [];
                var finalArray = [];
                var uniqueArray = []; // sent to backend
                for (var i = 0; i < ids.length; i++) {
                    if (!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) ) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }
                $.each(correctIDs, function (i, el) {
                    finalArray.push(el);
                    if ($.inArray(el, uniqueArray) === -1) {
                        uniqueArray.push(el);
                    }
                });
                if(wrongIDs.length){ // generate wrong-format labels
                    var wrong = '<b>Wrong IDs:</b><br/>';
                    for(i = 0; i < wrongIDs.length; ++i)
                        print_wrong.push('<b> <span style="color: #2A3F54;">' + wrongIDs[i] + '</span></b> <span style="color: black;"> | </span> Wrong Format<br/>')
                }
                if (finalArray.length) {
                    $.ajax({
                        type: 'GET',
                        url: '/warehouse/wokr/returns/inquiry',
                        dataType: 'text',
                        data: {ids: uniqueArray.join(",")}
                    }).done(function(response){
                        console.log(response);
                        responseObj = JSON.parse(response);
                        if(responseObj.success) {
                            var array = responseObj.data;
                            var wrong_status = responseObj.wrong_status;
                            var correct = '';
                            var wrong = '';
                            for(i = 0; i < finalArray.length; ++i) { // generate correct and wrong-status labels
                                var flag = true;
                                for(j = 0; j < wrong_status.length; ++j) {
                                    if(wrong_status[j].jollychic == finalArray[i] || wrong_status[j].order_number == finalArray[i] || wrong_status[j].order_reference == finalArray[i]) {
                                        if(wrong_status[j].jollychic == finalArray[i])
                                            print_wrong.push('<span style="color: #2A3F54;"><b>' + wrong_status[j].jollychic + ' <span style="color: black;"> | </span> </b></span> \'' + wrong_status[j].english + '\' Shipments can not be updated<br/>');
                                        if(wrong_status[j].order_number == finalArray[i])
                                            print_wrong.push('<span style="color: #2A3F54;"><b>' + wrong_status[j].order_number + ' <span style="color: black;"> | </span> </b></span> \'' + wrong_status[j].english + '\' Shipments can not be updated<br/>');
                                        if(wrong_status[j].order_reference == finalArray[i])
                                            print_wrong.push('<span style="color: #2A3F54;"><b>' + wrong_status[j].order_reference + ' <span style="color: black;"> | </span> </b></span> \'' + wrong_status[j].english + '\' Shipments can not be updated<br/>');
                                        flag = false;
                                        break;
                                    }
                                }
                                for(j = 0; flag && j < array.length; ++j) {
                                    if(array[j].jollychic == finalArray[i] || array[j].order_number == finalArray[i] || array[j].order_reference == finalArray[i]) {
                                        correct += '<b>';
                                        if(array[j].jollychic == finalArray[i])
                                            correct += '<span style="color: #2A3F54">' + array[j].jollychic + '</span>';
                                        if(array[j].order_number == finalArray[i])
                                            correct += '<span style="color: #2A3F54">' + array[j].order_number + '</span>';
                                        if(array[j].order_reference == finalArray[i])
                                            correct += '<span style="color: #2A3F54">' + array[j].order_referece + '</span>';
                                        correct += '<span style="color: black;"> | </span>';
                                        correct += ' <span style="color: #2A3F54;"> ' + array[j].company_name + '</span> <span style="color: black;"> | </span> <span style="color: #2A3F54"> Age: </span> </b>';
                                        correct += ' <span style="color: grey;"> ' + array[j].age + ' days </span> <span style="color: black;"> | </span> ';
                                        correct += '<b> <span style="color: #2A3F54;"> Attempts: </span></b><span style="color: grey;">' + array[j].num_faild_delivery_attempts + '</span>'; 
                                        correct += '<span style="color: black;"> | </span> <b> <span style="color: #2A3F54;"> Pieces: </span></b><span style="color: grey;">' + array[j].d_quantity + '</span>';
                                        correct += '<br/>';
                                        break;
                                    }
                                }
                            }
                            for(i = 0; i < finalArray.length; ++i) { // generate wrong-not-found label
                                var found = false;
                                for(j = 0; j < wrong_status.length; ++j) {
                                    if(wrong_status[j].jollychic == finalArray[i] || wrong_status[j].order_number == finalArray[i] || wrong_status[j].order_reference == finalArray[i]) {
                                        found = true;
                                        break;
                                    }
                                }
                                for(j = 0; !found && j < array.length; ++j) {
                                    if(array[j].jollychic == finalArray[i] || array[j].order_number == finalArray[i] || array[j].order_reference == finalArray[i]) {
                                        found = true;
                                        break;
                                    }
                                }
                                if(!found)
                                    wrong += '<b> <span style="color: #2A3F54;">' + finalArray[i] + '</span></b> <span style="color: black;"> | </span> Item Not Found <br/>';
                            }
                            for(i = 0; i < print_wrong.length; ++i)
                                wrong += print_wrong[i];
                            correct = correct;
                            wrong = wrong;
                            document.getElementById('correct_ids').innerHTML = correct;
                            document.getElementById('wrong_ids').innerHTML = wrong;
                        }
                    });
                }
            }
        }
    </script>

    @stop

