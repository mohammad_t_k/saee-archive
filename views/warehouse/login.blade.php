 <!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Saee</title>

    <!-- Bootstrap -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Datatables -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/build/css/custom.min.css" rel="stylesheet">
    
       
  </head>

  <body class="login">
    <div>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
             
             
            <form class="form" action="{{ URL::Route('WarehouseAdminVerify') }}" method="post">
                
              <?php  echo Form::token(); ?>
                <h1>Login</h1>
              <div>
                <input name="username" type="text" class="form-control" autocomplete="off" placeholder="Username" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your username / email" data-parsley-required value="" required="" />
              </div>
              
              <div>
                <input name="password" type="password" class="form-control" placeholder="Password" autocomplete="off" required="" />
              </div>
             
              
              <div class="form-actions">
                  <div>
                    <button class="btn btn-default submit" type="submit" >Log in</button>
                  </div>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                
                <div class="clearfix"></div>
                <br />

                <div class="pull-right">
                     &copy; All rights reserved Saee
                  </div>
                  <div class="clearfix"></div>
              </div>
              
            </form>
            <!-- END LOGIN FORM --> 
            
          </section>
        </div>

       
      </div>
    </div>
    
    <?php if ($error) { ?>
            <script type="text/javascript">
    alert('Invalid Username and Password');
            </script>
        <?php } ?>

    <?php if ($error == 2) { ?>
            <script type="text/javascript">
    alert('You are not allowed to login');
            </script>
        <?php } ?>
        
  </body>
  

</html>
