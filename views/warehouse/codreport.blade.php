@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>COD Report</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Export a COD report</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <form id="payments_codreport" action="/warehouse/payments/codreportpost" method="post">
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <select id="company" name="company" required="required" class="form-control"
                                                    type="text">
                                                <option value="all">All Companies</option>
                                                @foreach($companies as $compan)
                                                    <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <select name="city" id="city" class="form-control">
                                                @foreach($adminCities as $adcity)
                                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <textarea id="shipmentIDs" name="ids" required="required"
                                                      class="form-control" type="text" rows="20"></textarea>
                                        </div>
                                    </div>


                                    {{--<div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <p>
                                                <button id="submitForm" class="btn btn-success">Check</button>
                                            </p>
                                        </div>
                                    </div>--}}
                                </form>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button class="btn btn-success" onclick="CODpopupOption();">Check</button>
                                        </p>
                                    </div>
                                </div>
                                <table id="datatable_cod" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Nu.</th>
                                        <th>Pick Up date</th>
                                        <th>Order No.</th>
                                        <th>Company</th>
                                        <th>Delivery Date</th>
                                        <th>AWB</th>
                                        <th>Status</th>
                                        <th>COD Amount</th>
                                        <th>City</th>
                                        <th> Payment Date</th>
                                        <th> Paid to Supplier Date</th>
                                        <th> Transfer Code</th>
                                    </tr>
                                    </thead>


                                    <tfoot>
                                    <tr>
                                        <th>Nu.</th>
                                        <th>Pick Up date</th>
                                        <th>Order No.</th>
                                        <th>Company</th>
                                        <th>Delivery Date</th>
                                        <th>AWB</th>
                                        <th>Status</th>
                                        <th>COD Amount</th>
                                        <th>City</th>
                                        <th> Payment Date</th>
                                        <th> Paid to Supplier Date</th>
                                        <th> Transfer Code</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                            <!-- table start -->
                            <div class="modal fade" id="Modal_of_COD" role="dialog" style="overflow: auto">
                                <div class="modal-dialog  modal-sm">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Select Option For COD Report</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table id="cod_report_table" class="table table-striped table-bordered">
                                            </table>
                                        </div>
                                        <div class="modal-footer">

                                            <a id="download_codReportDirect" class="btn btn-success" onclick="submitselectedcod()">Submit</a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- !table start -->
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>



    <?php


    $mydata_cod = '';


    $count = 1;
    foreach ($items as $anitem) {
        $pickup = $anitem->pick_up;
        $order = addslashes($anitem->order);
        $company = $anitem->company;
        $dropoff = $anitem->drop_off;
        $waybill = $anitem->waybill;
        $status = $anitem->status;
        $cod = $anitem->cod;
        $updated_at = $anitem->updated_at;
        $city = $anitem->city;
        if ($anitem->Paid_to_Supplier_Date == "") {
            $Paid_to_Supplier_Date = 'Not Paid to Supplier';
        } else {
            $Paid_to_Supplier_Date = $anitem->Paid_to_Supplier_Date;
        }
        if ($anitem->transfer_code == "") {
            $transfer_code = '';
        } else {
            $transfer_code = $anitem->transfer_code;
        }
        $mydata_cod .= "[ \"$count\" ,\"$pickup\" ,\"$order\" ,\"$company\",\"$dropoff\" , \"$waybill\" ,\"$status\" ,  \"$cod\" , \" $city \" , \"$updated_at\",\"$Paid_to_Supplier_Date\",\"$transfer_code\"], ";
        ++$count;
    }


    ?>

    <script>


        $(document).ready(function () {


            var table_plan = $("#datatable_cod").DataTable({

                "data": [
                    <?php echo $mydata_cod ?>

                  ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],

                responsive: true,
                'order': [[0, 'asc']]
            });


            /*$("#submitForm").click(function () {
                        var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");


                        if (shipIDs == '') {
                            bootbox.alert('Please enter IDs!');
                            return;
                        }

                        var ids = shipIDs.split(',');

                        var wrongIDs = Array();
                        var correctIDs = Array();
                        var correctedIDs = Array();
                        var finalArray = [];
                        for (var i = 0; i < ids.length; i++) {
                            if (!ids[i].replace(/\s/g, '').length) {
                                continue;
                            }
                            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push(ids[i]);
                            }
                            else {
                                /!*if (/[0-9]{8}KS/.test(ids[i])) {
                                    correctIDs.push('JC' + ids[i]);
                                    correctedIDs.push(ids[i]);
                                }
                                else if (/C[0-9]{8}KS/.test(ids[i])) {
                                    correctIDs.push('J' + ids[i]);
                                    correctedIDs.push(ids[i]);
                                } *!/

                                wrongIDs.push(ids[i]);
                            }
                        }

                        $.each(correctIDs, function (i, el) {
                            if ($.inArray(el, finalArray) === -1) {
                                finalArray.push(el);
                            }
                        });


                        if (wrongIDs.length) {
                            bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                        }

                        /!*if (correctedIDs.length) {
                            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                        }*!/


                        if (finalArray.length) {
                            document.getElementById("shipmentIDs").value = JSON.stringify(finalArray);
                            return true;
                            /!*var responseObj;
                             $.ajax({
                             type: 'POST',
                             url: '/warehouse/payments/codreportpost',
                             dataType: 'text',
                             data: { ids:JSON.stringify(finalArray)},
                             }).done(function (response) {

                             // responseObj = JSON.parse(response);
                             console.log(response);

                             if (response)
                             {
                             location.reload();

                             }
                             else
                             {
                             bootbox.alert('Something went wrong!'   );

                             }
                             });*!/

                        }
                        else {
                            alert('Nothing to check!');
                            return false;
                            //location.reload();
                        }

                    }
            );*/
        });

    </script>
    <script>
        function CODpopupOption() {
            var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
            var company = document.getElementById("company").value;
            var city = document.getElementById("city").value;

            if (shipIDs == '') {
                bootbox.alert('Please enter IDs!');
                return;
            } else {
                function CODpopupOption(callback) {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/payments/checkcodreportviewstatus',
                        dataType: 'text',
                        cache: false,
                        success: callback,
                        data: {
                            ids: shipIDs, company: company, city: city
                        }
                    })
                }

                CODpopupOption(function (res) {
                    var data_count = JSON.parse(res);
                    if (data_count.error) {
                        alert('Failed: ' + data_count.error);
                    } else {
                        var COD_report_Information = '<caption><h4>COD Report</h4></caption><tr>' + '<th>Select</th>' + '<th>COD</th>' + '</tr>';
                        COD_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="codreportradio">' + '</td><td>' + 'Download COD Report' + '</td></tr><tr><td>'
                                + '<input id="viewcod"  type="radio" name="codreportradio">' + '</td><td>' + 'View COD Report' + '</td></tr>';

                        $('#cod_report_table').html(COD_report_Information);
                        $('#Modal_of_COD').modal("show");
                        if (data_count.count > 500) {
                            document.getElementById("viewcod").disabled = true;
                        }
                    }

                });
            }
            return;
        }

        function submitselectedcod() {
            var codreportradio = document.getElementsByName('codreportradio');
            var is_radio_check = '';

            for (var i = 0, length = codreportradio.length; i < length; i++) {
                if (codreportradio[i].checked) {
                    var cod_option = codreportradio[i].parentNode.parentNode.childNodes[1].innerHTML;
                    //if (confirm("Are you sure you want to " + cod_option) == true) {
                        if (cod_option == 'Download COD Report') {
                            var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

                            /*var path = '/warehouse/payments/downloadcaptaincodReportDirect?ids=' + shipIDs;
                            location.href = path;*/
                            var form = document.createElement("form");
                            var element1 = document.createElement("input");
                            form.method = "POST";
                            form.action = "/warehouse/payments/downloadcaptaincodReportDirect";
                            element1.value = shipIDs;
                            element1.name = "ids";
                            form.appendChild(element1);
                            document.body.appendChild(form);
                            form.submit();
                            $('#Modal_of_COD').modal('hide');

                            return;
                        } else if (cod_option == 'View COD Report') {
                            var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");


                            if (shipIDs == '') {
                                bootbox.alert('Please enter IDs!');
                                return;
                            }

                            var ids = shipIDs.split(',');

                            var wrongIDs = Array();
                            var correctIDs = Array();
                            var correctedIDs = Array();
                            var finalArray = [];
                            for (var i = 0; i < ids.length; i++) {
                                if (!ids[i].replace(/\s/g, '').length) {
                                    continue;
                                }
                                if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                                    correctIDs.push(ids[i]);
                                }
                                else {

                                    wrongIDs.push(ids[i]);
                                }
                            }

                            $.each(correctIDs, function (i, el) {
                                if ($.inArray(el, finalArray) === -1) {
                                    finalArray.push(el);
                                }
                            });

                            if (wrongIDs.length) {
                                bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                            }

                            if (finalArray.length) {
                                document.getElementById("shipmentIDs").value = JSON.stringify(finalArray);
                            }
                            else {
                                alert('Nothing to check!');
                                return false;

                            }
                            document.forms["payments_codreport"].submit();
                            $('#Modal_of_COD').modal('hide');
                        }
                    /*} else {
                        bootbox.alert('You cancled!');
                    }*/

                    var is_radio_check = 'ok';
                    break;
                }
            }

            if (is_radio_check == '') {
                bootbox.alert("please select option first");
            }
        }

    </script>
    @stop

