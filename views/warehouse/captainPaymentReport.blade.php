@if( Auth::user()->role < 4 || Auth::user()->role == 15)

    <script>window.location = "/warehouse/403";</script>

@endif


@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captain Payment Report</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/captainpaymentreportpost">
                        <input id="captain_id" name="captain_id" type="hidden"
                               value="<?php if (isset($captain_id)) echo $captain_id; else echo null;?>">

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12   col-md-offset-1">From <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <input id="fromdate" name="fromdate"
                                       value="{{$fromdate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">To <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="todate" name="todate"
                                       value="{{$todate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date" disabled>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    <option value="All">All</option>
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-5 col-sm-6 col-xs-12">Paid To Captain <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <select name="paidtocaptain" id="paidtocaptain" class="form-control">
                                    <option value="1"<?php if (isset($paidtocaptain) && $paidtocaptain == 1) echo 'selected';?>>
                                        Yes
                                    </option>
                                    <option value="0"<?php if (isset($paidtocaptain) && $paidtocaptain == 0) echo 'selected';?>>
                                        No
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Admin <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="admin" id="admin" class="form-control">
                                    <option value="All">All</option>
                                    @foreach($admins as $admin)
                                        <option value="{{$admin->id}}"<?php if (isset($admin_id) && $admin_id == $admin->id) echo 'selected';?>>{{$admin->username}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="hub" id="hub" class="form-control">
                                    @foreach($adminHubs as $adhub)
                                        <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Porta Hub</label>

                            <div class="col-md-7 col-sm-6 col-xs-12 col-md-offset-1">
                                <select name="chosen_porta_hub" id="chosen_porta_hub" class="form-control">
                                    @foreach($adminPortaHubs as $ad_porta_hub)
                                        <option value="{{$ad_porta_hub->porta_hub_id}}" <?php if (isset($chosen_porta_hub) && $ad_porta_hub->porta_hub_id == $chosen_porta_hub) echo 'selected';?>>{{$ad_porta_hub->name}}</option>
                                    @endforeach
                                    <option value="-1" <?php if (isset($chosen_porta_hub) && $chosen_porta_hub == '-1') echo 'selected';?>>
                                        No Porta Hub
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Search</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="searchcaptain" onkeyup="searchCaptain()"
                                       class="date-picker form-control col-md-7 col-xs-12" placeholder="Search">
                            </div>
                        </div>

                    </form>

                </div>
            </div>
            <label id="tab1"
                   style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;;background: #fff;box-shadow: inset 0 3px #2a3f54;"
                   onclick=allother_Payment()><span>Other Payment </span></label>
            <label id="tab2"
                   style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;background: #f0f0f0;;"
                   onclick=Pickup_Payment()><span>Pickup Payment</span></label>

            <div class="clearfix"></div>
            <div class="row">
               <section id="content1" class="tab-content">
                   <div class="col-md-4 col-sm-12 col-xs-12">
                       <div class="col-md-6 col-sm-6 col-xs-12">
                           <button class="btn btn-success" onclick="fnExcelReport()">Export to Excel</button>
                       </div>
                   </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">

                                <table id="datatable_cod" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th><input id="selectallup" class="dt-checkboxes"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Bank Deposit Date</th>
                                        <th>Admin</th>
                                        <th>Hub</th>
                                        <th>Porta Hub</th>
                                        <th>Paid to Captain Admin</th>
                                        <th>City</th>
                                        <th>Captain ID</th>
                                        <th>Captain</th>
                                        <th>Captain Payment</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $balance = 0;
                                    $total = 0;
                                    foreach ($items as $item) {
                                    $total += $item->credit;
                                    ?>
                                    <tr id='<?= 'tr' . $item['id'] ?>'>
                                        <td><input id="ch<?= $item['id'] ?>" class="dt-checkboxes"
                                                   value="<?= $item['id'] ?>"
                                                   onchange="onChangeAccountHistoryCheckBox('<?= $item['id'] ?>')"
                                                   type="checkbox"></td>
                                        <td><?= date('Y-m-d', strtotime($item['deposit_date']))  ?></td>
                                        <td><?= $item['adminuser'] ?></td>
                                        <td><?= $item['hub_name'] ?></td>
                                        <td><?php if ($item['porta_hub_id'] == '0') echo 'No Porta Hub'; else echo $item['porta_hub_name']; ?></td>
                                        <td><?php if ($item['paid_to_captain'] == 0) echo 'Not Paid to Captain'; else echo $item['paid_admin_name']; ?></td>
                                        <td><?= $item['city'] ?></td>
                                        <td><?= $item['captain_id'] ?></td>
                                        <td><?php echo $item->first_name . ' ' . $item->last_name . ' 0' . substr(str_replace(' ', '', $item->phone), -9);?></td>
                                        <td><?= $item['captain_amount'] ?></td>
                                        <td><?php if ($item['paid_to_captain'] == 0) echo 'Not Paid'; else  echo 'Paid';?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="4">Admin</td>
                                        <td>Selected Amount</td>
                                        <td id="selectedAmount">0</td>
                                    </tr>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Bank Deposit Date</th>
                                        <th>Admin</th>
                                        <th>Hub</th>
                                        <th>Porta Hub</th>
                                        <th>Paid to Captain Admin</th>
                                        <th>City</th>
                                        <th>Captain ID</th>
                                        <th>Captain</th>
                                        <th>Captain Payment</th>
                                        <th>Status</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount To Pay <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="amountPayed" name="amountPayed" required="required"
                                                   class="form-control" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <button id="updateCaptainPayed" class="btn btn-success">Pay to Captain</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               </section>

                <section id="content2" class="tab-content">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <button class="btn btn-success" onclick="pickupExcelReport()">Export to Excel</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <div class="row">

                                    <table id="datatable_pickup_cod" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th><input id="selectallpickup" class="dt-checkboxes"
                                                       onchange="onChangeAllPickupCheckBox(this)"
                                                       type="checkbox">Select
                                            </th>
                                            <th>Admin</th>
                                            <th>Paid to Captain Admin</th>
                                            <th>City</th>
                                            <th>Captain ID</th>
                                            <th>Pick_up Captain</th>
                                            <th>Captain Pick_up Payment</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $balance = 0;
                                        $total = 0;
                                        foreach ($captain_pickup_items as $pickup_item) {
                                        $total += $pickup_item->credit;
                                        ?>
                                        <tr id='<?= 'tr' . $pickup_item['id'] ?>'>
                                            <td><input id="ch<?= $pickup_item['id'] ?>" class="dt-checkboxes"
                                                       value="<?= $pickup_item['id'] ?>"
                                                       onchange="onChangeAccountHistorypickCheckBox('<?= $pickup_item['id'] ?>')"
                                                       type="checkbox"></td>
                                            <td><?= $pickup_item['adminuser'] ?></td>
                                            <td><?php if ($pickup_item['paid_to_captain'] == 0) echo 'Not Paid to Captain'; else echo $pickup_item['paid_admin_name']; ?></td>
                                            <td><?= $pickup_item['city'] ?></td>
                                            <td><?= $pickup_item['captain_id'] ?></td>
                                            <td><?php echo $pickup_item->first_name . ' ' . $pickup_item->last_name . ' 0' . substr(str_replace(' ', '', $pickup_item->phone), -9);?></td>
                                            <td><?= $pickup_item['captain_amount'] ?></td>
                                            <td><?php if ($pickup_item['paid_to_captain'] == 0) echo 'Not Paid'; else  echo 'Paid';?></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td colspan="4">Admin</td>
                                            <td>Selected Amount</td>
                                            <td id="selectedpickupAmount">0</td>
                                        </tr>
                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <th><input id="selectallpickdown" class="dt-checkboxes" style="width: auto"
                                                       onchange="onChangeAllPickupCheckBox(this)"
                                                       type="checkbox">Select
                                            </th>
                                            <th>Admin</th>
                                            <th>Paid to Captain Admin</th>
                                            <th>City</th>
                                            <th>Captain ID</th>
                                            <th>Pick_up Captain</th>
                                            <th>Captain Pick_up Payment</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>


                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount To Pay <span
                                                        class="required">*</span>
                                            </label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="amount_pickup_Payed" name="amount_pickup_Payed" required="required"
                                                       class="form-control" type="text"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                            <button id="updateCaptainpickupPayed" class="btn btn-success">Pay to Captain</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        <?php $admin_id = Session::get('admin_id'); if ($admin_id == 26) { ?>
        document.getElementById('todate').disabled = false;
                <?php }?>

        var paidtocaptain = document.getElementById('paidtocaptain').value;
        if (paidtocaptain == 1) {
            document.getElementById('updateCaptainPayed').disabled = true;
            document.getElementById('updateCaptainpickupPayed').disabled = true;
            //$("input:checkbox").attr('disabled', true)
        }
        var items = <?php echo $items; ?>;
        var pickupitems = <?php echo $captain_pickup_items; ?>;
        var captain_id = '<?php echo $captain_id; ?>';
        $("#updateCaptainPayed").click(function (e) {
            var amountPayed = document.getElementById('amountPayed').value;
            var city = document.getElementById('city').value;
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want to pay captain amount since rolling back will be difficult?") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatecaptainpayment',
                    dataType: 'text',
                    data: {
                        ids: ids.join(','),
                        captain_id: captain_id,
                        amountPayed: amountPayed,
                        city: city
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(amountPayed + ' SAR is payed to captain  ');
                        window.location = '/warehouse/captainpaymentreport?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });

        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == checkbox.checked || document.getElementById('ch' + items[i].id).disabled)
                    continue;
                document.getElementById('ch' + items[i].id).checked = checkbox.checked;
                onChangeAccountHistoryCheckBox(items[i].id);
            }
        }

        var selectedAmo = 0;

        function onChangeAccountHistoryCheckBox(history_id) {
            var row = document.getElementById("tr" + history_id);
            var checkbox = document.getElementById("ch" + history_id);
            var credit = row.getElementsByTagName("td")[9];
            var selectedAmount = document.getElementById("selectedAmount");
            selectedAmo = parseFloat(selectedAmount.innerHTML);
            if (checkbox.checked) {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo + parseFloat(credit.innerHTML);
                }
            } else {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo - parseFloat(credit.innerHTML);
                }
            }

            selectedAmount.innerHTML = selectedAmo.toFixed(2);
        }
        $("#updateCaptainpickupPayed").click(function (e) {
            var amount_pickup_Payed = document.getElementById('amount_pickup_Payed').value;
            var city = document.getElementById('city').value;
            var ids = [];
            for (var i = 0; i < pickupitems.length; i++) {
                if (document.getElementById('ch' + pickupitems[i].id).checked == true) {
                    ids.push(pickupitems[i].id);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want to pay captain amount since rolling back will be difficult?") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatecaptainpickuppayment',
                    dataType: 'text',
                    data: {
                        ids: ids.join(','),
                        captain_id: captain_id,
                        amountPayed: amount_pickup_Payed,
                        city: city
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(amount_pickup_Payed + ' SAR is payed to captain  ');
                        window.location = '/warehouse/captainpaymentreport?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });
        function onChangeAllPickupCheckBox(checkbox) {
            document.getElementById('selectallpickup').checked = checkbox.checked;
            document.getElementById('selectallpickdown').checked = checkbox.checked;
            for (var i = 0; i < pickupitems.length; i++) {
                if (document.getElementById('ch' + pickupitems[i].id).checked == checkbox.checked || document.getElementById('ch' + pickupitems[i].id).disabled)
                    continue;
                document.getElementById('ch' + pickupitems[i].id).checked = checkbox.checked;
                onChangeAccountHistorypickCheckBox(pickupitems[i].id);
            }
        }
        var selectedpickupAmo = 0;
        function onChangeAccountHistorypickCheckBox(history_id) {
            var row = document.getElementById("tr" + history_id);
            var checkbox = document.getElementById("ch" + history_id);
            var credit = row.getElementsByTagName("td")[6];
            var selectedpickupAmount = document.getElementById("selectedpickupAmount");
            selectedpickupAmo = parseFloat(selectedpickupAmount.innerHTML);
            if (checkbox.checked) {
                if (credit.innerHTML != '') {
                    selectedpickupAmo = selectedpickupAmo + parseFloat(credit.innerHTML);
                }
            } else {
                if (credit.innerHTML != '') {
                    selectedpickupAmo = selectedpickupAmo - parseFloat(credit.innerHTML);
                }
            }

            selectedpickupAmount.innerHTML = selectedpickupAmo.toFixed(2);
        }
        function searchCaptain() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchcaptain");
            filter = input.value.toUpperCase();
            table = document.getElementById("datatable_cod");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                tdcapid = tr[i].getElementsByTagName("td")[3];
                td = tr[i].getElementsByTagName("td")[4];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || tdcapid.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        tr[i].getElementsByTagName("td")[0].children[0].disabled = false;
                    } else {
                        tr[i].style.display = "none";
                        tr[i].getElementsByTagName("td")[0].children[0].disabled = true;
                    }
                }
            }
        }

        $('input:checkbox').removeAttr('checked');

        function exportTableToExcel(tableID) {
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

            // Specify file name
            var filename = 'Captain Payment Report.xls';

            // Create download link element
            downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if (navigator.msSaveOrOpenBlob) {
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob(blob, filename);
            } else {
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                // Setting the file name
                downloadLink.download = filename;

                //triggering the function
                downloadLink.click();
            }
        }

        function fnExcelReport() {
            var i, j;
            var csv = "";

            var table = document.getElementById("datatable_cod");

            var table_headings = table.children[0].children[0].children;
            var table_body_rows = table.children[1].children;

            var heading;
            var headingsArray = [];
            for (i = 1; i < table_headings.length; i++) {
                heading = table_headings[i];
                headingsArray.push('"' + heading.innerHTML + '"');
            }

            csv += headingsArray.join(',') + ";\n";

            var row;
            var columns;
            var column;


            var columnsArray;
            for (i = 0; i < table_body_rows.length - 1; i++) {
                row = table_body_rows[i];
                columns = row.children;
                columnsArray = [];
                for (j = 1; j < columns.length; j++) {
                    var column = columns[j];
                    columnsArray.push('"' + column.innerHTML + '"');
                }
                csv += columnsArray.join(',') + ";\n";
            }

            download("Captain Payment Report.csv", csv);

        }

        //From: http://stackoverflow.com/a/18197511/2265487
        function download(filename, text) {
            var universalBOM = "\uFEFF";
            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + text));
            pom.setAttribute('download', filename);

            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            }
            else {
                pom.click();
            }
        }

        function pickupExcelReport() {
            var i, j;
            var csv = "";
            var table = document.getElementById("datatable_pickup_cod");
            var table_headings = table.children[0].children[0].children;
            var table_body_rows = table.children[1].children;
            var heading;
            var headingsArray = [];
            for (i = 1; i < table_headings.length; i++) {
                heading = table_headings[i];
                headingsArray.push('"' + heading.innerHTML + '"');
            }
            csv += headingsArray.join(',') + ";\n";
            var row;
            var columns;
            var column;
            var columnsArray;
            for (i = 0; i < table_body_rows.length - 1; i++) {
                row = table_body_rows[i];
                columns = row.children;
                columnsArray = [];
                for (j = 1; j < columns.length; j++) {
                    var column = columns[j];
                    columnsArray.push('"' + column.innerHTML + '"');
                }
                csv += columnsArray.join(',') + ";\n";
            }

            download("Captain pickup Payment Report.csv", csv);

        }

        document.getElementById("content1").style.display = "block";
        document.getElementById("content2").style.display = "none";
        function allother_Payment() {
            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none";
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab2').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none' });
            }
        function Pickup_Payment() {
            document.getElementById("content1").style.display = "none";
            document.getElementById("content2").style.display = "block";
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none' });
            $('#tab2').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
        }

    </script>
@stop