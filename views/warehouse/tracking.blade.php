<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Saee </title>


    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
   <link href='<?php echo asset_url(); ?>/newweb/jquery-bar-rating-master/dist/themes/fontawesome-stars.css' rel='stylesheet' type='text/css'>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/search.js"></script>

  <script src="<?php echo asset_url(); ?>/newweb/jquery-bar-rating-master/dist/jquery.barrating.min.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="css/tracking.css">
  <link rel="stylesheet" type="text/css" href="css/badge.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- bootstrap-progressbar -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/build/css/tracking-page.css">

<style>
@import url("//www.jquery-steps.com/Content/examples.css");

table.dataTable thead .sorting::after{
  content: none !important;
}
table.dataTable thead .sorting_desc::after {
  content: none !important;
}
table.dataTable thead .sorting_asc::after {
  content: none !important;
}
.input-wrap {
    width: 100%;
    display: inline-block;
}
.wizard > .steps > ul > li {
    width: 35%;
}
.wizard > .content > .body{
  position: relative !important;
}
.wizard > .content{
  min-height: auto !important;
}
.collapse {
    display: block !important;
    border: none !important;
    box-shadow: none !important;
    text-align: right;
}

.actions ul li:nth-child(3) {
    display: none !important;
}

.logocenter {
    width: 50%;
    float: left;
    text-align: left !important;
}
.tracking-id {
    width: 15%;
    float: left;
    margin-top:25px;
}
.tracking-field {
    width: 85%;
    float: right;
}
#trackingnum {
    width: 70%;
}
.submit-tracking {
    height: 48px;
    width: 115px;
}
 #map_canvas {
          height: 300px;
          width: 100%;
          margin: 0;
      }
      #map_canvas .centerMarker {
          position: absolute;
          /*url of the marker*/
          background: url(../web/images/marker.png) no-repeat;
          /*center the marker*/
          top: 50%;
          left: 50%;
          z-index: 1;
          /*fix offset when needed*/
          margin-left: -10px;
          margin-top: -34px;
          /*size of the image*/
          height: 34px;
          width: 34px;
          cursor: pointer;
      }
#ratingForm {
    padding-left: 15px;
    padding-right: 15px;
    padding-bottom: 30px;
}
#example-basic-p-1{
width:100% !important;
}

.steps ul {
    display: none;
}

</style>

</head>
<body>

<!--header start-->
<?php


$scheduleDates = array();

$todayDate = date('Y-m-d');
$check = 0;



$SecheduledDatesQuery = 'select date(DATE_ADD(date(now()), INTERVAL 0 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 0 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 1 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 1 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 2 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 2 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 3 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 3 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 4 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 4 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 5 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 5 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 6 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 6 DAY))) not in ("Friday")
union
select date(DATE_ADD(date(now()), INTERVAL 7 DAY)) "scheduledDate"
from delivery_request where jollychic = "'.$waybill.'"
and dayname(date(DATE_ADD(date(now()), INTERVAL 7 DAY))) not in ("Friday")'; 

$Dates = DB::select($SecheduledDatesQuery);


//print_r($Dates[0]->scheduledDate);exit;


if(!empty($Dates)){

foreach($Dates as $date)
if($date->scheduledDate >= $todayDate){
  $check = 1;
  $scheduleDates[] = $date->scheduledDate;
}

}


?>
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->

                <div class="logocenter"><img src="<?php echo asset_url(); ?>/newweb/images/logo.png"></a>
                </div>
<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../changelanguage?language=ar&amp;page=/trackingpage?trackingnum=<?php echo $waybill ?>">عربى</a></li>
                    </ul>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>


<!--header end-->

<!--1st section start-->
<div class="container ">
    <div class="row">
        <div class="col-md-12 business">

            <h5>Track your Package with Saee</h5>

            <div class="graybg2">
                <div class="input-wrap">
                    <div class="tracking-id">
                    Your Tracking ID : 
                    </div>
                    <div class="tracking-field">
                        <form action="" method="GET">
                    <input type="text" name="trackingnum" id="trackingnum" value="<?php echo $waybill ?>">
                    <input class="submit-tracking btn-search" type="submit"  value="Search">
                        </form>
                </div>
            </div><!-- end input wrap -->
            </div>

            
<?php 

if(count($orderDetail)){

  $statueses = 'select status, updated_at from orders_history where waybill_number = "'.$waybill.'" and status <= "'.$orderDetail[0]->status.'" group by status order by status asc ';

$distinctStatusHistory = DB::select($statueses);

 ?>
    <!-- Button trigger modal -->
<div class="col-md-12 btncetner">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Update Address
</button>
</div>
<?php }?>
    <!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="example-basic">
<!-- <h3>Choose your Delivery Date</h3>
        <section>
          <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="shipment_date" id="shipment_date"
                                                                class="form-control">
                                                            
                                                            <?php

                          

                                                           /*foreach ($scheduleDates as $ascheduleDate) {
                                                              // echo "<option value='$ascheduleDate'>$ascheduleDate</option>";
                                                            } */

                                                            ?>
                                                        </select>
                                                    </div>
          </section> -->
<h3>Update your Address</h3>
          <section>
        
       <div style="display: inline-block; width: 100%;">
                
                <input type="hidden" id="address_latitude" name="address_latitude" value="" class="lat">
                <input type="hidden" id="address_longitude" name="address_longitude" value="" class="lng">
                <input type="hidden" id="trackingnum" name="trackingnum" value="" class="trackingnum">
               
                  
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <label><b>SELECT HOME LOCATION ON MAP</b></label>
                    </div>
                  </div>
                </div>
                
                  <div id="map_canvas"></div>
                  <p id="homeAddressErr" class="text-dangers"></p>
                
                <div class="col-md-12">
                  <button id="update" type="submit" onclick="if(!validateAllInputs()) return false;" class="button3">UPDATE</button>
                </div>
              
               </div> 
             </section>
             </div>
      </div> 
      
    </div>
  </div>
</div>
    

    
   <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_api_key?>&callback=initMap" async defer></script>
    <script>
      function initMap() {
          try {
              //var status = <?php //echo $orderDetail[0]->status; ?>;
              var address_latitude = 21.2854;
              var address_longitude = 39.2376;
              var trackingnum = '';
              var newshipmentdate = '';
              var shipdate = '';
              var mapOptions = {
                  zoom: 8,
                  center: new google.maps.LatLng(address_latitude, address_longitude),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };
              document.getElementById('map_canvas').style = 'height:200px';
              var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
              infoWindow = new google.maps.InfoWindow;

               // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {

            var accuracy = position.coords.accuracy;
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);


            if(accuracy > 35){

              infoWindow.setContent('Please Update Location Accuracy ' + accuracy);
              document.getElementById('update').disabled = true;
            }
            else{
              infoWindow.setContent('Location found. Accuracy is ' + accuracy);
            }
            
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }


              google.maps.event.addListener(map, 'center_changed', function () {
                  address_latitude= document.getElementById('address_latitude').value = map.getCenter().lat();
                 address_longitude= document.getElementById('address_longitude').value = map.getCenter().lng();
                 
              });
              $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
              //do something onclick
                  .click(function () {
                      var that = $(this);
                      if (!that.data('win')) {
                          that.data('win', new google.maps.InfoWindow({
                              content: 'this is the center'
                          }));
                          that.data('win').bindTo('position', map, 'center');
                      }
                      that.data('win').open(map);
                  });

        document.getElementById('update').addEventListener('click', function() {
         
                $('.lat').val(address_latitude);
                $('.lng').val(address_longitude);
                $('.trackingnum').val(trackingnum);
                trackingnum =  document.getElementById('trackingnum').value ;
                //newshipmentdate = document.getElementById('shipment_date').value ;

                /*if(newshipmentdate == ''){

                  alert('Sorry this shipment is too old, please contact customer service');
                  return false;
                } */
                
                  $.ajax({
                    type: 'POST',
                    url: '/update_tracking_address',
                    dataType: 'text',
                    data: {
                        address_latitude: address_latitude,
                        address_longitude: address_longitude,
                        trackingnum: trackingnum
                        //newshipmentdate: newshipmentdate
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(' New Address Updated');
                        //window.location = '/trackingpage?trackingnum=' + trackingnum;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });

                 });
          }
          catch (e) {
              alert(e);
          }
      }
    </script>
    <script>google.maps.event.addDomListener(window, 'load', initMap);</script>


            <div class="row">
                <div class="col-md-6">


                    <div class="graybg2">Captain Name: <?php echo $captainName; ?></div>

                </div>

            <div class="col-md-6">
                <div class="graybg2">Captain Phone: <?php echo $captainPhone; ?></div>

            </div>

    </div>

<!-- start rating system coding --> 

<?php if(!empty($captainId)) { ?> <!-- hide rating if captain not set -->
    <div class="row">

      <div class="col-md-6">
        <ul id="progress" style="margin-bottom: 50px;margin-top: 30px;"> 

    <li><div class="node green"></div><p>Created By Supplier - <?php if(!$distinctStatusHistory[0]=="") echo $distinctStatusHistory[0]->updated_at; ?></p></li>
    <li><div class="divider grey"></div></li>
    <li><div class="node grey"></div><p>In Route - <?php  echo $distinctStatusHistory[1]->updated_at; ?></p></li>
    <li><div class="divider grey"></div></li>
    <li><div class="node grey"></div><p>In Warehouse - <?php  echo $distinctStatusHistory[2]->updated_at; ?> </p></li>
    <li><div class="divider grey"></div></li>
    <li><div class="node grey"></div><p>With Captain - <?php if (array_key_exists("3",$distinctStatusHistory)) echo $distinctStatusHistory[3]->updated_at; ?></p></li>
    <li><div class="divider grey"></div></li>
    <li><div class="node grey"></div><p>Delivered - <?php if (array_key_exists("4",$distinctStatusHistory)) echo $distinctStatusHistory[4]->updated_at; ?></p></li>
  
</ul>
      </div>
      <div class="col-md-6">
   
 <h3 style="color:#F8941D;padding-left: 15px;padding-top: 15px;">Rate this captain</h3>
                    <form id="ratingForm" name="ratingForm" action=""> 

                      <div class="post-action">
                            <select  class='rating' id='rating_<?php echo $waybill; ?>' data-id='rating_<?php echo $waybill; ?>'>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                            </select>
                            <textarea name="comments" id="comments" class="comments" style="width: 100%;height: 200px" placeholder="Your Comments"></textarea>
                            <input type="hidden" name="captain_id" id="captain_id" value="<?php if(!empty($captainId)){ echo $captainId; } ?>">
                            <input type="hidden" name="waybill" id="waybill" value="<?php echo $waybill; ?>">
                            <input style="margin-top: 15px;" class="submit-tracking btn-search" type="submit" name="submitrating" id="submitrating" value="Submit">
                          </form>
                            <div style='clear: both;'></div>


                            <!-- Set rating -->
                            
                       
                      </div>

                      <div class="rating_success"></div>

        </div>

      <?php } ?>

    </div>

<!-- end rating system coding --> 
            <div class="graybg3"><?php
                echo $finalstaustxt
                ?> <br><br>

                <div class="progress progress_sm">
                    <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" data-transitiongoal=<?php echo $finalstausnum ?> ></div>
                </div>

            </div>

            <!--2nd section start-->

            <table id="datatable_history" class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Date and Time</th>
                    <th scope="col">Status</th>
                    <th scope="col">City</th>
                    <th scope="col">Remarks</th>
                </tr>
                </thead>
                <tbody>
                    <!--
                <tfoot  class="thead-dark">
                <tr>
                    <th>Date and Time</th>
                    <th>Status</th>
                    <th>City</th>
                    <th>Remarks</th>
                </tr>
                </tfoot>
                -->

                </tbody>
            </table>



            <!--2nd section end-->




        </div>



    </div>
</div>
<!--1st section end-->

<div class="container">

    <div class="footertext1">© All rights reserved Saee</div>
</div>


</body>

<!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

<!-- Bootstrap -->

<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Datatables -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/vfs_fonts.js"></script>
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/custom.min.js"></script>
</html>




<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->


<!-- Bootstrap -->

<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


<!-- iCheck -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/jquery.steps.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>


<script>
    

    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        onFinished: function (event, currentIndex)
        {
            // Submission code
            //$(this).submit();
            alert("Submitted!");
        }
    });


</script>


<script >

    <?php


    $mydata_history = '';



    foreach ($orderHistory as $astep)
    {

        $mydata_history .= "[  \"$astep->updated_at\" ,\"$astep->english\" , \"$astep->city\",
        \"$astep->notes\"], ";

    }


    ?>



    $(document).ready(function() {


                var table_history = $("#datatable_history").DataTable({

                    "data": [
                        <?php echo $mydata_history ?>

                      ],
                       "columnDefs": [ {
                      "targets": [0,1,2,3],
                      "orderable": false
                      } ],
                    "autoWidth": false,
                    dom: "rt",

                    responsive: true,
                    'order': [[0, 'asc']]
                });

            });

</script>


<script type="text/javascript">


  var dbrating = '';
  var hover = true;
  var click = false;

<?php
 if (!$captainRating->isEmpty()) { ?>

dbrating = "<?php echo $captainRating[0]->captain_rating; ?>"

 
 <?php } ?>

if(dbrating != ''){

  hover = false;
  click = true;

  $('.comments').hide();
  $('.submitrating').hide();
}



        
                $('#ratingForm').on('submit', function(e) {

               e.preventDefault();

                    // Get element id by data-id attribute

                    // Get captiain id by id

                    var captain_id = $('#captain_id').val();

                    var waybill = $('#waybill').val();

                    var rating = $('.rating').val();
                    var comments = $('.comments').val();


                    // rating was selected by a user
                   var that = this;

                        // AJAX Request
                        $.ajax({
                            url: '/tracking/ratecaptain',
                            type: 'post',
                            cache:false,
                            data: {waybill:waybill,rating:rating,captain_id:captain_id,comments:comments},
                            dataType: 'json',
                            success: function(data){

                              $('.rating').barrating('destroy'); // destroying barrating settings
                              $('.comments').hide();
                              $('.submitrating').hide();
                              $('.rating').barrating({   // initialiazing barrating plugin again with diff settings...
                theme: 'fontawesome-stars',
                hoverState: false,
                readonly: true,
                });

                              $('.rating_success').html('Thank you! your rating submitted'); // show thank you message

                              setTimeout(function(){
                              $('.rating_success').remove();
                              }, 2000);
                                
                            }


                        });
                    
         
        });

                  $('.rating').barrating({
                theme: 'fontawesome-stars',
                hoverState: hover,
                readonly: click,
                });
                
      
        </script>

        <script type='text/javascript'>
                            $(document).ready(function(){
                                $('#rating_<?php echo $waybill; ?>').barrating('set',<?php if (!$captainRating->isEmpty()) {echo $captainRating[0]->captain_rating; } else { echo '0'; } ?>);
                            });
                            
                            </script>

        <script type="text/javascript">
          
          var list = document.getElementById('progress');

    children = list.children,
    completed = <?php echo $orderDetail[0]->status; ?> 

// simulate activating a node

    if(completed == 2){
      completed = 3;
    }

    // count the number of completed nodes.
    completed = (completed === 0) ? 1 : completed + 2;

    if(completed == 6)
    {
      completed =7;
    }
    else if(completed == 7){
      completed = 9;
    }

    if (completed > children.length) {}
    
    // for each node that is completed, reflect the status
    // and show a green color!
    for (var i = 0; i < completed; i++) {
        children[i].children[0].classList.remove('grey');
        children[i].children[0].classList.add('green');
        
        // if this child is a node and not divider, 
        // make it shine a little more
        if (i % 2 === 0) {
            children[i].children[0].classList.add('activated');            
        }
    }
    

        </script>