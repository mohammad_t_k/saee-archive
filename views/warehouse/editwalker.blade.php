@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

<?php 
$layout = 'warehouse.layout';
if(Session::get('admin_role') == '9') {
    $layout = 'warehouse.cslayout';
}
else if(Session::get('admin_role') == '15') {
    $layout = 'warehouse.hr_layout';
}
?>
@extends($layout)


@section('content')

    <?php $walker = $walker[0]; ?>
    <?php $admin_id = Session::get('admin_id'); ?>

    <link rel="stylesheet" type="text/css" href="/styles/spinner.css">

    <!-- toggle library -->
    

    <style>
        #map_canvas {
            height: 300px;
            width: 100%;
            margin: 0;
        }

        #map_canvas .centerMarker {
            position: absolute;
            /*url of the marker*/
            background: url(../web/images/marker.png) no-repeat;
            /*center the marker*/
            top: 50%;
            left: 50%;
            z-index: 1;
            /*fix offset when needed*/
            margin-left: -10px;
            margin-top: -34px;
            /*size of the image*/
            height: 34px;
            width: 20px;
            cursor: pointer;
        }
    </style>
    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <!-- -->
        <div class="container">
            <h3>
                <div style="text-align: center; margin-top: 4%;">
                    <b>Captain ID <span style="">{{ $walker->id }}</span></b>
                </div>
            </h3>
            <hr>
            <div class="row">
                <!-- -->
                <form role="form" id="main-form" method="post" action="../warehouse/editwalkerpost" enctype="multipart/form-data">
                    <input id="latitude" name="latitude" required="required" type="hidden">
                    <input id="longitude" name="longitude" required="required" type="hidden">

                    <!-- left column -->
                    <div class="col-sm-3">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="col-sm-9 col-sm-offset-3 col-xs-12">
                                        <img id="img_uploadimage" src="{{ $walker->picture }}" class="zoomin avatar img-responsive img-thumbnail" alt="avatar" style="max-height: 50%; max-width: 50%; height: 50%; width: 50%;">
                                    </div>
                                    <div class="clearfix"></div>
                                    <label id="uploadphoto" class="btn btn-success btn-block" style="margin-top: 5%;">
                                        Upload Photo
                                        <input id="uploadimage" type="file" accept="image/*" class="form-control" name="photo" style="display: none !important;" onchange="upload_image(this, 'uploadphoto')">
                                    </label>
                                    <!-- spinner -->
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_uploadimage_spinner" style="display: none;">
                                        <br>
                                        <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                            <div class="spinner"></div>
                                        </div>
                                    </div>
                                    <br/><br/><br/>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div>
                            <table class="table table-striped">
                                <tr>
                                    <td><b style="margin-left: 10%;">Email</b></td>
                                    <td><div id="css_email" class="badge bg-{{ $walker->is_active ? 'green' : 'red' }}" style="margin-left: 40%;">{{ $walker->is_active ? 'Active' : 'Not Active' }}</div></td>
                                </tr>
                                <tr>
                                    <td><b style="margin-left: 10%;">Availability</b></td>
                                    <td><div id="css_availability" class="badge bg-{{ $walker->isJolly ? 'green' : 'red' }}" style="margin-left: 40%;">{{ $walker->isJolly ? 'Online' : 'Offline' }}</div></td>
                                </tr>
                                <tr>
                                    <td><b style="margin-left: 10%;">Status</b></td>
                                    <td><div id="css_status" class="badge bg-{{ $walker->is_approved ? 'green' : 'red' }}" style="margin-left: 40%;">{{ $walker->is_approved ? 'Approved' : 'Not Approved' }}</div></td>
                                </tr>
                            </table>
                            <br/><br/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-xs-12">

                            <!-- modal -->
                            <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <img src="" class="enlargeImageModalSource" style="width: 100%;">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- SCANS -->
                            <div class="container">

                                <div class="row">
                                    <div class="col-md-3"><small>{{ $walker->vehicle_registration_scan == '' ? 'NO VRHICLE REGISTRATION SCAN' : 'REGISTRATION' }}</small><img id="img_imagevrs" class="zoomin" style="width:100%; height:auto;" src="{{ $walker->vehicle_registration_scan }}"></div>
                                    <div class="col-md-3"><small>{{ $walker->vehicle_front_scan == '' ? 'NO VRHICLE FRONT SCAN' : 'FRONT' }}</small><img id="img_imagevfs" class="zoomin" style="width:100%; height:auto;" src="{{ $walker->vehicle_front_scan }}"></div>
                                    <div class="col-md-3"><small>{{ $walker->captain_iqama_scan == '' ? 'NO IQAMA SCAN' : 'IQAMA' }}</small><img id="img_imagevis" class="zoomin" style="width:100%; height:auto;" src="{{ $walker->captain_iqama_scan }}"></div>
                                    <div class="col-md-3"><small>{{ $walker->captain_license_scan == '' ? 'NO LICENSE SCAN' : 'LICENSE' }}</small><img id="img_imagecls" class="zoomin" style="width:100%; height:auto;" src="{{ $walker->captain_license_scan }}"></div>
                                </div>
                                <br/>
                                <div class="row">

                                    <!-- vehicle registration scan -->
                                    <div class="col-md-3">
                                        <label id="uploadvrs" class="btn btn-success btn-block">
                                            <i class="fa fa-upload"></i>
                                            <input id="imagevrs" type="file" accept="image/*" class="form-control" name="imagevrs" style="display: none !important;" onchange="upload_image(this, 'uploadvrs')">
                                        </label>
                                        <!-- spinner -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_imagevrs_spinner" style="display: none;">
                                            <br>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- vehicle fron scan -->
                                    <div class="col-md-3">
                                        <label id="uploadvfs" class="btn btn-success btn-block">
                                            <i class="fa fa-upload"></i>
                                            <input id="imagevfs" type="file" accept="image/*" class="form-control" name="imagevfs" style="display: none !important;" onchange="upload_image(this, 'uploadvfs')">
                                        </label>
                                        <!-- spinner -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_imagevfs_spinner" style="display: none;">
                                            <br>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- captain iqama scan -->
                                    <div class="col-md-3">
                                        <label id="uploadvis" class="btn btn-success btn-block">
                                            <i class="fa fa-upload"></i>
                                            <input id="imagevis" type="file" accept="image/*" class="form-control" name="imagevis" style="display: none !important;" onchange="upload_image(this, 'uploadvis')">
                                        </label>
                                        <!-- spinner -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_imagevis_spinner" style="display: none;">
                                            <br>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- captain license scan -->
                                    <div class="col-md-3">
                                        <label id="uploadcls" class="btn btn-success btn-block">
                                            <i class="fa fa-upload"></i>
                                            <input id="imagecls" type="file" accept="image/*" class="form-control" name="imagecls" style="display: none !important;" onchange="upload_image(this, 'uploadcls')">
                                        </label>
                                        <!-- spinner -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_imagecls_spinner" style="display: none;">
                                            <br>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <br/>

                                 <div class="row">
                                    <div class="col-md-3"><small>{{ $walker->captain_confession == '' ? 'NO CONFESSION' : 'CONFESSION' }}</small><img class="zoomin" id="img_confession" style="width:100%; height:auto;" src="{{ $walker->captain_confession }}"></div>
                                </div>
                                <br/>
                                <div class="row">
                                
                                    <!-- confession -->
                                    <div class="col-md-3">
                                        <label id="uploadconfession" class="btn btn-success btn-block">
                                            <i class="fa fa-upload"></i>
                                            <input id="confession" type="file" accept="image/*" class="form-control" name="confession" style="display: none !important;" onchange="upload_image(this, 'uploadconfession')">
                                        </label>
                                        <!-- spinner -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_confession_spinner" style="display: none;">
                                            <br>
                                            <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                <div class="spinner"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <style>
                                .zoomin {
                                    cursor: zoom-in;
                                }
                            </style>
                            <script>
                                $(function() {
                                    $('.zoomin').on('click', function() {
                                        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
                                        $('#enlargeImageModal').modal('show');
                                    });
                                });
                                function upload_image(element, label_id) {
                                    if(confirm("This image will start uploading instantly. Are you sure you want to upload this image?")) {
                                        $("#div_" + element.id + "_spinner").css('display', '');
                                        $("#" + label_id).css('display', 'none');
    
                                        // create form to include image in it
                                        var formData = new FormData();
                                        formData.append('file', $("#" + element.id)[0].files[0]);
                                        console.log(formData);
    
                                        $.ajax({
                                            type: 'POST',
                                            url: '/warehouse/editwalker/{{ $walker->id }}/upload/' + element.id,
                                            processData: false,
                                            contentType: false,
                                            enctype: 'multipart/form-data',
                                            cache: false,
                                            data: formData 
                                        }).done(function(response){ 
                                            console.log(response);
                                            $("#div_" + element.id + "_spinner").css('display', 'none');
                                            $("#" + label_id).css('display', '');
                                            if(!response.success) {
                                                alert(response.error);
                                            }
                                            else {
                                                var random = Math.floor(Math.random() * 1000);
                                                $("#img_" + element.id).attr('src', response.url + "?dummy=" + random);
                                            }
                                            $("#" + element.id).val(""); // remove value from file input to avoid re-uploading
                                        });
                                    }
                                }
                            </script>
                            <!-- END SCANS -->

                            <!-- start notes section -->
                            <div class="clearfix"></div>
                            <br><br><br>

                            <div style="text-align: center;">
                                <div class="col-xs-offset-4">
                                    <div style="float: left;">
                                        <h3>Notes &nbsp;</h3>
                                    </div>
                                    <div style="float: left;">
                                        <a data-target="#note_form_modal" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#note_form_modal">
                                            <i class="fa fa-plus-circle" style="font-size: 20px;"></i>
                                        </a>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                            @if(count($walker_notes))
                                <div>
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                                @foreach($walker_notes as $one_note)
                                                    <?php 
                                                    $one_note->note = str_replace("\n", '<br/>', $one_note->note); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p style="font-family: italic; text-align: center;">Posted by <b>{{ $one_note->username }}</b> On <b>{{ $one_note->created_at }}</b></p>
                                                            <p style="font-family: italic; text-align: center;">Date and Time of The Issue: <b>{{ $one_note->issue_timestamp }}</b> </p>
                                                            <hr>
                                                            <p dir="auto">{{ $one_note->note }}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif

                            <!-- start post form modal section -->
                            <div class="modal fade" id="note_form_modal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Post a Note on Captain {{ $walker->first_name }} {{ $walker->last_name }}</h4>
                                        </div>
                                        <div class="modal-body">

                                            <p style="color: red; font-weight: bold; text-align: center;">Please post full information about the issue. This action cannot be edited or undone.</p>
                                            <hr>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Date of The Issue </label>
                                                <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 ">
                                                    <input id="issue_date" name="issue_date" class="form-control col-md-7 col-xs-12" value="{{ date('Y-m-d') }}" type="date">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Time of The Issue </label>
                                                <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 ">
                                                    <input id="issue_time" name="issue_time" class="form-control col-md-7 col-xs-12" value="{{ date('H:i:s') }}" type="time">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Note </label>
                                                <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 ">
                                                    <textarea name="issue_note" id="issue_note" cols="30" rows="10" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_note_submit">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                                    <div id="submit" class="btn btn-success" onclick="post_walker_note()">Submit</div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- spinner -->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" id="div_note_spinner" style="display: none;">
                                                <br>
                                                <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-4">
                                                    <div class="spinner"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="clearfi"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                            <!-- end post form modal section -->

                            <script>
                                function post_walker_note() {
                                    var issue_date = $("#issue_date").val();
                                    var issue_time = $("#issue_time").val();
                                    var issue_note = $("#issue_note").val();
                                    if(!issue_note) {
                                        alert("You have to type a note");
                                        return false;
                                    }
                                    document.getElementById('div_note_submit').style.display = 'none';
                                    document.getElementById('div_note_spinner').style.display = '';
                                    $.ajax({
                                        type: 'POST',
                                        url: '/warehouse/editcaptain/note/{{ $walker->id }}',
                                        dataType: 'text',
                                        data: { issue_date: issue_date, issue_time: issue_time, issue_note: issue_note }
                                    }).done(function (response) {
                                        console.log(response);
                                        responseObj = JSON.parse(response);
                                        document.getElementById('div_note_submit').style.display = '';
                                        document.getElementById('div_note_spinner').style.display = 'none';
                                        if(responseObj.success) {
                                            alert(responseObj.message);
                                            new Promise((resolve) => setTimeout(resolve, 200)).then(() => {
                                                $("#note_form_modal").modal('hide');
                                                location.reload();
                                            });
                                        }
                                        else {
                                            alert(responseObj.error);
                                        }
                                    });
                                }
                            </script>

                            <!-- end notes section -->

                        </div>
                    </div>
                    <!-- --->

                    <div class="col-md-8 col-md-offset-1 personal-info">

                        <div class="clearfix"></div>
                        <div style="margin-left: 10%; margin-top: 2%; margin-bottom: 1%;">
                            <div class="disable">
                                <input  type="checkbox" id="checkbox_activation" data-toggle="toggle" data-on="Activate Email" data-off="Deactivate Email" data-onstyle="success" data-offstyle="danger" onchange="activation()" <?php echo (!$walker->is_active ? 'checked' : ''); ?> >
                            </div>
                            <div class="disable">
                                <input type="checkbox" id="checkbox_on_off_" data-toggle="toggle" data-on="Make Online" data-off="Make Offline" data-onstyle="success" data-offstyle="danger" onchange="on_off_()" <?php echo (!$walker->isJolly ? 'checked' : ''); ?> >
                            </div>
                            <div class="disable">
                                <input type="checkbox" id="checkbox_approve_walker" data-toggle="toggle" data-on="Approve" data-off="Disapprove" data-onstyle="success" data-offstyle="danger" onchange="approve_walker()" <?php echo (!$walker->is_approved ? 'checked' : ''); ?> >
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <!-- spinner -->
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_checkboxes_spinner" style="display: none;">
                                <br/>
                                <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-2">
                                    <div class="spinner"></div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function activation(){
                                document.getElementById('div_checkboxes_spinner').style.display = '';
                                var checkboxes = document.getElementsByClassName('disable');
                                for(i = 0; i < checkboxes.length; ++i) {
                                    checkboxes[i].style.display = 'none';
                                }
                                $.ajax({
                                    type: 'get',
                                    url: '/warehouse/editcaptain/activation/{{ $walker->id }}',
                                }).done(function (response) {
                                    console.log(response);
                                    document.getElementById('div_checkboxes_spinner').style.display = 'none';
                                    for(i = 0; i < checkboxes.length; ++i) {
                                        checkboxes[i].style.display = '';
                                    }
                                    var str = $("#css_email").text();
                                    if(str == 'Active'){
                                        $("#css_email").removeClass('badge bg-green').addClass('badge bg-red');
                                        $("#css_email").html('Not Active');
                                    }
                                    else {
                                        $("#css_email").removeClass('badge bg-red').addClass('badge bg-green');
                                        $("#css_email").html('Active');
                                    }
                                });
                            }
                            function on_off_(){
                                document.getElementById('div_checkboxes_spinner').style.display = '';
                                var checkboxes = document.getElementsByClassName('disable');
                                for(i = 0; i < checkboxes.length; ++i) {
                                    checkboxes[i].style.display = 'none';
                                }
                                $.ajax({
                                    type: 'get',
                                    url: '/warehouse/editcaptain/onlineoffline/{{ $walker->id }}',
                                }).done(function (response) {
                                    console.log(response);
                                    document.getElementById('div_checkboxes_spinner').style.display = 'none';
                                    for(i = 0; i < checkboxes.length; ++i) {
                                        checkboxes[i].style.display = '';
                                    }
                                    var str = $("#css_availability").text();
                                    if(str == 'Online'){
                                        $("#css_availability").removeClass('badge bg-green').addClass('badge bg-red');
                                        $("#css_availability").html('Offline');
                                    }
                                    else {
                                        $("#css_availability").removeClass('badge bg-red').addClass('badge bg-green');
                                        $("#css_availability").html('Online');
                                    }
                                });
                            }
                            function approve_walker(){
                                document.getElementById('div_checkboxes_spinner').style.display = '';
                                var checkboxes = document.getElementsByClassName('disable');
                                for(i = 0; i < checkboxes.length; ++i) {
                                    checkboxes[i].style.display = 'none';
                                }
                                $.ajax({
                                    type: 'get',
                                    url: '/warehouse/editcaptain/approve/{{ $walker->id }}',
                                }).done(function (response) {
                                    responseObj = response;
                                    console.log(responseObj);
                                    document.getElementById('div_checkboxes_spinner').style.display = 'none';
                                    for(i = 0; i < checkboxes.length; ++i) {
                                        checkboxes[i].style.display = '';
                                    }
                                    if(!responseObj.success) {
                                        alert(responseObj.error);
                                        location.reload();
                                        return;
                                    }
                                    alert(responseObj.message);
                                    var str = $("#css_status").text();
                                    if(str == 'Approved'){
                                        $("#css_status").removeClass('badge bg-green').addClass('badge bg-red');
                                        $("#css_status").html('Not Approved');
                                    }
                                    else {
                                        $("#css_status").removeClass('badge bg-red').addClass('badge bg-green');
                                        $("#css_status").html('Approved');
                                    }
                                });
                            }
                        </script>
                        <div class="clearfix"></div>

                        <!------------------------------------------------------------------------ Personal Inforamtion ------------------------------------------------------------------------>
                        <br/>
                        <label class="control-label col-md-4 col-sm-3 col-xs-12"><h2><span class="glyphicon glyphicon-user"></span> &nbsp; <b>Personal Inforamtion</b> </h2> </label>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">First Name </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="firs_name" name="first_name" class="form-control col-md-7 col-xs-12" value="{{ $walker->first_name }}" placeholder="First Name" type="text" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Last Name </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="last_name" name="last_name" class="form-control col-md-7 col-xs-12" value="{{ $walker->last_name }}"  placeholder="Last Name"type="text" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Email </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="email" name="email" class="form-control col-md-7 col-xs-12" value="{{ $walker->email }}" placeholder="Email" type="text" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label id="label_password" class="control-label col-md-2 col-sm-3 col-xs-12">Password <a data-toggle="tooltip" title="click here to activate"><i class="glyphicon glyphicon-info-sign"></i></a></label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="password" name="password" class="form-control col-md-7 col-xs-12" placeholder="Reset Password" type="password" maxlength="200" disabled>
                                <script>
                                    $("#label_password").click(function(){
                                        $("#password").prop("disabled", false);
                                    });
                                    $(document).ready(function(){
                                        $('[data-toggle="tooltip"]').tooltip();
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Phone </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="phone" name="phone" class="form-control col-md-7 col-xs-12" value="{{ $walker->phone }}" placeholder="966 555555555" type="text" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">City </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <select name="city" class="form-control">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->name }}" {{ $walker->city == $city->name ? 'selected' : '' }}>{{ $city->name }} - {{ $city->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Date of Birth </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <?php
                                $newdate = $walker->date_of_birth;
                                $date = mb_split('-', $walker->date_of_birth);
                                if(count($date) == 3){
                                    $date[0] = strlen($date[0]) < 2 ? '0'.$date[0] : $date[0];
                                    $date[1] = strlen($date[1]) < 2 ? '0'.$date[1] : $date[1];
                                    $date[2] = strlen($date[2]) < 2 ? '0'.$date[2] : $date[2];
                                }
                                if(count($date) == 3 && strlen($date[2]) > 3)
                                    $newdate = $date[2].'-'.$date[1].'-'.($date[0]);
                                $walker->date_of_birth = $newdate;
                                ?>
                                <input id="bdate" name="bdate" class="form-control col-md-7 col-xs-12" type="date" value = "{{ $walker->date_of_birth }}" placeholder="Date of Birth" maxlength="200">
                            </div>
                            <h5>{{ $newdate }}</h5>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Government ID Number </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="gid" name="gid" class="form-control col-md-7 col-xs-12" value="{{ $walker->government_id_number }}" placeholder="Government ID Number" type="text" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Is Currently Providing </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <b>{{ $iscurrentlyproviding == 0 ? 'No' : 'Yes' }}</b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">WASL Captain Number </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <b>{{ $walker->waslReferenceNumber != 0 ? $walker->waslReferenceNumber: '' }}</b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Service Type </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <b>
                                    @foreach($type as $types)
                                        <?php
                                        $ar = array();
                                        foreach ($ps as $pss) {
                                            $ser = ProviderType::where('id', $pss->type)->first();
                                            if ($ser)
                                                $ar[] = $ser->name;
                                        }
                                        $servname = $types->name;
                                        if (!empty($ar)) {
                                            if (in_array($servname, $ar))
                                                echo $types->name;
                                        }
                                        ?>
                                    @endforeach
                                </b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>

                        <!------------------------------------------------------------------------ Vehicle Details ------------------------------------------------------------------------>
                        <br/>
                        <div class="clearfix"></div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><h2><span class="fa fa-car"></span> &nbsp; <b>Vehicle Details</b> </h2></label>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <?php
                            $newdate = $walker->license_expiry_date;
                            $date = mb_split('-', $walker->license_expiry_date);
                            $flag = false;
                            if(count($date) == 3){
                                $flag = true;
                                $date[0] = strlen($date[0]) < 2 ? '0'.$date[0] : $date[0];
                                $date[1] = strlen($date[1]) < 2 ? '0'.$date[1] : $date[1];
                                $date[2] = strlen($date[2]) < 2 ? '0'.$date[2] : $date[2];
                                if(strlen($date[2]) > 3)
                                    $newdate = $date[2].'-'.$date[1].'-'.$date[0];
                                $date[2] = (int)$date[2];
                                $date[1] = (int)$date[1];
                                $date[0] = (int)$date[0];
                                $day = $date[2] > 1000 ? $date[0] : $date[2];
                                $month = $date[1];
                                $year = $date[2] > 1000 ? $date[2] : $date[0];
                            }
                            $walker->license_expiry_date = $newdate;
                            ?>
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">License Expiry Date </label>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="ledday" class="form-control">
                                    <option value="day" {{ $flag ? '' : 'selected' }}>Day</option>
                                    @for($i = 1; $i < 32; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $day ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="ledmonth" class="form-control">
                                    <option value="month" {{ $flag ? '' : 'selected' }}>Month</option>
                                    @for($i = 1; $i < 13; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $month ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="ledyear" class="form-control">
                                    <option value="year" {{ $flag ? '' : 'selected' }}>Year</option>
                                    @for($i = 1400; $i < 1601; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $year ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <h5>{{ $newdate }}</h5>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <?php
                            $newdate = $walker->car_registration_expiry;
                            $date = mb_split('-', $walker->car_registration_expiry);
                            $flag = false;
                            if(count($date) == 3){
                                $flag = true;
                                $date[0] = strlen($date[0]) < 2 ? '0'.$date[0] : $date[0];
                                $date[1] = strlen($date[1]) < 2 ? '0'.$date[1] : $date[1];
                                $date[2] = strlen($date[2]) < 2 ? '0'.$date[2] : $date[2];
                                if(strlen($date[2]) > 3)
                                    $newdate = $date[2].'-'.$date[1].'-'.$date[0];
                                $date[2] = (int)$date[2];
                                $date[1] = (int)$date[1];
                                $date[0] = (int)$date[0];
                                $day = $date[2] > 1000 ? $date[0] : $date[2];
                                $month = $date[1];
                                $year = $date[2] > 1000 ? $date[2] : $date[0];
                            }
                            $walker->car_registration_expiry = $newdate;
                            ?>
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Vehicle Registeration Expiry </label>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="vreday" class="form-control">
                                    <option value="day" {{ $flag ? '' : 'selected' }}>Day</option>
                                    @for($i = 1; $i < 32; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $day ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="vremonth" class="form-control">
                                    <option value="month" {{ $flag ? '' : 'selected' }}>Month</option>
                                    @for($i = 1; $i < 13; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $month ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <select name="vreyear" class="form-control">
                                    <option value="year" {{ $flag ? '' : 'selected' }}>Year</option>
                                    @for($i = 1400; $i < 1601; ++$i)
                                        <option value="{{ $i }}" {{ $flag && $i == $year ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <h5>{{ $newdate }}</h5>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Car Type </label>

                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <select name="car_type" class="form-control">
                                    <option value="">Select Car Type</option>
                                    @foreach($car_types as $carType)
                                        <option value="{{ $carType->id }}" {{ $walker->carType_id == $carType->id ? 'selected' : '' }}>{{ $carType->english }}
                                            - {{ $carType->arabic }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Car Model </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="car_model" name="car_model" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->car_model != '0' ? $walker->car_model : '' }}" placeholder="Car Model" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Vehicle Sequence Number </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="vsnumber" name="vsnumber" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->vehicle_sequence_number }}" placeholder="Vehicle Sequence Number" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Vehicle Number Plate </label>
                            <div class="col-md-3 col-sm-2 col-xs-12 ">
                                <input id="vnplate_numbers" name="vnplate_numbers" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->car_number_plate_number }}" placeholder="Plate Numbers" maxlength="200">
                            </div>
                            <div class="col-md-3 col-sm-2 col-xs-12 ">
                                <input id="vnplate_letters" name="vnplate_letters" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->car_number_plate_letter_1 . ' ' .  $walker->car_number_plate_letter_2 . ' ' .$walker->car_number_plate_letter_3 . ' ' }}" placeholder="Plate Letters" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Plate Type </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <select name="plate_type" id="plate_type" class="form-control">
                                    <option value="0" <?php echo ($walker->plate_type == '0' ? 'selected' : ''); ?> >Public </option>
                                    <option value="1" <?php echo ($walker->plate_type == '1' ? 'selected' : ''); ?> >Private </option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">WASL Vehicle Number </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <b>{{ $walker->vehicleReferenceNumber != "0" ? $walker->vehicleReferenceNumber : '' }}</b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>

                        <!------------------------------------------------------------------------ Banking Details ------------------------------------------------------------------------>
                        <br/>
                        <div class="clearfix"></div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><h2><span class="glyphicon glyphicon-credit-card"></span> &nbsp; <b>Banking Details</b> </h2></label>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Bank Name </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <select name="bank_id" id="bank_id" class="form-control">
                                    <option value="0" <?php echo ($bankname == '' ? 'selected' : ''); ?> >Not Specified </option>
                                    @foreach($banks as $bank)
                                        <option value="{{$bank->id}}" <?php echo ($bank->name == $bankname ? 'selected' : ''); ?> >{{$bank->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Account Name </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="account_name" name="account_name" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->account_name }}" placeholder="Account Name" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Account Number </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="account_number" name="account_number" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->account_number }}" placeholder="Account Number" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">IBAN </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="iban" name="iban" class="form-control col-md-7 col-xs-12" type="text" value="{{ $walker->iban }}" placeholder="IBAN" maxlength="200">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>

                        <!------------------------------------------------------------------------ Shipments Details ------------------------------------------------------------------------>
                        <br/>
                        <div class="clearfix"></div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><h2><span class="fa fa-table"></span> &nbsp; <b>Shipments Details</b> </h2></label>
                        <div class="clearfix"></div>
                        <br/>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Maximum Shipment </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="max_shipments" name="max_shipments" class="form-control col-md-7 col-xs-12" value="{{ $walker->max_shipments }}" type="text" maxlength="200" placeholder="Maximum Shipment">
                            </div>
                            <label><input type="checkbox" class="checkbox-inline form-check" name="maxcheckbox" value="true">&nbsp;<span class="outside"><span class="inside"></span></span>for all captains in this captain's city? </label><br/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Minimum Shipment </label>
                            <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                <input id="min_shipments" name="min_shipments" class="form-control col-md-7 col-xs-12" value="{{ $walker->min_shipments }}" type="text" maxlength="20" placeholder="Minimum Shipment">
                            </div>
                            <label><input type="checkbox" class="checkbox-inline form-check" name="mincheckbox" value="true">&nbsp;<span class="outside"><span class="inside"></span></span>for all captains in this captain's city?</label><br/>
                        </div>
                        <div class="clearfix"></div>
                        <br/>

                        <!------------------------------------------------------------------------ Update Location ------------------------------------------------------------------------>
                        <br/>
                        <div class="clearfix"></div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><h2><span class="fa fa-map-marker"></span> &nbsp; <b>Update Location</b> </h2></label>
                        <div class="clearfix"></div>
                        <div class=" col-md-12 col-lg-12 col-xs-12">
                            <div class="col-md-11 col-xs-12">
                                <div id="map_canvas" style="height: 500px; width: 100%; margin: 10px;"></div>
                                <input id="address_latitude" name="address_latitude" hidden />
                                <input id="address_longitude" name="address_longitude" hidden />
                                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
                                <script>
                                    function initMap() {
                                        try {
                                            var walker = JSON.parse('<?php echo $walker?>');
                                            var address_latitude=walker.address_latitude;
                                            var address_longitude=walker.address_longitude;
                                            var trackingnum = '';
                                            var mapOptions = {
                                                zoom: 15,
                                                center: new google.maps.LatLng(address_latitude, address_longitude),
                                            };
                                            document.getElementById('map_canvas').style = 'height:200px';
                                            var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                                            infoWindow = new google.maps.InfoWindow;
                                            google.maps.event.addListener(map, 'center_changed', function () {
                                                address_latitude= document.getElementById('address_latitude').value = map.getCenter().lat();
                                                address_longitude= document.getElementById('address_longitude').value = map.getCenter().lng();
                                            });
                                            $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                                            //do something onclick
                                                .click(function () {
                                                    var that = $(this);
                                                    if (!that.data('win')) {
                                                        that.data('win', new google.maps.InfoWindow({
                                                            content: 'this is the center'
                                                        }));
                                                        that.data('win').bindTo('position', map, 'center');
                                                    }
                                                    that.data('win').open(map);
                                                });
                                        }
                                        catch (e) {
                                            alert(e);
                                        }
                                    }
                                </script>
                                <script>google.maps.event.addDomListener(window, 'load', initMap);</script>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <!------------------------------------------------------------------------ Submit Form ------------------------------------------------------------------------>
                        <br/>
                        <div class="clearfix"></div>
                        <div class="col-md-8 col-sm-9 col-xs-12 form-group">
                            <button type="submit" class="btn btn-primary btn-flat btn-block">Update Captain</button>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $walker->id }}">
                    </div>
                </form>
                <!-- -->

            </div>
        </div>
        <!-- -->

    </div>


    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>


    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>




@stop

