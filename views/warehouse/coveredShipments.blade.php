@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
    @endif
    <?php
    if (Session::get('admin_role') == 9)
        $layout = 'warehouse.cslayout';
    else if (Session::get('admin_role') == 12)
        $layout = 'warehouse.inroutelayout';
    else if (Session::get('admin_role') == 15)
        $layout = 'warehouse.hr_layout';
    else
        $layout = 'warehouse.layout';
    ?>

    @extends($layout)

    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Covered Shipments</h3>
                </div>

            </div>
            <div class="col-md-12 col-sm-12">

                <form method="post" action="/warehouse/shipmentscoveredpost">

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-4 col-sm-6 col-xs-12">From <span
                                    class="required">*</span>
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input id="fromdate" name="fromdate"
                                   value="{{$fromdate}}" class="date-picker form-control col-md-7 col-xs-12"
                                   required="required" type="date">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-4 col-sm-6 col-xs-12">To <span
                                    class="required">*</span>
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input id="todate" name="todate"
                                   value="{{$todate}}" class="date-picker form-control col-md-7 col-xs-12"
                                   required="required" type="date">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                    class="required">*</span>
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="city" id="city" class="form-control">
                                @foreach($adminCities as $adcity)
                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="company" id="company" class="form-control">
                                    <option value="All">All</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->company_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="hub" id="hub" class="form-control">
                                    @foreach($adminHubs as $adhub)
                                        <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <p>
                                <button id="submitForm" class="btn btn-success">Filter</button>
                            </p>
                        </div>
                    </div>


                </form>

            </div>

            <div class="clearfix"></div>

            <!-- /top tiles -->

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="row">

                <div class="tab_container">
                    <label id="tab1"
                           style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;;background: #fff;box-shadow: inset 0 3px #2a3f54;"
                           onclick=Accounting()><span>Money Received</span></label>
                    <label id="tab2"
                           style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;background: #f0f0f0;"
                           onclick=allCoveredMoney()><span>Money not Received</span></label>
                    <!-----------------------------------  section 1-------------------->
                    <section id="content1" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="row">
                                <!-- table start -->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>covered Shipments</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <table id="covered_money_received"
                                                   class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Hub</th>
                                                    <th>Admin</th>
                                                    <th>City</th>
                                                    <th>Covered Amount</th>
                                                    <th>Captain ID</th>
                                                    <th>Captain</th>
                                                    <th>Received Money Date</th>
                                                </tr>
                                                </thead>

                                                <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Hub</th>
                                                    <th>Admin</th>
                                                    <th>City</th>
                                                    <th>Covered Amount</th>
                                                    <th>Captain ID</th>
                                                    <th>Captain</th>
                                                    <th>Received Money Date</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!-----------------------------------  section 2-------------------->
                    <section id="content2" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="row">
                                <!-- table start -->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>covered Shipments</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <table id="covered_money_not_received"
                                                   class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Hub</th>
                                                    <th>City</th>
                                                    <th>Covered Amount</th>
                                                    <th>Captain ID</th>
                                                    <th>Captain</th>
                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Hub</th>
                                                    <th>City</th>
                                                    <th>Covered Amount</th>
                                                    <th>Captain ID</th>
                                                    <th>Captain</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>


                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <script>

        <?php

        $covered_received_shipments = '';
        $covered_not_received_money = '';

        foreach ($received_covered_orders as $order) {
                $captain = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
                $received_money_date=strtotime($order->date);
                $received_money_date = date("Y-m-d",$received_money_date);
                if($order->hub_name !='All'){
                $hub_name =$order->hub_name;
                } else{
                $hub_name ='';
                }

                $username = explode("@", $order->username);
                $admin = $username[0];
            $covered_received_shipments .= "[ \"$order->jollychic</a>\",\"$order->company_name\",\"$hub_name\",\"$admin\",\"$order->d_city\", \"$order->cash_on_delivery\",\"$order->captain_id\", \"$captain\",\"$received_money_date\"], ";
        }

        foreach ($notreceived_covered_orders as $order) {
                $captain = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
                if($order->hub_name !='All'){
                $hub_name =$order->hub_name;
                } else{
                $hub_name ='';
                }

            $covered_not_received_money .= "[ \"$order->jollychic</a>\",\"$order->company_name\",\"$hub_name\",\"$order->d_city\",\"$order->cash_on_delivery\",\"$order->captain_id\", \"$captain\"], ";
        }

        ?>

        $(document).ready(function () {
                    document.getElementById("content1").style.display = "block";
                    document.getElementById("content2").style.display = "none";
                    $("#covered_money_received").DataTable({

                        "data": [
                            <?php echo $covered_received_shipments ?>

                        ],
                        "autoWidth": false,

                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,

                        'order': [[0, 'asc']]
                    });

                    $("#covered_money_not_received").DataTable({

                        "data": [
                            <?php echo $covered_not_received_money ?>

                        ],
                        "autoWidth": false,

                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,

                        'order': [[0, 'asc']]
                    });

                });


        function Accounting() {
            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none";
            $('#tab1').css({
                'font-size': '18px',
                'display': 'block',
                'float': 'left',
                'padding': '1.5em',
                'color': '#757575',
                'background': '#fff',
                'box-shadow': 'inset 0 3px #2a3f54'
            });
            $('#tab2').css({
                'font-size': '18px',
                'display': 'block',
                'float': 'left',
                'padding': '1.5em',
                'color': '#757575',
                'background': '#f0f0f0',
                'box-shadow': 'none'
            });

        }

        function allCoveredMoney() {
            document.getElementById("content2").style.display = "block";
            document.getElementById("content1").style.display = "none";
            $('#tab2').css({
                'font-size': '18px',
                'border-top': '5px solid #f7f7f7;',
                'display': 'block',
                'float': 'left',
                'padding': '1.5em',
                'color': '#757575',
                'background': '#fff',
                'box-shadow': 'inset 0 3px #2a3f54'
            });
            $('#tab1').css({
                'font-size': '18px',
                'display': 'block',
                'float': 'left',
                'padding': '1.5em',
                'color': '#757575',
                'background': '#f0f0f0',
                'box-shadow': 'none'
            });
        }

    </script>


    @stop
