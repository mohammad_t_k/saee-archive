@if( Auth::user()->role < 4)
    <script>window.location = "/warehouse/403";</script>
@endif
    <?php
if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>
@extends($layout)
@section('content')

    <link rel="stylesheet" type="text/css" href="/styles/spinner.css">
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3> Step 1: New Shipments</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Assign a date to an incoming shipment</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown">
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" class="form-control col-md-7 col-xs-12" required="required" type="text" >
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city_id" id="city_id" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city_id}}"<?php if (isset($city_id) && $city_id == $adcity->city_id) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Shipment <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="shipmentDate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="date">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Scheduled Dispatch <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="dispatchDate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="date">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="15"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_submit">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Submit</button>
                                        </p>
                                    </div>
                                </div>
                                <!-- spinner -->
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner" style="display: none;">
                                    <br/>
                                    <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-3">
                                        <div class="spinner"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="unknown_failure_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="unknown_failure_msg"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="general_success" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                    <h4 class="text-center"><span id="general_success_msg"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="failed_items_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_faileditems" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Reason</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="success_items_section" style="display: none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown"></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_successitems" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Company</th>
                                                <th>Order Number</th>
                                                <th>Order Reference</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Hub</th>
                                                <th>District</th>
                                                <th>Receiver Name</th>
                                                <th>Receiver Mobile</th>
                                                <th>Receiver Mobile 2</th>
                                                <th>Cash On Delivery</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>


        $(document).ready(function () {

            $("#submitForm").click(function () {
                    var company = document.getElementById("company").value;
                    var city_id = document.getElementById("city_id").value;
                    var shipDate = document.getElementById("shipmentDate").value;
                    var dispatchDate = document.getElementById("dispatchDate").value;
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

                    if (shipDate == '') {
                        bootbox.alert('Please enter date!');
                        return;
                    }

                    if (dispatchDate == '') {
                        bootbox.alert('Please enter dispatch date!');
                        return;
                    }
                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }


                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = ids[i].replace(/ /g, '');


                        if (ids[i].length > 1) {
                            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push(ids[i]);
                            }
                            else {
                                /*if (/[0-9]{8}KS/.test(ids[i])) {
                                    correctIDs.push('JC' + ids[i]);
                                    correctedIDs.push(ids[i]);
                                }
                                else if (/C[0-9]{8}KS/.test(ids[i])) {
                                    correctIDs.push('J' + ids[i]);
                                    correctedIDs.push(ids[i]);
                                } */

                                wrongIDs.push(ids[i]);
                            }
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /* if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    } */

                    if (finalArray.length) {
                        if (confirm("Are you sure you want set the shipment date to " + shipDate + "?") == true) {

                            $("#success_items_section").css("display", "none");
                            $("#failed_items_section").css("display", "none");
                            $("#unknown_failure_section").css("display", "none");
                            $("#general_success").css("display", "none");
                            $("#div_submit").css('display', 'none');
                            $("#div_spinner").css('display', '');

                            $.ajax({
                                type: 'POST',
                                url: '/warehouse/setshipmentdate',
                                dataType: 'text',
                                data: {date: shipDate,company: company,city_id: city_id, dispatchdate: dispatchDate, ids: finalArray.join(",")},
                            }).done(function (response) {

                                console.log(response);
                                $("#div_submit").css('display', '');
                                $("#div_spinner").css('display', 'none');
                                responseObj = JSON.parse(response);
                                if(responseObj.error_message)
                                    alert(responseObj.error_message);
                                responseObj = JSON.parse(response);
                                if (responseObj.success) {
                                    if(responseObj.general_success_msg) {
                                        $("#general_success_msg").text(responseObj.message);
                                        $("#general_success").css('display', '');
                                    }
                                    else {
                                        alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                                        $("#datatable_successitems").DataTable().destroy();
                                        if(responseObj.success_items.length) {
                                            let rows = [];
                                            responseObj.success_items.forEach(function(element) {
                                                if(!element.order_reference)
                                                    element.order_reference = ''
                                                if(!element.d_city)
                                                    element.d_city = '';
                                                if(!element.receiver_name)
                                                    element.receiver_name = '';
                                                if(!element.receiver_phone)
                                                    element.receiver_phone = '';
                                                if(!element.receiver_phone2)
                                                    element.receiver_phone2 == '';
                                                if(element.hub_name == 'All')
                                                    element.hub_name = '';
                                                if(element.porta_hub_name == 'All')
                                                    element.porta_hub_name = '';
                                                if(element.sub_status_code) 
                                                    element.status_name += '/' + element.sub_status_code;
                                                rows.push([
                                                    element.jollychic,
                                                    element.company_name,
                                                    element.order_number,
                                                    element.order_reference,
                                                    element.main_city,
                                                    element.d_city,
                                                    element.hub_name,
                                                    element.d_district,
                                                    element.receiver_name,
                                                    element.receiver_phone,
                                                    element.receiver_phone2,
                                                    element.cash_on_delivery,
                                                    element.status_name
                                                ]);
                                            });
                                            $("#datatable_successitems").DataTable({
                                                "data": rows,
                                                "autoWidth": false,
                                                dom: "Blfrtip",
                                                buttons: [
                                                    {
                                                        extend: "copy",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "csv",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "excel",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "pdfHtml5",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "print",
                                                        className: "btn-sm"
                                                    },
                                                ],
                                                responsive: true,
                                                'order': [[0, 'asc']]
                                            });
                                            $("#success_items_section").css("display", "");
                                        }
                                        if(responseObj.failed_items.length) {
                                            $("#datatable_faileditems").DataTable().destroy();
                                            let rows = [];
                                            responseObj.failed_items.forEach(function(element) {
                                                rows.push([element.waybill, element.error]);
                                            });
                                            $("#datatable_faileditems").DataTable({
                                                "data": rows,
                                                "autoWidth": false,
                                                dom: "Blfrtip",
                                                buttons: [
                                                    {
                                                        extend: "copy",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "csv",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "excel",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "pdfHtml5",
                                                        className: "btn-sm"
                                                    },
                                                    {
                                                        extend: "print",
                                                        className: "btn-sm"
                                                    },
                                                ],
                                                responsive: true,
                                                'order': [[0, 'asc']]
                                            });
                                            $("#failed_items_section").css("display", "");
                                        }
                                    }
                                }
                                else if(responseObj.error_code && responseObj.error_code == 2) {
                                    $("#unknown_failure_section").css('display', '');
                                    $("#unknown_failure_msg").html(responseObj.error);
                                }
                                else
                                    alert('Something went wrong!');

                                // console.log(response);
                                // if (response) {
                                //     bootbox.alert(JSON.parse(response).number_of_updated_items + ' items have been updated. You can start planning now.');
                                // }
                                // else {
                                //     bootbox.alert('Something went wrong!');
                                // }
                            }); // Ajax Call
                            
                            // update districts
                            // $.ajax({
                            //     type: 'POST',
                            //     url: '/warehouse/updateDistricts',
                            //     dataType: 'text',
                            //     data: {date: shipDate,company: company,city: city, dispatchdate: dispatchDate, ids: finalArray.join(",")},
                            // }).done(function (response) {
                            //     console.log('update districts: ', response);
                            // }); 

                        }
                    }
                    else {
                        bootbox.alert('Nothing to signin!');
                    }
                }
            );
        });

    </script>
    @stop

