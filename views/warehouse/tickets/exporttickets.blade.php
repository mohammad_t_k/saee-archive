@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

<?php
if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 16)
    $layout = 'warehouse.viewlayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<style>
    table thead tr th {
        text-align: center;
    }
    table tfoot tr th {
        text-align: center;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <div class="right_col" role="main">
        <div class="container">

            <br/><br/>


            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="tab_container">

                <input id="tab1" type="radio" name="tabs" <?php if ($tabFilter == 1) echo 'checked' ?>>
                <label for="tab1" class='label2'><i class="fa fa-ticket"></i><span>Export Single Ticket</span></label>

                <input id="tab3" type="radio" name="tabs" <?php if ($tabFilter == 3) echo 'checked' ?>>
                <label for="tab3" class='label2'><i class="fa fa-pencil-square-o"></i><span> Export Specific Tickets</span></label>

                <input id="tab2" type="radio" name="tabs" <?php if ($tabFilter == 2) echo 'checked' ?>>
                <label for="tab2" class='label2'><i class="	fa fa-level-down"></i><span> Export Tickets</span></label>

                <section id="content1" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Export Specific Ticket by Waybill</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form action="/warehouse/ticket/export/single" method="GET">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="1">

                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ticket ID <span class="required">*</span></label>

                                                <div class="col-md-2 col-sm-6 col-xs-12">
                                                    <input type="text" id="ticket_id" name="ticket_id" required="required" class="form-control">
                                                </div>
                                                <div class="theCount">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 10px;">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                            <p>
                                                                <input type="submit" id="submitForm" value="Search" class="btn btn-success" />
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <table id="datatable_ticket" class="table table-striped table-bordered " style="text-align: center;">
                                            <thead>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </thead>

                                            <tfoot>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <section id="content3" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Export Tickets</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form action="/warehouse/ticket/export/bulk" method="POST">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="3">

                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tickets IDs <span class="required">*</span></label>

                                                <div class="col-md-2 col-sm-6 col-xs-12">
                                                    <textarea name="ticket_ids" id="ticket_ids" cols="25" rows="15" required="required" class="form-control"></textarea>
                                                </div>
                                                <div class="theCount">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 10px;">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                            <p>
                                                                <input type="submit" id="submitIdsForm" value="Submit" class="btn btn-success" />
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <table id="datatable_bulktickets" class="table table-striped table-bordered " style="text-align: center;">
                                            <thead>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </thead>

                                            <tfoot>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <section id="content2" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Filter Tickets</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <form action="/warehouse/ticket/export/filter" method="get">
                                                    <input id="tabFilter" name='tabFilter' type="hidden" value="2">
                                                    <div class="row">

                                                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company</label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select id="company" name="company" required="required" class="form-control" type="text">
                                                                    <option value="all">All Companies</option>
                                                                    @foreach($companies as $compan)
                                                                        <option value="{{$compan->id}}" <?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->company_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City </label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select name="city" id="city" class="form-control">
                                                                    @foreach($adminCities as $adcity)
                                                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Status </label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select name="status" id="status" class="form-control">
                                                                    <option value="all">All</option>
                                                                    @foreach($statuses as $stat)
                                                                        <option value="{{ $stat->id }}" <?php if (isset($status) && $status == $stat->id) echo 'selected';?>>{{ $stat->english }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12"> Created At </label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <input type="date" name="created_at" id="created_at" class="form-control col-md-4 col-sm-6 col-xs-12" value="{{ isset($created_at) ? $created_at : '' }}" /> 
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Priority</label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select name="priority" id="priority" class="form-control">
                                                                    <option value="all">All</option>
                                                                    <option value="Low" {{ isset($priority) && $priority == 'Low' ? 'selected' : '' }}>Low</option>
                                                                    <option value="Medium" {{ isset($priority) && $priority == 'Medium' ? 'selected' : '' }}>Medium</option>
                                                                    <option value="High" {{ isset($priority) && $priority == 'High' ? 'selected' : '' }}>High</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Undelivered Reason </label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select name="reason" id="reason" class="form-control">
                                                                    <option value="all" selected>All</option>
                                                                    @foreach($reasons as $reas)
                                                                        <option value="{{ $reas->id }}" <?php if (isset($reason) && $reason != 'all' && $reason == $reas->id) echo 'selected';?> >{{ $reas->english }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Ticket Type </label>
                                                            <div class="col-md-8 col-sm-6 col-xs-12">
                                                                <select name="selected_types[]" id="selected_types" class="form-control" multiple>
                                                                    @foreach($ticket_types as $one_type)
                                                                        <option value="{{ $one_type->id }}" <?php if (!isset($selected_types) && $one_type->id != 8 || isset($selected_types) && in_array($one_type->id, $selected_types)) echo 'selected';?> >{{ $one_type->english }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-5 form-group">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <p>
                                                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <table id="datatable_tickets" class="table table-striped table-bordered " style="text-align: center;">
                                            <thead>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </thead>

                                            <tfoot>
                                            <tr>
                                                <th>Created At</th>
                                                <th>Waybill</th>
                                                <th>Priority</th>
                                                <th>Status</th>
                                                <th>Creation Reason</th>
                                                <th>Last Comment</th>
                                                <th>Last Update</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

            </div>
      

        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <!-- Export Single Ticket -->
    <script>

        <?php 
            $mydata = '';
            foreach($ticket as $order) {

                $order->created_for = fixStringForDatatables($order->created_for);
                $order->comment = fixStringForDatatables($order->comment);
                
                $showlink = "<a href='/warehouse/ticket/view/$order->waybill' target='_blank'>Show</a>";
                $mydata = "[ \"$order->created_at\", \"$order->waybill\", \"$order->priority\", \"$order->status\", \"$order->created_for\", \"$order->comment\", \"$order->updated_at\", \"$showlink\", ]";
            }
        ?>

        $(document).ready(function () {

            $("#datatable_ticket").DataTable({

                "data": [
                    <?php echo $mydata ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });
        });

    </script>

    <!-- Export Bulk -->
    <script>
        
        <?php
            $bulkdata = '';
            foreach($bulktickets as $order) {

                $order->created_for = fixStringForDatatables($order->created_for);
                $order->comment = fixStringForDatatables($order->comment);
                
                $showlink = "<a href='/warehouse/ticket/view/$order->waybill' target='_blank'>Show</a>";
                $bulkdata .= "[ \"$order->created_at\", \"$order->waybill\", \"$order->priority\", \"$order->status\", \"$order->created_for\", \"$order->comment\", \"$order->updated_at\", \"$showlink\", ], ";
            }
        ?>

        $(document).ready(function () {

            $("#datatable_bulktickets").DataTable({

                "data": [
                    <?php echo $bulkdata ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });
        });

    </script>

    <!-- Export Filter -->
    <script>

        <?php 
            $filterdata = '';
            foreach($tickets as $order) {

                $order->created_for = fixStringForDatatables($order->created_for);
                $order->comment = fixStringForDatatables($order->comment);

                $showlink = "<a href='/warehouse/ticket/view/$order->waybill' target='_blank'>Show</a>";
                $filterdata .= "[ \"$order->created_at\", \"$order->waybill\", \"$order->priority\", \"$order->status\", \"$order->created_for\", \"$order->comment\", \"$order->updated_at\", \"$showlink\", ], ";
            }
        ?>

        $(document).ready(function () {

            $("#datatable_tickets").DataTable({

                "data": [
                    <?php echo $filterdata ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });
        });

    </script>
    

@stop
