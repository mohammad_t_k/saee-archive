@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<style>
    div .form-group {
        margin: 10px;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<h3>Create Action</h3><hr/><br/>
                    @if(isset($error))
                        <div class="alert alert-danger text-center" style="font-size: 15px;" role="alert">
                            {{ $error['message'] }}
                        </div>
                    @endif
                    <h3 style="width: 35%; margin: auto;">Ticket Number: &nbsp;<b>{{ $shipment->jollychic }} </b> </h3>
                    <br/>

                    <form action="/warehouse/ticket/action/create" method="post" style="margin-top: 20px;" onsubmit="return check()">
                        <input type="hidden" name="tabFilter" value="true" />
                        <input type="hidden" name="waybill" value="{{ $waybill }}" />
                        <div class="row" style="width: 70%; margin: auto;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Ticket Priority  </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <select name="ticket_priority" id="priority" class="form-control col-md-7 col-xs-12">
                                        <option value="Low" {{ isset($ticket) && $ticket->priority == 'Low' ? 'selected' : '' }}>Low</option>
                                        <option value="Medium" {{ isset($ticket) && $ticket->priority == 'Medium' ? 'selected' : '' }}>Medium</option>
                                        <option value="High" {{ isset($ticket) && $ticket->priority == 'High' ? 'selected' : '' }}>High</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">  </label>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 " style="margin-top: 5px;">
                                    <label class=" col-md-5 col-xs-12">
                                        <input type="checkbox" name="urgent_delivery" value="1" id="urgent_delivery" class="form-inline form-check col-md-1 col-xs-12" {{ $shipment->urgent_delivery ? 'checked' : '' }} /> &nbsp; <b>Urgent Delivery </b>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Status </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <?php $admin_role = Session::get('admin_role'); ?>
                                    <select name="status" id="status" class="form-control col-md-7 col-xs-12">
                                        <option value="nothing" selected></option>
                                        @foreach($statuses as $status)
                                            @if($admin_role != 5 && $status->id == 3 && $ticket->status != 3 || $admin_role == 5)
                                                <option value="{{ $status->id }}">{{ $status->english }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Receiver Name </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <input id="shipment_name" name="shipment_name" class="form-control col-md-7 col-xs-12" type="text" value="{{ $shipment->receiver_name }}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Mobile 1 </label>
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                    <input id="shipment_mobile1" name="shipment_mobile1" class="form-control col-md-7 col-xs-12" type="text" value="{{ $shipment->receiver_phone }}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Mobile 2 </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <input id="shipment_mobile2" name="shipment_mobile2" class="form-control col-md-7 col-xs-12" type="text" value="{{ $shipment->receiver_phone2 }}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Main City </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <select name="shipment_maincity" id="maincity" class="form-control col-md-7 col-xs-12" onchange="digest_maincity()">
                                        @foreach($allMainCities as $one_maincity)
                                            <option value="{{ $one_maincity->name }}" {{ strtolower($shipment->main_city) == strtolower($one_maincity->name) ? 'selected' : '' }}>{{ $one_maincity->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->main_city }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Sub City </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <select name="shipment_subcity" id="subcity" class="form-control col-md-7 col-xs-12" onchange="digest_subcity()">
                                        @foreach($allSubCities as $one_subcity) 
                                            @if($shipment->main_city != '' && strtolower($one_subcity->city) != strtolower($shipment->main_city)) 
                                                <?php continue; ?> 
                                            @endif
                                            <option value="{{ $one_subcity->subcity }}" {{ strtolower($shipment->d_city) == strtolower($one_subcity->subcity) ? 'selected' : '' }}>{{ $one_subcity->subcity }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->d_city }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Hub </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <select name="shipment_hub" id="hub" class="form-control col-md-7 col-xs-12">
                                        @if($shipment->hub_id == 0)
                                            <option value="0"></option>
                                        @endif
                                        @foreach($allHubs as $one_hub)
                                            @if($shipment->main_city != '' && strtolower($one_hub->city) != strtolower($shipment->main_city))
                                                <?php continue; ?> 
                                            @endif
                                            <option value="{{ $one_hub->id }}" {{ $shipment->hub_id == $one_hub->id ? 'selected' : '' }}>{{ $one_hub->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <?php $hubs_map = array(); foreach($allHubs as $one_hub) $hubs_map[$one_hub->id] = $one_hub; ?>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->hub_id == 0 ? '' : $hubs_map[$shipment->hub_id]->name }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">District </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <select name="shipment_district" id="district" class="form-control col-md-7 col-xs-12">
                                        @foreach($allDistricts as $one_district)
                                            @if($shipment->d_city != '' && strtolower($one_district->subcity) != strtolower($shipment->d_city))
                                                <?php continue; ?> 
                                            @endif
                                            <option value="{{ $one_district->district_name }}" {{ strtolower($one_district->district_name) == strtolower($shipment->d_district) ? 'selected' : ''}}>{{ $one_district->district_name }} | {{ $one_district->district_name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->d_district }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Address </label>
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                    <input id="shipment_address" name="shipment_address" class="form-control col-md-7 col-xs-12" type="text" value="{{ $shipment->d_address }}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Reschedule at </label>
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                    <input id="shipment_reschedule" name="shipment_reschedule" value="{{ $shipment->scheduled_shipment_date }}" class="form-control col-md-7 col-xs-12" type="date">
                                </div>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->scheduled_shipment_date }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Is Cancelled </label>
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                    <select name="is_cancelled" id="is_cancelled" class="form-control col-md-7 col-xs-12">
                                        <option value="" ></option>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                                <div class="col-md-2" style="font-weight: bold;">{{ $shipment->status == 3 && $shipment->undelivered_reason == 3 ? 'Yes' : 'No' }}</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Comment </label>
                                <div class="col-lg-6 col-md-2 col-sm-6 col-xs-12 ">
                                    <textarea id="comment" name="comment" class="form-control col-md-7 col-xs-12"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br/>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                                <input type="submit" value="Create Action" class="btn btn-primary col-md-2 col-md-offset-4 col-xs-12" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <script>

        function getSubCities() {
            $("#subcity").empty();
            var maincity = $("#maincity").val();
            console.log('getSubCities function -> maincity: ' + maincity);
            let subcity = document.getElementById('subcity');
            @foreach($allSubCities as $one_subcity)
                if(maincity.toLowerCase() == "{{ strtolower($one_subcity->city) }}") {
                    option = document.createElement('option');
                    option.setAttribute('value', "{{ $one_subcity->subcity }}");
                    // option.setAttribute('selected', 'true');
                    option.appendChild(document.createTextNode("{{ $one_subcity->subcity }}"));
                    subcity.appendChild(option);
                }
            @endforeach
        }

        function getHubs() {
            $("#hub").empty();
            let maincity = $("#maincity").val();
            console.log('getHubs function -> maincity: ' + maincity);
            let hub = document.getElementById('hub');
            @if($shipment->hub_id == 0)
                option = document.createElement('option');
                option.setAttribute('value', "0");
                option.appendChild(document.createTextNode(""));
                hub.appendChild(option);
            @endif
            @foreach($allHubs as $one_hub)
                if(maincity.toLowerCase() == "{{ strtolower($one_hub->city) }}") {
                    option = document.createElement('option');
                    option.setAttribute('value', "{{ $one_hub->id }}");
                    // option.setAttribute('selected', 'true');
                    option.appendChild(document.createTextNode("{{ $one_hub->name }}"));
                    hub.appendChild(option);
                }
            @endforeach
        }

        function getDistricts() {
            $("#district").empty();
            let subcity = $("#subcity").val();
            console.log('getDistricts function -> subcity: ' + subcity);
            let district = document.getElementById('district');
            @foreach($allDistricts as $one_district)
                if(subcity.toLowerCase() == "{{ strtolower($one_district->subcity) }}") {
                    option = document.createElement('option');
                    option.setAttribute('value', "{{ $one_district->district_name }}");
                    // option.setAttribute('selected', 'true');
                    option.appendChild(document.createTextNode("{{ $one_district->district_name }} | {{ $one_district->district_name_ar }}"));
                    district.appendChild(option);
                }
            @endforeach
        }

        function digest_maincity() {
            console.log('digest_maincity function');
            getSubCities();
            getHubs();
            getDistricts();
        }

        function digest_subcity() {
            console.log('digest_subcity function');
            getDistricts();
        }

    </script>

    <script>
        function check() {
            var cancelled = document.getElementById('is_cancelled').value;
            var status = document.getElementById('status').value;
            var main_city = document.getElementById('maincity').value;
            if(main_city == '') {
                alert('Please enter correct city');
                return false;
            }
            var sub_city = document.getElementById('subcity').value;
            if(sub_city == '') {
                alert('Please enter correct city');
                return false;
            }
            if(cancelled == '1' && confirm('Are you sure you want to cancel this order?') == false)
                return false;
            else if(status == '3' && confirm('Are you sure you want to close this ticket?') == false)
                return false;
            return true;
        }
    </script>
    

@stop
