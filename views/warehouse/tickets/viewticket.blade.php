@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<style>
	.mark {
		color: black;
	}
	table {
		color: black;
	}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<h3 class="text-center">Ticket Information</h3>

                	<div class="row" style="width: 80%; margin: auto; margin-top: 20px;">
                		<style>
                		table thead tr th {
                			text-align: center;
                		}
                		</style>
						<table class="table table-bordered table-stripped" style="text-align: center;"> 
							<thead>
								<tr>
									<th>Created At</th>
									<th>Waybill</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Type</th>
									<th>Creation Reason</th>
									<th>Last Update</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $ticket->created_at }}</td>
									<td>{{ $ticket->waybill }}</td>
                                    <td>{{ $ticket->priority }}</td>
                                    <td>{{ $ticket->status }}</td>
                                    <td>{{ $ticket->type == 0 ? '' : $ticket_types_map[$ticket->type]->english }}</td>
									<td>{{ $ticket->created_for }}</td>
									<td>{{ $ticket->updated_at }}</td>
								</tr>
							</tbody>
						</table>
                	</div>

                	<h3 class="text-center" style="margin-top: 20px;">Ticket Actions History</h3>

                	<div class="row" style="width: 95%; margin: auto; margin-top: 20px;">
                		<table class="table table-bordered table-stripped text-center">
                			<thead>
                				<tr>
                					<th>Date and Time</th>
                					<th>Admin</th>
                					<th>Company (Action Made By)</th>
                					<th>Ticket Priority</th>
                					<th>Ticket Type</th>
                					<th>Status</th>
                					<th>Receiver Name</th>
                					<th>Mobile 1</th>
                					<th>Mobile 2</th>
                					<th>Main City</th>
                					<th>Sub City</th>
                					<th>Hub</th>
                					<th>District</th>
                                    <th>Address</th>
                                    <th>Schedule Date</th>
                					<th>Cancelled</th>
                					<th>Urgent Delivery</th>
                					<th>Comment</th>
                				</tr>
                			</thead>
                			<tbody>
	                		@foreach($actions as $action)
	                			<tr>
	                				<td>{{ $action->created_at }}</td>
	                				<td>{{ $action->admin }}</td>
	                				<td>{{ $action->company_id == 0 ? '' : $companies_names[$action->company_id] }}</td>
	                				<td class="{{ $action->is_priority ? 'mark' : '' }}">{{ $action->ticket_priority }}</td>
	                				<td class="{{ $action->is_type ? 'mark' : '' }}">{{ $action->ticket_type == 0 ? '' : $ticket_types_map[$action->ticket_type]->english }}</td>
	                				<td class="{{ $action->is_status ? 'mark' : '' }}">{{ $action->status }}</td>
	                				<td class="{{ $action->is_name ? 'mark' : '' }}">{{ $action->shipment_name }}</td>
	                				<td class="{{ $action->is_mobile1 ? 'mark' : '' }}">{{ $action->shipment_mobile1 }}</td>
	                				<td class="{{ $action->is_mobile2 ? 'mark' : '' }}">{{ $action->shipment_mobile2 }}</td>
	                				<td class="{{ $action->is_main_city ? 'mark' : '' }}">{{ $action->shipment_main_city }}</td>
	                				<td class="{{ $action->is_city ? 'mark' : '' }}">{{ $action->shipment_city }}</td>
	                				<td class="{{ $action->is_hub ? 'mark' : '' }}">{{ $action->shipment_hub == 0 ? '' : $allHubs[$action->shipment_hub]->name }}</td>
	                				<td class="{{ $action->is_district ? 'mark' : '' }}">{{ $action->shipment_district }}</td>
	                				<td class="{{ $action->is_address ? 'mark' : '' }}">{{ $action->shipment_address }}</td>
                                    <td class="{{ $action->is_scheduled_date ? 'mark' : '' }}">{{ $action->shipment_scheduled_date == '0000-00-00' ? '' : $action->shipment_scheduled_date }}</td>
                                    <td class="{{ $action->is_is_cancelled ? 'mark' : '' }}">{{ $action->shipment_is_cancelled == 0 ? 'No' : 'Yes' }}</td>
                                    <td class="{{ $action->is_urgent_delivery ? 'mark' : '' }}">{{ $action->shipment_urgent_delivery == 0 ? 'No' : 'Yes' }}</td>
	                				<td>{{ $action->comment }}</td>
	                			</tr>
	                		@endforeach
	                		</tbody>
                		</table>
                	</div>

                	<div style="width: 80%; margin: auto; margin-top: 20px;">
						<div class="row">
							<form action="/warehouse/ticket/action/create" method="get" class="col-md-2 col-md-offset-9 col-xs-12">
								<input type="hidden" name="waybill" value="{{ $ticket->waybill }}" />
								<input type="submit" value="Create Action" class="btn btn-primary" />
							</form>
							<div class="col-md-1 col-xs-12 main">
								<button class="btn btn-danger" onclick="close_ticket()">Close Ticket</button>
							</div>
						</div>
                	</div>

                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

	<script>
		function close_ticket() {
			$.ajax({
				type: 'GET',
				url: '/warehouse/ticket/close',
				data: { waybill: "{{ $ticket->waybill }}" }
			}).done(function(response) {
				console.log(response);
				location.reload();
			});
		}
	</script>

@stop
