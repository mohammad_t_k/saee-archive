@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

<?php
if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 16)
    $layout = 'warehouse.viewlayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-xs-12">
                            <div style="margin-top: 50px;">
                                <form action="/warehouse/ticket/inquiry" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="col-md-5 col-md-offset-2 col-xs-12">
                                        <label>Shipment ID</label>
                                        <input type="text" name="waybill" id="waybill" value="<?php echo isset($shipment) ? $shipment->jollychic : ''; ?>" class="form-control col-xs-12" required="required" />
                                    </div>
                                    <div class="col-md-2 col-xs-12">
                                        <input type="submit" value="Search" class="btn btn-primary form-control" style="margin-top: 23px; height: 35px; background-color: #73879C;" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <form action="/warehouse/ticket/newpost" method="post" onsubmit="return check()">
                        <input type="hidden" name="waybill" value="{{ isset($shipment) ? $shipment->jollychic : '' }}" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row" style="margin-top: 60px;">
                            
                            <div class="col-xs-12">
                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Name </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="name" name="name" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->receiver_name : '' }}" type="text" disabled="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Company </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="company" name="company" class="form-control col-md-7 col-xs-12" value="{{ isset($company) ? $company->company_name : '' }}" type="text" disabled="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Mobile 1 </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="mobile1" name="mobile1" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->receiver_phone : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Mobile 2 </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="mobile2" name="mobile2" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->receiver_phone2 : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Delivery Attempts </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="delivery_attempts" name="delivery_attempts" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->num_faild_delivery_attempts : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Undelivered Reason </label>
                                        <div class="col-md-7 col-xs-12">
                                            <?php
                                            if(isset($shipment))
                                                $query = DB::select('select english from undelivered_reasons where id = ' . $shipment->undelivered_reason)[0]->english;
                                            else
                                                $query = '';
                                            ?>
                                            <input id="reason" name="reason" class="form-control col-md-7 col-xs-12" value="{{ $query }}" type="text" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">City </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="city" name="city" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->d_city : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Hub </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="hub" name="hub" class="form-control col-md-7 col-xs-12" value="{{ !isset($shipment) || $shipment->hub_id == 0 ? '' : $allHubs[$shipment->hub_id]->name }}" type="text" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">District </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="district" name="district" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->d_district : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">COD </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="cod" name="cod" class="form-control col-md-7 col-xs-12" value="{{ isset($shipment) ? $shipment->cash_on_delivery : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Status </label>
                                        <div class="col-md-7 col-xs-12">
                                            <input id="status" name="status" class="form-control col-md-7 col-xs-12" value="{{ isset($statuses) ? $statuses[$shipment->status]->arabic : '' }}" type="text" disabled>
                                        </div>
                                    </div>
                                </div>

                                <br/><br/><br/>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Priority </label>
                                        <div class="col-md-7 col-xs-12">
                                            <select name="priority" id="priority" class="form-control">
                                                <option value="nothing" selected></option>
                                                <option value="Low">Low</option>
                                                <option value="Medium">Medium</option>
                                                <option value="High">High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <input type="submit" value="Create Ticket" class="col-md-3 col-xs-12 btn btn-primary" />
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Type </label>
                                        <div class="col-md-7 col-xs-12">
                                            <select name="ticket_type" id="ticket_type" class="form-control">
                                                <option value="0"></option>
                                                @foreach($ticket_types as $one_record)
                                                    <option value="{{ $one_record->id }}" {{ $one_record->id == 1 ? 'selected' : '' }}>{{ $one_record->english }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 5%;">
                                    <div class="col-md-6 col-xs-12 form-group" >
                                        <label class="control-label col-md-3 col-xs-12">Why you create this ticket ?</label>
                                        <div class="col-md-7 col-xs-12">
                                            <textarea id="created_for" name="created_for" class="col-xs-12" required="required"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 60px;">
                                    @if(isset($error))
                                        <div class="alert alert-danger text-center col-md-4 col-xs-12 col-md-offset-3" style="margin-top: 20px;">{{ $error }}</div>
                                    @endif
                                </div>
                                
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <!-- <script>
        $(document).ready(function(){ 
            $('#city').empty();
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text',
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for (var i = 0; i < cities.length; i += 1) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        if(cities[i].name == '{{ isset($shipment) ? $shipment->d_city : "" }}')
                            option.setAttribute('selected', true);
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }
                }
            });

            $("#district").empty();
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: '{{ isset($shipment) ? $shipment->d_city : '' }}' }
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == '{{ isset($shipment) ? $shipment->d_city : '' }}') {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    for (var i = 0; i < districts.length; i += 1) {
                         if(districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        if(districts[i].district == '{{ isset($shipment) ? $shipment->d_district : "" }}')
                            option.setAttribute('selected', true);
                        option.appendChild(document.createTextNode(districts[i].district + ' - ' + districts[i].district_ar));
                        select.appendChild(option);
                    }
                }
            });
        });
    </script> -->

    <script>
        function check() {
            {{ !isset($shipment) ? 'alert("Please insert right shipment number"); return false;' : '' }}
            var priority = document.getElementById('priority').value;
            if(priority == 'nothing'){
                alert('Please insert a priority for this shipment');
                return false;
            }
            return true; 
        }
    </script>

@stop
