@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif


        
            <div class="box-body ">
                <form action="" method="POST" id="CityForm">
                    <input type="hidden" name="id" id="id" value="{{ $city->id }}">
                <div class="form-group">
                    <label>City English</label><input id="cityen" value="{{ $city->name }}" required="" class="form-control" type="text" name="cityen"
                                              placeholder="Add City English">
                </div>
                <div class="form-group">
                    <label>City Arabic</label><input id="cityar" value="{{ $city->name_ar }}" required="" class="form-control" type="text" name="cityar"
                                              placeholder="Add City Arabic">
                </div>
                <div class="form-group">
                    <label>City Type</label>

                    <select name="citytype" id="citytype" required="" style="width: 100%;height: 35px;border: 1px solid #CCCCCC;padding-left: 10px;" >
                        <option <?php if (!empty($grouped) && $grouped == $city->grouped)  echo 'selected = "selected"'; ?> value="0">Discrete</option>
                        <option <?php if (!empty($grouped) && $grouped == $city->grouped)  echo 'selected = "selected"'; ?> value="1">Grouped</option>
                    </select>
                </div>
                <?php 
                $districtsen = array();
                $districtsar = array();
                foreach($districts as $district){

                    if($district->is_remote == 1){

                         $districtsen[] = $district->district." ".$district->is_remote;
                    }
                    else{

                    $districtsen[] = $district->district;

                    }
                    
                    $districtsar[] = $district->district_ar;
                } 
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Districts English<span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="districten" name="districten" required="required" class="form-control" type="text" rows="20" ><?php echo implode(PHP_EOL, $districtsen) ?></textarea>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Districts Arabic<span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="districtar" name="districtar" required="required" class="form-control" type="text" rows="20" ><?php echo implode(PHP_EOL, $districtsar) ?></textarea>
                        </div>
                    </div>

            </div>
           

                <button  type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Update City</button>

            </form>
        

    </div>
</div>
</div>

<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>


        $(document).ready(function () {

            $("#CityForm").submit(function (e) {
                

                        var cityen = document.getElementById("cityen").value;
                        var cityar = document.getElementById("cityar").value;
                        var citytype = document.getElementById("citytype").value;
                        var id = document.getElementById("id").value;
                        var districten = document.getElementById("districten").value.replace(/(?:\r\n|\r|\n)/g, ',');

                        var districtar = document.getElementById("districtar").value.replace(/(?:\r\n|\r|\n)/g, ',');

                        //check if entered equal districts
                        if (districten.split(',').length != districtar.split(',').length) {

                            bootbox.alert('English District and Arabic Should be equal');
                            return false;
                        }


                        var allDistrictsen = districten.split(',');
                        var allDistrictsar = districtar.split(',');

                        var wrongIDs = Array();
                        var correctDistrictsEn = Array();
                        var correctDistrictsAr = Array();
                        var correctedIDs = Array();
                        var finalArrayen = [];
                        var finalArrayar = [];
                        for (var i = 0; i < allDistrictsen.length; i++) {
                            allDistrictsen[i] = allDistrictsen[i].replace(/ /g, '');


                            if (allDistrictsen[i].length > 1) {
                               
                                correctDistrictsEn.push(allDistrictsen[i]);
                               
                            }
                        }

                        $.each(correctDistrictsEn, function (i, el) {
                            if ($.inArray(el, finalArrayen) === -1) {
                                finalArrayen.push(el);
                            }
                        });


                            // for Arabic Districts
                         for (var i = 0; i < allDistrictsar.length; i++) {
                            allDistrictsar[i] = allDistrictsar[i].replace(/ /g, '');


                            if (allDistrictsar[i].length > 1) {
                               
                                correctDistrictsAr.push(allDistrictsar[i]);
                               
                            }
                        }

                        $.each(correctDistrictsAr, function (i, el) {
                            if ($.inArray(el, finalArrayar) === -1) {
                                finalArrayar.push(el);
                            }
                        });



                       /* if (correctedIDs.length) {
                            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                        } */

                        if (finalArrayen.length && finalArrayar.length) {
                            if (confirm("Are you sure districts are correct ? ") == true) {
                                 e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/system/edit_city_post',
                                    dataType: 'text',
                                    data: {cityenglish: cityen,cityarabic: cityar, id: id, cityt: citytype, districtsen: finalArrayen.join(","), districtsar: finalArrayar.join(",")},
                                }).done(function (response) {

                                    console.log(response);

                                    window.location='/warehouse/system/cities?success=3';
                                    
                                }); // Ajax Call

                            }
                            else {
                                bootbox.alert('You cancled!');
                                return false;
                            }

                        }
                        else {
                            bootbox.alert('Nothing to signin!');
                            return false;
                        }


                    }
            );
        });

    </script>

@stop