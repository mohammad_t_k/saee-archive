@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


@extends('warehouse.layout')


@section('content')

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>In Route to supplier warehouse</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Move shipments back to supplier warehouse from Kasper Business warehouse</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20" ></textarea>
                        </div>
                    </div>
                    
                   
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <p><button id="submitForm"  class="btn btn-success">Submit</button>  </p>
                        </div>
                    </div>
                    
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
     
            
            
          </div>
        </div>
        <!-- /page content -->
        
          <!-- jQuery k-w-h.com/app/views/warehouse/vendors --> 
   
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
<script  >




$(document).ready(function() {
    
    $("#submitForm").click( function()
    {
        var shipIDs= document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
       
       
        if (shipIDs=='')
        {
            bootbox.alert('Please enter IDs!'   );
            return;
        }
        
        var ids = shipIDs.split(',');
        for(i = 0; i < ids.length;++i){
            if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                ids[i] = ids[i].toUpperCase();
        }
        
        var wrongIDs = Array();
        var correctIDs = Array();
        var correctedIDs = Array();
        var finalArray = [];
        for (var i = 0; i <ids.length; i++ )
        {
            if(!ids[i].replace(/\s/g, '').length)
            {
                continue;
            }
            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]))
            {
                correctIDs.push(ids[i]);
            }
            else
            {
               /* if (/[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('JC' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
                else if (/C[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('J' + ids[i]);
                    correctedIDs.push(ids[i]);
                } */
               
                wrongIDs.push(ids[i]);
            }
        }
        
        $.each(correctIDs, function(i, el)
        {
            if($.inArray(el, finalArray) === -1) 
            {
                finalArray.push(el);
            }
        });

        
        
        if (wrongIDs.length)
        {
            bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
        }
        
        /*if (correctedIDs.length)
        {
            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
        }*/
    
        if (finalArray.length ) 
        {
            if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true)
            {
                window.location = "/warehouse/work/returnstoriyadh?ids="+finalArray.join(",");

            }
            else
            {
                alert('You cancled!' );
            }
            
        } 
        else 
        {
            alert('Nothing to signin!'   );
           
        }    
        
       }
      );
});

</script  >
@stop

