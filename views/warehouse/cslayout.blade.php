<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico"/>

    <title>Kasper Business </title>

    <!-- Bootstrap -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/css/bootstrap.min.css"
          rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"
          rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.css"
          rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
          rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.css"> -->
    <!-- Custom Theme Style -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/build/css/custom.min.css" rel="stylesheet">
    <style>
        .nav-md .container.body .col-md-3.left_col.menu_fixed {
            overflow: auto;
        }

        .nav-md .container.body .col-md-3.left_col {
            width: 247px;
        }
    </style>

</head>
<?php

$admin_id = Session::get('admin_id');
$admin = Admin::find($admin_id);
$asminusername = 'Kasper Agent';
if (isset($admin)) {
    $asminusername = strtoupper($admin->username);
}
?>

<body class="nav-md">

<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="../warehouse" class="site_title"><i class="fa fa-industry" aria-hidden="true"></i> <span>Kasper Business</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <!-- m<div class="profile_pic">
                      <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    -->
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <?php echo '<h2>' . $asminusername . '</h2>';
                        ?>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <?php 

                $december2017start = strtotime("2017-12-01 00:00:00");
                $december2017end   = strtotime("2018-01-01 23:59:59");

                $january2018start = strtotime("2018-01-01 00:00:00");
                $january2018end   = strtotime("2018-02-01 23:59:59");

                $feb2018start = strtotime("2018-02-01 00:00:00");
                $feb2018end   = strtotime("2018-03-01 23:59:59");

                $march2018start = strtotime("2018-03-01 00:00:00");
                $march2018end   = strtotime("2018-04-01 23:59:59");

                $april2018start = strtotime("2018-04-01 00:00:00");
                $april2018end   = strtotime("2018-05-01 23:59:59");

                $may2018start = strtotime("2018-05-01 00:00:00");
                $may2018end   = strtotime("2018-06-01 23:59:59");

                ?>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="../warehouse">Dashboard</a></li>
                                    <li><a href="/warehouse/controlpanel" target="_blank">Control Panel</a></li>
                                    <li><a href="/warehouse/successratereport">Success Rate Report</a></li>
                                    <li><a href="/warehouse/sameDaySuccessRate">Same Day Success Rate Report</a></li>
                                    <li><a href="/warehouse/durationAnalysis">Delivery Duration Analysis</a></li>
                                    <li><a>2017<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $december2017start }}&monthend={{ $december2017end }}">December</a>
                                            </li>

                                        </ul>
                                    </li>
                                    <li><a>2018<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $january2018start }}&monthend={{ $january2018end }}">January</a>
                                            </li>
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $feb2018start }}&monthend={{ $feb2018end }}">February</a>
                                            </li>
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $march2018start }}&monthend={{ $march2018end }}">March</a>
                                            </li>
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $april2018start }}&monthend={{ $april2018end }}">April</a>
                                            </li>
                                            <li class="sub_menu"><a
                                                        href="/warehouse/shipmentstatusbymonth?monthstart={{ $may2018start }}&monthend={{ $may2018end }}">May</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li><a><i class="fa  fa-table"></i>Shipments | شحنات <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="/warehouse/shipmentsExport">Export Shipments</a></li>
                                    <!--<li><a href="/warehouse/shipmentsV2/all">All</a></li> -->

                                    <li><a>City<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">

                                            <?php
                                            $admin_id = Session::get('admin_id');
                                            $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
                                            if ($total_num_of_shipments = count($adminCities) > 0) {
                                                foreach ($adminCities as $city) {

                                                        if($city->city == 'All'){
                                                            continue;
                                                        }
                                                    echo "<li><a>$city->city<span class=\"fa fa-chevron-down\"></span></a><ul class=\"nav child_menu\">";
                                                    $shipmentdates = DeliveryRequest::distinct()->where('d_city', 'like', $city->city)->where('jollychic', '!=', '0')->orderBy('scheduled_shipment_date', 'desc')->limit(10)->get(['scheduled_shipment_date']);
                                                    $total_num_of_shipments = count($shipmentdates);
                                                    foreach ($shipmentdates as $ashipmentdates) {
                                                        if ($ashipmentdates->scheduled_shipment_date != null) {
                                                            echo "<li class=\"sub_menu\"><a href=\"/warehouse/shipmentsV2/$ashipmentdates->scheduled_shipment_date?city=$city->city\"> $ashipmentdates->scheduled_shipment_date </a></li>";
                                                        }
                                                    }
                                                    echo "</ul></li>";
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-male"></i> Companies <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/warehouse/registercompany/">Register Company</a></li>
                                    <li><a href="/warehouse/companies/">All Companies</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-male"></i> Captains <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/warehouse/captains/">All Captains</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <!-- <h3>Customer Service</h3>-->
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Customer Services <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/warehouse/customerservice">Dashboard</a></li>
                                    <li><a href="https://www.saee.sa/blackbox/login" target="_blank">Black Box &nbsp; <i class="fa fa-external-link" style="font-size: 12px;"></i></a></li>
                                    <li><a href="/warehouse/ticket/new">New Ticket</a></li>
                                    <li><a href="/warehouse/ticket/export">Export Tickets</a></li>
                                    <li><a href="/warehouse/branchperformanceen"> Branch Performance</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa  fa-table"></i>Tracking <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li class="sub_menu"><a href="/warehouse/admintracking">Search</a></li>
                                    <li class="sub_menu"><a href="/warehouse/admintrackingbulk">Multi Search</a></li>

                                </ul>

                            </li>
                        </ul>


                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="/warehouse/logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php echo $asminusername;
                                ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="/warehouse/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->


        <div class="container">
            @yield('content')
        </div>

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                &copy; All rights reserved Kasper
            </div>
            <div class="clearfix"></div>
        </footer>

        <!-- /footer content -->
    </div>


    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->


    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- Custom Theme Script s-->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/custom.js"></script>

</div>
</body>
</html>
 
    
