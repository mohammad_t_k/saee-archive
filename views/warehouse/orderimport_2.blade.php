@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->


    <div class="right_col" role="main">

        <form action="/warehouse/orderimportinDB" onsubmit="return validateForm()" method="post"
              enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"order_number","Order Number")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Order Number</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Order Number"
                                                                    name="order_number" id="order_number"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"></div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Company ID</label>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <select id="company" name="company" required="required" class="form-control"
                                type="text">
                            <option value="0">Select Company</option>
                            @foreach($companies as $compan)
                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"receiver_name","Receiver Name")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Name</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Name"
                                                                    name="receiver_name" id="receiver_name"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"receiver_phone","Receiver Mobile")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Mobile-1</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Mobile"
                                                                    name="receiver_phone" id="receiver_phone"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"receiver_phone2","receiver_phone2")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Mobile-2</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="receiver_phone2"
                                                                    name="receiver_phone2" id="receiver_phone2"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"ConsigneeEmail","Receiver Email")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Email</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Email"
                                                                    name="ConsigneeEmail" id="ConsigneeEmail"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"d_city","City")';></div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver City</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="City" name="d_city"
                                                                    id="d_city"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"d_address","Receiver Full Address")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Address</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Full Address" name="d_address"
                                                                    id="d_address">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"d_address2","Receiver Street Address")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver Address 2</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Street Address" name="d_address2"
                                                                    id="d_address2">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"district","Receiver District")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Receiver District</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver District" name="district"
                                                                    id="district">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"cash_on_delivery","COD Amount")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Cash On Delivery</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="COD Amount"
                                                                    name="cash_on_delivery" id="cash_on_delivery"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"Customs_Value","Customs_Value")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Customs Value</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Customs_Value"
                                                                    name="Customs_Value" id="Customs_Value"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"d_description","Shipment Description")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Description</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Shipment Description"
                                                                    name="d_description" id="d_description"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"weight","Weight")';></div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Weight</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Weight" name="d_weight"
                                                                    id="weight"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" checked
                                                                    onclick='clearvalue(this,"d_quantity","Quantity")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Quantity</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Quantity" name="d_quantity"
                                                                    id="d_quantity">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" id="state" checked
                                                                    onclick=clearvalue(this,"d_state","d_state");></div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">State</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="d_state" name="d_state"
                                                                    id="d_state"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" value="" id="sendername" checked
                                                                    onclick='clearvalue(this,"sender_name","Sender Name")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Sender Name</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Sender Name" name="sender_name"
                                                                    id="sender_name"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" checked
                                                                    onclick=clearvalue(this,"currency","currency");>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Currency</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="currency" name="currency"
                                                                    id="currency"></div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" checked
                                                                    onclick='clearvalue(this,"latlong","Receiver Lat Log")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Lat Long</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Receiver Lat Log" name="latlong"
                                                                    id="latlong"></div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" checked
                                                                    onclick='clearvalue(this,"order_reference","Order Reference")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Order Reference</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Order Reference" name="order_reference"
                                                                    id="order_reference"></div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" checked
                                                                    onclick='clearvalue(this,"timeslot","Timeslot")';>
                    </div>
                    <label class="control-label col-md-4 col-sm-4 col-xs-12">Time Slot</label>

                    <div class="col-md-7 col-sm-7 col-xs-12"><input type="text" value="Timeslot" name="timeslot"
                                                                    id="timeslot"></div>

                </div>


            </div>

            <label>Upload File</label>
            <input type="file" name="file">
            <input type="submit" value="Upload File" name="submit">
        </form>

        <?php
        if (!empty($success)) {

            echo $success;
        }
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group"
             style="<?php if (count($duplicateOrders) == 0) echo 'display:none'; else echo 'display:block'; ?>;margin-top: 30px;">
            <div class="x_panel">
                <h2>Following records are not created due to duplicate order number
                    {{--<small>Please create these records with unique order number</small>--}}
                </h2>
                <table id="datatablenr" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Receiver Name</th>
                        <th>Receiver Mobile</th>
                        <th>Receiver City</th>
                        <th>COD</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($duplicateOrders as $item) { ?>
                    <tr>
                        <td><?= $item['order_number'] ?></td>
                        <td><?= $item['customer_name'] ?></td>
                        <td><?= $item['customer_moble'] ?></td>
                        <td><?= $item['customer_city'] ?></td>
                        <td><?= $item['cash_on_delivery'] ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>

                    <tfoot>
                    <tr>
                        <th>Order Number</th>
                        <th>Receiver Name</th>
                        <th>Receiver Mobile</th>
                        <th>Receiver City</th>
                        <th>COD</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        {{--Out of Coverage shipments--}}
        <div class="col-md-12 col-sm-12 col-xs-12 form-group"
             style="<?php if (count($unCoveredOrders) == 0) echo 'display:none'; else echo 'display:block'; ?>;margin-top: 30px;">
            <div class="x_panel">
                <h2>Following records are not created due to out of coverage</h2>
                <table id="datatablenr" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Receiver Name</th>
                        <th>Receiver Mobile</th>
                        <th>Receiver City</th>
                        <th>COD</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($unCoveredOrders as $item) { ?>
                    <tr>
                        <td><?= $item['order_number'] ?></td>
                        <td><?= $item['customer_name'] ?></td>
                        <td><?= $item['customer_moble'] ?></td>
                        <td><?= $item['customer_city'] ?></td>
                        <td><?= $item['cash_on_delivery'] ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>

                    <tfoot>
                    <tr>
                        <th>Order Number</th>
                        <th>Receiver Name</th>
                        <th>Receiver Mobile</th>
                        <th>Receiver City</th>
                        <th>COD</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    <!-- /page content -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


    <script>
        function validateForm() {
            var company = document.getElementById("company").value;
            var receiver_name = document.getElementById("receiver_name").value;
            var receiver_phone = document.getElementById("receiver_phone").value;
            var d_city = document.getElementById("d_city").value;
            var d_address = document.getElementById("d_address").value;
            var cash_on_delivery = document.getElementById("cash_on_delivery").value;
            if (company == 0) {
                alert("Please Select Company First");
                return false;
            }
            if (receiver_name == "") {
                alert("Please Select Receiver Name, it's Mandatory Field.");
                return false;
            }
            if (receiver_phone == "") {
                alert("Please Select Receiver Mobile-1, it's Mandatory Field.");
                return false;
            }
            if (d_city == "") {
                alert("Please Select Receiver City, it's Mandatory Field.");
                return false;
            }
            if (d_address == "") {
                alert("Please Select Receiver Address, it's Mandatory Field.");
                return false;
            }
            if (cash_on_delivery == "") {
                alert("Please Select Cash on Delivery, it's Mandatory Field.");
                return false;
            }

        }
        function clearvalue(eve, id, value) {
            if (eve.checked == false) {
                document.getElementById(id).value = ""
                document.getElementById(id).disabled = true
            } else {
                document.getElementById(id).value = value
                document.getElementById(id).disabled = false
            }

        }


    </script>




    @stop
