@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    <style>
        input[type=text] {
            width: 100%;
            padding: 8px 10px;
            margin: 8px 0;
            box-sizing: border-box;
        }
    </style>

    <!-- page content -->
    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Scan Mission</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Assign mission to a Captain</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="city" id="city" class="form-control">
                                                @foreach($adminCities as $adcity)
                                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-6 col-xs-12">Hub </label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="hub" id="hub" class="form-control">
                                                @foreach($cityHubs as $adhub)
                                                    <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mission waybill <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input id="mission_waybill" name="mission_waybill" type="text" value="">
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-2">
                                        <p>
                                            <button id="planMission" class="btn btn-success">Plan</button>
                                            <button id="unplanMission" class="btn btn-success">UnPlan</button>
                                            <button id="rollback" class="btn btn-success">Rollback </button>
                                            <button id="scanMission" class="btn btn-success">Scan</button>
                                            <button id="copy_waybills" class="btn btn-success">Copy Waybills </button>
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="x_title">
                        <h2>Missions</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">

                            <table id="missions_datatable" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Mission Name</th>
                                    <th>City</th>
                                    <th>Districts</th>
                                    <th>Number of shipments</th>
                                    <th>Boxes</th>
                                    <th>Estimated Delivery Fee</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Mission Name</th>
                                    <th>City</th>
                                    <th>Districts</th>
                                    <th>Number of shipments</th>
                                    <th>Boxes</th>
                                    <th>Estimated Delivery Fee</th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                        <!-- table start -->
                        <div class="modal fade" id="Modal_of_scheduledDate" role="dialog" style="overflow: auto">
                            <div class="modal-dialog  modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="mission_title"></h4>
                                    </div>
                                    <div class="modal-body" style="min-height: 130px;">
                                        <h4>Please select date.if you want to update scheduled shipment date for all mission's shipments</h4><br>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-4 col-sm-3 col-xs-12">Scheduled Shipment Date<span
                                                        class="required">*</span>
                                            </label>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="dispatchDate" class="date-picker form-control col-md-7 col-xs-12"
                                                       required="required" type="date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <a class="btn btn-success" onclick="updatescheduledDate()" disabled>Update</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- !table start -->
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>

        $(document).ready(function () {

            $("#planMission").click(function () {
                var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
                var city = document.getElementById("city").value;
                var hub = document.getElementById("hub").value;

                if (mission_waybill == '') {
                    bootbox.alert('Please enter mission waybill!');
                    return;
                }

                var captain_id = prompt("Please captain ID:");
                var responseObj;
                if (captain_id != null && captain_id != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptaininfo',
                        dataType: 'text',
                        data: {captain_id: captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response == 0) {
                            bootbox.alert('No captain is registred with entered ID');
                            return;
                        }
                        else {
                            responseObj = JSON.parse(response);

                            if (confirm("Are you sure you want to plan mission's waybill " + mission_waybill + " shipments  to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/planmissiontocaptain',
                                    dataType: 'text',
                                    data: {captain_id: responseObj.id, city: city,hub:hub, mission_waybill: mission_waybill},
                                }).done(function (responsef) {
                                    responseObjf = JSON.parse(responsef);
                                    if (responseObjf.error) {
                                        alert(responseObjf.error);
                                    }
                                    else if (responseObjf.count) {
                                        bootbox.alert('Mission of ' + responseObjf.count + ' shipments have been Plan to captain '+responseObj.first_name + " " + responseObj.last_name );
                                        document.getElementById('mission_waybill').value = '';

                                        var missioninfo = responseObjf.missioninfo;
                                        var missions_datatable = $('#missions_datatable').DataTable();

                                        $.each(missioninfo, function (i, item) {
                                            var waybill = item.mission_waybill;
                                            missions_datatable.row.add([
                                                        item.id,
                                                        '<a onclick=opensticker("' + waybill + '")>' + waybill + '</a>',
                                                        item.mission_name,
                                                        item.city,
                                                        item.district,
                                                        item.total_shipments,
                                                        item.quantity,
                                                        item.estimated_delivery_fee]
                                            ).draw();
                                        });
                                    } else if (responseObjf.scheduledDate_error) {
                                        $('#mission_title').html(responseObjf.scheduledDate_error)
                                        $('#Modal_of_scheduledDate').modal("show");
                                    }
                                    else {
                                        bootbox.alert('Something went wrong!');
                                        //location.reload();
                                    }
                                });
                            }
                        }
                    }); // Ajax Call
                }
            });

            $("#unplanMission").click(function () {
                var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
                var city = document.getElementById("city").value;

                if (mission_waybill == '') {
                    bootbox.alert('Please enter mission waybill!');
                    return;
                }

                var captain_id = prompt("Please captain ID:");
                var responseObj;
                if (captain_id != null && captain_id != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptaininfo',
                        dataType: 'text',
                        data: {captain_id: captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response == 0) {
                            bootbox.alert('No captain is registred with entered ID');
                            return;
                        }
                        else {
                            responseObj = JSON.parse(response);

                            if (confirm("Are you sure you want to un plan mission's waybill " + mission_waybill + " shipments  for captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/unplanmissiontocaptain',
                                    dataType: 'text',
                                    data: {captain_id: responseObj.id, city: city, mission_waybill: mission_waybill},
                                }).done(function (responsef) {
                                    responseObjf = JSON.parse(responsef);
                                    if (responseObjf.error) {
                                        bootbox.alert(responseObjf.error);
                                    }
                                    else if (responseObjf.unplan_message) {

                                        bootbox.alert(responseObjf.unplan_message);
                                        document.getElementById('mission_waybill').value = '';

                                        var missioninfo = responseObjf.missioninfo;
                                        var missions_datatable = $('#missions_datatable').DataTable();
                                        missions_datatable.clear().draw();
                                    } else {
                                        bootbox.alert('Something went wrong!');
                                    }
                                });
                            }
                        }
                    }); // Ajax Call
                }
            });

            $("#scanMission").click(function () {
                var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
                var city = document.getElementById("city").value;

                if (mission_waybill == '') {
                    bootbox.alert('Please enter mission waybill!');
                    return;
                }

                var captain_id = prompt("Please captain ID:");
                var responseObj;
                if (captain_id != null && captain_id != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptaininfo',
                        dataType: 'text',
                        data: {captain_id: captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response == 0) {
                            bootbox.alert('No captain is registred with entered ID');
                            return;
                        }
                        else {
                            responseObj = JSON.parse(response);

                            if (confirm("Are you sure you want to Scan mission's waybill " + mission_waybill + " shipments  to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/scanmissiontocaptain',
                                    dataType: 'text',
                                    data: {captain_id: responseObj.id, city: city, mission_waybill: mission_waybill},
                                }).done(function (responsef) {
                                    responseObjf = JSON.parse(responsef);
                                    if (responseObjf.error) {
                                        bootbox.alert(responseObjf.error);
                                    }
                                    else if (responseObjf.scan_message) {
                                        bootbox.alert(responseObjf.scan_message + responseObj.first_name + " " + responseObj.last_name);
                                        document.getElementById('mission_waybill').value = '';

                                        var missions_datatable = $('#missions_datatable').DataTable();
                                        missions_datatable.clear().draw();
                                    } else if (responseObjf.scheduledDate_error) {
                                        $('#mission_title').html(responseObjf.scheduledDate_error)
                                        $('#Modal_of_scheduledDate').modal("show");
                                    } else {
                                        bootbox.alert('Something went wrong!');
                                        //location.reload();
                                    }
                                });
                            }
                        }
                    }); // Ajax Call
                }
            });

            $("#copy_waybills").click(function(){
                var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
                var city = document.getElementById("city").value;

                if (mission_waybill == '') {
                    bootbox.alert('Please enter mission waybill!');
                    return;
                }
                
                $.ajax({
                    url: '/warehouse/copymissionwaybill/?mission_waybill=' + mission_waybill +'&city='+city,
                    dataType: 'text',
                    async: false,
                    processData: false,
                    contentType: false,
                    type: 'get',
                    success: function(data){
                        let copyFrom = document.createElement("textarea");
                        document.body.appendChild(copyFrom);
                        copyFrom.textContent = data;
                        copyFrom.select();
                        document.execCommand("copy");
                        copyFrom.remove();
                    }
                });
                alert("Copied");
            });

            $("#rollback").click(function(){

                var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
                var city = document.getElementById("city").value;

                if (mission_waybill == '') {
                    bootbox.alert('Please enter mission waybill!');
                    return;
                }
                var responseObj;

                    if (confirm("Are you sure you want to RollBack entered Mission's shipments for city "+city ) == false) {
                        bootbox.alert('You canceled!');
                        return;
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/missionshipmentsrollback',
                        dataType: 'text',
                        data: {
                            city: city,
                            mission_waybill:mission_waybill
                        },
                    }).done(function (response) {
                        responseObj = JSON.parse(response);

                        if (responseObj.error) {
                            bootbox.alert(responseObj.error);
                        } else if (responseObj.success == true) {
                            if (responseObj.count != 0) {
                                alert(responseObj.count + ' missions have been Rollback');
                                location.reload();
                            } else {
                                alert('missions not Rollback  somthing went wrong.');
                            }
                        } else {
                            bootbox.alert('Something went wrong!');
                        }

                    }); // Ajax Call

            });


            ///////////////////////////
            $("#missions_datatable").DataTable({

                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

        });

        function opensticker(waybill) {
            window.open('/warehouse/missionsticker/' + waybill, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }

        function updatescheduledDate() {
            var mission_waybill = document.getElementById("mission_waybill").value.replace(/\s+/g, '').trim();
            var city = document.getElementById("city").value;
            var dispatchDate = document.getElementById("dispatchDate").value;
            if (dispatchDate == '') {
                bootbox.alert('Please enter Scheduled Shipment date!');
                return;
            }

            if (confirm("Are you sure you want to Update Scheduled Shipment Date " + dispatchDate + " for mission's waybill " + mission_waybill + " shipments") == true) {
                $('#Modal_of_scheduledDate').modal('hide');

                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatemissionScheduledDate',
                    dataType: 'text',
                    data: {city: city, dispatchdate: dispatchDate, mission_waybill: mission_waybill},
                }).done(function (responsef) {
                    responseObjf = JSON.parse(responsef);
                    if (responseObjf.error) {
                        bootbox.alert(responseObjf.error);
                    }
                    else if (responseObjf.count) {
                        bootbox.alert('Scheduled Shipment Date have been updated for mission ' + mission_waybill);

                    } else {
                        bootbox.alert('Something went wrong!');
                        //location.reload();
                    }
                });

            } else {
                bootbox.alert('You cancled!');
            }
            $('#Modal_of_scheduledDate').modal('hide');
        }

    </script>

@stop

