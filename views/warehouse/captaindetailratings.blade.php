@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

<?php 
$layout = 'warehouse.layout';
if(Session::get('admin_role') == '9') {
    $layout = 'warehouse.cslayout';
}
else if(Session::get('admin_role') == '15') {
    $layout = 'warehouse.hr_layout';
}
?>
@extends($layout)


    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captains Detail Ratings</h3>
                </div>

            </div>

           

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">

     
                        <div class="row">

                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>
                                           <small><?php echo $name ." ". "(". $captain_id.")"; ?></small>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <p class="text-muted font-13 m-b-30">
                                            Captains Detail Ratings
                                        </p>
                                        <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->


                                        <table id="datatable_detailratings" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Waybill</th>
                                                <th>Captain Rating</th>
                                                <th>Comments</th>
                                                <th>Receiver Name</th>
                                                <th>Date</th>
                                               
                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Waybill</th>
                                                <th>Captain Rating</th>
                                                <th>Comments</th>
                                                <th>Receiver Name</th>
                                                <th>Date</th>

                                            </tr>
                                            </tfoot>
                                        </table>
                                        <!-- <script>
                                            $(document).ready(function(){
                                                $('[data-toggle="tooltip"]').tooltip();
                                            });
                                        </script> -->
                                    </div>
                                </div>
                            </div>
                            <!-- !table start -->
                 



                </div>
      
    </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script>

        <?php

        $mydata_detailratings = '';

     foreach ($detailRatings as $detailRating) {

        //removing line breaks from comments
        $comments = preg_replace( "/\r|\n/", "", $detailRating->captain_comments);
        $mydata_detailratings .= "[  \"$detailRating->captain_id\",
            \"$detailRating->waybill\", \"$detailRating->captain_rating\",\"$comments\",\"$detailRating->receiver_name\",\"$detailRating->created_at\"], ";
    }
    ?>



    $(document).ready(function() {


                    var table_activecaptains = $("#datatable_detailratings").DataTable({

                        "data": [
                            <?php echo $mydata_detailratings ?>

                            ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,

                        'order': [[5, 'desc']]
                    });

                });

    </script >


    @stop

