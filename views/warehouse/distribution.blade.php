@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Distribution for Shipment <?php echo $shipmentDate ?></h3>
                </div>

            </div>
            <!-- top tiles -->
            <div class="row tile_count">
            </div>
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>

                    <div class="count"><?php echo $allitems ?> </div>

                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> New Packages</span>

                    <div class="count"><?php echo $newitemssignin  ?> </div>
                    <span class="count_bottom">Out off <i class="green"></i> <?php echo $newitems ?> </i></span>

                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Old Packages</span>

                    <div class="count"><?php echo $olditems ?> </div>

                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Number of captains</span>

                    <div class="count green"><?php echo $numofcaptains ?> </div>

                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Number of distcicts</span>

                    <div class="count green"><?php echo $numofdistricts ?> </div>

                </div>
            </div>
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Total Cash to be collected </span>

                    <div class="count red"><?php echo number_format($cashtobecollected, 2) ?></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> New Cash </span>

                    <div class="count red"><?php echo number_format($cashtobecollectednew, 2) ?></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Old Cash </span>

                    <div class="count red"><?php echo number_format($cashtobecollectedold, 2) ?></div>
                </div>
            </div>
            <div class="row tile_count">

            </div>


            <!-- /top tiles -->


            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form method="get" action="/warehouse/work/distribution">
                            <input type="hidden" id="shipmentDate" name="shipmentDate" value="{{$shipmentDate}}"/>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" required="required" class="form-control"
                                                type="text">
                                            @foreach($companies as $compan)
                                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- bar chart -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Districts
                                <small>Histogram</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="districtschart" style="width:100%; height:280px;"></div>
                        </div>
                    </div>
                </div>
                <!-- /bar charts -->
                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <small>Unaccounted for</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                These shipmenst are supposed to be scanned but were not!
                            </p>
                            <table id="datatable_unaccounedfor" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2> No captain set</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                These items are missing thier captain information
                            </p>

                            <p id=assignToCaptain>
                                <button>Assign selected to captain</button>
                            </p>
                            <table id="datatable_nocaptain" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->
                <!-- table start -->
                <!-- style="display:none;" to be used when you want to load on closed-->

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>These shipment are for today </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that are in the Warehouse and waiting for dispatch
                            </p>
                            <table id="datatable_todays" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->


            </div>
        </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>


    <script>

        <?php





        $mydata_todays = '';
        $mydata_unaccounedfor = '';
        $mydata_nocaptain = '';
        $mydata_histogram = '';

        foreach ($districtsHist as $order) {

            $mydata_histogram .= "{ district: \"$order->d_district\", items:\"$order->num_items\" }, ";

        }
        foreach ($notsignedin as $order) {

            $mydata_unaccounedfor .= "[ \"$order->jollychic\" ,\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }


        foreach ($todayshipmentsNew as $order) {

            $mydata_todays .= "[ \"$order->jollychic\" ,\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }

        foreach ($todayshipmentsOld as $order) {

            $mydata_todays .= "[ \"$order->jollychic\" ,\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }


        foreach ($nocaptaintodayshipmentsNew as $order) {

            $mydata_nocaptain .= "[ \"$order->id\",\"$order->jollychic\" ,\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }

        foreach ($nocaptaintodayshipmentsOld as $order) {

            $mydata_nocaptain .= "[ \"$order->id\",\"$order->jollychic\" ,\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }
        ?>
        $(document).ready(function () {


                    Morris.Bar({
                        element: 'districtschart',
                        data: [
                            <?php echo $mydata_histogram ?>
                          ],
                        xkey: 'district',
                        ykeys: ['items'],
                        labels: ['Items'],
                        barRatio: 0.6,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 75,
                        hideHover: 'auto',
                        resize: true,
                    });

                    var table_unaccounedfor = $("#datatable_unaccounedfor").DataTable({

                        "data": [
                            <?php echo $mydata_unaccounedfor ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'order': [[1, 'asc']]
                    });
                    var table_todays = $("#datatable_todays").DataTable({

                        "data": [
                            <?php echo $mydata_todays ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'order': [[1, 'asc']]
                    });

                    var table_nocaptain = $("#datatable_nocaptain").DataTable({

                        "data": [
                            <?php echo $mydata_nocaptain ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }
                        ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });


                    $("#assignToCaptain").click(function () {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                                var rows_selected = table_nocaptain.column(0).checkboxes.selected();
                                var responseObj;
                                if (rows_selected.count()) {
                                    var captainNumber = prompt("Please captain phone number:");
                                    if (captainNumber != null && captainNumber != "") {
                                        $.ajax({
                                            type: 'POST',
                                            url: '/warehouse/getcaptaininfo',
                                            dataType: 'text',
                                            data: {phone: captainNumber},
                                        }).done(function (response) {
                                            console.log(response);
                                            if (response == 0) {
                                                alert('No captain is registred with entered number');
                                                return;
                                            }
                                            else {
                                                responseObj = JSON.parse(response);

                                                if (confirm("Are you sure you want to assign selcted items to captain: " + responseObj.first_name + " " + responseObj.last_name) == true) {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '/warehouse/setplannedcaptain',
                                                        dataType: 'text',
                                                        data: {captainid: responseObj.id, ids: rows_selected.join(',')},
                                                    }).done(function (response) {
                                                        alert(rows_selected.count() + ' items have been assigned to captain ' + responseObj.first_name + " " + responseObj.last_name);
                                                        //console.log(response);
                                                        if (response == 1) {
                                                            location.reload();
                                                        }
                                                    }); // Ajax Call

                                                }
                                            }
                                        }); // Ajax Call

                                    }
                                }
                                else {
                                    alert('Please select items first!');
                                    return;
                                }


                            }
                    );
                });


    </script>

    @stop

