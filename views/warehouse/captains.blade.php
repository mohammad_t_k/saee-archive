@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

<?php 
$layout = 'warehouse.layout';
if(Session::get('admin_role') == '9') {
    $layout = 'warehouse.cslayout';
}
else if(Auth::user()->role == '15') {
    $layout = 'warehouse.hr_layout';
}
?>
@extends($layout)


    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captains data</h3>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <form class="col-md-6" method="post" action="/warehouse/captainspost">
                            <h3 style="padding-left: 10px;">Filter Data</h3>
                            <input id="tabFilter" name='tabFilter' type="hidden" value="true">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">City</label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                        <input id="tabFilter" name='tabFilter' type="hidden" value="true">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Car Types</label>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <select required="" name="car_type" id="car_type" class="form-control">
                                            <option value="All">All</option>
                                            @foreach($car_types as $cartype)
                                                <option value="{{$cartype->id}}"<?php if (isset($car_type) && $car_type == $cartype->id) echo 'selected';?>>{{$cartype->english}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="row">-->
                            <!--    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">-->
                            <!--        <label class="control-label col-md-3 col-sm-3 col-xs-12">-->
                            <!--            Search-->
                            <!--            <a href="#" data-toggle="tooltip" title="Search by ID, Email or Name."><span class="glyphicon glyphicon-info-sign"></span></a>-->
                            <!--            <script>-->
                            <!--                $(document).ready(function(){-->
                            <!--                    $('[data-toggle="tooltip"]').tooltip();-->
                            <!--                });-->
                            <!--            </script>-->
                            <!--        </label>-->
                            <!--        <div class="col-md-6 col-sm-6 col-xs-12">-->
                            <!--            <input type="text" name="search" class="form-control" placeholder="ex: 1234, example@gmail.com" value="{{ $search }}" /><br/>-->
                            <!--            <label><input type="checkbox" class="checkbox-inline" name="strictmatch" value="true" >&nbsp;<span class="outside"><span class="inside"></span></span>Strict Match</label><br/>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->

                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </form>

                        <form style="border-left: 1px solid #EDEDED;" class="col-md-6" method="post" id="notification-form" action="/warehouse/SendNotificationToActiveCaptains">

                            <h3 style="padding-left: 10px;">Broadcast Message</h3>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                        <input id="tabFilter" name='tabFilter' type="hidden" value="true">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Car Type</label>
                                        <select required="" name="car_type" id="car_type" class="form-control">
                                            <option value="All">All</option>
                                            @foreach($car_types as $cartype)
                                                <option value="{{$cartype->id}}"<?php if (isset($car_type) && $car_type == $cartype->id) echo 'selected';?>>{{$cartype->english}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Send To</label>
                                        <select name="sendto" id="sendto" class="form-control">
                                           <option value="active">Active Captains</option>
                                           <option value="all">All Captains</option>
                                        </select>
                                        
                                    </div>
           
            <div class="form-group col-md-12 col-sm-12">
                <label>Message</label>
                
                <textarea class="form-control" name="message" id="message">Type Message</textarea>
            </div>
            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <p>
                                            <button id="add" class="btn btn-success">Send Message</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
        </form>
      <div id="wait" style="display:none;width:auto;height:auto;border:1px solid black;top:50%;left:50%;padding:2px;text-align: center"><img src='<?php echo asset_url(); ?>/warehouseadmin/assets/admin/Loading_icon.gif' width="64" height="64" /><br>Sending Notifications Sit Tight...</div>
                    </div>
                </div>
            </div>

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">

            <div class="row">

                <div class="tab_container">

                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1" class='label2'><i
                                class="fa fa-car"></i><span>Active</span></label>

                    <input id="tab2" type="radio" name="tabs" <?php if ($tabFilter == true) echo 'checked' ?>>
                    <label for="tab2" class='label2'><i
                                class="fa fa-car"></i><span>All</span></label>


                    <section id="content1" class="tab-content">

                        <div class="row">

                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>
                                            <small>Active Captains information</small>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <p class="text-muted font-13 m-b-30">
                                            Captains data
                                        </p>
                                        <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->


                                        <table id="datatable_activecaptains" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Name</th>
                                                <th>Nationality</th>
                                                <th>City</th>
                                                <th>Car Type</th>
                                                <th>Mobile</th>
                                                <th>With Captain</th>
                                                <th>PickedUp</th>
                                                <th>Delivered</th>
                                                <th>Covered Money</th>
                                                <th>UnDelivered</th>
                                                <th>Balance</th>
                                                <th>Success Rate</th>
                                                <th>Last Scheduled Shipment Date</th>
                                                <th>Last Payment  Date</th>
                                                <th>Pending Since</th>
                                                <th>Status</th>
                                                <th>Last Note</th>
                                                <th>Frequency</th>
                                                <th>Delayed Shipments <a data-toggle="tooltip" title="more than 3 days"><i class="glyphicon glyphicon-info-sign"></i></a></th>
                                                <th>Delayed Amount</th>
                                                <th>Captain Average Rating</th>
                                                <?php $admin_role = Session::get('admin_role');
                                                if ($admin_role != 9) {
                                                    echo " <th>Account</th>";
                                                    echo " <th>Payment</th>";
                                                    echo " <th>Edit</th>";
                                                }?>
                                                
                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Name</th>
                                                <th>Nationality</th>
                                                <th>City</th>
                                                <th>Car Type</th>
                                                <th>Mobile</th>
                                                <th>With Captain</th>
                                                <th>PickedUp</th>
                                                <th>Delivered</th>
                                                <th>Covered Money</th>
                                                <th>UnDelivered</th>
                                                <th>Balance</th>
                                                <th>Success Rate</th>
                                                <th>Last Scheduled Shipment Date</th>
                                                <th>Last Payment  Date</th>
                                                <th>Pending Since</th>
                                                <th>Status</th>
                                                <th>Last Note</th>
                                                <th>Frequency</th>
                                                <th>Delayed Shipments</th>
                                                <th>Delayed Amount</th>
                                                <th>Captain Average Rating</th>
                                                <?php $admin_role = Session::get('admin_role');
                                                if ($admin_role != 9) {
                                                    echo " <th>Account</th>";
                                                    echo " <th>Payment</th>";
                                                    echo " <th>Edit</th>";
                                                }?>

                                            </tr>
                                            </tfoot>
                                        </table>
                                        <!-- <script>
                                            $(document).ready(function(){
                                                $('[data-toggle="tooltip"]').tooltip();
                                            });
                                        </script> -->
                                    </div>
                                </div>
                            </div>
                            <!-- !table start -->
                    </section>


                    <section id="content2" class="tab-content">

                        <div class="row">

                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>
                                            <small>All Captains information</small>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <p class="text-muted font-13 m-b-30">
                                            Captains data
                                        </p>
                                        <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->


                                        <table id="datatable_captains" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Created at</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Nationality</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Car Type</th>
                                                <th>Mobile</th>
                                                <th>Status</th>
                                                <th>Last Note</th>
                                                <th>Coming From</th>
                                                <?php $admin_role = Session::get('admin_role');
                                                if ($admin_role != 9) {
                                                    echo " <th>Edit</th>";
                                                }?>
                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th>Captain ID</th>
                                                <th>Created at</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Nationality</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Car Type</th>
                                                <th>Mobile</th>
                                                <th>Status</th>
                                                <th>Last Note</th>
                                                <th>Coming From</th>
                                                <?php $admin_role = Session::get('admin_role');
                                                if ($admin_role != 9) {
                                                    echo " <th>Edit</th>";
                                                }?>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- !table start -->
                    </section>

                </div>
            </div>

            <?php //echo $captainsdata ?>


        </div>
    </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

     <script type="text/javascript">
        $("#message").validate({
            rules: {
                name: "required",
            }
        });
    </script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script>

        <?php


        $mydata_captainsdata = '';
        $mydata_activecaptainsdata = '';

        $admin_role = Session::get('admin_role');



        foreach ($captainsdata as $aCaptain) {
        $edit = "";
        if ($admin_role != 9) {
            $edit =",   \"<a href=\'/warehouse/editcaptain?id=$aCaptain->id\' target='_blank'>Edit</a>\"";
        }
        $status = $aCaptain->is_approved ? "Approved" : "Not Approved";
        
        $aCaptain->last_note = str_replace("\t", ' ', $aCaptain->last_note); 
        $aCaptain->last_note = str_replace("\n", '<br/>', $aCaptain->last_note); 
        $aCaptain->last_note = str_replace("\r", ' ', $aCaptain->last_note); 
        
        $mydata_captainsdata .= "[  \"$aCaptain->id\", \"$aCaptain->created_at\" ,\"$aCaptain->email\" ,\"".$aCaptain->first_name." ".$aCaptain->last_name."\",
            \"$aCaptain->nationality\", \"$aCaptain->city\", \"$aCaptain->subcity\", \"$aCaptain->car_type\",\"$aCaptain->phone\", \"$status\",\"$aCaptain->last_note\", \"$aCaptain->coming_from\"$edit], ";
    }


     foreach ($activecaptainsdata as $activeCaptain) {

            if(empty($activeCaptain->captain_avg_rating)){

                $activeCaptain->captain_avg_rating = 'Not Rated Yet';
            }

            if ($activeCaptain->delivered + $activeCaptain->returned) {
                    $activeCaptain->successRate = round($activeCaptain->delivered / ($activeCaptain->delivered + $activeCaptain->returned),2);
                } else {
                    $activeCaptain->successRate = 0;
                }
        $accounting = "";
        $payment = "";
        $edit = "";
        if ($admin_role != 9) {
            $accounting = ",   \"<a href=\'/warehouse/captainaccounting?captain_id=$activeCaptain->id\' target='_blank'>Accounting</a>\"";
            $payment = ",   \"<a href=\'/warehouse/captainpaymentreport?captain_id=$activeCaptain->id\' target='_blank'>Payment</a>\"";
            $edit =",   \"<a href=\'/warehouse/editcaptain?id=$activeCaptain->id\' target='_blank'>Edit</a>\"";
        }
        if(!isset($activeCaptain->lastpaymentdate))
            $activeCaptain->lastpaymentdate='';
            if(!isset($activeCaptain->covered_amount)) { $covered_money=0;}
            else{ $covered_money=round($activeCaptain->covered_amount, 2);}
        if($activeCaptain->pending_since == '0000-00-00 00:00:00')
            $activeCaptain->pending_since = '';
        $activeCaptain->delayed_amount = number_format($activeCaptain->delayed_amount, 2);
        if($activeCaptain->captain_avg_rating != 'Not Rated Yet')
            $activeCaptain->captain_avg_rating = number_format($activeCaptain->captain_avg_rating, 2);
        $status = $activeCaptain->is_approved ? "Approved" : "Not Approved";

        $activeCaptain->last_note = str_replace("\t", ' ', $activeCaptain->last_note); 
        $activeCaptain->last_note = str_replace("\n", '<br/>', $activeCaptain->last_note); 
        $activeCaptain->last_note = str_replace("\r", ' ', $activeCaptain->last_note); 
        
        $mydata_activecaptainsdata .= "[  \"$activeCaptain->id\" ,\"".$activeCaptain->first_name." ".$activeCaptain->last_name."\",
            \"$activeCaptain->nationality\", \"$activeCaptain->city\", \"$activeCaptain->car_type\",\"$activeCaptain->phone\", \"$activeCaptain->withcaptain\", \"$activeCaptain->picked_up\", \"$activeCaptain->delivered\", \"$covered_money\", \"$activeCaptain->returned\",
            \"$activeCaptain->balance\",\"$activeCaptain->successRate\",\"$activeCaptain->lastscheduledshipmentdate\",\"$activeCaptain->lastpaymentdate\",\"$activeCaptain->pending_since\",\"$status\",\"$activeCaptain->last_note\",\"$activeCaptain->frequency\",\"$activeCaptain->delayed_count\",\"$activeCaptain->delayed_amount\",\"<a href=\'/warehouse/captaindetailratings?captain_id=$activeCaptain->id\' target='_blank'>$activeCaptain->rate</a>\"$accounting$payment$edit], ";
    }
    ?>



    $(document).ready(function() {

        var table_captains = $("#datatable_captains").DataTable({

            "data": [
                <?php echo $mydata_captainsdata ?>

                ],
            "autoWidth": false,
            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[1, 'desc']]

            
        });


        var table_activecaptains = $("#datatable_activecaptains").DataTable({

            "data": [
                <?php echo $mydata_activecaptainsdata ?>

                ],
            "autoWidth": false,
            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[12, 'desc']]
        });

    });

    </script>

    <script type="text/javascript">
    var frm = $('#notification-form');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({


            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            beforeSend: function(){
                // Handle the beforeSend event
             $("#wait").css("display", "inline-block");
            },
            complete: function(){
            // Handle the complete event
            $("#wait").css("display", "none");
            },
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
                  if(data.success){
                alert('Notifications Sent');
            }else{

                alert(data.message);
            }
               
            },
            error: function (data) {
                console.log('An error occurred.');
                alert('Error notifications not sent');
                console.log(data);
            },
        });
    });
</script>

    @stop

