@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends('warehouse.layout')

@section('content')
    
    <style>
        td {
            text-align: center;
        }
        th {
            text-align: center;
        }
        .map-search-box {
            left: 500px;
            top: 55px;
            z-index: 1;
        }
    </style>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>{{ $hub->name }} Details</h3>
                    <div class="x_panel">
                        <div class="x_title">
                            <form action="/warehouse/hub/edit/{{ $hub->id }}" method="post" id="form">
                                <input type="hidden" name="form" value="true" />
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <th class="col-md-2 col-xs-6">ID</th>
                                            <td class="col-md-4 col-xs-6">{{ $hub->id }}</td>
                                            <th class="col-md-2 col-xs-6">Owner</th>
                                            <td class="col-md-4 col-xs-6">
                                                <select name="owner_type" id="owner_id" class="form-control col-md-12 col-xs-12">
                                                    @foreach($owners as $owner)
                                                        <option value="{{ $owner->id }}" {{ $hub->owner_type == $owner->id ? 'selected' : '' }}>{{ $owner->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <td>
                                                <input type="text" name="name" id="name" value="{{ $hub->name }}" class="form-control col-md-12 col-xs-12" />
                                            </td>
                                            <th>Arabic Name</th>
                                            <td>
                                                <input type="text" name="name_ar" id="name_ar" value="{{ $hub->name_ar }}" class="form-control col-md-8 col-xs-12" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Manager</th>
                                            <td>
                                                <select name="manager_id" id="manager_id" class="form-control col-md-12 col-xs-12">
                                                    @foreach($admins as $admin)
                                                        <option value="{{ $admin->id }}" {{ $hub->manager_id == $admin->id ? 'selected' : '' }}>{{ $admin->username }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <th>Country</th>
                                            <td>
                                                <input type="text" name="country" id="country" value="{{ $hub->country }}" class="form-control col-md-12 col-xs-12" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>State</th>
                                            <td>
                                                <input type="text" name="state" id="state" value="{{ $hub->state }}" class="form-control col-md-12 col-xs-12" />
                                            </td>
                                            <th>City</th>
                                            <td>
                                                <select name="city" id="city" class="form-control col-md-12 col-xs-12" onchange="onChangeCity()">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>District</th>
                                            <td>
                                                <select name="district" id="district" class="form-control col-md-12 col-xs-12">
                                                </select>
                                            </td>
                                            <th>Address</th>
                                            <td>
                                                <input type="text" name="address" id="address" value="{{ $hub->address }}" class="form-control col-md-12 col-xs-12" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td>
                                                <input type="text" name="phone" id="phone" value="{{ $hub->phone }}" class="form-control col-md-8 col-xs-12" />
                                            </td>
                                            <th>Enable Missions Creation</th>
                                            <td>
                                                <select name="enable_missions_creation" id="enable_missions_creation" class="form-control col-md-12 col-xs-12">
                                                    <option value="0" {{ $hub->enable_missions_creation == '0' ? 'selected' : '' }}>No</option>
                                                    <option value="1" {{ $hub->enable_missions_creation == '1' ? 'selected' : '' }}>Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Created At</th>
                                            <td colspan="2">{{ $hub->created_at }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <?php $secret = Settings::where('key', '=', 'google_api_key')->select('value')->first()->value; ?>
                                                <script src="https://maps.googleapis.com/maps/api/js?key={{ $secret }}&callback=initMap&libraries=places" async defer></script>
                                                <div class="col-md-3 map-search-box">
                                                    <input id="pac-input" class="form-control" type="text" placeholder="Search Box">    
                                                </div>
                                                <div id="map_canvas" style="margin: 10px;"></div>
                                                <input id="address_latitude" name="address_latitude" hidden />
                                                <input id="address_longitude" name="address_longitude" hidden />
                                                <script>
                                                    function handleMarkerMove(event) {
                                                        document.getElementById('address_latitude').value = event.latLng.lat();
                                                        document.getElementById('address_longitude').value = event.latLng.lng();
                                                    }
                                                    function initMap() {
                                                        try {
                                                            var address_latitude = {{ $hub->latitude }};
                                                            var address_longitude = {{ $hub->longitude }};
                                                            document.getElementById('address_latitude').value = address_latitude
                                                            document.getElementById('address_longitude').value = address_longitude;
                                                            var center = new google.maps.LatLng(address_latitude, address_longitude);
                                                            var trackingnum = '';
                                                            var mapOptions = {
                                                                zoom: 15,
                                                                center: center,
                                                            };
                                                            var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

                                                            // marker
                                                            var marker = new google.maps.Marker({
                                                                position: center,
                                                                map: map,
                                                                title: 'Hub Location',
                                                                draggable: true
                                                            });
                                                            marker.addListener('drag', handleMarkerMove);
                                                            marker.addListener('dragend', handleMarkerMove); 

                                                            document.getElementById('map_canvas').style = 'height:400px; width: 100%;';
                                                            var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
                                                            google.maps.event.addListener(searchBox, 'places_changed', function() {
                                                                var places = searchBox.getPlaces();
                                                                var bounds = new google.maps.LatLngBounds();
                                                                var i, place;
                                                                for(var i = 0; place = places[i]; ++i) {
                                                                    bounds.extend(place.geometry.location);
                                                                    marker.setPosition(place.geometry.location);
                                                                    document.getElementById('address_latitude').value = place.geometry.location.lat();
                                                                    document.getElementById('address_longitude').value = place.geometry.location.lng();
                                                                }
                                                                map.fitBounds(bounds);
                                                                map.setZoom(15);
                                                            });
                                                            infoWindow = new google.maps.InfoWindow;
                                                            // google.maps.event.addListener(map, 'center_changed', function () {
                                                            //     address_latitude = document.getElementById('address_latitude').value = center.lat();
                                                            //     address_longitude = document.getElementById('address_longitude').value = center.lng();
                                                            // });
                                                            $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                                                            //do something onclick
                                                                .click(function () {
                                                                    var that = $(this);
                                                                    if (!that.data('win')) {
                                                                        that.data('win', new google.maps.InfoWindow({
                                                                            content: 'this is the center'
                                                                        }));
                                                                        that.data('win').bindTo('position', map, 'center');
                                                                    }
                                                                    that.data('win').open(map);
                                                                });
                                                        }
                                                        catch (e) {
                                                            alert(e);
                                                        }
                                                    }
                                                </script>
                                                <script>
                                                    //google.maps.event.addDomListener(window, 'load', initMap);
                                                    window.addEventListener("load", initMap);
                                                </script>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input type="submit" value="Submit" class="btn btn-primary col-md-2 col-md-offset-5" /> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
    </script>

    <script>
        // fill cities
        $(document).ready(function(){
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text'
            }).done(function(response){
                responseObj = JSON.parse(response);
                if(responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for(var i = 0; i < cities.length; ++i) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        if("{{ $hub->city }}" == cities[i].name)
                            option.setAttribute('selected', 'selected');
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }

                    city = "{{ $hub->city }}";
                    $.ajax({
                        type: 'GET',
                        url: '/deliveryrequest/getdistrictsbycity',
                        dataType: 'text',
                        data: {city: city},
                    }).done(function (response) {
                        responseObj = JSON.parse(response);
                        if (responseObj.city == city) {
                            var districts = responseObj.districts;
                            var select = document.getElementById('district');
                            for (var i = 0; i < districts.length; i += 1) {
                                if(districts[i].district == 'No District')
                                    continue;
                                // console.log(districts[i]);
                                option = document.createElement('option');
                                option.setAttribute('value', districts[i].district);
                                option.appendChild(document.createTextNode(districts[i].district + ' - ' + districts[i].district_ar));
                                select.appendChild(option);
                            }
                        }
                    });

                }
            });
        });
    </script>

    <script>
        function onChangeCity() {
            $("#district").empty();
            var city = document.getElementById('city').value;
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    for (var i = 0; i < districts.length; i += 1) {
                        if(districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        option.appendChild(document.createTextNode(districts[i].district + ' - ' + districts[i].district_ar));
                        select.appendChild(option);
                    }
                }
            });
        }
    </script>

@stop

