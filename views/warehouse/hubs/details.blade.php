@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends('warehouse.layout')

@section('content')
    
    <style>
        td {
            text-align: center;
        }
        th {
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="float: left;">
                        <h3>{{ $hub->name }} Details</h3>
                    </div>
                    <div style="float: right;">
                        <span id="edit_hub" class="btn btn-primary">Edit</span>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="x_panel">
                        <div class="x_title">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th class="col-md-2 col-xs-6">ID</th>
                                        <td class="col-md-4 col-xs-6">{{ $hub->id }}</td>
                                        <th class="col-md-2 col-xs-6">Owner</th>
                                        <td class="col-md-4 col-xs-6">{{ $hub->owner }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $hub->name }}</td>
                                        <th>Arabic Name</th>
                                        <td>{{ $hub->name_ar }}</td>
                                    </tr>
                                    <tr>
                                        <th>Manager</th>
                                        <td>{{ $hub->username }}</td>
                                        <th>Country</th>
                                        <td>{{ $hub->country }}</td>
                                    </tr>
                                    <tr>
                                        <th>State</th>
                                        <td>{{ $hub->state }}</td>
                                        <th>City</th>
                                        <td>{{ $hub->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>District</th>
                                        <td>{{ $hub->district }}</td>
                                        <th>Address</th>
                                        <td>{{ $hub->address }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $hub->phone }}</td>
                                        <th>Enable Missions Creation</th>
                                        <td>{{ $hub->enable_missions_creation == 0 ? 'No' : 'Yes' }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Created At</th>
                                        <td colspan="2">{{ $hub->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div id="map_canvas" style="margin: 10px;"></div>
                                            <input id="address_latitude" name="address_latitude" hidden />
                                            <input id="address_longitude" name="address_longitude" hidden />
                                            <?php $secret = Settings::where('key', '=', 'google_api_key')->select('value')->first()->value; ?>
                                            <script src="https://maps.googleapis.com/maps/api/js?key={{ $secret }}&callback=initMap" async defer></script>
                                            <script>
                                                function initMap() {
                                                    try {
                                                        var address_latitude = {{ $hub->latitude }};
                                                        var address_longitude = {{ $hub->longitude }};
                                                        var center = new google.maps.LatLng(address_latitude, address_longitude);
                                                        var trackingnum = '';
                                                        var mapOptions = {
                                                            zoom: 15,
                                                            center: center,
                                                        };
                                                        document.getElementById('map_canvas').style = 'height:300px';
                                                        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                                                        var marker = new google.maps.Marker({
                                                            position: center,
                                                            map: map,
                                                            title: 'Hub Location'
                                                        });
                                                        infoWindow = new google.maps.InfoWindow;
                                                        $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                                                        //do something onclick
                                                            .click(function () {
                                                                var that = $(this);
                                                                if (!that.data('win')) {
                                                                    that.data('win', new google.maps.InfoWindow({
                                                                        content: 'this is the center'
                                                                    }));
                                                                    that.data('win').bindTo('position', map, 'center');
                                                                }
                                                                that.data('win').open(map);
                                                            });
                                                    }
                                                    catch (e) {
                                                        alert(e);
                                                    }
                                                }
                                            </script>
                                            <script>
                                                //google.maps.event.addDomListener(window, 'load', initMap);
                                                window.addEventListener("load", initMap);
                                            </script>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#edit_hub").on('click', function(){
            window.location = "/warehouse/hub/edit/{{ $hub->id }}";
        });
    </script>

@stop

