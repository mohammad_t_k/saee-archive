@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends('warehouse.layout')

@section('content')
    
    <style>
        td {
            text-align: center;
        }
        th {
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="float: left;">
                        <h3>All Registered Hubs</h3>
                    </div>
                    <div style="float: right;">
                        <span id="add_new_hub" class="btn btn-primary">Add New Hub</span>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="x_panel">
                        <div class="x_title">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Owner</th>
                                        <th>Name</th>
                                        <th>Arabic Name</th>
                                        <th>Manager</th>
                                        <th>Country</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>District</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Enable Missions Creation</th>
                                        <th>Created At</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($hubs as $hub)
                                        <tr>
                                            <td>{{ $hub->id }}</td>
                                            <td>{{ $hub->owner }}</td>
                                            <td>{{ $hub->name }}</td>
                                            <td>{{ $hub->name_ar }}</td>
                                            <td>{{ $hub->username }}</td>
                                            <td>{{ $hub->country }}</td>
                                            <td>{{ $hub->state }}</td>
                                            <td>{{ $hub->city }}</td>
                                            <td>{{ $hub->district }}</td>
                                            <td>{{ $hub->address }}</td>
                                            <td>{{ $hub->phone }}</td>
                                            <td>{{ $hub->enable_missions_creation == 0 ? 'No' : 'Yes' }}</td>
                                            <td>{{ $hub->created_at }}</td>
                                            <td><a href='/warehouse/hub/view/{{ $hub->id }}/details'>Show</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Owner</th>
                                        <th>Name</th>
                                        <th>Arabic Name</th>
                                        <th>Manager</th>
                                        <th>Country</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>District</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Enable Missions Creation</th>
                                        <th>Created At</th>
                                        <th>Details</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#add_new_hub").on('click', function(){
            window.location = "/warehouse/hub/";
        });
    </script>

@stop

