@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends('warehouse.layout') 

@section('content')

    <style>
        .map-search-box {
            left: 500px;
            top: 55px;
            z-index: 1;
        }
    </style>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="clearfix"></div><br/>
                <h3 class="col-md-offset-1">Add New Hub</h3><br/>
                <form action="/warehouse/hub/add" method="post" id="form">
                    <input type="hidden" name="form" value="true" />
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">Owner</label>
                        <div class="col-md-8 col-xs-12">
                            <select name="owner_type" id="owner_id" class="form-control col-md-8 col-xs-12">
                                @foreach($owners as $owner)
                                    <option value="{{ $owner->id }}">{{ $owner->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-xs-12">Name</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="name" id="name" class="form-control col-md-8 col-xs-12" />
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">Arabic Name</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="name_ar" id="name_ar" class="form-control col-md-8 col-xs-12" />
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-xs-12">Manager</label>
                        <div class="col-md-8 col-xs-12">
                            <select name="manager_id" id="manager_id" class="form-control col-md-8 col-xs-12">
                                @foreach($admins as $admin)
                                    <option value="{{ $admin->id }}">{{ $admin->username }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">Country</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="country" id="country" class="form-control col-md-8 col-xs-12" value="Saudi Arabia" />
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-xs-12">State</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="state" id="state" class="form-control col-md-8 col-xs-12" />
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">City</label>
                        <div class="col-md-8 col-xs-12">
                            <select name="city" id="city" class="form-control col-md-8 col-xs-12" onchange="onChangeCity()">
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-xs-12">District</label>
                        <div class="col-md-8 col-xs-12">
                            <select name="district" id="district" class="form-control col-md-8 col-xs-12">
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">Address</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="address" id="address" class="form-control col-md-8 col-xs-12" />
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-xs-12">Phone</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" name="phone" id="phone" class="form-control col-md-8 col-xs-12" />
                        </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                        <label class="col-md-3 col-xs-12">Enable Missions Creation</label>
                        <div class="col-md-8 col-xs-12">
                            <select name="enable_missions_creation" id="enable_missions_creation" class="form-control col-md-8 col-xs-12">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div><br/><br/>
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
                        <?php $secret = Settings::where('key', '=', 'google_api_key')->select('value')->first()->value; ?>
                        <script src="https://maps.googleapis.com/maps/api/js?key={{ $secret }}&callback=initMap&libraries=places" async defer></script>
                        <div class="col-md-3 map-search-box">
                            <input id="pac-input" class="form-control" type="text" placeholder="Search Box">    
                        </div>
                        <div id="map_canvas" style="margin: 10px; width: 100%;"></div>
                        <input id="address_latitude" name="address_latitude" hidden />
                        <input id="address_longitude" name="address_longitude" hidden />
                        <script>
                            function handleMarkerMove(event) {
                                document.getElementById('address_latitude').value = event.latLng.lat();
                                document.getElementById('address_longitude').value = event.latLng.lng();
                            }
                            function initMap() {
                                try {
                                    var address_latitude = 21.5912954629946;
                                    var address_longitude = 39.175945799537544;
                                    document.getElementById('address_latitude').value = address_latitude
                                    document.getElementById('address_longitude').value = address_longitude;
                                    var center = new google.maps.LatLng(address_latitude, address_longitude);
                                    var trackingnum = '';
                                    var mapOptions = {
                                        zoom: 15,
                                        center: center,
                                    };
                                    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

                                    var marker = new google.maps.Marker({
                                        position: center,
                                        map: map,
                                        title: 'Hub Location',
                                        draggable: true
                                    });
                                    marker.addListener('drag', handleMarkerMove);
                                    marker.addListener('dragend', handleMarkerMove); 

                                    document.getElementById('map_canvas').style = 'height:400px; width: 100%;';
                                    var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
                                    google.maps.event.addListener(searchBox, 'places_changed', function() {
                                        var places = searchBox.getPlaces();
                                        var bounds = new google.maps.LatLngBounds();
                                        var i, place;
                                        for(var i = 0; place = places[i]; ++i) {
                                            bounds.extend(place.geometry.location);
                                            marker.setPosition(place.geometry.location);
                                            document.getElementById('address_latitude').value = place.geometry.location.lat();
                                            document.getElementById('address_longitude').value = place.geometry.location.lng();
                                        }
                                        map.fitBounds(bounds);
                                        map.setZoom(15);
                                    });
                                    infoWindow = new google.maps.InfoWindow;
                                    // google.maps.event.addListener(map, 'center_changed', function () {
                                    //     var center = map.getCenter();
                                    //     marker.setPosition(center);
                                    //     address_latitude = document.getElementById('address_latitude').value = center.lat();
                                    //     address_longitude = document.getElementById('address_longitude').value = center.lng();
                                    // });
                                    $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                                    //do something onclick
                                        .click(function () {
                                            var that = $(this);
                                            if (!that.data('win')) {
                                                that.data('win', new google.maps.InfoWindow({
                                                    content: 'this is the center'
                                                }));
                                                that.data('win').bindTo('position', map, 'center');
                                            }
                                            that.data('win').open(map);
                                        });
                                }
                                catch (e) {
                                    alert(e);
                                }
                            }
                        </script>
                        <script>
                            //google.maps.event.addDomListener(window, 'load', initMap);
                            window.addEventListener("load", initMap);
                        </script>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-1">
                        <div class="col-md-8 col-xs-12 col-md-offset-4">
                            <input type="submit" value="Submit" class="btn btn-primary col-md-3" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
    </script>

    <script>
        // fill cities
        $(document).ready(function(){
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text'
            }).done(function(response){
                responseObj = JSON.parse(response);
                if(responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for(var i = 0; i < cities.length; ++i) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }

                    city = cities[0].name;
                    $.ajax({
                        type: 'GET',
                        url: '/deliveryrequest/getdistrictsbycity',
                        dataType: 'text',
                        data: {city: city},
                    }).done(function (response) {
                        responseObj = JSON.parse(response);
                        if (responseObj.city == city) {
                            var districts = responseObj.districts;
                            var select = document.getElementById('district');
                            for (var i = 0; i < districts.length; i += 1) {
                                if(districts[i].district == 'No District')
                                    continue;
                                // console.log(districts[i]);
                                option = document.createElement('option');
                                option.setAttribute('value', districts[i].district);
                                option.appendChild(document.createTextNode(districts[i].district));
                                select.appendChild(option);
                            }
                        }
                    });

                }
            });
        });
    </script>

    <script>
        function onChangeCity() {
            $("#district").empty();
            var city = document.getElementById('city').value;
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    for (var i = 0; i < districts.length; i += 1) {
                        if(districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        option.appendChild(document.createTextNode(districts[i].district));
                        select.appendChild(option);
                    }
                }
            });
        }
    </script>

@stop

