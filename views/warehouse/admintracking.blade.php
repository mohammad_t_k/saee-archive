@if( Auth::user()->role < 2)

    <script>window.location = "/warehouse/403";</script>

    @endif
<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 2)
    $layout = 'warehouse.jclayout';
else
    $layout = 'warehouse.layout';
?>

    @extends($layout)


    @section('content')


            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Order Tracking</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <form id="searching" class="form-horizontal form-label-left" validate
                                  action="/warehouse/admintracking" method="get">
                                <span class="section">Tracking Number: <?php echo $waybill ?> </span>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Tracking
                                        Number <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="trackingnum" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <input type='submit' value='search' class="btn  btn-success"/>
                                    </div>
                                </div>
                            </form>
                            <h2>
                                <?php
                                echo $finalstaustxt
                                ?>
                            </h2>

                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar"
                                     data-transitiongoal= <?php echo $finalstausnum ?> ></div>
                            </div>


                            <table id="datatable_history" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Date and Time</th>
                                    <th>Company Name</th>
                                    <th>Status</th>
                                    <th>Captain Name</th>
                                    <th>Mobile</th>
                                    <th>Customer Name</th>
                                    <th>Admin Username</th>
                                    <th>City</th>
                                    <th>Remarks</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Date and Time</th>
                                    <th>Company Name</th>
                                    <th>Status</th>
                                    <th>Captain Name</th>
                                    <th>Mobile</th>
                                    <th>Customer Name</th>
                                    <th>Admin Username</th>
                                    <th>City</th>
                                    <th>Remarks</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

            </div>

        </div>

    </div>

    <!-- /page content -->



    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script>

        <?php


        $mydata_history = '';

        foreach ($history as $astep)
        {
            $captain_name = $astep->first_name . ' '. $astep->last_name;
            if ($astep->sender_name)
            $companyName = $astep->company_name.' ['.$astep->sender_name.']';
            else
            $companyName = $astep->company_name;

            if(isset($astep->substatustxt)) {
                $astep->statustxt .= "/" . $astep->substatustxt;
            }

            $mydata_history .= "[  \"$astep->created_at\" ,\"$companyName\" ,\"$astep->statustxt\" ,\"$captain_name\", \"$astep->phone\" ,\"$astep->receiver_name\", \"$astep->username\", \"$astep->city\",
            \"$astep->notes\"], ";

        }


        ?>



        $(document).ready(function () {


                    var table_history = $("#datatable_history").DataTable({

                        "data": [
                            <?php echo $mydata_history ?>

                          ],
                        "autoWidth": false,
                        dom: "rt",
                        pageLength: "50",

                        responsive: true,
                        'order': [[0, 'desc']]
                    });


                });

    </script>

    @stop

