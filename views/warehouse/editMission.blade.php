@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->
    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Modify Mission</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Modify mission for a Group of Shipments</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <select name="city" id="city" class="form-control">
                                                @foreach($adminCities as $adcity)
                                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mission waybill<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input value="" type="text" name="mission_waybill" id="mission_waybill"
                                               style="padding: 6px 0px;"
                                               onchange="checkmissioninfo(this.value)">

                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <textarea id="shipmentIDs" required="required" class="form-control" type="text"
                                                  rows="10" disabled></textarea>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                        <h5>Count = <span id="linesUsed">0</span></h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="assignShipments" class="btn btn-success">Assign</button>
                                            {{--<button id="submitForm" class="btn btn-success">UnAssign</button>--}}
                                            <button id="resetform" class="btn btn-success">Reset</button>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="x_title">
                        <h2>Mission Detail</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">

                            <table id="missions_datatable" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Mission Name</th>
                                    <th>City</th>
                                    <th>Districts</th>
                                    <th>Number of shipments</th>
                                    <th>Estimated Delivery Fee</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Mission Waybill</th>
                                    <th>Mission Name</th>
                                    <th>City</th>
                                    <th>Districts</th>
                                    <th>Number of shipments</th>
                                    <th>Estimated Delivery Fee</th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>

        $(document).ready(function () {
            var linesUsed = $('#linesUsed');
            $('#shipmentIDs').keydown(function (e) {
                newLines = $(this).val().split("\n").length;
                linesUsed.text(newLines);
            });
            //Reset Form
            $("#resetform").click(function () {
                var mission_waybill = document.getElementById("mission_waybill");
                mission_waybill.value = '';
                mission_waybill.focus();
                var shipmentIDs = document.getElementById('shipmentIDs')
                shipmentIDs.value = '';
                shipmentIDs.disabled = true;
                document.getElementById("linesUsed").innerHTML = 0;
                var missions_datatable = $('#missions_datatable').DataTable();
                missions_datatable.clear().draw();
            });

            $("#assignShipments").click(function () {
                var mission_waybill = document.getElementById("mission_waybill").value;
                var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                var city = document.getElementById("city").value;

                if (shipIDs == '') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');
                for (i = 0; i < ids.length; ++i) {
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]))
                        ids[i] = ids[i].toUpperCase();
                }

                var wrongIDs = Array();
                var correctIDs = Array();
                var finalArray = [];
                for (var i = 0; i < ids.length; i++) {
                    if (!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function (i, el) {
                    if ($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });


                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    return;
                }

                if (finalArray.length) {

                    if (confirm("Are you sure you want to Add " + finalArray.length + " shipments in  mission  " + mission_waybill + "  of city :" + city) == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/assignshipmentstomission',
                            dataType: 'text',
                            data: {city: city, mission_waybill: mission_waybill, ids: finalArray.join(",")},
                        }).done(function (response) {
                            responseObjf = JSON.parse(response);
                            if (responseObjf.error) {
                                bootbox.alert(responseObjf.error);
                            }

                            else if (responseObjf.count) {
                                bootbox.alert('Mission have been modify for ' + responseObjf.count + ' shipments');

                                var shipmentIDs = document.getElementById('shipmentIDs')
                                shipmentIDs.value = '';
                                shipmentIDs.disabled = true;
                                document.getElementById("linesUsed").innerHTML = 0


                                var missioninfo = responseObjf.missioninfo;

                                var missions_datatable = $('#missions_datatable').DataTable();
                                missions_datatable.clear().draw();
                                var waybill = missioninfo.mission_waybill;
                                missions_datatable.row.add([
                                            missioninfo.id,
                                            '<a onclick=opensticker("' + waybill + '")>' + waybill + '</a>',
                                            missioninfo.mission_name,
                                            missioninfo.city,
                                            missioninfo.district,
                                            missioninfo.total_shipments,
                                            missioninfo.estimated_delivery_fee]
                                ).draw();

                            } else {
                                bootbox.alert('Something went wrong!');
                                //location.reload();
                            }
                        });
                    } else {
                        bootbox.alert('You cancled!');
                    }
                }
            });

            ///////////////////////////
            $("#missions_datatable").DataTable({

                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

        });
        function checkmissioninfo(mission_waybill) {
            var city = document.getElementById("city").value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/missioninfo',
                dataType: 'text',
                data: {mission_waybill: mission_waybill, city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.error) {
                    bootbox.alert(responseObj.error);
                } else {
                    document.getElementById('shipmentIDs').disabled = false;
                    if (responseObj.count) {
                        document.getElementById('shipmentIDs').disabled = false;
                        document.getElementById('shipmentIDs').value = '';
                        var missioninfo = responseObj.missioninfo;
                        var missions_datatable = $('#missions_datatable').DataTable();
                        missions_datatable.clear().draw();
                        var waybill = missioninfo.mission_waybill;
                        missions_datatable.row.add([
                                    missioninfo.id,
                                    '<a onclick=opensticker("' + waybill + '")>' + waybill + '</a>',
                                    missioninfo.mission_name,
                                    missioninfo.city,
                                    missioninfo.district,
                                    missioninfo.total_shipments,
                                    missioninfo.estimated_delivery_fee]
                        ).draw();

                    } else {
                    }
                }
            })

        }

        function opensticker(waybill) {
            window.open('/warehouse/missionsticker/' + waybill, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }

    </script>

    @stop

