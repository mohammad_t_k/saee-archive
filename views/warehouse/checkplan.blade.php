@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Check Shipments Assignment</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Check who is planned to take entered shipments</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20" ></textarea>
                        </div>
                    </div>
                    
                   
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <p><button id="submitForm"  class="btn btn-success">Check</button>  </p>
                        </div>
                    </div>
                    
                     <table id="datatable_plan" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>Waybill</th>
                          <th>Order Number</th>  
                          <th>Receiver Name</th>
                          <th>Planned Captain</th>
                          <th>Confirmed Captain</th>
                          <th>Quantity</th>
                            <th>City</th>
                        </tr>
                      </thead>
                      
                      
                      <tfoot>
                        <tr>
                          <th>Waybill</th>
                          <th>Order Number</th>  
                          <th>Receiver Name</th>
                          <th>Planned Captain</th>
                          <th>Confirmed Captain</th>
                          <th>Quantity</th>
                            <th>City</th>
                        </tr>
                      </tfoot>
                    </table>
                    
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
     
            
            
          </div>
        </div>
        <!-- /page content -->
        
          <!-- jQuery k-w-h.com/app/views/warehouse/vendors --> 
   
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>



<?php
       
            
$mydata_plan = '';



foreach ($items as $anitem) 
{

    $waybill = $anitem['jollychic'];
    $order_number = $anitem['order_number'];
    $confirmedcaptain='';
    if (count($anitem['confirm_name'])) {
        $confirmedcaptain = $anitem['confirm_name'];
        $confirmedcaptain .= " 0";
        $confirmedcaptain .= substr(str_replace(' ', '', $anitem['confirm_phone']), -9);
    }
    $plannedcaptain = '';
    if (count($anitem['plan_name'])) {
        $plannedcaptain = $anitem['plan_name'];
        $plannedcaptain .= " 0";
        $plannedcaptain .= substr(str_replace(' ', '', $anitem['plan_phone']), -9);
    }
    $city               = $anitem['d_city'];
    $receiver_name    = $anitem['receiver_name'];
    $d_quantity         = $anitem['d_quantity'];

    $mydata_plan .= "[  \"$waybill\", \"$order_number\", \"$receiver_name\" ,\"$plannedcaptain\" ,  \"$confirmedcaptain\", \"$d_quantity\" , \"$city\"], ";

}


?>

<script  >




$(document).ready(function() {
    
    
    var table_plan = $("#datatable_plan").DataTable({
       aLengthMenu: [
            [10,25, 50, 100],
            [10,25, 50, 100]
        ],
        iDisplayLength: 25,
    
    "data": [
      <?php echo $mydata_plan ?> 
      
    ],
    "autoWidth": false,
    dom: "lfrtip",
    
    responsive: true, 
    'order': [[0, 'desc']]
    });
    
    
    $("#submitForm").click( function()
    {
        var shipIDs= document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
       
       
        if (shipIDs=='')
        {
            bootbox.alert('Please enter IDs!'   );
            return;
        }
        
        var ids = shipIDs.split(',');
        for(i = 0; i < ids.length;++i){
            if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                ids[i] = ids[i].toUpperCase();
        }
        
        var wrongIDs = Array();
        var correctIDs = Array();
        var correctedIDs = Array();
        var finalArray = [];
        for (var i = 0; i <ids.length; i++ )
        {
            if(!ids[i].replace(/\s/g, '').length)
            {
                continue;
            }
            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) )
            {
                correctIDs.push(ids[i]);
            }
            else
            {
             /*   if (/[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('JC' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
                else if (/C[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('J' + ids[i]);
                    correctedIDs.push(ids[i]);
                } */
               
                wrongIDs.push(ids[i]);
            }
        }
        
        $.each(correctIDs, function(i, el)
        {
            if($.inArray(el, finalArray) === -1) 
            {
                finalArray.push(el);
            }
        });

        
        
        if (wrongIDs.length)
        {
            bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
        }
        
        /*if (correctedIDs.length)
        {
            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
        }*/
    
    
   
        if (finalArray.length ) 
        {
            
                var responseObj;
                
                
                window.location = "/warehouse/work/checkplan?ids="+finalArray.join(",");
               /*$.ajax({
                    type: 'POST',
                    url: '/warehouse/checkplannedcaptain',
                    dataType: 'text',
                    data: { ids:finalArray.join(",")},
                    }).done(function (response) {
            
                    // responseObj = JSON.parse(response);
                    console.log(response);
                    
                    if (response)
                    {
                        
                         location.reload();
                        
                    }
                    else
                    {
                        bootbox.alert('Something went wrong!'   );
                        
                    }
                }); */
            
        } 
        else 
        {
            alert('Nothing to check!'   );
            location.reload();
        }    
        
       }
      );
});

</script  >
@stop

