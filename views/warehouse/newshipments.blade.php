@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif


    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

    <script>var tabFilter = {{ $tabFilter }};</script>

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Total New Shipments
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /top tiles -->
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="get" action="/warehouse/totalNewShipments/">

                        <input type="hidden" name="tabFilter" id="tabFilter" value="{{ $tabFilter }}" />

                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select id="company" name="company" required="required" class="form-control"
                                        type="text">
                                    <option value="all">All Companies</option>
                                    @foreach($companies as $compan)
                                        <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="hub" id="hub" class="form-control">
                                    @foreach($adminHubs as $adhub)
                                        <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Start Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="start_date" name="start_date" value="{{$start_date}}"
                                       class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">End Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                       value="{{$end_date}}"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="row">

                <div class="tab_container">


                    <input id="tab1" type="radio" name="tabs" onclick="emit(0)" {{ !isset($tabFilter) || $tabFilter == 0 ? 'checked' : '' }}>
                    <label for="tab1" class='label2'><i
                                class="fa fa-car"></i><span>New Packages Shipments</span></label>
                                
                    <input id="tab2" type="radio" name="tabs" onclick="emit(1)" {{ isset($tabFilter) && $tabFilter == 1 ? 'checked' : '' }}>
                    <label for="tab2" class='label2'><i
                                class="fa fa-car"></i><span>Returned Packages Shipments</span></label>

                    <section id="content1" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Total New Shipments
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_summary" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Count</th>
                                            <th>Amount</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>Company</th>
                                        </tr>
                                        </thead>


                                        <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>Count</th>
                                            <th>Amount</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>Company</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </section>
                    
                    <section id="content2" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Total Returned Shipments
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- Start of Total Returned Shipments Table -->

                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                                <div class="container">
                                    <table id="total_returned_shipments_view" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Returned Date</th>
                                            <th>Count</th>
                                            <th>Amount</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>Company</th>
                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>Returned Date</th>
                                            <th>Count</th>
                                            <th>Amount</th>
                                            <th>City</th>
                                            <th>Hub</th>
                                            <th>Company</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <!-- End of Total returned Shipments Table -->
                            </div>
                        </div>
                    </section>


                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
    <!-- /page content -->


    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>
        function emit(x) {
            document.getElementById('tabFilter').value = x;
        }
    </script>

    <script>

        <?php
        $mydata = '';
        $singledata = '';
        $totalcount=0;
        $totalamount=0;
        foreach ($totalnewrecords as $record) {
            $shipdate = $record["shipmentdate"];
            $shipdate = mb_split(' ', $shipdate)[0];
            $newcount = $record["newcount"];
            $totalcount+=$newcount;
            $newamount = round($record["newamount"],2);
            $totalamount+=$newamount;
            $city = $record['city'];
            $hub_id = $record['hub_id'];
            $company = $record['company'];
            $mydata .= "[ \"$shipdate\", \"$newcount\" , \"$newamount\" , \"$city\" , \"$hub_id\", \"$company\"], ";
        }
        $grandcount=$totalcount- $returnedcount;
        $grandamount=$totalamount- $returnedamount;
        $returnedamount=number_format($returnedamount,2);
        $grandamount=number_format($grandamount,2);
        $mydata.= "[ \"Supplier Returned \", \"$returnedcount\" , \"$returnedamount\" , \"\" , \"\", \"\"], ";
        $mydata.= "[ \"Sub Total\", \"$totalcount\" , \"$totalamount\" , \"\" , \"\", \"\"], ";
        $mydata.= "[ \"Total\", \"$grandcount\" , \"$grandamount\" , \"\" , \"\", \"\"], ";
        ?>


            $(document).ready(function () {

                    $("#datatable_summary").DataTable({

                        "data": [
                            <?php echo $mydata ?>

                          ],
                        "autoWidth": false,

                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,

                        'order': [[0, 'desc']]
                    });

                });
    </script>
    
    <script>

        <?php
        $mydata = '';
        $singledata = '';
        $totalcount=0;
        $totalamount=0;
        foreach ($totalreturnedshipments as $record) {
            $shipdate = $record->date;
            $shipdate = mb_split(' ', $shipdate)[0];
            $newcount = $record->count;
            $totalcount+=$newcount;
            $newamount = round($record->amount,2);
            $totalamount+=$newamount;
            $trs_city = $record->city;
            $trs_hub_id = $record->hub_id;
            $trs_company = $record->company;
            $mydata .= "[ \"$shipdate\", \"$newcount\" , \"$newamount\" , \"$trs_city\" , \"$trs_hub_id\", \"$trs_company\"], ";
        }
        //$returnedamount=number_format($returnedamount,2);
        //$grandamount=number_format($grandamount,2);
        $mydata.= "[ \"Supplier Returned \", \"$totalcount\" , \"$totalamount\" , \"\" , \"\", \"\"], ";
        ?>


        $(document).ready(function () {

            $("#total_returned_shipments_view").DataTable({

                "data": [ <?php echo $mydata ?> ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                'order': [[0, 'desc']]
            });

        });

    </script>
    
    @stop
