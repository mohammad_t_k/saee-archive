<?php if (Auth::check()){ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>

            <title>Saee</title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script
        src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.load('visualization', '1', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(overallChart);

        function overallChart() {

//AJAX Call is compulsory !

        var overallData = $.ajax({
          url: "/warehouse/overall_chart",
          dataType:"json",
          async: false
          }).responseText;

          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(overallData);

          var options = {
              backgroundColor:'#DADADA',
               title: 'Overall',
              is3D: 'true',
              width: 700,
              height: 600,
              sliceVisibilityThreshold: 0,
            };
          // Instantiate and draw our chart, passing in some options.
          // Do not forget to check your div ID
          var chart = new google.visualization.PieChart(document.getElementById('overall_chart'));
          chart.draw(data, options);

          var t0 = new Date().toLocaleString();
          document.getElementById("timeupdated").innerHTML = t0;
        }


        function overall_last_shipments() {

//AJAX Call is compulsory !

        var overallLastData = $.ajax({
          url: "/warehouse/overall_last_shipments",
          dataType:"json",
          async: false
          }).responseText;

          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(overallLastData);

          var options = {
              backgroundColor:'#DADADA',
               title: 'Overall Last 3 Shipments',
              is3D: 'true',
              width: 700,
              height: 600,
              sliceVisibilityThreshold: 0,
            };
          // Instantiate and draw our chart, passing in some options.
          // Do not forget to check your div ID
          var chart = new google.visualization.PieChart(document.getElementById('overall_last_shipments'));
          chart.draw(data, options);
        }


        function city_last_shipments() {

//AJAX Call is compulsory !

        var city = [];
      var data = [];
      var options = [];
      var chart = [];
        var perCityData = $.ajax({
          url: "/warehouse/city_last_shipments",
          dataType:"json",
          async: false
          }).success(function(res) {

       for (var i = 0; i < res.cities.length; i++) {
        
          city[i] = res.cities[i];
          data[i] = new google.visualization.DataTable(res.cities[i]);
          options[i] = {
               backgroundColor:'#DADADA',
               title: res.citynames[i]+' Last 3 Shipments',
              is3D: 'true',
              width: 700,
              height: 600,
              sliceVisibilityThreshold: 0,
             
            };

            chart[i] = new google.visualization.PieChart(document.getElementById('city_last_shipments_'+[i]));
            chart[i].draw(data[i], options[i]);
        }
       
});
        }


        function per_city() {

//AJAX Call is compulsory !
      var city = [];
      var data = [];
      var options = [];
      var chart = [];
        var perCityData = $.ajax({
          url: "/warehouse/per_city",
          dataType:"json",
          async: false
          }).success(function(res) {

       for (var i = 0; i < res.cities.length; i++) {
        
          city[i] = res.cities[i];
          data[i] = new google.visualization.DataTable(res.cities[i]);
          options[i] = {
               backgroundColor:'#DADADA',
               title: res.citynames[i],
              is3D: 'true',
              width: 700,
              height: 600,
              sliceVisibilityThreshold: 0,
              
            };

            chart[i] = new google.visualization.PieChart(document.getElementById('per_city'+[i]));
            chart[i].draw(data[i], options[i]);

        }
       
});

          // Create our data table out of JSON data loaded from server.
         // var  data1 = new google.visualization.DataTable(city0);
          //var  data2 = new google.visualization.DataTable(city1);
         

         
            
          // Instantiate and draw our chart, passing in some options.
          // Do not forget to check your div ID
         
        
         
          
          
        }

        </script>

        <script type="text/javascript">

                $(document).ready(function(){
                    // First load the chart once 

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var screen = getParameterByName('screen');
if(screen == 1){

  $("#screen4").hide();
  $("#screen3").hide();
  $("#screen2").hide();
}

else if (screen == 2){

 $("#screen1").hide();
  $("#screen3").hide();
  $("#screen4").hide();
 
 }

 else if (screen == 3){

 $("#screen1").hide();
  $("#screen2").hide();
  $("#screen4").hide();
 
 }
  else if (screen == 4){

 $("#screen1").hide();
  $("#screen2").hide();
  $("#screen3").hide();
 
 }
             
                    overallChart();
                    overall_last_shipments();
                    city_last_shipments();
                    per_city();


                    // Set interval to call the drawChart again
                    setInterval(overallChart, 30000);
                    setInterval(overall_last_shipments, 30000);
                    setInterval(city_last_shipments, 30000);
                    setInterval(per_city, 30000);



                    });
      
        </script>

    </head>

    <body style="">

      <div class="logo">
        <img src="<?php echo asset_url(); ?>/companiesdashboard/images/logo-b.png">
        <p style="text-align: center;width: 80%;float: right;margin-top: 30px;">Last Update: <span id="timeupdated"></span></p>
      </div>

<div style="overflow: hidden;width:100%;">

    <div id="screen1" class="row" style="overflow: hidden;">

      <div class="col-md-6">

        <div id="overall_last_shipments"> </div>

       </div>

        <div class="col-md-6">

        <div id="overall_chart"> </div>

       </div>

     </div>

     <div id="screen2" class="row" style="overflow: hidden;">
        <div class="col-md-6">

        <div id="city_last_shipments_0"> </div>

       </div>

       <div class="col-md-6">

        <div id="per_city0"> </div>

       </div>
     </div>



     <div id="screen3" class="row" style="overflow: hidden;">

        <div class="col-md-6">

        <div id="city_last_shipments_1"> </div>

       </div>
        <div class="col-md-6">

        <div id="per_city1"> </div>

       </div>

       
     </div>

<div id="screen4" class="row" style="overflow: hidden;">


  <div class="col-md-6">

        <div id="city_last_shipments_2"> </div>

       </div>
     <div class="col-md-6">

        <div id="per_city2"> </div>

       </div>

</div>

</div>


    </body>
</html>

<?php } ?>