@if( Auth::user()->role < 2)

  <script>window.location = "/warehouse/403";</script>
  
@endif
<?php
if(Session::get('admin_role') == 9)
	$layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 2)
	$layout = 'warehouse.jclayout';
else
	$layout = 'warehouse.layout';
?>

@extends($layout)


@section('content')


 <!-- page content -->
 
                    
<div class="right_col" role="main">
    
    <!-- top tiles -->
    <div class="row tile_count">
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>
          <div class="count"><?php echo $allitems ?> </div>
          
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-cubes"></i> Total COD Packages</span>
          <div class="count"><?php echo $allitemscod ?> </div>
          
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-cubes"></i> Total Prepaid Packages</span>
          <div class="count"><?php echo $allitemsprepaid ?> </div>
          
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Returned to Supplier</span>
          <div class="count black"><?php echo $returnedtojcitems ?></div>
        </div>
        
        
        
        
       </div>
       <div class="row tile_count">
           
           
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-home"></i> Created By Supplier</span>
          <div class="count "><?php echo $penddingitems ?></div>
        </div>
        
         <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-truck"></i> In Route</span>
          <div class="count "><?php echo $onwayitems ?></div>
        </div>
        
         <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-home"></i> In Warehouse</span>
          <div class="count green"><?php echo $arriveditems ?></div>
          <div><?php echo "Returned ". $returneditems; ?></div>
                <div><?php echo "New ". $newitems; ?></div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-car"></i> With Captains</span>
          <div class="count blue"><?php echo $outfordeliveryitems + $undelivreditems ?></div>

        </div>
        
        
         
        
        </div>
       <div class="row tile_count">
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-thumbs-ub"></i> Delivered </span>
          <div class="count green"><?php echo $delivreditems ?></div>
        </div>
        
        
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> Cash collected </span>
          <div class="count green"><?php echo number_format($cashcollected,2) ?></div>
        </div>
        
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> Cash to be collected </span>
          <div class="count red"><?php echo number_format($cashtobecollected,2) ?></div>
        </div>
        
        </div>
       <div class="row tile_count">
           
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> Success Rate </span>
          <div class="count red"><?php echo number_format($successrate,2) ?>%</div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> COD Success Rate </span>
          <div class="count red"><?php echo number_format($successratecod,2) ?>%</div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-money"></i> Prepaid Success Rate </span>
          <div class="count red"><?php echo number_format($successrateprepaid,2) ?>%</div>
        </div>
    </div>
<!-- /top tiles -->
<?php 
$monthstart = strtotime($monthstart);
$monthend =  strtotime($monthend);
?>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<form method="get" action="/warehouse/shipmentstatusbymonth">
					<input type="hidden" id="monthstart" name="monthstart" value="{{$monthstart}}" />
					<input type="hidden" id="monthend" name="monthend" value="{{$monthend}}" />
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
										class="required">*</span>
							</label>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<select id="company" name="company" required="required" class="form-control"
										type="text">
									<option value="all">All Companies</option>
									@foreach($companies as $compan)
										<option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
										class="required">*</span>
							</label>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<select name="city" id="city" class="form-control">
									@foreach($adminCities as $adcity)
										<option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<p>
									<button id="submitForm" class="btn btn-success">Filter</button>
								</p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

<br />

    <div class="row">
        
        <!-- bar chart -->
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Districts <small>Histogram</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div id="districtschart" style="width:100%; height:280px;"></div>
              </div>
            </div>
          </div>
          <!-- /bar charts -->
     </div>
     <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
                  
          <div class="x_panel tile fixed_height_580 overflow_hidden">
            <div class="x_title">
              <h2>Summary</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table class="" style="width:100%">
                
                <tr>
                  <td>
                    <div class="chart-container" style="position: center; ">
                    <canvas height="250" width="250" style="margin: 15px 10px 10px 0" id="homepagepiechart2"></canvas>
                    </div>
                    
                  </td>
                 
                </tr>
              </table>
            </div>
          </div>
        </div>
        </div>
     <!--<div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="x_panel tile fixed_height_580 overflow_hidden">
            <div class="x_title">
              <h2>test</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div id="homepagepiechart" style="height:350px;"></div>

            </div>
          </div>
        </div>-->
        
        
        <br />
        
       
              
    </div>
</div>
        <!-- /page content -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
 <!-- ECharts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script> 


<?php

$mydata_histogram = '';

foreach ($merged as $order) {
  
    $mydata_histogram .= "{ district: \"$order->d_district\", all:\"$order->allitems\", delivered:\"$order->delivereditems\" }, ";

}


?>


<script >  


    $(document).ready(function() {
        
        
       Morris.Bar({
				  element: 'districtschart',
				   data: [
					<?php echo $mydata_histogram ?> 
				  ],
				  xkey: 'district',
				  ykeys: ['all', 'delivered'],
				  labels: ['All Items', 'Delivered Items'],
				  barRatio: 0.6,
				  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
				  xLabelAngle: 75,
				  hideHover: 'auto',
				  resize: true
				});
        
        $('#homepagepiechart2').each(function(){
				
				
        var chart_element = $(this);
			var chart_doughnut = new Chart( $(this), {
            type: "doughnut",
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          
			<?php if($allitems==0) $allitems=1; ?>	
            data: {
                labels: ["Created By Supplier", 
                "Picked up from Riyadh", 
                "Kasper Warehouse", 
                "With captains", 
                "Delivered", 
                "Returned to Supplier"],
                datasets: [{
                data: [
                    <?php echo number_format(100.0*$penddingitems/$allitems,2) ?>,
                    <?php echo number_format(100.0*$onwayitems/$allitems,2) ?>, 
                    <?php echo number_format(100.0*$arriveditems/$allitems,2) ?>, 
                    <?php echo number_format(100.0*($outfordeliveryitems+$undelivreditems)/$allitems,2) ?>, 
                    <?php echo number_format(100.0*$delivreditems/$allitems,2) ?>, 
                    <?php echo number_format(100.0*$returnedtojcitems/$allitems,2) ?> ] ,
                backgroundColor: [
                	"#BDC3C7",
                	"#9B59B6",
                	"#037f3b",
                	"#26B99A",
                	"#3498DB",
                	"#E74C3C"
                ],
                hoverBackgroundColor: [
                	"#CFD4D8",
                	"#B370CF",
                	"#359862",
                	"#36CAAB",
                	"#49A9EA",
                	"#eb6f62"
                ]
                }],
    			options: { 
    				legend: true, 
    				responsive: true 
    			}
            }
        
        });
        
        });	
        
        var theme = {
				  color: [
					  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
					  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
				  ],

				  title: {
					  itemGap: 8,
					  textStyle: {
						  fontWeight: 'normal',
						  color: '#408829'
					  }
				  },

				  dataRange: {
					  color: ['#1f610a', '#97b58d']
				  },

				  toolbox: {
					  color: ['#408829', '#408829', '#408829', '#408829']
				  },

				  tooltip: {
					  backgroundColor: 'rgba(0,0,0,0.5)',
					  axisPointer: {
						  type: 'line',
						  lineStyle: {
							  color: '#408829',
							  type: 'dashed'
						  },
						  crossStyle: {
							  color: '#408829'
						  },
						  shadowStyle: {
							  color: 'rgba(200,200,200,0.3)'
						  }
					  }
				  },

				  dataZoom: {
					  dataBackgroundColor: '#eee',
					  fillerColor: 'rgba(64,136,41,0.2)',
					  handleColor: '#408829'
				  },
				  grid: {
					  borderWidth: 0
				  },

				  categoryAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },

				  valueAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitArea: {
						  show: true,
						  areaStyle: {
							  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },
				  timeline: {
					  lineStyle: {
						  color: '#408829'
					  },
					  controlStyle: {
						  normal: {color: '#408829'},
						  emphasis: {color: '#408829'}
					  }
				  },

				  k: {
					  itemStyle: {
						  normal: {
							  color: '#68a54a',
							  color0: '#a9cba2',
							  lineStyle: {
								  width: 1,
								  color: '#408829',
								  color0: '#86b379'
							  }
						  }
					  }
				  },
				  map: {
					  itemStyle: {
						  normal: {
							  areaStyle: {
								  color: '#ddd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  },
						  emphasis: {
							  areaStyle: {
								  color: '#99d2dd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  }
					  }
				  },
				  force: {
					  itemStyle: {
						  normal: {
							  linkStyle: {
								  strokeColor: '#408829'
							  }
						  }
					  }
				  },
				  chord: {
					  padding: 4,
					  itemStyle: {
						  normal: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  },
						  emphasis: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  }
					  }
				  },
				  gauge: {
					  startAngle: 225,
					  endAngle: -45,
					  axisLine: {
						  show: true,
						  lineStyle: {
							  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
							  width: 8
						  }
					  },
					  axisTick: {
						  splitNumber: 10,
						  length: 12,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  axisLabel: {
						  textStyle: {
							  color: 'auto'
						  }
					  },
					  splitLine: {
						  length: 18,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  pointer: {
						  length: '90%',
						  color: 'auto'
					  },
					  title: {
						  textStyle: {
							  color: '#333'
						  }
					  },
					  detail: {
						  textStyle: {
							  color: 'auto'
						  }
					  }
				  },
				  textStyle: {
					  fontFamily: 'Arial, Verdana, sans-serif'
				  }
			  };

        
        /*$('#homepagepiechart').each(function(){
				
			  var echartPieCollapse = echarts.init(document.getElementById('homepagepiechart'), theme);
			  
			  echartPieCollapse.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['In Riyadh', 'Picked up from Riyadh', 'Kasper Warehouse', 'With captains', 'Delivered', 'Returned to JC']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel']
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				series: [{
				  name: 'Status',
				  type: 'pie',
				  radius: [10, 150],
				  center: ['50%', 170],
				  roseType: 'area',
				  x: '50%',
				  max: 100,
				  sort: 'ascending',
				  data: [{
					value: < ?php //echo number_format(100.0*$penddingitems/$allitems,2) ?>,
					name: 'In Riyadh'
				  }, {
					value: < ?php //echo number_format(100.0*$onwayitems/$allitems,2) ?>,
					name: 'Picked up from Riyadh'
				  }, {
					value: < ?php //echo number_format(100.0*$arriveditems/$allitems,2) ?>,
					name: 'Kasper Warehouse'
				  }, {
					value: < ?php //echo number_format(100.0*($outfordeliveryitems+$undelivreditems)/$allitems,2) ?>,
					name: 'With captains'
				  }, {
					value: < ?php //echo number_format(100.0*$delivreditems/$allitems,2) ?>,
					name: 'Delivered'
				  }, {
					value: < ?php// echo number_format(100.0*$returnedtojcitems/$allitems,2) ?>,
					name: 'Returned to JC'
				  }]
				}]
			  });

        });	*/
        
  
    });

</script> 
@stop
