@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Captain Account</h3>
      </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Captain <?php echo $captaindata->first_name ?> <?php echo $captaindata->last_name ?>  Account</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <p class="text-muted font-13 m-b-30">
            Mobile:  <?php echo $captaindata->phone ?>  
            </p>
            <p id = markAsDelivered><button>Mark selected as Delivered</button></p>
            <table id="datatable_account" class="table table-striped table-bordered ">
            <thead>
                <tr>
                  <th></th>
                  <th>Transaction ID</th>
                  <th>Date/Time</th>
                  <th>Description</th>
                  <th>Credit</th>
                  <th>Debit</th>
                  <th>Balance</th>
                  <th>Notes</th>
                </tr>
              </thead>
              
              
              <tfoot>
                <tr>
                  <th></th>
                  <th>Transaction ID</th>
                  <th>Date/Time</th>
                  <th>Description</th>
                  <th>Credit</th>
                  <th>Debit</th>
                  <th>Balance</th>
                  <th>Notes</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>إستلام مبلغ من الكابتن</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!--  <form class="form-horizontal form-label-left" validate  action="/action_pagee.php"
                onsubmit="return confirm('Is the form filled out correctly?');"> -->
            <form id="insertpayment" class="form-horizontal form-label-left" validate >
              <span class="section">Insert Payment</span>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="number" step="0.01" id="amount" name="op_amount" required="required" data-validate-minmax="0,100000" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paymentmethod">Payment Method <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="op_method" class="form-control" required>
                    <option value="1">Cash</option>
                    <option value="2">Debit Card</option>
                    <option value="3">Credit Card</option>
                  </select>                
                 </div>
              </div>
              
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="notes" type="text" class="form-control" >
                </div>
              </div>
             
              
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="insertpayment" type="submit" class="btn btn-success">Insert Payment</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
     
      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>تسليم مبلغ للكابتن</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <form id="paycaptain" class="form-horizontal form-label-left" validate  >

              <span class="section">Pay Captain</span>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amout">Amount <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="number" step="0.01" id="amout" name="op_amount" required="required" data-validate-minmax="0,100000" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Payment Method <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="op_method" class="form-control" required>
                    <option value="1">Cash</option>
                    <option value="4">Bank wire</option>
                  </select>                
                 </div>
              </div>
              
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="notes" type="text" class="form-control" >
                </div>
              </div>
             
              
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="paycaptain" type="submit" class="btn btn-success">Pay Captain</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
    
</div>

<!-- /page content -->
       


<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->

 <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
 
 
 <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
   
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script> 
 
<script >

<?php
       
            
$mydata_account = '';




foreach ($captainpayments as $aPayment) {
  
    $mydata_account .= "[ \"$aPayment->id\",\"$aPayment->id\",\"$aPayment->op_time\",\"$aPayment->description\" , \"$aPayment->credit\", 
    \"$aPayment->debit\", \"$aPayment->balance\", \"$aPayment->notes\"], ";

}


?>

        
$(document).ready(function() {
    

    var table_account = $("#datatable_account").DataTable({
    
    "data": [
      <?php echo $mydata_account ?> 
      
    ],
    "autoWidth": false,
    dom: "Blfrtip",
    buttons: [
        {
        extend: "copy",
        className: "btn-sm"
        },
        {
        extend: "csv",
        className: "btn-sm"
        },
        {
        extend: "excel",
        className: "btn-sm"
        },
        {
        extend: "pdfHtml5",
        className: "btn-sm"
        },
        {
        extend: "print",
        className: "btn-sm"
        },
    ],
    responsive: true, 
    'columnDefs': [
        {
        'targets': 0,
        'checkboxes': {
        'selectRow': true
            }
        }
        ],
    'select': {
    'style': 'multi'
    },
    'order': [[0, 'asc']],
    "ordering": false
    });
    
    
    $("#insertpayment").submit(function(event) { // intercepts the submit event
        
        if (confirm("Are you sure you want to submit?" ) == true) 
        {
            var dataToSend = $("#insertpayment").serialize()+"&captain_id=<?php echo $captaindata->id ?>"+"&op_type=2";
            console.log(dataToSend);
            
            event.preventDefault();
            $.ajax({
                type: 'POST',
                url: '/warehouse/payments/insertpayment',
                dataType: 'text',
                data: dataToSend,
            }).done(function (response) {
                //alert(rows_selected.count() + ' items have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                console.log(response);
                if (response==1)
                {
                    location.reload();
                }
                
            }); // Ajax Call
           
        }
    });
    
    $("#paycaptain").submit(function(event) { // intercepts the submit event
        
        if (confirm("Are you sure you want to submit?" ) == true) 
        {
            var dataToSend = $("#paycaptain").serialize()+"&captain_id=<?php echo $captaindata->id ?>"+"&op_type=4";
            console.log(dataToSend);
            
            event.preventDefault();
            $.ajax({
                type: 'POST',
                url: '/warehouse/payments/insertpayment',
                dataType: 'text',
                data: dataToSend,
            }).done(function (response) {
                //alert(rows_selected.count() + ' items have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                console.log(response);
                if (response==1)
                {
                    location.reload();
                }
                
            }); // Ajax Call
           
        }
    });
    
});   
   
</script  >

@stop

