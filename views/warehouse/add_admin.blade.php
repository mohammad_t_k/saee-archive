@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif

<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<?php
$cities = City::select(['id', 'name'])->get();
$roles = Role::select(['id', 'title'])->get();
$owners = [];
?>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="x_panel">
                    <div class="clearfix"></div><br/>
                    <h3 class="col-md-offset-2">Add New User</h3><br/>
                    <form id="add_admin_form" action="/warehouse/system/add_admin_post" method="POST" id="AdminForm">
                        <input type="hidden" name="form" value="true" />
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12 custom-input">
                                <input type="text" name="username" id="username" class="form-control col-md-8 col-xs-12" placeholder="Username" required="required" />
                                <label class="">Username</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="col-md-12 col-xs-12 custom-input">
                                <input type="text" name="password" id="password" class="form-control col-md-8 col-xs-12" placeholder="Password" required="required" />
                                <label class="">Password</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12 custom-input">
                                <input type="text" name="email" id="email" class="form-control col-md-8 col-xs-12" placeholder="Email" required="required" />
                                <label class="">Email</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="col-md-12 col-xs-12 custom-input has-data" >
                                <select name="city" id="city" class="form-control col-md-8 col-xs-12" required="required" >
                                    @foreach($all_cities as $acity)
                                        <?php if($acity->name == 'All') continue; ?>
                                        <option value="{{ $acity->name }}">{{ $acity->name }}</option>
                                    @endforeach
                                </select>
                                <label style="color: #73879C; font-size: 13px; top: 0px;">City</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12 custom-input has-data" >
                                <select name="hub" id="hub" class="form-control col-md-8 col-xs-12" required="required" >
                                    @foreach($all_hubs as $ahub)
                                        <?php if($ahub->id == 0) continue; ?>
                                        <option value="{{ $ahub->id }}">{{ $ahub->name }}</option>
                                    @endforeach
                                </select>
                                <label style="color: #73879C; font-size: 13px; top: 0px;">Hub</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-xs-12 custom-input has-data" >
                                <select name="porta_hub" id="porta_hub" class="form-control col-md-8 col-xs-12" required="required" >
                                    <option value="0"></option>
                                    @foreach($all_porta_hubs as $aportahub)
                                        <?php if($aportahub->id == 0) continue; ?>
                                        <option value="{{ $aportahub->id }}">{{ $aportahub->name }}</option>
                                    @endforeach
                                </select>
                                <label style="color: #73879C; font-size: 13px; top: 0px;">Porta Hub</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12 custom-input has-data" >
                                <select name="role" id="role" class="form-control col-md-8 col-xs-12" required="required" >
                                    @foreach($roles as $aRole)
                                        <option value="{{ $aRole->id }}">{{ $aRole->title }}</option>
                                    @endforeach
                                </select>
                                <label style="color: #73879C; font-size: 13px; top: 0px;">Role</label>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12">
                                <label class="">Cities</label>
                                <select name="cities[]" id="cities" class="form-control col-md-8 col-xs-12" style="height: 150px;" required="required" multiple>
                                    @foreach($cities as $aCity)
                                        <option value="{{ $aCity->name }}">{{ $aCity->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="col-md-12 col-xs-12">
                                <label class="">Hubs</label>
                                <select name="hubs[]" id="hubs" class="form-control col-md-8 col-xs-12" style="height: 150px;" required="required" multiple>
                                    @foreach($all_hubs as $ahub)
                                        <option value="{{ $ahub->id }}">{{ $ahub->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12">
                                <label class="">Porta Hubs</label>
                                <select name="porta_hubs[]" id="porta_hubs" class="form-control col-md-8 col-xs-12" style="height: 150px;" required="required" multiple>
                                    @foreach($all_porta_hubs as $aportahub)
                                        <option value="{{ $aportahub->id }}">{{ $aportahub->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-2">
                            <div class="col-md-12 col-xs-12 custom-input has-data" >
                                <select name="is_bb_user" id="is_bb_user" class="form-control col-md-8 col-xs-12" required="required" >
                                    
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        
                                </select>
                                <label style="color: #73879C; font-size: 13px; top: 0px;">Is Blackbox User ?</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br/><br/>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-md-offset-4">
                            <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Add Admin</button>
                        </div>
                    </form>
                    <div id="wait" style="display:none;width:auto;height:auto;border:1px solid black;top:50%;left:50%;padding:2px;text-align: center"><img src='<?php echo asset_url(); ?>/warehouseadmin/assets/admin/Loading_icon.gif' width="64" height="64" /><br>Adding User...</div>
                    <br/><br/>

                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <!-- <script>
        $(document).ready(function () {
            $("#AdminForm").submit(function (e) {
                var username = document.getElementById("username").value;
                var email = document.getElementById("email").value;
                var city = document.getElementById("city").value;
                var password = document.getElementById("password").value;
                var role = document.getElementById("role").value;
                var cities = new Array();//storing the selected values inside an array
                $('#cities :selected').each(function(i, selected) {
                    cities[i] = $(selected).val();
                });
                var hubs = new Array();
                $('#hubs :selected').each(function(i, selected) {
                    hubs[i] = $(selected).val();
                });
                if (cities.length && hubs.length) {
                    if (confirm("Are you sure you want to add new admin ? ") == true) {
                            e.preventDefault();
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/system/add_admin_post',
                            dataType: 'text',
                            data: {username: username,email: email, password: password, city: city, role: role, cities: cities, hubs: hubs},
                        }).done(function (response) {
                            console.log(response);
                        }); 
                    }
                    else {
                        bootbox.alert('You cancled!');
                        return false;
                    }
                }
                else {
                    bootbox.alert('At least one city and one hub must be selected!');
                    return false;
                }
            });
        });
    </script> -->

    <style>
        :focus {
            outline: none;
        }
        .custom-input {
            margin: 30px auto;
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 30px;
            top: 30px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input input {
            height: 40px;
            padding: 5px 10px;
            border: 1px solid #dfdfdf;
            background: transparent;
        }
        .custom-input input:focus + label {
            left: 20px;
            top: 0px;
            font-size: 13px;
            color: #73879C;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 0px;
            font-size: 13px;
            color: #73879C;
            z-index: 0;
            transition: .3s ease-in-out;
        }

        .custom-input {
            margin: 30px auto;
            position: relative;
            padding: 20px;
        }
        .custom-input label {
            position: absolute;
            left: 30px;
            top: 30px;
            font-family: initial, arial, tahoma;
            font-size: 15px;
            color: #d9d9d9;
            z-index: -1;
            transition: .3s ease-in-out;
        }
        .custom-input select {
            height: 40px;
            padding: 5px 10px;
            border: 1px solid #dfdfdf;
            background: transparent;
        }
        .custom-input select:focus + label {
            left: 20px;
            top: 0px;
            font-size: 13px;
            color: #73879C;
            transition: .3s ease-in-out;
        }
        .has-data label {
            left: 20px;
            top: 0px;
            font-size: 13px;
            color: #73879C;
            z-index: 0;
            transition: .3s ease-in-out;
        }
    </style>
    <script>
        /* Start of Floating Label Jquery */
        $(function() {
            'use strict';
            $('.custom-input input').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });
            $('.custom-input input').on('focus', function(){
                if($(this).val() == '') {
                    $(this).parent().addClass('has-data');
                }
            });

            $('.custom-input select').on('focusout', function(){
                if($(this).val() != '') {
                    $(this).parent().addClass('has-data');
                } else {
                    $(this).parent().removeClass('has-data');
                }
            });
            $('.custom-input select').on('focus', function(){
                if($(this).val() == '') {
                    $(this).parent().addClass('has-data');
                }
            });
        });
        /* End of Floating Label Jquery */
    </script>


    <script type="text/javascript">
    var frm = $('#add_admin_form');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({


            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            beforeSend: function(){
                // Handle the beforeSend event
             $("#wait").css("display", "block");
            },
            complete: function(){
            // Handle the complete event
            $("#wait").css("display", "none");
            },
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);

                var response = JSON.parse(data);

                if(response.message == 'bad') { // usrname already exist in Admin table
                    //success message here no object 
                    alert('Admin already registered please choose different username');
                }
                else if(response.message == 'good') {
                    //success message here no object 
                    alert('Successfully registered your username is ' + response.username);
                    window.location = '/warehouse/system/users';
                }
                else if(response.error) {
                    alert(response.error);
                }
                
            },
            error: function (data) {
                console.log('An error occurred.');
                var response = JSON.parse(data);
                alert('something went wrong');
                console.log(response);
            },
        });
    });
</script>

@stop