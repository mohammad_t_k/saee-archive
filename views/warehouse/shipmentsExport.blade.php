@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif


<?php
if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else if (Session::get('admin_role') == 16)
    $layout = 'warehouse.viewlayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)


@section('content')


    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Export Shipments</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <!-- /top tiles -->

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="row">

                <div class="tab_container">


                    <input id="tab3" type="radio" name="tabs" checked>
                    <label for="tab3" class='label2'><i
                                class="fa fa-car"></i><span>Export Single Shipment</span></label>

                    <input id="tab1" type="radio" name="tabs" <?php if ($tabFilter == 2) echo 'checked' ?>>
                    <label for="tab1" class='label2'><i
                                class="fa fa-car"></i><span>Export Specific Shipments</span></label>

                    <input id="tab2" type="radio" name="tabs" <?php if ($tabFilter == 3) echo 'checked' ?>>
                    <label for="tab2" class='label2'><i
                                class="fa fa-pencil-square-o"></i><span> Export Shipments</span></label>

                    <section id="content1" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="row">
                                <!-- table start -->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Export Shipments by Waybill</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form class="multiple-ids" id="shipmentsIdsExportform" action="/warehouse/shipmentsIdsExport"
                                                  method="post">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value="2">

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <textarea id="shipmentIDs" name="ids" required="required" class="form-control" type="text" rows="10"></textarea>
                                                    </div>
                                                    <div class="theCount">(Max 100) Lines used: <span id="linesUsed">0</span>
                                                        <div class="clearfix"></div>
                                                        <br/>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 col-md-offset-3">
                                                                <p>
                                                                    {{--<button id="submitForm" class="btn btn-success">
                                                                        Submit
                                                                    </button>--}}
                                                                    <button type="button" class="btn btn-success" onclick="shipmentsIdspopupOption();">Submit</button>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-1 form-group" style="margin-right: 35px;" >
                                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                                    <button type="button" id="copy_waybills_box" class="btn btn-success">Copy All Waybills</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                            <table id="datatable_summary" class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>

                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>


                    <section id="content3" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="row">
                                <!-- table start -->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Export Shipment by Waybill</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form action="/warehouse/shipmentsIdExport" method="post">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value='false'>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment
                                                        ID <span class="required">*</span>
                                                    </label>

                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <input id="shipmentID" name="id" required="required"
                                                               class="form-control" type="text" rows="10" 
                                                               value="{{ count($singleorder) ? $singleorder[0]->jollychic : '' }}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p>
                                                            <button id="submitForm" class="btn btn-success">Submit
                                                            </button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </form>

                                            <table id="datatable_single" class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>

                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>


                    <section id="content2" class="tab-content"> 
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">

                                            <form method="get" id="shipmentsExportform" action="/warehouse/shipmentsExport">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value="3">

                                                <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Company</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="company" name="chosen_companies[]" required="required" class="form-control" type="text" multiple>
                                                            <option value="all" <?php if (isset($chosen_companies) && in_array('all', $chosen_companies)) echo 'selected';?> >All Companies</option>
                                                            <option value="71" <?php if (isset($chosen_companies) && in_array('71', $chosen_companies)) echo 'selected';?> >JollyChic Group</option>
                                                            <option value="73" <?php if (isset($chosen_companies) && in_array('73', $chosen_companies)) echo 'selected';?> >JollyChic Local</option>
                                                            <option value="74" <?php if (isset($chosen_companies) && in_array('74', $chosen_companies)) echo 'selected';?> >JollyChic Global</option>
                                                            @foreach($companies as $compan)
                                                                <option value="{{ $compan->id }}" <?php if (isset($chosen_companies) && in_array($compan->id, $chosen_companies)) echo 'selected';?>>{{ $compan->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Main City</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="city" name="chosen_cities[]" class="form-control" multiple>
                                                            @foreach($adminCities as $adcity)
                                                                <option value="{{ $adcity->city }}"<?php if (isset($chosen_cities) && in_array($adcity->city, $chosen_cities)) echo 'selected';?> >{{ $adcity->city }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Sub City</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="subcity" name="chosen_subcities[]" class="form-control" multiple>
                                                            <option value="All" <?php if (isset($chosen_subcities) && in_array('All', $chosen_subcities)) echo 'selected';?> >All</option>
                                                            @foreach($adminSubCities as $adsubcity)
                                                                <option value="{{ $adsubcity->subcity }}" <?php if (isset($chosen_subcities) && in_array($adsubcity->subcity, $chosen_subcities)) echo 'selected';?>>{{ $adsubcity->city }} | {{ $adsubcity->subcity }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="hub" name="chosen_hubs[]" class="form-control" multiple>
                                                            @foreach($adminHubs as $adhub)
                                                                <option value="{{$adhub->hub_id}}"<?php if (isset($chosen_hubs) && in_array($adhub->hub_id, $chosen_hubs)) echo 'selected';?>>{{$adhub->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Porta Hub </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="porta_hub" name="chosen_porta_hubs[]" class="form-control" multiple>
                                                            <option value="ignore" <?php if (isset($chosen_porta_hubs) && in_array('ignore', $chosen_porta_hubs)) echo 'selected';?>>Ignore</option>
                                                            @foreach($adminPortaHubs as $ad_porta_hub)
                                                                <option value="{{$ad_porta_hub->porta_hub_id}}" <?php if (isset($chosen_porta_hubs) && in_array($ad_porta_hub->porta_hub_id . '', $chosen_porta_hubs, true)) echo 'selected';?>>{{$ad_porta_hub->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Status </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select multiple name="status[]" id="status" class="form-control" >
                                                            <option value="all" <?php if (!isset($statusArray) || empty($statusArray) || !empty($statusArray) && in_array('all',$statusArray)) echo 'selected';?>>All</option>
                                                            <option value="-3"<?php if (!empty($statusArray) && in_array(-3, $statusArray)) echo 'selected';?> >
                                                                To Be Picked-up
                                                            </option>
                                                            <option value="-2"<?php if (!empty($statusArray) && in_array(-2, $statusArray)) echo 'selected';?> >
                                                                Reserved To Pickup
                                                            </option>
                                                            <option value="-1"<?php if (!empty($statusArray) && in_array(-1, $statusArray)) echo 'selected';?> >
                                                                Picked-Up by Captain
                                                            </option>
                                                            <option value="0"<?php if (!empty($statusArray) && in_array(0, $statusArray, true)) echo 'selected';?> >
                                                                Created By Origin
                                                            </option>
                                                            <option value="1"<?php if (!empty($statusArray) && in_array(1,$statusArray)) echo 'selected';?> >
                                                                In Sorting Area
                                                            </option>
                                                            <option value="2"<?php if (!empty($statusArray) && in_array(2,$statusArray)) echo 'selected';?> >
                                                                Picked-Up
                                                            </option>
                                                            <option value="3"<?php if (!empty($statusArray) && in_array(3,$statusArray)  && empty($inwarehouse)) echo 'selected';?> >
                                                                In Warehouse
                                                            </option>
                                                            <option value="3/R"<?php if (!empty($statusArray) && in_array(3,$statusArray) && in_array(1, $inwarehouse)) echo 'selected';?> >
                                                                In Warehouse/R
                                                            </option>
                                                            <option value="3/Res"<?php if (!empty($statusArray) && in_array(3,$statusArray) && in_array(2, $inwarehouse)) echo 'selected';?> >
                                                                In Warehouse/Res
                                                            </option>
                                                            <option value="3/new"<?php if (!empty($statusArray) && in_array(3,$statusArray) && in_array(3, $inwarehouse)) echo 'selected';?> >
                                                                In Warehouse/New
                                                            </option>
                                                            <option value="4"<?php if (!empty($statusArray) && in_array(4,$statusArray)) echo 'selected';?> >
                                                                Out For Delivery
                                                            </option>
                                                            <option value="5"<?php if (!empty($statusArray) && in_array(5,$statusArray)) echo 'selected';?> >
                                                                Delivered
                                                            </option>
                                                            <option value="6"<?php if (!empty($statusArray) && in_array(6,$statusArray)) echo 'selected';?> >
                                                                UnDelivered
                                                            </option>
                                                            <option value="7"<?php if (!empty($statusArray) && in_array(7,$statusArray)) echo 'selected';?> >
                                                                Returned to Origin
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Sub Status </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="chosen_sub_statuses[]" id="sub_status" class="form-control" multiple>
                                                            <option value="all" <?php if(in_array('all', $chosen_sub_statuses)) echo 'selected'; ?> >All</option>
                                                            @foreach($subStatuses as $one_sub_status)
                                                                <option value="{{ $one_sub_status->id }}" <?php if(isset($chosen_sub_statuses) && in_array($one_sub_status->id, $chosen_sub_statuses)) echo 'selected' ?> >{{ $statusDef[$one_sub_status->status_id] }}/{{ $one_sub_status->code }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Reason </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="chosen_undelivered_reason[]" id="undelivered_reason" class="form-control" multiple>
                                                            <option value="all" <?php if(isset($chosen_undelivered_reason) && in_array('all', $chosen_undelivered_reason, true)) echo 'selected'; ?> >All</option>
                                                            @foreach($UndeliveredReason as $one_reason)
                                                                <option value="{{ $one_reason->id }}" <?php if(isset($chosen_undelivered_reason) && in_array($one_reason->id . '' /* because we forces data type matching here */, $chosen_undelivered_reason, true)) echo 'selected' ?> >{{ $one_reason->id == 0 ? 'No Reason' : $one_reason->english }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Cash On Delivery </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="zero_cod" id="zero_cod" class="form-control">
                                                            <option value="all" {{ isset($zero_cod) && $zero_cod == 'all' ? 'selected' : '' }}>All</option>
                                                            <option value="yes" {{ isset($zero_cod) && $zero_cod == 'yes' ? 'selected' : '' }}>Zero COD</option>
                                                            <option value="no" {{ isset($zero_cod) && $zero_cod == 'no' ? 'selected' : '' }}>Non-Zero COD</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group" style="display: none;">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12"> Scheduled Shipment Date </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="shipment_date" id="shipment_date" class="form-control">
                                                            <option value="all">All</option>
                                                            <?php 
                                                                $sel = '';
                                                                if(!isset($shipmentDate)){

                                                                    $shipmentDate = 'all';
                                                                }
                                                                if($shipmentDate == 'null'){
                                                                    $sel = 'selected';
                                                                }
                                                             ?>
                                                            <option {{$sel}} value="null">Null</option>
                                                            <?php
                                                            // $shipmentdates = DeliveryRequest::distinct()->where('jollychic', '!=', '0')->orderBy('scheduled_shipment_date', 'desc')->get(['scheduled_shipment_date']);
                                                            // $total_num_of_shipments = count($shipmentdates);
                                                            // foreach ($shipmentdates as $ashipmentdates) {
                                                            //     if ($ashipmentdates->scheduled_shipment_date != null) {
                                                            //         $selected = '';
                                                            //         $date = $ashipmentdates->scheduled_shipment_date;
                                                            //         if (isset($shipmentDate) && $shipmentDate == $date)
                                                            //             $selected = 'selected';
                                                            //         echo "<option value='$date' $selected>$date</option>";
                                                            //     }
                                                            // }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group" style="display: none;">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">New Shipment Date</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="new_shipment_date" id="new_shipment_date" class="form-control">
                                                            <option value="all">All</option>
                                                            <?php
                                                            // $total_num_of_new_shipments = count($newshipmentDates);
                                                            // foreach ($newshipmentDates as $anewshipmentdates) {
                                                            //     if ($anewshipmentdates->new_shipment_date != null) {
                                                            //         $newselected = '';
                                                            //         $newdate = $anewshipmentdates->new_shipment_date;
                                                            //         if (isset($newShipmentDate) && $newShipmentDate == $newdate)
                                                            //             $newselected = 'selected';
                                                            //         echo "<option value='$newdate' $newselected>$newdate</option>";
                                                            //     }
                                                            // }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Failed Delivery Attempts </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="failed_operator" id="failed_operator" class="form-control">
                                                            <option value="none">Operator</option>
                                                            <option value="=" {{ isset($failed_operator) && $failed_operator == '=' ? 'selected' : '' }}>=</option>
                                                            <option value=">" {{ isset($failed_operator) && $failed_operator == '>' ? 'selected' : '' }}>></option>
                                                            <option value="<" {{ isset($failed_operator) && $failed_operator == '<' ? 'selected' : '' }}><</option>
                                                            <option value=">=" {{ isset($failed_operator) && $failed_operator == '>=' ? 'selected' : '' }}>>=</option>
                                                            <option value="<=" {{ isset($failed_operator) && $failed_operator == '<=' ? 'selected' : '' }}><=</option>
                                                        </select>
                                                        <input type="number" name="failed_delivery_attempts" id="failed_delivery_attempts" value="{{ isset($failed_delivery_attempts) && $failed_delivery_attempts !== 'all' ? $failed_delivery_attempts : '' }}" class="form-control" min="0">
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Age </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="age_operator" id="age_operator" class="form-control">
                                                            <option value="none">Operator</option>
                                                            <option value="=" {{ isset($age_operator) && $age_operator == '=' ? 'selected' : '' }}>=</option>
                                                            <option value=">" {{ isset($age_operator) && $age_operator == '>' ? 'selected' : '' }}>></option>
                                                            <option value="<" {{ isset($age_operator) && $age_operator == '<' ? 'selected' : '' }}><</option>
                                                            <option value=">=" {{ isset($age_operator) && $age_operator == '>=' ? 'selected' : '' }}>>=</option>
                                                            <option value="<=" {{ isset($age_operator) && $age_operator == '<=' ? 'selected' : '' }}><=</option>
                                                        </select>
                                                        <input type="number" name="age" id="age" value="{{ isset($age) && $age !== 'all' ? $age : '' }}" class="form-control" min="0">
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Pieces </label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="pieces_operator" id="pieces_operator" class="form-control">
                                                            <option value="none">Operator</option>
                                                            <option value="=" {{ isset($pieces_operator) && $pieces_operator == '=' ? 'selected' : '' }}>=</option>
                                                            <option value=">" {{ isset($pieces_operator) && $pieces_operator == '>' ? 'selected' : '' }}>></option>
                                                            <option value="<" {{ isset($pieces_operator) && $pieces_operator == '<' ? 'selected' : '' }}><</option>
                                                            <option value=">=" {{ isset($pieces_operator) && $pieces_operator == '>=' ? 'selected' : '' }}>>=</option>
                                                            <option value="<=" {{ isset($pieces_operator) && $pieces_operator == '<=' ? 'selected' : '' }}><=</option>
                                                        </select>
                                                        <input type="number" name="pieces" id="pieces" value="{{ isset($pieces) && $pieces !== 'all' ? $pieces : '' }}" class="form-control" min="1">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <hr>

                                                <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Date Type</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="date_type" id="date_type" class="form-control">
                                                        <option value=""></option>
                                                            <option value="created_at" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'created_at' ? 'selected' : '' }}>Creation Date</option>
                                                            <option value="sort_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'sort_date' ? 'selected' : '' }}>Sort Date</option>
                                                            <option value="new_shipment_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'new_shipment_date' ? 'selected' : '' }}>Pick-Up Date</option>
                                                            <option value="signin_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'signin_date' ? 'selected' : '' }}>Sign-In Date</option>
                                                            <option value="scheduled_shipment_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'scheduled_shipment_date' ? 'selected' : '' }}>Scheduled Shipment Date</option>
                                                            <option value="to_be_paid_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'to_be_paid_date' ? 'selected' : '' }}>First Delivery Attempt Date</option>
                                                            <option value="second_delivery_attempt" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'second_delivery_attempt' ? 'selected' : '' }}>Second Delivery Attempt Date</option>
                                                            <option value="third_delivery_attempt" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'third_delivery_attempt' ? 'selected' : '' }}>Third Delivery Attempt Date</option>
                                                            <option value="dropoff_timestamp" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'dropoff_timestamp' ? 'selected' : '' }}>Delivery Date</option>
                                                            <option value="return_to_supplier_date" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'return_to_supplier_date' ? 'selected' : '' }}>Return To Origin Date</option>
                                                            <option value="updated_at" {{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' && $date_type == 'updated_at' ? 'selected' : '' }}>Last Update</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">From</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input type="date" name="from" id="from" value="{{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' ? $from : '' }}" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">To</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input type="date" name="to" id="to" value="{{ isset($date_type) && isset($from) && isset($to) && $date_type != '' && $from != '' && $to != '' ? $to : '' }}" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <hr>
                                                <br/>

                                                {{--<div class="col-md-1 col-xs-12 col-xs-offset-3 form-group" >
                                                    <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <button id="submitForm" class="btn btn-success">Filter</button>
                                                    </div>
                                                </div>--}}
                                                <div class="col-md-1 col-xs-12 col-xs-offset-1 form-group">
                                                    <button type="button" class="btn btn-success" onclick="FilterpopupOption();">Filter</button>
                                                </div>

                                                <div class="col-md-1 col-xs-12 col-xs-offset-1 form-group" >
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <button type="button" id="copy_waybills" class="btn btn-success">Copy All Waybills</button>
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-xs-12 col-md-offset-1 col-sm-offset-2 form-group" >
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <button type="button" id="pincode_report" class="btn btn-success">Pincode Report</button>
                                                    </div>
                                                </div>


                                            </
                                            >
                                            <!-- export shipment -->
                                            <table id="datatable_filtered" class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>

                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company Name</th>
                                                    <th>Main City</th>
                                                    <th>Sub City</th>
                                                    <th>Main Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Pickup Date</th>
                                                    <th>Failed Delivery Attempts</th>
                                                    <th>Reason</th>
                                                    <th>Pieces</th>
                                                    <th>Name</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Street Addrs</th>
                                                    <th>District</th>
                                                    <th>Scheduled Shipment Date</th>
                                                    <th>COD</th>
                                                    <th>Pincode</th>
                                                    <th>Captain</th>
                                                    <th>Called</th>
                                                    <th>Status</th>
                                                    <th>Status Update</th>
                                                    <th>Age</th>
                                                    <th>Delivery Date</th>
                                                    <th>First Delivery Attempt</th>
                                                    <th>Details</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- table start -->
                    <div class="modal fade" id="Modal_of_exportShipents" role="dialog" style="overflow: auto">
                        <div class="modal-dialog modal-sm">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Select Option For Export Shipments</h4>
                                </div>
                                <div class="modal-body">
                                    <table id="export_report_table" class="table table-striped table-bordered">
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <a id="download_codReportDirect" class="btn btn-success" onclick="submitselectedcod()">Submit</a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- table start -->
                    <div class="modal fade" id="Modal_of_shipmentsIdsExport" role="dialog" style="overflow: auto">
                        <div class="modal-dialog modal-sm">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Select Option For Export Shipments by waybill</h4>
                                </div>
                                <div class="modal-body">
                                    <table id="export_shipmentsIds_table" class="table table-striped table-bordered">
                                    </table>
                                </div>
                                <div class="modal-footer">

                                    <a id="download_codReportDirect" class="btn btn-success" onclick="submitselectedreport()">Submit</a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->



    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
            
    <script>

        <?php



        $mydata = '';

        $singledata = '';

        foreach ($specificorders as $order) { // Ids
            $undelivered_reason = $order->undelivered_reason;
            if($undelivered_reason == 0)
                $undelivered_reason = 'Nothing';
            else
                $undelivered_reason = $UndeliveredReasonMap[$undelivered_reason]->english;

            $today = date('Y-m-d');
            $status = $statusDef[$order->status];
            if(isset($order->sub_status_code)) 
                $status .= '/' . $order->sub_status_code;

            if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status .= '/Res';
            else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status .= '/R';
            else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                $status .= '/New';

            if ($order->first_name)
                $cnf_cap = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
            else
                $cnf_cap = 'Not Set';

            if($order->delivery_date)
                $delivery_date = date('Y-m-d', strtotime($order->delivery_date));
            else
                $delivery_date =  'Not Delivered';

            $d_address = addslashes($order->d_address);

	        if ($order->sender_name)
                $order->company_id = $companiesmap[$order->company_id].' ['.$order->sender_name.']';
            else
                $order->company_id = $companiesmap[$order->company_id];

            if($order->hub_name == 'All')
                $order->hub_name = '';  
            if($order->porta_hub_name == 'All')
                $order->porta_hub_name = '';

            $to_be_paid = "";
            if($order->to_be_paid == 1)
                $to_be_paid = $order->to_be_paid_date;

            if($order->called_to_consignee)
                $called='yes';
            else
                $called='No';

            $d_address = fixStringForDatatables($d_address);
            $order->receiver_name = fixStringForDatatables($order->receiver_name);
            $order->d_district = fixStringForDatatables($order->d_district);

            $mydata .= "[\"<a onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>\",\"$order->company_id\", \"$order->main_city\", \"$order->d_city\" , \"$order->hub_name\" , \"$order->porta_hub_name\", \"$order->order_number\", \"$order->sort_date\", \"$order->num_faild_delivery_attempts\", \"$undelivered_reason\", \"$order->d_quantity\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$order->scheduled_shipment_date\", \"$order->cash_on_delivery\", \"$order->pincode\", \" $cnf_cap\",\" $called\", \"$status\", \"$order->status_update\", \"$order->age\", \"$delivery_date\", \"$to_be_paid\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";

        }

        foreach ($singleorder as $order) { // Id

            $undelivered_reason = $order->undelivered_reason;
            if($undelivered_reason == 0)
                $undelivered_reason = 'Nothing';
            else
                $undelivered_reason = $UndeliveredReasonMap[$undelivered_reason]->english;

            $today = date('Y-m-d');
            $status = $statusDef[$order->status];
            if(isset($order->sub_status_code)) 
                $status .= '/' . $order->sub_status_code;
            if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status .= '/Res';
            else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status .= '/R';
            else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                $status .= '/New';

            if ($order->first_name)
                $cnf_cap = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
            else
                $cnf_cap = 'Not Set';

            if($order->delivery_date)
                $delivery_date = date('Y-m-d', strtotime($order->delivery_date));
            else
                $delivery_date =  'Not Delivered';

            $d_address = addslashes($order->d_address);
	        if ($order->sender_name)
                $order->company_id = $companiesmap[$order->company_id].' ['.$order->sender_name.']';
            else
                $order->company_id = $companiesmap[$order->company_id];

            if($order->hub_name == 'All')
                $order->hub_name = '';
            if($order->porta_hub_name == 'All')
                $order->porta_hub_name = '';

            $to_be_paid = "";
            if($order->called_to_consignee)
                $called='yes';
            else
                $called='No';

            if($order->to_be_paid == 1)
                $to_be_paid = $order->to_be_paid_date;

            $d_address = fixStringForDatatables($d_address);
            $order->receiver_name = fixStringForDatatables($order->receiver_name);
            $order->d_district = fixStringForDatatables($order->d_district);

            $singledata .= "[ \"<a onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>\",\"$order->company_id\", \"$order->main_city\", \"$order->d_city\", \"$order->hub_name\", \"$order->porta_hub_name\" , \"$order->order_number\", \"$order->sort_date\", \"$order->num_faild_delivery_attempts\", \"$undelivered_reason\", \"$order->d_quantity\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$order->scheduled_shipment_date\", \"$order->cash_on_delivery\", \"$order->pincode\", \" $cnf_cap\",\"$called\", \"$status\", \"$order->status_update\", \"$order->age\", \"$delivery_date\", \"$to_be_paid\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";

        }


        $filter_data = '';
        foreach ($orders as $order) { // filter
            $undelivered_reason = $order->undelivered_reason;
            if($undelivered_reason == 0)
                $undelivered_reason = 'Nothing';
            else
                $undelivered_reason = $UndeliveredReasonMap[$undelivered_reason]->english;
            
            $today = date('Y-m-d');
            $status = $statusDef[$order->status];
            if(isset($order->sub_status_code)) 
                $status .= '/' . $order->sub_status_code;
            if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status .= '/Res';
            else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status .= '/R';
            else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                $status .= '/New';
                
            if ($order->first_name)
                $cnf_cap = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9) . ' /C';
            else if($order->planned_walker != '0')
                $cnf_cap = $order->p_first_name . ' ' . $order->p_last_name . ' 0' . substr(str_replace(' ', '', $order->p_phone), -9) . ' /P';
            else
                $cnf_cap = 'Not Set';

            if($order->delivery_date)
                $delivery_date = date('Y-m-d', strtotime($order->delivery_date));
            else
                $delivery_date =  'Not Delivered';
                
            $d_address = addslashes($order->d_address);
            $receiver_phone = addslashes($order->receiver_phone);
	        if ($order->sender_name)
                $order->company_name .= ' ['.$order->sender_name.']';
            $hub_name = $all_hubs[$order->hub_id]->name;
            if($hub_name == 'All')
                $hub_name = '';
            if($order->porta_hub_name == 'All')
                $order->porta_hub_name = '';
            if(!isset($order->order_reference))
                $order->order_reference = '';

            $to_be_paid = "";
            if($order->to_be_paid == 1)
                $to_be_paid = $order->to_be_paid_date;
                
            if($order->called_to_consignee)
                $called='yes';
            else
                $called='No';

            $d_address = fixStringForDatatables($d_address);
            $order->receiver_name = fixStringForDatatables($order->receiver_name);
            $order->d_district = fixStringForDatatables($order->d_district);
            $filter_data .= "[ \"<a onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>\",\"$order->company_name\", \"$order->main_city\", \"$order->d_city\", \"$hub_name\", \"$order->porta_hub_name\", \"$order->order_number\", \"$order->order_reference\", \"$order->sort_date\", \"$order->num_faild_delivery_attempts\", \"$undelivered_reason\", \"$order->d_quantity\", \"$order->receiver_name\",  \"$receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$order->scheduled_shipment_date\", \"$order->cash_on_delivery\", \"$order->pincode\", \" $cnf_cap\",\" $called\", \"$status\", \"$order->status_update\", \"$order->age\", \"$delivery_date\", \"$to_be_paid\", \"<a href=\'/warehouse/package/$order->jollychic\'>Show</a>\"], ";
        }
        ?>

        $(document).ready(function () {

            var lines = 100;
            var linesUsed = $('#linesUsed');

            $('#shipmentIDs').keydown(function (e) {

                newLines = $(this).val().split("\n").length;
                linesUsed.text(newLines);

                if (e.keyCode == 13 && newLines >= lines) {
                    linesUsed.css('color', 'red');
                    return false;
                }
                else {
                    linesUsed.css('color', '');
                }
            });


           $("#datatable_summary").DataTable({

                "data": [
                    <?php echo $mydata ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
               
                'order': [[0, 'asc']]
            });

             


            $("#datatable_single").DataTable({

                "data": [
                    <?php echo $singledata; ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });


            $("#datatable_filtered").DataTable({

                "data": [
                    <?php echo $filter_data ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });

        });

        /*
        $("#submitForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            if (/[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('JC' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }
                            else if (/C[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('J' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }

                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }


                    if (finalArray.length) {
                        document.getElementById("shipmentIDs").value = JSON.stringify(finalArray);
                        return true;
                    }
                    else {
                        alert('Nothing to check!');
                        return false;
                        //location.reload();
                    }

                }
        );
*/
        function opensticker(jollychic) {
            window.open('/warehouse/sticker/' + jollychic, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }

        $("#copy_waybills").click(function(){
            <?php
            $waybills = "";
            foreach($orders as $order) {
                $waybills .= "$order->jollychic\n";
            }
            ?>
            let str = `{{ $waybills }}`;
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert("Copied");
        });

        $("#copy_waybills_box").click(function(){
            <?php
            $waybills = "";
            foreach($specificorders as $order) {
                $waybills .= "$order->jollychic\n";
            }
            ?>
            let str = `{{ $waybills }}`;
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert("Copied");
        });

        $("#pincode_report").click(function(){
            <?php if(!isset($_GET['tabFilter']) || $_GET['tabFilter'] != 3) { ?>
                alert("Please filter first!");
            <?php } else { ?>
                <?php if(!isset($_GET['chosen_companies'])) $_GET['chosen_companies'] = array(); ?>
                <?php if(!isset($_GET['chosen_cities'])) $_GET['chosen_cities'] = array(); ?>
                <?php if(!isset($_GET['chosen_subcities'])) $_GET['chosen_subcities'] = array(); ?>
                <?php if(!isset($_GET['chosen_hubs'])) $_GET['chosen_hubs'] = array(); ?>
                <?php if(!isset($_GET['chosen_porta_hubs'])) $_GET['chosen_porta_hubs'] = array(); ?>
                <?php if(!isset($_GET['status'])) $_GET['status'] = array(); ?>
                <?php if(!isset($_GET['chosen_sub_statuses'])) $_GET['chosen_sub_statuses'] = array(); ?>
                <?php if(!isset($_GET['chosen_undelivered_reason'])) $_GET['chosen_undelivered_reason'] = array(); ?>
                let chosen_companies = "{{ implode(',', $_GET['chosen_companies']) }}";
                let chosen_cities = "{{ implode(',', $_GET['chosen_cities']) }}";
                let chosen_subcities = "{{ implode(',', $_GET['chosen_subcities']) }}";
                let chosen_hubs = "{{ implode(',', $_GET['chosen_hubs']) }}";
                let chosen_porta_hubs = "{{ implode(',', $_GET['chosen_porta_hubs']) }}";
                let status = "{{ implode(',', $_GET['status']) }}";
                let chosen_sub_statuses = "{{ implode(',', $_GET['chosen_sub_statuses']) }}";
                let chosen_undelivered_reason = "{{ implode(',', $_GET['chosen_undelivered_reason']) }}";
                let shipment_date = "{{ $_GET['shipment_date'] }}";
                let new_shipment_date = "{{ $_GET['new_shipment_date'] }}";
                let date_type = "{{ $_GET['date_type'] }}";
                let from = "{{ $_GET['from'] }}";
                let to = "{{ $_GET['to'] }}";
                let zero_cod = "{{ $_GET['zero_cod'] }}";
                let failed_delivery_attempts = "{{ $_GET['failed_delivery_attempts'] }}";
                let failed_operator = "{{ $_GET['failed_operator'] }}";
                let pieces = "{{ $_GET['pieces'] }}";
                let pieces_operator = "{{ $_GET['pieces_operator'] }}";
                let age = "{{ $_GET['age'] }}";
                let age_operator = "{{ $_GET['age_operator'] }}";
                window.open("/warehouse/pincodereport?chosen_companies="+chosen_companies+"&chosen_cities="+chosen_cities+"&chosen_subcities="+chosen_subcities+"&chosen_hubs="+chosen_hubs
                    +"&chosen_porta_hubs="+chosen_porta_hubs+"&status="+status+"&chosen_sub_statuses="+chosen_sub_statuses+"&chosen_undelivered_reason="+chosen_undelivered_reason+"&shipment_date="+shipment_date
                    +"&new_shipment_date="+new_shipment_date+"&date_type="+date_type+"&from="+from+"&to="+to
                    +"&zero_cod="+zero_cod+"&failed_delivery_attempts="+failed_delivery_attempts+"&failed_operator="+failed_operator+"&pieces="+pieces+"&pieces_operator="+pieces_operator+"&age="+age+"&age_operator="+age_operator
                );
            <?php } ?>
        });

        function FilterpopupOption() { 
            var chosen_companies = $("#company").val();
            var chosen_cities = $("#city").val();
            var chosen_subcities = $("#subcity").val();
            var chosen_hubs = $("#hub").val();
            var chosen_porta_hubs = $("#porta_hub").val();
            var status = $('#status').val();
            var chosen_sub_statuses = $('#sub_status').val();
            var chosen_undelivered_reason = $('#undelivered_reason').val();
            var date_type = document.getElementById("date_type").value;
            var from = document.getElementById("from").value;
            var to = document.getElementById("to").value;
            var shipment_date = document.getElementById("shipment_date").value;
            var new_shipment_date = document.getElementById("new_shipment_date").value;
            var zero_cod = document.getElementById('zero_cod').value;
            var failed_delivery_attempts = document.getElementById('failed_delivery_attempts').value;
            var failed_operator = document.getElementById('failed_operator').value;
            var pieces = document.getElementById('pieces').value;
            var pieces_operator = document.getElementById('pieces_operator').value;
            var age = document.getElementById('age').value;
            var age_operator = document.getElementById('age_operator').value;
            var tabFilter = document.getElementById("tabFilter").value;

            function FilterpopupOption(callback) {
                console.log('FilterpopupOption');
                console.log('companies: ', chosen_companies);
                console.log('cities: ', chosen_cities);
                console.log('subcities: ', chosen_subcities);
                console.log('hubs: ', chosen_hubs);
                console.log('porta_hubs: ', chosen_porta_hubs);
                console.log('statuses: ', status);
                console.log('sub_statuses: ', chosen_sub_statuses);
                console.log('reason: ', chosen_undelivered_reason);
                console.log('zero_cod: ', zero_cod);
                console.log('failed_delivery_attempts: ', failed_delivery_attempts);
                console.log('failed_operator: ', failed_operator);
                console.log('pieces: ', pieces);
                console.log('pieces_operator: ', pieces_operator);
                console.log('age: ', age);
                console.log('age_operator: ', age_operator);
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/checkshipmentsExportviewstatus',
                    dataType: 'text',
                    cache: false,
                    success: callback,
                    data: {
                        chosen_companies: chosen_companies,
                        chosen_cities: chosen_cities,
                        chosen_subcities: chosen_subcities,
                        chosen_hubs: chosen_hubs,
                        chosen_porta_hubs: chosen_porta_hubs,
                        status: status,
                        chosen_sub_statuses: chosen_sub_statuses,
                        shipment_date: shipment_date,
                        new_shipment_date: new_shipment_date,
                        chosen_undelivered_reason: chosen_undelivered_reason,
                        date_type: date_type,
                        from: from,
                        to: to,
                        zero_cod: zero_cod,
                        failed_delivery_attempts: failed_delivery_attempts,
                        failed_operator: failed_operator,
                        pieces: pieces,
                        pieces_operator: pieces_operator,
                        age: age,
                        age_operator: age_operator,
                    }
                })
            }

            FilterpopupOption(function (res) {
                console.log('FilterpopupOption');
                console.log(res);
                var data_count = JSON.parse(res);
                var Export_report_Information = '<caption><h4>Export Report</h4></caption><tr>' + '<th>Select</th>' + '<th>Report</th>' + '</tr>';
                Export_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="exportreportradio" style="display:block">' + '</td><td>' + 'Download Filter Report' + '</td></tr><tr><td>'
                        + '<input id="viewcod"  type="radio" name="exportreportradio" style="display:block">' + '</td><td>' + 'View Filter Report' + '</td></tr>';

                $('#export_report_table').html(Export_report_Information);
                $('#Modal_of_exportShipents').modal("show");
                if (data_count.count > 700) {
                    document.getElementById("viewcod").disabled = true;
                }
            });

            return;
        }

        function submitselectedcod() {
            var exportreportradio = document.getElementsByName('exportreportradio');

            var chosen_companies = $("#company").val();
            var chosen_cities = $("#city").val();
            var chosen_subcities = $("#subcity").val();
            var chosen_hubs = $("#hub").val();
            var chosen_porta_hubs = $("#porta_hub").val();
            var status = $('#status').val();
            var chosen_sub_statuses = $('#sub_status').val();
            var chosen_undelivered_reason = $('#undelivered_reason').val();
            var shipment_date = document.getElementById("shipment_date").value;
            var date_type = document.getElementById("date_type").value;
            var from = document.getElementById("from").value;
            var to = document.getElementById("to").value;
            var new_shipment_date = document.getElementById("new_shipment_date").value;
            var zero_cod = document.getElementById('zero_cod').value;
            var failed_delivery_attempts = document.getElementById('failed_delivery_attempts').value;
            var failed_operator = document.getElementById('failed_operator').value;
            var pieces = document.getElementById('pieces').value;
            var pieces_operator = document.getElementById('pieces_operator').value;
            var age = document.getElementById('age').value;
            var age_operator = document.getElementById('age_operator').value;
            var tabFilter = document.getElementById("tabFilter").value;

            var is_radio_check = '';

            for (var i = 0, length = exportreportradio.length; i < length; i++) {
                if (exportreportradio[i].checked) {
                    var export_option = exportreportradio[i].parentNode.parentNode.childNodes[1].innerHTML;
                    //if (confirm("Are you sure you want to " + export_option) == true) {
                        if (export_option == 'Download Filter Report') {

                            var path = '/downloadshipmentsExportReport?chosen_companies=' + chosen_companies + '&chosen_cities=' + chosen_cities + '&chosen_subcities=' + chosen_subcities 
                                + '&chosen_hubs=' + chosen_hubs + '&chosen_porta_hubs=' + chosen_porta_hubs + "&status=" + status + "&chosen_sub_statuses=" + chosen_sub_statuses
                                + "&chosen_undelivered_reason=" + chosen_undelivered_reason 
                                + '&shipment_date=' + shipment_date + '&new_shipment_date=' + new_shipment_date + '&undelivered_reason=' + undelivered_reason + '&date_type=' + date_type 
                                + '&from=' + from + '&to=' + to + '&zero_cod=' + zero_cod + '&failed_delivery_attempts=' + failed_delivery_attempts + '&failed_operator=' + failed_operator 
                                + '&pieces=' + pieces + '&pieces_operator=' + pieces_operator + '&age=' + age + '&age_operator=' + age_operator + '&tabFilter=' + tabFilter;
                            location.href = path;
                            $('#Modal_of_exportShipents').modal('hide');
                            return;
                        } else if (export_option == 'View Filter Report') {
                            document.forms["shipmentsExportform"].submit();
                            $('#Modal_of_exportShipents').modal('hide');
                        }
                    /*} else {
                        alert('You cancled!');
                    }*/

                    var is_radio_check = 'ok';
                    break;
                }
            }

            if (is_radio_check == '') {
                alert("please select option first");
            }
        }

        // shipmentsIds popupOption
        function shipmentsIdspopupOption() {
            var ids = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

            if (ids == '') {
                alert("please enter shipments ids first");
                return;
            }

            function shipmentsIdspopupOption(callback) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/checkshipmentsIdsviewstatus',
                    dataType: 'text',
                    cache: false,
                    success: callback,
                    data: {
                        ids: ids
                    }
                })
            }

            shipmentsIdspopupOption(function (res) {
                console.log('shipmentsIdspopupOption');
                console.log(res);
                var data_count = JSON.parse(res);
                var Export_report_Information = '<caption><h4>Export Shipments by Waybill</h4></caption><tr>' + '<th>Select</th>' + '<th>Report</th>' + '</tr>';
                Export_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="shipmentsExportreportradio" style="display:block">' + '</td><td>' + 'Download Shipments Report' + '</td></tr><tr><td>'
                        + '<input id="viewcod"  type="radio" name="shipmentsExportreportradio" style="display:block">' + '</td><td>' + 'View Shipments Report' + '</td></tr>';

                $('#export_shipmentsIds_table').html(Export_report_Information);
                $('#Modal_of_shipmentsIdsExport').modal("show");
                if (data_count.count > 700) {
                    document.getElementById("viewcod").disabled = true;
                }
            });

            return;
        }

        function submitselectedreport() {
            var shipmentsExportreportradio = document.getElementsByName('shipmentsExportreportradio');
            var ids = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");

            if (ids == '') {
                alert("please enter shipments ids first");
                return;
            }


            var is_radio_check = '';

            for (var i = 0, length = shipmentsExportreportradio.length; i < length; i++) {
                if (shipmentsExportreportradio[i].checked) {
                    var export_option = shipmentsExportreportradio[i].parentNode.parentNode.childNodes[1].innerHTML;
                    //if (confirm("Are you sure you want to " + export_option) == true) {
                        if (export_option == 'Download Shipments Report') {

                            /*var path = '/downloadshipmentsIdsReport?&ids=' + ids;
                            location.href = path;*/

                            var form = document.createElement("form");
                            var element1 = document.createElement("input");
                            form.method = "POST";
                            form.action = "/downloadshipmentsIdsReport";
                            element1.value = ids;
                            element1.name = "ids";
                            form.appendChild(element1);
                            document.body.appendChild(form);
                            form.submit();
                            $('#Modal_of_shipmentsIdsExport').modal('hide');
                            return;
                        } else if (export_option == 'View Shipments Report') {
                            document.forms["shipmentsIdsExportform"].submit();
                            $('#Modal_of_shipmentsIdsExport').modal('hide');
                        }
                    /*} else {
                        alert('You cancled!');
                    }*/

                    var is_radio_check = 'ok';
                    break;
                }
            }

            if (is_radio_check == '') {
                alert("please select option first");
            }
        }
        
    </script>


@stop
