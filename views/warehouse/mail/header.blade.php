<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div style="width: 50%; margin-left: auto; margin-right: auto; text-align: center;">
        <div style="margin-left: auto; margin-right: auto;">
            <img src="<?php echo asset_url(); ?>/companiesdashboard/images/logo-b.png" alt="Saee">
        </div>
        <hr style="border-color: orange; background-color: orange;" />
    </div>
    @yield('content')
    @yield('footer')
</body>
</html>