@extends('warehouse.mail.header')
@section('content')

<div style="width: 50%; margin-left: auto; margin-right: auto;">
    <h3>Dear {{ $one_company->company_name }} Team,
    <br>
    This is the daily update for the last 3 months for the shipments of 
    @if($one_company->id == '71')
    JollyChic, JollyChic Express, Dealy, MarkaVIP, Dealy Express and MarkaVIP Express.
    @else
    {{ $one_company->company_name }}
    @endif
    </h3>

    <h5>Regards..</h5>
</div>

@extends('warehouse.mail.footer')
@stop