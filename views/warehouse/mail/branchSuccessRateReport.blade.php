@extends('warehouse.mail.header')
@section('content')

<div style="width: 50%; margin-left: auto; margin-right: auto;">
    <h3>Dear All,
    <br>
    Kindly find attached the success rate report of your branch/branches grouped by sub cities for your kind attention.
    </h3>

    <h5>Best Regards,</h5>
</div>

@extends('warehouse.mail.footer')
@stop