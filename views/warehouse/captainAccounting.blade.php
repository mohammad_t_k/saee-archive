@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif
    <?php  $adminRole = Session::get('admin_role');
    if ($adminRole == 14) {
        $layout = 'warehouse.portahub_layout';
    } else if ($adminRole == 9) {
        $layout = 'warehouse.cslayout';
    } else {
        $layout = 'warehouse.layout';
    }
    ?>
@extends($layout)



    @section('content')
            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <small>Captain :{{$captain_info}}</small>
                            </h2>


                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row tile_count">

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-cubes"></i> Total Delivered Amount</span>

                                <div class="count"><?php echo $deliveredAmount ?> </div>

                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-cubes"></i> Total Covered Amount</span>

                                <div class="count"><?php echo $coveredAmount ?> </div>

                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-cubes"></i> Total Covered Packages</span>

                                <div class="count"><?php echo $coveredamountPieces?> </div>

                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-cubes"></i> Total Delivered Packages</span>

                                <div class="count"><?php echo $deliveredPieces ?> </div>

                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-cubes"></i> Total Undelivered Amount</span>

                                <div class="count"><?php echo $undeliveredAmount ?> </div>

                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-user"></i>Total Undelivered Packages </span>

                                <div class="count black"><?php echo $undeliveredPieces ?></div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-user"></i>Amount With Captain </span>

                                <div class="count black"><?php echo $withCaptainAmount ?></div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-user"></i>Total Packages  With Captain</span>

                                <div class="count black"><?php echo $withCaptainPieces ?></div>
                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-user"></i>Total Packages PickedUp By Captain</span>

                                <div class="count black"><?php echo $pickedupPieces ?></div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form method="get" action="/warehouse/captainaccounting">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="hidden" value="{{$captain_id}}" name="captain_id" />

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                <p>
                                    <button class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>
                    </div>
                        <label id="tab1"
                               style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;;background: #fff;box-shadow: inset 0 3px #2a3f54;"
                               onclick=Accounting()><span>Accounting</span></label>
                        <label id="tab2"
                               style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;background: #f0f0f0;"
                               onclick=allCoveredMoney()><span>Covered Money </span></label>
                        <label id="tab3"
                               style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;background: #f0f0f0;;"
                               onclick=Pickup_Payment()><span>Pickup Payment</span></label>
                        <section id="content1" class="tab-content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">
                            <!--<p> <a class="setdistrict" ><button>Set District</button></a></p> -->
                            <table id="table_captain_account" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th><input id="selectallup" class="dt-checkboxes"
                                               onchange="onChangeAllCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                    <th><input id="selectcoveredallup" class="dt-checkboxes"
                                               onchange="onChangeAllCoveredcheckbox(this)" disabled
                                               type="checkbox">Is Money Covered
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $balance = 0;
                                foreach ($orders as $order) { ?>
                                <tr id='<?= 'tr' . $order['jollychic'] ?>'>
                                    <td><input id="ch<?= $order['jollychic'] ?>" class="dt-checkboxes"
                                               value="<?= $order['jollychic'] ?>"
                                               onchange="onChangeCreditCheckBox('<?= $order['jollychic'] ?>')"
                                               type="checkbox"></td>
                                    <td><?= $order['jollychic'] ?></td>
                                    <td><?php if ($order['hub_name'] == 'All') echo ''; else echo $order['hub_name']; ?></td>
                                    <td><?php if ($order['is_money_received'] == 0) {
                                            $balance += $order['cash_on_delivery'];
                                            echo $order['cash_on_delivery'];
                                        }?></td>
                                    <td><?php if ($order['is_money_received'] == 1) echo $order['cash_on_delivery'] ?></td>
                                    <td></td>
                                    <td><input type="checkbox" id="CoveredAmountCheck<?= $order['jollychic'] ?>"
                                               disabled
                                               onclick=onChangeCoveredAmountCheckBox(this,'<?= $order['jollychic'] ?>')>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Admin</td>
                                    <td>Total</td>
                                    <td id="balance">{{$balance}}</td>
                                    <td>Selected Amount</td>
                                    <td id="selectedAmount">0</td>
                                    <td id="selectedCoveredAmount">0</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                               onchange="onChangeAllCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                    <th><input id="selectcoveredalldown" class="dt-checkboxes" style="width: auto"
                                               onchange="onChangeAllCoveredcheckbox(this)" disabled
                                               type="checkbox">Is Money Covered
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Amount Received <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">Cash Amount:</label>
                                <input id="cashReceived" required="required" class="form-control" type="text"/>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">ATM Amount:</label>
                                <input id="atmReceived" required="required" class="form-control" type="text"/>
                            </div>
                            {{--<label class="control-label col-md-2 col-sm-3 col-xs-12"> ATM Transaction ID: <span
                                        class="required">*</span></label>--}}
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-12 col-sm-6 col-xs-12"> Approval Code:</label>
                                <input id="approval_code" required="required" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div>
                            <button id=updateMoneyReceived>Update Captain Account</button>
                        </div>
                            </div>
                        </section>
                <section id="content2" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">
                            <table id="table_captain_account" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th><input id="selectcoverallup" class="dt-checkboxes"
                                               onchange="onChangeAllcoveredMoneyCheckBox(this)"
                                               type="checkbox">Select
                                    </th>

                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $cov_balance = 0;
                                foreach ($covered_orders as $covered_order) { ?>
                                <tr id='<?= 'tr' . $covered_order['jollychic'] ?>'>
                                    <td><input id="ch<?= $covered_order['jollychic'] ?>" class="dt-checkboxes"
                                               value="<?= $covered_order['jollychic'] ?>"
                                               onchange="onChangecoverCreditCheckBox('<?= $covered_order['jollychic'] ?>')"
                                               type="checkbox"></td>

                                    <td><?= $covered_order['jollychic'] ?></td>
                                    <td><?php if ($covered_order['hub_name'] == 'All') echo ''; else echo $covered_order['hub_name']; ?></td>
                                    <td><?php if ($covered_order['is_money_received'] == 0) {
                                            $cov_balance += $covered_order['cash_on_delivery'];
                                            echo $covered_order['cash_on_delivery'];
                                        }?></td>
                                    <td><?php if ($covered_order['is_money_received'] == 1) echo $covered_order['cash_on_delivery'] ?></td>
                                    <td></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Admin</td>
                                    <td>Total</td>
                                    <td id="covbalance">{{$cov_balance}}</td>
                                    <td>Selected Amount</td>
                                    <td id="selectedcoverAmount">0</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><input id="selectcoveralldown" class="dt-checkboxes" style="width: auto"
                                               onchange="onChangeAllcoveredMoneyCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Covered Amount Received <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <input id="cashcoverReceived" required="required" class="form-control" type="text"/>
                            </div>


                        </div>
                        <div>
                            <button id="coveredButton" onclick=updatecaptaincoveredMoney() disabled>Update Captain
                                Covered Amount
                            </button>
                        </div>

                     </div>
                </section>

                <section id="content3" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">
                            <!--<p> <a class="setdistrict" ><button>Set District</button></a></p> -->
                            <table id="table_captain_account" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th><input id="selectallpickup" class="dt-checkboxes"
                                               onchange="onChangeAllPickupCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $pickup_balance = 0;
                                foreach ($pickup_orders as $pickup_order) { ?>
                                <tr id='<?= 'tr' . $pickup_order['jollychic'] ?>'>
                                    <td><input id="pickupordercheck<?= $pickup_order['jollychic'] ?>" class="dt-checkboxes"
                                               value="<?= $pickup_order['jollychic'] ?>"
                                               onchange="onChangepickupCheckBox('<?= $pickup_order['jollychic'] ?>')"
                                               type="checkbox"></td>
                                    <td><?= $pickup_order['jollychic'] ?></td>
                                    <td><?php if ($pickup_order['hub_name'] == 'All') echo ''; else echo $pickup_order['hub_name']; ?></td>
                                    <td><?php if ($pickup_order['is_pickup_cash_received'] == 0) {
                                            $pickup_balance += $pickup_order['cash_on_pickup'];
                                            echo $pickup_order['cash_on_pickup'];
                                        }?></td>
                                    <td><?php if ($pickup_order['is_pickup_cash_received'] == 1) echo $pickup_order['cash_on_pickup'] ?></td>
                                    <td></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Admin</td>
                                    <td>Total</td>
                                    <td id="pickup_balance">{{$pickup_balance}}</td>
                                    <td>Selected Amount</td>
                                    <td id="selected_pickup_Amount">0</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><input id="selectallpickupdown" class="dt-checkboxes" style="width: auto"
                                               onchange="onChangeAllPickupCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>

                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Amount Received <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">Cash Amount:</label>
                                <input id="pickup_cashReceived" required="required" class="form-control" type="text"/>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">ATM Amount:</label>
                                <input id="pickup_atmReceived" required="required" class="form-control" type="text"/>
                            </div>
                            {{--<label class="control-label col-md-2 col-sm-3 col-xs-12"> ATM Transaction ID: <span
                                        class="required">*</span></label>--}}
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-12 col-sm-6 col-xs-12"> Approval Code:</label>
                                <input id="pickup_approval_code" required="required" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div>
                            <button onclick=updatecaptainpickupMoney()>Pickup cash Received</button>
                        </div>
                    </div>
                </section>
                <section id="content4" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">
                            <!--<p> <a class="setdistrict" ><button>Set District</button></a></p> -->
                            <table id="portaTable_captain_account" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th><input id="selectportaallup" class="dt-checkboxes"
                                               onchange="onChangeportaAllCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $balance = 0;
                                foreach ($portaOrders as $portaOrder) { ?>
                                <tr id='<?= 'portatr' . $portaOrder['jollychic'] ?>'>
                                    <td><input id="portach<?= $portaOrder['jollychic'] ?>" class="dt-checkboxes"
                                               value="<?= $portaOrder['jollychic'] ?>"
                                               onchange="onChangePortaCreditCheckBox('<?= $portaOrder['jollychic'] ?>')"
                                               type="checkbox"></td>
                                    <td><?= $portaOrder['jollychic'] ?></td>
                                    <td><?php if ($portaOrder['hub_name'] == 'All') echo ''; else echo $portaOrder['hub_name']; ?></td>
                                    <td><?php if ($portaOrder['is_money_received'] == 0) {
                                            $balance += $portaOrder['cash_on_delivery'];
                                            echo $portaOrder['cash_on_delivery'];
                                        }?></td>
                                    <td><?php if ($portaOrder['is_money_received'] == 1) echo $portaOrder['cash_on_delivery'] ?></td>
                                    <td></td>

                                </tr>
                                <?php } ?>
                                <tr>
                                    <td>Admin</td>
                                    <td>Total</td>
                                    <td id="portaBalance">{{$balance}}</td>
                                    <td>Selected Amount</td>
                                    <td id="portaselectedAmount">0</td>

                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><input id="selectportaalldown" class="dt-checkboxes" style="width: auto"
                                               onchange="onChangeportaAllCheckBox(this)"
                                               type="checkbox">Select
                                    </th>
                                    <th>WayBill</th>
                                    <th>Hub</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>

                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Amount Received <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">Cash Amount:</label>
                                <input id="portacashReceived" required="required" class="form-control" type="text"/>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-10 col-sm-6 col-xs-12">ATM Amount:</label>
                                <input id="portaatmReceived" required="required" class="form-control" type="text"/>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label class="control-label col-md-12 col-sm-6 col-xs-12"> Approval Code:</label>
                                <input id="portaapproval_code" required="required" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div>
                            <button id=updateportaMoneyReceived>Update Captain Account</button>
                        </div>
                    </div>
                </section>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>
        var adminRole = <?php echo $adminRole; ?>;
        if (adminRole == 14) {
            document.getElementById("content1").style.display = "none";
            document.getElementById("content2").style.display = "none";
            document.getElementById("content3").style.display = "none";
            document.getElementById("content4").style.display = "block";
            document.getElementById("tab1").style.display = "none";
            document.getElementById("tab2").style.display = "none";
            document.getElementById("tab3").style.display = "none";

        } else {
            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none";
            document.getElementById("content3").style.display = "none";
            document.getElementById("content4").style.display = "none";
        }
        var orders = <?php echo $orders; ?>;
        var portorders = <?php echo $portaOrders; ?>;
        var covered_orders = <?php echo $covered_orders; ?>;
        var pickup_orders = <?php echo $pickup_orders; ?>;
        <?php $admin_id = Session::get('admin_id'); if ($admin_id == 26 || $admin_id == 32) { ?>
        document.getElementById('coveredButton').disabled = false;
        <?php }?>
        $("#updateMoneyReceived").click(function (e) {
            var captain_id = '<?php echo $captain_id; ?>';
            var captain_info = '<?php echo $captain_info; ?>';
            var cashReceived = document.getElementById('cashReceived').value;
            var atmReceived = document.getElementById('atmReceived').value;
            var approval_code = document.getElementById('approval_code').value;
            var amountReceived = (+cashReceived + +atmReceived).toFixed(2);
            var cashpercentage=(cashReceived/amountReceived*100).toFixed(2);
            var atmpercentage=(atmReceived/amountReceived*100).toFixed(2);
            var jsonArr = [];
            for (var i = 0; i < orders.length; i++) {
                if (document.getElementById('ch' + orders[i].jollychic).checked == true && document.getElementById('CoveredAmountCheck' + orders[i].jollychic).checked == true) {
                    jsonArr.push({
                        ids: orders[i].jollychic,
                        is_amount_covered: 1
                    });

                } else if (document.getElementById('ch' + orders[i].jollychic).checked == true) {
                    jsonArr.push({
                        ids: orders[i].jollychic,
                        is_amount_covered: 0
                    });
                }
            }
            console.log(jsonArr);
            var responseObj;
            if (confirm("Are you sure you want to update the captain account of " + captain_info + " since rolling back will be difficult?: ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updateMoneyReceivedFromCaptain',
                    dataType: 'text',
                    data: {
                        captain_id: captain_id,
                        //ids: ids.join(','),
                        jsonarray: jsonArr,
                        amountReceived: amountReceived,
                        cashpercentage: cashpercentage,
                        atmpercentage: atmpercentage,
                        approval_code: approval_code
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(parseFloat(responseObj.amount).toFixed(2) + ' SAR Received from captain : ' + responseObj.captaindata);
                        window.location = '/warehouse/captainaccounting?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });
        //Update Porta Money received from Captain
        $("#updateportaMoneyReceived").click(function (e) {
            var captain_id = '<?php echo $captain_id; ?>';
            var captain_info = '<?php echo $captain_info; ?>';
            var portaCashReceived = document.getElementById('portacashReceived').value;
            var portaAtmReceived = document.getElementById('portaatmReceived').value;
            var approval_code = document.getElementById('portaapproval_code').value;
            var portaAmountReceived = (+portaCashReceived + +portaAtmReceived).toFixed(2);
            var cashpercentage=(portaCashReceived/portaAmountReceived*100).toFixed(2);
            var atmpercentage=(portaAtmReceived/portaAmountReceived*100).toFixed(2);
            var ids = [];
            for (var i = 0; i < portorders.length; i++) {
                if (document.getElementById('portach' + portorders[i].jollychic).checked == true) {
                    ids.push(portorders[i].jollychic);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want to update the captain account of " + captain_info + " since rolling back will be difficult?: ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatePortaMoneyReceivedFromCaptain',
                    dataType: 'text',
                    data: {
                        captain_id: captain_id,
                        ids: ids.join(','),
                        amountReceived: portaAmountReceived,
                        cashpercentage: cashpercentage,
                        atmpercentage: atmpercentage,
                        approval_code: approval_code,
                        portaHubId:<?php echo $adminPortaHubId; ?>
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(parseFloat(responseObj.amount).toFixed(2) + ' SAR Received from captain : ' + responseObj.captaindata);
                        window.location = '/warehouse/captainaccounting?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });
        function updatecaptaincoveredMoney(){
            var captain_id = '<?php echo $captain_id; ?>';
            var captain_info = '<?php echo $captain_info; ?>';
            var amountcoverReceived = document.getElementById('cashcoverReceived').value;

            var ids = [];
            for (var i = 0; i < covered_orders.length; i++) {
                if (document.getElementById('ch' + covered_orders[i].jollychic).checked == true) {
                    ids.push(covered_orders[i].jollychic);
                    console.log(covered_orders[i].jollychic);
                }
            }

            var responseObj;
            if (confirm("Are you sure you want to update the captain account of " + captain_info + " since rolling back will be difficult?: ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updateCaptainCoveredMoney',
                    dataType: 'text',
                    data: {
                        captain_id: captain_id,
                        ids: ids.join(','),
                        amountReceived: amountcoverReceived,
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(parseFloat(responseObj.amount).toFixed(2) + ' SAR Covered Amount Received from captain : ' + responseObj.captaindata);
                        window.location = '/warehouse/captainaccounting?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        };
        var bal = parseFloat(balance.innerHTML);
        var portabal = parseFloat(portaBalance.innerHTML);
        var selectedAmo = parseFloat(selectedAmount.innerHTML);
        var covbal = parseFloat(covbalance.innerHTML);
        var selectedcoverAmo = parseFloat(selectedcoverAmount.innerHTML);
        var CoveredAmount = parseFloat(selectedCoveredAmount.innerHTML);
        var pickup_bal = parseFloat(pickup_balance.innerHTML);
        var selectedpickupAmount = parseFloat(selected_pickup_Amount.innerHTML);
        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < orders.length; i++) {
                if(document.getElementById('ch' + orders[i].jollychic).checked ==  checkbox.checked)
                    continue;
                document.getElementById('ch' + orders[i].jollychic).checked =  checkbox.checked;
                onChangeCreditCheckBox(orders[i].jollychic);
            }
            <?php $admin_role = Session::get('admin_role'); if ($admin_role == 5 || $admin_role == 4) { ?>
            if (document.getElementById('selectalldown').checked == true) {
                <?php $admin_id = Session::get('admin_id'); if ($admin_id == 26 || $admin_id == 32) { ?>
                document.getElementById('selectcoveredallup').disabled = false;
                document.getElementById('selectcoveredalldown').disabled = false;
                <?php   } ?>
            } else if (document.getElementById('selectalldown').checked == false) {
                document.getElementById('selectcoveredallup').disabled = true;
                document.getElementById('selectcoveredalldown').disabled = true;
                document.getElementById('selectcoveredallup').checked = false;
                document.getElementById('selectcoveredalldown').checked = false;
            }
            <?php } ?>
        }
        function onChangeAllCoveredcheckbox(checkbox) {
            document.getElementById('selectcoveredallup').checked = checkbox.checked;
            document.getElementById('selectcoveredalldown').checked = checkbox.checked;
            for (var i = 0; i < orders.length; i++) {
                if (document.getElementById('CoveredAmountCheck' + orders[i].jollychic).checked == checkbox.checked)
                    continue;
                document.getElementById('CoveredAmountCheck' + orders[i].jollychic).checked = checkbox.checked;
                onChangeallCoveredAmountCheckBox(orders[i].jollychic);
            }
        }
        function onChangeallCoveredAmountCheckBox(waybill) {
            var row = document.getElementById("tr" + waybill);
            var credit = row.getElementsByTagName("td")[4];
            var selectedCoveredAmount = document.getElementById("selectedCoveredAmount");
            if (credit.innerHTML != '' && document.getElementById('CoveredAmountCheck' + waybill).checked == true) {
                CoveredAmount = CoveredAmount + parseFloat(credit.innerHTML);
            } else {
                CoveredAmount = CoveredAmount - parseFloat(credit.innerHTML);
            }
            selectedCoveredAmount.innerHTML = CoveredAmount.toFixed(2);
        }
        function onChangecoverCreditCheckBox(waybill) {
            var row = document.getElementById("tr" + waybill);
            var debit = row.getElementsByTagName("td")[3];
            var credit = row.getElementsByTagName("td")[4];
            var covbalance = document.getElementById("covbalance");
            var selectedcoverAmount = document.getElementById("selectedcoverAmount");
            if (debit.innerHTML != '') {
                credit.innerHTML = debit.innerHTML;
                covbal = covbal - parseFloat(debit.innerHTML);
                selectedcoverAmo = selectedcoverAmo + parseFloat(debit.innerHTML);
                debit.innerHTML = '';
            } else {
                debit.innerHTML = credit.innerHTML;
                covbal = covbal + parseFloat(credit.innerHTML);
                selectedcoverAmo = selectedcoverAmo - parseFloat(credit.innerHTML);
                credit.innerHTML = '';
            }
            covbalance.innerHTML = covbal.toFixed(2);
            selectedcoverAmount.innerHTML = selectedcoverAmo.toFixed(2);

        }
        function onChangeAllcoveredMoneyCheckBox(checkbox) {
            document.getElementById('selectcoverallup').checked = checkbox.checked;
            document.getElementById('selectcoveralldown').checked = checkbox.checked;
            for (var i = 0; i < covered_orders.length; i++) {
                if(document.getElementById('ch' + covered_orders[i].jollychic).checked ==  checkbox.checked)
                    continue;
                document.getElementById('ch' + covered_orders[i].jollychic).checked =  checkbox.checked;
                onChangecoverCreditCheckBox(covered_orders[i].jollychic);
            }
        }
        function onChangeCreditCheckBox(waybill) {
            var row = document.getElementById("tr" + waybill);
            var debit = row.getElementsByTagName("td")[3];
            var credit = row.getElementsByTagName("td")[4];
            var balance = document.getElementById("balance");
            var selectedAmount = document.getElementById("selectedAmount");
            if (debit.innerHTML != '') {
                credit.innerHTML = debit.innerHTML;
                bal = bal - parseFloat(debit.innerHTML);
                selectedAmo = selectedAmo + parseFloat(debit.innerHTML);
                debit.innerHTML = '';
            } else {
                debit.innerHTML = credit.innerHTML;
                bal = bal + parseFloat(credit.innerHTML);
                selectedAmo = selectedAmo - parseFloat(credit.innerHTML);
                credit.innerHTML = '';
            }
            balance.innerHTML = bal.toFixed(2);
            selectedAmount.innerHTML = selectedAmo.toFixed(2);
            <?php $admin_role = Session::get('admin_role'); if ($admin_role == 5 || $admin_role == 4) { ?>
            if (document.getElementById('ch' + waybill).checked == true) {
                <?php $admin_id = Session::get('admin_id'); if ($admin_id == 26 || $admin_id == 32) { ?>
                document.getElementById('CoveredAmountCheck' + waybill).disabled = false;
                <?php   } ?>
            }else if (document.getElementById('ch' + waybill).checked == false) {
                document.getElementById('CoveredAmountCheck' + waybill).disabled = true;
                document.getElementById('CoveredAmountCheck' + waybill).checked =  false;
            }
            <?php   } ?>
        }
        function onChangeCoveredAmountCheckBox(eve, waybill) {
            var row = document.getElementById("tr" + waybill);
            var credit = row.getElementsByTagName("td")[4];
            var selectedCoveredAmount = document.getElementById("selectedCoveredAmount");

            if (credit.innerHTML != '' && eve.checked == true ) {
                CoveredAmount = CoveredAmount + parseFloat(credit.innerHTML);
            } else if (eve.checked == false && credit.innerHTML != '') {
                CoveredAmount = CoveredAmount - parseFloat(credit.innerHTML);
            }
            selectedCoveredAmount.innerHTML = CoveredAmount.toFixed(2);
        }
        function onChangeAllPickupCheckBox(checkbox) {
            document.getElementById('selectallpickup').checked = checkbox.checked;
            document.getElementById('selectallpickupdown').checked = checkbox.checked;
            for (var i = 0; i < pickup_orders.length; i++) {
                if (document.getElementById('pickupordercheck' + pickup_orders[i].jollychic).checked == checkbox.checked)
                    continue;
                document.getElementById('pickupordercheck' + pickup_orders[i].jollychic).checked = checkbox.checked;
                onChangepickupCheckBox(pickup_orders[i].jollychic);
            }
        }
        function onChangepickupCheckBox(waybill) {
            var row = document.getElementById("tr" + waybill);
            var debit = row.getElementsByTagName("td")[3];
            var credit = row.getElementsByTagName("td")[4];
            var pickup_balance = document.getElementById("pickup_balance");
            var selected_pickup_Amount = document.getElementById("selected_pickup_Amount");
            if (debit.innerHTML != '') {
                credit.innerHTML = debit.innerHTML;
                pickup_bal = pickup_bal - parseFloat(debit.innerHTML);
                selectedpickupAmount = selectedpickupAmount + parseFloat(debit.innerHTML);
                debit.innerHTML = '';
            } else {
                debit.innerHTML = credit.innerHTML;
                pickup_bal = pickup_bal + parseFloat(credit.innerHTML);
                selectedpickupAmount = selectedpickupAmount - parseFloat(credit.innerHTML);
                credit.innerHTML = '';
            }
            pickup_balance.innerHTML = pickup_bal.toFixed(2);
            selected_pickup_Amount.innerHTML = selectedpickupAmount.toFixed(2);
        }
        function onChangeportaAllCheckBox(checkbox) {
            document.getElementById('selectportaallup').checked = checkbox.checked;
            document.getElementById('selectportaalldown').checked = checkbox.checked;
            for (var i = 0; i < portorders.length; i++) {
                if(document.getElementById('portach' + portorders[i].jollychic).checked ==  checkbox.checked)
                    continue;
                document.getElementById('portach' + portorders[i].jollychic).checked =  checkbox.checked;
                onChangePortaCreditCheckBox(portorders[i].jollychic);
            }
        }
        function onChangePortaCreditCheckBox(waybill) {
            var row = document.getElementById("portatr" + waybill);
            var portaDebit = row.getElementsByTagName("td")[3];
            var portaCredit = row.getElementsByTagName("td")[4];
            var portaBalance = document.getElementById("portaBalance");
            var portaselectedAmount = document.getElementById("portaselectedAmount");
            if (portaDebit.innerHTML != '') {
                portaCredit.innerHTML = portaDebit.innerHTML;
                portabal = portabal - parseFloat(portaDebit.innerHTML);
                selectedAmo = selectedAmo + parseFloat(portaDebit.innerHTML);
                portaDebit.innerHTML = '';
            } else {
                portaDebit.innerHTML = portaCredit.innerHTML;
                portabal = portabal + parseFloat(portaCredit.innerHTML);
                selectedAmo = selectedAmo - parseFloat(portaCredit.innerHTML);
                portaCredit.innerHTML = '';
            }
            portaBalance.innerHTML = portabal.toFixed(2);
            portaselectedAmount.innerHTML = selectedAmo.toFixed(2);
        }
        $('input:checkbox').removeAttr('checked');

        function Accounting() {
            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none";
            document.getElementById("content3").style.display = "none";
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab2').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none' });
            $('#tab3').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none'});
        }
        function allCoveredMoney() {
            document.getElementById("content2").style.display = "block";
            document.getElementById("content1").style.display = "none";
            document.getElementById("content3").style.display = "none";
            $('#tab2').css({'font-size': '18px','border-top':'5px solid #f7f7f7;','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none'});
            $('#tab3').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none'});
        }
        function Pickup_Payment() {
            document.getElementById("content3").style.display = "block";
            document.getElementById("content1").style.display = "none";
            document.getElementById("content2").style.display = "none";
            $('#tab3').css({'font-size': '18px','border-top':'5px solid #f7f7f7;','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none'});
            $('#tab2').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0','box-shadow': 'none'});
        }
        function updatecaptainpickupMoney() {
            var captain_id = '<?php echo $captain_id; ?>';
            var captain_info = '<?php echo $captain_info; ?>';
            var cashReceived = document.getElementById('pickup_cashReceived').value;
            var atmReceived = document.getElementById('pickup_atmReceived').value;
            var approval_code = document.getElementById('pickup_approval_code').value;
            var amountReceived = (+cashReceived + +atmReceived).toFixed(2);
            var cashpercentage = (cashReceived / amountReceived * 100).toFixed(2);
            var atmpercentage = (atmReceived / amountReceived * 100).toFixed(2);
            var ids = [];
            for (var i = 0; i < pickup_orders.length; i++) {
                if (document.getElementById('pickupordercheck' + pickup_orders[i].jollychic).checked == true) {
                    ids.push(pickup_orders[i].jollychic);
                    console.log(pickup_orders[i].jollychic);
                }
            }

            var responseObj;
            if (confirm("Are you sure you want to update the captain account of " + captain_info + " since rolling back will be difficult?: ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatePickupMoneyReceivedFromCaptain',
                    dataType: 'text',
                    data: {
                        captain_id: captain_id,
                        ids: ids.join(','),
                        amountReceived: amountReceived,
                        cashpercentage: cashpercentage,
                        atmpercentage: atmpercentage,
                        approval_code: approval_code
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(parseFloat(responseObj.amount).toFixed(2) + ' SAR Received from captain : ' + responseObj.captaindata);
                        window.location = '/warehouse/captainaccounting?captain_id=' + captain_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }
        }

    </script>


    @stop

