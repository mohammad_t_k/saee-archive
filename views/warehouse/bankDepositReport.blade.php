@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Bank Deposit Report</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/bankDepositReportpost">

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">From <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="fromdate" name="fromdate"
                                       value="{{$fromdate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">To <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="todate" name="todate"
                                       value="{{$todate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="company" id="company" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Paid to supplier</label>

                                <div class="col-md-8 col-sm-6 col-lg-7 col-xs-12">
                                    <select name="paidtosupplier" id="paidtosupplier" class="form-control">
                                        <option value="1"<?php if (isset($paidtosupplier) && $paidtosupplier == 1) echo 'selected';?>>
                                            Yes
                                        </option>
                                        <option value="0"<?php if (isset($paidtosupplier) && $paidtosupplier == 0) echo 'selected';?>>
                                            No
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Bank Deposit Report</label>

                                <div class="col-md-8 col-sm-6 col-lg-7 col-xs-12">
                                    <select name="bankdepositreport" id="bankdepositreport" class="form-control">
                                        <option value="deposit_date"<?php if (isset($bankdepositreport) && $bankdepositreport == 'deposit_date') echo 'selected';?>>
                                            Deposit date
                                        </option>
                                        <option value="banks_deposit.updated_at"<?php if (isset($bankdepositreport) && $bankdepositreport == 'banks_deposit.updated_at') echo 'selected';?>>
                                            Paid to Supplier Date
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Transaction <span class="required">*</span></label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="transaction" id="transaction" class="form-control">
                                        <option value="All">All</option>
                                        <option value="1" <?php if (isset($transaction) && $transaction == 1) echo 'selected';?>>Cash</option>
                                        <option value="2" <?php if (isset($transaction) && $transaction == 2) echo 'selected';?>>ATM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Hub </label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Transfer code</label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="transferCode" id="transferCode" class="form-control">
                                        <option value="All">All</option>
                                        @foreach($transferCodes as $code)
                                            <option value="{{$code->transfer_code}}"<?php if (isset($transfer_code) && $transfer_code == $code->transfer_code) echo 'selected';?>>{{$code->transfer_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <button class="btn btn-success" onclick="bankexcelReport()">Export to Excel</button>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-sm-offset-4 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <button class="btn btn-success" onclick="Generate_Bank_report()">Generate Bank Deposit</button>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-sm-offset-8 col-xs-12">
                Search:
                <input type="text" id="searchtransfercode" onkeyup="transfercode()" placeholder="Search for transfer code"  style="padding: 6px; line-height: 20px;margin: 15px 0">
                </div>
            <div class="modal fade" id="myModal" role="dialog" style="overflow: auto">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" dir="rtl">
                            <div id='ModalToPrint'>
                                <img src="/companiesdashboard/images/logo-b.png"
                                     style="margin:auto; width:60px;display:block">
                                <h4>الموضوع: الإيداع البنكي</h4>
                                <h4>التاريخ: &nbsp;&nbsp;&nbsp;<span id="deposit_dates"></span></h4>
                                <h4>المدينة: <span id="city_name"></span></h4>
                                <h4>الفروع: <span id="hub_names"></span></h4>
                                <h4>السادة / الإدارة المالية</h4>
                                <h4>السيد /
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    تحية طيبة وبعد...</h4>
                                <h4>الرجاء الإيداع البنكي في حساب الشركة وفقاً للجدول التالي:</h4>
                            </div>
                            <table id="firsttable" class="table table-striped table-bordered">
                            </table>
                            <div id='tablebotam'>
                                <h4>شاكرين لكم حسن تعاونكم معنا....</h4>
                                <h4 style="margin-right: 20%;">الإدارة المالية&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    المودع بالبنك</h4>
                                <h4>الاسم:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    الاسم:</h4>
                                <h4>التاريخ:&nbsp;&nbsp;&nbsp;<?php echo date("d-m-Y") ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    التاريخ:&nbsp;&nbsp;&nbsp;<?php echo date("d-m-Y") ?></h4>
                                <h4>التوقيع:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    التوقيع: </h4>
                                <h4>رقم الحساب: 607125073900006</h4>
                                <h4>جوال: 0551506465</h4>
                                <h4>جوال: 0506600574</h4>
                            </div>
                            <div id='footer'>
                                <h5 style="margin:auto auto;;width: 100%;text-align: center;">One SAAR Co.For IT<br>CR#4030438178<br>https://www.saee.sa
                                </h5>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="print_modal" class="btn btn-success">Print</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">

                                <table id="datatable_cod" class="table table-striped table-bordered sortable " >
                                    <thead>
                                    <tr>
                                        <th><input id="selectallup" class="dt-checkboxes"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Date</th>
                                        <th>Bank</th>
                                        <th>Company</th>
                                        <th>Hub</th>
                                        <th>City</th>
                                        <th>Admin</th>
                                        <th>Amount</th>
                                        <th>Covered Amount</th>
                                        <th>Cash</th>
                                        <th>ATM</th>
                                        <th>Paid to Supplier</th>
                                        <th>Paid to Supplier Admin</th>
                                        <th>Paid to Supplier Date</th>
                                        <th>Transfer Code</th>
                                        <th>Printed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($items as $item) {
                                    $updated_at = $item['updated_at'];
                                    $updated_at = date_format($updated_at, "Y-m-d");
                                    ?>
                                    <tr id='<?= 'tr' . $item['id'] ?>'>
                                        <td><input id="ch<?= $item['id'] ?>" class="dt-checkboxes"
                                                   value="<?= $item['id'] ?>"
                                                   onchange="onChangeBankDepositCheckBox('<?= $item['id'] ?>')"
                                                   type="checkbox"></td>
                                        <td><?= date('l d M Y', strtotime($item['deposit_date']))  ?></td>
                                        <td><?= $item['bank_name'] ?></td>
                                        <td><?= $item['company_name'] ?></td>
                                        <td><?php if ($item['hub_name'] == 'All') echo ''; else echo $item['hub_name']; ?></td>
                                        <td><?= $item['city'] ?></td>
                                        <td><?= $item['adminuser'] ?></td>
                                        <td><?= $item['amount'] ?></td>
                                        <td><?php if ($item['covered_amount'] == 0) echo '0'; else echo $item['covered_amount']; ?></td>
                                        <td><?= $item['cash_transfer'] ?></td>
                                        <td><?= $item['atm_transfer'] ?></td>
                                        <td><?php if ($item['paid_to_supplier'] == 0) echo 'Not Paid'; else echo 'Paid'; ?></td>
                                        <td><?php if ($item['paid_to_supplier'] == 0) echo 'Not Paid'; else echo $item['paid_admin_name']; ?></td>
                                        <td><?php if ($item['paid_to_supplier'] == 0) echo 'Not Paid'; else echo $updated_at; ?></td>
                                        <td><?php if ($item['transfer_code'] == '') echo ''; else echo $item['transfer_code']; ?></td>
                                        <td><?php if ($item['is_printed'] == 0) echo 'Not Printed'; else echo 'Printed'; ?></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tbody class="avoid-sort">
                                    <tr>
                                        <td colspan="4">Admin</td>
                                        <td>Selected Amount</td>
                                        <td id="selectedAmount">0</td>
                                        <td colspan="2">Total Cash</td>
                                        <td id="selectedcashAmount">0</td>
                                        <td>Total ATM</td>
                                        <td id="selectedatmAmount">0</td>
                                    </tr>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Date</th>
                                        <th>Bank</th>
                                        <th>Company</th>
                                        <th>Hub</th>
                                        <th>City</th>
                                        <th>Admin</th>
                                        <th>Amount</th>
                                        <th>Covered Amount</th>
                                        <th>Cash</th>
                                        <th>ATM</th>
                                        <th>Paid to Supplier</th>
                                        <th>Paid to Supplier Admin</th>
                                        <th>Paid to Supplier Date</th>
                                        <th>Transfer Code</th>
                                        <th>Printed</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                                <input id="amountPayed" name="amountPayed" required="required"
                                       class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12 form-group">
                            <input id="updateSupplierPayedDate" name="updatesupplierpayeddate" type="date" class="form-control" />
                        </div>
                        <div class="col-md-2 col-xs-12 form-group">
                            <input id="transfer_code" name="transfer_code" value="" placeholder="Transfer Code"
                                   class="form-control" type="text"/>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
                            <button id="updateSupplierPayed" class="btn btn-success"
                            <?php $admin_role = Session::get('admin_role');if ($admin_role == 8) {
                                echo "disabled";
                            }?>>Pay to Supplier
                            </button>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
                            <button id="update_payed_to_supplier_date" class="btn btn-success" style="display: none;"
                                    onclick="updatePayedToSupplierDate();">Update Pay to Supplier Date
                            </button>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            {{--<button id="codReportDirect" class="btn btn-success">Cash On Delivery Report</button>--}}
                            <button type="button" class="btn btn-success" onclick="CODpopupOption();">Cash On Delivery
                                Report
                            </button>
                        </div>

                    </div>
                </div>
            </div>
            <!-- table start -->
            <div class="modal fade" id="Modal_of_COD" role="dialog" style="overflow: auto">
                <div class="modal-dialog modal-sm">{{--style="overflow-y: scroll;height: 500px;"--}}
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select Option For COD Report</h4>
                        </div>
                        <div class="modal-body">
                            <table id="cod_report_table" class="table table-striped table-bordered">
                            </table>
                        </div>
                        <div class="modal-footer">
                            <a id="download_codReportDirect" class="btn btn-success" onclick="submitselectedcod()">Submit</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- !table start -->
        </div>


    </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/sorttable/sorttable.js"></script>

    <script>
        var paidtosupplier = document.getElementById('paidtosupplier').value;
        if (paidtosupplier == 1) {
            document.getElementById('updateSupplierPayed').disabled = true;
            //$("input:checkbox").attr('disabled', true)
        }
        var items = <?php echo $items; ?>;
        var company_id = '<?php echo $company_id; ?>';
        <?php $admin_id = Session::get('admin_id'); if ($admin_id == 26 || $admin_id == 32) { ?>
        document.getElementById('update_payed_to_supplier_date').style.display = "block";
        <?php   } ?>
        $("#updateSupplierPayed").click(function (e) {
            var datevalue = document.getElementById('updateSupplierPayedDate').value;
            if(datevalue == ''){
                alert('Please Insert a date');
                return;
            }
            var transfer_code = document.getElementById('transfer_code').value;
            if(transfer_code == ''){
                alert('Please Insert transfer code');
                return;
            }
            var amountPayed = document.getElementById('amountPayed').value;
            var company = document.getElementById('company').value;
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            var responseObj;
            if (confirm("Are you sure you want update deposit amount on " + datevalue + " since rolling back will be difficult? ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatepaymentsupplier',
                    dataType: 'text',
                    data: {
                        ids: ids.join(','),
                        amountPayed: amountPayed,
                        company_id: company,
                        transfer_code: transfer_code,
                        date: datevalue
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(amountPayed + ' SAR is payed to supplier');
                        window.location = '/warehouse/bankDepositReport?company_id=' + company_id;
                    } else {
                        alert('Failed: ' + responseObj.error);
                    }
                });
            }


        });

        function updatePayedToSupplierDate() {
            var datevalue = document.getElementById('updateSupplierPayedDate').value;
            if (datevalue == '') {
                alert('Please Insert a date');
                return;
            }
            var transfer_code = document.getElementById('transfer_code').value;
            if (transfer_code == '') {
                alert('Please Insert transfer code');
                return;
            }
            var company = document.getElementById('company').value;

            var responseObj;
            if (confirm("Are you sure you want to update Paid to Supplier Date " + datevalue + " for transfer code " + transfer_code + " since rolling back will be difficult? ") == true) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updatePayedToSupplierDate',
                    dataType: 'text',
                    data: {
                        company_id: company,
                        transfer_code: transfer_code,
                        date: datevalue
                    },
                }).done(function (response) {
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if (responseObj.success == true) {
                        alert(responseObj.message);
                        document.getElementById('updateSupplierPayedDate').value = '';
                        document.getElementById('transfer_code').value = '';
                    } else {
                        alert(responseObj.error);
                    }
                });
            }

        }
        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == checkbox.checked)
                    continue;
                document.getElementById('ch' + items[i].id).checked = checkbox.checked;
                onChangeBankDepositCheckBox(items[i].id);
            }
        }

        var selectedAmo = 0;

        function onChangeBankDepositCheckBox(deposit_id) {
            var row = document.getElementById("tr" + deposit_id);
            var checkbox = document.getElementById("ch" + deposit_id);
            var credit = row.getElementsByTagName("td")[7];
            var cash = row.getElementsByTagName("td")[9];
            var atm = row.getElementsByTagName("td")[10];
            var selectedAmount = document.getElementById("selectedAmount");
            var selectedcashAmount = document.getElementById("selectedcashAmount");
            var selectedatmAmount = document.getElementById("selectedatmAmount");
            selectedCashAmo = parseFloat(selectedcashAmount.innerHTML);
            selectedAtmAmo = parseFloat(selectedatmAmount.innerHTML);
            selectedAmo = parseFloat(selectedAmount.innerHTML);
            if (checkbox.checked) {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo + parseFloat(credit.innerHTML);
                    selectedCashAmo = selectedCashAmo + parseFloat(cash.innerHTML);
                    selectedAtmAmo = selectedAtmAmo + parseFloat(atm.innerHTML);
                }
            } else {
                if (credit.innerHTML != '') {
                    selectedAmo = selectedAmo - parseFloat(credit.innerHTML);
                    selectedCashAmo = selectedCashAmo - parseFloat(cash.innerHTML);
                    selectedAtmAmo = selectedAtmAmo - parseFloat(atm.innerHTML);
                }
            }
            selectedAmount.innerHTML = selectedAmo.toFixed(2);
            selectedcashAmount.innerHTML = selectedCashAmo.toFixed(2);
            selectedatmAmount.innerHTML = selectedAtmAmo.toFixed(2);
        }

        $("#codReportDirect").click(function (e) {
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0)
                alert('Please select records to view in COD Report');
            else {
                var form = document.createElement("form");
                var element1 = document.createElement("input");

                form.method = "POST";
                form.action = "/warehouse/codreportredirect";


                element1.value = ids.join(',');
                element1.name = "ids";
                form.appendChild(element1);

                document.body.appendChild(form);

                form.submit();
            }

        });

        function bankexcelReport() {
            var i, j;
            var csv = "";

            var table = document.getElementById("datatable_cod");

            var table_headings = table.children[0].children[0].children;
            var table_body_rows = table.children[1].children;

            var heading;
            var headingsArray = [];
            for (i = 1; i < table_headings.length; i++) {
                heading = table_headings[i];
                headingsArray.push('"' + heading.innerHTML + '"');
            }

            csv += headingsArray.join(',') + ";\n";

            var row;
            var columns;
            var column;


            var columnsArray;
            for (i = 0; i < table_body_rows.length ; i++) {
                row = table_body_rows[i];
                columns = row.children;
                columnsArray = [];
                for (j = 1; j < columns.length; j++) {
                    var column = columns[j];
                    columnsArray.push('"' + column.innerHTML + '"');
                }
                csv += columnsArray.join(',') + ";\n";
            }

            download("Bank Deposit Report.csv", csv);

        }

        //From: http://stackoverflow.com/a/18197511/2265487
        function download(filename, text) {
            var universalBOM = "\uFEFF";
            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + text));
            pom.setAttribute('download', filename);

            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            }
            else {
                pom.click();
            }
        }

        $('input:checkbox').removeAttr('checked');
        function Generate_Bank_report() {

            var company = document.getElementById('company').value;
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0) {
                bootbox.alert('Please select records to generate Bank Deposit');
            } else {
                function Generate_Bank_report(callback) {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/generatebankreport',
                        dataType: 'text',
                        cache: false,
                        success: callback,
                        data: {
                            ids: ids.join(','),
                            company_id: company,
                        }
                    })
                }

                Generate_Bank_report(function (res) {
                    var mydata = JSON.parse(res);
                    if (mydata == 0) {
                        bootbox.alert('<h4>No Result  Found</h4>');
                        return;
                    }
                    else {
                        var city_name = '';
                        var deposit_dates = '';
                        var hub_names = '';
                        var saee_bank_deposit = '<tr>' + '<th style="text-align: center">الشركة</th>' + '<th style="text-align: center">المبلغ</th>' + '<th style="text-align: center;width: 20%">مسحوبات</th>'
                                + '<th style="text-align: center">ملاحظات</th>' + '</tr>';
                        var total_amount = 0;
                        $.each(mydata.company_data, function (i, item) {
                            total_amount += parseFloat(item.amount)
                            saee_bank_deposit += '<tr><td>' + item.company_name + '</td><td>' + item.amount + '</td><td>' +
                                    '<input type="text" value="0" class="drawing_amount" onchange="myFunction(this.value,this)"; onkeypress="return isNumber(event,this)"; style="width: 100%;border: none">' +
                                    '</td><td>' + '<input type="text" value="" class="notes" onchange="write_note(this.value,this)" style="width: 100%;border: none" >' + '</td></tr>';

                        });//style="display: none;"
                        saee_bank_deposit += '<tr><td>' + 'الإجمالي' + '</td><td id="totalamount">' + total_amount.toFixed(2) + '</td><td id="total_drawing">' + '0.00' + '</td><td></td></tr>' +
                                '<tr><td>' + 'المودع في البنك' + '</td><td colspan="4" id="bank_amount">' + total_amount.toFixed(2) + '</td></tr>';

                        $.each(mydata.cities, function (i, item) {
                            city_name += item.city + ' ⸲ ';

                        });
                        $.each(mydata.deposit_dates, function (i, item) {
                            deposit_dates += item.deposit_date + ' ⸲ ';

                        });
                        $.each(mydata.hub_names, function (i, item) {
                            hub_names += item.hub_name + ' ⸲ ';

                        });
                        $('#deposit_dates').html(deposit_dates);
                        $('#city_name').html(city_name);
                        $('#hub_names').html(hub_names);
                        $('#firsttable').html(saee_bank_deposit);
                    }
                    $('#myModal').modal("show");
                });
            }
        }


        function isNumber(evt, element) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46 || $(element).val().indexOf('.') != -1)) {
                return false;
            }
            return true;
        }

        function myFunction(val, obj) {
            var total_drawing = document.getElementById("total_drawing");
            var totalamount = document.getElementById("totalamount");
            var bank_amount = document.getElementById("bank_amount");
            var sum = 0;
            $(".drawing_amount").each(function () {
                sum += +$(this).val();
            });
            total_drawing.innerHTML = sum.toFixed(2);
            bank_amount.innerHTML = (totalamount.innerHTML - total_drawing.innerHTML).toFixed(2);
            obj.setAttribute('value', val);
        }
        ;
        function write_note(val, obj) {
            obj.setAttribute('value', val);
        }
        ;

        $('#print_modal').click(function () {
            var ModalToPrint = document.getElementById('ModalToPrint');
            var tablebotam = document.getElementById('tablebotam');
            var footer = document.getElementById('footer');
            var firsttable = document.getElementById('firsttable');
            var Table = '' +
                    '<style type="text/css">' +
                    'table ,th,  td {' +
                    'border:1px solid #000;border-collapse: collapse;direction:rtl;' +
                    '} table {' +
                    'width:100%' +
                    '}' +
                    '</style>';
            Table += firsttable.outerHTML;

            var Table_botam = '' +
                    '<style type="text/css">' +
                    'h4{' +
                    'direction:rtl;' +
                    '}' +
                    '</style>';
            Table_botam += tablebotam.innerHTML;

            var newWin = window.open('', '', 'width=600,height=800');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + ModalToPrint.innerHTML + Table + Table_botam + footer.innerHTML + '</body></html>');
            newWin.document.close();
            newWin.focus();
            newWin.print();
            newWin.close();
        });

        function transfercode() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("searchtransfercode");
            filter = input.value.toUpperCase();
            table = document.getElementById("datatable_cod");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[14];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        function CODpopupOption() {

            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0) {
                alert('Please select records for COD Report');
                return;
            }
            else {

                function CODpopupOption(callback) {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/checkcodviewstatus',
                        dataType: 'text',
                        cache: false,
                        success: callback,
                        data: {
                            ids: ids.join(',')
                        }
                    })
                }

                CODpopupOption(function (res) {
                    var data_count = JSON.parse(res);
                    var COD_report_Information = '<caption><h4>COD Report</h4></caption><tr>' + '<th>Select</th>' + '<th>COD</th>' + '</tr>';
                    COD_report_Information += '<tr><td>' + '<input id="other_check"  type="radio" name="codreportradio">' + '</td><td>' + 'Download COD Report' + '</td></tr><tr><td>'
                            + '<input id="viewcod"  type="radio" name="codreportradio">' + '</td><td>' + 'View COD Report' + '</td></tr>';

                    $('#cod_report_table').html(COD_report_Information);
                    $('#Modal_of_COD').modal("show");
                    if (data_count.count > 500) {
                        document.getElementById("viewcod").disabled = true;
                    }

                });
            }
            return;
        }


        function submitselectedcod() {
            var codreportradio = document.getElementsByName('codreportradio');
            var is_radio_check = '';

            for (var i = 0, length = codreportradio.length; i < length; i++) {
                if (codreportradio[i].checked) {
                    var cod_option = codreportradio[i].parentNode.parentNode.childNodes[1].innerHTML;
                    //if (confirm("Are you sure you want to " + cod_option) == true) {
                        if (cod_option == 'Download COD Report') {
                            var ids = [];
                            for (var i = 0; i < items.length; i++) {
                                if (document.getElementById('ch' + items[i].id).checked == true) {
                                    ids.push(items[i].id);
                                }
                            }
                            if (ids.length == 0) {
                                alert('Please select records to download COD Report');

                            } else {
                                var ids = ids.join(',');
                                var path = '/downloadcodReportDirect?ids=' + ids;
                                location.href = path;
                                $('#Modal_of_COD').modal('hide');
                            }
                            return;
                        } else if (cod_option == 'View COD Report') {
                            var ids = [];
                            for (var i = 0; i < items.length; i++) {
                                if (document.getElementById('ch' + items[i].id).checked == true) {
                                    ids.push(items[i].id);
                                }
                            }
                            if (ids.length == 0) {
                                alert('Please select records to view in COD Report');
                                return;
                            }
                            else {
                                var form = document.createElement("form");
                                var element1 = document.createElement("input");
                                form.method = "POST";
                                form.action = "/warehouse/codreportredirect";
                                element1.value = ids.join(',');
                                element1.name = "ids";
                                form.appendChild(element1);
                                document.body.appendChild(form);
                                form.submit();
                            }
                            $('#Modal_of_COD').modal('hide');
                        }
                    /*} else {
                        bootbox.alert('You cancled!');
                    }*/

                    var is_radio_check = 'ok';
                    break;
                }
            }

            if (is_radio_check == '') {
                bootbox.alert("please select option first");
            }
        }


    </script>
@stop