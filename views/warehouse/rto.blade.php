@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


@extends('warehouse.layout')


@section('content')

    <style>
        /* The Modal (background) */
        #modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 400px; /* Could be more or less, depending on screen size */
            height: 250px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .vertical-line {
            border-left: 1px solid black;
            height: 200px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/styles/spinner.css">

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="container">

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">

                <div class="tab_container">

                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1" class='label2'><i class="fa fa-outdent"></i><span> Under RTO Process</span></label>

                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2" class='label2'><i class="fa fa-outdent"></i> Confirmed RTO</span></label>

                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3" class='label2'><i class="fa fa-outdent"></i> Returned to Origin</span></label>

                    <section id="content1" class="tab-content" style="color: #7E90A4;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                        
                                        <h2>Preparing Shipments To Be Returned</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown">
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="x_content">
                                            <br/>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <textarea id="shipmentIDs1" required="required" class="form-control" type="text" rows="20" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_submit1">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p><button id="submitForm1" class="btn btn-success">Submit</button></p>
                                                    </div>
                                                </div>
                                                <!-- spinner -->
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner1" style="display: none;">
                                                    <br/>
                                                    <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <div class="spinner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- start of reporting section for first tab -->
                        <div class="row" id="unknown_failure_section1" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="unknown_failure_msg1"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="general_success1" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="general_success_msg1"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="failed_items_section1" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_faileditems1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="success_items_section1" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_successitems1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of reporting section for first tab -->

                    </section>


                    <section id="content2" class="tab-content" style="color: #7E90A4;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                
                                        <h2>Shipments Will Be Sent To Origin</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown">
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="x_content">
                                            <br/>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <textarea id="shipmentIDs2" required="required" class="form-control" type="text" rows="20" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_submit2">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p><button id="submitForm2" class="btn btn-success">Submit</button>  </p>
                                                    </div>
                                                </div>
                                                <!-- spinner -->
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner2" style="display: none;">
                                                    <br/>
                                                    <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <div class="spinner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- start of reporting section for second tab -->
                        <div class="row" id="unknown_failure_section2" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="unknown_failure_msg2"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="general_success2" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="general_success_msg2"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="failed_items_section2" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_faileditems2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="success_items_section2" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_successitems2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of reporting section for second tab -->

                    </section>

                    <section id="content3" class="tab-content" style="color: #7E90A4;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">

                                        <h2>Shipments Arrival To Origin is Confirmed</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown">
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="x_content">
                                            <br/>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <textarea id="shipmentIDs3" required="required" class="form-control" type="text" rows="20" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_submit3">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p><button id="submitForm3" class="btn btn-success">Submit</button>  </p>
                                                    </div>
                                                </div>
                                                <!-- spinner -->
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_spinner3" style="display: none;">
                                                    <br/>
                                                    <div class="col-md-1 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <div class="spinner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- start of reporting section for third tab -->
                        <div class="row" id="unknown_failure_section3" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Error <i class="fa fa-times" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="unknown_failure_msg3"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="general_success3" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <h3 class="text-center">Done <i class="fa fa-check" id="correct" aria-hidden="true"></i></h3>
                                        <h4 class="text-center"><span id="general_success_msg3"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="failed_items_section3" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_faileditems3" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Reason</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="success_items_section3" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown"></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="datatable_successitems3" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Waybill</th>
                                                    <th>Company</th>
                                                    <th>Order Number</th>
                                                    <th>Order Reference</th>
                                                    <th>Main City</th>
                                                    <th>City</th>
                                                    <th>Hub</th>
                                                    <th>Porta Hub</th>
                                                    <th>District</th>
                                                    <th>Receiver Name</th>
                                                    <th>Receiver Mobile</th>
                                                    <th>Receiver Mobile 2</th>
                                                    <th>Cash On Delivery</th>
                                                    <th>Status</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of reporting section for third tab -->

                    </section>
            
                </div>
            </div>
        </div>
    </div>
   
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>

        $(document).ready(function() {

            $("#submitForm1").click( function() {

                var shipIDs= document.getElementById("shipmentIDs1").value.replace(/\s/g, ",");
                if (shipIDs=='') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }
            
                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i) {
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }
            
                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i <ids.length; i++ ) {
                    if(!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }
            
                $.each(correctIDs, function(i, el) {
                    if($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });

                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }
        
                if (finalArray.length) {

                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {

                        $("#success_items_section1").css("display", "none");
                        $("#failed_items_section1").css("display", "none");
                        $("#unknown_failure_section1").css("display", "none");
                        $("#general_success1").css("display", "none");
                        $("#div_submit1").css('display', 'none');
                        $("#div_spinner1").css('display', '');
                        
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/work/rto/underProcess',
                            dataType: 'text',
                            data: {ids: finalArray.join(",")}
                        }).done(function(response) {

                            console.log(response);
                            $("#div_submit1").css('display', '');
                            $("#div_spinner1").css('display', 'none');
                            responseObj = JSON.parse(response);
                            if(responseObj.error_message)
                                alert(responseObj.error_message);
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                if(responseObj.general_success_msg) {
                                    $("#general_success_msg1").text(responseObj.message);
                                    $("#general_success1").css('display', '');
                                }
                                else {
                                    alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                                    $("#datatable_successitems1").DataTable().destroy();
                                    if(responseObj.success_items.length) {
                                        let rows = [];
                                        responseObj.success_items.forEach(function(element) {
                                            if(!element.order_reference)
                                                element.order_reference = ''
                                            if(!element.d_city)
                                                element.d_city = '';
                                            if(!element.receiver_name)
                                                element.receiver_name = '';
                                            if(!element.receiver_phone)
                                                element.receiver_phone = '';
                                            if(!element.receiver_phone2)
                                                element.receiver_phone2 == '';
                                            if(element.hub_name == 'All')
                                                element.hub_name = '';
                                            if(element.porta_hub_name == 'All')
                                                element.porta_hub_name = '';
                                            if(element.sub_status_code) 
                                                element.status_name += '/' + element.sub_status_code;
                                            rows.push([
                                                element.jollychic,
                                                element.company_name,
                                                element.order_number,
                                                element.order_reference,
                                                element.main_city,
                                                element.d_city,
                                                element.hub_name,
                                                element.porta_hub_name,
                                                element.d_district,
                                                element.receiver_name,
                                                element.receiver_phone,
                                                element.receiver_phone2,
                                                element.cash_on_delivery,
                                                element.status_name
                                            ]);
                                        });
                                        $("#datatable_successitems1").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#success_items_section1").css("display", "");
                                    }
                                    if(responseObj.failed_items.length) {
                                        $("#datatable_faileditems1").DataTable().destroy();
                                        let rows = [];
                                        responseObj.failed_items.forEach(function(element) {
                                            rows.push([element.waybill, element.error]);
                                        });
                                        $("#datatable_faileditems1").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#failed_items_section1").css("display", "");
                                    }
                                }
                            }
                            else if(responseObj.error_code && responseObj.error_code == 2) {
                                $("#unknown_failure_section1").css('display', '');
                                $("#unknown_failure_msg1").html(responseObj.error);
                            }
                            else
                                alert('Something went wrong!');

                        });
                    }
                }
            });

            $("#submitForm2").click( function() {

                var shipIDs= document.getElementById("shipmentIDs2").value.replace(/\s/g, ",");
                if (shipIDs=='') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i) {
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }

                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i <ids.length; i++ ) {
                    if(!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function(i, el) {
                    if($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });

                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }

                if (finalArray.length) {

                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {

                        $("#success_items_section2").css("display", "none");
                        $("#failed_items_section2").css("display", "none");
                        $("#unknown_failure_section2").css("display", "none");
                        $("#general_success2").css("display", "none");
                        $("#div_submit2").css('display', 'none');
                        $("#div_spinner2").css('display', '');
                        
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/work/rto/confirmed',
                            dataType: 'text',
                            data: {ids: finalArray.join(",")}
                        }).done(function(response) {

                            console.log(response);
                            $("#div_submit2").css('display', '');
                            $("#div_spinner2").css('display', 'none');
                            responseObj = JSON.parse(response);
                            if(responseObj.error_message)
                                alert(responseObj.error_message);
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                if(responseObj.general_success_msg) {
                                    $("#general_success_msg2").text(responseObj.message);
                                    $("#general_success2").css('display', '');
                                }
                                else {
                                    alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                                    $("#datatable_successitems2").DataTable().destroy();
                                    if(responseObj.success_items.length) {
                                        let rows = [];
                                        responseObj.success_items.forEach(function(element) {
                                            if(!element.order_reference)
                                                element.order_reference = ''
                                            if(!element.d_city)
                                                element.d_city = '';
                                            if(!element.receiver_name)
                                                element.receiver_name = '';
                                            if(!element.receiver_phone)
                                                element.receiver_phone = '';
                                            if(!element.receiver_phone2)
                                                element.receiver_phone2 == '';
                                            if(element.hub_name == 'All')
                                                element.hub_name = '';
                                            if(element.porta_hub_name == 'All')
                                                element.porta_hub_name = '';
                                            if(element.sub_status_code)
                                                element.status_name += '/' + element.sub_status_code;
                                            rows.push([
                                                element.jollychic,
                                                element.company_name,
                                                element.order_number,
                                                element.order_reference,
                                                element.main_city,
                                                element.d_city,
                                                element.hub_name,
                                                element.porta_hub_name,
                                                element.d_district,
                                                element.receiver_name,
                                                element.receiver_phone,
                                                element.receiver_phone2,
                                                element.cash_on_delivery,
                                                element.status_name
                                            ]);
                                        });
                                        $("#datatable_successitems2").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#success_items_section2").css("display", "");
                                    }
                                    if(responseObj.failed_items.length) {
                                        $("#datatable_faileditems2").DataTable().destroy();
                                        let rows = [];
                                        responseObj.failed_items.forEach(function(element) {
                                            rows.push([element.waybill, element.error]);
                                        });
                                        $("#datatable_faileditems2").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#failed_items_section2").css("display", "");
                                    }
                                }
                            }
                            else if(responseObj.error_code && responseObj.error_code == 2) {
                                $("#unknown_failure_section2").css('display', '');
                                $("#unknown_failure_msg2").html(responseObj.error);
                            }
                            else
                                alert('Something went wrong!');

                        });
                    }
                }
            });

            $("#submitForm3").click( function() {

                var shipIDs= document.getElementById("shipmentIDs3").value.replace(/\s/g, ",");
                if (shipIDs=='') {
                    bootbox.alert('Please enter IDs!');
                    return;
                }

                var ids = shipIDs.split(',');
                for(i = 0; i < ids.length;++i) {
                    if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                        ids[i] = ids[i].toUpperCase();
                }

                var wrongIDs = Array();
                var correctIDs = Array();
                var correctedIDs = Array();
                var finalArray = [];
                for (var i = 0; i <ids.length; i++ ) {
                    if(!ids[i].replace(/\s/g, '').length) {
                        continue;
                    }
                    if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i])) {
                        correctIDs.push(ids[i]);
                    }
                    else {
                        wrongIDs.push(ids[i]);
                    }
                }

                $.each(correctIDs, function(i, el) {
                    if($.inArray(el, finalArray) === -1) {
                        finalArray.push(el);
                    }
                });

                if (wrongIDs.length) {
                    bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                }

                if (finalArray.length) {

                    if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {

                        $("#success_items_section3").css("display", "none");
                        $("#failed_items_section3").css("display", "none");
                        $("#unknown_failure_section3").css("display", "none");
                        $("#general_success3").css("display", "none");
                        $("#div_submit3").css('display', 'none');
                        $("#div_spinner3").css('display', '');
                        
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/updatestatus',
                            dataType: 'text',
                            data: {to: 7, ids: finalArray.join(",")}
                        }).done(function(response) {

                            console.log(response);
                            $("#div_submit3").css('display', '');
                            $("#div_spinner3").css('display', 'none');
                            responseObj = JSON.parse(response);
                            if(responseObj.error_message)
                                alert(responseObj.error_message);
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                if(responseObj.general_success_msg) {
                                    $("#general_success_msg3").text(responseObj.message);
                                    $("#general_success3").css('display', '');
                                }
                                else {
                                    alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                                    $("#datatable_successitems3").DataTable().destroy();
                                    if(responseObj.success_items.length) {
                                        let rows = [];
                                        responseObj.success_items.forEach(function(element) {
                                            if(!element.order_reference)
                                                element.order_reference = ''
                                            if(!element.d_city)
                                                element.d_city = '';
                                            if(!element.receiver_name)
                                                element.receiver_name = '';
                                            if(!element.receiver_phone)
                                                element.receiver_phone = '';
                                            if(!element.receiver_phone2)
                                                element.receiver_phone2 == '';
                                            if(element.hub_name == 'All')
                                                element.hub_name = '';
                                            if(element.porta_hub_name == 'All')
                                                element.porta_hub_name = '';
                                            if(element.sub_status_code)
                                                element.status_name += '/' + element.sub_status_code;
                                            rows.push([
                                                element.jollychic,
                                                element.company_name,
                                                element.order_number,
                                                element.order_reference,
                                                element.main_city,
                                                element.d_city,
                                                element.hub_name,
                                                element.porta_hub_name,
                                                element.d_district,
                                                element.receiver_name,
                                                element.receiver_phone,
                                                element.receiver_phone2,
                                                element.cash_on_delivery,
                                                element.status_name
                                            ]);
                                        });
                                        $("#datatable_successitems3").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#success_items_section3").css("display", "");
                                    }
                                    if(responseObj.failed_items.length) {
                                        $("#datatable_faileditems3").DataTable().destroy();
                                        let rows = [];
                                        responseObj.failed_items.forEach(function(element) {
                                            rows.push([element.waybill, element.error]);
                                        });
                                        $("#datatable_faileditems3").DataTable({
                                            "data": rows,
                                            "autoWidth": false,
                                            dom: "Blfrtip",
                                            buttons: [
                                                {
                                                    extend: "copy",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "csv",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "excel",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "pdfHtml5",
                                                    className: "btn-sm"
                                                },
                                                {
                                                    extend: "print",
                                                    className: "btn-sm"
                                                },
                                            ],
                                            responsive: true,
                                            'order': [[0, 'asc']]
                                        });
                                        $("#failed_items_section3").css("display", "");
                                    }
                                }
                            }
                            else if(responseObj.error_code && responseObj.error_code == 2) {
                                $("#unknown_failure_section3").css('display', '');
                                $("#unknown_failure_msg3").html(responseObj.error);
                            }
                            else
                                alert('Something went wrong!');

                        });
                    }
                }
            });

        });
    </script>

@stop

