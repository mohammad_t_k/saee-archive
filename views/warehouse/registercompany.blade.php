@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif

@extends('warehouse.layout')


@section('content')
<style>
#map_canvas {
  height: 300px;
  width: 100%;
  margin: 0;
}

#map_canvas .centerMarker {
  position: absolute;
  /*url of the marker*/
  background: url(../web/images/marker.png) no-repeat;
  /*center the marker*/
  top: 50%;
  left: 50%;
  z-index: 1;
  /*fix offset when needed*/
  margin-left: -10px;
  margin-top: -34px;
  /*size of the image*/
  height: 34px;
  width: 20px;
  cursor: pointer;
}
</style>
 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Register Company</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
       
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" class="form" id="main-form" method="post" action="/warehouse/registercompanypost"
              enctype="multipart/form-data">
            <input id="latitude" name="latitude" required="required" type="hidden">
            <input id="longitude" name="longitude" required="required" type="hidden">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="name" name="name" class="form-control col-md-7 col-xs-12" required="required" placeholder="e.g. Jolly Chic" type="text" maxlength="200">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Arabic Name <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="name_ar" name="name_ar" class="form-control col-md-7 col-xs-12" required="required" placeholder="e.g. جولي شيك" type="text" maxlength="200">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Username <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="username" name="username" class="form-control col-md-7 col-xs-12" required="required" type="text" placeholder="e.g. jollychic" maxlength="20">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="email" name="email" class="form-control col-md-7 col-xs-12" required="required" placeholder="e.g. jollychic@gmail.com" type="text" maxlength="200">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="phone" name="phone" class="form-control col-md-7 col-xs-12" required="required" placeholder="e.g. +966512345678" type="text" maxlength="15">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="password" name="password" class="form-control col-md-7 col-xs-12" required="required" type="text" maxlength="25">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="address" name="address" class="form-control col-md-7 col-xs-12" required="required" type="text" maxlength="500">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="city" name="city" class="form-control col-md-7 col-xs-12" required="required" placeholder="E.g. Jaddah" type="text" maxlength="50">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip Code <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="zipcode" name="zipcode" class="form-control col-md-7 col-xs-12" required="required" placeholder="" type="text" maxlength="10">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Country <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="country" name="country" class="form-control col-md-7 col-xs-12" required="required" placeholder="" type="text">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Transportation Fee  in SAR<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="transportation_fee" name="transportation_fee" class="form-control col-md-7 col-xs-12" placeholder="e.g. 15" required="required" type="text" onkeypress="return isNumber(event)" maxlength="5">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pick Up Fee<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="pickup_fee" name="pickup_fee" class="form-control col-md-7 col-xs-12" placeholder="" required="required" type="text" onkeypress="return isNumber(event)" maxlength="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Fee<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="delivery_fee" name="delivery_fee" class="form-control col-md-7 col-xs-12" placeholder="" required="required" type="text" onkeypress="return isNumber(event)" maxlength="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Fee 2<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="delivery_fee2" name="delivery_fee2" class="form-control col-md-7 col-xs-12" placeholder="" required="required" type="text" onkeypress="return isNumber(event)" maxlength="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Fee 3<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="delivery_fee3" name="delivery_fee3" class="form-control col-md-7 col-xs-12" placeholder="" required="required" type="text" onkeypress="return isNumber(event)" maxlength="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Max Regular Weight in KGs<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="max_regular_weight" name="max_regular_weight" class="form-control col-md-7 col-xs-12" placeholder="e.g. 10" required="required" type="text" onkeypress="return isNumber(event)"maxlength="5" >
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Excess Weight Fee<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <input id="excess_weight_fees" name="excess_weight_fees" class="form-control col-md-7 col-xs-12" placeholder="e.g. 1" required="required" type="text" onkeypress="return isNumber(event)" maxlength="5">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">API Access<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <select name="is_api" id="is_api" class="form-control col-md-7 col-xs-12">
                        <option>Select API</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Allow Creation<span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <select name="allow_creation" id="allow_creation" class="form-control col-md-7 col-xs-12">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Location On Map <span class="required">*</span></label>
                <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                    <div id="map_canvas"></div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
                <button type="submit" class="btn btn-primary btn-flat btn-block">Register Company</button>
            </div>
        </form>
    </div>
</div>
</div>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function initMap() {
    try{
    var  latitude=21.2854;
    var  longitude=39.2376;
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(latitude, longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
    var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);
    google.maps.event.addListener(map,'center_changed', function() {
        document.getElementById('latitude').value = map.getCenter().lat();
        document.getElementById('longitude').value = map.getCenter().lng();
    });
      $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
        //do something onclick
        .click(function() {
          var that = $(this);
          if (!that.data('win')) {
            that.data('win', new google.maps.InfoWindow({
              content: 'this is the center'
            }));
            that.data('win').bindTo('position', map, 'center');
          }
          that.data('win').open(map);
        });}
    	catch(e){
    	alert(e);
    }
}
google.maps.event.addDomListener(window, 'load', initMap);</script>
@stop

