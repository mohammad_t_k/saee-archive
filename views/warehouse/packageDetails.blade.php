@if( Auth::user()->role < 3)

  <script>window.location = "/warehouse/403";</script>
  
@endif


@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<?php

$text = 'Dear '.$company_name.' Customer, we cannot reach you, please WhatsApp us on this number to confirm the address of your shipment '.$packagedetails->jollychic.' %0A
عزيزي عميل '.$company_name.' لم نستطع الوصول إليك الرجاء التواصل/واتساب على هذا الرقم للتأكد من عنوان شحنتكم '.$packagedetails->jollychic.'';

$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

?>

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Shipment Details</h3>
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $packagedetails->jollychic ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                    <form class="form-horizontal form-label-right" action="/warehouse/updatecontact" method="post" >
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Waybill</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="waybill" readonly value =<?php echo $packagedetails->jollychic ?> class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name </label>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->receiver_name ?> </label>
                        </div>

                        <?php 

                        $phone1 = preg_replace('~^[0\D]++|\D++~', '', $packagedetails->receiver_phone);
                        $phone2 = preg_replace('~^[0\D]++|\D++~', '', $packagedetails->receiver_phone2);



                        ?>
                      
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Mobile<span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile1"  align="right" value ="<?php echo $packagedetails->receiver_phone ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">

                          <?php if ($iphone || $android || $palmpre || $ipod || $berry == true)
                          { ?>
                         <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $phone1; ?>&text=<?php echo $text; ?>"> <img width="30" src="<?php echo asset_url(); ?>/warehouseadmin/build/images/whatsapp.png"></a>
                          <?php } else{ ?>
                          <a target="_blank" href="https://web.whatsapp.com/send?phone=<?php echo $phone1; ?>&text=<?php echo $text; ?>"><img width="30" src="<?php echo asset_url(); ?>/warehouseadmin/build/images/whatsapp.png"></a>
                          <?php } ?>

                        </div>
                        </div>

                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Mobile<span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile2"   align="right" value ="<?php echo $packagedetails->receiver_phone2 ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12">

                          <?php if ($iphone || $android || $palmpre || $ipod || $berry == true)
                          { ?>
                         <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $phone2; ?>&text=<?php echo $text; ?>"> <img width="30" src="<?php echo asset_url(); ?>/warehouseadmin/build/images/whatsapp.png"></a>
                          <?php } else{ ?>
                          <a target="_blank" href="https://web.whatsapp.com/send?phone=<?php echo $phone2; ?>&text=<?php echo $text; ?>"><img width="30" src="<?php echo asset_url(); ?>/warehouseadmin/build/images/whatsapp.png"></a>
                          <?php } ?>

                        </div>
                        </div>
                        
                     
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address ?> </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address2 ?> </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address3 ?> </label>
                      </div>
                      
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_district ?> </label>
                      </div>
                      
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Change district to</span>
                            </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="district">
                                <?php 

                                $grouped = 'select distinct grouped from districts a,city b where SUBSTRING_INDEX(district, "+", 1) = "'.$packagedetails->d_city.'" and a.city = b.name';

                                $groupedCity = DB::select($grouped);

                                
                                
                                if (count($groupedCity)) {

                                      $checkgroup = '1';
                                     
                                  } else {

        
                                      $checkgroup = '0';

                                  }

                                  if($checkgroup==1){

                                $cityDistricts = 'SUBSTRING_INDEX(district, "+", 1)= "'.$packagedetails->d_city.'"';

                                $districts = Districts::whereRaw($cityDistricts)->orderBy('district', 'asc')->get(['district']);

                                }else{

                                  $districts = Districts::where('city','like','%'.$packagedetails->d_city.'%')->orderBy('district', 'asc')->get(['district']);
                                }
                             


                                echo "<option value=\"$packagedetails->d_district\">$packagedetails->d_district</option> ";

                                foreach ( $districts as $adistrict) 
                                {
                                    
                                    echo "<option value=\"$adistrict->district\">$adistrict->district</option> ";
                                    
                                }
                              ?>
                                 
    
                            </select>
                        </div>
                        </div>
                       
                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_city ?> </label>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name </label>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Companies::select('company_name')->where('id', '=', $packagedetails->company_id)->first()->company_name ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">State </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_state ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Created at </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->created_at ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment Date</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->shipment_date ?> </label>
                      </div>

                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Scheduled Shipment Date</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->scheduled_shipment_date  ?> </label>
                      </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sort Date</label>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php  if ($packagedetails->sort_date == '0000-00-00 00:00:00') {
                                    echo '';
                                } else {
                                    echo $packagedetails->sort_date;
                                }  ?> </label>
                        </div>

                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">In Route Date</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->new_shipment_date  ?> </label>
                      </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sign in Date</label>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->signin_date  ?> </label>
                        </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php
                            if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0 && strtotime($packagedetails->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                                echo 'Warehouse/Res';
                            else if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0)
                                echo 'Warehouse/R';
                            else if($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts == 0)
                                echo 'Warehouse/New';
                            else
                                echo $statusDef[$packagedetails->status];?> </label>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Last Status Update</label>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->laststatusupdate; ?></label>
                      </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Date</label>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $Delivery_Date; ?></label>
                        </div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Return To Supplier Date</label>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->return_to_supplier_date; ?></label>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Paid TO Supplier Date</label>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $Paid_to_Supplier_Date?></label>
                      </div>
                      
                      <div class="form-group" style="display:<?php if($packagedetails->num_faild_delivery_attempts>0) echo 'block';else echo 'none';?>" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Undelivered reason </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->undelivered_reason ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">COD </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->cash_on_delivery ?>  SAR</label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pincode </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->pincode ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_description ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_quantity ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_weight ?> </label>
                      </div>
                      
                      
                      <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Comments</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea class="resizable_textarea form-control" name="notes" placeholder=""></textarea>
                    </div>
                    </div>
                    <br/>
                          <div class="col-md-6 col-md-offset-3">
                            <input type='submit'value='save' class="btn  btn-success" />
                        </div>

                    </form>
                      
                      
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


  <!-- iCheck -->
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
  <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

  <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
        rel="stylesheet"/>
  <script type="text/javascript"
          src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

@stop