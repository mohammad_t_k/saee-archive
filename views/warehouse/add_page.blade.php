@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


<?php
if(Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if(Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

@section('content')

<?php

$allowed_actions =  AllowedActions::select(['id', 'action_name'])->get();

 ?>

<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        @if (Session::has('msg'))
            <h4 class="alert alert-info">
                {{ Session::get('msg')}}
                {{Session::put('msg',NULL)}}
            </h4>
        @endif
        
            <div class="box-body ">
                <form action="" method="POST" id="ActionForm">
                <div class="form-group">
                    <label>Page Name</label><input id="pagename" required="" class="form-control" type="text" name="pagename"
                                              placeholder="Add Page Name">
                </div>

                <div class="form-group">
                    <label>Page URL</label><input id="pageurl" required="" class="form-control" type="text" name="pageurl"
                                              placeholder="Add Page URL">
                </div>

                <div class="form-group">
                    <label>Select Allowed Actions (Multiple selection allowed)</label>
                    <select required="" style="width: 100%" name="allowed_actions" id="allowed_actions" multiple>
                       @foreach($allowed_actions as $aAction)
                        <option value="{{$aAction->id}}">{{$aAction->action_name}}</option>
                        @endforeach
                    </select>
                </div> 


            </div>
           
                <button  type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Add Page</button>

            </form>
        

    </div>
</div>
</div>

<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>


    <script>


        $(document).ready(function () {

            $("#ActionForm").submit(function (e) {
                

                        var pagename = document.getElementById("pagename").value;
                        var pageurl = document.getElementById("pageurl").value;

                        //var allowed_pages = new Array();//storing the selected values inside an array
                        //var allowed_actions = new Array();

                        /* $('#allowed_pages :selected').each(function(i, selected) {
                                allowed_pages[i] = $(selected).val();
                            }); */

                         var allowed_actions = new Array();//storing the selected values inside an array

                         $('#allowed_actions :selected').each(function(j, selected) {
                                allowed_actions[j] = $(selected).val();
                            }); 

                        //var allowed_pages = allowed_pages.split(',');
                       // var allowed_actions = allowed_actions.split(',');


                        var wrongIDs = Array();
                        var correctAllowedPages = Array();
                        var correctAllowedActions = Array();
                        var correctedIDs = Array();
                        var finalArrayAllowedPages = [];
                        var finalArrayAllowedActions = [];
                        



                       /* if (correctedIDs.length) {
                            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                        } */

                        if (allowed_actions.length) {
                            if (confirm("Are you sure Actions and pages are correct ? ") == true) {
                                 e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/system/add_page_post',
                                    dataType: 'text',
                                    data: {pagename: pagename, pageurl: pageurl, allowedactions: allowed_actions},
                                }).done(function (response) {

                                    console.log(response);

                                   window.location='/warehouse/system/pages?success=1';
                                    
                                }); // Ajax Call

                            }
                            else {
                                bootbox.alert('You cancled!');
                                return false;
                            }

                        }
                        else {
                            bootbox.alert('Nothing to signin!');
                            return false;
                        }


                    }
            );
        });

    </script>

@stop