@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends('warehouse.layout')

<style>
    
    .yellowClass{
        background-color:yellow !important;
    }
    .print-link{

        cursor: pointer;
    }
</style>

    @section('content')
            <!-- page content -->
    <script>
        var todatsdate = '<?php echo $shipmentDate ?>';
        console.log(todatsdate);
    </script>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Planning for Shipment <?php echo $shipmentDate ?></h3>
                </div>

            </div>
            <!-- top tiles -->
            <div class="row tile_count">
            </div>
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>
                    <div class="count"><?php echo $allitems ?> </div>


                </div>


                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Number of distcicts</span>
                    <div class="count green"><?php echo $numofdistricts ?> </div>

                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Total Cash to be collected </span>
                    <div class="count red"><?php echo number_format($cashtobecollected,2) ?></div>
                </div>

                <!--
                 <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> New Packages</span>
                    <div class="count">< ?php// echo $newitems ?> </div>

                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Old Packages</span>
                    <div class="count">< ?php //echo $olditems ?> </div>

                </div>
                -->

            </div>
            <div class="row tile_count">

                <!--
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> New Cash to be collected </span>
                    <div class="count red">< ?php //echo number_format($cashtobecollectednew,2) ?></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Old Cash to be collected </span>
                    <div class="count red">< ?php //echo number_format($cashtobecollectedold,2) ?></div>
                </div>
                -->
            </div>

            <!-- /top tiles -->


            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form method="get" action="/warehouse/work/plandispatch">
                            
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" required="required" class="form-control"
                                                type="text">
                                            <option value="all">All</option>
                                            @foreach($companies as $compan)
                                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Scheduled Shipment Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                               <select name="scheduled_date" id="scheduled_date" class="form-control">
                                    <option value="all" selected>All</option>
                                    <?php
                                    foreach ($citiesDates as $citydates) {
                                        if ($citydates->scheduled_shipment_date != null) {
                                            $selected = '';
                                            $date = $citydates->scheduled_shipment_date;
                                            if (isset($shipmentDate) && $shipmentDate == $date)
                                                $selected = 'selected';
                                            echo "<option value='$date' $selected>$date</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- bar chart -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Districts <small>Histogram</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="districtschart" style="width:100%; height:280px;"></div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Update Districts </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">Company</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <select name="company" id="_company" class="form-control">
                                        @foreach($companies as $_compan)
                                            <option value="{{$_compan->id}}">{{$_compan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">City</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <select name="city" id="_city" class="form-control">
                                        @foreach($adminCities as $_cit)
                                            <option value="{{$_cit->city}}">{{$_cit->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">From</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="date" name="from" id="_from" class="form-control col-md-8 col-xs-12" />
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">To</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="date" name="to" id="_to" class="form-control col-md-8 col-xs-12" />
                                </div>
                            </div>
                            <div class="clearfix"></div><br/>
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">Scheduled Shipment Date</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="date" name="scheduled_shipment_date" id="_scheduled_shipment_date" class="form-control col-md-8 col-xs-12" />
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="col-md-4 col-xs-12">
                                    <label class="col-xs-12">Status</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <?php
                                    $_order_statuses = OrderStatuses::all()->except(8);
                                    ?>
                                    <select name="status" id="_status" class="form-control">
                                        @foreach($_order_statuses as $_stat)
                                            <option value="{{$_stat->id}}">{{$_stat->english}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-md-offset-1">
                                <div class="col-md-5 col-xs-12">
                                    <button id="districtButton" class="btn btn-primary">Update Districts</button>
                                </div>
                                <div class="col-md-5 col-xs-12">
                                    <span class="fa fa-spinner fa-spin" id="district_spinner" style="font-size:24px; display: none;"></span>
                                    <i class="fa fa-check" id="district_correct" style="color: green; display: none; font-size: 24px;" aria-hidden="true"></i>
                                    <i class="fa fa-times" id="district_times" style="color: red; display: none; font-size: 24px;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!-- /bar charts -->
                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><small>Items that are missing district information</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that are missing district info
                            </p>
                            <!--<p> <a class="setdistrict" ><button>Set District</button></a></p> -->

                            <p id =setdistrict><button>Set District</button></p>

                            <table id="datatable_missingDistrict" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All shipments Shipment <small>Planning by districts</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                All items for today + planning by district
                            </p>
                            <p id =assignToCaptainAllByDistrict><button>Assign selected to captain</button></p>
                            <p id =assignGroupIDAllByDistrict><button>Set Group ID</button></p>
                            <p id =autoassigngroupid><button>Auto Assign Group ID</button></p>
                            <table id="datatable_planningbydistrict" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>District</th>
                                    <th>Count</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>District</th>
                                    <th>Count</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->


                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Shipment <small>Planning</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                All items for today
                            </p>
                            <p id =assignToCaptainAll><button>Assign selected to captain</button></p>
                            <p id =groupIDall><button>Set Group ID</button></p>
                            <p id =autoplan><button>AUTO PLAN!!</button></p>

                            <table id="datatable_planningALL" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>District Updated</th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                    <th>Status</th>
                                    <th>Last Comment</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>District Updated</th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Captain</th>
                                    <th>Group ID</th>
                                    <th>Status</th>
                                    <th>Last Comment</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->
                <!-- table start -->
                <!--
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>New shipment <small>Planning</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                        Items that are in thier way to Kasper Warehouse
                        </p>
                        <p id = assignToCaptain><button>Assign selected to captain</button></p>
                        <p id =groupIDnew><button>Set Group ID</button></p>
                        <table id="datatable_planning" class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                              <th></th>
                              <th>JollyChicID</th>
                              <th>Name</th>
                              <th>Mobile1</th>
                              <th>Mobile2</th>
                              <th>Street Addrs</th>
                              <th>District</th>
                              <th>COD</th>
                              <th>Captain</th>
                              <th>Group ID</th>
                            </tr>
                          </thead>


                          <tfoot>
                            <tr>
                              <th></th>
                              <th>JollyChicID</th>
                              <th>Name</th>
                              <th>Mobile1</th>
                              <th>Mobile2</th>
                              <th>Street Addrs</th>
                              <th>District</th>
                              <th>COD</th>
                              <th>Captain</th>
                              <th>Group ID</th>
                            </tr>
                          </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
                -->
                <!-- !table start -->
                <!-- table start -->
                <!--
               <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="x_panel">
                       <div class="x_title">
                           <h2> Old Shipments | رجيع <small>Planning</small></h2>
                           <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                               </li>
                               <li><a class="close-link"><i class="fa fa-close"></i></a>
                               </li>
                           </ul>
                           <div class="clearfix"></div>
                       </div>
                       <div class="x_content">
                       <p class="text-muted font-13 m-b-30">
                       These items are already in the warehouse and need to be redeliverd. رجيع مجدول للتوزيع مع السحنة الجديدة.
                       </p>
                       <p id =assignToCaptainOld><button>Assign selected to captain</button></p>
                       <p id =groupIDold><button>Set Group ID</button></p>
                       <table id="datatable_oldUndelivered" class="table table-striped table-bordered ">
                       <thead>
                           <tr>
                             <th></th>
                             <th>JollyChicID</th>
                             <th>Name</th>
                             <th>Mobile1</th>
                             <th>Mobile2</th>
                             <th>Street Addrs</th>
                             <th>District</th>
                             <th>COD</th>
                             <th>Captain</th>
                             <th>Group ID</th>
                           </tr>
                         </thead>


                         <tfoot>
                           <tr>
                             <th></th>
                             <th>JollyChicID</th>
                             <th>Name</th>
                             <th>Mobile1</th>
                             <th>Mobile2</th>
                             <th>Street Addrs</th>
                             <th>District</th>
                             <th>COD</th>
                             <th>Captain</th>
                             <th>Group ID</th>
                           </tr>
                         </tfoot>
                       </table>
                       </div>
                   </div>
               </div>
               -->
                <!-- !table start -->
                <?php //echo $orders ?>



            </div>
        </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#districtbutton").click(function() {
            if(confirm('This update may take a few minutes. Are you sure you want to update?') == true) {
                document.getElementById("district_spinner").style.display = '';
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/updateDistricts',
                    dataType: 'text'
                }).done(function (response) {
                    console.log(response);
                    document.getElementById("district_spinner").style.display = 'none';
                    document.getElementById("districtmessage").style.display = '';
                    if (response > 0)
                        document.getElementById("districtmessage").textContent = response + ' districts remains not updated. try updating again.';
                });
            }
        });
    </script>

    <script >

        <?php



        $mydata_newShipments = '';
        $mydata_oldShipments = '';
        $mydata_histogram = '';
        $mydata_missingDistrict = '';
        $mydata_districts = '';
        $mydata_planningbydistrict = '';
        $mydata_allShipments= '';

        foreach ($bydistrict as $row) {
            $Isgrouped = 'select distinct grouped from districts a,city b where SUBSTRING_INDEX(district, "+", 1) = "'.$row->d_city.'" and a.city = b.name';
            $grouped = DB::select($Isgrouped);
            //$grouped = $grouped[0]->grouped;

            if(count($grouped)){
               $grouped = 1;
            }
            else {

                $grouped = 0;

            }
          $row->cod = number_format($row->cod,2);

          if($grouped==1){
           $mydata_planningbydistrict .= "[ \"$row->d_district\",\"$row->d_city+$row->d_district\" , \"$row->counts\" ,\"$row->cod\", \"$row->planned_walker\", \"$row->group_id\"], ";
       }else{

        $mydata_planningbydistrict .= "[ \"$row->d_district\",\"$row->d_district\" , \"$row->counts\" ,\"$row->cod\", \"$row->planned_walker\", \"$row->group_id\"], ";
           // $mydata_planningbydistrict .= "[ \"$row->d_district\",\"$row->d_district\" , \"$row->counts\" ,\"$row->cod\", \"$row->planned_walker\"], ";
       }

        }

        foreach ($districtsHist as $order) {

            $mydata_histogram .= "{ district: \"$order->d_district\", items:\"$order->num_items\" }, ";

        }

        foreach ($missingdistrict as $order) {

            $mydata_missingDistrict .= "[ \"$order->jollychic\",\"$order->jollychic\",\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\"], ";

        }


        /*foreach ($missingdistrictnew as $order) {

            $mydata_missingDistrict .= "[ \"$order->jollychic\",\"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\"], ";


        }*/

        /*foreach ($newestshipments as $order) {

            $mydata_newShipments .= "[ \"$order->jollychic\",\"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";

        }


        foreach ($oldshipments as $order) {

            $mydata_oldShipments .= "[ \"$order->jollychic\",\"$order->jollychic\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\"], ";


        }*/

        //$mydata_allShipments = $mydata_newShipments . $mydata_oldShipments;

        foreach ($allshipments as $order) {
            
            if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                $status = 'Warehouse/Res';
            else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                $status = 'Warehouse/R';
            else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                $status = 'Warehouse/New';
            else
                $status = $statusDef[$order->status];

            $order->comment = str_replace("\t", ' ', $order->comment); 
            $order->comment = str_replace("\n", ' ', $order->comment); 
            $order->comment = str_replace("\r", ' ', $order->comment); 

            $mydata_allShipments .= "[ \"$order->is_district_updated\",\" $order->jollychic\",\"<a class='print-link' onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>\",\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->planned_walker\", \"$order->group_id\" , \"$status\", \"$order->comment\" ], ";


        }
        foreach ($distictsInfo as $district) {

            $mydata_districts .= "{ value:  \"$district->id\", text:\"$district->district\" }, ";
        }

        ?>



        $(document).ready(function() {


                    Morris.Bar({
                        element: 'districtschart',
                        data: [
                            <?php echo $mydata_histogram ?>
                          ],
                        xkey: 'district',
                        ykeys: ['items'],
                        labels: ['Items'],
                        barRatio: 0.6,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 75,
                        hideHover: 'auto',
                        resize: true
                    });

                    var table_missingDistrict = $("#datatable_missingDistrict").DataTable({

                        "data": [
                            <?php echo $mydata_missingDistrict ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }
                        ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });


                    var table_planningbydistrict = $("#datatable_planningbydistrict").DataTable({

                        "data": [
                            <?php echo $mydata_planningbydistrict ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }
                        ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });



                    var table_planningALL = $("#datatable_planningALL").DataTable({


                        "createdRow": function( row, data, dataIndex){

                   
                    var is_district_updated = data[0];
                
              
                if(is_district_updated == 1) {
                     $(row).addClass('yellowClass');
                 }
                
            },

                        "data": [
                            <?php echo $mydata_allShipments ?>

                          ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            },

                            {
                                "targets": 1,
                                "visible": false
                            }
                        ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });

                     
                    /*var table_planning = $("#datatable_planning").DataTable({

                     "data": [
                     < ?php echo $mydata_newShipments ?>

                     ],
                     "autoWidth": false,
                     dom: "Blfrtip",
                     buttons: [
                     {
                     extend: "copy",
                     className: "btn-sm"
                     },
                     {
                     extend: "csv",
                     className: "btn-sm"
                     },
                     {
                     extend: "excel",
                     className: "btn-sm"
                     },
                     {
                     extend: "pdfHtml5",
                     className: "btn-sm"
                     },
                     {
                     extend: "print",
                     className: "btn-sm"
                     },
                     ],
                     responsive: true,
                     'columnDefs': [
                     {
                     'targets': 0,
                     'checkboxes': {
                     'selectRow': true
                     }
                     }
                     ],
                     'select': {
                     'style': 'multi'
                     },
                     'order': [[1, 'asc']]
                     });*/

                    /*var table_oldUndelivered = $("#datatable_oldUndelivered").DataTable({

                     "data": [
                     < ?php echo $mydata_oldShipments ?>

                     ],
                     "autoWidth": false,
                     dom: "Blfrtip",
                     buttons: [
                     {
                     extend: "copy",
                     className: "btn-sm"
                     },
                     {
                     extend: "csv",
                     className: "btn-sm"
                     },
                     {
                     extend: "excel",
                     className: "btn-sm"
                     },
                     {
                     extend: "pdfHtml5",
                     className: "btn-sm"
                     },
                     {
                     extend: "print",
                     className: "btn-sm"
                     },
                     ],
                     responsive: true,
                     'columnDefs': [
                     {
                     'targets': 0,
                     'checkboxes': {
                     'selectRow': true
                     }
                     }
                     ],
                     'select': {
                     'style': 'multi'
                     },
                     'order': [[1, 'asc']]
                     }); */


 
                    $("#setdistrict").click(function(e)  {

                        var rows_selected = table_missingDistrict.column(0).checkboxes.selected();
                        if (rows_selected.count())
                        {
                            bootbox.prompt({
                                title: "Please select a district!",
                                inputType: 'select',
                                inputOptions: [
                                    <?php echo  $mydata_districts ?>
                                 ],
                                callback: function (districtID)
                                {
                                    console.log(districtID);

                                    var responseObj;
                                    console.log("rows: " + rows_selected.join(','));
                                    $.ajax({
                                        type: 'POST',
                                        url: '/warehouse/setdistrict',
                                        dataType: 'text',
                                        data: {districtid:districtID, ids:rows_selected.join(',') },
                                    }).done(function (response) {
                                        console.log(response);
                                        responseObj = JSON.parse(response);
                                        alert(rows_selected.count() + ' items have been assigned to district ' +responseObj.district_name   );
                                        console.log(response);
                                        if (responseObj.success==true)
                                        {
                                            location.reload(true);
                                        }
                                        else
                                        {
                                            alert('Failed: ' + responseObj.error);
                                        }
                                    });
                                }
                            });
                        }
                        else
                        {
                            bootbox.alert('Please select items first!');
                            return;
                        }

                    });

                    $("#autoplan").click( function()
                    {
                        bootbox.alert('Coming soon!');
                    });




                    $("#assignToCaptainAllByDistrict").click( function()
                            {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                                var rows_selected = table_planningbydistrict.column(0).checkboxes.selected();
                                var responseObj;
                                console.log(rows_selected.join(','));

                                if (rows_selected.count())
                                {
                                    var captainNumber = prompt("Please captain phone number:");
                                    if (captainNumber != null && captainNumber != "")
                                    {
                                        $.ajax({
                                            type: 'POST',
                                            url: '/warehouse/getcaptaininfo',
                                            dataType: 'text',
                                            data: {phone:captainNumber},
                                        }).done(function (response) {
                                            //console.log(response);
                                            if (response==0)
                                            {
                                                bootbox.alert('No captain is registred with entered number');
                                                return;
                                            }
                                            else
                                            {
                                                responseObj = JSON.parse(response);

                                                if (confirm("Are you sure you want to assign selcted districts to captain: " +todatsdate + " " + responseObj.first_name + " "  + responseObj.last_name) == true)
                                                {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '/warehouse/setplannedcaptainbydistrict',
                                                        dataType: 'text',
                                                        data: {captainid:responseObj.id,  districts:rows_selected.join(','), shipmentdate:todatsdate},
                                                    }).done(function (response) {
                                                        bootbox.alert(rows_selected.count() + ' districts have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                                                        console.log(response);
                                                        if (response==1)
                                                        {
                                                            location.reload();
                                                        }
                                                    }); // Ajax Call

                                                }
                                            }
                                        }); // Ajax Call

                                    }
                                }
                                else
                                {
                                    bootbox.alert('Please select items first!');
                                    return;
                                }


                            }
                    );



                    $("#assignGroupIDAllByDistrict").click( function()
                            {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                                var rows_selected = table_planningbydistrict.column(0).checkboxes.selected();
                                var responseObj;
                                console.log(rows_selected.join(','));

                                if (rows_selected.count())
                                {
                                    var groupID = prompt("Please enter group ID:");
                                    if (groupID != null && groupID != "")
                                    {
                                        if (confirm("Are you sure you want to assign selcted items to group ID: " +groupID) == true)
                                        {
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/setplannedgroupbydistrict',
                                                dataType: 'text',
                                                data: {groupid:groupID,  districts:rows_selected.join(','), shipmentdate:todatsdate},
                                            }).done(function (response) {


                                                responseObj = JSON.parse(response);
                                                if (responseObj.success)
                                                {
                                                    alert(responseObj.count + ' district have been assigned to group ' +responseObj.group_id  );
                                                    location.reload();
                                                }
                                                else
                                                {
                                                    alert(responseObj.error );
                                                }
                                            }); // Ajax Call


                                        }


                                    }
                                    else
                                    {
                                        alert('Please enter a group ID');
                                        return;
                                    }


                                }
                                else
                                {
                                    alert('Please select items first!');
                                    return;
                                }


                            }
                    );

            $("#autoassigngroupid").click( function()
                {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                    var scheduled_date = document.getElementById('scheduled_date').value;
                    var city = document.getElementById('city').value;
                    var company = document.getElementById('company').value;
                    var responseObj;
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/autoassigngroupid',
                        dataType: 'text',
                        data: { city:city,company:company, scheduled_date:scheduled_date},
                    }).done(function (response) {


                        responseObj = JSON.parse(response);
                        if (responseObj.success)
                        {
                            alert(responseObj.count + ' district have been assigned to groups'  );
                            location.reload();
                        }
                        else
                        {
                            alert(responseObj.error );
                        }
                    }); // Ajax Call


                }
            );
                    $("#assignToCaptainAll").click( function()
                            {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                                var rows_selected = table_planningALL.column(0).checkboxes.selected();
                                var responseObj;
                                if (rows_selected.count())
                                {
                                    var captainNumber = prompt("Please captain phone number:");
                                    if (captainNumber != null && captainNumber != "")
                                    {
                                        $.ajax({
                                            type: 'POST',
                                            url: '/warehouse/getcaptaininfo',
                                            dataType: 'text',
                                            data: {phone:captainNumber},
                                        }).done(function (response) {
                                            console.log(response);
                                            if (response==0)
                                            {
                                                bootbox.alert('No captain is registred with entered number');
                                                return;
                                            }
                                            else
                                            {
                                                responseObj = JSON.parse(response);

                                                if (confirm("Are you sure you want to assign selcted items to captain: " +responseObj.first_name + " "  + responseObj.last_name) == true)
                                                {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '/warehouse/setplannedcaptain',
                                                        dataType: 'text',
                                                        data: {captainid:responseObj.id,  ids:rows_selected.join(',')},
                                                    }).done(function (response) {
                                                        var responseObj2 = JSON.parse(response);
                                                        console.log(response);
                                                        if (responseObj2.success==true)
                                                        {
                                                            alert(responseObj2.count + ' items have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                                                            location.reload();
                                                        }
                                                        else
                                                        {
                                                            alert('error:' + responseObj2.error);
                                                        }
                                                    }); // Ajax Call

                                                }
                                            }
                                        }); // Ajax Call

                                    }
                                }
                                else
                                {
                                    bootbox.alert('Please select items first!');
                                    return;
                                }


                            }
                    );

                    /*  $("#assignToCaptain").click( function()
                     {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                     var rows_selected = table_planning.column(0).checkboxes.selected();
                     var responseObj;
                     if (rows_selected.count())
                     {
                     var captainNumber = prompt("Please captain phone number:");
                     if (captainNumber != null && captainNumber != "")
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/getcaptaininfo',
                     dataType: 'text',
                     data: {phone:captainNumber},
                     }).done(function (response) {
                     console.log(response);
                     if (response==0)
                     {
                     bootbox.alert('No captain is registred with entered number');
                     return;
                     }
                     else
                     {
                     responseObj = JSON.parse(response);

                     if (confirm("Are you sure you want to assign selcted items to captain: " +responseObj.first_name + " "  + responseObj.last_name) == true)
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/setplannedcaptain',
                     dataType: 'text',
                     data: {captainid:responseObj.id,  ids:rows_selected.join(',')},
                     }).done(function (response) {
                     var responseObj2 = JSON.parse(response);
                     console.log(response);
                     if (responseObj2.success==true)
                     {
                     alert(responseObj2.count + ' items have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                     location.reload();
                     }
                     else
                     {
                     alert('error:' + responseObj2.error);
                     }
                     }); // Ajax Call

                     }
                     }
                     }); // Ajax Call

                     }
                     }
                     else
                     {
                     bootbox.alert('Please select items first!');
                     return;
                     }


                     }
                     ); */


                    /*$("#assignToCaptainOld").click( function()
                     {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                     var rows_selected = table_oldUndelivered.column(0).checkboxes.selected();
                     var responseObj;
                     if (rows_selected.count())
                     {
                     var captainNumber = prompt("Please captain phone number:");
                     if (captainNumber != null && captainNumber != "")
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/getcaptaininfo',
                     dataType: 'text',
                     data: {phone:captainNumber},
                     }).done(function (response) {
                     console.log(response);
                     if (response==0)
                     {
                     bootbox.alert('No captain is registred with entered number');
                     return;
                     }
                     else
                     {
                     responseObj = JSON.parse(response);

                     if (confirm("Are you sure you want to assign selcted items to captain: " +responseObj.first_name + " "  + responseObj.last_name) == true)
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/setplannedcaptain',
                     dataType: 'text',
                     data: {captainid:responseObj.id,  ids:rows_selected.join(',')},
                     }).done(function (response) {
                     var responseObj2 = JSON.parse(response);
                     console.log(response);
                     if (responseObj2.success==true)
                     {
                     alert(responseObj2.count + ' items have been assigned to captain ' +responseObj.first_name + " "  + responseObj.last_name  );
                     location.reload();
                     }
                     else
                     {
                     alert('error:' + responseObj2.error);
                     }
                     }); // Ajax Call

                     }
                     }
                     }); // Ajax Call

                     }
                     }
                     else
                     {
                     bootbox.alert('Please select items first!');
                     return;
                     }


                     }
                     );*/



                    ///
                    $("#groupIDall").click( function()
                            {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                                var rows_selected = table_planningALL.column(0).checkboxes.selected();
                                var responseObj;
                                if (rows_selected.count())
                                {
                                    var groupID = prompt("Please enter group ID:");
                                    if (groupID != null && groupID != "")
                                    {
                                        if (confirm("Are you sure you want to assign selcted items to group ID: " +groupID) == true)
                                        {
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/setplannedgroup',
                                                dataType: 'text',
                                                data: {groupid:groupID,  ids:rows_selected.join(',')},
                                            }).done(function (response) {

                                                responseObj = JSON.parse(response);
                                                if (responseObj.success)
                                                {
                                                    //bootbox.alert(  );
                                                    var str = responseObj.count + ' items have been assigned to group ' +responseObj.group_id;
                                                    bootbox.alert({
                                                        size: "small",
                                                        title: "Kasper",
                                                        message: str ,
                                                        callback:  location.reload()
                                                    });


                                                }
                                                else
                                                {
                                                    bootbox.alert(responseObj.error );
                                                }


                                            }); // Ajax Call

                                        }

                                    }


                                }
                                else
                                {
                                    bootbox.alert('Please select items first!');
                                    return;
                                }


                            }
                    );

                    /* $("#groupIDnew").click( function()
                     {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                     var rows_selected = table_planning.column(0).checkboxes.selected();
                     var responseObj;
                     if (rows_selected.count())
                     {
                     var groupID = prompt("Please enter group ID:");
                     if (groupID != null && groupID != "")
                     {
                     if (confirm("Are you sure you want to assign selcted items to group ID: " +groupID) == true)
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/setplannedgroup',
                     dataType: 'text',
                     data: {groupid:groupID,  ids:rows_selected.join(',')},
                     }).done(function (response) {

                     responseObj = JSON.parse(response);
                     if (responseObj.success)
                     {
                     bootbox.alert(responseObj.count + ' items have been assigned to group ' +responseObj.group_id  );
                     }
                     else
                     {
                     bootbox.alert(responseObj.error );
                     }

                     location.reload();
                     }); // Ajax Call

                     }

                     }

                     }
                     else
                     {
                     bootbox.alert('Please select items first!');
                     return;
                     }


                     }
                     ); */


                    /* $("#groupIDold").click( function()
                     {//https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
                     var rows_selected = table_oldUndelivered.column(0).checkboxes.selected();
                     var responseObj;
                     if (rows_selected.count())
                     {
                     var groupID = prompt("Please enter group ID:");
                     if (groupID != null && groupID != "")
                     {
                     if (confirm("Are you sure you want to assign selcted items to group ID: " +groupID) == true)
                     {
                     $.ajax({
                     type: 'POST',
                     url: '/warehouse/setplannedgroup',
                     dataType: 'text',
                     data: {groupid:groupID,  ids:rows_selected.join(',')},
                     }).done(function (response) {

                     responseObj = JSON.parse(response);
                     if (responseObj.success)
                     {
                     bootbox.alert(responseObj.count + ' items have been assigned to group ' +responseObj.group_id  );
                     }
                     else
                     {
                     bootbox.alert(responseObj.error );
                     }

                     location.reload();
                     }); // Ajax Call

                     }

                     }

                     }
                     else
                     {
                     bootbox.alert('Please select items first!');
                     return;
                     }


                     }
                     );*/


                });

function opensticker(jollychic) {
            window.open('/warehouse/sticker/' + jollychic, 'newwindow', 'width=504px,height=597px,scrollbars=no');
            return false;
        }

    </script>

    <script>
        $("#districtButton").click(function(){
            if(confirm('This update may take a few minutes. Are you sure you want to update?') == true) {
                document.getElementById('district_spinner').style.display = '';
                document.getElementById('district_correct').style.display = 'none';
                document.getElementById('district_times').style.display = 'none';
                var company = document.getElementById('_company').value;
                var city = document.getElementById('_city').value;
                var from = document.getElementById('_from').value;
                var to = document.getElementById('_to').value;
                var scheduled_shipment_date = document.getElementById('_scheduled_shipment_date').value;
                var status = '(' + document.getElementById('_status').value + ')';
                $.ajax({
                    type: 'GET',
                    url: '/deliveryrequest/updateDistricts',
                    dataType: 'text',
                    data: {
                        company: company,
                        city: city,
                        from: from,
                        to: to,
                        scheduled_shipment_date: scheduled_shipment_date,
                        status: status,
                        limit: 200,
                        update: 1
                    }
                }).done(function (response) {
                    document.getElementById('district_spinner').style.display = 'none';
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if(responseObj.success) {
                        document.getElementById('district_correct').style.display = '';
                    }
                    else {
                        document.getElementById('district_times').style.display = '';
                    }
                });
            }
        });
    </script>

    @stop

