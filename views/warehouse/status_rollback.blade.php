@if( Session::get('admin_id') != 32 && Session::get('admin_id') != 34 && Session::get('admin_id') != 51 )

    <script>window.location = "/warehouse/403";</script>

@endif

@extends('warehouse.layout')

@section('content')
    
    <style>
        .right {
            text-align: right;
        }
        .left {
            text-align: left;
        }
        .center {
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="center">
                        Status Rollback &nbsp;
                        <span class="fa fa-spinner fa-spin" id="spinner" style="font-size:24px; display: none;"></span> 
                        <i class="fa fa-check" id="correct" style="color: green; display: none;" aria-hidden="true"></i>
                        <i class="fa fa-times" id="times" style="color: red; display: none;"></i>
                    </h3> 
                    <div class="x_panel">
                        <div style="margin: auto;">
                            <div class="col-md-2 right">
                                <h4>Rollback From Status </h4>
                            </div>
                            <div class="col-md-2"> 
                                <select name="from" id="from" class="form-control col-md-2">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->id }}" {{ $status->id == 6 ? 'selected' : '' }}>{{ $status->english }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-1 center">
                                <h4>To Status</h4>
                            </div>
                            <div class="col-md-2">
                                <select name="to" id="to" class="form-control"> 
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->id }}" {{ $status->id == 4 ? 'selected' : '' }}>{{ $status->english }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-2 center">
                                <h4>For Shipments IDs</h4>
                            </div>
                            <div class="col-md-2">
                                <textarea name="shipmentIDs" id="shipmentIDs" class="form-control" rows="2"></textarea>
                            </div>
                            <div class="col-md-1">
                                <input type="submit" id="submit" value="Submit" class="btn btn-success" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-md-offset-3" style="display: inline-block;">
                        <div id="success_container" class="col-md-4 col-xs-12" style="display: none;">
                            <h3 style="color: green;">Success Items</h3>
                            <div id="success_items" class="col-md-12 col-xs-12"></div>
                        </div>
                        <div id="faild_container" class="col-md-6 col-xs-12" style="display: none;">
                            <h3 style="color: red;">Faild Items</h3>
                            <div id="faild_items" class="col-md-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $("#submit").click(function() {
            var from = document.getElementById('from').value;
            var to = document.getElementById('to').value;
            var shipIDs= document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
            if (shipIDs=='') {
                bootbox.alert('Please enter IDs!');
                return;
            }
            var ids = shipIDs.split(',');
            for(i = 0; i < ids.length;++i){
                ids[i] = ids[i].toUpperCase();
            }
            var wrongIDs = Array();
            var correctIDs = Array();
            var correctedIDs = Array();
            var finalArray = [];
            for (var i = 0; i <ids.length; i++ ) {
                if(!ids[i].replace(/\s/g, '').length)
                    continue;
                if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) ) {
                    correctIDs.push(ids[i]);
                }
                else {
                    wrongIDs.push(ids[i]);
                }
            }
            $.each(correctIDs, function(i, el) {
                if($.inArray(el, finalArray) === -1) {
                    finalArray.push(el);
                }
            });
            if (wrongIDs.length) {
                bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
            }
            if (finalArray.length) {
                if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                    document.getElementById('spinner').style.display = '';
                    document.getElementById('correct').style.display = 'none';
                    document.getElementById('times').style.display = 'none';
                    document.getElementById('success_container').style.display = 'none';
                    document.getElementById('faild_container').style.display = 'none';
                    document.getElementById('success_items').innerHTML = '';
                    document.getElementById('faild_items').innerHTML = '';
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/statusrollbackpost',
                        dataType: 'text',
                        data: {from: from, to: to, ids:finalArray.join(",")},
                    }).done(function (response) {
                        document.getElementById('spinner').style.display = 'none';
                        console.log(response);
                        responseObj = JSON.parse(response);
                        if (responseObj.success) {
                            document.getElementById('correct').style.display = '';
                            var success = responseObj.success_items;
                            var faild = responseObj.faild_items;
                            if(success.length > 0) {
                                document.getElementById('success_container').style.display = '';
                                responseObj.success_items.forEach(function(id){
                                    var cur = document.getElementById('success_items').innerHTML;
                                    document.getElementById('success_items').innerHTML = cur + '<h4>' + id + '</h4>';
                                });
                            }
                            if(faild.length > 0) {
                                document.getElementById('faild_container').style.display = '';
                                responseObj.faild_items.forEach(function(res){
                                    var cur = document.getElementById('faild_items').innerHTML;
                                    document.getElementById('faild_items').innerHTML = cur + '<h4>' + res['id'] + ' - ' + res['reason'] + '</h4>'; 
                                });
                            }
                        }
                        else {
                            document.getElementById('times').style.display = '';
                            alert('Something went wrong!');
                        }
                    });
                }
                else {
                    alert('You canceled!');
                }
            } 
            else  {
                alert('Nothing to update!');
            }    
                    
        });
    </script>

@stop

