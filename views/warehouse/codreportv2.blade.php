@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends('warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captain Account Report</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="get" action="/warehouse/CaptainAccountReport">


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Date <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="historydate" name="historydate"
                                       value="{{$historydate}}" class="date-picker form-control col-md-7 col-xs-12"
                                       required="required" type="date">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">

                                <table id="datatable_cod" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>City</th>
                                        <th>Captain</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $balance = 0;
                                    $total = 0;
                                    foreach ($items as $item) {
                                    $balance += $item->debit;
                                    $total += $item->credit;
                                    ?>
                                    <tr>
                                        <td>{{$historydate}}</td>
                                        <td><?= $item['city'] ?></td>
                                        <td><?php echo $item->first_name . ' ' . $item->last_name . ' 0' . substr(str_replace(' ', '', $item->phone), -9);?></td>
                                        <td><?= $item['debit'] ?></td>
                                        <td><?= $item['credit'] ?></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>{{$historydate}}</td>
                                        <td colspan="2">Admin</td>
                                        <td id="received">Total : {{$total}}</td>
                                        <td id="balance">Balance : {{$balance}}</td>
                                    </tr>
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>Date</th>
                                        <th>City</th>
                                        <th>Captain</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- /page content -->

    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

@stop