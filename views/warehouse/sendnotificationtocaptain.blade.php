
@extends('warehouse.layout')
@section('content')

    <div class="right_col"  role="main">
        <div class="box-header">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- form start -->
        <div>
            <h4><?if(isset($message)){echo $message;} ?></h4>
        </div>
        <form method="post" id="basic" action="/warehouse/SendNotificationToCaptainPost">
            <div class="form-group col-md-12 col-sm-12">
                <label>Captain ID*</label>
                <input type="text" class="form-control" name="captain_id" placeholder="Type Captain id" id="captain_id" value="" required>
            </div>
            <div class="form-group col-md-12 col-sm-12">
                <label>Message</label>
                <textarea maxlength="250" id="message" name="message" required="required" class="form-control" type="text"
                          rows="6"></textarea>
            </div>
            <div class="box-footer">
                <button type="submit" id="add" class="btn btn-primary btn-flat btn-block">Send Message to Captain</button>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $("#message").validate({
            rules: {
                name: "required",
            }
        });
    </script>
    <!-- /page content -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>
@stop