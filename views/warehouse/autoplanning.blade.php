@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Auto Planning</h3>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form id="autoplanningform" method="get" action="/warehouse/autoplanning">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" disabled required="required"
                                                class="form-control"
                                                type="text">
                                            <option value="all">All</option>
                                            @foreach($companies as $compan)
                                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Scheduled Shipment Date
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="scheduled_date" id="scheduled_date" class="form-control">
                                            <?php
                                            foreach ($scheduled_Dates as $scheduled_Date) {
                                                if ($scheduled_Date->scheduled_shipment_date != null) {
                                                    $selected = '';
                                                    $date = $scheduled_Date->scheduled_shipment_date;
                                                    if (isset($shipmentDate) && $shipmentDate == $date)
                                                        $selected = 'selected';
                                                    echo "<option value='$date' $selected>$date</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nationality Weight
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="nationality_weight" id="nationality_weight" type="text"
                                               class="form-control" value="0.2"></input>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Success Ratio Weight
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="success_weight" id="success_weight" type="text"
                                               class="form-control" value="0.3"></input>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Min Distance Weight
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="distance_weight" id="distance_weight" type="text"
                                               class="form-control" value="0.3"></input>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Max Distance
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="max_distance" id="max_distance" type="text"
                                               class="form-control" value="5"></input>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Frequency Weight
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="frequency_weight" id="frequency_weight" type="text"
                                               class="form-control" value="0.2">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">

                <!-- table start -->
                <div class="modal fade" id="myModal" role="dialog" style="overflow: auto">
                    <div class="modal-dialog modal-lg">{{--style="overflow-y: scroll;height: 500px;"--}}
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Manually Assign Shipments to Captain</h4>
                            </div>
                            <div class="modal-body">
                                <table id="firsttable" class="table table-striped table-bordered">
                                </table>
                                <table id="secondtable" class="table table-striped table-bordered">
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- !table start -->
                <!-- table start -->
                <label id="tab1"
                       style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;;background: #fff;box-shadow: inset 0 3px #2a3f54;"
                       onclick=availablecaptains()><span>Active</span></label>
                <label id="tab2"
                       style="font-size: 18px; display: block; float: left; padding: 1.5em; color: #757575;background: #f0f0f0;"
                       onclick=allcaptains()><span>Remaining </span></label>

                <section id="content1" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                Available Captains
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->


                            <table id="datatable_activecaptains"
                                   class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Captain ID</th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>Mobile</th>
                                    <th>Max Shipments</th>
                                    <th>Success Ratio</th>
                                    <th>Nationality</th>
                                    <th>Weight</th>
                                    <th>Plan Shipments</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Captain ID</th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>Mobile</th>
                                    <th>Max Shipments</th>
                                    <th>Success Ratio</th>
                                    <th>Nationality</th>
                                    <th>Weight</th>
                                    <th>Plan Shipments</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table end -->

                </section>
                <section id="content2" class="tab-content">
                    <div class="row">
                        <!-- table start -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>
                                        Remaining Captains
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_allcaptains" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Captain ID</th>
                                            <th>Name</th>
                                            <th>City</th>
                                            <th>Mobile</th>
                                            <th>Max Shipments</th>
                                            <th>Success Ratio</th>
                                            <th>Nationality</th>
                                            <th>Weight</th>
                                            <th>Plan Shipments</th>
                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Captain ID</th>
                                            <th>Name</th>
                                            <th>City</th>
                                            <th>Mobile</th>
                                            <th>Max Shipments</th>
                                            <th>Success Ratio</th>
                                            <th>Nationality</th>
                                            <th>Weight</th>
                                            <th>Plan Shipments</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- !table start -->
                    </div>
                </section>
                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Shipment
                                <small>Planning</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12">Start Pickup Time<span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <input id="start_pickup_time" name="start_pickup_time"
                                           value="13:00" class="time-picker form-control col-md-7 col-xs-12"
                                           type="time">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12">End Pickup Time<span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <input id="end_pickup_time" name="end_pickup_time"
                                           value="16:00" class="time-picker form-control col-md-7 col-xs-12"
                                           type="time">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <p id=autoplan>
                                    <button class="btn btn-success">AUTO PLAN!!</button>
                                </p>
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <p id=rollback>
                                    <button class="btn btn-success">Rollback!!</button>
                                </p>
                            </div>

                            <table id="datatable_planningALL" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Group ID</th>
                                    <th>Plan Shipments</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>WayBill</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>COD</th>
                                    <th>Group ID</th>
                                    <th>Plan Shipments</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Auto Planing Result</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table id="autoplanningresult" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Captain Id</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Weight</th>
                                    <th>Min/Max Shipments</th>
                                    <th>Suggested Pickup Time</th>
                                    <th>Total Planned Shipments</th>
                                    <th>Waybills of Planned Shipments</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Captain Id</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Weight</th>
                                    <th>Min/Max Shipments</th>
                                    <th>Suggested Pickup Time</th>
                                    <th>Total Planned Shipments</th>
                                    <th>Waybills of Planned Shipments</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->
            </div>

        </div>
    </div>
    </div>
    <!-- /page content -->


    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script>

        <?php

        $mydata_activecaptainsdata = '';
        $mydata_allShipments = '';
        $mydata_allcaptainsdata = '';
				 foreach ($allcaptains as $walker) {
            $captain_name = $walker->first_name . ' ' . $walker->last_name;
            $mydata_allcaptainsdata .= "[  \"$walker->id\" ,\"$walker->id\" ,\"$captain_name\",\"$walker->city\",\"$walker->phone\",\"$walker->max_shipments\",\"$walker->success_ratio\",\"$walker->nationality\",\"$walker->weight\",
            \"<a onclick=unplanshipmentsforcaptain(\'$walker->id\')><button>Plan </button></a>\"], ";
        }
        foreach ($activecaptains as $walker) {
            $captain_name = $walker->first_name . ' ' . $walker->last_name;
            $mydata_activecaptainsdata .= "[  \"$walker->id\" ,\"$walker->id\" ,\"$captain_name\",\"$walker->city\",\"$walker->phone\",\"$walker->max_shipments\",\"$walker->success_ratio\",\"$walker->nationality\",\"$walker->weight\",
            \"<a onclick=unplanshipmentsforcaptain(\'$walker->id\')><button>Plan </button></a>\"], ";
        }
        foreach ($allshipments as $order) {

            $mydata_allShipments .= "[ \"$order->jollychic\",\"$order->jollychic\",\"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\",
            \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->group_id\",\"<a onclick=captainsforunplanshipment(\'$order->jollychic\')><button>Plan </button></a>\"], ";
        }

        ?>



        $(document).ready(function () {

            var table_activecaptains = $("#datatable_activecaptains").DataTable({

                "data": [
                    <?php echo $mydata_activecaptainsdata ?>

                ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                'order': [[1, 'asc']]
            });


            var table_planningALL = $("#datatable_planningALL").DataTable({

                "data": [
                    <?php echo $mydata_allShipments ?>

                ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                'order': [[1, 'asc']]
            });

                    var datatable_allcaptains = $("#datatable_allcaptains").DataTable({

                        "data": [
                            <?php echo $mydata_allcaptainsdata ?>

                        ],
                        "autoWidth": false,
                        dom: "Blfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true,
                        'columnDefs': [
                            {
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }
                        ],
                        'select': {
                            'style': 'multi'
                        },
                        'order': [[1, 'asc']]
                    });
            $("#autoplan").click(function () {
                var rows_selected = table_planningALL.column(0).checkboxes.selected();
                var rows_selected1 = table_activecaptains.column(0).checkboxes.selected();
                var rows_selected2 = datatable_allcaptains.column(0).checkboxes.selected();
                var responseObj;
                if (rows_selected1.count() || rows_selected2.count()) {
                    if (rows_selected.count()) {
                        if (rows_selected1.count() && rows_selected2.count()) {
                            var captains_ids = rows_selected1.join(",") + ',' + rows_selected2.join(",");
                        } else if (rows_selected1.count()) {
                            var captains_ids = rows_selected1.join(",")
                        } else {
                            var captains_ids = rows_selected2.join(",")
                        }
                        var city = document.getElementById('city').value;
                        var company = document.getElementById('company').value;
                        var start_pickup_time = document.getElementById('start_pickup_time').value;
                        var end_pickup_time = document.getElementById('end_pickup_time').value;
                        var scheduled_shipment_date = document.getElementById('scheduled_date').value;
                        var max_distance = document.getElementById('max_distance').value;
                        var distance_weight = document.getElementById('distance_weight').value;
                        var responseObj;
                        if (confirm("Are you sure you want Auto Plan for selected items" ) == false){
                            return;
                        }
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/autoPlanningAssign',
                            dataType: 'text',
                            data: {
                                city: city,
                                company: company,
                                start_pickup_time: start_pickup_time,
                                end_pickup_time: end_pickup_time,
                                scheduled_shipment_date: scheduled_shipment_date,
                                max_distance: max_distance,
                                distance_weight:distance_weight,
                                ids: rows_selected.join(','),
                                captains:captains_ids
                            },
                        }).done(function (response) {
                            responseObj = JSON.parse(response);
                            if (responseObj.success) {
                                var captains_planned = responseObj.captains_planned;
                                var shipments_planned = responseObj.shipments_planned;
                                $('#autoplanningresult tr').has('td').remove();
                                var table = document.getElementById("autoplanningresult");
                                for (var i = 0; i < captains_planned.length; i += 1) {
                                    var row = table.insertRow(1);
                                    var cell1 = row.insertCell(0);
                                    var cell2 = row.insertCell(1);
                                    var cell3 = row.insertCell(2);
                                    var cell4 = row.insertCell(3);
                                    var cell5 = row.insertCell(4);
                                    var cell6 = row.insertCell(5);
                                    var cell7 = row.insertCell(6);
                                    var cell8 = row.insertCell(7);
                                    var cap_id = captains_planned[i];
                                    cell1.innerHTML = cap_id;
                                    cell2.innerHTML = shipments_planned[cap_id].name;
                                    cell3.innerHTML = shipments_planned[cap_id].phone;
                                    cell4.innerHTML = shipments_planned[cap_id].weight;
                                    cell5.innerHTML = shipments_planned[cap_id].min_shipments+'/'+shipments_planned[cap_id].max_shipments;
                                    cell6.innerHTML = shipments_planned[cap_id].suggested_pickup_time;
                                    cell7.innerHTML = shipments_planned[cap_id].count_planned_shipments;
                                    cell8.innerHTML = shipments_planned[cap_id].planned_shipments.join('\n');
                                }
                                var message = '';
                                if (responseObj.count_planned > 0)
                                    message = responseObj.count_planned + '  items have been planned successfully. ';
                                if (responseObj.count_not_planned > 0)
                                    message += 'No suitable captain found for ' + responseObj.count_not_planned + ' items having waybill :  ' + responseObj.shipments_not_planned.join(',');
                                alert(message);
                            } else {
                                alert(responseObj.error);
                            }
                        }); // Ajax Call
                    }
                    else {
                        bootbox.alert('Please select items first!');
                        return;
                    }
                } else {
                    bootbox.alert('Please select Captains first!');
                    return;
                }
                ;
            });
                    document.getElementById("content1").style.display = "block";
                    document.getElementById("content2").style.display = "none";

        });
        $("#rollback").click(function () {
            var responseObj;
            var city = document.getElementById('city').value;
            var scheduled_shipment_date = document.getElementById('scheduled_date').value;
            var responseObj;
            if (confirm("Are you sure you want Roll Back Auto Planing for selected city and scheduled date" ) == false) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/warehouse/autoplanningrollback',
                dataType: 'text',
                data: {
                    city: city,
                    scheduled_shipment_date: scheduled_shipment_date
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    var shipments_rollbacked = responseObj.shipments_rollbacked;
                    $('#autoplanningresult tr').has('td').remove();
                    var message = '';
                    if (responseObj.shipments_rollbacked > 0)
                        message = responseObj.shipments_rollbacked + '  items have been planned successfully. ';
                    alert(message);
                } else {
                    alert(responseObj.error);
                }
            }); // Ajax Call
        });

        function unplanshipmentsforcaptain(id) {
            var city = document.getElementById('city').value;
            var scheduled_shipment_date = document.getElementById('scheduled_date').value;
            var distance_weight = document.getElementById('distance_weight').value;
            var captain_id = id;

            function unplanshipmentsforcaptain(callback) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/unplanshipmentsforcaptain',
                    dataType: 'text',
                    cache: false,
                    success: callback,
                    data: {
                        city: city,
                        scheduled_shipment_date: scheduled_shipment_date,
                        distance_weight: distance_weight,
                        captain_id: captain_id
                    }
                })
            }

            unplanshipmentsforcaptain(function (res) {
                var mydata = JSON.parse(res);
                if (mydata == 0) {
                    bootbox.alert('<h4>No Shipment Found Forwhich Planned Walker not planned Where City= ' + city + '</h4>');
                    return;
                } else {
                    var captaininforamation = '<caption><h4>Captain Information</h4></caption>' + '<tr>' + '<th>Captain_ID</th>' + '<th>Captain Name</th>' + '<th>Mobile</th>'
                            + '<th>success Ratio</th>' + '<th>Max Shipments</th>' + '<th>Min Shipments</th>' + '<th>Planned  For Captain</th>' + '</tr>';
                    var captain = mydata.captaininfo;
                    var plannedforcaptain = mydata.plannedforcaptain;
                    var captain_name = captain.first_name + " " + captain.last_name;
                    captaininforamation += '<tr><td>' + captain.id + '</td><td>' + captain_name + '</td><td>' + captain.phone + '</td><td>' + captain.success_ratio + '</td><td>' +
                            captain.max_shipments + '</td><td>' + captain.min_shipments + '</td><td>' + plannedforcaptain + '</tr>';
                    var unplannedshipments = '<caption><h4>Unplanned Shipments</h4></caption><tr>' + '<th>Waybill</th>' + '<th>City</th>' +
                            '<th>Receiver Name</th>' + '<th>District</th>' + '<th>COD</th>' + '<th>Distance</th>' + '<th>Assign</th>' + '</tr>';
                    $.each(mydata.un_planned_shipments, function (i, item) {

                        unplannedshipments += '<tr><td>' + item.waybill + '</td><td>' + item.d_city + '</td><td>' + item.receiver_name + '</td><td>' +
                                item.d_district + '</td><td>' + item.cod + '</td><td>' + item.distance + '</td>' +
                                '<td><button class="btn btn-success" onclick="assignshipment(this);">Assign</button></td></tr>';

                    });
                    $('#firsttable').html(captaininforamation);
                    $('#secondtable').html(unplannedshipments);
                }
                $('#myModal').modal("show");
            });

        }
        function assignshipment(eve) {
            var waybill = eve.parentNode.parentNode.childNodes[0].innerHTML;
            var captain_id = document.getElementById("firsttable").rows[1].childNodes[0].innerHTML;
            var captain_name = document.getElementById("firsttable").rows[1].childNodes[1].innerHTML;
                    if (confirm("Are you sure you want to assign  Shipment " + waybill + "  to captain:  " + captain_name ) == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/assignshipmenttocaptain',
                            dataType: 'text',
                            data: {captainid: captain_id, jollychic: waybill},
                        }).done(function (response) {
                            if (response == 0) {
                                bootbox.alert('<h2>This Shipment already have been Assign to Captain</h2>');
                                return;
                            } else {
                                responseObj = JSON.parse(response);
                                bootbox.alert(responseObj.msg);
                            }

                        }); // Ajax Call

                    } else {
                        bootbox.alert('You cancled!');
                    }
        }

        function captainsforunplanshipment(jollychic) {
            var city = document.getElementById('city').value;
            var scheduled_shipment_date = document.getElementById('scheduled_date').value;
            var distance_weight = document.getElementById('distance_weight').value;
            var jollychic = jollychic;

            function captainsforunplanshipment(callback) {
                $.ajax({
                    type: 'POST',
                    url: '/warehouse/captainsforunplanshipment',
                    dataType: 'text',
                    cache: false,
                    success: callback,
                    data: {
                        city: city,
                        scheduled_shipment_date: scheduled_shipment_date,
                        distance_weight: distance_weight,
                        jollychic: jollychic
                    }
                })
            }

            captainsforunplanshipment(function (res) {
                var mydata = JSON.parse(res);
                if (mydata == 0) {
                    bootbox.alert('<h4>No Captain Found Which is available Where City= ' + city + '</h4>');
                    return;
                } else if (mydata == 1) {
                    bootbox.alert('<h4>This Shipment already have been Assign to Captain</h4>');
                    return;
                }
                else {
                    var shipmentinforamation = '<caption><h4>Shipment information</h4></caption>' + '<tr>' + '<th>Waybill</th>' + '<th>City</th>' + '<th>Receiver Name</th>'
                            + '<th>Mobile 1</th>' + '<th>Street Address</th>' + '<th>District</th>' + '<th>COD</th>' + '</tr>';
                    var shipment = mydata.shipmentinfo;
                    shipmentinforamation += '<tr><td>' + shipment.jollychic + '</td><td>' + shipment.d_city + '</td><td>' + shipment.receiver_name + '</td><td>' + shipment.receiver_phone + '</td><td>' +
                            shipment.d_address + '</td><td>' + shipment.d_district + '</td><td>' + shipment.cash_on_delivery + '</td></tr>';
                    var captainsforunplannedshipment = '<caption><h4>Available Captains</h4></caption><tr>' + '<th>Captain_ID</th>' + '<th>Name</th>' + '<th>CIty</th>' +
                            '<th>Mobile</th>' + '<th>Max Shipments</th>' + '<th>Planned For Captain</th>' + '<th>Success Ratio</th>' + '<th>Distance</th>' + '<th>Assign</th>' + '</tr>';
                    $.each(mydata.captainsforunplanshipment, function (i, item) {
                        //var undelivered_shipments = mydata.undelivered_shipments;
                        var captain_name = item.first_name + " " + item.last_name;
                        captainsforunplannedshipment += '<tr><td>' + item.captain_id + '</td><td>' + captain_name + '</td><td>' + item.captain_city + '</td><td>' + item.phone + '</td><td>' +
                                item.max_shipments + '</td><td>' + item.plannedforcaptain + '</td><td>' + item.success_ratio + '</td><td>' + item.distance + '</td>' +
                                '<td><button class="btn btn-success" onclick="assignshipmenttocaptain(this);">Assign</button></td></tr>';

                    });
                    $('#firsttable').html(shipmentinforamation);
                    $('#secondtable').html(captainsforunplannedshipment);
                }
                $('#myModal').modal("show");
            });

        }

        function assignshipmenttocaptain(eve) {
            var captain_id = eve.parentNode.parentNode.childNodes[0].innerHTML;
            var waybill = document.getElementById("firsttable").rows[1].childNodes[0].innerHTML;
            var captain_name = document.getElementById("firsttable").rows[1].childNodes[2].innerHTML;
                    if (confirm("Are you sure you want to assign  Shipment " + waybill + "  to captain:  " + captain_name ) == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/assignshipmenttocaptain',
                            dataType: 'text',
                            data: {captainid: captain_id, jollychic: waybill},
                        }).done(function (response) {
                            if (response == 0) {
                                bootbox.alert('<h2>This Shipment already have been Assign to Captain</h2>');
                                return;
                            } else {
                                responseObj = JSON.parse(response);
                                bootbox.alert(responseObj.msg);
                            }

                        }); // Ajax Call

                    } else {
                        bootbox.alert('You cancled!');
                    }
        }
        function availablecaptains() {
            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none";
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab2').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0', });
        }

        function allcaptains() {
            document.getElementById("content2").style.display = "block";
            document.getElementById("content1").style.display = "none";
            $('#tab2').css({'font-size': '18px','border-top':'5px solid #f7f7f7;','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#fff', 'box-shadow': 'inset 0 3px #2a3f54'});
            $('#tab1').css({'font-size': '18px','display': 'block', 'float': 'left', 'padding': '1.5em','color': '#757575', 'background': '#f0f0f0',});

        }

    </script>

@stop

