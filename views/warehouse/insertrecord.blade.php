@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <style>
        #map_canvas {
            height: 150px;
            width: 75%;
            margin: 0;
        }

        #map_canvas .centerMarker {
            position: absolute;
            /*url of the marker*/
            background: url(../web/images/marker.png) no-repeat;
            /*center the marker*/
            top: 50%;
            left: 50%;
            z-index: 1;
            /*fix offset when needed*/
            margin-left: -10px;
            margin-top: -34px;
            /*size of the image*/
            height: 34px;
            width: 20px;
            cursor: pointer;
        }
    </style>
    <!-- page content -->

    <div class="right_col" role="main">
        <div>
            <div class="row">

                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" class="form" id="main-form" method="post" action="/warehouse/generateshipmentpost"
                      enctype="multipart/form-data">
                    <input id="latitude" name="latitude" required="required" type="hidden">
                    <input id="longitude" name="longitude" required="required" type="hidden">

                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Customer Name<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="name" name="name" class="form-control col-md-7 col-xs-12" required="required"
                                   placeholder=" " type="text" maxlength="">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Waybill<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="waybill" name="waybill" class="form-control col-md-7 col-xs-12"
                                   placeholder="JC12345678KS" required="required" type="text" maxlength="12">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Mobile 1<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="mobile" name="mobile" class="form-control col-md-7 col-xs-12" required="required"
                                   placeholder="e.g. 966512345678" type="text" onkeypress="return isNumber(event)"
                                   maxlength="15">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Mobile 2*</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="mobile2" name="mobile2" class="form-control col-md-7 col-xs-12" type="text"
                                   placeholder="e.g. 966512345678" onkeypress="return isNumber(event)" maxlength="15">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Email</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="email" name="email" class="form-control col-md-7 col-xs-12" type="text"
                                   placeholder="" maxlength="200">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Shipment Description<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="3" cols="45" id="description" name="description"
                                      class="form-control col-md-7 col-xs-12" required="required"
                                      type="text">Kitchen Accessories
                                </textarea>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">City<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select name="city" id="city" class="form-control" onchange="onChangeCity()">
                                <option value="" selected="">Select City</option>
                            </select>

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">District<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select name="district" id="district" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Street Address</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="streetaddress2" name="streetaddress2"
                                   class="form-control col-md-7 col-xs-12" required="required"
                                   type="text">
                            </input>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Full Address<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="5" cols="45" id="streetaddress" name="streetaddress"
                                      class="form-control col-md-7 col-xs-12" required="required"
                                      type="text"></textarea>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Cash on Delivery<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="cashondelivery" name="cashondelivery" class="form-control col-md-7 col-xs-12"
                                   required="required" placeholder="" type="number" step="any"
                                   maxlength="20">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Order No.</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="order_number" name="order_number" class="form-control col-md-7 col-xs-12"
                                   type="text" placeholder="" maxlength="30">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Quantity<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="quantity" name="quantity" class="form-control col-md-7 col-xs-12"
                                   placeholder="e.g. 1" required="required" type="text"
                                   onkeypress="return isNumber(event)" maxlength="5">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Weight<span
                                    class="required">*</span></label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="weight" name="weight" class="form-control col-md-7 col-xs-12"
                                   placeholder="e.g. 10" required="required" type="text"
                                   maxlength="5">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Customer Map Location</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <div id="map_canvas"></div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
                        <button type="submit" class="btn btn-primary btn-flat btn-block">Generate Shipment Sticker
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-header {
            background-color: #fefefe;
            padding: 5px;
            border: 1px solid #888;
            width: 100%; /* Could be more or less, depending on screen size */
            height: 50px; /* Could be more or less, depending on screen size */
        }

        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 300px; /* Could be more or less, depending on screen size */
            height: 150px; /* Could be more or less, depending on screen size */
        }

        #sticker-content {
            margin: 8% auto;
            width: 10.16cm;

        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
    <div id="modal" class="modal">
        <!-- Modal content -->
        <div id="modal-content" class="modal-content">
            <div class='row'>
                <div class='col-md-6 col-sm-6 col-xs-6'>
                    <label>Your Way Bill is:</label>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-6'>
                    <label id="waybilllabel"></label>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p>
                        <button id="printsticker" class="btn btn-success">Print Waybill Sticker</button>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div id="stickermodal" class="modal">

        <!-- Modal content -->
        <div id="sticker-content">
            <div class="modal-header">
                <button id='stickerclose' type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sticker</h4>
            </div>
            <div id="stickercontent">

            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap"
            async defer></script>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function initMap() {
            try {
                var latitude = 21.2854;
                var longitude = 39.2376;
                var mapOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(latitude, longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById('map_canvas'),
                        mapOptions);
                google.maps.event.addListener(map, 'center_changed', function () {
                    document.getElementById('latitude').value = map.getCenter().lat();
                    document.getElementById('longitude').value = map.getCenter().lng();
                });
                $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                    //do something onclick
                        .click(function () {
                            var that = $(this);
                            if (!that.data('win')) {
                                that.data('win', new google.maps.InfoWindow({
                                    content: 'this is the center'
                                }));
                                that.data('win').bindTo('position', map, 'center');
                            }
                            that.data('win').open(map);
                        });
            }
            catch (e) {
                alert(e);
            }
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

    <script>
        // fill cities
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text'
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for (var i = 0; i < cities.length; ++i) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }
                }
            });
        });
    </script>

    <script>
        function onChangeCity() {
            $("#district").empty();
            var city = document.getElementById('city').value;
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    for (var i = 0; i < districts.length; i += 1) {
                        if (districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        option.appendChild(document.createTextNode(districts[i].district));
                        select.appendChild(option);
                    }
                }
            });
        }
        var modal = document.getElementById('modal');
        var stickermodal = document.getElementById('stickermodal');
        var stickercontent = document.getElementById('stickercontent');
        var waybilllabel = document.getElementById('waybilllabel');
        var printsticker = document.getElementById("printsticker");
        var stickerclose = document.getElementById("stickerclose");
        var sticker = '<?php if(isset($sticker)) echo $sticker; else false;?>';
        var message = '<?php if(isset($error)) echo $error; else false;?>';
        var waybill = '<?php if(isset($waybill)) echo $waybill; else false;?>';
        if (message) {
            alert(message);
        }
        var url = '';
        if (waybill) {
            waybilllabel.innerHTML = waybill;
            url = "/warehouse/sticker/" + waybill;
            modal.style.display = "block";
        }
        var doc = new jsPDF();
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };
        stickerclose.onclick = function () {
            window.open(url);
            stickermodal.style.display = "none";
        }
        printsticker.onclick = function () {
            modal.style.display = "none";
            stickercontent.innerHTML = sticker;
            stickermodal.style.display = "block";

        }

        function printElement(elem) {
            var domClone = elem.cloneNode(true);

            var stickercontent = document.getElementById("stickercontent");

            if (!stickercontent) {
                var stickercontent = document.createElement("div");
                stickercontent.id = "printSection";
                document.body.appendChild(stickercontent);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }
    </script>

@stop
