@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif


    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')


    <?php
    ?>

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3><?php echo $shipmentDate ?> Shipments
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- top tiles -->
            <div class="row tile_count">
                <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-truck"></i> Shipment Date</span>
                    <div class="count">< ?php echo $shipmentDate ?> </div>

                </div>
                -->
                <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cubes"></i> Total Packages</span>

                    <div class="count"><?php echo $allitems ?> </div>

                </div>

                <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-thumbs-down"></i>  In Warehouse </span>

                    <div class="count red"><?php echo $arriveditems ?></div>
                    <div><?php echo "Returned ". $returneditems; ?></div>
                <div><?php echo "New ". $newitems; ?></div>
                </div>

                <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-car"></i> With Captains</span>

                    <div class="count blue"><?php echo $outfordeliveryitems ?></div>
                </div>

                <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-thumbs-ub"></i> Delivered </span>

                    <div class="count green"><?php echo $delivreditems ?></div>
                </div>
            </div>
            <div class="row tile_count">

                <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Cash Collected </span>

                    <div class="count green"><?php echo number_format($cashcollected, 2) ?></div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Cash to be Collected </span>

                    <div class="count red"><?php echo number_format($cashtobecollected, 2) ?></div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Success Rate </span>

                    <div class="count red"><?php echo number_format($successrate, 2) ?>%</div>
                </div>
            </div>
            <!-- /top tiles -->
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="get" action="/warehouse/shipmentsV2/{{$shipmentDate}}">


                        <input id="shipmentDate" name="shipmentDate" class="form-control" type="hidden"
                               value="{{$shipmentDate}}">

                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select id="company" name="company" required="required" class="form-control"
                                        type="text">
                                    <option value="all">All Companies</option>
                                    @foreach($companies as $compan)
                                        <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="city" id="city" class="form-control">
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Status <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="status" id="status" class="form-control">
                                    <option value="all">All</option>
                                    <option value="0"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                        Created By JC
                                    </option>
                                    <option value="1"<?php if (isset($status) && $status == '1') echo 'selected';?> >In
                                        Route
                                    </option>
                                    <option value="2"<?php if (isset($status) && $status == '2') echo 'selected';?> >In
                                        Warehouse
                                    </option>
                                    <option value="4"<?php if (isset($status) && $status == '4') echo 'selected';?> >
                                        With Captain
                                    </option>
                                    <option value="5"<?php if (isset($status) && $status == '5') echo 'selected';?> >
                                        Delivered
                                    </option>
                                    <option value="6"<?php if (isset($status) && $status == '6') echo 'selected';?> >
                                        UnDelivered
                                    </option>
                                    <option value="7"<?php if (isset($status) && $status == '7') echo 'selected';?> >
                                        Returned to JC
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Start Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="start_date" name="start_date"
                                       class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">End Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Or Shipment Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="shipment_date" id="shipment_date" class="form-control">
                                    <option value="all">All</option>
                                    <?php

                                    $shipmentdates = DeliveryRequest::distinct()->where('jollychic', '!=', '0')->orderBy('scheduled_shipment_date', 'desc')->get(['scheduled_shipment_date']);

                                    $total_num_of_shipments = count($shipmentdates);

                                    foreach ($shipmentdates as $ashipmentdates) {
                                        if ($ashipmentdates->scheduled_shipment_date != null) {
                                            $selected = '';
                                            $date = $ashipmentdates->scheduled_shipment_date;
                                            if (isset($shipmentDate) && $shipmentDate == $date)
                                                $selected = 'selected';
                                            echo "<option value='$date' $selected>$date</option>";
                                        }
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="row">

                <div class="tab_container">


                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1" class='label2'><i class="fa fa-car"></i><span>All Statues Shipments</span></label>


                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2" class='label2'><i
                                class="fa fa-pencil-square-o"></i><span>Dispatched </span></label>

                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3" class='label2'><i class="fa fa-bar-chart-o"></i><span>Delivered </span></label>


                    <section id="content1" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>All Statues
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $orders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscount; }} off {{ $allitems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        جدول بجميع الحالات
                                    </p>

                                    <table id="datatable_summary" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>Mobile2</th>
                                            <th>Street Addrs</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Status</th>
                                            <th>Details</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($orders) > 0){ foreach ($orders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->receiver_phone ?></td>
                                            <td><?= $order->receiver_phone2 ?></td>
                                            <td><?= $order->d_address ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?= $order->pincode?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>

                                            <td><?php if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && $order->planned_walker == 0)
                                                    echo 'Warehouse/R';
                                                else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && $order->planned_walker != 0)
                                                    echo 'Warehouse/Res';
                                                else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                                                    echo 'Warehouse/New';
                                                else
                                                    echo $statusDef[$order->status];?></td>
                                            <td><a href='/warehouse/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="12">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>Mobile2</th>
                                            <th>Street Addrs</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Status</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $orders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscount; }} off {{ $allitems; }} </p>
                            </div>
                        </div>
                    </section>

                    <section id="content2" class="tab-content">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Dispatched | مع الكباتن
                                        <small>In delivery process</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $withcaptainsorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountwithcaptain; }} off {{ $outfordeliveryitems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        Items that are out for delivery
                                    </p>

                                    <table id="datatable_inWithCaptains" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($withcaptainsorders) > 0){ foreach ($withcaptainsorders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->receiver_phone ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?= $order->pincode ?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>
                                            <td><a href='/warehouse/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="9">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $withcaptainsorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountwithcaptain; }} off {{ $outfordeliveryitems; }} </p>
                            </div>
                        </div>
                    </section>

                    <section id="content3" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Delivered
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div align="left"
                                           id="paglink"><?php echo $deliveredorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountdelivered; }} off {{ $delivreditems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        Items that are delivered
                                    </p>


                                    <table id="datatable_delivered" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Captain</th>
                                            <th>Delivery Date</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($deliveredorders) > 0){ foreach ($deliveredorders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>
                                            <td><?= $order->dropoff_timestamp ?></td>
                                            <td><a href='/warehouse/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="8">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Captain</th>
                                            <th>Delivery Date</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $deliveredorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountdelivered; }} off {{ $delivreditems; }} </p>
                            </div>


                        </div>
                    </section>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
    <!-- /page content -->





    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    @stop
