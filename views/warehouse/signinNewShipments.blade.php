@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif


@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')

@section('content')

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Sign-In New Shipments</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sign-In new Incoming Shipments to our Warehouse</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                             <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20" ></textarea>
                        </div>
                    </div>
                    
                   
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" >
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <p><button id="submitForm"  class="btn btn-success">Sign in</button>  </p>
                        </div>
                    </div>
                    
                   
                    </div>
                  </div>
                </div>
                <div class="row" id="failed_items_section" style="display: none;">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                          <div class="x_title">
                              <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                              <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                  <li class="dropdown"></li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                              </ul>
                              <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                              <table id="datatable_faileditems" class="table table-bordered table-striped">
                                  <thead>
                                      <tr>
                                          <th>Waybill</th>
                                          <th>Reason</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                          <th>Waybill</th>
                                          <th>Reason</th>
                                      </tr>
                                  </tfoot>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row" id="success_items_section" style="display: none;">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                          <div class="x_title">
                              <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                              <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                  <li class="dropdown"></li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                              </ul>
                              <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                              <table id="datatable_successitems" class="table table-bordered table-striped">
                                  <thead>
                                      <tr>
                                          <th>Waybill</th>
                                          <th>Order Number</th>
                                          <th>Order Number 2</th>
                                          <th>Order Reference</th>
                                          <th>City</th>
                                          <th>Receiver Name</th>
                                          <th>Receiver Mobile</th>
                                          <th>Receiver Mobile 2</th>
                                          <th>Cash On Delivery</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                          <th>Waybill</th>
                                          <th>Order Number</th>
                                          <th>Order Number 2</th>
                                          <th>Order Reference</th>
                                          <th>City</th>
                                          <th>Receiver Name</th>
                                          <th>Receiver Mobile</th>
                                          <th>Receiver Mobile 2</th>
                                          <th>Cash On Delivery</th>
                                      </tr>
                                  </tfoot>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
              </div>
            </div>
     
            
            
          </div>
        </div>
        <!-- /page content -->
        
          <!-- jQuery k-w-h.com/app/views/warehouse/vendors --> 
   
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
<script  >




$(document).ready(function() {
    
    $("#submitForm").click( function()
    {
        var shipIDs= document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
       
       
        if (shipIDs=='')
        {
            bootbox.alert('Please enter IDs!'   );
            return;
        }
        
        var ids = shipIDs.split(',');
        for(i = 0; i < ids.length;++i){
            if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                ids[i] = ids[i].toUpperCase();
        }
        
        var wrongIDs = Array();
        var correctIDs = Array();
        var correctedIDs = Array();
        var finalArray = [];
        for (var i = 0; i <ids.length; i++ )
        {
            if(!ids[i].replace(/\s/g, '').length)
            {
                continue;
            }
             /*

                /^[a-z0-9]+$/i

                ^         Start of string
                [a-z0-9]  a or b or c or ... z or 0 or 1 or ... 9
                +         one or more times (change to * to allow empty string)
                $         end of string    
                /i        case-insensitive

            */
            if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i]) )
            {
                correctIDs.push(ids[i]);
            }
            else
            {
               /* if (/[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('JC' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
                else if (/C[0-9]{8}KS/.test(ids[i]))
                {
                    correctIDs.push('J' + ids[i]);
                    correctedIDs.push(ids[i]);
                }
               */
                wrongIDs.push(ids[i]);
            }
        }
        
        $.each(correctIDs, function(i, el)
        {
            if($.inArray(el, finalArray) === -1) 
            {
                finalArray.push(el);
            }
        });

        
        
        if (wrongIDs.length)
        {
            bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
        }
        
        /*if (correctedIDs.length)
        {
            bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
        }*/
    
        if (finalArray.length ) 
        {
            if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true)
            {
              $("#success_items_section").css("display", "none");
              $("#failed_items_section").css("display", "none");
              $.ajax({
                type: 'POST',
                url: '/warehouse/updatestatus',
                dataType: 'text',
                data: {to:3,  ids:finalArray.join(",")},
                }).done(function (response) {
                console.log(response);
                responseObj = JSON.parse(response);
                if(responseObj.error_message)
                  alert(responseObj.error_message);
                  responseObj = JSON.parse(response);
                  if (responseObj.success) {
                    alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                    $("#datatable_successitems").DataTable().destroy();
                    if(responseObj.success_items.length) {
                      let rows = [];
                      responseObj.success_items.forEach(function(element) {
                          if(!element.order_reference)
                              element.order_reference = ''
                          if(!element.order_number2)
                              element.order_number2 = '';
                          if(!element.d_city)
                              element.d_city = '';
                          if(!element.receiver_name)
                              element.receiver_name = '';
                          if(!element.receiver_phone)
                              element.receiver_phone = '';
                          if(!element.receiver_phone2)
                              element.receiver_phone2 == '';
                          rows.push([
                              element.jollychic,
                              element.order_number,
                              element.order_number2,
                              element.order_reference,
                              element.d_city,
                              element.receiver_name,
                              element.receiver_phone,
                              element.receiver_phone2,
                              element.cash_on_delivery
                          ]);
                      });
                      $("#datatable_successitems").DataTable({
                          "data": rows,
                          "autoWidth": false,
                          dom: "Blfrtip",
                          buttons: [
                              {
                                  extend: "copy",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "csv",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "excel",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "pdfHtml5",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "print",
                                  className: "btn-sm"
                              },
                          ],
                          responsive: true,
                          'order': [[0, 'asc']]
                      });
                      $("#success_items_section").css("display", "");
                    }
                    if(responseObj.failed_items.length) {
                      $("#datatable_faileditems").DataTable().destroy();
                      let rows = [];
                      responseObj.failed_items.forEach(function(element) {
                          rows.push([element.waybill, element.error]);
                      });
                      $("#datatable_faileditems").DataTable({
                          "data": rows,
                          "autoWidth": false,
                          dom: "Blfrtip",
                          buttons: [
                              {
                                  extend: "copy",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "csv",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "excel",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "pdfHtml5",
                                  className: "btn-sm"
                              },
                              {
                                  extend: "print",
                                  className: "btn-sm"
                              },
                          ],
                          responsive: true,
                          'order': [[0, 'asc']]
                      });
                      $("#failed_items_section").css("display", "");
                    }
                  }
                else
                  alert('Something went wrong!'   );
              });
            }
            else {
                alert('You cancled!' );
            }
            
        } 
        else {
            alert('Nothing to signin!'   );
        }    
       }
      );
});

</script  >
@stop

