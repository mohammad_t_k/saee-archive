@if( Auth::user()->role < 2)

  <script>window.location = "/warehouse/403";</script>
  
@endif


@extends('warehouse.jclayout')


@section('content')

 <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Shipment Details</h3>
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $packagedetails->jollychic ?> <small>Shipment ID <?php echo $packagedetails->shipment_id ?> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                      <form class="form-horizontal form-label-left form-label-right ">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->receiver_name ?> </label>
                      </div>
                      
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->receiver_phone ?> </label>
                      </div>
                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->receiver_phone2 ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address ?> </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address2 ?> </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address3 ?> </label>
                      </div>
                      
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_district ?> </label>
                      </div>
                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_city ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">State </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_state ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Created at </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->created_at ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->status ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Undelivered reason </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->undelivered_reason ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">COD </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->cash_on_delivery ?>  SAR</label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pincode </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->pincode ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_description ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_quantity ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_weight ?> </label>
                      </div>
                      
                      
                      <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Comments</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea class="resizable_textarea form-control" placeholder="Not functional yet.."></textarea>
                    </div>
                    </div>
                    <br/>
                          <span class="btn btn-primary">Save</span>

                    </form>
                      
                      
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


@stop