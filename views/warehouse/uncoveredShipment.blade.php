@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif


    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Uncovered Area Shipments</h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="post" action="/warehouse/unCoveredShipmentspost">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Companies *
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="company" id="company" class="form-control">
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"<?php if (isset($company_id) && $company_id == $company->id) echo 'selected';?>>{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Main City</label>

                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <select id="city" name="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{ $adcity->city }}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?> >{{ $adcity->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Moved</label>

                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <select id="is_moved" name="is_moved" class="form-control">
                                        <option value="1"<?php if (isset($isMoved) && $isMoved == 1) echo 'selected';?> >
                                            Yes
                                        </option>
                                        <option value="0"<?php if (isset($isMoved) && $isMoved == 0) echo 'selected';?> >
                                            No
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <p>
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_content">

                            <div class="row">
                                <table id="unCoveredShipmentsTable" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th><input id="selectallup" class="dt-checkboxes"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Company Name</th>
                                        <th>Order Number</th>
                                        <th>Waybill</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Address</th>
                                        <th>Mobile</th>
                                        <th>Main City</th>
                                        <th>Sub City</th>
                                        <th>District</th>
                                        <th>COD</th>

                                    </tr>
                                    </thead>
                                    <?php
                                    foreach ($items as $order) {
                                    ?>
                                    <tr id='<?= 'tr' . $order['id'] ?>'>
                                        <td><input id="ch<?= $order['id'] ?>" class="dt-checkboxes"
                                                   value="<?= $order['id'] ?>"
                                                   type="checkbox"></td>
                                        <td><?= $order['company_name'] ?></td>
                                        <td><?= $order['order_number'] ?></td>
                                        <td><?= $order['waybill'] ?></td>
                                        <td><?= $order['receiver_name'] ?></td>
                                        <td><?= $order['d_address'] ?></td>
                                        <td><?= $order['receiver_phone'] ?></td>
                                        <td><?= $order['main_city'] ?></td>
                                        <td><?= $order['d_city'] ?></td>
                                        <td><?= $order['d_district'] ?></td>
                                        <td><?= $order['cash_on_delivery'] ?></td>
                                    </tr>
                                    <?php } ?>


                                    <tfoot>
                                    <tr>
                                        <th><input id="selectalldown" class="dt-checkboxes" style="width: auto"
                                                   onchange="onChangeAllCheckBox(this)"
                                                   type="checkbox">Select
                                        </th>
                                        <th>Company Name</th>
                                        <th>Order Number</th>
                                        <th>Waybill</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Address</th>
                                        <th>Mobile</th>
                                        <th>Main City</th>
                                        <th>Sub City</th>
                                        <th>District</th>
                                        <th>COD</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                    <button id="moveToDeliveryRequest" class="btn btn-success" onclick="moveToDR();">Moved to Delivery
                        Request
                    </button>
                </div>
            </div>

        </div>
    </div>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>


    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>


    <script>
        var items = <?php echo $items; ?>;
        var recordsMoved = document.getElementById('is_moved').value;
        if (recordsMoved == 1) {
            $("#moveToDeliveryRequest").attr("disabled", true);
        }

        function onChangeAllCheckBox(checkbox) {
            document.getElementById('selectallup').checked = checkbox.checked;
            document.getElementById('selectalldown').checked = checkbox.checked;
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == checkbox.checked)
                    continue;
                document.getElementById('ch' + items[i].id).checked = checkbox.checked;

            }
        }

        function moveToDR() {
            $("#moveToDeliveryRequest").attr("disabled", true);
            var company = document.getElementById('company').value;
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                if (document.getElementById('ch' + items[i].id).checked == true) {
                    ids.push(items[i].id);
                }
            }
            if (ids.length == 0) {
                alert('Please select records to move');
                $("#moveToDeliveryRequest").attr("disabled", false);
                return;
            }
            else {
                var responseObj;
                if (confirm("Are you sure you want to move selected records in delivery request since rolling back will be difficult?") == true) {
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/moveUnCoveredintoDeliveryRequest',
                        dataType: 'text',
                        data: {
                            ids: ids.join(','),
                            company: company
                        },
                    }).done(function (response) {
                        console.log(response);
                        responseObj = JSON.parse(response);
                        if (responseObj.success == true) {
                            alert(responseObj.movedRecords + ' orders moved to delivery Request successfully');
                            window.location = '/warehouse/unCoveredShipments';
                        } else {
                            alert('Failed: ' + responseObj.error);
                        }
                        $("#moveToDeliveryRequest").attr("disabled", false);
                    });
                } else {
                    $("#moveToDeliveryRequest").attr("disabled", false);
                }
            }
        }


    </script>
    @stop