@if( Auth::user()->role < 3)

    <script>window.location = "/warehouse/403";</script>

@endif
<?php
if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 16)
    $layout = 'warehouse.viewlayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)

<style>
    
    .yellowClass{
        background-color:yellow !important;
    }
    .redClass{
        background-color:red !important;
        color: white;
    }
    #datatable_cancelled{
        width: 100% !important;
    }
    #datatable_returnedToJC {
    width: 100% !important;
}
</style>
@section('content')
    <!-- page content -->
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Shipments
                        <small>Jolly Chick Shipments</small>
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- top tiles -->
            <div class="row tile_count">


                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-thumbs-down"></i> Back in our office </span>

                    <div class="count red"><?php echo $undelivreditems ?></div>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Returned to JC </span>

                    <div class="count black"><?php echo $returnedtojcitems ?></div>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Cash to be collected </span>

                    <div class="count red"><?php echo number_format($cashtobecollected, 2) ?></div>
                </div>

            </div>
            <!-- /top tiles -->


            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <form method="get" action="/warehouse/customerservice">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="company" name="company" required="required" class="form-control"
                                                type="text">
                                            <option value="all">All Companies</option>
                                            @foreach($companies as $compan)
                                                <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Age More Than <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="age" value="{{ $agemorethan }}" placeholder="Days" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="submitForm" class="btn btn-success">Filter</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">


                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>In the warehouse | الرجيع
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were attemted to deliver but failed. رجيع.
                            </p>

                            <table style="width:100% !important" id="datatable_unDelivered" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th style="width: 7%">Updated at</th>
                                    <th style="width: 7%">Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th style="width: 7%">Updated at</th>
                                    <th style="width: 7%">Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->

                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Historical Returns | الرجيع السابق
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were attemted to deliver but failed. رجيع.
                            </p>

                            <table style="width: 100% !important;" id="datatable_historical_return" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->


                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Shipments with Age | الشحنات مع أعمارها
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were attemted to deliver but failed. رجيع.
                            </p>

                            <table style="width: 100% !important;" id="datatable_allshipments_age" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Company Name</th>
                                    <th>Sub City</th>
                                    <th>Main City</th>
                                    <th>Hub Name</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Company Name</th>
                                    <th>Sub City</th>
                                    <th>Main City</th>
                                    <th>Hub Name</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                 <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Returns | كامل الرجيع
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were attemted to deliver but failed. رجيع.
                            </p>

                            <table style="width: 100% !important;" id="datatable_all_return" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>District</th>

                                    <th>Updated at</th>
                                    <th>Scheduled Date</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th style="width: 10%">Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->


                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Cancelled
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were cancelled by customers
                            </p>
                            <table id="datatable_cancelled" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th>Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th>Reason</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>


                <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Returned to JollyChic
                                <small>Shipments</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Items that were returned to JollyChic
                            </p>
                            <table id="datatable_returnedToJC" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th>Reason</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Waybill</th>
                                    <th>Compay Name</th>
                                    <th>City</th>
                                    <th>Name</th>
                                    <th>Mobile1</th>
                                    <th>Mobile2</th>
                                    <th>Street Addrs</th>
                                    <th>District</th>
                                    <th>Delivery Attempts</th>
                                    <th>COD</th>
                                    <th>Age</th>
                                    <th>Comments</th>
                                    <th>Reason</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->
                <?php //echo $undeliveredorders ?>


            </div>
        </div>
    </div>
    <!-- /page content -->




    <div id="dialog-cancel">

        <p>Are you sure you want to cancel this shipment ?</p>

        <form method="post" action="/warehouse/customerservice/cancelshipment" id="confirmation-form">
            <input type="submit" id="yes" name="confirm" value="Yes">
            <input type="submit" id="no" name="confirm" value="No">
            <input type="hidden" name="id" value="">
        </form>
    </div>

    <div id="dialog-res">
        <form method="post" action="/warehouse/customerservice/newscheduled" id="confirmation-form">

            <label class="control-label col-md-12 col-sm-12 col-xs-12">Please Select New Scheduled Date
            </label>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <input id="new_date" name="new_date" value=""
                       class="date-picker form-control col-md-7 col-xs-12"
                       type="date">
            </div>

            <input type="hidden" name="resid" value="">
            <input type="submit" value="Save">
        </form>

    </div>




    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css"
          href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css"/>

    <div id="dialog-tracking">
        <form method="post" action="" id="confirmation-form">
            <input type="hidden" name="trackingnum" value="">
        </form>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="order_tracking">
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30 jc-tracking">

                    </p>
                    <table id="datatable_hovertracking" class="table table-striped table-bordered ">
                        <thead>
                        <tr>

                            <th>Date and Time</th>
                            <th>Status</th>
                            <th>Captain Name</th>
                            <th>Mobile</th>
                            <th>Customer Name</th>
                            <th>City</th>
                            <th>Remarks</th>
                        </tr>
                        </thead>


                        <tfoot>
                        <tr>
                            <th>Date and Time</th>
                            <th>Status</th>
                            <th>Captain Name</th>
                            <th>Mobile</th>
                            <th>Customer Name</th>
                            <th>City</th>
                            <th>Remarks</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>


    <!-- jstables script -->

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>

    <script>
        <?php


        $mydata_backToOffice = '';
        $historical_returns = '';
        $all_returns = '';
        $allshipments_with_age ='';
        $mydata_returnedToJC = '';
        $mydata_cancelled = '';
        $append = 'res';

        foreach ($undeliveredorders as $order) {

            $mydata_backToOffice .= "[ \"$order->updated_at\" , \"$order->jollychic\" ,\"$order->company_name\", \"$order->d_city\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->d_district\", \"$order->updated_at\", \"$order->scheduled_shipment_date\", \"$order->age\" , \"$order->comments\", \"$order->undelivered_reason\" ,  \"<a href=\'/warehouse/package/$order->jollychic\'>Edit</a>\", \" <select id='actionsel'><option value='' selected></option><option name='cancel' value='$order->jollychic'>Cancel</option><option name='res' value='$order->jollychic'>Reschedule</option><option hidden class='age' name='age' value = '$order->age'></option><option hidden class='failed_delivery' name='failed_delivery' value='$order->num_faild_delivery_attempts'></option><option hidden class='age_limit' name='age_limit' value='$order->age_limit'></option></select>\"], ";


        }

        foreach ($historicalOrders as $order) {

          
            $historical_returns .= "[ \"$order->updated_at\" ,  \"$order->jollychic\" ,\"$order->company_name\", \"$order->d_city\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->d_district\", \"$order->updated_at\", \"$order->scheduled_shipment_date\",  \"$order->age\" , \"$order->comments\", \"$order->undelivered_reason\" , \"<a href=\'/warehouse/package/$order->jollychic\'>Edit</a>\", \" <select id='actionsel'><option value='' selected></option><option name='cancel' value='$order->jollychic'>Cancel</option><option name='res' value='$order->jollychic'>Reschedule</option><option hidden class='age' name='age' value = '$order->age'></option><option hidden class='failed_delivery' name='failed_delivery' value='$order->num_faild_delivery_attempts'></option><option hidden class='age_limit' name='age_limit' value='$order->age_limit'></option></select>\"], ";


        }

        foreach ($allshipmentswithage as $order) {

          
            $allshipments_with_age .= "[ \"$order->updated_at\" ,  \"$order->jollychic\" , \"$order->company_name\" , \"$order->d_city\", \"$order->main_city\", \"$order->hub_name\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->d_district\", \"$order->updated_at\", \"$order->scheduled_shipment_date\",  \"$order->age\" , \"$order->comments\", \"$order->undelivered_reason\" , \"$order->status\", \"<a href=\'/warehouse/package/$order->jollychic\'>Edit</a>\"], ";


        }

         foreach ($allReturns as $order) {

            $all_returns .= "[ \"$order->updated_at\" , \"$order->jollychic\" ,\"$order->company_name\", \"$order->d_city\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->d_district\", \"$order->updated_at\", \"$order->scheduled_shipment_date\", \"$order->age\" ,  \"$order->comments\", \"$order->undelivered_reason\" , \"<a href=\'/warehouse/package/$order->jollychic\'>Edit</a>\", \" <select id='actionsel'><option value='' selected></option><option name='cancel' value='$order->jollychic'>Cancel</option><option name='res' value='$order->jollychic'>Reschedule</option><option hidden class='age' name='age' value = '$order->age'></option><option hidden class='failed_delivery' name='failed_delivery' value='$order->num_faild_delivery_attempts'></option><option hidden class='age_limit' name='age_limit' value='$order->age_limit'></option></select>\"], ";


        }

        foreach ($returnedorders as $order) {

            //JollyChicID  Name    Mobile1     Mobile2     Street Addrs    District    COD     Reason
            $mydata_returnedToJC .= "[ \"$order->scheduled_shipment_date\", \"$order->jollychic\" , \"$order->company_name\", \"$order->d_city\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->age\" , \"$order->comments\" , \"$order->undelivered_reason\"], ";

        }

        foreach ($cancelled as $order) {

            //JollyChicID  Name    Mobile1     Mobile2     Street Addrs    District    COD     Reason
            $mydata_cancelled .= "[ \"$order->scheduled_shipment_date\", \"$order->jollychic\" , \"$order->company_name\", \"$order->d_city\", \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \"$order->d_address\", \"$order->d_district\", \"$order->num_faild_delivery_attempts\" , \"$order->cash_on_delivery\", \"$order->age\" , \"$order->comments\" , \"$order->undelivered_reason\", \"<a href=\'/warehouse/package/$order->jollychic\'>Edit</a>\", \" <select id='actionsel'><option value='' selected></option><option name='cancel' value='$order->jollychic'>Cancel</option><option name='res' value='$order->jollychic'>Reschedule</option><option hidden class='age' name='age' value = '$order->age'></option><option hidden class='failed_delivery' name='failed_delivery' value='$order->num_faild_delivery_attempts'></option><option hidden class='age_limit' name='age_limit' value='$order->age_limit'></option></select>\"], ";

        }

        ?>

        $(document).ready(function () {


            var table_unDelivered = $("#datatable_unDelivered").DataTable({

                "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[11];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },

                "data": [
                    <?php echo $mydata_backToOffice ?>

                ],

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }

                ],
                'order': [[0, 'desc']]
            });

            var table_historicalReturns = $("#datatable_historical_return").DataTable({

                "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[11];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },

                "data": [
                    <?php echo $historical_returns ?>

                ],


                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                    
                ],
                'order': [[0, 'desc']]
            });



            var table_all_shipments_age = $("#datatable_allshipments_age").DataTable({

                "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[10];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },

                "data": [
                    <?php echo $allshipments_with_age ?>

                ],


                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                    
                ],
                'order': [[0, 'desc']]
            });


             var table_historicalReturns = $("#datatable_all_return").DataTable({

                 "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[11];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },

                "data": [
                    <?php echo $all_returns ?>

                ],

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }

                ],
                'order': [[0, 'desc']]
            });


            var table_returnedToJC = $("#datatable_returnedToJC").DataTable({

                "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[0];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },  

                "data": [
                    <?php echo $mydata_returnedToJC ?>

                ],

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }

                ],
                'order': [[0, 'asc']]
            });


            var table_returnedToJC = $("#datatable_cancelled").DataTable({

                         "createdRow": function( row, data, dataIndex){

                   
                    var scheduled_shipment_date = data[0];

                var e = new Date(scheduled_shipment_date),
        smonth = '' + (e.getMonth() + 1),
        sday = '' + e.getDate(),
        syear = e.getFullYear();

    if (smonth.length < 2) smonth = '0' + smonth;
    if (sday.length < 2) sday = '0' + sday;

                scheduled_shipment_date = [syear, smonth, sday].join('-');


                    var d = new Date();

                    var month = d.getMonth()+1;
                    var day = d.getDate();

                    var currentDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

                    diffDays = Math.abs(d.getTime() - e.getTime());

                    
                    diffDays = Math.ceil(diffDays / (1000 * 3600 * 24)); 

              
                if(diffDays > 21) {
                     $(row).addClass('redClass');
                 }
                 else if(data[7] > 3) {
                     $(row).addClass('yellowClass');
                 }
            },    

                "data": [
                    <?php echo $mydata_cancelled ?>

                ],

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }

                ],
                'order': [[1, 'asc']]
            });


        });


    </script>

    <script>

        $(document).ready(function () {


            $('#dialog-cancel').dialog({
                modal: true,
                autoOpen: false,
                maxWidth: 600,
                maxHeight: 500,
                width: 600,
                height: 200
            });
            $('#dialog-res').dialog({
                modal: true,
                autoOpen: false,
                maxWidth: 600,
                maxHeight: 500,
                width: 600,
                height: 200,
            });

            $('#dialog-tracking').dialog({
                modal: true,
                autoOpen: false,
                maxWidth: 600,
                maxHeight: 500,
                width: 800,
                height: 300,
            });


            $(document).on('change', 'select', function () {
                var dialog = $(this).find('option:selected').attr("name");

                var age =  $(this).find('.age').val();

                var age_limit = $(this).find('.age_limit').val();

                var num_faild_delivery_attempts = $(this).find('.failed_delivery').val();

                if (dialog == "cancel") {

                    var id = $(this).val();

                    $('#dialog-cancel').data('id', id).dialog('open');

                    $('input[name="id"]').val(id);

                }

                if (dialog == "res") {

                    var id = $(this).val();

                    if (age > age_limit || num_faild_delivery_attempts > 2){
                        alert('Sorry Delivery Attempts  > 2 or Shipment Age > age limit');
                    }
                    else{
                    $('#dialog-res').data('resid', id).dialog('open');


                    $('input[name="resid"]').val(id);
                }

                }
            });


            $('#datatable_unDelivered tbody').on('mousedown', 'tr td[tabindex*="0"]', function () {

                var table = $('#datatable_unDelivered').DataTable();


                var id = $(this).html();

                function getJSONCustomers(callback) {
                    $.ajax({
                        type: "GET",
                        url: '/warehouse/hovertracking?trackingnum=' + id,
                        dataType: 'text',
                        cache: false,
                        success: callback
                    });
                };


                getJSONCustomers(function (res) {
                    var orderid = '';
                    var trHTML = '<tr>' +

                        '<th>Date and Time</th>' +
                        '<th>Status</th>' +
                        '<th>Captain Name</th>' +
                        '<th>Mobile</th>' +
                        '<th>Customer Name</th>' +
                        '<th>City</th>' +
                        '<th>Remarks</th>' +
                        '</tr>';
                    var mydata = JSON.parse(res);
                    $.each(mydata.data, function (i, item) {

                        var captain_name = '';
                        var captain_phone = '';
                        if (item.first_name) {
                            captain_name = item.first_name + " " + item.last_name;
                            captain_phone = item.phone;
                        }
                        trHTML += '<tr><td>' + item.created_at + '</td><td>' + item.statustxt + '</td><td>' + captain_name + '</td><td>' + captain_phone + '</td><td>' + item.receiver_name + '</td><td>' + item.d_city + '</td><td>' + item.notes + '</td></tr>';
                        orderid = 'Order Tracking ' + item.jollychic;

                    });
                    $('#datatable_hovertracking').html(trHTML);
                    $('#order_tracking').html(orderid);

                });


                $("#dialog-tracking").on("dialogopen", function (event, ui) {


                });

                $('#dialog-tracking').data('trackingnum', id).dialog('open');

                $('input[name="trackingnum"]').val(id);

            });


        });

    </script>

@stop
