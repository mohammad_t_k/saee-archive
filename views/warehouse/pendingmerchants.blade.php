@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
<!-- page content -->

<style>
    table thead tr th {
        text-align: center;
    }
    table tbody tr td {
        text-align: center;
    }
    table tfoot tr th {
        text-align: center;
    }
</style>

<div class="right_col" role="main">
    <div class="">
    <div class="page-title">
        <div class="title_left">
        <h3>Merchants data</h3>
        </div>
        
    </div>
        
            
        <div class="row">
           
          <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Merchants information</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Merchants data
                    </p>
                    <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->
                  
                    
                    <table id="datatable_captains" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                          <th>Name</th> 
                          <th>Phone</th>
                          <th>Email</th>
                          <th>Company Registration</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($merchants as $merchant) { ?>
                            <td><?= $merchant['name'] ?></td>
                            <td><?= $merchant['phone'] ?></td>
                            <td><?= $merchant['email'] ?></td>
                            <td><?= $merchant['has_cr'] == 0 ? 'NO' : 'YES' ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                      <tfoot>
                        <tr>
                          <th>Name</th> 
                          <th>Phone</th>
                          <th>Email</th>
                          <th>Company Registration</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            
        <?php //echo $captainsdata ?>



        </div>
    </div>
</div>
<!-- /page content -->
                    

<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->

 <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
@stop
