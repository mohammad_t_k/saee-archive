@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <div class="right_col" role="main">
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <form method="post" action="/warehouse/branchperformanceenpost">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Companies</label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <select name="branches[]" class="mdb-select colorful-select dropdown-primary form-control" multiple searchable="Search here..">
                                                @foreach($companieslist as $compan)
                                                    <option value="{{$compan->id}}"<?php if (in_array($compan->id, $selectedBranches)) echo 'selected';?>>{{$compan->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">City</label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <select name="city" id="city" class="form-control">
                                                @foreach($adminCities as $adcity)
                                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">From</label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <input type="date" class="date-picker form-control col-md-7 col-xs-12" name="from" value="{{ $from }}" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">To</label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <input type="date" class="date-picker form-control col-md-7 col-xs-12" name="to" value="{{ $to }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <p><button id="submitForm" class="btn btn-success">Filter</button></p>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $branches = array_reverse($branches);

           foreach($branches as $newbr){

           	$other_br[] = $newbr;
           }
            
            
            $otherbranches = Companies::select('id')->whereNotIn('id', $other_br)->get();

            $other_branches = '';

            foreach($otherbranches as $otherbr){

            	$other_branches .= '&chosen_companies[]='.$otherbr->id;
            }

            
            $comp_width = 100.0 / (count($branches) + 6);
            $bayan = $comp_width + 4 * $comp_width
            ?>

            <div class="row">
                <div class="container">
                    <div class="x_panel">
                        @if(isset($city) && $city == 'All')
                            <h3 style="text-align: center;">Total Cities</h3>
                            <table class="table">
                                <thead style="text-align: center;">
                                <tr>
								<th style="width: {{ $bayan }}%; text-align: center;">Description</th>
                                    
                                    @foreach($branches as $branch)
                                        <th style="width: {{ $comp_width }}%;">{{ $companiesnames[$branch] }}</th>
                                    @endforeach
                                    
                                    <th style="width: {{ $comp_width }}%;">Other</th>
									<th style="width: {{ $comp_width }}%;">Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->ToBePickedUp ?>
                                    <?php $total += $res->ToBePickedUp ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->ToBePickedUp; ?>
                                                <?php $total += $res->ToBePickedUp; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Shipments On Hold</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->ReservedToPickup ?>
                                    <?php $total += $res->ReservedToPickup ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->ReservedToPickup; ?>
                                                <?php $total += $res->ReservedToPickup; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Shipments Reserved</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
									
									
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->PickedUp ?>
                                    <?php $total += $res->PickedUp ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->PickedUp; ?>
                                                <?php $total += $res->PickedUp; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									 <td style="width: 50%; text-align: center;">Shipments Received From Company</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                   
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->CreatedBySupplier ?>
                                    <?php $total += $res->CreatedBySupplier ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->CreatedBySupplier; ?>
                                                <?php $total += $res->CreatedBySupplier; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Shipments Created</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
									
									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->InRoute ?>
                                    <?php $total += $res->InRoute ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->InRoute; ?>
                                                <?php $total += $res->InRoute; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Shipments In Route</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->NewNotScheduled ?>
                                    <?php $total += $res->NewNotScheduled ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->NewNotScheduled; ?>
                                                <?php $total += $res->NewNotScheduled; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">New Unscheduled Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->OldNotScheduled ?>
                                    <?php $total += $res->OldNotScheduled ?>
                                @endforeach
                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->OldNotScheduled; ?>
                                                <?php $total += $res->OldNotScheduled; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Old Unscheduled Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalNotScheduled ?>
                                    <?php $total += $res->TotalNotScheduled ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalNotScheduled; ?>
                                                <?php $total += $res->TotalNotScheduled; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total Unscheduled Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{ $other_branches }}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</td>
                                </tr>

                                <tr>
                                    <th colspan="{{ count($branches) + 3 }}" style="text-align: center; background-color: #f2f2f2">Daily Tasks</th>
                                </tr>

                                <tr>
                                    <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Shipments To Dispatch Today</th>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->NewScheduledforToday ?>
                                    <?php $total += $res->NewScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->NewScheduledforToday; ?>
                                                <?php $total += $res->NewScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total New Shipemnts To Dispatch Today</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->OldScheduledforToday ?>
                                    <?php $total += $res->OldScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->OldScheduledforToday; ?>
                                                <?php $total += $res->OldScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total Rescheduled Shipments To Dispatch Today</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
									
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalScheduledforToday ?>
                                    <?php $total += $res->TotalScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalScheduledforToday; ?>
                                                <?php $total += $res->TotalScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                    <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Dispatched Today To Captains</th>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->NewWithCaptainsScheduledforToday ?>
                                    <?php $total += $res->NewWithCaptainsScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->NewWithCaptainsScheduledforToday; ?>
                                                <?php $total += $res->NewWithCaptainsScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">New Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->OldWithCaptainsScheduledforToday ?>
                                    <?php $total += $res->OldWithCaptainsScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->OldWithCaptainsScheduledforToday; ?>
                                                <?php $total += $res->OldWithCaptainsScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Rescheduled Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalWithCaptainsScheduledforToday ?>
                                    <?php $total += $res->TotalWithCaptainsScheduledforToday ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalWithCaptainsScheduledforToday; ?>
                                                <?php $total += $res->TotalWithCaptainsScheduledforToday; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>

                                <tr>
                                    <!-- calculate 'Total Left Shipments Not Dispatched Today' -->
                                    <!-- equation:  'Total Left Shipments Not Dispatched Today' = 'Shipments To Dispatch Today' - 'Dispatched Today To Captains' -->
                                    <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Total Left Shipments Not Dispatched Today</th>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->NewRemaining ?>
                                    <?php $total += $res->NewRemaining ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->NewRemaining; ?>
                                                <?php $total += $res->NewRemaining; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">New Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td>{{ $cell[$branch] }}</td>
                                    @endforeach
                                    
                                    <td>{{ $other }}</td>
									<td>{{ $total }}</td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->OldRemaining ?>
                                    <?php $total += $res->OldRemaining ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->OldRemaining; ?>
                                                <?php $total += $res->OldRemaining; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Rescheduled Shipments</td>
                                    
                                    @foreach($branches as $branch)
                                        <td>{{ $cell[$branch] }}</td>
                                    @endforeach
                                    
                                    <td>{{ $other }}</td>
									<td>{{ $total }}</td>
                                </tr>

                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalRemaining ?>
                                    <?php $total += $res->TotalRemaining ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalRemaining; ?>
                                                <?php $total += $res->TotalRemaining; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total Left</td>
                                    
                                    @foreach($branches as $branch)
                                        <td>{{ $cell[$branch] }}</td>
                                    @endforeach
                                    
                                    <td>{{ $other }}</td>
									<td>{{ $total }}</td>
                                </tr>

                                <tr>
                                    <th colspan="{{ count($branches) + 3 }}" style="text-align: center;  background-color: #f2f2f2">Stocktaking On the System</th>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalDelivered ?>
                                    <?php $total += $res->TotalDelivered ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalDelivered; ?>
                                                <?php $total += $res->TotalDelivered; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total Delivered In Branch</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalReturnedtoSupplier ?>
                                    <?php $total += $res->TotalReturnedtoSupplier ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalReturnedtoSupplier; ?>
                                                <?php $total += $res->TotalReturnedtoSupplier; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">RTO</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->WrongNumberAddressandShiftingReturns ?>
                                    <?php $total += $res->WrongNumberAddressandShiftingReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->WrongNumberAddressandShiftingReturns; ?>
                                                <?php $total += $res->WrongNumberAddressandShiftingReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Wrong Address / Customer Moved From City</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->NoAnswerReturns ?>
                                    <?php $total += $res->NoAnswerReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->NoAnswerReturns; ?>
                                                <?php $total += $res->NoAnswerReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Customer Does Not Answer / Mobile Switched Off</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalRescheduledReturns ?>
                                    <?php $total += $res->TotalRescheduledReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalRescheduledReturns; ?>
                                                <?php $total += $res->TotalRescheduledReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									 <td style="width: 50%; text-align: center;">Rescheduled - Captains / Customer Service</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                   
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->RescheduledbyCaptainsReturns ?>
                                    <?php $total += $res->RescheduledbyCaptainsReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->RescheduledbyCaptainsReturns; ?>
                                                <?php $total += $res->RescheduledbyCaptainsReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Captains</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>
									
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->RescheduledbyCSReturns ?>
                                    <?php $total += $res->RescheduledbyCSReturns ?>
                                @endforeach

                                <!-- calc companies cells independently-->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->RescheduledbyCSReturns; ?>
                                                <?php $total += $res->RescheduledbyCSReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Customer Service</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                  
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalCancelledReturns ?>
                                    <?php $total += $res->TotalCancelledReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalCancelledReturns; ?>
                                                <?php $total += $res->TotalCancelledReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Canceled By Customer - Captains / Customer Service</td>
                                    
                                    @foreach($branches as $branch)
                                      <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->CancelledbyCaptainsReturns ?>
                                    <?php $total += $res->CancelledbyCaptainsReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->CancelledbyCaptainsReturns; ?>
                                                <?php $total += $res->CancelledbyCaptainsReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Captains</td>
                                    
                                    @foreach($branches as $branch)
                                       <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->CancelledbyCSReturns ?>
                                    <?php $total += $res->CancelledbyCSReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->CancelledbyCSReturns; ?>
                                                <?php $total += $res->CancelledbyCSReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Customer Service</td>
                                    
                                    @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>


                                <tr>
                                <?php
                                $total = $other = 0;
                                $cell = [];
                                ?>
                                <!-- calc 'others' and 'total' cells -->
                                @foreach($results_rev as $res)
                                    <?php $other += $res->TotalReturns ?>
                                    <?php $total += $res->TotalReturns ?>
                                @endforeach

                                <!-- calc companies cells independently -->
                                    @foreach($branches as $branch)
                                        <?php $cell[$branch] = 0; ?>
                                        @foreach($results as $res)
                                            @if($res->company_id == $branch)
                                                <?php $cell[$branch] += $res->TotalReturns; ?>
                                                <?php $total += $res->TotalReturns; ?>
                                            @endif
                                        @endforeach
                                    @endforeach
									<td style="width: 50%; text-align: center;">Total Returns</td>
                                    
                                    @foreach($branches as $branch)
                                       <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                </tr>
                                </tbody>
                            </table>
                            <?php
                            $NewActiveCaptains = $NewNotActiveCaptains = $OldActiveCaptains = $OldNotActiveCaptains = $TotalCaptains = 0;
                            foreach($citieslist as $city) {
                                $NewActiveCaptains += $captains[$city]->NewActiveCaptains;
                                $NewNotActiveCaptains += $captains[$city]->NewNotActiveCaptains;
                                $OldActiveCaptains += $captains[$city]->OldActiveCaptains;
                                $OldNotActiveCaptains += $captains[$city]->OldNotActiveCaptains;
                                $TotalCaptains += $captains[$city]->TotalCaptains;
                            }
                            ?>
                            <table class="table table-bordered" style="width: 50%; margin-left:auto; margin-right:auto;">
                                <tbody>
                                <tr>
                                    <th colspan="2" style="text-align: center; background-color: #f2f2f2">Captains Follow Up</th>
                                </tr>
                                <tr style="text-align: center;">
                                    
                                    <td>New Active Captains - Over a Month</td>
									<td style="width: 50%;">{{ $NewActiveCaptains }}</td>
                                </tr>

                                <tr style="text-align: center;">
                                    
                                    <td>New Inactive Captains - Over a Month</td>
									<td>{{ $NewNotActiveCaptains }}</td>
                                </tr>

                                <tr style="text-align: center;">
                                    
                                    <td>Old Active Captains - Over a Month</td>
									<td>{{ $OldActiveCaptains }}</td>
                                </tr>

                                <tr style="text-align: center;">
                                    
                                    <td>Old Inactive Captains - Over a Month</td>
									<td>{{ $OldNotActiveCaptains }}</td>
                                </tr>

                                <tr style="text-align: center;">
                                    
                                    <td>Total Captains</td>
									<td>{{ $TotalCaptains }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <hr/>
                            <div id="flag_between_tables" style="color: red;"></div>
                        @endif
                    <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------- CITIES TABLES --------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
                        @foreach($citieslist as $city)
                            @if($city != 'All')
                                <h3 style="text-align: center;">{{ $citiesnames[$city] }}</h3>
                                <table class="table table-stripped">
                                    <thead style="text-align: center;">
                                    <tr>
									<th style="width: {{ $bayan }}%; text-align: center;">Description</th>
                                        
                                        @foreach($branches as $branch)
                                            <th style="width: {{ $comp_width }}%">{{ $companiesnames[$branch] }}</th>
                                        @endforeach
                                        
                                        <th style="width: {{ $comp_width }}%">Other</th>
										<th style="width: {{ $comp_width }}%">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->ToBePickedUp ?>
                                            <?php $total += $res->ToBePickedUp ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->ToBePickedUp; ?>
                                                    <?php $total += $res->ToBePickedUp; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Shipments On Hold</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-3&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->ReservedToPickup ?>
                                            <?php $total += $res->ReservedToPickup ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->ReservedToPickup; ?>
                                                    <?php $total += $res->ReservedToPickup; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Shipments Reserved</td>
                                        
                                        @foreach($branches as $branch)
                                             <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=pickup_reserved_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->PickedUp ?>
                                            <?php $total += $res->PickedUp ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->PickedUp; ?>
                                                    <?php $total += $res->PickedUp; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Shipments Received From Company</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=-1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->CreatedBySupplier ?>
                                            <?php $total += $res->CreatedBySupplier ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->CreatedBySupplier; ?>
                                                    <?php $total += $res->CreatedBySupplier; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Shipments Created</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=0&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->InRoute ?>
                                            <?php $total += $res->InRoute ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->InRoute; ?>
                                                    <?php $total += $res->InRoute; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Shipments In Route</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=1&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=all&new_shipment_date=all&failed_delivery_attempts=&pieces=&age_more=&age_less=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->NewNotScheduled ?>
                                            <?php $total += $res->NewNotScheduled ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->NewNotScheduled; ?>
                                                    <?php $total += $res->NewNotScheduled; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">New Unscheduled Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                             <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->OldNotScheduled ?>
                                            <?php $total += $res->OldNotScheduled ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->OldNotScheduled; ?>
                                                    <?php $total += $res->OldNotScheduled; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Old Unscheduled Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalNotScheduled ?>
                                            <?php $total += $res->TotalNotScheduled ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalNotScheduled; ?>
                                                    <?php $total += $res->TotalNotScheduled; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total Unscheduled Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=null&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                        <th colspan="{{ count($branches) + 3 }}" style="text-align: center; background-color: #f2f2f2">Daily Tasks</th>
                                    </tr>

                                    <tr>
                                        <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Shipments To Dispatch Today</th>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->NewScheduledforToday ?>
                                            <?php $total += $res->NewScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->NewScheduledforToday; ?>
                                                    <?php $total += $res->NewScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total New Shipemnts To Dispatch Today</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
										 <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                       
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->OldScheduledforToday ?>
                                            <?php $total += $res->OldScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->OldScheduledforToday; ?>
                                                    <?php $total += $res->OldScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total Rescheduled Shipments To Dispatch Today</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalScheduledforToday ?>
                                            <?php $total += $res->TotalScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalScheduledforToday; ?>
                                                    <?php $total += $res->TotalScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                        
                                    </tr>

                                    <tr>
                                        <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Dispatched Today To Captains</th>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->NewWithCaptainsScheduledforToday ?>
                                            <?php $total += $res->NewWithCaptainsScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->NewWithCaptainsScheduledforToday; ?>
                                                    <?php $total += $res->NewWithCaptainsScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">New Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator==&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->OldWithCaptainsScheduledforToday ?>
                                            <?php $total += $res->OldWithCaptainsScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->OldWithCaptainsScheduledforToday; ?>
                                                    <?php $total += $res->OldWithCaptainsScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Rescheduled Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalWithCaptainsScheduledforToday ?>
                                            <?php $total += $res->TotalWithCaptainsScheduledforToday ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalWithCaptainsScheduledforToday; ?>
                                                    <?php $total += $res->TotalWithCaptainsScheduledforToday; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total</td>
                                        
                                        @foreach($branches as $branch)
                                            <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                        @endforeach
                                        
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

										<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=4&status[]=5&status[]=6&status[]=6&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=scheduled_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>

                                    <tr>
                                        <th colspan="{{ count($branches) + 3 }}" style="text-align: center;">Total Left Shipments Not Dispatched Today</th>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->NewRemaining ?>
                                            <?php $total += $res->NewRemaining ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->NewRemaining; ?>
                                                    <?php $total += $res->NewRemaining; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">New Shipments</td>
                                        
                                        @foreach($branches as $branch)
                                            <td>{{ $cell[$branch] }}</td>
                                        @endforeach
                                        
                                        <td>{{ $other }}</td>
										<td>{{ $total }}</td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->OldRemaining ?>
                                            <?php $total += $res->OldRemaining ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->OldRemaining; ?>
                                                    <?php $total += $res->OldRemaining; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Rescheduled Shipments</td>
                                       
                                        @foreach($branches as $branch)
                                            <td>{{ $cell[$branch] }}</td>
                                        @endforeach
                                         
                                        <td>{{ $other }}</td>
										<td>{{ $total }}</td>
                                    </tr>

                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalRemaining ?>
                                            <?php $total += $res->TotalRemaining ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalRemaining; ?>
                                                    <?php $total += $res->TotalRemaining; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total Left</td>
                                        
                                        @foreach($branches as $branch)
                                            <td>{{ $cell[$branch] }}</td>
                                        @endforeach
										<td>{{ $other }}</td>
                                        <td>{{ $total }}</td>
                                        
                                    </tr>

                                    <tr>
                                        <th colspan="{{ count($branches) + 3 }}" style="text-align: center;  background-color: #f2f2f2">Stocktaking On the System</th>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalDelivered ?>
                                            <?php $total += $res->TotalDelivered ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalDelivered; ?>
                                                    <?php $total += $res->TotalDelivered; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total Delivered In Branch</td>
                                        
                                       @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=5&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                        
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalReturnedtoSupplier ?>
                                            <?php $total += $res->TotalReturnedtoSupplier ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalReturnedtoSupplier; ?>
                                                    <?php $total += $res->TotalReturnedtoSupplier; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">RTO</td>
                                        
                                        @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=7&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=all&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->WrongNumberAddressandShiftingReturns ?>
                                            <?php $total += $res->WrongNumberAddressandShiftingReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->WrongNumberAddressandShiftingReturns; ?>
                                                    <?php $total += $res->WrongNumberAddressandShiftingReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Wrong Address / Customer Moved From City</td>
                                        
                                        @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=4&chosen_undelivered_reason[]=7&chosen_undelivered_reason[]=8&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->NoAnswerReturns ?>
                                            <?php $total += $res->NoAnswerReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->NoAnswerReturns; ?>
                                                    <?php $total += $res->NoAnswerReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Customer Does Not Answer / Mobile Switched Off</td>
                                        
                                         @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=1&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalRescheduledReturns ?>
                                            <?php $total += $res->TotalRescheduledReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalRescheduledReturns; ?>
                                                    <?php $total += $res->TotalRescheduledReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Rescheduled - Captains / Customer Service</td>
                                        
                                        @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                   
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                        
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->RescheduledbyCaptainsReturns ?>
                                            <?php $total += $res->RescheduledbyCaptainsReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->RescheduledbyCaptainsReturns; ?>
                                                    <?php $total += $res->RescheduledbyCaptainsReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Captains</td>
                                        
                                       @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>

                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=12&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->RescheduledbyCSReturns ?>
                                            <?php $total += $res->RescheduledbyCSReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->RescheduledbyCSReturns; ?>
                                                    <?php $total += $res->RescheduledbyCSReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Customer Service</td>
                                        
                                       @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                  
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{$city}}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=6&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalCancelledReturns ?>
                                            <?php $total += $res->TotalCancelledReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalCancelledReturns; ?>
                                                    <?php $total += $res->TotalCancelledReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Canceled By Customer - Captains / Customer Service</td>
                                        
                                        @foreach($branches as $branch)
                                      <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->CancelledbyCaptainsReturns ?>
                                            <?php $total += $res->CancelledbyCaptainsReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->CancelledbyCaptainsReturns; ?>
                                                    <?php $total += $res->CancelledbyCaptainsReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Captains</td>
                                       
                                        @foreach($branches as $branch)
                                       <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=13&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                        
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->CancelledbyCSReturns ?>
                                            <?php $total += $res->CancelledbyCSReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->CancelledbyCSReturns; ?>
                                                    <?php $total += $res->CancelledbyCSReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Customer Service</td>
                                        
                                       @foreach($branches as $branch)
                                        <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=3&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                        
                                    </tr>


                                    <tr>
                                    <?php
                                    $total = $other = 0;
                                    $cell = [];
                                    ?>
                                    <!-- calc 'others' and 'total' cells -->
                                    @foreach($results_rev as $res)
                                        @if($res->d_city == $city)
                                            <?php $other += $res->TotalReturns ?>
                                            <?php $total += $res->TotalReturns ?>
                                        @endif
                                    @endforeach

                                    <!-- calc companies cells independently -->
                                        @foreach($branches as $branch)
                                            <?php $cell[$branch] = 0; ?>
                                            @foreach($results as $res)
                                                @if($res->company_id == $branch && $res->d_city == $city)
                                                    <?php $cell[$branch] += $res->TotalReturns; ?>
                                                    <?php $total += $res->TotalReturns; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
										<td style="width: 50%; text-align: center;">Total Returns</td>
                                        
                                        @foreach($branches as $branch)
                                       <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$branch}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $cell[$branch] }}</a></td>
                                    @endforeach
                                    
                                    <td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]={{$other_branches}}&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $other }}</a></td>

									<td><a href="/warehouse/shipmentsExport?tabFilter=3&chosen_companies[]=all&chosen_cities[]={{ $city }}&chosen_subcities[]=All&chosen_hubs[]=0&chosen_porta_hubs[]=ignore&status[]=2&chosen_sub_statuses[]=all&chosen_undelivered_reason[]=&zero_cod=all&shipment_date=&new_shipment_date=all&failed_delivery_attempts=0&failed_operator=>&pieces=&age_more=&age_less=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10">{{ $total }}</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered" style="width: 50%; margin-left: auto; margin-right: auto;">
                                    <tbody>
                                    <tr>
                                        <th colspan="2" style="text-align: center; background-color: #f2f2f2;">Captains Follow Up</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        
                                        <td>New Active Captains - Over a Month</td>
										<td style="width: 50%;">{{ $captains[$city]->NewActiveCaptains }}</td>
                                    </tr>

                                    <tr style="text-align: center;">
                                       
                                        <td>New Inactive Captains - Over a Month</td>
										 <td style="width: 50%;">{{ $captains[$city]->NewNotActiveCaptains }}</td>
                                    </tr>

                                    <tr style="text-align: center;">
                                       
                                        <td>Old Active Captains - Over a Month</td>
										 <td style="width: 50%;">{{ $captains[$city]->OldActiveCaptains }}</td>
                                    </tr>

                                    <tr style="text-align: center;">
                                        
                                        <td>Old Inactive Captains - Over a Month</td>
										<td style="width: 50%;">{{ $captains[$city]->OldNotActiveCaptains }}</td>
                                    </tr>

                                    <tr style="text-align: center;">
                                        
                                        <td>Total Captains</td>
										<td style="width: 50%;">{{ $captains[$city]->TotalCaptains }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <hr/>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <p></p>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

@stop
