@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table th {
            text-align: center;
        } 
        table td {
            text-align: center;
            cursor: pointer;
        } 
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
    </style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/returnsreportpost">
                        <input type="hidden" name="tabFilter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Hub </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12">Undelivered Reason </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="undelivered_reason" id="undelivered_reason" class="form-control">
                                        <option value="all">All</option>
                                        @foreach($undeliveredReasons as $und_reason)
                                            <option value="{{$und_reason->id}}">{{$und_reason->english}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Attempts </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="number" name="attempts" id="attempts" class="form-control" value="{{ $attempts }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ $from }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ $to }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container" style="text-align: right;">

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content" style="text-align: left;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Details</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <table id="datatable_details" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Waybill</td>
                                            <td>In Route</td>
                                            <td>Attempts</td>
                                            <td>Undelivered Reason</td>
                                            <td>City</td>
                                            <td>Company</td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Waybill</td>
                                            <td>In Route</td>
                                            <td>Attempts</td>
                                            <td>Undelivered Reason</td>
                                            <td>City</td>
                                            <td>Company</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <table id="datatable_summary" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Count</th>
                                    <th>Attempts</th>
                                    <th>Undelivered Reason</th>
                                    <th>City</th>
                                    <th>Company</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Count</th>
                                    <th>Attempts</th>
                                    <th>Undelivered Reason</th>
                                    <th>City</th>
                                    <th>Company</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        

   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>
        function inquiry(primary_key) { // every click on any record
            $('#datatable_details').DataTable().destroy(); // clear the table in pop up window for current view
            var array = [];
            @foreach($details as $record)
                if('{{ $record->foreign_key }}' == primary_key)
                array.push(['{{ $record->jollychic }}', '{{ $record->new_shipment_date }}', '{{ $record->attempts }}', 
                    "{{ $record->english }}", "{{ $record->d_city }}", '{{ $companies_names[$record->company_id] }}']);
            @endforeach
            $("#datatable_details").dataTable({

                "data": array,
                "autoWidth": false,

                "pageLength": 10,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });
            $("#myModal").modal(); // show pop up window
        }
    </script>

    <script>

        <?php
        $summary = '';
        foreach($overview as $record) {
            $company_name = $companies_names[$record->company_id];
            $count = "<div onclick='inquiry($record->primary_key)'>$record->count</div>";
            $attempts = "<div onclick='inquiry($record->primary_key)'>$record->attempts</div>";
            $english = "<div onclick='inquiry($record->primary_key)'>$record->english</div>";
            $d_city = "<div onclick='inquiry($record->primary_key)'>$record->d_city</div>";
            $company = "<div onclick='inquiry($record->primary_key)'>$company_name</div>";
            $summary .= " [ \"$count\", \"$attempts\", \"$english\", \"$d_city\", \"$company\" ], ";
        }
        ?>
        $("#datatable_summary").dataTable({

            "data": [
                <?php echo $summary; ?>

            ],
            "autoWidth": false,

            "pageLength": 25,

            dom: "Blfrtip",
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                },
            ],
            responsive: true,

            'order': [[0, 'asc']]
        });
    </script>

@stop
