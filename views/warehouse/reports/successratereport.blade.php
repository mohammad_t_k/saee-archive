@if( Auth::user()->role < 4)
    <script>window.location = "/warehouse/403";</script>
@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')
@section('content')

    <style>
        th {
            text-align: center;
        }
        td {
            text-align: center;
        }
        .border-left {
            border-left: 2px solid #73879C !important;
        }
        .border-right {
            border-right: 2px solid #73879C !important;
        }
        .border-bottom {
            border-bottom: 2px solid #73879C !important;
        }
        .border-top {
            border-top: 2px solid #73879C !important;
        }
        .border-bottom-gray {
            border-bottom: 2px solid #DDDDDD !important;
        }
        .border-top-gray {
            border-top: 2px solid #DDDDDD !important;
        }
        .border-top-purple {
            border-top: 3px solid #9e42f5 !important;
        }
        .border-left-thick {
            border-left: 3px solid #73879C !important;
        }
        .border-right-thick {
            border-right: 3px solid #73879C !important;
        }
        .border-bottom-thick {
            border-bottom: 3px solid #73879C !important;
        }
        .border-top-thick { 
            border-top: 3px solid #73879C !important;
        }
        .border-all-thick {
            border: 3px solid #73879C !important;
        }
        .green {
            color: green !important;
        }
        .red {
            color: red !important;
        }
        .url:hover {    
            border-bottom: 1px solid #73879C !important;
            padding-bottom: 1px;
        }
        .hide {
            display: none;
        }
        .gray-background {
            background-color: #F9F9F9 !important;
        }
        .white-background {
            background-color: #FFFFFF !important;
        }
        body {
            color: black !important;
        }
    </style>

    <?php $sr_color = 75; ?>
    <?php ini_set('memory_limit','2048M'); ?> <!-- daysrow totals arrays takes too much memory -->

    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- - Each section is divided in three parts:  -->
    <!-- 1) the attempts calculation with the calculation adjustments  -->
    <!-- 2) the daysrow calculation  -->
    <!-- 3) the companies calculations with attempts first then dayseow  -->
    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="get" action="/warehouse/successratereport">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="company" name="companies[]" required="required" class="form-control" type="text" multiple>
                                        <option value="71" <?php if (!isset($companies) || isset($companies) && in_array(71, $companies)) echo 'selected';?> >JollyChic Group</option>
                                        <option value="73" <?php if (!isset($companies) || isset($companies) && in_array(73, $companies)) echo 'selected';?> >JollyChic Local</option>
                                        <option value="74" <?php if (!isset($companies) || isset($companies) && in_array(74, $companies)) echo 'selected';?> >JollyChic Global</option>
                                        @foreach($activeCompanies as $compan)
                                            <option value="{{$compan->id}}" <?php if (isset($companies) && in_array($compan->id, $companies)) echo 'selected';?> >{{ $compan->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="city" name="cities[]" class="form-control" multiple>
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city_id}}" <?php if (isset($cities) && in_array($adcity->city_id, $cities)) echo 'selected';?> >{{ $adcity->city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Hub</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="hub" name="hubs[]" class="form-control" multiple>
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hubs) && in_array($adhub->hub_id, $hubs)) echo 'selected';?>>{{ $allHubs[$adhub->hub_id]->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" value="{{ $from }}" class="form-control col-xs-12" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" value="{{ $to }}" class="form-control col-xs-12" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <p>
                                        <button id="submitForm" class="btn btn-success">Filter</button>
                                    </p>
                                </div>

                                <div class="container" style="text-align: right;">
                                    <!-- Trigger the modal with a button -->
                                    <a href="#myModal" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-info-sign" style="margin-top: 10px; margin-right: 10px; font-size: 30px;"></span></a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content" style="text-align: left;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Help</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <b>Note</b>: All numbers are calculated starting from the first recorded In Route Date which is 2018-03-27.<br/><br/>
                                                    <b>Table Legends:</b>
                                                    <hr>
                                                    <b>- Total Picked Up:</b> is the total number of shipments picked up from origin according to the filter values.
                                                    <br/><br/><b>- Total Signed In:</b> is the total number of shipments signed-in to our warehouses.
                                                    <br/><br/><b>- Average Days To First Dispatch:</b> is the average number of days from the pickup date until the first attempt.
                                                    <br/><br/><b>- 0 Attempts <span style="color: red; font-weight: bold;">(Clickable)</span>:</b> is the total number of shipments that has been picked-up from origin and never attempted delivery (in hubs tables it is calculated from signed-in shipments only).
                                                    <hr>
                                                    <b>- 1<sup>st</sup> Attempt On 1<sup>st</sup> Day:</b> is the total number of first-attempt that has been done in the same pickup date.
                                                    <br/><br/><b>- 1<sup>st</sup> Attempt On 2<sup>nd</sup> Day:</b> is the total number of first-attempt that has been done in the next day of the pickup date.
                                                    <br/><br/><b>- 1<sup>st</sup> Attempt On 3<sup>rd</sup> Day:</b> is the total number of first-attempt that has been done in the third day of the pickup date.
                                                    <br/><br/><b>- Total First Attempt After 3 Days:</b> is the total number of first-attempt that has been done in more than 3 days after the pickup date.
                                                    <br/><br/><b>- Total First Attempt Over All Days:</b> is the total number of first-attempt that has been done.
                                                    <hr>
                                                    <!-- ------------------------------- Decreasing Higher Attempts -------------------------- --> 
                                                    <b>- At Least 1 Attempt:</b> is the total number of shipments that has been signed-in into warehouse and attempted delivery at least once.
                                                    <br/><br/><b>- At Least 2 Attempts:</b> is the total number of shipments that has been signed-in into warehouse and attempted delivery at least twice.
                                                    <br/><br/><b>- At Least 3 Attempts:</b> is the total number of shipments that has been signed-in into warehouse and attempted delivery at least thrice.
                                                    <br/><br/><b>- At Least 4 Attempts:</b> is the total number of shipments that has been signed-in into warehouse and attempted delivery at least 4 times.
                                                    <br/><br/><b>- At Least 5 Attempts:</b> is the total number of shipments that has been signed-in into warehouse and attempted delivery at least 5 times.
                                                    <hr>
                                                    <b>- Dis. S.R. (Dispatched Success Rate):</b> is the delivered shipments divided by dispatched shipments multiplied by 100 out of dispatched shipments. The Perc. is colored by <span style="color: green;">green</span> if success rate is greater than {{ $sr_color }}% and by  <span style="color: red;">red</span> if success rate is less than {{ $sr_color }}%.
                                                    <br/><br/><b>- Pic. S.R. (Picked-Up Success Rate):</b> is the delivered shipments divided by dispatched shipments multiplied by 100 out of all picked-up shipments in this row. The Perc. is colored by <span style="color: green;">green</span> if success rate is greater than {{ $sr_color }}% and by  <span style="color: red;">red</span> if success rate is less than {{ $sr_color }}%.
                                                    <br/><br/><b>- Signed. S.R. (Signed-In Success Rate):</b> is the delivered shipments divided by dispatched shipments multiplied by 100 out of all signed-in shipments to specific hub. The Perc. is colored by <span style="color: green;">green</span> if success rate is greater than {{ $sr_color }}% and by  <span style="color: red;">red</span> if success rate is less than {{ $sr_color }}%.
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php
        function get_index_dis($num_faild_delivery_attempts, $status) {
            $index = -1;
            if($num_faild_delivery_attempts > 4 && $status <> 5 || $num_faild_delivery_attempts == 4 && $status <> 5) {
                $index = 4;
            }
            else if($num_faild_delivery_attempts == 3 && $status <> 5) {
                $index = 3;
            }
            else if($num_faild_delivery_attempts == 2 && $status <> 5) {
                $index = 2;
            }
            else if($num_faild_delivery_attempts == 1 && $status <> 5) {
                $index = 1;
            }
            else if($num_faild_delivery_attempts == 0 && $status <> 5) {
                $index = 0;
            }
            else {
                // echo 'attempts: ' . $num_faild_delivery_attempts . '<br/>';
                // echo 'status: ' . $status . '<br/>';
                // die();
            }
            return $index;
        }
        function get_index_del($num_faild_delivery_attempts, $status) {
            $index = -1;
            if($num_faild_delivery_attempts == 0 && $status == 5) {
                $index = 0;
            }
            else if($num_faild_delivery_attempts == 1 && $status == 5) {
                $index = 1;
            }
            else if($num_faild_delivery_attempts == 2 && $status == 5) {
                $index = 2;
            }
            else if($num_faild_delivery_attempts == 3 && $status == 5) {
                $index = 3;
            }
            else if($num_faild_delivery_attempts >= 4 && $status == 5) {
                $index = 4;
            }
            else {
                // echo 'attempts: ' . $num_faild_delivery_attempts . '<br/>';
                // echo 'status: ' . $status . '<br/>';
                // die();
            }
            return $index;
        }

        // set up link prefix for export shipments
        $all_companies_str = implode($companies, ',');
        $all_cities_str = implode($cities, ',');
        $all_hubs_str = implode($hubs, ',');
        $export_zero_attempts_link = "/warehouse/successratereport/export?type=0"; // type: 0 = zero attempts
        ?>

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel overflow_hidden table-responsive">
                    <?php

                    //////////////////////////////////////////////////// ---------------------------- separator (attempts) ---------------------------- ////////////////////////////////////////////////////

                    $total_pickedup             = 0;
                    $total_signedin             = 0;
                    $total_avg_dispatched_days  = 0;
                    $total_avg_dispatched_count = 0;
                    $total_zero_attempts        = 0;
                    $total_attempts_dis         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                    $total_attempts_del         = array(0,0,0,0,0); 

                    foreach($cities as $one_city) {
                        $citytotal_pickedup[$one_city]             = 0;
                        $citytotal_signedin[$one_city]             = 0;
                        $citytotal_avg_dispatched_days[$one_city]  = 0;
                        $citytotal_avg_dispatched_count[$one_city] = 0;
                        $citytotal_zero_attempts[$one_city]        = 0;
                        $citytotal_attempts_dis[$one_city]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                        $citytotal_attempts_del[$one_city]         = array(0,0,0,0,0); 
                    }

                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $hubtotal_pickedup[$one_hub]             = 0;
                        $hubtotal_signedin[$one_hub]             = 0;
                        $hubtotal_avg_dispatched_days[$one_hub]  = 0;
                        $hubtotal_avg_dispatched_count[$one_hub] = 0;
                        $hubtotal_zero_attempts[$one_hub]        = 0;
                        $hubtotal_attempts_dis[$one_hub]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                        $hubtotal_attempts_del[$one_hub]         = array(0,0,0,0,0); 
                    }
                    array_pop($hubs);

                    /////////////////////
                    // calculate values for total of totals
                    foreach($result as $one_result) {

                        ///////////////////
                        // Total Of Totals
                        $total_pickedup += $one_result->pickedup;
                        $total_signedin += $one_result->signedin;

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $total_avg_dispatched_days += $one_result->avg_dispatched * $one_result->dispatched;
                            $total_avg_dispatched_count += $one_result->dispatched;
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $total_attempts_dis[$i] += $one_result->dispatched;
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $total_attempts_del[$i] += $one_result->delivered;
                        }
                        ///////////////////

                        ///////////////////
                        // Total Of Totals By City
                        $citytotal_pickedup[$one_result->main_city_id] += $one_result->pickedup;
                        $citytotal_signedin[$one_result->main_city_id] += $one_result->signedin;

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $citytotal_avg_dispatched_days[$one_result->main_city_id] += $one_result->avg_dispatched * $one_result->dispatched;
                            $citytotal_avg_dispatched_count[$one_result->main_city_id] += $one_result->dispatched;
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $citytotal_attempts_dis[$one_result->main_city_id][$i] += $one_result->dispatched;
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $citytotal_attempts_del[$one_result->main_city_id][$i] += $one_result->delivered;
                        }
                        ///////////////////

                        ///////////////////
                        // Total Of Totals By Hub
                        $hubtotal_pickedup[$one_result->hub_id] += $one_result->pickedup;
                        $hubtotal_signedin[$one_result->hub_id] += $one_result->signedin;

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $hubtotal_avg_dispatched_days[$one_result->hub_id] += $one_result->avg_dispatched * $one_result->dispatched;
                            $hubtotal_avg_dispatched_count[$one_result->hub_id] += $one_result->dispatched;
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $hubtotal_attempts_dis[$one_result->hub_id][$i] += $one_result->dispatched;
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $hubtotal_attempts_del[$one_result->hub_id][$i] += $one_result->delivered;
                        }
                        ///////////////////

                    }
                    ///////////////////// end of calculation 

                    ///////////////////// 
                    // adjust calculations for total of totals 
                    for($i = 0; $i < 5; ++$i) {
                        $total_attempts_dis[$i] += $total_attempts_del[$i];
                    }
                    $total_pickedup += $total_signedin;
                    $total_zero_attempts = $total_pickedup - $total_attempts_dis[0];

                    foreach($cities as $one_city) {
                        for($i = 0; $i < 5; ++$i) {
                            $citytotal_attempts_dis[$one_city][$i] += $citytotal_attempts_del[$one_city][$i];
                        }
                        $citytotal_pickedup[$one_city] += $citytotal_signedin[$one_city];
                        $citytotal_zero_attempts[$one_city] = $citytotal_pickedup[$one_city] - $citytotal_attempts_dis[$one_city][0];
                    }

                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        for($i = 0; $i < 5; ++$i) {
                            $hubtotal_attempts_dis[$one_hub][$i] += $hubtotal_attempts_del[$one_hub][$i];
                        }
                        $hubtotal_zero_attempts[$one_hub] = $hubtotal_signedin[$one_hub] - $hubtotal_attempts_dis[$one_hub][0];
                    }
                    array_pop($hubs);
                    ///////////////////// end of adjustment 

                    //////////////////////////////////////////////////// ---------------------------- separator (daysrow) ---------------------------- ////////////////////////////////////////////////////

                    // calculate the days row in the totals table
                    $daysrowtotal_dis = array(0, 0, 0, 0, 0); // 0: 1st day (same pickup day) , 1: 2nd day, 3: 3rd day, 4: total days
                    $daysrowtotal_del = array(0, 0, 0, 0, 0);

                    foreach($cities as $one_city) {
                        $citydaysrowtotal_dis[$one_city] = array(0, 0, 0, 0, 0);
                        $citydaysrowtotal_del[$one_city] = array(0, 0, 0, 0, 0);
                    }

                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $hubdaysrowtotal_dis[$one_hub] = array(0, 0, 0, 0, 0);
                        $hubdaysrowtotal_del[$one_hub] = array(0, 0, 0, 0, 0);
                    }
                    array_pop($hubs);

                    foreach($daysrow_result as $one_result) {
                        ////////////////////////////
                        /// total
                        $daysrowtotal_dis[0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $daysrowtotal_dis[1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $daysrowtotal_dis[2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $daysrowtotal_dis[3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $daysrowtotal_dis[4] += $one_result->dispatched; 

                        $daysrowtotal_del[0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $daysrowtotal_del[1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $daysrowtotal_del[2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $daysrowtotal_del[3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $daysrowtotal_del[4] += $one_result->delivered; 
                        ////////////////////////////

                        ////////////////////////////
                        /// total by city
                        $citydaysrowtotal_dis[$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $citydaysrowtotal_dis[$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $citydaysrowtotal_dis[$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $citydaysrowtotal_dis[$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $citydaysrowtotal_dis[$one_result->main_city_id][4] += $one_result->dispatched; 

                        $citydaysrowtotal_del[$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $citydaysrowtotal_del[$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $citydaysrowtotal_del[$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $citydaysrowtotal_del[$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $citydaysrowtotal_del[$one_result->main_city_id][4] += $one_result->delivered; 
                        ////////////////////////////

                        ////////////////////////////
                        /// total by hub
                        $hubdaysrowtotal_dis[$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $hubdaysrowtotal_dis[$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $hubdaysrowtotal_dis[$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $hubdaysrowtotal_dis[$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $hubdaysrowtotal_dis[$one_result->hub_id][4] += $one_result->dispatched; 

                        $hubdaysrowtotal_del[$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $hubdaysrowtotal_del[$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $hubdaysrowtotal_del[$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $hubdaysrowtotal_del[$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $hubdaysrowtotal_del[$one_result->hub_id][4] += $one_result->delivered; 
                        ////////////////////////////

                    }

                    //////////////////////////////////////////////////// ---------------------------- separator (companies: attempts and daysrow) ---------------------------- ////////////////////////////////////////////////////

                    // initialize arrays
                    if(in_array('71', $companies_init)) {
                        $counter = 0;
                        foreach($jollychic_group as $one_item) {
                            if(!in_array($one_item, $companies_init)) {
                                array_push($companies_init, $one_item);
                                ++$counter;
                            }
                        }
                    }
                    if(in_array('73', $companies_init)) {
                        $counter = 0;
                        foreach($jollychic_local as $one_item) {
                            if(!in_array($one_item, $companies_init)) {
                                array_push($companies_init, $one_item);
                                ++$counter;
                            }
                        }
                    }
                    if(in_array('74', $companies_init)) {
                        $counter = 0;
                        foreach($jollychic_global as $one_item) {
                            if(!in_array($one_item, $companies_init)) {
                                array_push($companies_init, $one_item);
                                ++$counter;
                            }
                        }
                    }
                    foreach($companies_init as $one_company) {

                        ///////////////////////////
                        // daysrow
                        $companiesdaysrowtotal_dis[$one_company]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                        $companiesdaysrowtotal_del[$one_company]         = array(0,0,0,0,0); 
    
                        foreach($cities as $one_city) {
                            $citycompaniesdaysrowtotal_dis[$one_company][$one_city] = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                            $citycompaniesdaysrowtotal_del[$one_company][$one_city] = array(0,0,0,0,0); 
                        }
    
                        array_push($hubs, 0);
                        foreach($hubs as $one_hub) {
                            $hubcompaniesdaysrowtotal_dis[$one_company][$one_hub] = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                            $hubcompaniesdaysrowtotal_del[$one_company][$one_hub] = array(0,0,0,0,0); 
                        }
                        array_pop($hubs);
                        ///////////////////////////
                        
                        ///////////////////////////
                        // attempts
                        $companiestotal_pickedup[$one_company]             = 0;
                        $companiestotal_signedin[$one_company]             = 0;
                        $companiestotal_avg_dispatched_days[$one_company]  = 0;
                        $companiestotal_avg_dispatched_count[$one_company] = 0;
                        $companiestotal_zero_attempts[$one_company]        = 0;
                        $companiestotal_attempts_dis[$one_company]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                        $companiestotal_attempts_del[$one_company]         = array(0,0,0,0,0); 
    
                        foreach($cities as $one_city) {
                            $citycompaniestotal_pickedup[$one_company][$one_city]             = 0;
                            $citycompaniestotal_signedin[$one_company][$one_city]             = 0;
                            $citycompaniestotal_avg_dispatched_days[$one_company][$one_city]  = 0;
                            $citycompaniestotal_avg_dispatched_count[$one_company][$one_city] = 0;
                            $citycompaniestotal_zero_attempts[$one_company][$one_city]        = 0;
                            $citycompaniestotal_attempts_dis[$one_company][$one_city]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                            $citycompaniestotal_attempts_del[$one_company][$one_city]         = array(0,0,0,0,0); 
                        }
    
                        array_push($hubs, 0);
                        foreach($hubs as $one_hub) {
                            $hubcompaniestotal_pickedup[$one_company][$one_hub]             = 0;
                            $hubcompaniestotal_signedin[$one_company][$one_hub]             = 0;
                            $hubcompaniestotal_avg_dispatched_days[$one_company][$one_hub]  = 0;
                            $hubcompaniestotal_avg_dispatched_count[$one_company][$one_hub] = 0;
                            $hubcompaniestotal_zero_attempts[$one_company][$one_hub]        = 0;
                            $hubcompaniestotal_attempts_dis[$one_company][$one_hub]         = array(0,0,0,0,0); // 0: 1st attempt, 1: 2nd attempt, 3: 3rd attempt, 4: At Least 5 Attempts 
                            $hubcompaniestotal_attempts_del[$one_company][$one_hub]         = array(0,0,0,0,0); 
                        }
                        array_pop($hubs);
                        ///////////////////////////

                    }
                    // remove in the same order
                    if(in_array('74', $companies_init)) {
                        foreach($jollychic_global as $one_item) {
                            if(!$counter) break;
                            array_pop($companies_init);
                            --$counter;
                        }
                    }
                    if(in_array('73', $companies_init)) {
                        foreach($jollychic_local as $one_item) {
                            if(!$counter) break;
                            array_pop($companies_init);
                            --$counter;
                        }
                    }
                    if(in_array('71', $companies_init)) {
                        foreach($jollychic_group as $one_item) {
                            if(!$counter) break;
                            array_pop($companies_init);
                            --$counter;
                        }
                    }

                    /////////////////////
                    // calculate values for companies total of totals
                    foreach($result as $one_result) {

                        ///////////////////
                        // Total Of Totals
                        $companiestotal_pickedup[$one_result->company_id] += $one_result->pickedup;
                        $companiestotal_signedin[$one_result->company_id] += $one_result->signedin;
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $companiestotal_pickedup['71'] += in_array($one_result->company_id, $jollychic_group) * $one_result->pickedup;
                            $companiestotal_signedin['71'] += in_array($one_result->company_id, $jollychic_group) * $one_result->signedin;
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $companiestotal_pickedup['73'] += in_array($one_result->company_id, $jollychic_local) * $one_result->pickedup;
                            $companiestotal_signedin['73'] += in_array($one_result->company_id, $jollychic_local) * $one_result->signedin;
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $companiestotal_pickedup['74'] += in_array($one_result->company_id, $jollychic_global) * $one_result->pickedup;
                            $companiestotal_signedin['74'] += in_array($one_result->company_id, $jollychic_global) * $one_result->signedin;
                        }

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $companiestotal_avg_dispatched_days[$one_result->company_id] += $one_result->avg_dispatched * $one_result->dispatched;
                            $companiestotal_avg_dispatched_count[$one_result->company_id] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $companiestotal_avg_dispatched_days['71'] += $one_result->avg_dispatched * $one_result->dispatched;
                                $companiestotal_avg_dispatched_count['71'] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $companiestotal_avg_dispatched_days['73'] += $one_result->avg_dispatched * $one_result->dispatched;
                                $companiestotal_avg_dispatched_count['73'] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $companiestotal_avg_dispatched_days['74'] += $one_result->avg_dispatched * $one_result->dispatched;
                                $companiestotal_avg_dispatched_count['74'] += $one_result->dispatched;
                            }
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $companiestotal_attempts_dis[$one_result->company_id][$i] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $companiestotal_attempts_dis['71'][$i] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $companiestotal_attempts_dis['73'][$i] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $companiestotal_attempts_dis['74'][$i] += $one_result->dispatched;
                            }
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $companiestotal_attempts_del[$one_result->company_id][$i] += $one_result->delivered;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $companiestotal_attempts_del['71'][$i] += $one_result->delivered;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $companiestotal_attempts_del['73'][$i] += $one_result->delivered;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $companiestotal_attempts_del['74'][$i] += $one_result->delivered;
                            }
                        }
                        ///////////////////

                        ///////////////////
                        // Total Of Totals By City
                        $citycompaniestotal_pickedup[$one_result->company_id][$one_result->main_city_id] += $one_result->pickedup;
                        $citycompaniestotal_signedin[$one_result->company_id][$one_result->main_city_id] += $one_result->signedin;
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $citycompaniestotal_pickedup['71'][$one_result->main_city_id] += $one_result->pickedup;
                            $citycompaniestotal_signedin['71'][$one_result->main_city_id] += $one_result->signedin;
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $citycompaniestotal_pickedup['73'][$one_result->main_city_id] += $one_result->pickedup;
                            $citycompaniestotal_signedin['73'][$one_result->main_city_id] += $one_result->signedin;
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $citycompaniestotal_pickedup['74'][$one_result->main_city_id] += $one_result->pickedup;
                            $citycompaniestotal_signedin['74'][$one_result->main_city_id] += $one_result->signedin;
                        }

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $citycompaniestotal_avg_dispatched_days[$one_result->company_id][$one_result->main_city_id] += $one_result->avg_dispatched * $one_result->dispatched;
                            $citycompaniestotal_avg_dispatched_count[$one_result->company_id][$one_result->main_city_id] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $citycompaniestotal_avg_dispatched_days['71'][$one_result->main_city_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $citycompaniestotal_avg_dispatched_count['71'][$one_result->main_city_id] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $citycompaniestotal_avg_dispatched_days['73'][$one_result->main_city_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $citycompaniestotal_avg_dispatched_count['73'][$one_result->main_city_id] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $citycompaniestotal_avg_dispatched_days['74'][$one_result->main_city_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $citycompaniestotal_avg_dispatched_count['74'][$one_result->main_city_id] += $one_result->dispatched;
                            }
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $citycompaniestotal_attempts_dis[$one_result->company_id][$one_result->main_city_id][$i] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $citycompaniestotal_attempts_dis['71'][$one_result->main_city_id][$i] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $citycompaniestotal_attempts_dis['73'][$one_result->main_city_id][$i] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $citycompaniestotal_attempts_dis['74'][$one_result->main_city_id][$i] += $one_result->dispatched;
                            }
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $citycompaniestotal_attempts_del[$one_result->company_id][$one_result->main_city_id][$i] += $one_result->delivered;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $citycompaniestotal_attempts_del['71'][$one_result->main_city_id][$i] += $one_result->delivered;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $citycompaniestotal_attempts_del['73'][$one_result->main_city_id][$i] += $one_result->delivered;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $citycompaniestotal_attempts_del['74'][$one_result->main_city_id][$i] += $one_result->delivered;
                            }
                        }
                        ///////////////////

                        ///////////////////
                        // Total Of Totals By Hub
                        $hubcompaniestotal_pickedup[$one_result->company_id][$one_result->hub_id] += $one_result->pickedup;
                        $hubcompaniestotal_signedin[$one_result->company_id][$one_result->hub_id] += $one_result->signedin;
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $hubcompaniestotal_pickedup['71'][$one_result->hub_id] += $one_result->pickedup;
                            $hubcompaniestotal_signedin['71'][$one_result->hub_id] += $one_result->signedin;
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $hubcompaniestotal_pickedup['73'][$one_result->hub_id] += $one_result->pickedup;
                            $hubcompaniestotal_signedin['73'][$one_result->hub_id] += $one_result->signedin;
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $hubcompaniestotal_pickedup['74'][$one_result->hub_id] += $one_result->pickedup;
                            $hubcompaniestotal_signedin['74'][$one_result->hub_id] += $one_result->signedin;
                        }

                        // calculate average days to first dispatch
                        if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                            $hubcompaniestotal_avg_dispatched_days[$one_result->company_id][$one_result->hub_id] += $one_result->avg_dispatched * $one_result->dispatched;
                            $hubcompaniestotal_avg_dispatched_count[$one_result->company_id][$one_result->hub_id] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $hubcompaniestotal_avg_dispatched_days['71'][$one_result->hub_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $hubcompaniestotal_avg_dispatched_count['71'][$one_result->hub_id] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $hubcompaniestotal_avg_dispatched_days['73'][$one_result->hub_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $hubcompaniestotal_avg_dispatched_count['73'][$one_result->hub_id] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $hubcompaniestotal_avg_dispatched_days['74'][$one_result->hub_id] += $one_result->avg_dispatched * $one_result->dispatched;
                                $hubcompaniestotal_avg_dispatched_count['74'][$one_result->hub_id] += $one_result->dispatched;
                            }
                        }

                        $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                        $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                        for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                            $hubcompaniestotal_attempts_dis[$one_result->company_id][$one_result->hub_id][$i] += $one_result->dispatched;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $hubcompaniestotal_attempts_dis['71'][$one_result->hub_id][$i] += $one_result->dispatched;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $hubcompaniestotal_attempts_dis['73'][$one_result->hub_id][$i] += $one_result->dispatched;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $hubcompaniestotal_attempts_dis['74'][$one_result->hub_id][$i] += $one_result->dispatched;
                            }
                        }
                        for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                            $hubcompaniestotal_attempts_del[$one_result->company_id][$one_result->hub_id][$i] += $one_result->delivered;
                            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                                $hubcompaniestotal_attempts_del['71'][$one_result->hub_id][$i] += $one_result->delivered;
                            }
                            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                                $hubcompaniestotal_attempts_del['73'][$one_result->hub_id][$i] += $one_result->delivered;
                            }
                            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                                $hubcompaniestotal_attempts_del['74'][$one_result->hub_id][$i] += $one_result->delivered;
                            }
                        }
                        ///////////////////

                    }
                    ///////////////////// end of companies calculation 

                    ///////////////////// 
                    // adjust calculations for companies total of totals 
                    foreach($companies as $one_comapny) {
                        
                        for($i = 0; $i < 5; ++$i) {
                            $companiestotal_attempts_dis[$one_comapny][$i] += $companiestotal_attempts_del[$one_comapny][$i];
                        }
                        $companiestotal_pickedup[$one_comapny] += $companiestotal_signedin[$one_comapny];
                        $companiestotal_zero_attempts[$one_comapny] = $companiestotal_pickedup[$one_comapny] - $companiestotal_attempts_dis[$one_comapny][0];
    
                        foreach($cities as $one_city) {
                            for($i = 0; $i < 5; ++$i) {
                                $citycompaniestotal_attempts_dis[$one_comapny][$one_city][$i] += $citycompaniestotal_attempts_del[$one_comapny][$one_city][$i];
                            }
                            $citycompaniestotal_pickedup[$one_comapny][$one_city] += $citycompaniestotal_signedin[$one_comapny][$one_city];
                            $citycompaniestotal_zero_attempts[$one_comapny][$one_city] = $citycompaniestotal_pickedup[$one_comapny][$one_city] - $citycompaniestotal_attempts_dis[$one_comapny][$one_city][0];
                        }
    
                        array_push($hubs, 0);
                        foreach($hubs as $one_hub) {
                            for($i = 0; $i < 5; ++$i) {
                                $hubcompaniestotal_attempts_dis[$one_comapny][$one_hub][$i] += $hubcompaniestotal_attempts_del[$one_comapny][$one_hub][$i];
                            }
                            $hubcompaniestotal_zero_attempts[$one_comapny][$one_hub] = $hubcompaniestotal_signedin[$one_comapny][$one_hub] - $hubcompaniestotal_attempts_dis[$one_comapny][$one_hub][0];
                        }
                        array_pop($hubs);
                    }
                    ///////////////////// end of adjustment 

                    foreach($daysrow_result as $one_result) {
                        ////////////////////////////
                        /// total
                        $companiesdaysrowtotal_dis[$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $companiesdaysrowtotal_dis[$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $companiesdaysrowtotal_dis[$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $companiesdaysrowtotal_dis[$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $companiesdaysrowtotal_dis[$one_result->company_id][4] += $one_result->dispatched; 

                        $companiesdaysrowtotal_del[$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $companiesdaysrowtotal_del[$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $companiesdaysrowtotal_del[$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $companiesdaysrowtotal_del[$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $companiesdaysrowtotal_del[$one_result->company_id][4] += $one_result->delivered; 
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $companiesdaysrowtotal_dis['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['71'][4] += $one_result->dispatched; 
    
                            $companiesdaysrowtotal_del['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['71'][4] += $one_result->delivered; 
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $companiesdaysrowtotal_dis['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['73'][4] += $one_result->dispatched; 
    
                            $companiesdaysrowtotal_del['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['73'][4] += $one_result->delivered; 
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $companiesdaysrowtotal_dis['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $companiesdaysrowtotal_dis['74'][4] += $one_result->dispatched; 
    
                            $companiesdaysrowtotal_del['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $companiesdaysrowtotal_del['74'][4] += $one_result->delivered; 
                        }
                        ////////////////////////////

                        ////////////////////////////
                        /// total by city
                        $citycompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $citycompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $citycompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $citycompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $citycompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->main_city_id][4] += $one_result->dispatched; 

                        $citycompaniesdaysrowtotal_del[$one_result->company_id][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $citycompaniesdaysrowtotal_del[$one_result->company_id][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $citycompaniesdaysrowtotal_del[$one_result->company_id][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $citycompaniesdaysrowtotal_del[$one_result->company_id][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $citycompaniesdaysrowtotal_del[$one_result->company_id][$one_result->main_city_id][4] += $one_result->delivered; 
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $citycompaniesdaysrowtotal_dis['71'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['71'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['71'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['71'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['71'][$one_result->main_city_id][4] += $one_result->dispatched; 
    
                            $citycompaniesdaysrowtotal_del['71'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['71'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['71'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['71'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['71'][$one_result->main_city_id][4] += $one_result->delivered; 
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $citycompaniesdaysrowtotal_dis['73'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['73'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['73'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['73'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['73'][$one_result->main_city_id][4] += $one_result->dispatched; 
    
                            $citycompaniesdaysrowtotal_del['73'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['73'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['73'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['73'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['73'][$one_result->main_city_id][4] += $one_result->delivered; 
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $citycompaniesdaysrowtotal_dis['74'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['74'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['74'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['74'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $citycompaniesdaysrowtotal_dis['74'][$one_result->main_city_id][4] += $one_result->dispatched; 
    
                            $citycompaniesdaysrowtotal_del['74'][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['74'][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['74'][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['74'][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $citycompaniesdaysrowtotal_del['74'][$one_result->main_city_id][4] += $one_result->delivered; 
                        }
                        ////////////////////////////

                        ////////////////////////////
                        /// total by hub
                        $hubcompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                        $hubcompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                        $hubcompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                        $hubcompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                        $hubcompaniesdaysrowtotal_dis[$one_result->company_id][$one_result->hub_id][4] += $one_result->dispatched; 

                        $hubcompaniesdaysrowtotal_del[$one_result->company_id][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                        $hubcompaniesdaysrowtotal_del[$one_result->company_id][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                        $hubcompaniesdaysrowtotal_del[$one_result->company_id][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                        $hubcompaniesdaysrowtotal_del[$one_result->company_id][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                        $hubcompaniesdaysrowtotal_del[$one_result->company_id][$one_result->hub_id][4] += $one_result->delivered; 
                        if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                            $hubcompaniesdaysrowtotal_dis['71'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['71'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['71'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['71'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['71'][$one_result->hub_id][4] += $one_result->dispatched; 
    
                            $hubcompaniesdaysrowtotal_del['71'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['71'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['71'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['71'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['71'][$one_result->hub_id][4] += $one_result->delivered; 
                        }
                        if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                            $hubcompaniesdaysrowtotal_dis['73'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['73'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['73'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['73'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['73'][$one_result->hub_id][4] += $one_result->dispatched; 
    
                            $hubcompaniesdaysrowtotal_del['73'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['73'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['73'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['73'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['73'][$one_result->hub_id][4] += $one_result->delivered; 
                        }
                        if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                            $hubcompaniesdaysrowtotal_dis['74'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['74'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['74'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['74'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                            $hubcompaniesdaysrowtotal_dis['74'][$one_result->hub_id][4] += $one_result->dispatched; 
    
                            $hubcompaniesdaysrowtotal_del['74'][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['74'][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['74'][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['74'][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                            $hubcompaniesdaysrowtotal_del['74'][$one_result->hub_id][4] += $one_result->delivered; 
                        }
                        ////////////////////////////
                    }

                    ?>

                    <!-- Total of Totals -->
                    <div style="text-align: right;">
                        <button class="btn btn-primary" onclick="total_click()" id="total_button"> Show Companies</button>
                    </div>
                    <div style="overflow-x:auto; overflow-y:auto;">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th colspan="27" class="border-all-thick">Total of All Months</th>
                                </tr>
                                <tr>
                                    <th rowspan="4" class="border-left-thick"></th>
                                    <th rowspan="4" class="border-left">Total Picked Up</th>
                                    <th rowspan="3" colspan="2" class="border-left">Total Signed In</th>
                                    <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                    <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                    @for($i = 0; $i < 5; ++$i)
                                        <th class="border-left">Dis.</th>
                                        <th>Del.</th>
                                        <th>Dis. S.R.</th>
                                        <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                    @endfor
                                </tr>
                                <tr>
                                    <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                    <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                    <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                    <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                    <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                </tr>
                                <tr>
                                    @for($i = 0; $i < 5; ++$i)
                                        <td class="gray-background border-left">{{ number_format($daysrowtotal_dis[$i]) }}</td>
                                        <td class="gray-background">{{ number_format($daysrowtotal_del[$i]) }}</td>
                                        <td class="gray-background {{ $daysrowtotal_dis[$i] > 0 ? ($daysrowtotal_del[$i] / $daysrowtotal_dis[$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daysrowtotal_del[$i] / max(1, $daysrowtotal_dis[$i]) * 100.0, 2) }}%</td>
                                        <td class="gray-background {{ $total_pickedup > 0 ? ($daysrowtotal_del[$i] / $total_pickedup * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($daysrowtotal_del[$i] / max(1, $total_pickedup) * 100.0, 2) }}%</td>
                                    @endfor
                                </tr>
                                <tr>
                                    <th class="border-left">Count</th>
                                    <th>Perc.</th>
                                    <th class="border-left">Count</th>
                                    <th>Perc.</th>
                                    <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                    <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                    <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                    <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                    <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border-left-thick border-bottom-thick"></td>
                                    <td class="border-left border-bottom-thick">{{ number_format($total_pickedup) }}</td>
                                    <td class="border-left border-bottom-thick">{{ number_format($total_signedin) }}</td>
                                    <td class="border-bottom-thick">{{ number_format($total_signedin / max(1, $total_pickedup) * 100.0, 2) }}%</td>
                                    <td class="border-left border-bottom-thick">{{ number_format(1.0 * $total_avg_dispatched_days / max(1, $total_avg_dispatched_count), 2) }}</td>
                                    <td class="border-left border-bottom-thick">
                                        <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $from }}&to={{ $to }}">
                                            {{ number_format($total_zero_attempts) }}
                                        </a>
                                    </td>
                                    <td class="border-bottom-thick">{{ number_format($total_zero_attempts / max(1, $total_pickedup) * 100.0, 2) }}%</td>
                                    @for($i = 0; $i < 5; ++$i)
                                        <td class="border-left border-bottom-thick">{{ number_format($total_attempts_dis[$i]) }}</td>
                                        <td class="border-bottom-thick">{{ number_format($total_attempts_del[$i]) }}</td>
                                        <td class="{{ $total_attempts_dis[$i] > 0 ? ($total_attempts_del[$i] / $total_attempts_dis[$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($total_attempts_del[$i] / max(1, $total_attempts_dis[$i]) * 100.0, 2) }}%</td>
                                        <td class="{{ $total_pickedup > 0 ? ($total_attempts_del[$i] / $total_pickedup * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($total_attempts_del[$i] / max(1, $total_pickedup) * 100.0, 2) }}%</td>
                                    @endfor
                                </tr>      
                                <tr class="total-show-hide" style="display: none;">
                                    <th colspan="27" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                </tr>
                                @foreach($companies as $one_company) 
                                    <tr class="total-show-hide" style="display: none;">
                                        <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                        <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($companiestotal_pickedup[$one_company]) }}</td>
                                        <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($companiestotal_signedin[$one_company]) }}</td>
                                        <td rowspan="4" class="border-top border-bottom">{{ number_format($companiestotal_signedin[$one_company] / max(1, $companiestotal_pickedup[$one_company]) * 100.0, 2) }}%</td>
                                        <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $companiestotal_avg_dispatched_days[$one_company] / max(1, $companiestotal_avg_dispatched_count[$one_company]), 2) }}</td>
                                        <td rowspan="4" class="border-top border-left border-bottom">
                                            <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $from }}&to={{ $to }}">
                                                {{ number_format($companiestotal_zero_attempts[$one_company]) }}
                                            </a>
                                        </td>
                                        <td rowspan="4" class="border-top border-bottom">{{ number_format($companiestotal_zero_attempts[$one_company] / max(1, $companiestotal_pickedup[$one_company]) * 100.0, 2) }}%</td>
                                        <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                        <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                        <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                        <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                        <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                    </tr>
                                    <tr class="total-show-hide" style="display: none;">
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="border-left">{{ number_format($companiesdaysrowtotal_dis[$one_company][$i]) }}</td>
                                            <td class="">{{ number_format($companiesdaysrowtotal_del[$one_company][$i]) }}</td>
                                            <td class="{{ $companiesdaysrowtotal_dis[$one_company][$i] > 0 ? ($companiesdaysrowtotal_del[$one_company][$i] / $companiesdaysrowtotal_dis[$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($companiesdaysrowtotal_del[$one_company][$i] / max(1, $companiesdaysrowtotal_dis[$one_company][$i]) * 100.0, 2) }}%</td>
                                            <td class="border-right {{ $companiestotal_pickedup[$one_company] > 0 ? ($companiesdaysrowtotal_del[$one_company][$i] / $companiestotal_pickedup[$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($companiesdaysrowtotal_del[$one_company][$i] / max(1, $companiestotal_pickedup[$one_company]) * 100.0, 2) }}%</td>
                                        @endfor
                                    </tr>
                                    <tr class="total-show-hide" style="display: none;">
                                        <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                        <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                        <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                        <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                        <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                    </tr>
                                    <tr class="total-show-hide" style="display: none;">
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="border-bottom border-left">{{ number_format($companiestotal_attempts_dis[$one_company][$i]) }}</td>
                                            <td class="border-bottom">{{ number_format($companiestotal_attempts_del[$one_company][$i]) }}</td>
                                            <td class="border-bottom {{ $companiestotal_attempts_dis[$one_company][$i] > 0 ? ($companiestotal_attempts_del[$one_company][$i] / $companiestotal_attempts_dis[$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($companiestotal_attempts_del[$one_company][$i] / max(1, $companiestotal_attempts_dis[$one_company][$i]) * 100.0, 2) }}%</td>
                                            <td class="border-bottom border-right {{ $companiestotal_pickedup[$one_company] > 0 ? ($companiestotal_attempts_del[$one_company][$i] / $companiestotal_pickedup[$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($companiestotal_attempts_del[$one_company][$i] / max(1, $companiestotal_pickedup[$one_company]) * 100.0, 2) }}%</td>
                                        @endfor
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <script>
                            function total_click() {
                                var flag = document.getElementById('total_button').innerText;
                                var rows = document.getElementsByClassName('total-show-hide');
                                if(flag == 'Show Companies') {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = '';
                                    });
                                    document.getElementById('total_button').innerText = 'Hide Companies';
                                }
                                else {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = 'none';
                                    });
                                    document.getElementById('total_button').innerText = 'Show Companies';
                                }
                            }
                        </script>
                    </div>
                    <hr>

                    <!-- Total of Totals By City -->
                    <div style="text-align: right;">
                        <button class="btn btn-primary" onclick="citytotal_click()" id="citytotal_button"> Show Companies</button>
                    </div>
                    <div style="overflow-x:auto; overflow-y:auto;">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    @foreach($cities as $one_city)
                                        <th colspan="27" class="border-all-thick">{{ $cities_map[$one_city]->name }}</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($cities as $one_city)
                                        <th rowspan="4" class="border-left-thick"></th>
                                        <th rowspan="4" class="border-left">Total Picked Up</th>
                                        <th rowspan="3" colspan="2" class="border-left">Total Signed In</th>
                                        <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                        <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                        @for($i = 0; $i < 5; ++$i)
                                            <th class="border-left">Dis.</th>
                                            <th>Del.</th>
                                            <th>Dis. S.R.</th>
                                            <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($cities as $one_city)
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                        <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                        <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($cities as $one_city)
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="gray-background border-left ">{{ number_format($citydaysrowtotal_dis[$one_city][$i]) }}</td>
                                            <td class="gray-background">{{ number_format($citydaysrowtotal_del[$one_city][$i]) }}</td>
                                            <td class="gray-background {{ $citydaysrowtotal_dis[$one_city][$i] > 0 ? ($citydaysrowtotal_del[$one_city][$i] / $citydaysrowtotal_dis[$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($citydaysrowtotal_del[$one_city][$i] / max(1, $citydaysrowtotal_dis[$one_city][$i]) * 100.0, 2) }}%</td>
                                            <td class="gray-background {{ $citytotal_pickedup[$one_city] > 0 ? ($citydaysrowtotal_del[$one_city][$i] / $citytotal_pickedup[$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($citydaysrowtotal_del[$one_city][$i] / max(1, $citytotal_pickedup[$one_city]) * 100.0, 2) }}%</td>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($cities as $one_city)
                                        <th class="border-left">Count</th>
                                        <th>Perc.</th>
                                        <th class="border-left">Count</th>
                                        <th>Perc.</th>
                                        <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                        <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                        <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                        <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                        <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach($cities as $one_city)
                                        <td class="border-left-thick border-bottom-thick"></td>
                                        <td class="border-left border-bottom-thick">{{ number_format($citytotal_pickedup[$one_city]) }}</td>
                                        <td class="border-left border-bottom-thick">{{ number_format($citytotal_signedin[$one_city]) }}</td>
                                        <td class="border-bottom-thick">{{ number_format($citytotal_signedin[$one_city] / max(1, $citytotal_pickedup[$one_city]) * 100.0, 2) }}%</td>
                                        <td class="border-left border-bottom-thick">{{ number_format(1.0 * $citytotal_avg_dispatched_days[$one_city] / max(1, $citytotal_avg_dispatched_count[$one_city]), 2) }}</td>
                                        <td class="border-left border-bottom-thick">
                                            <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $one_city }}&from={{ $from }}&to={{ $to }}">
                                                {{ number_format($citytotal_zero_attempts[$one_city]) }}
                                            </a>
                                        </td>
                                        <td class="border-bottom-thick">{{ number_format($citytotal_zero_attempts[$one_city] / max(1, $citytotal_pickedup[$one_city]) * 100.0, 2) }}%</td>
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="border-left border-bottom-thick">{{ number_format($citytotal_attempts_dis[$one_city][$i]) }}</td>
                                            <td class="border-bottom-thick">{{ number_format($citytotal_attempts_del[$one_city][$i]) }}</td>
                                            <td class="{{ $citytotal_attempts_dis[$one_city][$i] > 0 ? ($citytotal_attempts_del[$one_city][$i] / $citytotal_attempts_dis[$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($citytotal_attempts_del[$one_city][$i] / max(1, $citytotal_attempts_dis[$one_city][$i]) * 100.0, 2) }}%</td>
                                            <td class="{{ $citytotal_pickedup[$one_city] > 0 ? ($citytotal_attempts_del[$one_city][$i] / $citytotal_pickedup[$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($citytotal_attempts_del[$one_city][$i] / max(1, $citytotal_pickedup[$one_city]) * 100.0, 2) }}%</td>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr class="citytotal-show-hide" style="display: none;">
                                    @foreach($cities as $one_city)
                                        <th colspan="27" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                    @endforeach
                                </tr>
                                @foreach($companies as $one_company) 
                                        <tr class="citytotal-show-hide" style="display: none;">
                                            @foreach($cities as $one_city)
                                                <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($citycompaniestotal_pickedup[$one_company][$one_city]) }}</td>
                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($citycompaniestotal_signedin[$one_company][$one_city]) }}</td>
                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($citycompaniestotal_signedin[$one_company][$one_city] / max(1, $citycompaniestotal_pickedup[$one_company][$one_city]) * 100.0, 2) }}%</td>
                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $citycompaniestotal_avg_dispatched_days[$one_company][$one_city] / max(1, $citycompaniestotal_avg_dispatched_count[$one_company][$one_city]), 2) }}</td>
                                                <td rowspan="4" class="border-top border-left border-bottom">
                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $one_city }}&from={{ $from }}&to={{ $to }}">
                                                        {{ number_format($citycompaniestotal_zero_attempts[$one_company][$one_city]) }}
                                                    </a>
                                                </td>
                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($citycompaniestotal_zero_attempts[$one_company][$one_city] / max(1, $citycompaniestotal_pickedup[$one_company][$one_city]) * 100.0, 2) }}%</td>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                            @endforeach
                                        </tr>
                                        <tr class="citytotal-show-hide" style="display: none;">
                                            @foreach($cities as $one_city)
                                                @for($i = 0; $i < 5; ++$i)
                                                    <td class="border-left">{{ number_format($citycompaniesdaysrowtotal_dis[$one_company][$one_city][$i]) }}</td>
                                                    <td class="">{{ number_format($citycompaniesdaysrowtotal_del[$one_company][$one_city][$i]) }}</td>
                                                    <td class="{{ $citycompaniesdaysrowtotal_dis[$one_company][$one_city][$i] > 0 ? ($citycompaniesdaysrowtotal_del[$one_company][$one_city][$i] / $citycompaniesdaysrowtotal_dis[$one_company][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($citycompaniesdaysrowtotal_del[$one_company][$one_city][$i] / max(1, $citycompaniesdaysrowtotal_dis[$one_company][$one_city][$i]) * 100.0, 2) }}%</td>
                                                    <td class="border-right {{ $citycompaniestotal_pickedup[$one_company][$one_city] > 0 ? ($citycompaniesdaysrowtotal_del[$one_company][$one_city][$i] / $citycompaniestotal_pickedup[$one_company][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($citycompaniesdaysrowtotal_del[$one_company][$one_city][$i] / max(1, $citycompaniestotal_pickedup[$one_company][$one_city]) * 100.0, 2) }}%</td>
                                                @endfor
                                            @endforeach
                                        </tr>
                                        <tr class="citytotal-show-hide" style="display: none;">
                                            @foreach($cities as $one_city)
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                            @endforeach
                                        </tr>
                                        <tr class="citytotal-show-hide" style="display: none;">
                                            @foreach($cities as $one_city)
                                                @for($i = 0; $i < 5; ++$i)
                                                    <td class="border-bottom border-left">{{ number_format($citycompaniestotal_attempts_dis[$one_company][$one_city][$i]) }}</td>
                                                    <td class="border-bottom">{{ number_format($citycompaniestotal_attempts_del[$one_company][$one_city][$i]) }}</td>
                                                    <td class="border-bottom {{ $citycompaniestotal_attempts_dis[$one_company][$one_city][$i] > 0 ? ($citycompaniestotal_attempts_del[$one_company][$one_city][$i] / $citycompaniestotal_attempts_dis[$one_company][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($citycompaniestotal_attempts_del[$one_company][$one_city][$i] / max(1, $citycompaniestotal_attempts_dis[$one_company][$one_city][$i]) * 100.0, 2) }}%</td>
                                                    <td class="border-bottom border-right {{ $citycompaniestotal_pickedup[$one_company][$one_city] > 0 ? ($citycompaniestotal_attempts_del[$one_company][$one_city][$i] / $citycompaniestotal_pickedup[$one_company][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($citycompaniestotal_attempts_del[$one_company][$one_city][$i] / max(1, $citycompaniestotal_pickedup[$one_company][$one_city]) * 100.0, 2) }}%</td>
                                                @endfor
                                            @endforeach
                                        </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <script>
                            function citytotal_click() {
                                var flag = document.getElementById('citytotal_button').innerText;
                                var rows = document.getElementsByClassName('citytotal-show-hide');
                                if(flag == 'Show Companies') {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = '';
                                    });
                                    document.getElementById('citytotal_button').innerText = 'Hide Companies';
                                }
                                else {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = 'none';
                                    });
                                    document.getElementById('citytotal_button').innerText = 'Show Companies';
                                }
                            }
                        </script>
                    </div>
                    <hr>

                    <!-- Total of Totals By Hub -->
                    <div style="text-align: right;">
                        <button class="btn btn-primary" onclick="hubtotal_click()" id="hubtotal_button"> Show Companies</button>
                    </div>
                    <div style="overflow-x:auto; overflow-y:auto;">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        <th colspan="25" class="border-all-thick">{{ $hubs_map[$one_hub]->name }}</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        <th rowspan="4" class="border-left-thick"></th>
                                        <th rowspan="4" class="border-left">Total Signed In</th>
                                        <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                        <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                        @for($i = 0; $i < 5; ++$i)
                                            <th class="border-left">Dis.</th>
                                            <th>Del.</th>
                                            <th>Dis. S.R.</th>
                                            <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                        <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                        <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="gray-background border-left ">{{ number_format($hubdaysrowtotal_dis[$one_hub][$i]) }}</td>
                                            <td class="gray-background">{{ number_format($hubdaysrowtotal_del[$one_hub][$i]) }}</td>
                                            <td class="gray-background {{ $hubdaysrowtotal_dis[$one_hub][$i] > 0 ? ($hubdaysrowtotal_del[$one_hub][$i] / $hubdaysrowtotal_dis[$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($hubdaysrowtotal_del[$one_hub][$i] / max(1, $hubdaysrowtotal_dis[$one_hub][$i]) * 100.0, 2) }}%</td>
                                            <td class="gray-background {{ $hubtotal_signedin[$one_hub] > 0 ? ($hubdaysrowtotal_del[$one_hub][$i] / $hubtotal_signedin[$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($hubdaysrowtotal_del[$one_hub][$i] / max(1, $hubtotal_signedin[$one_hub]) * 100.0, 2) }}%</td>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        <th class="border-left">Count</th>
                                        <th>Perc.</th>
                                        <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                        <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                        <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                        <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                        <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach($hubs as $one_hub)
                                        <td class="border-left-thick border-bottom-thick"></td>
                                        <td class="border-left border-bottom-thick">{{ number_format($hubtotal_signedin[$one_hub]) }}</td>
                                        <td class="border-left border-bottom-thick">{{ number_format(1.0 * $hubtotal_avg_dispatched_days[$one_hub] / max(1, $hubtotal_avg_dispatched_count[$one_hub]), 2) }}</td>
                                        <td class="border-left border-bottom-thick">
                                            <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&hubs={{ $one_hub }}&from={{ $from }}&to={{ $to }}">
                                                {{ number_format($hubtotal_zero_attempts[$one_hub]) }}
                                            </a>
                                        </td>
                                        <td class="border-bottom-thick">{{ number_format($hubtotal_zero_attempts[$one_hub] / max(1, $hubtotal_signedin[$one_hub]) * 100.0, 2) }}%</td>
                                        @for($i = 0; $i < 5; ++$i)
                                            <td class="border-left border-bottom-thick">{{ number_format($hubtotal_attempts_dis[$one_hub][$i]) }}</td>
                                            <td class="border-bottom-thick">{{ number_format($hubtotal_attempts_del[$one_hub][$i]) }}</td>
                                            <td class="{{ $hubtotal_attempts_dis[$one_hub][$i] > 0 ? ($hubtotal_attempts_del[$one_hub][$i] / $hubtotal_attempts_dis[$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($hubtotal_attempts_del[$one_hub][$i] / max(1, $hubtotal_attempts_dis[$one_hub][$i]) * 100.0, 2) }}%</td>
                                            <td class="{{ $hubtotal_signedin[$one_hub] > 0 ? ($hubtotal_attempts_del[$one_hub][$i] / $hubtotal_signedin[$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($hubtotal_attempts_del[$one_hub][$i] / max(1, $hubtotal_signedin[$one_hub]) * 100.0, 2) }}%</td>
                                        @endfor
                                    @endforeach
                                </tr>
                                <tr class="hubtotal-show-hide" style="display: none;">
                                    @foreach($hubs as $one_hub)
                                        <th colspan="25" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                    @endforeach
                                </tr>
                                @foreach($companies as $one_company) 
                                        <tr class="hubtotal-show-hide" style="display: none;">
                                            @foreach($hubs as $one_hub)
                                                <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($hubcompaniestotal_signedin[$one_company][$one_hub]) }}</td>
                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $hubcompaniestotal_avg_dispatched_days[$one_company][$one_hub] / max(1, $hubcompaniestotal_avg_dispatched_count[$one_company][$one_hub]), 2) }}</td>
                                                <td rowspan="4" class="border-top border-left border-bottom">
                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&hubs={{ $one_hub }}&from={{ $from }}&to={{ $to }}">
                                                        {{ number_format($hubcompaniestotal_zero_attempts[$one_company][$one_hub]) }}
                                                    </a>
                                                </td>
                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($hubcompaniestotal_zero_attempts[$one_company][$one_hub] / max(1, $hubcompaniestotal_pickedup[$one_company][$one_hub]) * 100.0, 2) }}%</td>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                            @endforeach
                                        </tr>
                                        <tr class="hubtotal-show-hide" style="display: none;">
                                            @foreach($hubs as $one_hub)
                                                @for($i = 0; $i < 5; ++$i)
                                                    <td class="border-left">{{ number_format($hubcompaniesdaysrowtotal_dis[$one_company][$one_hub][$i]) }}</td>
                                                    <td class="">{{ number_format($hubcompaniesdaysrowtotal_del[$one_company][$one_hub][$i]) }}</td>
                                                    <td class="{{ $hubcompaniesdaysrowtotal_dis[$one_company][$one_hub][$i] > 0 ? ($hubcompaniesdaysrowtotal_del[$one_company][$one_hub][$i] / $hubcompaniesdaysrowtotal_dis[$one_company][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($hubcompaniesdaysrowtotal_del[$one_company][$one_hub][$i] / max(1, $hubcompaniesdaysrowtotal_dis[$one_company][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                    <td class="border-right {{ $hubcompaniestotal_pickedup[$one_company][$one_hub] > 0 ? ($hubcompaniesdaysrowtotal_del[$one_company][$one_hub][$i] / $hubcompaniestotal_pickedup[$one_company][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($hubcompaniesdaysrowtotal_del[$one_company][$one_hub][$i] / max(1, $hubcompaniestotal_pickedup[$one_company][$one_hub]) * 100.0, 2) }}%</td>
                                                @endfor
                                            @endforeach
                                        </tr>
                                        <tr class="hubtotal-show-hide" style="display: none;">
                                            @foreach($hubs as $one_hub)
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                            @endforeach
                                        </tr>
                                        <tr class="hubtotal-show-hide" style="display: none;">
                                            @foreach($hubs as $one_hub)
                                                @for($i = 0; $i < 5; ++$i)
                                                    <td class="border-bottom border-left">{{ number_format($hubcompaniestotal_attempts_dis[$one_company][$one_hub][$i]) }}</td>
                                                    <td class="border-bottom">{{ number_format($hubcompaniestotal_attempts_del[$one_company][$one_hub][$i]) }}</td>
                                                    <td class="border-bottom {{ $hubcompaniestotal_attempts_dis[$one_company][$one_hub][$i] > 0 ? ($hubcompaniestotal_attempts_del[$one_company][$one_hub][$i] / $hubcompaniestotal_attempts_dis[$one_company][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($hubcompaniestotal_attempts_del[$one_company][$one_hub][$i] / max(1, $hubcompaniestotal_attempts_dis[$one_company][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                    <td class="border-bottom border-right {{ $hubcompaniestotal_pickedup[$one_company][$one_hub] > 0 ? ($hubcompaniestotal_attempts_del[$one_company][$one_hub][$i] / $hubcompaniestotal_pickedup[$one_company][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($hubcompaniestotal_attempts_del[$one_company][$one_hub][$i] / max(1, $hubcompaniestotal_pickedup[$one_company][$one_hub]) * 100.0, 2) }}%</td>
                                                @endfor
                                            @endforeach
                                        </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <script>
                            function hubtotal_click() {
                                var flag = document.getElementById('hubtotal_button').innerText;
                                var rows = document.getElementsByClassName('hubtotal-show-hide');
                                if(flag == 'Show Companies') {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = '';
                                    });
                                    document.getElementById('hubtotal_button').innerText = 'Hide Companies';
                                }
                                else {
                                    [].forEach.call(rows, function(e) {
                                        e.style.display = 'none';
                                    });
                                    document.getElementById('hubtotal_button').innerText = 'Show Companies';
                                }
                            }
                        </script>
                    </div>

                </div>
            </div>
        </div>

        <?php 
        $unique_years = array(); // save the distinct years, 1D array
        $years = array();
        $months = array(); // save the months for each year, 2D array
        $days = array(); // save the days for each (year,month) pair, 3D array
        foreach($result as $one_result)         array_push($unique_years, $one_result->year);
        foreach($daysrow_result as $one_result) array_push($unique_years, $one_result->year);
        $unique_years = array_unique($unique_years);
        foreach($unique_years as $one_year) {
            array_push($years, $one_year);
            $months[$one_year] = array();
            $days[$one_year] = array();
        }
        foreach($result as $one_result)         array_push($months[$one_result->year], $one_result->month);
        foreach($daysrow_result as $one_result) array_push($months[$one_result->year], $one_result->month);
        foreach($years as $one_year) {
            $unique_months = array_unique($months[$one_year]); // sort and uniquify
            $months[$one_year] = array();
            foreach($unique_months as $one_month) {
                array_push($months[$one_year], $one_month);
                $days[$one_year][$one_month] = array();
            }
        }
        foreach($result as $one_result)         array_push($days[$one_result->year][$one_result->month], $one_result->day);
        foreach($daysrow_result as $one_result) array_push($days[$one_result->year][$one_result->month], $one_result->day);
        foreach($years as $one_year) {
            foreach($months[$one_year] as $one_month) {
                $unique_days = array_unique($days[$one_year][$one_month]);
                $days[$one_year][$one_month] = array();
                foreach($unique_days as $one_day) {
                    array_push($days[$one_year][$one_month], $one_day);
                }
            }
        }

        //////////////////////////////////////////////////// ---------------------------- separator (attempts) ---------------------------- ////////////////////////////////////////////////////

        // initialize all the arrays (months totals, days totals)
        foreach($years as $one_year) {
            foreach($months[$one_year] as $one_month) {

                ////////////////////////
                ///// initialize months totals
                $monthtotal_pickedup[$one_year][$one_month]             = 0;
                $monthtotal_signedin[$one_year][$one_month]             = 0;
                $monthtotal_avg_dispatched_days[$one_year][$one_month]  = 0;
                $monthtotal_avg_dispatched_count[$one_year][$one_month] = 0;
                $monthtotal_zeroattempts[$one_year][$one_month]         = 0;
                $monthtotal_dis[$one_year][$one_month]                  = array();
                $monthtotal_del[$one_year][$one_month]                  = array();
                for($i = 0; $i < 5; ++$i) {
                    $monthtotal_dis[$one_year][$one_month][$i]          = 0;
                    $monthtotal_del[$one_year][$one_month][$i]          = 0;
                }

                foreach($cities as $one_city) {
                    $monthcitytotal_pickedup[$one_year][$one_month][$one_city]             = 0;
                    $monthcitytotal_signedin[$one_year][$one_month][$one_city]             = 0;
                    $monthcitytotal_avg_dispatched_days[$one_year][$one_month][$one_city]  = 0;
                    $monthcitytotal_avg_dispatched_count[$one_year][$one_month][$one_city] = 0;
                    $monthcitytotal_zeroattempts[$one_year][$one_month][$one_city]         = 0;
                    $monthcitytotal_dis[$one_year][$one_month][$one_city]                  = array();
                    $monthcitytotal_del[$one_year][$one_month][$one_city]                  = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthcitytotal_dis[$one_year][$one_month][$one_city][$i]          = 0;
                        $monthcitytotal_del[$one_year][$one_month][$one_city][$i]          = 0;
                    }
                }

                array_push($hubs, 0);
                foreach($hubs as $one_hub) {
                    $monthhubtotal_pickedup[$one_year][$one_month][$one_hub]             = 0;
                    $monthhubtotal_signedin[$one_year][$one_month][$one_hub]             = 0;
                    $monthhubtotal_avg_dispatched_days[$one_year][$one_month][$one_hub]  = 0;
                    $monthhubtotal_avg_dispatched_count[$one_year][$one_month][$one_hub] = 0;
                    $monthhubtotal_zeroattempts[$one_year][$one_month][$one_hub]         = 0;
                    $monthhubtotal_dis[$one_year][$one_month][$one_hub]                  = array();
                    $monthhubtotal_del[$one_year][$one_month][$one_hub]                  = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthhubtotal_dis[$one_year][$one_month][$one_hub][$i]          = 0;
                        $monthhubtotal_del[$one_year][$one_month][$one_hub][$i]          = 0;  
                    }
                }
                array_pop($hubs);
                ////////////////////////

                ////////////////////////
                ///// initialize days totals
                foreach($days[$one_year][$one_month] as $one_day) {
                                        
                    $daytotal_pickedup[$one_year][$one_month][$one_day]             = 0;
                    $daytotal_signedin[$one_year][$one_month][$one_day]             = 0;
                    $daytotal_avg_dispatched_days[$one_year][$one_month][$one_day]  = 0;
                    $daytotal_avg_dispatched_count[$one_year][$one_month][$one_day] = 0;
                    $daytotal_zeroattempts[$one_year][$one_month][$one_day]         = 0;
                    $daytotal_dis[$one_year][$one_month][$one_day]                  = array();
                    $daytotal_del[$one_year][$one_month][$one_day]                  = array();
                    for($i = 0; $i < 5; ++$i) {
                        $daytotal_dis[$one_year][$one_month][$one_day][$i]          = 0;
                        $daytotal_del[$one_year][$one_month][$one_day][$i]          = 0;
                    }

                    foreach($cities as $one_city) {
                        $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]             = 0;
                        $daycitytotal_signedin[$one_year][$one_month][$one_day][$one_city]             = 0;
                        $daycitytotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_city]  = 0;
                        $daycitytotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_city] = 0;
                        $daycitytotal_zeroattempts[$one_year][$one_month][$one_day][$one_city]         = 0;
                        $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city]                  = array();
                        $daycitytotal_del[$one_year][$one_month][$one_day][$one_city]                  = array();
                        for($i = 0; $i < 5; ++$i) {
                            $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i]          = 0;
                            $daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i]          = 0;
                        }
                    }

                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $dayhubtotal_pickedup[$one_year][$one_month][$one_day][$one_hub]             = 0;
                        $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub]             = 0;
                        $dayhubtotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_hub]  = 0;
                        $dayhubtotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_hub] = 0;
                        $dayhubtotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub]         = 0;
                        $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub]                  = array();
                        $dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub]                  = array();
                        for($i = 0; $i < 5; ++$i) {
                            $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i]          = 0;
                            $dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i]          = 0;  
                        }
                    }
                    array_pop($hubs);
                    
                }
                ////////////////////////

            }
        }

        // calculate the actual numbers
        foreach($result as $one_result) {

            ////////////////////////
            ///// months totals
            $monthtotal_pickedup[$one_result->year][$one_result->month]                 += $one_result->pickedup;
            $monthtotal_signedin[$one_result->year][$one_result->month]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthtotal_avg_dispatched_days[$one_result->year][$one_result->month]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthtotal_avg_dispatched_count[$one_result->year][$one_result->month] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthtotal_dis[$one_result->year][$one_result->month][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthtotal_del[$one_result->year][$one_result->month][$i]              += $one_result->delivered;
            }

            $monthcitytotal_pickedup[$one_result->year][$one_result->month][$one_result->main_city_id]                 += $one_result->pickedup;
            $monthcitytotal_signedin[$one_result->year][$one_result->month][$one_result->main_city_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthcitytotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->main_city_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthcitytotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->main_city_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthcitytotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthcitytotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$i]              += $one_result->delivered;
            }

            $monthhubtotal_pickedup[$one_result->year][$one_result->month][$one_result->hub_id]                 += $one_result->pickedup;
            $monthhubtotal_signedin[$one_result->year][$one_result->month][$one_result->hub_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthhubtotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->hub_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthhubtotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->hub_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthhubtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthhubtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$i]              += $one_result->delivered;
            }
            ////////////////////////

            ////////////////////////
            ///// days totals
            $daytotal_pickedup[$one_result->year][$one_result->month][$one_result->day]                 += $one_result->pickedup;
            $daytotal_signedin[$one_result->year][$one_result->month][$one_result->day]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $daytotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day]  += $one_result->avg_dispatched * $one_result->dispatched;
                $daytotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $daytotal_dis[$one_result->year][$one_result->month][$one_result->day][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $daytotal_del[$one_result->year][$one_result->month][$one_result->day][$i]              += $one_result->delivered;
            }

            $daycitytotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]                 += $one_result->pickedup;
            $daycitytotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $daycitytotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $daycitytotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $daycitytotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $daycitytotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$i]              += $one_result->delivered;
            }

            $dayhubtotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]                 += $one_result->pickedup;
            $dayhubtotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $dayhubtotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $dayhubtotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $dayhubtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $dayhubtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$i]              += $one_result->delivered;
            }
            ////////////////////////

        }

        // adjust calculations
        // - add delivered to dispatched
        // - calculate zero attempts
        // - add signedin to picked up (not in the hubs tables)
        foreach($years as $one_year) {
            foreach($months[$one_year] as $one_month) {

                ////////////////////////
                ///// months totals
                for($i = 0; $i < 5; ++$i) {
                    $monthtotal_dis[$one_year][$one_month][$i] += $monthtotal_del[$one_year][$one_month][$i];
                }
                $monthtotal_pickedup[$one_year][$one_month] += $monthtotal_signedin[$one_year][$one_month];
                $monthtotal_zeroattempts[$one_year][$one_month] = $monthtotal_pickedup[$one_year][$one_month] - $monthtotal_dis[$one_year][$one_month][0];
                foreach($cities as $one_city) {
                    for($i = 0; $i < 5; ++$i) {
                        $monthcitytotal_dis[$one_year][$one_month][$one_city][$i] += $monthcitytotal_del[$one_year][$one_month][$one_city][$i];
                    }
                    $monthcitytotal_pickedup[$one_year][$one_month][$one_city] += $monthcitytotal_signedin[$one_year][$one_month][$one_city];
                    $monthcitytotal_zeroattempts[$one_year][$one_month][$one_city] = $monthcitytotal_pickedup[$one_year][$one_month][$one_city] - $monthcitytotal_dis[$one_year][$one_month][$one_city][0];
                }
                foreach($hubs as $one_hub) {
                    for($i = 0; $i < 5; ++$i) {
                        $monthhubtotal_dis[$one_year][$one_month][$one_hub][$i] += $monthhubtotal_del[$one_year][$one_month][$one_hub][$i];
                    }
                    $monthhubtotal_zeroattempts[$one_year][$one_month][$one_hub] = $monthhubtotal_signedin[$one_year][$one_month][$one_hub] - $monthhubtotal_dis[$one_year][$one_month][$one_hub][0];
                }
                ////////////////////////

                ////////////////////////
                ///// days totals
                foreach($days[$one_year][$one_month] as $one_day) {
                    for($i = 0; $i < 5; ++$i) {
                        $daytotal_dis[$one_year][$one_month][$one_day][$i] += $daytotal_del[$one_year][$one_month][$one_day][$i];
                    }
                    $daytotal_pickedup[$one_year][$one_month][$one_day] += $daytotal_signedin[$one_year][$one_month][$one_day];
                    $daytotal_zeroattempts[$one_year][$one_month][$one_day] = $daytotal_pickedup[$one_year][$one_month][$one_day] - $daytotal_dis[$one_year][$one_month][$one_day][0];
                    foreach($cities as $one_city) {
                        for($i = 0; $i < 5; ++$i) {
                            $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i] += $daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i];
                        }
                        $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] += $daycitytotal_signedin[$one_year][$one_month][$one_day][$one_city];
                        $daycitytotal_zeroattempts[$one_year][$one_month][$one_day][$one_city] = $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] - $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][0];
                    }
                    foreach($hubs as $one_hub) {
                        for($i = 0; $i < 5; ++$i) {
                            $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] += $dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i];
                        }
                        $dayhubtotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub] = $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub] - $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][0];
                    }
                }
                ////////////////////////

            }
        }

        //////////////////////////////////////////////////// ---------------------------- separator (daysrow) ---------------------------- ////////////////////////////////////////////////////

        // initialize days row variables
        foreach($years as $one_year) {
            foreach($months[$one_year] as $one_month) {

                ///// initialize months totals
                $monthdaysrowtotal_dis[$one_year][$one_month]         = array();
                $monthdaysrowtotal_del[$one_year][$one_month]         = array();
                for($i = 0; $i < 5; ++$i) {
                    $monthdaysrowtotal_dis[$one_year][$one_month][$i] = 0;
                    $monthdaysrowtotal_del[$one_year][$one_month][$i] = 0;
                }

                foreach($cities as $one_city) {
                    $monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city]         = array();
                    $monthcitydaysrowtotal_del[$one_year][$one_month][$one_city]         = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city][$i] = 0;
                        $monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i] = 0;
                    }
                }

                array_push($hubs, 0);
                foreach($hubs as $one_hub) {
                    $monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub]         = array();
                    $monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub]         = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub][$i] = 0;
                        $monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i] = 0;  
                    }
                }
                array_pop($hubs);

                foreach($days[$one_year][$one_month] as $one_day) {

                    $daydaysrowtotal_dis[$one_year][$one_month][$one_day]         = array();
                    $daydaysrowtotal_del[$one_year][$one_month][$one_day]         = array();
                    for($i = 0; $i < 5; ++$i) {
                        $daydaysrowtotal_dis[$one_year][$one_month][$one_day][$i] = 0;
                        $daydaysrowtotal_del[$one_year][$one_month][$one_day][$i] = 0;
                    }

                    foreach($cities as $one_city) {
                        $daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city]         = array();
                        $daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city]         = array();
                        for($i = 0; $i < 5; ++$i) {
                            $daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$i] = 0;
                            $daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i] = 0;
                        }
                    }

                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub]         = array();
                        $dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub]         = array();
                        for($i = 0; $i < 5; ++$i) {
                            $dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] = 0;
                            $dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] = 0;  
                        }
                    }
                    array_pop($hubs);
                }
                
            }
        }

        // calculate the days row in the month totals tables
        foreach($daysrow_result as $one_result) {

            /////////////////
            // month totals
            $monthdaysrowtotal_dis[$one_result->year][$one_result->month][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthdaysrowtotal_dis[$one_result->year][$one_result->month][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthdaysrowtotal_dis[$one_result->year][$one_result->month][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthdaysrowtotal_dis[$one_result->year][$one_result->month][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthdaysrowtotal_dis[$one_result->year][$one_result->month][4] += $one_result->dispatched; 

            $monthdaysrowtotal_del[$one_result->year][$one_result->month][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthdaysrowtotal_del[$one_result->year][$one_result->month][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthdaysrowtotal_del[$one_result->year][$one_result->month][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthdaysrowtotal_del[$one_result->year][$one_result->month][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthdaysrowtotal_del[$one_result->year][$one_result->month][4] += $one_result->delivered; 
            ///////////////////

            ///////////////////
            // month city totals
            $monthcitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthcitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthcitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthcitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthcitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][4] += $one_result->dispatched; 

            $monthcitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthcitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthcitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthcitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthcitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][4] += $one_result->delivered; 
            ///////////////////

            ///////////////////
            // month hub totals
            $monthhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][4] += $one_result->dispatched; 

            $monthhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][4] += $one_result->delivered; 
            ///////////////////

            ///////////////////
            // day totals
            $daydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $daydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $daydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $daydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $daydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][4] += $one_result->dispatched; 

            $daydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $daydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $daydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $daydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $daydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][4] += $one_result->delivered; 
            ///////////////////

            ///////////////////
            // day city totals
            $daycitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $daycitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $daycitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $daycitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $daycitydaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][4] += $one_result->dispatched; 

            $daycitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $daycitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $daycitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $daycitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $daycitydaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][4] += $one_result->delivered; 
            ///////////////////

            ///////////////////
            // day hub totals
            $dayhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $dayhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $dayhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $dayhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $dayhubdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][4] += $one_result->dispatched; 

            $dayhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $dayhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $dayhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $dayhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $dayhubdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][4] += $one_result->delivered; 
            ///////////////////
        }

        //////////////////////////////////////////////////// ---------------------------- separator (companies: attempts and daysrow) ---------------------------- ////////////////////////////////////////////////////

        if(in_array('71', $companies_init)) {
            $counter = 0;
            foreach($jollychic_group as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        if(in_array('73', $companies_init)) {
            $counter = 0;
            foreach($jollychic_local as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        if(in_array('74', $companies_init)) {
            $counter = 0;
            foreach($jollychic_global as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        // initialize all the arrays (months companies totals, days comapnies totals)
        foreach($companies_init as $one_company) {
            foreach($years as $one_year) {
                foreach($months[$one_year] as $one_month) {
    
                    ////////////////////////
                    ///// initialize months companies totals
                    $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]             = 0;
                    $monthcompaniestotal_signedin[$one_year][$one_month][$one_company]             = 0;
                    $monthcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_company]  = 0;
                    $monthcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_company] = 0;
                    $monthcompaniestotal_zeroattempts[$one_year][$one_month][$one_company]         = 0;
                    $monthcompaniestotal_dis[$one_year][$one_month][$one_company]                  = array();
                    $monthcompaniestotal_del[$one_year][$one_month][$one_company]                  = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i]          = 0;
                        $monthcompaniestotal_del[$one_year][$one_month][$one_company][$i]          = 0;
                    }
    
                    foreach($cities as $one_city) {
                        $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]             = 0;
                        $monthcitycompaniestotal_signedin[$one_year][$one_month][$one_city][$one_company]             = 0;
                        $monthcitycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_city][$one_company]  = 0;
                        $monthcitycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_city][$one_company] = 0;
                        $monthcitycompaniestotal_zeroattempts[$one_year][$one_month][$one_city][$one_company]         = 0;
                        $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company]                  = array();
                        $monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company]                  = array();
                        for($i = 0; $i < 5; ++$i) {
                            $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i]          = 0;
                            $monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i]          = 0;
                        }
                    }
    
                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $monthhubcompaniestotal_pickedup[$one_year][$one_month][$one_hub][$one_company]             = 0;
                        $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company]             = 0;
                        $monthhubcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_hub][$one_company]  = 0;
                        $monthhubcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_hub][$one_company] = 0;
                        $monthhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_hub][$one_company]         = 0;
                        $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company]                  = array();
                        $monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company]                  = array();
                        for($i = 0; $i < 5; ++$i) {
                            $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i]          = 0;
                            $monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i]          = 0;  
                        }
                    }
                    array_pop($hubs);
                    ////////////////////////
    
                    ////////////////////////
                    ///// initialize months daysrows companies totals
                    $monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company]         = array();
                    $monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company]         = array();
                    for($i = 0; $i < 5; ++$i) {
                        $monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company][$i] = 0;
                        $monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i] = 0;
                    }
    
                    foreach($cities as $one_city) {
                        $monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company]         = array();
                        $monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company]         = array();
                        for($i = 0; $i < 5; ++$i) {
                            $monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company][$i] = 0;
                            $monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i] = 0;
                        }
                    }
    
                    array_push($hubs, 0);
                    foreach($hubs as $one_hub) {
                        $monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company]         = array();
                        $monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company]         = array();
                        for($i = 0; $i < 5; ++$i) {
                            $monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] = 0;
                            $monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i] = 0;  
                        }
                    }
                    array_pop($hubs);
                    ////////////////////////
    
                    foreach($days[$one_year][$one_month] as $one_day) {
                                            
                        ////////////////////////
                        // initialize days companies totals
                        $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]             = 0;
                        $daycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_company]             = 0;
                        $daycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_company]  = 0;
                        $daycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_company] = 0;
                        $daycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_company]         = 0;
                        $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company]                  = array();
                        $daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company]                  = array();
                        for($i = 0; $i < 5; ++$i) {
                            $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i]          = 0;
                            $daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i]          = 0;
                        }
    
                        foreach($cities as $one_city) {
                            $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]             = 0;
                            $daycitycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_city][$one_company]             = 0;
                            $daycitycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_city][$one_company]  = 0;
                            $daycitycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_city][$one_company] = 0;
                            $daycitycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_city][$one_company]         = 0;
                            $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company]                  = array();
                            $daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company]                  = array();
                            for($i = 0; $i < 5; ++$i) {
                                $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i]          = 0;
                                $daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i]          = 0;
                            }
                        }
    
                        array_push($hubs, 0);
                        foreach($hubs as $one_hub) {
                            $dayhubcompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_hub][$one_company]             = 0;
                            $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company]             = 0;
                            $dayhubcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_hub][$one_company]  = 0;
                            $dayhubcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_hub][$one_company] = 0;
                            $dayhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub][$one_company]         = 0;
                            $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company]                  = array();
                            $dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company]                  = array();
                            for($i = 0; $i < 5; ++$i) {
                                $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]          = 0;
                                $dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]          = 0;  
                            }
                        }
                        array_pop($hubs);
                        ////////////////////////
    
                        ////////////////////////
                        // initialize days daysrows companies totals
                        $daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company]         = array();
                        $daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company]         = array();
                        for($i = 0; $i < 5; ++$i) {
                            $daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company][$i] = 0;
                            $daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i] = 0;
                        }
    
                        foreach($cities as $one_city) {
                            $daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company]         = array();
                            $daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company]         = array();
                            for($i = 0; $i < 5; ++$i) {
                                $daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] = 0;
                                $daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] = 0;
                            }
                        }
    
                        array_push($hubs, 0);
                        foreach($hubs as $one_hub) {
                            $dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company]         = array();
                            $dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company]         = array();
                            for($i = 0; $i < 5; ++$i) {
                                $dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] = 0;
                                $dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] = 0;  
                            }
                        }
                        array_pop($hubs);
                        /////////////////////////
                        
                    }
    
                }
            }
        }
        if(in_array('74', $companies_init)) {
            foreach($jollychic_global as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }
        if(in_array('73', $companies_init)) {
            foreach($jollychic_local as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }
        if(in_array('71', $companies_init)) {
            foreach($jollychic_group as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }

        // calculate the attempts actual numbers
        foreach($result as $one_result) {

            ////////////////////////
            ///// months totals
            $monthcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->company_id]                 += $one_result->pickedup;
            $monthcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthcompaniestotal_del[$one_result->year][$one_result->month][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthcompaniestotal_pickedup[$one_result->year][$one_result->month]['71']                                += $one_result->pickedup;
                $monthcompaniestotal_signedin[$one_result->year][$one_result->month]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcompaniestotal_dis[$one_result->year][$one_result->month]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcompaniestotal_del[$one_result->year][$one_result->month]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthcompaniestotal_pickedup[$one_result->year][$one_result->month]['73']                                += $one_result->pickedup;
                $monthcompaniestotal_signedin[$one_result->year][$one_result->month]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcompaniestotal_dis[$one_result->year][$one_result->month]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcompaniestotal_del[$one_result->year][$one_result->month]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthcompaniestotal_pickedup[$one_result->year][$one_result->month]['74']                                += $one_result->pickedup;
                $monthcompaniestotal_signedin[$one_result->year][$one_result->month]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcompaniestotal_dis[$one_result->year][$one_result->month]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcompaniestotal_del[$one_result->year][$one_result->month]['74'][$i]                             += $one_result->delivered;
                }
            }

            $monthcitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id]                 += $one_result->pickedup;
            $monthcitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthcitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthcitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthcitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthcitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthcitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->main_city_id]['71']                                += $one_result->pickedup;
                $monthcitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->main_city_id]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->main_city_id]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->main_city_id]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthcitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->main_city_id]['73']                                += $one_result->pickedup;
                $monthcitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->main_city_id]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->main_city_id]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->main_city_id]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthcitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->main_city_id]['74']                                += $one_result->pickedup;
                $monthcitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->main_city_id]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthcitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->main_city_id]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthcitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->main_city_id]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthcitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthcitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][$i]                             += $one_result->delivered;
                }
            }

            $monthhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id]                 += $one_result->pickedup;
            $monthhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $monthhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $monthhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $monthhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $monthhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->hub_id]['71']                                += $one_result->pickedup;
                $monthhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->hub_id]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->hub_id]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->hub_id]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->hub_id]['73']                                += $one_result->pickedup;
                $monthhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->hub_id]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->hub_id]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->hub_id]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->hub_id]['74']                                += $one_result->pickedup;
                $monthhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->hub_id]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $monthhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->hub_id]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $monthhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->hub_id]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $monthhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $monthhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][$i]                             += $one_result->delivered;
                }
            }
            ////////////////////////

            ////////////////////////
            ///// days totals
            $daycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id]                 += $one_result->pickedup;
            $daycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $daycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $daycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $daycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $daycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $daycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day]['71']                                += $one_result->pickedup;
                $daycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $daycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day]['73']                                += $one_result->pickedup;
                $daycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $daycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day]['74']                                += $one_result->pickedup;
                $daycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][$i]                             += $one_result->delivered;
                }
            }

            $daycitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id]                 += $one_result->pickedup;
            $daycitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $daycitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $daycitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $daycitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $daycitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $daycitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71']                                += $one_result->pickedup;
                $daycitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $daycitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73']                                += $one_result->pickedup;
                $daycitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $daycitycompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74']                                += $one_result->pickedup;
                $daycitycompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $daycitycompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $daycitycompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $daycitycompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $daycitycompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][$i]                             += $one_result->delivered;
                }
            }

            $dayhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id]                 += $one_result->pickedup;
            $dayhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id]                 += $one_result->signedin;
            if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                $dayhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id]  += $one_result->avg_dispatched * $one_result->dispatched;
                $dayhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id] += $one_result->dispatched;
            }
            $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
            $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
            for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                $dayhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][$i]              += $one_result->dispatched;
            }
            for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                $dayhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][$i]              += $one_result->delivered;
            }
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $dayhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71']                                += $one_result->pickedup;
                $dayhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $dayhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $dayhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $dayhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $dayhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $dayhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73']                                += $one_result->pickedup;
                $dayhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $dayhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $dayhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $dayhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $dayhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][$i]                             += $one_result->delivered;
                }
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $dayhubcompaniestotal_pickedup[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74']                                += $one_result->pickedup;
                $dayhubcompaniestotal_signedin[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74']                                += $one_result->signedin;
                if($one_result->num_faild_delivery_attempts > 0 || in_array($one_result->status, array(4,5))) {
                    $dayhubcompaniestotal_avg_dispatched_days[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74']                 += $one_result->avg_dispatched * $one_result->dispatched;
                    $dayhubcompaniestotal_avg_dispatched_count[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74']                += $one_result->dispatched;
                }
                $index_dis = get_index_dis($one_result->num_faild_delivery_attempts, $one_result->status);
                $index_del = get_index_del($one_result->num_faild_delivery_attempts, $one_result->status);
                for($i = 0; $i <= $index_dis; ++$i) { // if no case apply then index_dis = -1
                    $dayhubcompaniestotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][$i]                             += $one_result->dispatched;
                }
                for($i = 0; $i <= $index_del; ++$i) { // if no case apply then index_del = -1
                    $dayhubcompaniestotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][$i]                             += $one_result->delivered;
                }
            }
            ////////////////////////

        }

        // adjust calculations
        // - add delivered to dispatched
        // - calculate zero attempts
        // - add signedin to picked up (not in the hubs tables)
        if(in_array('71', $companies_init)) {
            $counter = 0;
            foreach($jollychic_group as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        if(in_array('73', $companies_init)) {
            $counter = 0;
            foreach($jollychic_local as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        if(in_array('74', $companies_init)) {
            $counter = 0;
            foreach($jollychic_global as $one_item) {
                if(!in_array($one_item, $companies_init)) {
                    array_push($companies_init, $one_item);
                    ++$counter;
                }
            }
        }
        foreach($companies_init as $one_company) {
            foreach($years as $one_year) {
                foreach($months[$one_year] as $one_month) {
    
                    ////////////////////////
                    ///// months totals
                    for($i = 0; $i < 5; ++$i) {
                        $monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i] += $monthcompaniestotal_del[$one_year][$one_month][$one_company][$i];
                    }
                    $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] += $monthcompaniestotal_signedin[$one_year][$one_month][$one_company];
                    $monthcompaniestotal_zeroattempts[$one_year][$one_month][$one_company] = $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] - $monthcompaniestotal_dis[$one_year][$one_month][$one_company][0];
                    foreach($cities as $one_city) {
                        for($i = 0; $i < 5; ++$i) {
                            $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i] += $monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i];
                        }
                        $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] += $monthcitycompaniestotal_signedin[$one_year][$one_month][$one_city][$one_company];
                        $monthcitycompaniestotal_zeroattempts[$one_year][$one_month][$one_city][$one_company] = $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] - $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][0];
                    }
                    foreach($hubs as $one_hub) {
                        for($i = 0; $i < 5; ++$i) {
                            $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] += $monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i];
                        }
                        $monthhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_hub][$one_company] = $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company] - $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][0];
                    }
                    ////////////////////////
    
                    ////////////////////////
                    ///// days totals
                    foreach($days[$one_year][$one_month] as $one_day) {
                        for($i = 0; $i < 5; ++$i) {
                            $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i] += $daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i];
                        }
                        $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] += $daycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_company];
                        $daycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_company] = $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] - $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][0];
                        foreach($cities as $one_city) {
                            for($i = 0; $i < 5; ++$i) {
                                $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] += $daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i];
                            }
                            $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] += $daycitycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_city][$one_company];
                            $daycitycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_city][$one_company] = $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] - $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][0];
                        }
                        foreach($hubs as $one_hub) {
                            for($i = 0; $i < 5; ++$i) {
                                $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] += $dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i];
                            }
                            $dayhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub][$one_company] = $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company] - $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][0];
                        }
                    }
                    ////////////////////////
    
                }
            }
        }
        if(in_array('74', $companies_init)) {
            foreach($jollychic_global as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }
        if(in_array('73', $companies_init)) {
            foreach($jollychic_local as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }
        if(in_array('71', $companies_init)) {
            foreach($jollychic_group as $one_item) {
                if(!$counter) break;
                array_pop($companies_init);
                --$counter;
            }
        }

        // calculate the companies days row in the month totals tables
        foreach($daysrow_result as $one_result) {

            /////////////////
            // month totals
            $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->company_id][4] += $one_result->dispatched; 

            $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['71'][4] += $one_result->dispatched; 
    
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['73'][4] += $one_result->dispatched; 
    
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month]['74'][4] += $one_result->dispatched; 
    
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcompaniesdaysrowtotal_del[$one_result->year][$one_result->month]['74'][4] += $one_result->delivered; 
            }
            ///////////////////

            ///////////////////
            // month city totals
            $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][4] += $one_result->dispatched; 

            $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][4] += $one_result->dispatched; 
    
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][4] += $one_result->dispatched; 
    
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthcitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][4] += $one_result->dispatched; 
    
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthcitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->main_city_id]['74'][4] += $one_result->delivered; 
            }
            ///////////////////

            ///////////////////
            // month hub totals
            $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][4] += $one_result->dispatched; 

            $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['71'][4] += $one_result->dispatched; 
    
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['73'][4] += $one_result->dispatched; 
    
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $monthhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->hub_id]['74'][4] += $one_result->dispatched; 
    
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $monthhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->hub_id]['74'][4] += $one_result->delivered; 
            }
            ///////////////////

            ///////////////////
            // day totals
            $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][4] += $one_result->dispatched; 

            $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['71'][4] += $one_result->dispatched; 
    
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['73'][4] += $one_result->dispatched; 
    
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day]['74'][4] += $one_result->dispatched; 
    
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day]['74'][4] += $one_result->delivered; 
            }
            ///////////////////

            ///////////////////
            // day city totals
            $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][4] += $one_result->dispatched; 

            $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][4] += $one_result->dispatched; 
    
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][4] += $one_result->dispatched; 
    
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $daycitycompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][4] += $one_result->dispatched; 
    
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $daycitycompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->main_city_id]['74'][4] += $one_result->delivered; 
            }
            ///////////////////

            ///////////////////
            // day hub totals
            $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
            $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
            $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
            $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
            $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][4] += $one_result->dispatched; 

            $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
            $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
            $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
            $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
            $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id][$one_result->company_id][4] += $one_result->delivered; 
            if(in_array('71', $companies_init) && in_array($one_result->company_id, $jollychic_group)) {
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][4] += $one_result->dispatched; 
    
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['71'][4] += $one_result->delivered; 
            }
            if(in_array('73', $companies_init) && in_array($one_result->company_id, $jollychic_local)) {
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][4] += $one_result->dispatched; 
    
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['73'][4] += $one_result->delivered; 
            }
            if(in_array('74', $companies_init) && in_array($one_result->company_id, $jollychic_global)) {
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->dispatched; 
                $dayhubcompaniesdaysrowtotal_dis[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][4] += $one_result->dispatched; 
    
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][0] += ($one_result->first_attempt == 0) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][1] += ($one_result->first_attempt == 1) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][2] += ($one_result->first_attempt == 2) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][3] += ($one_result->first_attempt >= 3) * $one_result->delivered; 
                $dayhubcompaniesdaysrowtotal_del[$one_result->year][$one_result->month][$one_result->day][$one_result->hub_id]['74'][4] += $one_result->delivered; 
            }
            ///////////////////
        }

        $memory_used_so_far = memory_get_usage();
        ?>

        <script>
            console.log('--- End of all memory allocation ---');
            console.log('Memory used: {{ $memory_used_so_far }}');
        </script>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    @foreach($years as $one_year)
                        @foreach($months[$one_year] as $one_month)
                            <div class="x_panel tile overflow_hidden row padding-0">
                                <h1>{{ date('M', strtotime("$one_year-$one_month")) }} {{ $one_year }} </h1>
                                <hr/>
                                <div class="tile overflow_hidden row">
                                    <div class="x_title table-responsive">

                                        <!-- Total of The Month -->
                                        <div style="text-align: right;">
                                            <button class="btn btn-primary" onclick="monthtotal_{{ $one_month }}_{{ $one_year }}_click()" id="monthtotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                        </div>
                                        <div style="overflow-x:auto; overflow-y:hidden;">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th colspan="27" class="border-all-thick">Total of The Month</th>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="4" class="border-left-thick"></th>
                                                        <th rowspan="4" class="border-left">Total Picked Up</th>
                                                        <th rowspan="3" colspan="2" class="border-left">Total Signed In</th>
                                                        <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                                        <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <th class="border-left">Dis.</th>
                                                            <th>Del.</th>
                                                            <th>Dis. S.R.</th>
                                                            <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                                        @endfor
                                                    </tr>
                                                    <tr>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                        <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                        <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                    </tr>
                                                    <tr>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <td class="gray-background border-left ">{{ number_format($monthdaysrowtotal_dis[$one_year][$one_month][$i]) }}</td>
                                                            <td class="gray-background">{{ number_format($monthdaysrowtotal_del[$one_year][$one_month][$i]) }}</td>
                                                            <td class="gray-background {{ $monthdaysrowtotal_dis[$one_year][$one_month][$i] > 0 ? ($monthdaysrowtotal_del[$one_year][$one_month][$i] / $monthdaysrowtotal_dis[$one_year][$one_month][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthdaysrowtotal_del[$one_year][$one_month][$i] / max(1, $monthdaysrowtotal_dis[$one_year][$one_month][$i]) * 100.0, 2) }}%</td>
                                                            <td class="gray-background {{ $total_pickedup > 0 ? ($monthdaysrowtotal_del[$one_year][$one_month][$i] / $total_pickedup * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthdaysrowtotal_del[$one_year][$one_month][$i] / max(1, $total_pickedup) * 100.0, 2) }}%</td>
                                                        @endfor
                                                    </tr>
                                                    <tr>
                                                        <th class="border-left">Count</th>
                                                        <th>Perc.</th>
                                                        <th class="border-left">Count</th>
                                                        <th>Perc.</th>
                                                        <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                        <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                        <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                        <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                        <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="border-left-thick border-bottom-thick"></td>
                                                        <td class="border-left border-bottom-thick">{{ number_format($monthtotal_pickedup[$one_year][$one_month]) }}</td>
                                                        <td class="border-left border-bottom-thick">{{ number_format($monthtotal_signedin[$one_year][$one_month]) }}</td>
                                                        <td class="border-bottom-thick">{{ number_format($monthtotal_signedin[$one_year][$one_month] / max(1, $monthtotal_pickedup[$one_year][$one_month]) * 100.0, 2) }}%</td>
                                                        <td class="border-left border-bottom-thick">{{ number_format(1.0 * $monthtotal_avg_dispatched_days[$one_year][$one_month] / max(1, $monthtotal_avg_dispatched_count[$one_year][$one_month]), 2) }}</td>
                                                        <td class="border-left border-bottom-thick">
                                                            <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                {{ number_format($monthtotal_zeroattempts[$one_year][$one_month]) }}
                                                            </a>
                                                        </td>
                                                        <td class="border-bottom-thick">{{ number_format($monthtotal_zeroattempts[$one_year][$one_month] / max(1, $monthtotal_pickedup[$one_year][$one_month]) * 100.0, 2) }}%</td>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <td class="border-left border-bottom-thick">{{ number_format($monthtotal_dis[$one_year][$one_month][$i]) }}</td>
                                                            <td class="border-bottom-thick">{{ number_format($monthtotal_del[$one_year][$one_month][$i]) }}</td>
                                                            <td class="{{ $monthtotal_dis[$one_year][$one_month][$i] > 0 ? ($monthtotal_del[$one_year][$one_month][$i] / $monthtotal_dis[$one_year][$one_month][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($monthtotal_del[$one_year][$one_month][$i] / max(1, $monthtotal_dis[$one_year][$one_month][$i]) * 100.0, 2) }}%</td>
                                                            <td class="{{ $monthtotal_pickedup[$one_year][$one_month] > 0 ? ($monthtotal_del[$one_year][$one_month][$i] / $monthtotal_pickedup[$one_year][$one_month] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthtotal_del[$one_year][$one_month][$i] / max(1, $monthtotal_pickedup[$one_year][$one_month]) * 100.0, 2) }}%</td>
                                                        @endfor
                                                    </tr>
                                                    <tr class="monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        <th colspan="27" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                            <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]) }}</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($monthcompaniestotal_signedin[$one_year][$one_month][$one_company]) }}</td>
                                                            <td rowspan="4" class="border-top border-bottom">{{ number_format($monthcompaniestotal_signedin[$one_year][$one_month][$one_company] / max(1, $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]) * 100.0, 2) }}%</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $monthcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_company] / max(1, $monthcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_company]), 2) }}</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                    {{ number_format($monthcompaniestotal_zeroattempts[$one_year][$one_month][$one_company]) }}
                                                                </a>
                                                            </td>
                                                            <td rowspan="4" class="border-top border-bottom">{{ number_format($monthcompaniestotal_zeroattempts[$one_year][$one_month][$one_company] / max(1, $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]) * 100.0, 2) }}%</td>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                        </tr>
                                                        <tr class="monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-left">{{ number_format($monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company][$i]) }}</td>
                                                                <td class="">{{ number_format($monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i]) }}</td>
                                                                <td class="{{ $monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company][$i] > 0 ? ($monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i] / $monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i] / max(1, $monthcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-right {{ $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] > 0 ? ($monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i] / $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcompaniesdaysrowtotal_del[$one_year][$one_month][$one_company][$i] / max(1, $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        </tr>
                                                        <tr class="monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                        </tr>
                                                        <tr class="monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-bottom border-left">{{ number_format($monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i]) }}</td>
                                                                <td class="border-bottom">{{ number_format($monthcompaniestotal_del[$one_year][$one_month][$one_company][$i]) }}</td>
                                                                <td class="border-bottom {{ $monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i] > 0 ? ($monthcompaniestotal_del[$one_year][$one_month][$one_company][$i] / $monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcompaniestotal_del[$one_year][$one_month][$one_company][$i] / max(1, $monthcompaniestotal_dis[$one_year][$one_month][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-bottom border-right {{ $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] > 0 ? ($monthcompaniestotal_del[$one_year][$one_month][$one_company][$i] / $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcompaniestotal_del[$one_year][$one_month][$one_company][$i] / max(1, $monthcompaniestotal_pickedup[$one_year][$one_month][$one_company]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <script>
                                                function monthtotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                    var flag = document.getElementById('monthtotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                    var rows = document.getElementsByClassName('monthtotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                    console.log('monthtotal_{{ $one_month }}_{{ $one_year }}_button');
                                                    console.log(flag);
                                                    if(flag == 'Show Companies') {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = '';
                                                        });
                                                        document.getElementById('monthtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                    }
                                                    else {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = 'none';
                                                        });
                                                        document.getElementById('monthtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                    }
                                                }
                                            </script>
                                        </div>
                                        <hr>

                                        <!-- Total of The Month By City -->
                                        <div style="text-align: right;">
                                            <button class="btn btn-primary" onclick="monthcitytotal_{{ $one_month }}_{{ $one_year }}_click()" id="monthcitytotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                        </div>
                                        <div style="overflow-x:auto; overflow-y:hidden;">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            <th colspan="27" class="border-all-thick">{{ $cities_map[$one_city]->name }}</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            <th rowspan="4" class="border-left-thick"></th>
                                                            <th rowspan="4" class="border-left">Total Picked Up</th>
                                                            <th rowspan="3" colspan="2" class="border-left">Total Signed In</th>
                                                            <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                                            <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <th class="border-left">Dis.</th>
                                                                <th>Del.</th>
                                                                <th>Dis. S.R.</th>
                                                                <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="gray-background border-left ">{{ number_format($monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city][$i]) }}</td>
                                                                <td class="gray-background">{{ number_format($monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i]) }}</td>
                                                                <td class="gray-background {{ $monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city][$i] > 0 ? ($monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i] / $monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i] / max(1, $monthcitydaysrowtotal_dis[$one_year][$one_month][$one_city][$i]) * 100.0, 2) }}%</td>
                                                                <td class="gray-background {{ $monthcitytotal_pickedup[$one_year][$one_month][$one_city] > 0 ? ($monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i] / $monthcitytotal_pickedup[$one_year][$one_month][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthcitydaysrowtotal_del[$one_year][$one_month][$one_city][$i] / max(1, $monthcitytotal_pickedup[$one_year][$one_month][$one_city]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            <th class="border-left">Count</th>
                                                            <th>Perc.</th>
                                                            <th class="border-left">Count</th>
                                                            <th>Perc.</th>
                                                            <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        @foreach($cities as $one_city)
                                                            <td class="border-left-thick border-bottom-thick"></td>
                                                            <td class="border-left border-bottom-thick">{{ number_format($monthcitytotal_pickedup[$one_year][$one_month][$one_city]) }}</td>
                                                            <td class="border-left border-bottom-thick">{{ number_format($monthcitytotal_signedin[$one_year][$one_month][$one_city]) }}</td>
                                                            <td class="border-bottom-thick">{{ number_format($monthcitytotal_signedin[$one_year][$one_month][$one_city] / max(1, $monthcitytotal_pickedup[$one_year][$one_month][$one_city]) * 100.0, 2) }}%</td>
                                                            <td class="border-left border-bottom-thick">{{ number_format(1.0 * $monthcitytotal_avg_dispatched_days[$one_year][$one_month][$one_city] / max(1, $monthcitytotal_avg_dispatched_count[$one_year][$one_month][$one_city]), 2) }}</td>
                                                            <td class="border-left border-bottom-thick">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $one_city }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                    {{ number_format($monthcitytotal_zeroattempts[$one_year][$one_month][$one_city]) }}
                                                                </a>
                                                            </td>
                                                            <td class="border-bottom-thick">{{ number_format($monthcitytotal_zeroattempts[$one_year][$one_month][$one_city] / max(1, $monthcitytotal_pickedup[$one_year][$one_month][$one_city]) * 100.0, 2) }}%</td>
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-left border-bottom-thick">{{ number_format($monthcitytotal_dis[$one_year][$one_month][$one_city][$i]) }}</td>
                                                                <td class="border-bottom-thick">{{ number_format($monthcitytotal_del[$one_year][$one_month][$one_city][$i]) }}</td>
                                                                <td class="{{ $monthcitytotal_dis[$one_year][$one_month][$one_city][$i] > 0 ? ($monthcitytotal_del[$one_year][$one_month][$one_city][$i] / $monthcitytotal_dis[$one_year][$one_month][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($monthcitytotal_del[$one_year][$one_month][$one_city][$i] / max(1, $monthcitytotal_dis[$one_year][$one_month][$one_city][$i]) * 100.0, 2) }}%</td>
                                                                <td class="{{ $monthcitytotal_pickedup[$one_year][$one_month][$one_city] > 0 ? ($monthcitytotal_del[$one_year][$one_month][$one_city][$i] / $monthcitytotal_pickedup[$one_year][$one_month][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthcitytotal_del[$one_year][$one_month][$one_city][$i] / max(1, $monthcitytotal_pickedup[$one_year][$one_month][$one_city]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr class="monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        @foreach($cities as $one_city)
                                                            <th colspan="27" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                                        @endforeach
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($monthcitycompaniestotal_signedin[$one_year][$one_month][$one_city][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($monthcitycompaniestotal_signedin[$one_year][$one_month][$one_city][$one_company] / max(1, $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $monthcitycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_city][$one_company] / max(1, $monthcitycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_city][$one_company]), 2) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom">
                                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $one_city }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                        {{ number_format($monthcitycompaniestotal_zeroattempts[$one_year][$one_month][$one_city][$one_company]) }}
                                                                    </a>
                                                                </td>
                                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($monthcitycompaniestotal_zeroattempts[$one_year][$one_month][$one_city][$one_company] / max(1, $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-left">{{ number_format($monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="">{{ number_format($monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="{{ $monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company][$i] > 0 ? ($monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i] / $monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i] / max(1, $monthcitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_city][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-right {{ $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] > 0 ? ($monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i] / $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_city][$one_company][$i] / max(1, $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-bottom border-left">{{ number_format($monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom">{{ number_format($monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom {{ $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i] > 0 ? ($monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i] / $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i] / max(1, $monthcitycompaniestotal_dis[$one_year][$one_month][$one_city][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-bottom border-right {{ $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] > 0 ? ($monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i] / $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthcitycompaniestotal_del[$one_year][$one_month][$one_city][$one_company][$i] / max(1, $monthcitycompaniestotal_pickedup[$one_year][$one_month][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <script>
                                                function monthcitytotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                    var flag = document.getElementById('monthcitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                    var rows = document.getElementsByClassName('monthcitytotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                    if(flag == 'Show Companies') {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = '';
                                                        });
                                                        document.getElementById('monthcitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                    }
                                                    else {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = 'none';
                                                        });
                                                        document.getElementById('monthcitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                    }
                                                }
                                            </script>
                                        </div>
                                        <hr>

                                        <!-- Total of The Month By Hub -->
                                        <div style="text-align: right;">
                                            <button class="btn btn-primary" onclick="monthhubtotal_{{ $one_month }}_{{ $one_year }}_click()" id="monthhubtotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                        </div>
                                        <div style="overflow-x:auto; overflow-y:hidden;">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            <th colspan="25" class="border-all-thick">{{ $hubs_map[$one_hub]->name }}</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            <th rowspan="4" class="border-left-thick"></th>
                                                            <th rowspan="4" class="border-left">Total Signed In</th>
                                                            <th rowspan="4" class="border-left">Average Days To First Dispatch</th>
                                                            <th rowspan="3" colspan="2" class="border-left">0 Attempts</th>
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <th class="border-left">Dis.</th>
                                                                <th>Del.</th>
                                                                <th>Dis. S.R.</th>
                                                                <th class="{{ $i == 4 ? 'border-right-thick' : '' }}">Signed. S.R.</th>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="gray-background border-left ">{{ number_format($monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub][$i]) }}</td>
                                                                <td class="gray-background">{{ number_format($monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i]) }}</td>
                                                                <td class="gray-background {{ $monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub][$i] > 0 ? ($monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i] / $monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i] / max(1, $monthhubdaysrowtotal_dis[$one_year][$one_month][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                                <td class="gray-background {{ $monthhubtotal_signedin[$one_year][$one_month][$one_hub] > 0 ? ($monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i] / $monthhubtotal_signedin[$one_year][$one_month][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthhubdaysrowtotal_del[$one_year][$one_month][$one_hub][$i] / max(1, $monthhubtotal_signedin[$one_year][$one_month][$one_hub]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            <th class="border-left">Count</th>
                                                            <th>Perc.</th>
                                                            <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        @foreach($hubs as $one_hub)
                                                            <td class="border-left-thick border-bottom-thick"></td>
                                                            <td class="border-left border-bottom-thick">{{ number_format($monthhubtotal_signedin[$one_year][$one_month][$one_hub]) }}</td>
                                                            <td class="border-left border-bottom-thick">{{ number_format(1.0 * $monthhubtotal_avg_dispatched_days[$one_year][$one_month][$one_hub] / max(1, $monthhubtotal_avg_dispatched_count[$one_year][$one_month][$one_hub]), 2) }}</td>
                                                            <td class="border-left border-bottom-thick">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&hubs={{ $one_hub }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                    {{ number_format($monthhubtotal_zeroattempts[$one_year][$one_month][$one_hub]) }}
                                                                </a>
                                                            </td>
                                                            <td class="border-bottom-thick">{{ number_format($monthhubtotal_zeroattempts[$one_year][$one_month][$one_hub] / max(1, $monthhubtotal_signedin[$one_year][$one_month][$one_hub]) * 100.0, 2) }}%</td>
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-left border-bottom-thick">{{ number_format($monthhubtotal_dis[$one_year][$one_month][$one_hub][$i]) }}</td>
                                                                <td class="border-bottom-thick">{{ number_format($monthhubtotal_del[$one_year][$one_month][$one_hub][$i]) }}</td>
                                                                <td class="{{ $monthhubtotal_dis[$one_year][$one_month][$one_hub][$i] > 0 ? ($monthhubtotal_del[$one_year][$one_month][$one_hub][$i] / $monthhubtotal_dis[$one_year][$one_month][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick">{{ number_format($monthhubtotal_del[$one_year][$one_month][$one_hub][$i] / max(1, $monthhubtotal_dis[$one_year][$one_month][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                                <td class="{{ $monthhubtotal_signedin[$one_year][$one_month][$one_hub] > 0 ? ($monthhubtotal_del[$one_year][$one_month][$one_hub][$i] / $monthhubtotal_signedin[$one_year][$one_month][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} border-bottom-thick {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($monthhubtotal_del[$one_year][$one_month][$one_hub][$i] / max(1, $monthhubtotal_signedin[$one_year][$one_month][$one_hub]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr class="monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        @foreach($hubs as $one_hub)
                                                            <th colspan="25" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                                        @endforeach
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                <th rowspan="4" class="border-top border-left border-bottom white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format($monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom">{{ number_format(1.0 * $monthhubcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_hub][$one_company] / max(1, $monthhubcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_hub][$one_company]), 2) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom">
                                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&hubs={{ $one_hub }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ date('d', strtotime($from)) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-31">
                                                                        {{ number_format($monthhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_hub][$one_company]) }}
                                                                    </a>
                                                                </td>
                                                                <td rowspan="4" class="border-top border-bottom">{{ number_format($monthhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_hub][$one_company] / max(1, $monthhubcompaniestotal_pickedup[$one_year][$one_month][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-left">{{ number_format($monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="">{{ number_format($monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="{{ $monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] > 0 ? ($monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / $monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / max(1, $monthhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_hub][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-right {{ $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company] > 0 ? ($monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / max(1, $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-bottom border-left">{{ number_format($monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom">{{ number_format($monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom {{ $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] > 0 ? ($monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / max(1, $monthhubcompaniestotal_dis[$one_year][$one_month][$one_hub][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-bottom border-right {{ $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company] > 0 ? ($monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($monthhubcompaniestotal_del[$one_year][$one_month][$one_hub][$one_company][$i] / max(1, $monthhubcompaniestotal_signedin[$one_year][$one_month][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <script>
                                                function monthhubtotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                    var flag = document.getElementById('monthhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                    var rows = document.getElementsByClassName('monthhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                    if(flag == 'Show Companies') {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = '';
                                                        });
                                                        document.getElementById('monthhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                    }
                                                    else {
                                                        [].forEach.call(rows, function(e) {
                                                            e.style.display = 'none';
                                                        });
                                                        document.getElementById('monthhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                    }
                                                }
                                            </script>
                                        </div>
                                        <br>

                                        <ul class="nav navbar-left panel_toolbox">
                                            <li>
                                                <a style="color: black !important;" class="collapse-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseData">
                                                    <i class="fa fa-chevron-down"></i> &nbsp; <b>Show Days</b>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div id="collapseTwo" class="x_content collapse table-responsive" aria-labelledby="headingTwo" data-parent="#accordion">
                                    
                                    <!-- Total of The Days -->
                                    <div style="text-align: right;">
                                        <button class="btn btn-primary" onclick="daytotal_{{ $one_month }}_{{ $one_year }}_click()" id="daytotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                    </div>
                                    <div style="overflow-x:auto; overflow-y:hidden;">
                                        <table id="total_of_days" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th rowspan="3" class="border-top-thick border-left-thick">DOM</th>
                                                    <th colspan="27" class="border-all-thick">Total of The Days</th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="2" class="border-left-thick"></th>
                                                    <th rowspan="2" class="border-left">Total Picked Up</th>
                                                    <th colspan="2" class="border-left">Total Signed In</th>
                                                    <th rowspan="2" class="border-left">Average Days To First Dispatch</th>
                                                    <th colspan="2" class="border-left">0 Attempts</th>
                                                    @for($i = 0; $i < 5; ++$i)
                                                        <th rowspan="2" class="border-left">Dis.</th>
                                                        <th rowspan="2">Del.</th>
                                                        <th rowspan="2">Dis. S.R.</th>
                                                        <th rowspan="2" class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                                    @endfor
                                                </tr>
                                                <tr>
                                                    <th class=" border-left">Count</th>
                                                    <th class="">Perc.</th>
                                                    <th class=" border-left">Count</th>
                                                    <th class="">Perc.</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $len = count($days[$one_year][$one_month]); ?>
                                                @foreach($days[$one_year][$one_month] as $one_day)
                                                    @if($len != count($days[$one_year][$one_month]))
                                                        <tr>
                                                            <td colspan="28" class="border-left-thick border-right-thick border-bottom-thick daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;"></td>
                                                        </tr>
                                                    @endif
                                                    <?php --$len; ?>
                                                    <tr>
                                                        <td rowspan="4" class="border-left-thick border-bottom-thick companies-size-affect-day_{{ $one_month }}_{{ $one_year }}">{{ $one_day }}</td>
                                                        <td rowspan="4" class="border-left-thick border-bottom-thick"></td>
                                                        <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daytotal_pickedup[$one_year][$one_month][$one_day]) }}</td>
                                                        <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daytotal_signedin[$one_year][$one_month][$one_day]) }}</td>
                                                        <td rowspan="4" class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daytotal_signedin[$one_year][$one_month][$one_day] / max(1, $daytotal_pickedup[$one_year][$one_month][$one_day]) * 100.0, 2) }}%</td>
                                                        <td rowspan="4" class="border-bottom-thick border-left {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format(1.0 * $daytotal_avg_dispatched_days[$one_year][$one_month][$one_day] / max(1, $daytotal_avg_dispatched_count[$one_year][$one_month][$one_day]), 2) }}</td>
                                                        <td rowspan="4" class="border-bottom-thick border-left {{ $len == 0 ? 'border-bottom-thick' : '' }}">
                                                            <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                {{ number_format($daytotal_zeroattempts[$one_year][$one_month][$one_day]) }}
                                                            </a>
                                                        </td>
                                                        <td rowspan="4" class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daytotal_zeroattempts[$one_year][$one_month][$one_day] / max(1, $daytotal_pickedup[$one_year][$one_month][$one_day]) * 100.0, 2) }}%</td>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                        <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                        <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                        <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                    </tr>
                                                    <tr>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <td class="gray-background border-left">{{ number_format($daydaysrowtotal_dis[$one_year][$one_month][$one_day][$i]) }}</td>
                                                            <td class="gray-background">{{ number_format($daydaysrowtotal_del[$one_year][$one_month][$one_day][$i]) }}</td>
                                                            <td class="gray-background {{ $daydaysrowtotal_dis[$one_year][$one_month][$one_day][$i] > 0 ? ($daydaysrowtotal_del[$one_year][$one_month][$one_day][$i] / $daydaysrowtotal_dis[$one_year][$one_month][$one_day][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daydaysrowtotal_del[$one_year][$one_month][$one_day][$i] / max(1, $daydaysrowtotal_dis[$one_year][$one_month][$one_day][$i]) * 100.0, 2) }}%</td>
                                                            <td class="gray-background {{ $daytotal_pickedup[$one_year][$one_month][$one_day] > 0 ? ($daydaysrowtotal_del[$one_year][$one_month][$one_day][$i] / $daytotal_pickedup[$one_year][$one_month][$one_day] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($daydaysrowtotal_del[$one_year][$one_month][$one_day][$i] / max(1, $daytotal_pickedup[$one_year][$one_month][$one_day]) * 100.0, 2) }}%</td>
                                                        @endfor
                                                    </tr>
                                                    <tr>
                                                        <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                        <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                        <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                        <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                        <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                    </tr>
                                                    <tr>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} border-left ">{{ number_format($daytotal_dis[$one_year][$one_month][$one_day][$i]) }}</td>
                                                            <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daytotal_del[$one_year][$one_month][$one_day][$i]) }}</td>
                                                            <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $daytotal_dis[$one_year][$one_month][$one_day][$i] > 0 ? ($daytotal_del[$one_year][$one_month][$one_day][$i] / $daytotal_dis[$one_year][$one_month][$one_day][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} ">{{ number_format($daytotal_del[$one_year][$one_month][$one_day][$i] / max(1, $daytotal_dis[$one_year][$one_month][$one_day][$i]) * 100.0, 2) }}%</td>
                                                            <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $daytotal_pickedup[$one_year][$one_month][$one_day] > 0 ? ($daytotal_del[$one_year][$one_month][$one_day][$i] / $daytotal_pickedup[$one_year][$one_month][$one_day] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }} ">{{ number_format($daytotal_del[$one_year][$one_month][$one_day][$i] / max(1, $daytotal_pickedup[$one_year][$one_month][$one_day]) * 100.0, 2) }}%</td>
                                                        @endfor
                                                    </tr>
                                                    <tr class="daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        <th colspan="27" class="border-left border-right border-top-thick" style="text-align: center;">Companies Details</th>
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            <th rowspan="4" class="border-top border-left border-bottom-thick white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                            <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format($daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]) }}</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format($daycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_company]) }}</td>
                                                            <td rowspan="4" class="border-top border-bottom-thick">{{ number_format($daycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_company] / max(1, $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]) * 100.0, 2) }}%</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format(1.0 * $daycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_company] / max(1, $daycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_company]), 2) }}</td>
                                                            <td rowspan="4" class="border-top border-left border-bottom-thick">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $all_cities_str }}&hubs={{ $all_hubs_str }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                    {{ number_format($daycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_company]) }}
                                                                </a>
                                                            </td>
                                                            <td rowspan="4" class="border-top border-bottom-thick">{{ number_format($daycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_company] / max(1, $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]) * 100.0, 2) }}%</td>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                        </tr>
                                                        <tr class="daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-left">{{ number_format($daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company][$i]) }}</td>
                                                                <td class="">{{ number_format($daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i]) }}</td>
                                                                <td class="{{ $daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company][$i] > 0 ? ($daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i] / $daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i] / max(1, $daycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-right {{ $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] > 0 ? ($daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i] / $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_company][$i] / max(1, $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        </tr>
                                                        <tr class="daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                        </tr>
                                                        <tr class="daytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-bottom-thick border-left">{{ number_format($daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i]) }}</td>
                                                                <td class="border-bottom-thick">{{ number_format($daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i]) }}</td>
                                                                <td class="border-bottom-thick {{ $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i] > 0 ? ($daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i] / $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i] / max(1, $daycompaniestotal_dis[$one_year][$one_month][$one_day][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-bottom-thick border-right {{ $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] > 0 ? ($daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i] / $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycompaniestotal_del[$one_year][$one_month][$one_day][$one_company][$i] / max(1, $daycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_company]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <script>
                                            function daytotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                var flag = document.getElementById('daytotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                var rows = document.getElementsByClassName('daytotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                var days = document.getElementsByClassName('companies-size-affect-day_{{ $one_month }}_{{ $one_year }}');
                                                if(flag == 'Show Companies') {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = '';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '{{ 5 + count($companies) * 4 }}';
                                                    });
                                                    document.getElementById('daytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                }
                                                else {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = 'none';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '4';
                                                    });
                                                    document.getElementById('daytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                }
                                            }
                                        </script>
                                    </div>
                                    <hr>
                                    
                                    <!-- Total of The Days By City -->
                                    <div style="text-align: right;">
                                        <button class="btn btn-primary" onclick="daycitytotal_{{ $one_month }}_{{ $one_year }}_click()" id="daycitytotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                    </div>
                                    <div style="overflow-x:auto; overflow-y:hidden;">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    @foreach($cities as $one_city)
                                                        <th rowspan="3" class="border-top-thick border-left-thick">DOM</th>
                                                        <th colspan="27" class="border-all-thick">{{ $cities_map[$one_city]->name }}</th>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    @foreach($cities as $one_city)
                                                        <th rowspan="2"></th>
                                                        <th rowspan="2" class="border-left-thick">Total Picked Up</th>
                                                        <th colspan="2" class="border-left">Total Signed In</th>
                                                        <th rowspan="2" class="border-left">Average Days To First Dispatch</th>
                                                        <th colspan="2" class="border-left">0 Attempts</th>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <th rowspan="2" class="border-left">Dis.</th>
                                                            <th rowspan="2">Del.</th>
                                                            <th rowspan="2">Dis. S.R.</th>
                                                            <th rowspan="2" class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                                        @endfor
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    @foreach($cities as $one_city)
                                                        <th class=" border-left">Count</th>
                                                        <th class="">Perc.</th>
                                                        <th class=" border-left">Count</th>
                                                        <th class="">Perc.</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $len = count($days[$one_year][$one_month]); ?>
                                                @foreach($days[$one_year][$one_month] as $one_day)
                                                    @if($len != count($days[$one_year][$one_month]))
                                                        <tr>
                                                            <td colspan="{{ 28 * count($cities) }}" class="border-left-thick border-right-thick border-bottom-thick daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;"></td>
                                                        </tr>
                                                    @endif
                                                    <?php --$len; ?>
                                                    <tr>
                                                        @foreach($cities as $one_city) 
                                                            <td rowspan="4" class="border-left-thick border-bottom-thick companies-size-affect-day_{{ $one_month }}_{{ $one_year }}city">{{ $one_day }}</td>
                                                            <td rowspan="4" class="border-left-thick border-bottom-thick"></td>
                                                            <td rowspan="4" class="border-left-thick border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]) }}</td>
                                                            <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daycitytotal_signedin[$one_year][$one_month][$one_day][$one_city]) }}</td>
                                                            <td rowspan="4" class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daycitytotal_signedin[$one_year][$one_month][$one_day][$one_city] / max(1, $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]) * 100.0, 2) }}%</td>
                                                            <td rowspan="4" class="border-bottom-thick border-left {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format(1.0 * $daycitytotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_city] / max(1, $daycitytotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_city]), 2) }}</td>
                                                            <td rowspan="4" class="border-bottom-thick border-left {{ $len == 0 ? 'border-bottom-thick' : '' }}">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&cities={{ $one_city }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                    {{ number_format($daycitytotal_zeroattempts[$one_year][$one_month][$one_day][$one_city]) }}
                                                                </a>
                                                            </td>
                                                            <td rowspan="4" class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daycitytotal_zeroattempts[$one_year][$one_month][$one_day][$one_city] / max(1, $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]) * 100.0, 2) }}%</td>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city) 
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="gray-background border-left">{{ number_format($daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$i]) }}</td>
                                                                <td class="gray-background">{{ number_format($daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i]) }}</td>
                                                                <td class="gray-background {{ $daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$i] > 0 ? ($daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i] / $daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i] / max(1, $daycitydaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$i]) * 100.0, 2) }}%</td>
                                                                <td class="gray-background {{ $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] > 0 ? ($daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i] / $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($daycitydaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$i] / max(1, $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city) 
                                                            <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($cities as $one_city) 
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} border-left ">{{ number_format($daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i]) }}</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i]) }}</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i] > 0 ? ($daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i] / $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} ">{{ number_format($daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i] / max(1, $daycitytotal_dis[$one_year][$one_month][$one_day][$one_city][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] > 0 ? ($daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i] / $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }} ">{{ number_format($daycitytotal_del[$one_year][$one_month][$one_day][$one_city][$i] / max(1, $daycitytotal_pickedup[$one_year][$one_month][$one_day][$one_city]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr class="daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        @foreach($cities as $one_city)
                                                            <th colspan="27" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                                        @endforeach
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                <th rowspan="4" class="border-top border-left border-bottom-thick white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format($daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format($daycitycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_city][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-bottom-thick">{{ number_format($daycitycompaniestotal_signedin[$one_year][$one_month][$one_day][$one_city][$one_company] / max(1, $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format(1.0 * $daycitycompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_city][$one_company] / max(1, $daycitycompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_city][$one_company]), 2) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">
                                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&cities={{ $one_city }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                        {{ number_format($daycitycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_city][$one_company]) }}
                                                                    </a>
                                                                </td>
                                                                <td rowspan="4" class="border-top border-bottom-thick">{{ number_format($daycitycompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_city][$one_company] / max(1, $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-left">{{ number_format($daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="">{{ number_format($daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="{{ $daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] > 0 ? ($daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / $daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / max(1, $daycitycompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-right {{ $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] > 0 ? ($daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycitycompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / max(1, $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                        <tr class="daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($cities as $one_city)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-bottom-thick border-left">{{ number_format($daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom-thick">{{ number_format($daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom-thick {{ $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] > 0 ? ($daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / max(1, $daycitycompaniestotal_dis[$one_year][$one_month][$one_day][$one_city][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-bottom-thick border-right {{ $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] > 0 ? ($daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($daycitycompaniestotal_del[$one_year][$one_month][$one_day][$one_city][$one_company][$i] / max(1, $daycitycompaniestotal_pickedup[$one_year][$one_month][$one_day][$one_city][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <script>
                                            function daycitytotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                var flag = document.getElementById('daycitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                var rows = document.getElementsByClassName('daycitytotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                var days = document.getElementsByClassName('companies-size-affect-day_{{ $one_month }}_{{ $one_year }}city');
                                                if(flag == 'Show Companies') {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = '';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '{{ 5 + count($companies) * 4 }}';
                                                    });
                                                    document.getElementById('daycitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                }
                                                else {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = 'none';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '4';
                                                    });
                                                    document.getElementById('daycitytotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                }
                                            }
                                        </script>
                                    </div>
                                    <hr>

                                    <!-- Total of The Days By Hub -->
                                    <div style="text-align: right;">
                                        <button class="btn btn-primary" onclick="dayhubtotal_{{ $one_month }}_{{ $one_year }}_click()" id="dayhubtotal_{{ $one_month }}_{{ $one_year }}_button"> Show Companies</button>
                                    </div>
                                    <div style="overflow-x:auto; overflow-y:hidden;">
                                    <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    @foreach($hubs as $one_hub)
                                                        <th rowspan="3" class="border-top-thick border-left-thick">DOM</th>
                                                        <th colspan="25" class="border-all-thick">{{ $hubs_map[$one_hub]->name }}</th>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    @foreach($hubs as $one_hub)
                                                        <th rowspan="2"></th>
                                                        <th rowspan="2" class="border-left">Total Signed In</th>
                                                        <th rowspan="2" class="border-left">Average Days To First Dispatch</th>
                                                        <th colspan="2" class="border-left">0 Attempts</th>
                                                        @for($i = 0; $i < 5; ++$i)
                                                            <th rowspan="2" class="border-left">Dis.</th>
                                                            <th rowspan="2">Del.</th>
                                                            <th rowspan="2">Dis. S.R.</th>
                                                            <th rowspan="2" class="{{ $i == 4 ? 'border-right-thick' : '' }}">Pic. S.R.</th>
                                                        @endfor
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    @foreach($hubs as $one_hub)
                                                        <th class=" border-left">Count</th>
                                                        <th class="">Perc.</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $len = count($days[$one_year][$one_month]); ?>
                                                @foreach($days[$one_year][$one_month] as $one_day)
                                                    @if($len != count($days[$one_year][$one_month]))
                                                        <tr>
                                                            <td colspan="{{ 27 * count($hubs) }}" class="border-left-thick border-right-thick border-bottom-thick dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;"></td>
                                                        </tr>
                                                    @endif
                                                    <?php --$len; ?>
                                                    <tr>
                                                        @foreach($hubs as $one_hub) 
                                                            <td rowspan="4" class="border-left-thick border-bottom-thick companies-size-affect-day_{{ $one_month }}_{{ $one_year }}hub">{{ $one_day }}</td>
                                                            <td rowspan="4" class="border-left-thick border-bottom-thick"></td>
                                                            <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub]) }}</td>
                                                            <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format(1.0 * $dayhubtotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_hub] / max(1, $dayhubtotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_hub]), 2) }}</td>
                                                            <td rowspan="4" class="border-left border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">
                                                                <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $all_companies_str }}&hubs={{ $one_hub }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                    {{ number_format($dayhubtotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub]) }}</td>
                                                                </a>
                                                            </td>
                                                            <td rowspan="4" class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($dayhubtotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub] / max(1, $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub]) * 100.0, 2) }}%</td>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                            <th colspan="4" class="border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                            <th colspan="4" class="border-left">Total First Attempt After 3 Days</th>
                                                            <th colspan="4" class="border-left border-right-thick">Total First Attempt Over All Days</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub) 
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="gray-background border-left">{{ number_format($dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i]) }}</td>
                                                                <td class="gray-background">{{ number_format($dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i]) }}</td>
                                                                <td class="gray-background {{ $dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] > 0 ? ($dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / $dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / max(1, $dayhubdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                                <td class="gray-background {{ $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub] > 0 ? ($dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }}">{{ number_format($dayhubdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / max(1, $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub) 
                                                            <th colspan="4" class="border-left">At Least 1 Attempt</th>
                                                            <th colspan="4" class="border-left">At Least 2 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 3 Attempts</th>
                                                            <th colspan="4" class="border-left">At Least 4 Attempts</th>
                                                            <th colspan="4" class="border-left border-right-thick">At Least 5 Attempts</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach($hubs as $one_hub) 
                                                            @for($i = 0; $i < 5; ++$i)
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} border-left ">{{ number_format($dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i]) }}</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }}">{{ number_format($dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i]) }}</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] > 0 ? ($dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }} ">{{ number_format($dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / max(1, $dayhubtotal_dis[$one_year][$one_month][$one_day][$one_hub][$i]) * 100.0, 2) }}%</td>
                                                                <td class="border-bottom-thick {{ $len == 0 ? 'border-bottom-thick' : '' }} {{ $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub] > 0 ? ($dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub] * 100.0 > $sr_color ? 'green' : 'red') : '' }} {{ $i == 4 ? 'border-right-thick' : '' }} ">{{ number_format($dayhubtotal_del[$one_year][$one_month][$one_day][$one_hub][$i] / max(1, $dayhubtotal_signedin[$one_year][$one_month][$one_day][$one_hub]) * 100.0, 2) }}%</td>
                                                            @endfor
                                                        @endforeach
                                                    </tr>

                                                    <tr class="dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                        @foreach($hubs as $one_hub)
                                                            <th colspan="25" class="border-left border-right" style="text-align: center;">Companies Details</th>
                                                        @endforeach
                                                    </tr>
                                                    @foreach($companies as $one_company) 
                                                        <tr class="dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                <th rowspan="4" class="border-top border-left border-bottom-thick white-background">{{ $companies_map[$one_company]->company_name }}</th>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format($dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company]) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">{{ number_format(1.0 * $dayhubcompaniestotal_avg_dispatched_days[$one_year][$one_month][$one_day][$one_hub][$one_company] / max(1, $dayhubcompaniestotal_avg_dispatched_count[$one_year][$one_month][$one_day][$one_hub][$one_company]), 2) }}</td>
                                                                <td rowspan="4" class="border-top border-left border-bottom-thick">
                                                                    <a target="_blank" class="url" href="{{ $export_zero_attempts_link }}&companies={{ $one_company }}&hubs={{ $one_hub }}&from={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}&to={{ $one_year }}-{{ str_pad($one_month, 2, '0', STR_PAD_LEFT) }}-{{ str_pad($one_day, 2, '0', STR_PAD_LEFT) }}">
                                                                        {{ number_format($dayhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub][$one_company]) }}
                                                                    </a>
                                                                </td>
                                                                <td rowspan="4" class="border-top border-bottom-thick">{{ number_format($dayhubcompaniestotal_zeroattempts[$one_year][$one_month][$one_day][$one_hub][$one_company] / max(1, $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 1<sup>st</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 2<sup>nd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">1<sup>st</sup> Attempt On 3<sup>rd</sup> Day</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left">Total First Attempt After 3 Days</th>
                                                                <th colspan="4" class="border-top border-bottom-gray border-left border-right">Total First Attempt Over All Days</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-left">{{ number_format($dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="">{{ number_format($dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="{{ $dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] > 0 ? ($dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / $dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / max(1, $dayhubcompaniesdaysrowtotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-right {{ $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company] > 0 ? ($dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($dayhubcompaniesdaysrowtotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / max(1, $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                        <tr class="dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 1 Attempt</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 2 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 3 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left">At Least 4 Attempts</th>
                                                                <th colspan="4" class="border-top-gray border-bottom-gray border-left border-right">At Least 5 Attempts</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide" style="display: none;">
                                                            @foreach($hubs as $one_hub)
                                                                @for($i = 0; $i < 5; ++$i)
                                                                    <td class="border-bottom-thick border-left">{{ number_format($dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom-thick">{{ number_format($dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) }}</td>
                                                                    <td class="border-bottom-thick {{ $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] > 0 ? ($dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / max(1, $dayhubcompaniestotal_dis[$one_year][$one_month][$one_day][$one_hub][$one_company][$i]) * 100.0, 2) }}%</td>
                                                                    <td class="border-bottom-thick border-right {{ $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company] > 0 ? ($dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company] * 100.0 > $sr_color ? 'green' : 'red') : '' }}">{{ number_format($dayhubcompaniestotal_del[$one_year][$one_month][$one_day][$one_hub][$one_company][$i] / max(1, $dayhubcompaniestotal_signedin[$one_year][$one_month][$one_day][$one_hub][$one_company]) * 100.0, 2) }}%</td>
                                                                @endfor
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <script>
                                            function dayhubtotal_{{ $one_month }}_{{ $one_year }}_click() {
                                                var flag = document.getElementById('dayhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText;
                                                var rows = document.getElementsByClassName('dayhubtotal_{{ $one_month }}_{{ $one_year }}-show-hide');
                                                var days = document.getElementsByClassName('companies-size-affect-day_{{ $one_month }}_{{ $one_year }}hub');
                                                if(flag == 'Show Companies') {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = '';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '{{ 5 + count($companies) * 4 }}';
                                                    });
                                                    document.getElementById('dayhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Hide Companies';
                                                }
                                                else {
                                                    [].forEach.call(rows, function(e) {
                                                        e.style.display = 'none';
                                                    });
                                                    [].forEach.call(days, function(e) {
                                                        e.rowSpan = '4';
                                                    });
                                                    document.getElementById('dayhubtotal_{{ $one_month }}_{{ $one_year }}_button').innerText = 'Show Companies';
                                                }
                                            }
                                        </script>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>

        
    
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

@stop