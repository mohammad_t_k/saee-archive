@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <style>
        table thead tr th {
            text-align: center;
        }
        table tbody tr td {
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-2 col-xs-12">
                    <h3>Insert Calls Record</h3>
                </div>
                <div class="col-md-3 col-xs-12">
                    <select name="username" id="username" class="form-control">
                        @foreach($customer_service as $user)
                            <option value="{{ $user->username }}">{{ $user->username }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 col-xs-12">
                    <input type="text" name="total" id="total" class="form-control" placeholder="number of calls" />
                </div>
                <div class="col-md-2 col-xs-12">
                    <input type="text" name="not_answered" id="not_answered" class="form-control" placeholder="not answered" />
                </div>
                <div class="col-md-3 col-xs-12">
                    <button id="insert_record" class="btn btn-primary" onclick="insert_record()">Insert</button>
                </div>
            </div>
        </div>

        <hr style="width: 50%;" />

        <div class="row tile_count">

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Warehouse </span>

                <div class="count"><?php echo number_format($inwarehouse) ?> </div>

            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Warehouse/Res </span>

                <div class="count"><?php echo number_format($rescheduled) ?> </div>

            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Delivered From Scheduling </span>

                <div class="count"><?php echo number_format($delivered_scheduled) ?> </div>

            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Delivered From Rescheduling </span>

                <div class="count"><?php echo number_format($delivered_rescheduled) ?> </div>

            </div>

        </div>

        <div class="row tile_count">

            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Cancelled By Customer </span>

                <div class="count"><?php echo number_format($customer_cancelled) ?> </div>

            </div>

            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Total Phone Calls </span>

                <div class="count green"><?php echo number_format($total_calls) ?> </div>

            </div>

            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cubes"></i> Not Answered </span>

                <div class="count red"><?php echo number_format($not_answered) ?> </div>

            </div>
            
        </div>

        @for($i = 0; $i < count($user_calls); $i += 6)
            <div class="row tile_count">
                
                @for($j = $i; $j < min(count($user_calls), 6); ++$j)
                
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-cubes"></i> {{ $user_calls[$j]->username }} </span>

                        <div class="count green"><?php echo number_format($user_calls[$j]->total_calls); ?> </div>
                        <div><?php echo "Not Answered ". number_format($user_calls[$j]->not_answered); ?></div>

                    </div>
                
                @endfor
                
            </div>
        @endfor
                
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/ticketsreportpost">
                        <input type="hidden" name="filter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companies as $compan)
                                            <option value="{{ $compan->id }}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{ $adcity->city }}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Status </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="status" id="status" class="form-control">
                                        <option value="all">All</option>
                                        @foreach($statuses as $stat)
                                            <option value="{{ $stat->id }}"<?php if (isset($status) && $status == $stat->id) echo 'selected';?> >{{ $stat->english }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ isset($from) ? $from : '' }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ isset($to) ? $to : '' }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <?php 
                        $total = [];
                        $all = 0;
                        $average = 0;
                        $days = 0;; // average days to close ticket: days of closed tickets / days all tickets
                        for($i = 1; $i <= 4; ++$i)
                            $total[$i] = 0;
                        foreach($result as $res) {
                            $total[$res->status] += $res->count;
                            $all += $res->count;
                            if($res->status == 3) {
                                $close = new DateTime($res->updated_at);
                                $open = new Datetime($res->created_at);
                                $diff = $close->diff($open)->days;
                                $days += $diff;
                            }
                        }
                        $average = $total[3] == 0 ? 0 : number_format($days / $total[3], 2);
                         ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 15%;">Total</th>
                                    @if(!isset($status) || $status == 'all' || $status == '1')
                                        <th style="width: 15%;">Opened</th>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '2')
                                        <th style="width: 15%;">In Action</th>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '3')
                                        <th style="width: 15%;">Closed</th>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '4')
                                        <th style="width: 20%;">Reopened</th>
                                    @endif
                                    <th style="width: 20%;">Averaga Days To Close Ticket</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width: 15%;">{{ $all }}</td>
                                    @if(!isset($status) || $status == 'all' || $status == '1')
                                        <td style="width: 15%;">{{ $total[1] }}</td>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '2')
                                        <td style="width: 15%;">{{ $total[2] }}</td>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '3')
                                        <td style="width: 15%;">{{ $total[3] }}</td>
                                    @endif
                                    @if(!isset($status) || $status == 'all' || $status == '4')
                                        <td style="width: 20%;">{{ $total[4] }}</td>
                                    @endif
                                    <td style="width: 20%;">{{ $average }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- ---------------------------------------------------------------------------------------------------------------------- Calculate Months Total Tables ------------------------- -->
                    <?php 
                    $month_name = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    $months = $years = $found = [];
                    for($i = 0; $i <= 12; ++$i)
                        $found[$i] = false;
                    foreach($result as $record) {
                        if(!$found[$record->month]) {
                            array_push($months, $record->month);
                            $found[$record->month] = true;
                        }
                        if(!isset($found[$record->year])) {
                            array_push($years, $record->year);
                            $found[$record->year] = true;
                        }
                    }
                    rsort($months);
                    rsort($years);
                    $total = $total_opened = $total_inaction = $total_closed = $total_reopened = $average = $days = [];
                    foreach($years as $year) {
                        $total[$year] = [];
                        $total_opened[$year] = [];
                        $total_inaction[$year] = [];
                        $total_closed[$year] = [];
                        $total_reopened[$year] = [];
                        $average[$year] = [];
                        $days[$year] = [];
                        foreach($months as $month) {
                            $total[$year][$month] = [];
                            $total_opened[$year][$month] = [];
                            $total_inaction[$year][$month] = [];
                            $total_closed[$year][$month] = [];
                            $total_reopened[$year][$month] = [];
                            $average[$year][$month] = [];
                            $days[$year][$month] = [];
                            foreach($cities as $city) {
                                $total[$year][$month][$city] = 0;
                                $total_opened[$year][$month][$city] = 0;
                                $total_inaction[$year][$month][$city] = 0;
                                $total_closed[$year][$month][$city] = 0;
                                $total_reopened[$year][$month][$city] = 0;
                                $average[$year][$month][$city] = 0;
                                $days[$year][$month][$city] = 0;
                            }
                            // avoid error in case of filtering by city
                            $city = 'All';
                            $total[$year][$month]['All'] = 0;
                            $total_opened[$year][$month]['All'] = 0;
                            $total_inaction[$year][$month]['All'] = 0;
                            $total_closed[$year][$month]['All'] = 0;
                            $total_reopened[$year][$month]['All'] = 0;
                            $average[$year][$month]['All'] = 0;
                            $days[$year][$month]['All'] = 0;
                        }
                    }
                    for($i = 0; $i < count($result); ++$i)
                        $result[$i]->d_city = trim($result[$i]->d_city);
                    foreach($result as $record) {
                        $record->d_city = $main_city[$record->d_city];
                        if($record->status == 1) {
                            $total_opened[$record->year][$record->month][$record->d_city] += $record->count;
                            $total_opened[$record->year][$record->month]['All'] += $record->count;
                        }
                        if($record->status == 2) {
                            $total_inaction[$record->year][$record->month][$record->d_city] += $record->count;
                            $total_inaction[$record->year][$record->month]['All'] += $record->count;
                        }
                        if($record->status == 3) {
                            $total_closed[$record->year][$record->month][$record->d_city] += $record->count;
                            $total_closed[$record->year][$record->month]['All'] += $record->count;

                            $close = new DateTime($record->updated_at);
                            $open = new Datetime($record->created_at);
                            $diff = $close->diff($open)->days;
                            $days[$record->year][$record->month][$record->d_city] += $diff;
                            $days[$record->year][$record->month]['All'] += $diff;
                        }
                        if($record->status == 4) {
                            $total_reopened[$record->year][$record->month][$record->d_city] += $record->count;
                            $total_reopened[$record->year][$record->month]['All'] += $record->count;
                        }
                        $total[$record->year][$record->month][$record->d_city] += $record->count;
                        $total[$record->year][$record->month]['All'] += $record->count;
                    }
                    foreach($years as $ye) {
                        foreach($months as $mo) {
                            foreach($cities as $ci) {
                                $average[$ye][$mo][$ci] = $total_closed[$ye][$mo][$ci] == 0 ? 0 : number_format($days[$ye][$mo][$ci] / $total_closed[$ye][$mo][$ci], 2);
                            }
                        }
                    }
                     ?>
                     @foreach($years as $year)
                        @foreach($months as $month)
                            <div class="x_panel tile fixed_height_580 overflow_hidden row" style="overflow-x:auto; overflow-y:auto;">
                                <h1>{{ $month_name[$month] }} {{ $year }}</h1><br/>
                                <div class="x_title table-responsive" style="overflow-x:auto; overflow-y:hidden;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <th colspan="<?php echo (!isset($status) || $status == 'all') ? 6 : 3; ?>">{{ $city == 'All' ? 'Total' : $city }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <th>Tot.</th>
                                                    @if(!isset($status) || $status == 'all' || $status == '1')
                                                        <th>Opn.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '2')
                                                        <th>Act.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '3')
                                                        <th>Cls.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '4')
                                                        <th>Rep</th>
                                                    @endif
                                                    <th>DAvg.</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <td>{{ $total[$year][$month][$city] }}</td>
                                                    @if(!isset($status) || $status == 'all' || $status == '1')
                                                        <td>{{ $total_opened[$year][$month][$city] }}</td>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '2')
                                                        <td>{{ $total_inaction[$year][$month][$city] }}</td>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '3')
                                                        <td>{{ $total_closed[$year][$month][$city] }}</td>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '4')
                                                        <td>{{ $total_reopened[$year][$month][$city] }}</td>
                                                    @endif
                                                    <td><b>{{ $average[$year][$month][$city] }}</b></td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="col horizontal-divider" style="margin-top: 30px"></div>
                                    <ul class="nav navbar-left panel_toolbox">
                                        <li><a class="collapse-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseData"><i class="fa fa-chevron-down"></i></a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- ------------------------------------------------------------------------------------------------------------------------ Calculate Days Table ----------------------- -->
                                <?php 

                                $d_total = $d_total_opened = $d_total_inaction = $d_total_closed = $d_total_reopened = $d_average = $d_days = [];
                                foreach($years as $ye) {

                                    $d_total[$ye] = [];
                                    $d_total_opened[$ye] = [];
                                    $d_total_inaction[$ye] = [];
                                    $d_total_closed[$ye] = [];
                                    $d_total_reopened[$ye] = [];
                                    $d_average[$ye] = [];
                                    $d_days[$ye] = [];

                                    foreach($months as $mo) {

                                        $d_total[$ye][$mo] = [];
                                        $d_total_opened[$ye][$mo] = [];
                                        $d_total_inaction[$ye][$mo] = [];
                                        $d_total_closed[$ye][$mo] = [];
                                        $d_total_reopened[$ye][$mo] = [];
                                        $d_average[$ye][$mo] = [];
                                        $d_days[$ye][$mo] = [];

                                        for($i = 0; $i <= 31; ++$i) {

                                            $d_total[$ye][$mo][$i] = [];
                                            $d_total_opened[$ye][$mo][$i] = [];
                                            $d_total_inaction[$ye][$mo][$i] = [];
                                            $d_total_closed[$ye][$mo][$i] = [];
                                            $d_total_reopened[$ye][$mo][$i] = [];
                                            $d_average[$ye][$mo][$i] = [];
                                            $d_days[$ye][$mo][$i] = [];

                                            foreach($cities as $city) {

                                                $d_total[$ye][$mo][$i][$city] = 0;
                                                $d_total_opened[$ye][$mo][$i][$city] = 0;
                                                $d_total_inaction[$ye][$mo][$i][$city] = 0;
                                                $d_total_closed[$ye][$mo][$i][$city] = 0;
                                                $d_total_reopened[$ye][$mo][$i][$city] = 0;
                                                $d_average[$ye][$mo][$i][$city] = 0;
                                                $d_days[$ye][$mo][$i][$city] = 0;

                                            }

                                            // avoid error in case of filtering by city
                                            $city = 'All';
                                            $d_total[$ye][$mo][$i]['All'] = 0;
                                            $d_total_opened[$ye][$mo][$i]['All'] = 0;
                                            $d_total_inaction[$ye][$mo][$i]['All'] = 0;
                                            $d_total_closed[$ye][$mo][$i]['All'] = 0;
                                            $d_total_reopened[$ye][$mo][$i]['All'] = 0;
                                            $d_average[$ye][$mo][$i]['All'] = 0;
                                            $d_days[$ye][$mo][$i]['All'] = 0;

                                        }
                                    }
                                }
                                for($i = 0; $i < count($result); ++$i)
                                    $result[$i]->d_city = trim($result[$i]->d_city);
                                foreach($result as $record) {
                                    $record->d_city = $main_city[$record->d_city];
                                    if($record->status == 1) {
                                        $d_total_opened[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                        $d_total_opened[$record->year][$record->month][$record->day]['All'] += $record->count;
                                    }
                                    if($record->status == 2) {
                                        $d_total_inaction[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                        $d_total_inaction[$record->year][$record->month][$record->day]['All'] += $record->count;
                                    }
                                    if($record->status == 3) {
                                        $d_total_closed[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                        $d_total_closed[$record->year][$record->month][$record->day]['All'] += $record->count;

                                        $close = new DateTime($record->updated_at);
                                        $open = new Datetime($record->created_at);
                                        $diff = $close->diff($open)->days;
                                        $d_days[$record->year][$record->month][$record->day][$record->d_city] += $diff;
                                        $d_days[$record->year][$record->month][$record->day]['All'] += $diff;
                                    }
                                    if($record->status == 4) {
                                        $d_total_reopened[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                        $d_total_reopened[$record->year][$record->month][$record->day]['All'] += $record->count;
                                    }
                                    $d_total[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $d_total[$record->year][$record->month][$record->day]['All'] += $record->count;
                                }
                                foreach($years as $ye) 
                                    foreach($months as $mo) 
                                        foreach($cities as $ci) 
                                            for($i = 1; $i <= 31; ++$i)
                                                $d_average[$ye][$mo][$i][$ci] = $d_total_closed[$ye][$mo][$i][$ci] == 0 ? 0 : number_format($d_days[$ye][$mo][$i][$ci] / $d_total_closed[$ye][$mo][$i][$ci], 2);
                                 ?>
                                <div id="collapseTwo" class="x_content collapse table-responsive" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <table class="table table-bordered table-striped" style="border: 1px solid black;">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Day</th>
                                                @foreach($cities as $city)
                                                    <th colspan="<?php echo (!isset($status) || $status == 'all') ? 6 : 3; ?>">{{ $city == 'All' ? 'Total' : $city }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <th>Tot.</th>
                                                    @if(!isset($status) || $status == 'all' || $status == '1')
                                                        <th>Opn.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '2')
                                                        <th>Act.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '3')
                                                        <th>Cls.</th>
                                                    @endif
                                                    @if(!isset($status) || $status == 'all' || $status == '4')
                                                        <th>Rep</th>
                                                    @endif
                                                    <th>DAvg.</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php 
                                                for($i = 31; $i >= 1; --$i) {
                                                    $flag = true;
                                                    foreach($cities as $city)
                                                        if($d_total[$year][$month][$i][$city] != 0) {
                                                            $flag = false;
                                                            break;
                                                        }
                                                    if($flag)
                                                        continue;
                                                 ?>
                                                 <tr>
                                                    <td>{{ $i }} / {{ $month_name[$month] }}</td>
                                                    @foreach($cities as $city)
                                                        <td>{{ $d_total[$year][$month][$i][$city] }}</td>
                                                        @if(!isset($status) || $status == 'all' || $status == '1')
                                                            <td>{{ $d_total_opened[$year][$month][$i][$city] }}</td>
                                                        @endif
                                                        @if(!isset($status) || $status == 'all' || $status == '2')
                                                            <td>{{ $d_total_inaction[$year][$month][$i][$city] }}</td>
                                                        @endif
                                                        @if(!isset($status) || $status == 'all' || $status == '3')
                                                            <td>{{ $d_total_closed[$year][$month][$i][$city] }}</td>
                                                        @endif
                                                        @if(!isset($status) || $status == 'all' || $status == '4')
                                                            <td>{{ $d_total_reopened[$year][$month][$i][$city] }}</td>
                                                        @endif
                                                        <td><b>{{ $d_average[$year][$month][$i][$city] }}</b></td>
                                                    @endforeach
                                                 </tr>
                                                <?php } ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
           
   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <script>
        function insert_record() {
            var username = document.getElementById('username').value;
            var total = document.getElementById('total').value;
            var not_answered = document.getElementById('not_answered').value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/ticket/calls/insert',
                dataType: 'text',
                data: { username: username, total: total, not_answered: not_answered }
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if(responseObj.success)
                    alert('Record inserted successfully');
                else
                    alert(responseObj.error);
            });
        }
    </script>

@stop
