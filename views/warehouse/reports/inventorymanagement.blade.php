@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table tbody td {
            text-align: center;
        }    
        table tbody th {
            text-align: center;
        }    
        table thead th {
            text-align: center;
        }    
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
    </style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/inventoryreportpost">
                        <input type="hidden" name="tabFilter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($adminCompanies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ isset($from) ? $from : '' }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ isset($to) ? $to : '' }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <?php 


        // $total 
        // $inwarehouse 
        // $inwarehouse_new 
        // $inwarehouse_r 
        // $inwarehouse_res 
        // $with_captains 
        // $delivered 
        // $returned_to_supplier 
        
        $months = $years = [];
        $year_prefix = ' / ';
        
        // total ot totals
        $_total = 0;
        $_inwarehouse = 0;
        $_inwarehouse_new = 0;
        $_inwarehouse_r = 0;
        $_inwarehouse_res = 0;
        $_with_captains = 0;
        $_delivered = 0;
        $_returned_to_supplier = 0;

        // total of each month
        $t_total = [];
        $t_inwarehouse = [];
        $t_inwarehouse_new = [];
        $t_inwarehouse_r = [];
        $t_inwarehouse_res = [];
        $t_with_captains = [];
        $t_delivered = [];
        $t_returned_to_supplier = [];

        foreach($total as $tot){ // only show the months in $total
            array_push($months, $tot->month . $year_prefix . $tot->year);
            array_push($years, $tot->year);
            $t_total[$tot->month . $year_prefix . $tot->year] = 0;
            $t_inwarehouse[$tot->month . $year_prefix . $tot->year] = 0;
            $t_inwarehouse_new[$tot->month . $year_prefix . $tot->year] = 0;
            $t_inwarehouse_r[$tot->month . $year_prefix . $tot->year] = 0;
            $t_inwarehouse_res[$tot->month . $year_prefix . $tot->year] = 0;
            $t_with_captains[$tot->month . $year_prefix . $tot->year] = 0;
            $t_delivered[$tot->month . $year_prefix . $tot->year] = 0;
            $t_returned_to_supplier[$tot->month . $year_prefix . $tot->year] = 0;
        }
        $flag = $tmp_months = $tmp_years = [];
        foreach($months as $month)
            if(!isset($flag[$month])) {
                $flag[$month] = 1;
                array_push($tmp_months, $month);
            }
        unset($flag);
        $flag = [];
        foreach($years as $ye)
            if(!isset($flag[$ye])) {
                $flag[$ye] = 1;
                array_push($tmp_years, $ye);
            }
        $months = $tmp_months;
        $years = $tmp_years;

        // total of each company in each month: [company][month]
        $c_total = [];
        $c_inwarehouse = [];
        $c_inwarehouse_new = [];
        $c_inwarehouse_r = [];
        $c_inwarehouse_res = [];
        $c_with_captains = [];
        $c_delivered = [];
        $c_returned_to_supplier = [];
        foreach($companies as $comp) {

            $c_total[$comp] = [];
            $c_inwarehouse[$comp] = [];
            $c_inwarehouse_new[$comp] = [];
            $c_inwarehouse_r[$comp] = [];
            $c_inwarehouse_res[$comp] = [];
            $c_with_captains[$comp] = [];
            $c_delivered[$comp] = [];
            $c_returned_to_supplier[$comp] = [];

            foreach($months as $month) {

                $c_total[$comp][$month] = 0;
                $c_inwarehouse[$comp][$month] = 0;
                $c_inwarehouse_new[$comp][$month] = 0;
                $c_inwarehouse_r[$comp][$month] = 0;
                $c_inwarehouse_res[$comp][$month] = 0;
                $c_with_captains[$comp][$month] = 0;
                $c_delivered[$comp][$month] = 0;
                $c_returned_to_supplier[$comp][$month] = 0;

            }

            $month = 'All';
            $c_total[$comp][$month] = 0;
            $c_inwarehouse[$comp][$month] = 0;
            $c_inwarehouse_new[$comp][$month] = 0;
            $c_inwarehouse_r[$comp][$month] = 0;
            $c_inwarehouse_res[$comp][$month] = 0;
            $c_with_captains[$comp][$month] = 0;
            $c_delivered[$comp][$month] = 0;
            $c_returned_to_supplier[$comp][$month] = 0;

        }

        foreach($total as $record){
            $_total += $record->count;
            $t_total [$record->month . $year_prefix . $record->year] += $record->count;
            $c_total [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_total [$record->company_id]['All'] += $record->count;
        }
        foreach($inwarehouse as $record){
            $_inwarehouse += $record->count;
            $t_inwarehouse [$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse [$record->company_id]['All'] += $record->count;
        }
        foreach($inwarehouse_new as $record){
            $_inwarehouse_new += $record->count;
            $t_inwarehouse_new [$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_new [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_new [$record->company_id]['All'] += $record->count;
        }
        foreach($inwarehouse_r as $record){
            $_inwarehouse_r += $record->count;
            $t_inwarehouse_r [$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_r [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_r [$record->company_id]['All'] += $record->count;
        }
        foreach($inwarehouse_res as $record){
            $_inwarehouse_res += $record->count;
            $t_inwarehouse_res [$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_res [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_inwarehouse_res [$record->company_id]['All'] += $record->count;
        }
        foreach($with_captains as $record){
            $_with_captains += $record->count;
            $t_with_captains [$record->month . $year_prefix . $record->year] += $record->count;
            $c_with_captains [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_with_captains [$record->company_id]['All'] += $record->count;
        }
        foreach($delivered as $record){
            $_delivered += $record->count;
            $t_delivered [$record->month . $year_prefix . $record->year] += $record->count;
            $c_delivered [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_delivered [$record->company_id]['All'] += $record->count;
        }
        foreach($returned_to_supplier as $record){
            $_returned_to_supplier += $record->count;
            $t_returned_to_supplier [$record->month . $year_prefix . $record->year] += $record->count;
            $c_returned_to_supplier [$record->company_id][$record->month . $year_prefix . $record->year] += $record->count;
            $c_returned_to_supplier [$record->company_id]['All'] += $record->count;
        }
        
         ?>

       <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <?php $div = 80.0 / (1 + count($months)); ?>
                                    <th style="width: 20%;"></th>
                                    <th style="width: {{ $div }}%;">Total</th>
                                    @foreach($months as $month)
                                        <th style="width: {{ $div }}%;">{{ $month }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total</td>
                                    <td>{{ number_format($_total) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_total[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>In Warehouse</td>
                                    <td>{{ number_format($_inwarehouse) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_inwarehouse[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>In Warehouse / New</td>
                                    <td>{{ number_format($_inwarehouse_new) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_inwarehouse_new[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>In Warehouse / Returned</td>
                                    <td>{{ number_format($_inwarehouse_r) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_inwarehouse_r[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>In Warehouse / Rescheduled</td>
                                    <td>{{ number_format($_inwarehouse_res) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_inwarehouse_res[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>With Captains</td>
                                    <td>{{ number_format($_with_captains) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_with_captains[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Delivered</td>
                                    <td>{{ number_format($_delivered) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_delivered[$month]) }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>Returned To Supplier</td>
                                    <td>{{ number_format($_returned_to_supplier) }}</td>
                                    @foreach($months as $month)
                                        <td>{{ number_format($t_returned_to_supplier[$month]) }}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            @if(count($companies) > 1)
                @foreach($companies as $com)
                    <div class="tab-content">
                        <div id="dataview" class="tab-pane fade in active">
                            <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                                <h3 class="text text-center">{{ $main_company[$com] }}</h3>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <?php $div = 80.0 / (1 + count($months)); ?>
                                            <th style="width: 20%;"></th>
                                            <th style="width: {{ $div }}%;">Total</th>
                                            @foreach($months as $month)
                                                <th style="width: {{ $div }}%;">{{ $month }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total</td>
                                            <td>{{ number_format($c_total[$com]['All']) }}</td>
                                            @foreach($months as $month)
                                                <td>{{ number_format($c_total[$com][$month]) }}</td>
                                            @endforeach
                                        </tr>
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>In Warehouse</td>
                                                <td>{{ number_format($c_inwarehouse[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_inwarehouse[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>In Warehouse / New</td>
                                                <td>{{ number_format($c_inwarehouse_new[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_inwarehouse_new[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>In Warehouse / Returned</td>
                                                <td>{{ number_format($c_inwarehouse_r[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_inwarehouse_r[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>In Warehouse / Rescheduled</td>
                                                <td>{{ number_format($c_inwarehouse_res[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_inwarehouse_res[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>With Captains</td>
                                                <td>{{ number_format($c_with_captains[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_with_captains[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>Delivered</td>
                                                <td>{{ number_format($c_delivered[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_delivered[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                        @if($c_total[$com]['All'])
                                            <tr>
                                                <td>Returned To Supplier</td>
                                                <td>{{ number_format($c_returned_to_supplier[$com]['All']) }}</td>
                                                @foreach($months as $month)
                                                    <td>{{ number_format($c_returned_to_supplier[$com][$month]) }}</td>
                                                @endforeach
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>


   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>



@stop
