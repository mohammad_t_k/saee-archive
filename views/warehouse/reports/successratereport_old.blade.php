@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/successratereportpost">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control"
                                            type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                            class="required">*</span>
                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <p>
                                        <button id="submitForm" class="btn btn-success">Filter</button>
                                    </p>
                                </div>

                                <div class="container" style="text-align: right;">
                                    <!-- Trigger the modal with a button -->
                                    <a href="#myModal" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-info-sign" style="margin-top: 10px; margin-right: 10px; font-size: 30px;"></span></a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content" style="text-align: left;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Help</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <b>Note</b>: All numbers are calculated starting from the first recorded In Route Date which is 2018-03-27.<br/><br/>
                                                    <b>Table Legends:</b>
                                                    <br/><br/><b>- Total (Tot.):</b> is the total number of shipments <b><u>excluding</u></b> created by supplier shipments and <b><u>including</u></b> the in route shipments sent by specific company or all companies in specific in route date.
                                                    <br/><br/><b>- Delivered (Del.):</b> is the total number of delivered shipments sent by specific company or all companies in specific in route date.
                                                    <br/><br/><b>- Dispatched (Dis.):</b> is the total number of shipments <b><u>excluding</u></b> the in route and created by supplier shipments sent by specific company or all companies in specific in route date.
                                                    <br/><br/><b>- Average Days to Dispatch (Avg.):</b> is the average number of days between in route date and scheduled date to dispatch shipments sent by specific company or all companies in specific in route date.
                                                    <br/><br/><b>- Average Days to Pick-up (PAvg.):</b> is the average number of days between in route date and pick-up date done by captain to dispatch shipments sent by specific company or all companies in specific in route date.
                                                    <br/><br/><b>- Success Rate (SR%):</b> is the delivered shipments divided by dispatched shipments sent by specific company or all companies in specific in route date multiplied by 100. The percentage is colored by <span style="color: green;">green</span> if success rate is greater than 75% and by  <span style="color: red;">red</span> if success rate is less than 75%.

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <?php

    // items of groupef and nongrouped are array of arrays, so it must loop 2 nested loops to check them
    $month_flag = [false, false,false,false,false,false,false,false,false,false,false,false,false];
    $month_name = ['Dec','Nov','Oct','Sep','Aug','Jul','Jun','May','Apr','Mar','Feb','Jan', ''];
    $month_name = array_reverse($month_name);
    $printed_months = [];
    $printed_years = [];

    for($i = 0; $i < count($nongrouped); ++$i)
        for($j = 0; $j < count($nongrouped[$i]); ++$j)
            if(!$month_flag[$nongrouped[$i][$j]->month]){
                $month_flag[$nongrouped[$i][$j]->month] = true;
                array_push($printed_months, $nongrouped[$i][$j]->month);
                array_push($printed_years, $nongrouped[$i][$j]->year);
            }
    for($i = 0; $i < count($grouped); ++$i)
        for($j = 0; $j < count($grouped[$i]); ++$j)
            if(!$month_flag[$grouped[$i][$j]->month]){
                $month_flag[$grouped[$i][$j]->month] = true;
                array_push($printed_months, $grouped[$i][$j]->month);
                array_push($printed_years, $grouped[$i][$j]->year);
            }

    for($i = 0; $i < count($printed_months); ++$i)
        $printed_months[$i] = $month_name[$printed_months[$i]] . ' ' . $printed_years[$i];

    // total of all totals
    $t_total = $t_delivered = $t_dispatched = $t_successrate = $t_avg = $t_count = $t_avgcaptain = $t_countcaptain = 0;

    // Total tables in months tabs
    $total_tables = array();

    // months
    for($m = 0; $m < count($printed_months); ++$m){
        $curmonth = explode(' ', $printed_months[$m])[0];

        // city
        for($c = 0; $c < count($cities); ++$c){
            $m_total = $m_delivered = $m_dispatched = $m_successrate = $m_avg = $m_count = $m_avgcaptain = $m_countcaptain = 0;
            $curcity = $cities[$c];

            // loop over all items
            // grouped cities
            if($checkgroup[$curcity] == 1){
                for($i = 0; $i < count($grouped); ++$i)
                    for($j = 0; $j < count($grouped[$i]); ++$j){
                        if($grouped[$i][$j]->city == $curcity && $month_name[$grouped[$i][$j]->month] == $curmonth){
                            $m_total +=      $grouped[$i][$j]->total;
                            $m_delivered +=  $grouped[$i][$j]->delivered;
                            $m_dispatched += $grouped[$i][$j]->dispatched;
                            $m_avg += $grouped[$i][$j]->average;
                            ++$m_count;
                            $m_avgcaptain += $grouped[$i][$j]->averagecaptain;
                            ++$m_countcaptain;
                        }
                    }
            }
            // non grouped cities
            else if($curcity != 'All') {
                for($i = 0; $i < count($nongrouped); ++$i)
                    for($j = 0; $j < count($nongrouped[$i]); ++$j){
                        if($nongrouped[$i][$j]->city == $curcity && $month_name[$nongrouped[$i][$j]->month] == $curmonth){
                            $m_total +=      $nongrouped[$i][$j]->total;
                            $m_delivered +=  $nongrouped[$i][$j]->delivered;
                            $m_dispatched += $nongrouped[$i][$j]->dispatched;
                            $m_avg += $nongrouped[$i][$j]->average;
                            ++$m_count;
                            $m_avgcaptain += $nongrouped[$i][$j]->averagecaptain;
                            ++$m_countcaptain;
                        }
                    }
            }
            // 'All' case
            else {
                for($i = 0; $i < count($grouped); ++$i)
                    for($j = 0; $j < count($grouped[$i]); ++$j){
                        if($month_name[$grouped[$i][$j]->month] == $curmonth){
                            $m_total +=      $grouped[$i][$j]->total;
                            $m_delivered +=  $grouped[$i][$j]->delivered;
                            $m_dispatched += $grouped[$i][$j]->dispatched;
                            $m_avg += $grouped[$i][$j]->average;
                            ++$m_count;
                            $m_avgcaptain += $grouped[$i][$j]->averagecaptain;
                            ++$m_countcaptain;
                        }
                    }
                for($i = 0; $i < count($nongrouped); ++$i)
                    for($j = 0; $j < count($nongrouped[$i]); ++$j){
                        if($month_name[$nongrouped[$i][$j]->month] == $curmonth){
                            $m_total +=      $nongrouped[$i][$j]->total;
                            $m_delivered +=  $nongrouped[$i][$j]->delivered;
                            $m_dispatched += $nongrouped[$i][$j]->dispatched;
                            $m_avg += $nongrouped[$i][$j]->average;
                            ++$m_count;
                            $m_avgcaptain += $nongrouped[$i][$j]->averagecaptain;
                            ++$m_countcaptain;
                        }
                    }
            }
            if($m_dispatched > 0)
                $m_successrate = 1.0 * $m_delivered / $m_dispatched * 100.0;
            if($m_count > 0)
                $m_avg /= 1.0 * $m_count;
            if($m_countcaptain > 0)
                $m_avgcaptain /= 1.0 * $m_countcaptain;
            $total_tables[$curmonth][$curcity] = [
                "total" =>      number_format($m_total),
                "dispatched" => number_format($m_dispatched),
                "delivered" =>  number_format($m_delivered),
                "successrate" => number_format($m_successrate, 2),
                "average" => number_format($m_avg, 2),
                "averagecaptain" => number_format($m_avgcaptain, 2)
            ];

            if($curcity == $city){
                $t_total += $m_total;
                $t_delivered += $m_delivered;
                $t_dispatched += $m_dispatched;
                $t_avg += $m_avg;
                ++$t_count;
                $t_avgcaptain += $m_avgcaptain;
                ++$t_countcaptain;
            }

        }
    }

    if($t_dispatched > 0)
        $t_successrate = (1.0 * $t_delivered / $t_dispatched * 100.0);
    if($t_count > 0)
        $t_avg /= 1.0 * $t_count;
    if($t_countcaptain > 0)
        $t_avgcaptain /= 1.0 * $t_countcaptain;

    $t_total = number_format($t_total);
    $t_delivered = number_format($t_delivered);
    $t_dispatched = number_format($t_dispatched);
    $t_successrate = number_format($t_successrate, 2);
    $t_avg = number_format($t_avg, 2);
    $t_avgcaptain = number_format($t_avgcaptain, 2);


    ?>


    <!-- make tables responsive -->
        <style>
            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
            }
            th, td {
                text-align: left;
                padding: 8px;
            }
            tr:nth-child(even){background-color: #f2f2f2}
        </style>

        <div class="container">
            <div class="tab-content">
                <!-- Data View -->
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <!-- Total of Totals every day -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 15%;">Total</th>
                                <th style="width: 15%;">Delivered</th>
                                <th style="width: 15%;">Dispatched</th>
                                <th style="width: 20%;">Average Days to Dispatch</th>
                                <th style="width: 20%;">Average Days to Pick-up</th>
                                <th style="width: 15%;">Success Rate</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">{{ $t_total }}</th>
                                <td>{{ $t_delivered }}</td>
                                <td>{{ $t_dispatched }}</td>
                                <td>{{ $t_avg }}</td>
                                <td>{{ $t_avgcaptain }}</td>
                                <td style="color: <?php echo ($t_successrate > 75 ? 'green' : 'red'); ?>;">{{ $t_successrate }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @foreach($printed_months as $month_year)
                        <div class="x_panel tile fixed_height_580 overflow_hidden row" style="overflow-x:auto; overflow-y:auto;">
                            <?php $current_month = explode(' ' , $month_year)[0]; ?>
                            <h1>{{ $month_year }}</h1>
                            <div class="x_title table-responsive" style="overflow-x:auto; overflow-y:hidden;">
                                <br/><br/>
                                <table class="table table-bordered">
                                    <tr>
                                        @foreach($cities as $city)
                                            <td>
                                                <!-- Total of Every city Table -->
                                                <table class="table table-striped">
                                                    <caption style="text-align: center;">{{ ($city == 'All' ? 'Total' : $city) }}</caption>
                                                    <thead>
                                                    <tr>
                                                        <th>Tot.</th>
                                                        <th>Del.</th>
                                                        <th>Dis.</th>
                                                        <th>Avg.</th>
                                                        <th>PAvg.</th>
                                                        <th>SR(%)</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{ $total_tables[$current_month][$city]['total'] }}</td>
                                                        <td>{{ $total_tables[$current_month][$city]['delivered'] }}</td>
                                                        <td>{{ $total_tables[$current_month][$city]['dispatched'] }}</td>
                                                        <td>{{ $total_tables[$current_month][$city]['average'] }}</td>
                                                        <td>{{ $total_tables[$current_month][$city]['averagecaptain'] }}</td>
                                                        <?php $sr = $total_tables[$current_month][$city]['successrate']; ?>
                                                        <td style="color: <?php echo ($sr > 75 ? 'green' : 'red'); ?>;">{{ $sr }}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        @endforeach
                                    </tr>
                                </table>
                                <div class="col horizontal-divider" style="margin-top: 30px"></div>
                                <ul class="nav navbar-left panel_toolbox">
                                    <li><a class="collapse-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseData"><i class="fa fa-chevron-down"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapseTwo" class="x_content collapse table-responsive" aria-labelledby="headingTwo" data-parent="#accordion">
                                <!-- Detailed Data Table -->
                                <table class="table table-bordered table-striped" style="border: 1px solid black;">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">DOM</th>
                                        <th rowspan="2">DOW</th>
                                        @foreach($cities as $city)
                                            <th colspan="6">{{ ($city == 'All' ? 'Total' : $city) }}</th>
                                        @endforeach
                                        <th>Day</th>
                                    </tr>
                                    <tr>
                                        @foreach($cities as $city)
                                            <th style="background-color: white;">Tot.</th>
                                            <th style="background-color: white;">Del.</th>
                                            <th style="background-color: white;">Dis.</th>
                                            <th style="background-color: white;">Avg.</th>
                                            <th style="background-color: white;">PAvg.</th>
                                            <th style="background-color: white;">SR(%)</th>
                                        @endforeach
                                        <th style="background-color: white;"></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php
                                    for($d = 31; $d >= 1; --$d){

                                        // check if we have to print this line or not
                                        $ok = false;
                                        for($c = 0; $c < count($cities) && !$ok; ++$c){
                                            $curcity = $cities[$c];
                                            if($checkgroup[$curcity]){
                                                for($i = 0; $i < count($grouped) && !$ok; ++$i)
                                                    for($j = 0; $j < count($grouped[$i]) && !$ok; ++$j){
                                                        if($grouped[$i][$j]->city == $curcity && $month_name[$grouped[$i][$j]->month] == $current_month && $grouped[$i][$j]->day == $d)
                                                            $ok = true;
                                                    }
                                            }
                                            else if($curcity != 'All') {
                                                for($i = 0; $i < count($nongrouped) && !$ok; ++$i)
                                                    for($j = 0; $j < count($nongrouped[$i]) && !$ok; ++$j){
                                                        if($nongrouped[$i][$j]->city == $curcity && $month_name[$nongrouped[$i][$j]->month] == $current_month && $nongrouped[$i][$j]->day == $d)
                                                            $ok = true;
                                                    }
                                            }
                                            else {
                                                for($i = 0; $i < count($grouped) && !$ok; ++$i)
                                                    for($j = 0; $j < count($grouped[$i]) && !$ok; ++$j){
                                                        if($month_name[$grouped[$i][$j]->month] == $current_month && $grouped[$i][$j]->day == $d)
                                                            $ok = true;
                                                    }
                                                for($i = 0; $i < count($nongrouped) && !$ok; ++$i)
                                                    for($j = 0; $j < count($nongrouped[$i]) && !$ok; ++$j){
                                                        if($month_name[$nongrouped[$i][$j]->month] == $current_month && $nongrouped[$i][$j]->day == $d)
                                                            $ok = true;
                                                    }
                                            }
                                        }

                                        if(!$ok)
                                            continue;

                                        // those variables for 'All' city, will be calculated first, and will be zeroed by the end of the current day
                                        $tot = $del = $dis = $sr = $avg = $cnt = $avgcap = $cntcap = 0;
                                        for($c = 0; $c < count($cities); ++$c){
                                            $curcity = $cities[$c];
                                            $flag = false;
                                            // grouped cities
                                            if($checkgroup[$curcity] == 1){
                                                for($i = 0; $i < count($grouped) && !$flag; ++$i)
                                                    for($j = 0; $j < count($grouped[$i]) && !$flag; ++$j){
                                                        if($grouped[$i][$j]->city == $curcity && $month_name[$grouped[$i][$j]->month] == $current_month && $grouped[$i][$j]->day == $d){
                                                            $tot += $grouped[$i][$j]->total;
                                                            $del += $grouped[$i][$j]->delivered;
                                                            $dis += $grouped[$i][$j]->dispatched;
                                                            $avg += $grouped[$i][$j]->average;
                                                            ++$cnt;
                                                            $avgcap += $grouped[$i][$j]->averagecaptain;
                                                            ++$cntcap;
                                                            $flag = true;
                                                            break;
                                                        }
                                                    }
                                            }
                                            // non grouped cities
                                            else if($curcity != 'All') {
                                                for($i = 0; $i < count($nongrouped) && !$flag; ++$i)
                                                    for($j = 0; $j < count($nongrouped[$i]) && !$flag; ++$j){
                                                        if($nongrouped[$i][$j]->city == $curcity && $month_name[$nongrouped[$i][$j]->month] == $current_month && $nongrouped[$i][$j]->day == $d){
                                                            $tot += $nongrouped[$i][$j]->total;
                                                            $del += $nongrouped[$i][$j]->delivered;
                                                            $dis += $nongrouped[$i][$j]->dispatched;
                                                            $avg += $nongrouped[$i][$j]->average;
                                                            ++$cnt;
                                                            $avgcap += $nongrouped[$i][$j]->averagecaptain;
                                                            ++$cntcap;
                                                            $flag = true;
                                                            break;
                                                        }
                                                    }
                                            }
                                        }

                                        // print one record
                                        echo '<tr>';
                                        echo '<td>'. $d . ' / ' . $current_month .  '</td>';
                                        $month_map = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07',
                                            'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                                        $divide = explode(' ', $month_year);
                                        echo '<td>' . date('D', strtotime($divide[1] . '-' . $month_map[$divide[0]] . '-' . $d)) .  '</td>';
                                        for($c = 0; $c < count($cities); ++$c){
                                            $curcity = $cities[$c];
                                            $flag = false;
                                            // grouped cities
                                            if($checkgroup[$curcity] == 1){
                                                for($i = 0; $i < count($grouped) && !$flag; ++$i)
                                                    for($j = 0; $j < count($grouped[$i]) && !$flag; ++$j){
                                                        if($grouped[$i][$j]->city == $curcity && $month_name[$grouped[$i][$j]->month] == $current_month && $grouped[$i][$j]->day == $d){
                                                            echo '<td>'. number_format($grouped[$i][$j]->total) . '</td>';
                                                            echo '<td>'. number_format($grouped[$i][$j]->delivered) . '</td>';
                                                            echo '<td>'. number_format($grouped[$i][$j]->dispatched) . '</td>';
                                                            echo '<td>'. number_format($grouped[$i][$j]->average, 2) . '</td>';
                                                            echo '<td>'. number_format($grouped[$i][$j]->averagecaptain, 2) . '</td>';
                                                            $sr = ($grouped[$i][$j]->dispatched > 0 ? number_format(1.0 * $grouped[$i][$j]->delivered / $grouped[$i][$j]->dispatched * 100.0, 2) : 0);
                                                            if($sr > 75) echo '<td style="color: green;">' . $sr . '</td>';
                                                            else echo '<td style="color: red;">' . $sr . '</td>';
                                                            $flag = true;
                                                            break;
                                                        }
                                                    }
                                            }
                                            // non grouped cities
                                            else if($curcity != 'All') {
                                                for($i = 0; $i < count($nongrouped) && !$flag; ++$i)
                                                    for($j = 0; $j < count($nongrouped[$i]) && !$flag; ++$j){
                                                        if($nongrouped[$i][$j]->city == $curcity && $month_name[$nongrouped[$i][$j]->month] == $current_month && $nongrouped[$i][$j]->day == $d){
                                                            echo '<td>'. number_format($nongrouped[$i][$j]->total) . '</td>';
                                                            echo '<td>'. number_format($nongrouped[$i][$j]->delivered) . '</td>';
                                                            echo '<td>'. number_format($nongrouped[$i][$j]->dispatched) . '</td>';
                                                            echo '<td>'. number_format($nongrouped[$i][$j]->average, 2) . '</td>';
                                                            echo '<td>'. number_format($nongrouped[$i][$j]->averagecaptain, 2) . '</td>';
                                                            $sr = ($nongrouped[$i][$j]->dispatched > 0 ? number_format(1.0 * $nongrouped[$i][$j]->delivered / $nongrouped[$i][$j]->dispatched * 100.0, 2) : 0);
                                                            if($sr > 75) echo '<td style="color: green;">' . $sr . '</td>';
                                                            else echo '<td style="color: red;">' . $sr . '</td>';
                                                            $flag = true;
                                                            break;
                                                        }
                                                    }
                                            }
                                            // 'All' case
                                            else {
                                                if($dis > 0)
                                                    $sr = 1.0 * $del / $dis * 100;
                                                if($cnt > 0)
                                                    $avg /= 1.0 * $cnt;
                                                if($cntcap > 0)
                                                    $avgcap /= 1.0 * $cntcap;
                                                echo '<td>'. number_format($tot) . '</td>';
                                                echo '<td>'. number_format($del) . '</td>';
                                                echo '<td>'. number_format($dis) . '</td>';
                                                echo '<td>'. number_format($avg, 2) . '</td>';
                                                echo '<td>'. number_format($avgcap, 2) . '</td>';
                                                $sr = ($dis > 0 ? number_format(1.0 * $del / $dis * 100.0, 2) : 0);
                                                if($sr > 75) echo '<td style="color: green;">'. $sr . '</td>';
                                                else echo '<td style="color: red;">'. $sr . '</td>';
                                                $flag = true;
                                            }
                                            if(!$flag){
                                                echo '<td>'. '-' . '</td>';
                                                echo '<td>'. '-' . '</td>';
                                                echo '<td>'. '-' . '</td>';
                                                echo '<td>'. '-' . '</td>';
                                                echo '<td>'. '-' . '</td>';
                                                echo '<td>'. '-' . '</td>';
                                            }
                                        }
                                        echo '<td>'. $d . ' / ' . $current_month .  '</td>';
                                        echo '</tr>';
                                    }

                                    ?>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <!-- End of Success Rate Report-->
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>



@stop
