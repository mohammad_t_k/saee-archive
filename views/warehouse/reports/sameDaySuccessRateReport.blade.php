@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table th {
            text-align: center;
            color: #5d6c7c;
        } 
        table td {
            text-align: center;
            color: #5d6c7c;
            font-weight: bold;
        } 
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
        h1, h2, h3, h4, h5 {
            text-align: center;
            color: #5d6c7c;
        }
    </style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/sameDaySuccessRate/filter">
                        <input type="hidden" name="tabFilter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From (Sign-In Date) </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ $from }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To (Sign-In Date) </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ $to }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    @foreach($finalResult as $curResult)
                        <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                            <div class="x_title table-responsive" style="overflow-x:auto; overflow-y:hidden;">
                                <div class="col horizontal-divider"></div>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseData"><i class="fa fa-chevron-down"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                                <h3>{{ $curResult['date'] }}</h3>
                                <hr style="width: 20%; align: center;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Same Day Dispatched Rate</th>
                                            <td>{{ number_format($curResult['sameDayDispatched'], 2) }}%</td>
                                            <th>Same Day Success Rate</th>
                                            <td>{{ number_format($curResult['sameDaySuccessRate'], 2) }}%</td>
                                            <th>Current Success Rate</th>
                                            <td>{{ number_format($curResult['CurrentSuccessRate'], 2) }}%</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>                        
                            <div id="collapseTwo" class="x_content collapse table-responsive" aria-labelledby="headingTwo" data-parent="#accordion">
                                <table id="datatable_items{{ $curResult['date'] }}" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Waybill</th>
                                            <th>Order Number</th>
                                            <th>Status</th>
                                            <th>Last Status Update</th>
                                            <th>Last Comment</th>
                                        </tr>
                                    </thead>
                                    
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Waybill</th>
                                            <th>Order Number</th>
                                            <th>Status</th>
                                            <th>Last Status Update</th>
                                            <th>Last Comment</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>

    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <script>

        <?php
        $data = array();
        foreach($finalResult as $curResult) {
            $data[$curResult['date']] = '';
            $counter = 1;
            foreach($curResult['result'] as $record) {
                $data[$curResult['date']] .= "[ \"$counter\", \"$record->jollychic\", \"$record->order_number\", \"$record->english\", \"$record->LastStatusDate\", \"$record->comment\"],";
                ++$counter;
            }
        }
        ?>

        @foreach($finalResult as $curResult) 
            $("#datatable_items{{ $curResult['date'] }}").DataTable({

                "data": [
                    <?php echo $data[$curResult['date']] ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });
        @endforeach
        

    </script>

@stop
