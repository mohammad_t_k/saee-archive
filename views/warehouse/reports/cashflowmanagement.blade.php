@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table tbody td {
            text-align: center;
        }    
        table tbody th {
            text-align: center;
        }    
        table thead th {
            text-align: center;
        }    
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
    </style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="post" action="/warehouse/cashflowpost">
                        <input type="hidden" name="tabFilter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ $from }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ $to }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <!-- ---------------------------------------------------------------------------------------------------------------------- Total of Totals --------------------------- -->
                        <?php
                        $total_received_count = $total_received_amount = 0;
                        foreach($total_received as $record) {
                            $total_received_count += $record->count;
                            $total_received_amount += $record->amount;
                        }
                        $paid_to_supplier_count = $paid_to_supplier_amount = 0;
                        foreach($paid_to_supplier as $record) {
                            $paid_to_supplier_count += $record->count;
                            $paid_to_supplier_amount += $record->amount;
                        }
                        $not_paid_to_supplier_count = $not_paid_to_supplier_amount = 0;
                        foreach($not_paid_to_supplier as $record) {
                            $not_paid_to_supplier_count += $record->count;
                            $not_paid_to_supplier_amount += $record->amount;
                        }
                        $delivered_not_localy_transfared_count = $delivered_not_localy_transfared_amount = 0;
                        foreach($delivered_not_localy_transfared as $record) {
                            $delivered_not_localy_transfared_count += $record->count;
                            $delivered_not_localy_transfared_amount += $record->amount;
                        }
                        $delivered_with_captain_count = $delivered_with_captain_amount = 0;
                        foreach($delivered_with_captain as $record) {
                            $delivered_with_captain_count += $record->count;
                            $delivered_with_captain_amount += $record->amount;
                        }
                        $total_delivered_count = $total_delivered_amount = 0;
                        foreach($total_delivered as $record) {
                            $total_delivered_count += $record->count;
                            $total_delivered_amount += $record->amount;
                        }
                        $in_warehouse_count = $in_warehouse_amount = 0;
                        foreach($in_warehouse as $record) {
                            $in_warehouse_count += $record->count;
                            $in_warehouse_amount += $record->amount;
                        }
                        $with_captain_count = $with_captain_amount = 0;
                        foreach($with_captain as $record) {
                            $with_captain_count += $record->count;
                            $with_captain_amount += $record->amount;
                        }
                        $returned_to_supplier_count = $returned_to_supplier_amount = 0;
                        foreach($returned_to_supplier as $record) {
                            $returned_to_supplier_count += $record->count;
                            $returned_to_supplier_amount += $record->amount;
                        }
                        ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th style="width: 15%;"></th>
                                <th style="width: 15%;">Count</th>
                                <th style="width: 15%;">Amount</th>
                                <th style="width: 20%;">% From Total</th>
                                <th style="width: 20%;">% From Delivered</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Total Received</th>
                                    <td>{{ number_format($total_received_count) }}</td>
                                    <td>{{ number_format($total_received_amount, 2) }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Paid To Supplier</th>
                                    <td>{{ number_format($paid_to_supplier_count) }}</td>
                                    <td>{{ number_format($paid_to_supplier_amount, 2) }}</td>
                                    <td class="important">{{ $total_received_amount == 0 ? '0.00' : number_format($paid_to_supplier_amount / $total_received_amount * 100.0, 2) }}%</td>
                                    <td class="important">{{ $total_delivered_amount == 0 ? '0.00' : number_format($paid_to_supplier_amount / $total_delivered_amount * 100.0, 2) }}%</td>
                                </tr>
                                <tr>
                                    <th>Not Paid To Supplier</th>
                                    <td>{{ number_format($not_paid_to_supplier_count) }}</td>
                                    <td>{{ number_format($not_paid_to_supplier_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' :  number_format($not_paid_to_supplier_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td>{{ $total_delivered_amount == 0 ? '0.00' : number_format($not_paid_to_supplier_amount / $total_delivered_amount * 100.0, 2) }}%</td>
                                </tr>
                                <tr>
                                    <th>Delivered Not Locally Transferred</th>
                                    <td>{{ number_format($delivered_not_localy_transfared_count) }}</td>
                                    <td>{{ number_format($delivered_not_localy_transfared_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' :  number_format($delivered_not_localy_transfared_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td>{{ $total_delivered_amount == 0 ? '0.00' : number_format($delivered_not_localy_transfared_amount / $total_delivered_amount * 100.0, 2) }}%</td>
                                </tr>
                                <tr>
                                    <th>Delivered With Captains</th>
                                    <td>{{ number_format($delivered_with_captain_count) }}</td>
                                    <td>{{ number_format($delivered_with_captain_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' :  number_format($delivered_with_captain_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td>{{ $total_delivered_amount == 0 ? '0.00' : number_format($delivered_with_captain_amount / $total_delivered_amount * 100.0, 2) }}%</td>
                                </tr>
                                <tr>
                                    <th>Total Delivered</th>
                                    <td>{{ number_format($total_delivered_count) }}</td>
                                    <td>{{ number_format($total_delivered_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' : number_format($total_delivered_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>In Warehouse</th>
                                    <td>{{ number_format($in_warehouse_count) }}</td>
                                    <td>{{ number_format($in_warehouse_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' : number_format($in_warehouse_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>With Captains</th>
                                    <td>{{ number_format($with_captain_count) }}</td>
                                    <td>{{ number_format($with_captain_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' : number_format($with_captain_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Returned To Supplier</th>
                                    <td>{{ number_format($returned_to_supplier_count) }}</td>
                                    <td>{{ number_format($returned_to_supplier_amount, 2) }}</td>
                                    <td>{{ $total_received_amount == 0 ? '0.00' : number_format($returned_to_supplier_amount / $total_received_amount  * 100.0, 2) }}%</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- ---------------------------------------------------------------------------------------------------------------------- Calculate Months Total Tables ------------------------- -->
                    <?php
                    $month_name = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    $months = $years = $found = [];
                    for($i = 0; $i <= 12; ++$i)
                        $found[$i] = false;
                    foreach($total_received as $record) {
                        if(!$found[$record->month]) {
                            array_push($months, $record->month);
                            $found[$record->month] = true;
                        }
                        if(!isset($found[$record->year])) {
                            array_push($years, $record->year);
                            $found[$record->year] = true;
                        }
                    }
                    rsort($months);
                    rsort($years);

                    // initialize arrays $row_of_table_count[year][month][city] and $row_of_table_amount[year][month][city]
                    $table_total_received_count =                  $table_total_received_amount = [];
                    $table_paid_to_supplier_count =                $table_paid_to_supplier_amount = [];
                    $table_not_paid_to_supplier_count =            $table_not_paid_to_supplier_amount = [];
                    $table_delivered_not_localy_transfared_count = $table_delivered_not_localy_transfared_amount = [];
                    $table_delivered_with_captain_count =          $table_delivered_with_captain_amount = [];
                    $table_total_delivered_count =                 $table_total_delivered_amount = [];
                    $table_in_warehouse_count =                    $table_in_warehouse_amount = [];
                    $table_with_captain_count =                    $table_with_captain_amount = [];
                    $table_returned_to_supplier_count =            $table_returned_to_supplier_amount = [];
                    foreach($years as $year) {

                        $table_total_received_count[$year] =                  $table_total_received_amount[$year] = [];
                        $table_paid_to_supplier_count[$year] =                $table_paid_to_supplier_amount[$year] = [];
                        $table_not_paid_to_supplier_count[$year] =            $table_not_paid_to_supplier_amount[$year] = [];
                        $table_delivered_not_localy_transfared_count[$year] = $table_delivered_not_localy_transfared_amount[$year] = [];
                        $table_delivered_with_captain_count[$year] =          $table_delivered_with_captain_amount[$year] = [];
                        $table_total_delivered_count[$year] =                 $table_total_delivered_amount[$year] = [];
                        $table_in_warehouse_count[$year] =                    $table_in_warehouse_amount[$year] = [];
                        $table_with_captain_count[$year] =                    $table_with_captain_amount[$year] = [];
                        $table_returned_to_supplier_count[$year] =            $table_returned_to_supplier_amount[$year] = [];

                        foreach($months as $month) {

                            $table_total_received_count[$year][$month] =                  $table_total_received_amount[$year][$month] = [];
                            $table_paid_to_supplier_count[$year][$month] =                $table_paid_to_supplier_amount[$year][$month] = [];
                            $table_not_paid_to_supplier_count[$year][$month] =            $table_not_paid_to_supplier_amount[$year][$month] = [];
                            $table_delivered_not_localy_transfared_count[$year][$month] = $table_delivered_not_localy_transfared_amount[$year][$month] = [];
                            $table_delivered_with_captain_count[$year][$month] =          $table_delivered_with_captain_amount[$year][$month] = [];
                            $table_total_delivered_count[$year][$month] =                 $table_total_delivered_amount[$year][$month] = [];
                            $table_in_warehouse_count[$year][$month] =                    $table_in_warehouse_amount[$year][$month] = [];
                            $table_with_captain_count[$year][$month] =                    $table_with_captain_amount[$year][$month] = [];
                            $table_returned_to_supplier_count[$year][$month] =            $table_returned_to_supplier_amount[$year][$month] = [];

                            foreach($cities as $city) {
                                $table_total_received_count[$year][$month][$city] =                  $table_total_received_amount[$year][$month][$city] = 0;
                                $table_paid_to_supplier_count[$year][$month][$city] =                $table_paid_to_supplier_amount[$year][$month][$city] = 0;
                                $table_not_paid_to_supplier_count[$year][$month][$city] =            $table_not_paid_to_supplier_amount[$year][$month][$city] = 0;
                                $table_delivered_not_localy_transfared_count[$year][$month][$city] = $table_delivered_not_localy_transfared_amount[$year][$month][$city] = 0;
                                $table_delivered_with_captain_count[$year][$month][$city] =          $table_delivered_with_captain_amount[$year][$month][$city] = 0;
                                $table_total_delivered_count[$year][$month][$city] =                 $table_total_delivered_amount[$year][$month][$city] = 0;
                                $table_in_warehouse_count[$year][$month][$city] =                    $table_in_warehouse_amount[$year][$month][$city] = 0;
                                $table_with_captain_count[$year][$month][$city] =                    $table_with_captain_amount[$year][$month][$city] = 0;
                                $table_returned_to_supplier_count[$year][$month][$city] =            $table_returned_to_supplier_amount[$year][$month][$city] = 0;
                            }

                            // avoid error in case of filtering by city
                            $city = 'All';
                            $table_total_received_count[$year][$month][$city] =                  $table_total_received_amount[$year][$month][$city] = 0;
                            $table_paid_to_supplier_count[$year][$month][$city] =                $table_paid_to_supplier_amount[$year][$month][$city] = 0;
                            $table_not_paid_to_supplier_count[$year][$month][$city] =            $table_not_paid_to_supplier_amount[$year][$month][$city] = 0;
                            $table_delivered_not_localy_transfared_count[$year][$month][$city] = $table_delivered_not_localy_transfared_amount[$year][$month][$city] = 0;
                            $table_delivered_with_captain_count[$year][$month][$city] =          $table_delivered_with_captain_amount[$year][$month][$city] = 0;
                            $table_total_delivered_count[$year][$month][$city] =                 $table_total_delivered_amount[$year][$month][$city] = 0;
                            $table_in_warehouse_count[$year][$month][$city] =                    $table_in_warehouse_amount[$year][$month][$city] = 0;
                            $table_with_captain_count[$year][$month][$city] =                    $table_with_captain_amount[$year][$month][$city] = 0;
                            $table_returned_to_supplier_count[$year][$month][$city] =            $table_returned_to_supplier_amount[$year][$month][$city] = 0;
                        }
                    }

                    // trim city strings
                    for($i = 0; $i < count($total_received); ++$i) $total_received[$i]->d_city = trim($total_received[$i]->d_city);
                    for($i = 0; $i < count($paid_to_supplier); ++$i) $paid_to_supplier[$i]->d_city = trim($paid_to_supplier[$i]->d_city);
                    for($i = 0; $i < count($not_paid_to_supplier); ++$i) $not_paid_to_supplier[$i]->d_city = trim($not_paid_to_supplier[$i]->d_city);
                    for($i = 0; $i < count($delivered_not_localy_transfared); ++$i) $delivered_not_localy_transfared[$i]->d_city = trim($delivered_not_localy_transfared[$i]->d_city);
                    for($i = 0; $i < count($delivered_with_captain); ++$i) $delivered_with_captain[$i]->d_city = trim($delivered_with_captain[$i]->d_city);
                    for($i = 0; $i < count($total_delivered); ++$i) $total_delivered[$i]->d_city = trim($total_delivered[$i]->d_city);
                    for($i = 0; $i < count($in_warehouse); ++$i) $in_warehouse[$i]->d_city = trim($in_warehouse[$i]->d_city);
                    for($i = 0; $i < count($with_captain); ++$i) $with_captain[$i]->d_city = trim($with_captain[$i]->d_city);
                    for($i = 0; $i < count($returned_to_supplier); ++$i) $returned_to_supplier[$i]->d_city = trim($returned_to_supplier[$i]->d_city);

                    // adding the values in all table rows according to the month and city
                    foreach($total_received as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_total_received_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_total_received_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_total_received_count [$record->year][$record->month]['All'] += $record->count;
                        $table_total_received_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($paid_to_supplier as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_paid_to_supplier_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_paid_to_supplier_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_paid_to_supplier_count [$record->year][$record->month]['All'] += $record->count;
                        $table_paid_to_supplier_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($not_paid_to_supplier as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_not_paid_to_supplier_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_not_paid_to_supplier_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_not_paid_to_supplier_count [$record->year][$record->month]['All'] += $record->count;
                        $table_not_paid_to_supplier_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($delivered_not_localy_transfared as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_delivered_not_localy_transfared_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_delivered_not_localy_transfared_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_delivered_not_localy_transfared_count [$record->year][$record->month]['All'] += $record->count;
                        $table_delivered_not_localy_transfared_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($delivered_with_captain as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_delivered_with_captain_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_delivered_with_captain_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_delivered_with_captain_count [$record->year][$record->month]['All'] += $record->count;
                        $table_delivered_with_captain_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($total_delivered as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_total_delivered_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_total_delivered_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_total_delivered_count [$record->year][$record->month]['All'] += $record->count;
                        $table_total_delivered_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($in_warehouse as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_in_warehouse_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_in_warehouse_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_in_warehouse_count [$record->year][$record->month]['All'] += $record->count;
                        $table_in_warehouse_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($with_captain as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_with_captain_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_with_captain_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_with_captain_count [$record->year][$record->month]['All'] += $record->count;
                        $table_with_captain_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    foreach($returned_to_supplier as $record) {
                        if(!isset($main_city[$record->d_city])){
                            continue;
                        }
                        $record->d_city = $main_city[$record->d_city];
                        $table_returned_to_supplier_count[$record->year][$record->month][$record->d_city] += $record->count;
                        $table_returned_to_supplier_amount[$record->year][$record->month][$record->d_city] += $record->amount;

                        $table_returned_to_supplier_count [$record->year][$record->month]['All'] += $record->count;
                        $table_returned_to_supplier_amount[$record->year][$record->month]['All'] += $record->amount;
                    }
                    ?>
                    <!-- start printing months -->
                    @foreach($years as $year)
                        @foreach($months as $month)
                            <?php
                            $today = strtotime(date('Y-m-d'));
                            $current_loop = strtotime($year . '-' . str_pad($month, 2, "0", STR_PAD_LEFT) . '-01');
                            $oldest = strtotime("2018-03-01");
                            if($current_loop > $today || $current_loop < $oldest)
                                continue;
                            ?>
                            <div class="x_panel tile fixed_height_580 overflow_hidden row" style="overflow-x:auto; overflow-y:auto;">
                                <h1>{{ $month_name[$month] }} {{ $year }}</h1><br/>
                                <div class="x_title table-responsive" style="overflow-x:auto; overflow-y:hidden;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2"></th>
                                                @foreach($cities as $city)
                                                    <th colspan="4">{{ $city == 'All' ? 'Total' : $city }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <th>Cnt.</th>
                                                    <th>Amt.</th>
                                                    <th>%Tot.</th>
                                                    <th>%Del.</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Total Received</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_total_received_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_total_received_amount[$year][$month][$city], 2) }}</td>
                                                    <td>-{{ /* number_format($table_total_received_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) */ ''}}</td>
                                                    <td>-{{ /* number_format($table_total_received_amount[$year][$month][$city] / $table_total_delivered_amount[$year][$month][$city] * 100.0, 2) */ ''}}</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Paid To Supplier</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_paid_to_supplier_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_paid_to_supplier_amount[$year][$month][$city], 2) }}</td>
                                                    <td class="important">{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_paid_to_supplier_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td class="important">{{ $table_total_delivered_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_paid_to_supplier_amount[$year][$month][$city] / $table_total_delivered_amount[$year][$month][$city] * 100.0, 2) }}%</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Not Paid To Supplier</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_not_paid_to_supplier_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_not_paid_to_supplier_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_not_paid_to_supplier_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>{{ $table_total_delivered_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_not_paid_to_supplier_amount[$year][$month][$city] / $table_total_delivered_amount[$year][$month][$city] * 100.0, 2) }}%</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Delivered Not Locally Transferred</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_delivered_not_localy_transfared_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_delivered_not_localy_transfared_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_delivered_not_localy_transfared_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>{{ $table_total_delivered_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_delivered_not_localy_transfared_amount[$year][$month][$city] / $table_total_delivered_amount[$year][$month][$city] * 100.0, 2) }}%</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Delivered With Captains</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_delivered_with_captain_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_delivered_with_captain_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_delivered_with_captain_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>{{ $table_total_delivered_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_delivered_with_captain_amount[$year][$month][$city] / $table_total_delivered_amount[$year][$month][$city] * 100.0, 2) }}%</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Delivered</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_total_delivered_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_total_delivered_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_total_delivered_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>-</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>In Warehouse</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_in_warehouse_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_in_warehouse_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_in_warehouse_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>-</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>With Captain</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_with_captain_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_with_captain_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_with_captain_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>-</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Returned To Supplier</td>
                                                @foreach($cities as $city)
                                                    <td>{{ number_format($table_returned_to_supplier_count[$year][$month][$city]) }}</td>
                                                    <td>{{ number_format($table_returned_to_supplier_amount[$year][$month][$city], 2) }}</td>
                                                    <td>{{ $table_total_received_amount[$year][$month][$city] == 0 ? '0.00' : number_format($table_returned_to_supplier_amount[$year][$month][$city] / $table_total_received_amount[$year][$month][$city]  * 100.0, 2) }}%</td>
                                                    <td>-</td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="col horizontal-divider" style="margin-top: 30px"></div>
                                    <ul class="nav navbar-left panel_toolbox">
                                        <li><a class="collapse-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseData"><i class="fa fa-chevron-down"></i></a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- ------------------------------------------------------------------------------------------------------------------------ Calculate Days Table ----------------------- -->
                                <?php 
                                // initialize arrays $row_of_table_count[year][month][city] and $row_of_table_amount[year][month][city]
                                $days_total_received_count =                  $days_total_received_amount = [];
                                $days_paid_to_supplier_count =                $days_paid_to_supplier_amount = [];
                                $days_not_paid_to_supplier_count =            $days_not_paid_to_supplier_amount = [];
                                $days_delivered_not_localy_transfared_count = $days_delivered_not_localy_transfared_amount = [];
                                $days_delivered_with_captain_count =          $days_delivered_with_captain_amount = [];
                                $days_total_delivered_count =                 $days_total_delivered_amount = [];
                                $days_in_warehouse_count =                    $days_in_warehouse_amount = [];
                                $days_with_captain_count =                    $days_with_captain_amount = [];
                                $days_returned_to_supplier_count =            $days_returned_to_supplier_amount = [];
                                foreach($years as $_year) {

                                    $days_total_received_count[$_year] =                  $days_total_received_amount[$_year] = [];
                                    $days_paid_to_supplier_count[$_year] =                $days_paid_to_supplier_amount[$_year] = [];
                                    $days_not_paid_to_supplier_count[$_year] =            $days_not_paid_to_supplier_amount[$_year] = [];
                                    $days_delivered_not_localy_transfared_count[$_year] = $days_delivered_not_localy_transfared_amount[$_year] = [];
                                    $days_delivered_with_captain_count[$_year] =          $days_delivered_with_captain_amount[$_year] = [];
                                    $days_total_delivered_count[$_year] =                 $days_total_delivered_amount[$_year] = [];
                                    $days_in_warehouse_count[$_year] =                    $days_in_warehouse_amount[$_year] = [];
                                    $days_with_captain_count[$_year] =                    $days_with_captain_amount[$_year] = [];
                                    $days_returned_to_supplier_count[$_year] =            $days_returned_to_supplier_amount[$_year] = [];

                                    foreach($months as $_month) {

                                        $days_total_received_count[$_year][$_month] =                  $days_total_received_amount[$_year][$_month] = [];
                                        $days_paid_to_supplier_count[$_year][$_month] =                $days_paid_to_supplier_amount[$_year][$_month] = [];
                                        $days_not_paid_to_supplier_count[$_year][$_month] =            $days_not_paid_to_supplier_amount[$_year][$_month] = [];
                                        $days_delivered_not_localy_transfared_count[$_year][$_month] = $days_delivered_not_localy_transfared_amount[$_year][$_month] = [];
                                        $days_delivered_with_captain_count[$_year][$_month] =          $days_delivered_with_captain_amount[$_year][$_month] = [];
                                        $days_total_delivered_count[$_year][$_month] =                 $days_total_delivered_amount[$_year][$_month] = [];
                                        $days_in_warehouse_count[$_year][$_month] =                    $days_in_warehouse_amount[$_year][$_month] = [];
                                        $days_with_captain_count[$_year][$_month] =                    $days_with_captain_amount[$_year][$_month] = [];
                                        $days_returned_to_supplier_count[$_year][$_month] =            $days_returned_to_supplier_amount[$_year][$_month] = [];

                                        for($i = 31; $i >= 1; --$i) {

                                            $days_total_received_count[$_year][$_month][$i] =                  $days_total_received_amount[$_year][$_month][$i] = [];
                                            $days_paid_to_supplier_count[$_year][$_month][$i] =                $days_paid_to_supplier_amount[$_year][$_month][$i] = [];
                                            $days_not_paid_to_supplier_count[$_year][$_month][$i] =            $days_not_paid_to_supplier_amount[$_year][$_month][$i] = [];
                                            $days_delivered_not_localy_transfared_count[$_year][$_month][$i] = $days_delivered_not_localy_transfared_amount[$_year][$_month][$i] = [];
                                            $days_delivered_with_captain_count[$_year][$_month][$i] =          $days_delivered_with_captain_amount[$_year][$_month][$i] = [];
                                            $days_total_delivered_count[$_year][$_month][$i] =                 $days_total_delivered_amount[$_year][$_month][$i] = [];
                                            $days_in_warehouse_count[$_year][$_month][$i] =                    $days_in_warehouse_amount[$_year][$_month][$i] = [];
                                            $days_with_captain_count[$_year][$_month][$i] =                    $days_with_captain_amount[$_year][$_month][$i] = [];
                                            $days_returned_to_supplier_count[$_year][$_month][$i] =            $days_returned_to_supplier_amount[$_year][$_month][$i] = [];

                                            foreach($cities as $city) {

                                                $days_total_received_count[$_year][$_month][$i][$city] =                  $days_total_received_amount[$_year][$_month][$i][$city] = 0;
                                                $days_paid_to_supplier_count[$_year][$_month][$i][$city] =                $days_paid_to_supplier_amount[$_year][$_month][$i][$city] = 0;
                                                $days_not_paid_to_supplier_count[$_year][$_month][$i][$city] =            $days_not_paid_to_supplier_amount[$_year][$_month][$i][$city] = 0;
                                                $days_delivered_not_localy_transfared_count[$_year][$_month][$i][$city] = $days_delivered_not_localy_transfared_amount[$_year][$_month][$i][$city] = 0;
                                                $days_delivered_with_captain_count[$_year][$_month][$i][$city] =          $days_delivered_with_captain_amount[$_year][$_month][$i][$city] = 0;
                                                $days_total_delivered_count[$_year][$_month][$i][$city] =                 $days_total_delivered_amount[$_year][$_month][$i][$city] = 0;
                                                $days_in_warehouse_count[$_year][$_month][$i][$city] =                    $days_in_warehouse_amount[$_year][$_month][$i][$city] = 0;
                                                $days_with_captain_count[$_year][$_month][$i][$city] =                    $days_with_captain_amount[$_year][$_month][$i][$city] = 0;
                                                $days_returned_to_supplier_count[$_year][$_month][$i][$city] =            $days_returned_to_supplier_amount[$_year][$_month][$i][$city] = 0;

                                            }

                                            // avoid error in case of filtering by city
                                            $city = 'All';
                                            $days_total_received_count[$_year][$_month][$i][$city] =                  $days_total_received_amount[$_year][$_month][$i][$city] = 0;
                                            $days_paid_to_supplier_count[$_year][$_month][$i][$city] =                $days_paid_to_supplier_amount[$_year][$_month][$i][$city] = 0;
                                            $days_not_paid_to_supplier_count[$_year][$_month][$i][$city] =            $days_not_paid_to_supplier_amount[$_year][$_month][$i][$city] = 0;
                                            $days_delivered_not_localy_transfared_count[$_year][$_month][$i][$city] = $days_delivered_not_localy_transfared_amount[$_year][$_month][$i][$city] = 0;
                                            $days_delivered_with_captain_count[$_year][$_month][$i][$city] =          $days_delivered_with_captain_amount[$_year][$_month][$i][$city] = 0;
                                            $days_total_delivered_count[$_year][$_month][$i][$city] =                 $days_total_delivered_amount[$_year][$_month][$i][$city] = 0;
                                            $days_in_warehouse_count[$_year][$_month][$i][$city] =                    $days_in_warehouse_amount[$_year][$_month][$i][$city] = 0;
                                            $days_with_captain_count[$_year][$_month][$i][$city] =                    $days_with_captain_amount[$_year][$_month][$i][$city] = 0;
                                            $days_returned_to_supplier_count[$_year][$_month][$i][$city] =            $days_returned_to_supplier_amount[$_year][$_month][$i][$city] = 0;

                                        }

                                    }

                                }

                                // adding the values in all table rows according to the month and city
                                foreach($total_received as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_total_received_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_total_received_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_total_received_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_total_received_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($paid_to_supplier as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_paid_to_supplier_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_paid_to_supplier_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_paid_to_supplier_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_paid_to_supplier_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($not_paid_to_supplier as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_not_paid_to_supplier_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_not_paid_to_supplier_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_not_paid_to_supplier_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_not_paid_to_supplier_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($delivered_not_localy_transfared as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_delivered_not_localy_transfared_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_delivered_not_localy_transfared_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_delivered_not_localy_transfared_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_delivered_not_localy_transfared_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($delivered_with_captain as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_delivered_with_captain_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_delivered_with_captain_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_delivered_with_captain_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_delivered_with_captain_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($total_delivered as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_total_delivered_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_total_delivered_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_total_delivered_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_total_delivered_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($in_warehouse as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_in_warehouse_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_in_warehouse_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_in_warehouse_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_in_warehouse_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($with_captain as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_with_captain_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_with_captain_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_with_captain_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_with_captain_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }
                                foreach($returned_to_supplier as $record) {
                                    if(!isset($main_city[$record->d_city])){
                                        continue;
                                    }
                                    $record->d_city = $main_city[$record->d_city];
                                    $days_returned_to_supplier_count[$record->year][$record->month][$record->day][$record->d_city] += $record->count;
                                    $days_returned_to_supplier_amount[$record->year][$record->month][$record->day][$record->d_city] += $record->amount;

                                    $days_returned_to_supplier_count [$record->year][$record->month][$record->day]['All'] += $record->count;
                                    $days_returned_to_supplier_amount[$record->year][$record->month][$record->day]['All'] += $record->amount;
                                }

                                 ?>
                                <div id="collapseTwo" class="x_content collapse table-responsive" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <table class="table table-bordered table-striped" style="border: 1px solid black;">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" style="padding-bottom: 1.5%;">Day</th>
                                                <th rowspan="2"></th>
                                                @foreach($cities as $city)
                                                    <th colspan="4">{{ $city == 'All' ? 'Total' : $city }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach($cities as $city)
                                                    <th>Cnt.</th>
                                                    <th>Amt.</th>
                                                    <th>%Tot.</th>
                                                    <th>%Del.</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for($i = 31; $i >= 1; --$i) { ?>
                                                <?php 
                                                    $flag = true;
                                                    foreach($cities as $city)
                                                        if($days_total_received_count[$year][$month][$i][$city] != 0) {
                                                            $flag = false;
                                                            break;
                                                        }         
                                                    if($flag)
                                                        continue;                                       
                                                 ?>
                                                <tr>
                                                    <td rowspan="9" style="padding-top: 13%;">{{ $i }} / {{ $month_name[$month] }}</td>
                                                    <td>Total Received</td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_total_received_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_total_received_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>Paid To Supplier</td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_paid_to_supplier_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_paid_to_supplier_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td class="important">{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_paid_to_supplier_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td class="important">{{ $days_total_delivered_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_paid_to_supplier_amount[$year][$month][$i][$city] / $days_total_delivered_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>Not Paid To Supplier</td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_not_paid_to_supplier_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_not_paid_to_supplier_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_not_paid_to_supplier_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>{{ $days_total_delivered_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_not_paid_to_supplier_amount[$year][$month][$i][$city] / $days_total_delivered_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>Delivered Not Locally Transferred</td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_delivered_not_localy_transfared_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_delivered_not_localy_transfared_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_delivered_not_localy_transfared_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>{{ $days_total_delivered_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_delivered_not_localy_transfared_amount[$year][$month][$i][$city] / $days_total_delivered_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>Delivered With Captains </td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_delivered_with_captain_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_delivered_with_captain_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_delivered_with_captain_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>{{ $days_total_delivered_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_delivered_with_captain_amount[$year][$month][$i][$city] / $days_total_delivered_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>Total Delivered </td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_total_delivered_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_total_delivered_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_total_delivered_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>-</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>In Warehouse </td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_in_warehouse_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_in_warehouse_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_in_warehouse_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>-</td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>With Captains </td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_with_captain_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_with_captain_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_with_captain_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>-</td>
                                                    @endforeach
                                                </tr>
                                                <tr class="bottom-border">
                                                    <td>Returned To Supplier </td>
                                                    @foreach($cities as $city)
                                                        <td>{{ number_format($days_returned_to_supplier_count[$year][$month][$i][$city]) }} </td>
                                                        <td>{{ number_format($days_returned_to_supplier_amount[$year][$month][$i][$city], 2) }}</td>
                                                        <td>{{ $days_total_received_amount[$year][$month][$i][$city] == 0 ? '0.00' : number_format($days_returned_to_supplier_amount[$year][$month][$i][$city] / $days_total_received_amount[$year][$month][$i][$city] * 100.0, 2) }}%</td>
                                                        <td>-</td>
                                                    @endforeach
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>

   </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>



@stop
