@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')

<style>
table th {
    text-align: center;
}
table td {
    text-align: center;
}
</style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="get" action="/warehouse/shipmentsperbranchpost">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control"
                                            type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($companieslist as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Hub </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="hub" id="hub" class="form-control">
                                        @foreach($adminHubs as $adhub)
                                            <option value="{{$adhub->hub_id}}"<?php if (isset($hub) && $hub == $adhub->hub_id) echo 'selected';?>>{{$adhub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" class="date-picker form-control col-md-7 col-xs-12" name="from" value="{{ $from }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" class="date-picker form-control col-md-7 col-xs-12" name="to" value="{{ $to }}" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <p><button id="submitForm" class="btn btn-success">Filter</button></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel">

                        <!-- Total By Company, City and Hub  Tables -->
                        <?php
                        $total_comp_returned_parcels = [];
                        $total_comp_returned_amount = [];
                        $total_comp_paid_parcels = [];
                        $total_comp_paid_amount = [];
                        $total_comp_paid_rate_amount = [];
                        $total_comp_paid_samecityrate_amount = [];

                        $total_city_returned_parcels = [];
                        $total_city_returned_amount = [];
                        $total_city_paid_parcels = [];
                        $total_city_paid_amount = [];

                        $total_hub_returned_parcels = [];
                        $total_hub_returned_amount = [];
                        $total_hub_paid_parcels = [];
                        $total_hub_paid_amount = [];

                        $all_comp_returned_parcels = $all_comp_returned_amount = $all_comp_paid_parcels = $all_comp_paid_amount = 0;
                        $all_city_returned_parcels = $all_city_returned_amount = $all_city_paid_parcels = $all_city_paid_amount  = 0;
                        $all_hub_returned_parcels = $all_hub_returned_amount = $all_hub_paid_parcels = $all_hub_paid_amount  = 0;

                        // initialize the arrays
                        foreach($companies as $company){
                            $total_comp_returned_parcels[$company->name] = 0;
                            $total_comp_returned_amount[$company->name] = 0;
                            $total_comp_paid_parcels[$company->name] = 0;
                            $total_comp_paid_amount[$company->name] = 0;
                            $total_comp_paid_rate_amount[$company->name] = 0;
                            $total_comp_paid_samecityrate_amount[$company->name] = 0;
                        }
                        foreach($adminCities as $adcity){
                            $total_city_returned_parcels[$adcity->city] = 0;
                            $total_city_returned_amount[$adcity->city] = 0;
                            $total_city_paid_parcels[$adcity->city] = 0;
                            $total_city_paid_amount[$adcity->city] = 0;
                        }
                        foreach($adminHubs as $adhub){
                            $total_hub_returned_parcels[$adhub->hub_id] = 0;
                            $total_hub_returned_amount[$adhub->hub_id] = 0;
                            $total_hub_paid_parcels[$adhub->hub_id] = 0;
                            $total_hub_paid_amount[$adhub->hub_id] = 0;
                        }

                        // calculate the total tables
                        foreach($returnedgrouped as $item) {
                            $total_comp_returned_parcels[$item->companyname] += $item->returnedtosupplierparcels;
                            $total_comp_returned_amount[$item->companyname] += $item->returnedtosupplieramount;
                            $total_city_returned_parcels[$item->city] += $item->returnedtosupplierparcels;
                            $total_city_returned_amount[$item->city] += $item->returnedtosupplieramount;
                            $total_hub_returned_parcels[$item->hub_id] += $item->returnedtosupplierparcels;
                            $total_hub_returned_amount[$item->hub_id] += $item->returnedtosupplieramount;

                            $all_comp_returned_parcels += $item->returnedtosupplierparcels;
                            $all_comp_returned_amount += $item->returnedtosupplieramount;
                            $all_city_returned_parcels += $item->returnedtosupplierparcels;
                            $all_city_returned_amount += $item->returnedtosupplieramount;
                            $all_hub_returned_parcels += $item->returnedtosupplierparcels;
                            $all_hub_returned_amount += $item->returnedtosupplieramount;
                        }
                        foreach($returnednongrouped as $item) {
                            $total_comp_returned_parcels[$item->companyname] += $item->returnedtosupplierparcels;
                            $total_comp_returned_amount[$item->companyname] += $item->returnedtosupplieramount;
                            $total_city_returned_parcels[$item->city] += $item->returnedtosupplierparcels;
                            $total_city_returned_amount[$item->city] += $item->returnedtosupplieramount;
                            $total_hub_returned_parcels[$item->hub_id] += $item->returnedtosupplierparcels;
                            $total_hub_returned_amount[$item->hub_id] += $item->returnedtosupplieramount;

                            $all_comp_returned_parcels += $item->returnedtosupplierparcels;
                            $all_comp_returned_amount += $item->returnedtosupplieramount;
                            $all_city_returned_parcels += $item->returnedtosupplierparcels;
                            $all_city_returned_amount += $item->returnedtosupplieramount;
                            $all_hub_returned_parcels += $item->returnedtosupplierparcels;
                            $all_hub_returned_amount += $item->returnedtosupplieramount;
                        }
                        foreach($paid as $item) {
                            $rate = $companiesbyname[$item->companyname]->rate;
                            $samecityrate = $companiesbyname[$item->companyname]->samecityrate;

                            $total_comp_paid_parcels[$item->companyname] += $item->paidtosupplierparcels;
                            $total_comp_paid_amount[$item->companyname] += $item->paidtosupplieramount;
                            $total_comp_paid_rate_amount[$item->companyname] += $item->paidtosupplierparcels * ($item->city != $companiesbyname[$item->companyname]->city ? $rate : 0);
                            $total_comp_paid_samecityrate_amount[$item->companyname] += $item->paidtosupplierparcels * ($item->city != $companiesbyname[$item->companyname]->city ? 0 : $samecityrate);
                            $total_city_paid_parcels[$item->city] += $item->paidtosupplierparcels;
                            $total_city_paid_amount[$item->city] += $item->paidtosupplieramount;
                            $total_hub_paid_parcels[$item->hub_id] += $item->paidtosupplierparcels;
                            $total_hub_paid_amount[$item->hub_id] += $item->paidtosupplieramount;

                            $all_comp_paid_parcels += $item->paidtosupplierparcels;
                            $all_comp_paid_amount += $item->paidtosupplieramount;
                            $all_city_paid_parcels += $item->paidtosupplierparcels;
                            $all_city_paid_amount += $item->paidtosupplieramount;
                            $all_hub_paid_parcels += $item->paidtosupplierparcels;
                            $all_hub_paid_amount += $item->paidtosupplieramount;
                        }

                        // fix display format for totals table
                        $all_comp_returned_parcels = number_format($all_comp_returned_parcels);
                        $all_comp_returned_amount = number_format($all_comp_returned_amount, 2);
                        $all_comp_paid_parcels = number_format($all_comp_paid_parcels);
                        $all_comp_paid_amount = number_format($all_comp_paid_amount, 2);

                        $all_city_returned_parcels = number_format($all_city_returned_parcels);
                        $all_city_returned_amount = number_format($all_city_returned_amount, 2);
                        $all_city_paid_parcels = number_format($all_city_paid_parcels);
                        $all_city_paid_amount = number_format($all_city_paid_amount, 2);

                        $all_hub_returned_parcels = number_format($all_hub_returned_parcels);
                        $all_hub_returned_amount = number_format($all_hub_returned_amount, 2);
                        $all_hub_paid_parcels = number_format($all_hub_paid_parcels);
                        $all_hub_paid_amount = number_format($all_hub_paid_amount, 2);

                        /////////////

                        $data_companies = '';
                        $tra = 0; // total rate amount
                        foreach($companies as $company){
                            $rate = $company->rate; 
                            $samecityrate = $company->samecityrate; 
                            $tcpra = ($total_comp_paid_rate_amount[$company->name] + $total_comp_paid_samecityrate_amount[$company->name]) * 0.2; // total company paid rate amount 
                            $tra += $tcpra;
                            $tcrp = number_format($total_comp_returned_parcels[$company->name]);
                            $tcra = number_format($total_comp_returned_amount[$company->name], 2);
                            $tcpp = number_format($total_comp_paid_parcels[$company->name]);
                            $tcpa = number_format($total_comp_paid_amount[$company->name], 2);
                            if($tcrp == 0 && $tcra == 0 && $tcpp == 0 && $tcpa == 0)
                                continue;
                            $rate = number_format($rate, 2);
                            $samecityrate = number_format($samecityrate, 2);
                            $tcpra = number_format($tcpra, 2);
                            $data_companies .= "[ \"<span style='display: none;'> </span>$company->name\", \"$tcpp\", \"$rate\", \"$samecityrate\", \"$tcpa\", \"$tcpra\", \"$tcrp\", \"$tcra\" ], ";
                        }
                        $tra = number_format($tra, 2);
                        $data_companies .= "[ \"<b>Total</b>\", \"<b>$all_comp_paid_parcels</b>\", \"\", \"\", \"<b>$all_city_paid_amount</b>\", \"<b>$tra</b>\", \"<b>$all_comp_returned_parcels</b>\", \"<b>$all_comp_returned_amount</b>\" ] ,";

                        $data_cities = '';
                        foreach($citieslist as $adcity){
                            if($adcity != 'All'){
                                $tcrp = number_format($total_city_returned_parcels[$adcity]);
                                $tcra = number_format($total_city_returned_amount[$adcity], 2);
                                $tcpp = number_format($total_city_paid_parcels[$adcity]);
                                $tcpa = number_format($total_city_paid_amount[$adcity], 2);
                                $data_cities .= "[ \"<span style='display: none;'> </span>$adcity\", \"$tcpp\", \"$tcpa\", \"$tcrp\", \"$tcra\" ], ";
                            }
                        }
                        $data_cities .= "[ \"<b>Total</b>\", \"<b>$all_city_paid_parcels</b>\", \"<b>$all_city_paid_amount</b>\", \"<b>$all_city_returned_parcels</b>\", \"<b>$all_city_returned_amount</b>\" ]";

                        $data_hubs = '';
                        foreach($hubslist as $adhub){
                            if($hub_by_id[$adhub]->name != 'All'){
                                $thrp = number_format($total_hub_returned_parcels[$adhub]);
                                $thra = number_format($total_hub_returned_amount[$adhub], 2);
                                $thpp = number_format($total_hub_paid_parcels[$adhub]);
                                $thpa = number_format($total_hub_paid_amount[$adhub], 2);
                                $hub_name = $hub_by_id[$adhub]->name;
                                $data_hubs .= "[ \"$adhub\", \"<span style='display: none;'> </span>$hub_name\", \"$thpp\", \"$thpa\", \"$thrp\", \"$thra\" ], ";
                            }
                        }
                        ++$adhub;
                        $data_hubs .= "[ \"$adhub\", \"<b>Total</b>\", \"<b>$all_hub_paid_parcels</b>\", \"<b>$all_hub_paid_amount</b>\", \"<b>$all_hub_returned_parcels</b>\", \"<b>$all_hub_returned_amount</b>\" ]";

                        ?>

                        <br/>

                        <!-- Total By Company -->
                        <h3 style="float: left;">Total By Company</h3><div style="clear: both;"></div>
                        <table id="datatable_companies" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Paid To Supplier Parcels</th>
                                    <th>Different City Rate Amount</th>
                                    <th>Same City Rate Amount</th>
                                    <th>Paid To Supplier Amount</th>
                                    <th>Rate Amount</th>
                                    <th>Returned To Supplier Parcels</th>
                                    <th>Returned To Supplier Amount</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Company</th>
                                    <th>Paid To Supplier Parcels</th>
                                    <th>Different City Rate Amount</th>
                                    <th>Same City Rate Amount</th>
                                    <th>Paid To Supplier Amount</th>
                                    <th>Rate Amount</th>
                                    <th>Returned To Supplier Parcels</th>
                                    <th>Returned To Supplier Amount</th>
                                </tr>
                            </tfoot>
                        </table>

                        <br/>

                        <!-- Total By City -->
                        <br/><br/><h3 style="float: left;">Total By City</h3><div style="clear: both;"></div>
                        <table id="datatable_cities" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>City</th>
                                <th>Paid To Supplier Parcels</th>
                                <th>Paid To Supplier Amount</th>
                                <th>Returned To Supplier Parcels</th>
                                <th>Returned To Supplier Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>City</th>
                                <th>Paid To Supplier Parcels</th>
                                <th>Paid To Supplier Amount</th>
                                <th>Returned To Supplier Parcels</th>
                                <th>Returned To Supplier Amount</th>
                            </tr>
                            </tfoot>
                        </table>

                        <!-- Total By Hub -->
                        <br/><br/><h3 style="float: left;">Total By Hub</h3><div style="clear: both;"></div>
                        <table id="datatable_hubs" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Hub</th>
                                <th>Paid To Supplier Parcels</th>
                                <th>Paid To Supplier Amount</th>
                                <th>Returned To Supplier Parcels</th>
                                <th>Returned To Supplier Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Hub</th>
                                <th>Paid To Supplier Parcels</th>
                                <th>Paid To Supplier Amount</th>
                                <th>Returned To Supplier Parcels</th>
                                <th>Returned To Supplier Amount</th>
                            </tr>
                            </tfoot>
                        </table>

                        <div style="clear: both;"></div><br/>
                        <!-- End of Totals Tables -->

                        <hr/>

                        <!-- --------------------------------- -->

                        <!-- Companies Tables -->
                        @foreach($companies as $company)
                            <br/><br/><br/>
                            <div style="float: left;">
                                <br/>
                                <h3>{{ $company->name }}</h3>
                            </div>
                            <div class="col-xs-3" style="float: right;">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th>Different City Rate Amount</th>
                                        <td>{{ $rates[$company->name] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Same City Rate Amount</th>
                                        <td>{{ $samecityrates[$company->name] }}</td>
                                    </tr>
                                </table>
                            </div>
                            <table id="datatable_{{ $company->id }}" class="table table-striped table-bordered">
                                <div class="clear: both;"></div>
                                <thead>
                                    <tr>
                                        <th>Branch</th>
                                        <th>Hub</th>
                                        <th>Returned To Supplier Parcels</th>
                                        <th>Returned To Supplier Amount</th>
                                        <th>Paid to Supplier Parcels</th>
                                        <th>Paid to Supplier Amount</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <thead>
                                    <tr>
                                        <th>Branch </th>
                                        <th>Hub </th>
                                        <th>Returned To Supplier Parcels</th>
                                        <th>Returned To Supplier Amount</th>
                                        <th>Paid to Supplier Parcels</th>
                                        <th>Paid to Supplier Amount</th>
                                    </tr>
                                </thead>
                            </table>
                            <br/><br/>
                        @endforeach
                    <!-- End of Companies Tables -->

                    </div>
                </div>
            </div>


    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


    <script>
        $(document).ready(function () {

            $("#datatable_cities").DataTable({

                "data": [
                    <?php echo $data_cities; ?>
                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });

        });

        $(document).ready(function () {

            $("#datatable_hubs").DataTable({

                "data": [
                    <?php echo $data_hubs; ?>
                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']],
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });

        });

        $(document).ready(function () {

            $("#datatable_companies").DataTable({

                "data": [
                    <?php echo $data_companies; ?>
                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });

        });
    </script>


    <?php
        $data = [];
        foreach($companies as $company){
            $data[$company->id] = '';
            $all_returned_parcels = $all_returned_amount = $all_paid_parcels = $all_paid_amount = 0;
            foreach($hubslist as $ahub){
                $current_hub = $hub_by_id[$ahub];
                if($current_hub->name != 'All'){
                    $returned_parcels = $returned_amount = $paid_parcels = $paid_amount = 0;
                    for($i = 0; $i < count($returnedgrouped); ++$i){
                        if($returnedgrouped[$i]->companyname == $company->name && $returnedgrouped[$i]->city == $current_hub->city && $returnedgrouped[$i]->hub_id == $current_hub->id){
                            $returned_parcels += $returnedgrouped[$i]->returnedtosupplierparcels;
                            $returned_amount += $returnedgrouped[$i]->returnedtosupplieramount;
                            $all_returned_parcels += $returnedgrouped[$i]->returnedtosupplierparcels;
                            $all_returned_amount += $returnedgrouped[$i]->returnedtosupplieramount;
                        }
                    }
                    for($i = 0; $i < count($returnednongrouped); ++$i){
                        if($returnednongrouped[$i]->companyname == $company->name && $returnednongrouped[$i]->city == $current_hub->city && $returnednongrouped[$i]->hub_id == $current_hub->id){
                            $returned_parcels += $returnednongrouped[$i]->returnedtosupplierparcels;
                            $returned_amount += $returnednongrouped[$i]->returnedtosupplieramount;
                            $all_returned_parcels += $returnednongrouped[$i]->returnedtosupplierparcels;
                            $all_returned_amount += $returnednongrouped[$i]->returnedtosupplieramount;
                        }
                    }
                    for($i = 0; $i < count($paid); ++$i){
                        if($paid[$i]->companyname == $company->name && $paid[$i]->city == $current_hub->city && $paid[$i]->hub_id == $current_hub->id){
                            $paid_parcels += $paid[$i]->paidtosupplierparcels;
                            $paid_amount += $paid[$i]->paidtosupplieramount;
                            $all_paid_parcels += $paid[$i]->paidtosupplierparcels;
                            $all_paid_amount += $paid[$i]->paidtosupplieramount;
                        }
                    }
                    $returned_parcels = number_format($returned_parcels);
                    $returned_amount = number_format($returned_amount, 2);
                    $paid_parcels = number_format($paid_parcels);
                    $paid_amount = number_format($paid_amount, 2);

                    if($returned_parcels == 0 && $returned_amount == 0 && $paid_parcels == 0 && $paid_amount == 0)
                        continue;
                    $data[$company->id] .= "[ \"<span style='display: none;'> </span>$current_hub->city\", \"$current_hub->name\", \"$returned_parcels\" , \"$returned_amount\" , \"$paid_parcels\" , \"$paid_amount\" ], ";
                }
            }
            $all_returned_parcels = number_format($all_returned_parcels);
            $all_returned_amount = number_format($all_returned_amount, 2);
            $all_paid_parcels = number_format($all_paid_parcels);
            $all_paid_amount = number_format($all_paid_amount, 2);

            $data[$company->id] .= "[ \"<b>Total</b>\", \"\", \"<b>$all_returned_parcels</b>\", \"<b>$all_returned_amount</b>\" , \"<b>$all_paid_parcels</b>\" , \"<b>$all_paid_amount</b>\" ] ";
        }

    ?>

    @foreach($companies as $company)
    <script>
        $(document).ready(function () {
            $("#datatable_{{ $company->id }}").DataTable({
                "data": [
                    <?php echo $data[$company->id]; ?>
                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm",
                        title: '{{ $company->name }}, Rate {{ $rates[$company->name] }}',
                    },
                ],
                responsive: true,

                'order': [[0, 'asc']]
            });

        });
    </script>
    @endforeach

        </div>

    </div>


@stop
