@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

    @endif

    @extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


    @section('content')

            <!-- page content -->
    <!-- page content -->
    <style>
        /* The Modal (background) */
        #modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 400px; /* Could be more or less, depending on screen size */
            height: 250px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Captain Scanning Per Shipment</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Assign  Shipments to a Captain</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <select name="city" id="city" class="form-control">
                                            @foreach($adminCities as $adcity)
                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Captain ID<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input value="" type="text" name="captain_id" id="captain_id" onchange="checkcaptainname(this.value)"; onkeypress="return isNumber(event)">

                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4 id="name_here"></h4>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment ID <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <input id="shipmentID1" type="text"  onchange="scanform(this.value,this.id)" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input id="shipmentID2" type="text"  onchange="scanform(this.value,this.id)" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input id="shipmentID3" type="text"  onchange="scanform(this.value,this.id)" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input id="shipmentID4" type="text"  onchange="scanform(this.value,this.id)" disabled>
                                        <h4 style="float: right;"><span id="shipment_scanned">0</span>/<span id="scanned_status">0</span></h4>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">

                                    <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
                                        <p>
                                            <button id="resetform" class="btn btn-success"
                                            <?php
                                                    $admin_role = Session::get('admin_role');
                                                    if ($admin_role == 9 || $admin_role == 12) {
                                                        echo 'style="visibility: hidden;" disabled';
                                                    }
                                                    ?>
                                                    >Reset
                                            </button>
                                        </p>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="x_title">
                        <h2>Planned Shipments to a Captain</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">

                            <table id="shipments_planned" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Receiver Name</th>
                                    <th>Planned Captain</th>
                                    <th>Confirmed Captain</th>
                                    <th>Age</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Receiver Name</th>
                                    <th>Planned Captain</th>
                                    <th>Confirmed Captain</th>
                                    <th>Age</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>

                    <div class="x_title">
                        <h2>Shipments not Planned to Captain,<small> Something went wrong</small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">

                            <table id="unplan_shipments" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    <th>Receiver Name</th>
                                    <th>Age</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </thead>


                                <tfoot>
                                <tr>
                                    <th>Waybill</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    <th>Receiver Name</th>
                                    <th>Age</th>
                                    <th>Quantity</th>
                                    <th>City</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- /page content -->
    <div id="modal" class="modal">
        <!-- Modal content -->
        <div id="modal-content" class="modal-content">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <input name="waybill_id" id="waybill_id" type="hidden"/>

                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label>Way Bill :</label>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label id="waybilllabel"></label>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                </div>
                <label class="control-label col-md-12 col-sm-12 col-xs-12">Pin <span class="required">*</span></label>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input id="pin" name="pin" class="form-control" type="text"/>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p>
                        <button id="markAsDeliveredButton" class="btn btn-success">Mark As Delivered</button>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <!-- jQuery k-w-h.com/app/views/warehouse/vendors -->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>


        $(document).ready(function () {


            ///////////////////////////
            $("#shipments_planned").DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });
            ///////////////////////////
            $("#unplan_shipments").DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

            /////// Scan Code ///////

//Reset Form
            $("#resetform").click(function () {
                var captain_id = document.getElementById("captain_id");
                captain_id.value = '';
                captain_id.focus();
                scanned_status = document.getElementById("scanned_status").innerHTML=0;
                shipment_scanned = document.getElementById("shipment_scanned").innerHTML= 0;
                document.getElementById("name_here").innerHTML='';
                var shipments_planned = $('#shipments_planned').DataTable();
                shipments_planned.clear().draw();
                var unplan_shipments = $('#unplan_shipments').DataTable();
                unplan_shipments.clear().draw();
                $("input#shipmentID1").attr("disabled", "disabled");
                $("input#shipmentID2").attr("disabled", "disabled");
                $("input#shipmentID3").attr("disabled", "disabled");
                $("input#shipmentID4").attr("disabled", "disabled");
                $("input#shipmentID1").val("");
                $("input#shipmentID2").val("");
                $("input#shipmentID3").val("");
                $("input#shipmentID4").val("");


            });

        });

    </script>
    <script>

        function scanform(val,textid) {
            var shipment_id = val.replace(/\s+/g, '').trim();
            var scanned_status = document.getElementById("scanned_status");
            var shipment_scanned = document.getElementById("shipment_scanned");
            var city = document.getElementById("city").value;
            var captain_id = document.getElementById("captain_id").value;

            if (shipment_id == '') {
                bootbox.alert('Please enter ID!');
                return;
            }
            //focus on next textbox
            if (textid == 'shipmentID1') {
                document.getElementById("shipmentID2").focus()
            } else if (textid == 'shipmentID2') {
                document.getElementById("shipmentID3").focus()
            } else if (textid == 'shipmentID3') {
                document.getElementById("shipmentID4").focus()
            }else if (textid == 'shipmentID4') {
                document.getElementById("shipmentID1").focus()
            }

//scan shipment when shipment not planned to anyone or another captain

            $.ajax({
                type: 'POST',
                url: '/warehouse/scanshipmentscaptain',
                dataType: 'text',
                data: {captainid: captain_id, city: city, id: shipment_id},
            }).done(function (responsef) {
                responseObjf = JSON.parse(responsef);
                if (responseObjf.count) {
                    //empty text box
                    if (textid == 'shipmentID1') {
                        $("input#shipmentID1").val("");
                    } else if (textid == 'shipmentID2') {
                        $("input#shipmentID2").val("");
                    } else if (textid == 'shipmentID3') {
                        $("input#shipmentID3").val("");
                    } else if (textid == 'shipmentID4') {
                        $("input#shipmentID4").val("");
                    }

                    var plannedItems = responseObjf.scannedItems;
                    var mydata_plan = []
                    var shipments_planned = $('#shipments_planned').DataTable();
                    $.each(plannedItems, function (i, item) {
                        shipments_planned.row.add([
                                    item.jollychic,
                                    item.order_number,
                                    item.receiver_name,
                                    item.plan_name + (item.plan_phone).replace("+966", " 0"),
                                    item.confirm_name + (item.confirm_phone).replace("+966", " 0"),
                                    item.age,
                                    item.d_quantity,
                                    item.d_city]
                        ).draw();
                    });
                    shipment_scanned.innerHTML = 1;
                    scanned_status.innerHTML = +scanned_status.innerHTML + responseObjf.count;
                } else if (responseObjf.mission_id) {
                    var shipmentJollychic = responseObjf.jollychic;
                    if (confirm("Shipment "+shipmentJollychic+" is under mission id " + responseObjf.mission_id + ", do you want to scan it separately with captain: " + captain_id) == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/scanshipmentscaptain',
                            dataType: 'text',
                            data: {captainid: captain_id, city: city, id: shipmentJollychic, updateMissionShipment: 'yes'},
                        }).done(function (responsef) {
                            responseObjf = JSON.parse(responsef);
                            if (responseObjf.error) {
                                bootbox.alert(responseObjf.error);
                            } else if (responseObjf.count) {
                                //empty text box
                                if (textid == 'shipmentID1') {
                                    $("input#shipmentID1").val("");
                                } else if (textid == 'shipmentID2') {
                                    $("input#shipmentID2").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID3").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID4").val("");
                                }
                                var plannedItems = responseObjf.scannedItems;
                                var mydata_plan = []
                                var shipments_planned = $('#shipments_planned').DataTable();
                                $.each(plannedItems, function (i, item) {

                                    shipments_planned.row.add([
                                                item.jollychic,
                                                item.order_number,
                                                item.receiver_name,
                                                item.plan_name + (item.plan_phone).replace("+966", " 0"),
                                                item.confirm_name + (item.confirm_phone).replace("+966", " 0"),
                                                item.age,
                                                item.d_quantity,
                                                item.d_city]
                                    ).draw();
                                });
                                scanned_status.innerHTML = +scanned_status.innerHTML + responseObjf.count;

                            } else {
                                shipment_scanned.innerHTML = 0;
                                var plannedItems = responseObjf.scannedItems;
                                var unplan_shipments = $('#unplan_shipments').DataTable();
                                $.each(plannedItems, function (i, item) {

                                    unplan_shipments.row.add([
                                                item.jollychic,
                                                item.order_number,
                                                item.status_english,
                                                item.receiver_name,
                                                item.age,
                                                item.d_quantity,
                                                item.d_city]
                                    ).draw();
                                });
                                //empty text box
                                if (textid == 'shipmentID1') {
                                    $("input#shipmentID1").val("");
                                } else if (textid == 'shipmentID2') {
                                    $("input#shipmentID2").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID3").val("");
                                } else if (textid == 'shipmentID4') {
                                    $("input#shipmentID4").val("");
                                }
                            }
                        })
                    } else {
                        bootbox.alert('You cancled!');
                    }

                } else if (responseObjf.first_planned) {
                    var first = responseObjf.first_planned
                    var first_planned_captain = first.planned_walker;
                    if (confirm("This shipment already assign to captain id " + first_planned_captain + " Are you sure you want to update status for shipment = " +
                                    "" + shipment_id + "  to captain: " + captain_id) == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/warehouse/scanshipmentscaptain',
                            dataType: 'text',
                            data: {captainid: captain_id, city: city, id: shipment_id, first_walker: 'yes'},
                        }).done(function (responsef) {
                            responseObjf = JSON.parse(responsef);
                            if (responseObjf.error) {
                                bootbox.alert(responseObjf.error);
                            } else if (responseObjf.mission_shipment) {
                                var missionShipment = responseObjf.mission_shipment;
                                bootbox.alert("Sorry the shipment " + missionShipment.jollychic + " is under planned or scanned mission: " + missionShipment.mission_waybill +
                                        " you can not scan for any other captain");
                                if (textid == 'shipmentID1') {
                                    $("input#shipmentID1").val("");
                                } else if (textid == 'shipmentID2') {
                                    $("input#shipmentID2").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID3").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID4").val("");
                                }
                                shipment_scanned.innerHTML = 0;
                            }
                            else if (responseObjf.count) {
                                //empty text box
                                if (textid == 'shipmentID1') {
                                    $("input#shipmentID1").val("");
                                } else if (textid == 'shipmentID2') {
                                    $("input#shipmentID2").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID3").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID4").val("");
                                }
                                var plannedItems = responseObjf.scannedItems;
                                var mydata_plan = []
                                var shipments_planned = $('#shipments_planned').DataTable();
                                $.each(plannedItems, function (i, item) {

                                    shipments_planned.row.add([
                                                item.jollychic,
                                                item.order_number,
                                                item.receiver_name,
                                                item.plan_name + (item.plan_phone).replace("+966", " 0"),
                                                item.confirm_name + (item.confirm_phone).replace("+966", " 0"),
                                                item.age,
                                                item.d_quantity,
                                                item.d_city]
                                    ).draw();
                                });
                                scanned_status.innerHTML = +scanned_status.innerHTML + responseObjf.count;

                            } else {
                                shipment_scanned.innerHTML = 0;
                                //bootbox.alert('Status not update for shipment ' + shipment_id + ' ,Something went wrong!');
                                var plannedItems = responseObjf.scannedItems;
                                var unplan_shipments = $('#unplan_shipments').DataTable();
                                $.each(plannedItems, function (i, item) {

                                    unplan_shipments.row.add([
                                                item.jollychic,
                                                item.order_number,
                                                item.status_english,
                                                item.receiver_name,
                                                item.age,
                                                item.d_quantity,
                                                item.d_city]
                                    ).draw();
                                });
                                //empty text box
                                if (textid == 'shipmentID1') {
                                    $("input#shipmentID1").val("");
                                } else if (textid == 'shipmentID2') {
                                    $("input#shipmentID2").val("");
                                } else if (textid == 'shipmentID3') {
                                    $("input#shipmentID3").val("");
                                } else if (textid == 'shipmentID4') {
                                    $("input#shipmentID4").val("");
                                }
                            }
                        });
                    }
                    else {
                        bootbox.alert('You cancled!');
                    }
                    //if mission shipments already planned or scanned
                } else if (responseObjf.mission_shipment) {
                    bootbox.alert("Sorry the shipment "+ shipment_id +" is under planned or scanned mission ( "+ responseObjf.mission_id +" ) ");
                    if (textid == 'shipmentID1') {
                        $("input#shipmentID1").val("");
                    } else if (textid == 'shipmentID2') {
                        $("input#shipmentID2").val("");
                    } else if (textid == 'shipmentID3') {
                        $("input#shipmentID3").val("");
                    } else if (textid == 'shipmentID3') {
                        $("input#shipmentID4").val("");
                    }
                    shipment_scanned.innerHTML = 0;
                }
                else {
                    shipment_scanned.innerHTML = 0;
                    //bootbox.alert('Status not update for shipment ' + shipment_id + ' ,Something went wrong!');
                    var plannedItems = responseObjf.scannedItems;
                    var unplan_shipments = $('#unplan_shipments').DataTable();
                    $.each(plannedItems, function (i, item) {

                        unplan_shipments.row.add([
                                    item.jollychic,
                                    item.order_number,
                                    item.status_english,
                                    item.receiver_name,
                                    item.age,
                                    item.d_quantity,
                                    item.d_city]
                        ).draw();
                    });
                    //empty text box
                    if (textid == 'shipmentID1') {
                        $("input#shipmentID1").val("");
                    } else if (textid == 'shipmentID2') {
                        $("input#shipmentID2").val("");
                    } else if (textid == 'shipmentID3') {
                        $("input#shipmentID3").val("");
                    }else if (textid == 'shipmentID4') {
                        $("input#shipmentID4").val("");
                    }
                    if(responseObjf.waybill || responseObjf.error)
                        alert(responseObjf.error);
                }


            });
        }





        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function checkcaptainname(captain_id) {
            var Captain_name = document.getElementById("name_here");
            var city = document.getElementById("city").value;
            $.ajax({
                type: 'POST',
                url: '/warehouse/captaininfo',
                dataType: 'text',
                data: {captain_id: captain_id, city: city},
            }).done(function (response) {
                console.log(response);
                responseObj = JSON.parse(response);
                if (response == 0) {
                    Captain_name.innerHTML = 'No captain is registred with entered ID';
                    $("input#shipmentID1").attr("disabled", "disabled");
                    $("input#shipmentID2").attr("disabled", "disabled");
                    $("input#shipmentID3").attr("disabled", "disabled");
                    $("input#shipmentID4").attr("disabled", "disabled");
                    return;
                } else if (responseObj.error) {
                    bootbox.alert(responseObj.error);
                    $("input#shipmentID1").attr("disabled", "disabled");
                    $("input#shipmentID2").attr("disabled", "disabled");
                    $("input#shipmentID3").attr("disabled", "disabled");
                    $("input#shipmentID4").attr("disabled", "disabled");
                }
                else {
                    Captain_name.innerHTML = responseObj.first_name + " " + responseObj.last_name;
                    $("input#shipmentID1").prop("disabled", false);
                    $("input#shipmentID2").prop("disabled", false);
                    $("input#shipmentID3").prop("disabled", false);
                    $("input#shipmentID4").prop("disabled", false);
                    document.getElementById("shipmentID1").focus()
//captain scane shipments
                    $.ajax({
                        type: 'POST',
                        url: '/warehouse/getcaptainscanshipment',
                        dataType: 'text',
                        data: {captain_id: captain_id},
                    }).done(function (response) {
                        console.log(response);
                        if (response == 0) {
                            var scanned_status = document.getElementById("scanned_status");
                            scanned_status.innerHTML = 0
                            var shipments_planned = $('#shipments_planned').DataTable();
                            shipments_planned.clear().draw();
                        }
                        else {
                            responseObj = JSON.parse(response);

                            var plannedItems = responseObj.scannedItems;
                            var scanned_status = document.getElementById("scanned_status");
                            var shipments_planned = $('#shipments_planned').DataTable();
                            shipments_planned.clear().draw();
                            $.each(plannedItems, function (i, item) {

                                shipments_planned.row.add([
                                            item.jollychic,
                                            item.order_number,
                                            item.receiver_name,
                                            item.plan_name + (item.plan_phone).replace("+966", " 0"),
                                            item.confirm_name + (item.confirm_phone).replace("+966", " 0"),
                                            item.age,
                                            item.d_quantity,
                                            item.d_city]
                                ).draw();
                            });
                            scanned_status.innerHTML = responseObj.count;
                        }
                    })
                }

            })

        }
    </script>
    @stop

