@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>

@endif


<?php

if (Session::get('admin_role') == 9)
    $layout = 'warehouse.cslayout';
else if (Session::get('admin_role') == 12)
    $layout = 'warehouse.inroutelayout';
else
    $layout = 'warehouse.layout';
?>

@extends($layout)


@section('content')


    <!-- page content -->

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Delivery Duration Analysis</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <!-- /top tiles -->

            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <div class="row">

                <div class="tab_container">


                    <input id="tab2" type="radio" name="tabs" checked>
                    <label for="tab2" class='label2'><i
                                class="fa fa-pencil-square-o"></i><span> Delivery Duration Analysis</span></label>


                    <section id="content2" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">

                                            <form method="get" action="/warehouse/durationAnalysis">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value="3">

                                                <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Company
                                                        <span
                                                                class="required">*</span>
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select id="company" name="company" required="required"
                                                                class="form-control" type="text">
                                                            <option value="all">All Companies</option>
                                                            @foreach($companies as $compan)
                                                                <option value="{{$compan->id}}" <?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                                                class="required">*</span>
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <select name="city" id="city" class="form-control">
                                                            @foreach($adminCities as $adcity)
                                                                <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                               

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">From
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="start_date" name="start_date" value="<?php if(empty($_GET['start_date'])){ echo '2017-01-01'; }else{ echo $_GET['start_date'];} ?>" 
                                       class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                                                    </div>
                                                </div>


                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">To
                                                    </label>

                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="end_date" name="end_date" value="<?php if(empty($_GET['end_date'])) { echo date('Y-m-d'); } else { echo $_GET['end_date']; } ?>" 
                                       class="date-picker form-control col-md-7 col-xs-12" " 
                                       type="date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p>
                                                            <button id="submitForm" class="btn btn-success">Filter
                                                            </button>
                                                        </p>
                                                    </div>
                                                </div>


                                            </
                                            >
                                            <table id="datatable_filtered" class="table table-striped table-bordered ">
                                                <thead>
                                                <tr>
                                                    <th>In Route Duration (days)</th>
                                                    <th>In Warehouse Duration (days)</th>
                                                    <th>Delivery Duration (days)</th>
                                                    <th>Total Duration (days)</th>
                                                
                                                </tr>
                                                </thead>


                                                <tfoot>
                                                <tr>
                                                    <th>In Route Duration (days)</th>
                                                    <th>In Warehouse Duration (days)</th>
                                                    <th>Delivery Duration (days)</th>
                                                    <th>Total Duration (days)</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <!-- table start -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Details</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Details
                            </p>
                        
                            <table id="datatable_details" class="table table-striped table-bordered ">
                                <thead>
                                <tr>
                                    <th>WayBill</th>
                                    <th>Create Date</th>
                                    <th>In Route Date</th>
                                    <th>In Route Duration</th>
                                    <th>Schedule Date</th>
                                    <th>In Warehouse Duration</th>
                                    <th>Delivery Date</th>
                                    <th>Delivery Duration</th>
                                    <th>Total Duration</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                   <th>WayBill</th>
                                    <th>Create Date</th>
                                    <th>In Route Date</th>
                                    <th>In Route Duration</th>
                                    <th>Schedule Date</th>
                                    <th>In Warehouse Duration</th>
                                    <th>Delivery Date</th>
                                    <th>Delivery Duration</th>
                                    <th>Total Duration</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- !table start -->
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <script>

        <?php


        $filter_data = '';

        $details_data = '';
        
         foreach($inRoute as $key => $value): $inwh = $inWarehouse[0];
$dd = $deliveryDuration[0];
 $td = $totalDuration[0];           
    $filter_data .= "[ \"Min: $value->minInRoute\",\"Min: $inwh->minInWarehouse\", \"Min: $dd->minDeliveryDuration\", \"Min: $td->minTotalDuration\" ], ";
    $filter_data .= "[ \"Max: $value->maxInRoute\", \"Max: $inwh->maxInWarehouse\", \"Max: $dd->maxDeliveryDuration\", \"Max: $td->maxTotalDuration\" ], ";
    $filter_data .= "[ \"Avg: $value->averageInRoute\", \"Avg: $inwh->averageInWarehouse\", \"Avg: $dd->averageDeliveryDuration\", \"Avg: $td->averageTotalDuration\" ], ";
   
endforeach;


foreach($deliveryDetails as $detail){

 $details_data .= "[ \"$detail->waybill\",\"$detail->createDate\", \"$detail->inRouteDate\", \"$detail->inRouteDuration\" , \"$detail->scheduledDate\", \"$detail->inWarehouseDuration\", \"$detail->deliveryDate\", \"$detail->deliveryDuration\", \"$detail->totalDuration\" ], ";

}
            
        
    
        ?>

        $(document).ready(function () {
            


            $("#datatable_filtered").DataTable({

                "data": [
                    <?php echo $filter_data ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, '']]
            });

            $("#datatable_details").DataTable({

                "data": [
                    <?php echo $details_data ?>

                ],
                "autoWidth": false,

                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,

                'order': [[0, '']]
            });

        });

    
       
    </script>



@stop
