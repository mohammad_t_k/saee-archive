@if( Auth::user()->role < 4)

    <script>window.location = "/warehouse/403";</script>
@endif

@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
    
    <style>
        table th {
            text-align: center;
        } 
        table td {
            text-align: center;
        } 
        .important {
            background-color: #26B99A; 
            color: white;
        }
        .bottom-border {
            border-bottom: 2px solid; 
        }
    </style>

    <div class="right_col" role="main">

        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="get" action="/warehouse/controlpanel/operation">
                        <input type="hidden" name="tabFilter" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="company" name="company" required="required" class="form-control" type="text">
                                        <option value="all">All Companies</option>
                                        @foreach($adminCompanies as $compan)
                                            <option value="{{$compan->id}}"<?php if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="city" id="city" class="form-control">
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ isset($from) ? $from : '' }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ isset($to) ? $to : '' }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="tab-content">
                <div id="dataview" class="tab-pane fade in active">
                    <div class="x_panel tile fixed_height_580 overflow_hidden row padding-0">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Max in Days</th>
                                    <th>Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="export-shipment" id="row_cbs">
                                    <td>1</td>
                                    <td>Created By Supplier</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="created_by_supplier"><a>{{ $created_by_supplier }}</a></td>
                                </tr>
                                <tr class="export-shipment" id="row_dcbs">
                                    <td>2</td>
                                    <td>Delayed Created By Supplier</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="delayed_created_by_supplier">{{ $delayed_created_by_supplier }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_ir">
                                    <td>3</td>
                                    <td>Delayed In route</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="in_route">{{ $in_route }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_iw">
                                    <td>4</td>
                                    <td>Delayed In warehouse</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="in_warehouse">{{ $in_warehouse }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_da">
                                    <td>5</td>
                                    <td>Delayed Delivery Attempt</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="delivery_attempt">{{ $delivery_attempt }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_eda">
                                    <td>6</td>
                                    <td>Exceeded Maximum Delivery Attempts</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="exceeded_delivery_attempt">{{ $exceeded_delivery_attempt }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_crm">
                                    <td>7</td>
                                    <td>Delayed Money With Captains</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="cod_money_reception">{{ $cod_money_reception }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_rmib">
                                    <td>8</td>
                                    <td>Delayed Reception of Money Not Locally Transfered</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="reception_money_in_branches">{{ $reception_money_in_branches }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_rmfb">
                                    <td>8</td>
                                    <td>Delayed Reception of Money Locally Transfered</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="reception_money_from_branches">{{ $reception_money_from_branches }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_cps">
                                    <td>9</td>
                                    <td>Delayed Pay to Supplier COD</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="cod_paid_to_supplier">{{ $cod_paid_to_supplier }}</td>
                                </tr>
                                <tr class="export-shipment" id="row_rts">
                                    <td>10</td>
                                    <td>Delayed Returned to Supplier</td>
                                    <td>{{ $company != 'all' && $city != 'All' && count($intervals) > 0 ? $intervals[0]->created_by_supplier : '' }}</td>
                                    <td id="returned_to_supplier">{{ $returned_to_supplier }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <style>
        .export-shipment:hover { 
            cursor: pointer;
        } 
    </style>

    <script>
        $(document).ready(function(){
            <?php
            $statuses_rts = '';
            $query = OrderStatuses::whereNotIn('id', array(5,7,8))->get();
            $flag = false;
            foreach($query as $st) {
                if($flag)
                    $statuses_rts .= "&";
                $flag = true;
                $statuses_rts .= "status%5B%5D=$st->id";
            }
            ?>
        });
        $("#row_cbs").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=created_by_supplier&company={{ $company }}&city={{ $city }}&status%5B%5D=0&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_dcbs").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=delayed_created_by_supplier&company={{ $company }}&city={{ $city }}&status%5B%5D=0&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_ir").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=in_route&company={{ $company }}&city={{ $city }}&status%5B%5D=1&shipment_date=all&new_shipment_date=all&last_update=&date_type=new_shipment_date&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_iw").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=in_warehouse&company={{ $company }}&city={{ $city }}&status%5B%5D=2&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_da").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=delivery_attempts&company={{ $company }}&city={{ $city }}&status%5B%5D=2&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_eda").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=exceed_delivery_attempts&company={{ $company }}&city={{ $city }}&status%5B%5D=2&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_crm").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=cod_reception_money&company={{ $company }}&city={{ $city }}&status%5B%5D=2&status%5B%5D=3&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_rmib").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=reception_money_in_branches&company={{ $company }}&city={{ $city }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_rmfb").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=reception_money_from_branches&company={{ $company }}&city={{ $city }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_cps").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=cod_paid_to_supplier&company={{ $company }}&city={{ $city }}&status%5B%5D=5&shipment_date=all&new_shipment_date=all&last_update=&date_type=&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
        $("#row_rts").click(function(){
            window.open('/warehouse/shipmentsExport?tabFilter=3&controlpanel=returned_to_supplier&company={{ $company }}&city={{ $city }}&{{ $statuses_rts }}&shipment_date=all&new_shipment_date=all&last_update=&date_type=created_at&from={{ $from }}&to={{ $to }}&datatable_filtered_length=10', '_blank');
        });
    </script>

    <style>
        @-webkit-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @-moz-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @-o-keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        @keyframes invalid {
            from { background-color: yellow; }
            to { background-color: inherit; }
        }
        .invalid {
            -webkit-animation: invalid 5s; /* infinite Safari 4+ */
            -moz-animation:    invalid 5s; /* infinite Fx 5+ */
            -o-animation:      invalid 5s; /* infinite Opera 12+ */
            animation:         invalid 5s; /* infinite IE 10+ */
        }

        td {
            padding: 1em;
        }
    </style>

    <script>

        var company = document.getElementById('company').value;   
        var city = document.getElementById('city').value;   
        var from = document.getElementById('from').value;
        var to = document.getElementById('to').value;
        var timer = 5100;

        setInterval(function() {
            $.ajax({
                type: 'GET',
                url: '/warehouse/controlpanel/operation/inquiry',
                dataType: 'text',
                data: {company: company, city: city, from: from, to: to}
            }).done(function(response){
                console.log(response);
                responseObj = JSON.parse(response);
                if(responseObj.success == true) {
                    var created_by_supplier = responseObj.created_by_supplier;
                    var cur = document.getElementById('created_by_supplier').innerText;
                    if(cur != created_by_supplier) {
                        $("#created_by_supplier").html(created_by_supplier);
                        $("#row_cbs").addClass("invalid");
                        setTimeout(function() {
                            $("#row_cbs").removeClass("invalid");
                        }, timer);
                    }
                    var delayed_created_by_supplier = responseObj.delayed_created_by_supplier;
                    cur = document.getElementById('delayed_created_by_supplier').innerText;
                    if(cur != delayed_created_by_supplier) {
                        $("#delayed_created_by_supplier").html(delayed_created_by_supplier);
                        $("#row_dcbs").addClass("invalid");
                        setTimeout(function() {
                            $("#row_dcbs").removeClass("invalid");
                        }, timer);
                    }
                    var in_route = responseObj.in_route;
                    cur = document.getElementById('in_route').innerText;
                    if(cur != in_route) {
                        $("#in_route").html(in_route);
                        $("#row_ir").addClass("invalid");
                        setTimeout(function() {
                            $("#row_ir").removeClass("invalid");
                        }, timer);
                    }
                    var in_warehouse = responseObj.in_warehouse;
                    cur = document.getElementById('in_warehouse').innerText;
                    if(cur != in_warehouse) {
                        $("#in_warehouse").html(in_warehouse);
                        $("#row_iw").addClass("invalid");
                        setTimeout(function() {
                            $("#row_iw").removeClass("invalid");
                        }, timer);
                    }
                    var delivery_attempt = responseObj.delivery_attempt;
                    cur = document.getElementById('delivery_attempt').innerText;
                    if(cur != delivery_attempt) {
                        $("#delivery_attempt").html(delivery_attempt);
                        $("#row_da").addClass("invalid");
                        setTimeout(function() {
                            $("#row_da").removeClass("invalid");
                        }, timer);
                    }
                    var exceeded_delivery_attempt = responseObj.exceeded_delivery_attempt;
                    cur = document.getElementById('exceeded_delivery_attempt').innerText;
                    if(cur != exceeded_delivery_attempt) {
                        $("#exceeded_delivery_attempt").html(exceeded_delivery_attempt);
                        $("#row_eda").addClass("invalid");
                        setTimeout(function() {
                            $("#row_eda").removeClass("invalid");
                        }, timer);
                    }
                    var cod_money_reception = responseObj.cod_money_reception;
                    cur = document.getElementById('cod_money_reception').innerText;
                    if(cur != cod_money_reception) {
                        $("#cod_money_reception").html(cod_money_reception);
                        $("#row_crm").addClass("invalid");
                        setTimeout(function() {
                            $("#row_crm").removeClass("invalid");
                        }, timer);
                    }
                    var reception_money_in_branches = responseObj.reception_money_in_branches;
                    cur = document.getElementById('reception_money_in_branches').innerText;
                    if(cur != reception_money_in_branches) {
                        $("#reception_money_in_branches").html(reception_money_in_branches);
                        $("#row_rmib").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rmib").removeClass("invalid");
                        }, timer);
                    }
                    var reception_money_from_branches = responseObj.reception_money_from_branches;
                    cur = document.getElementById('reception_money_from_branches').innerText;
                    if(cur != reception_money_from_branches) {
                        $("#reception_money_from_branches").html(reception_money_from_branches);
                        $("#row_rmfb").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rmfb").removeClass("invalid");
                        }, timer);
                    }
                    var cod_paid_to_supplier = responseObj.cod_paid_to_supplier;
                    cur = document.getElementById('cod_paid_to_supplier').innerText;
                    if(cur != cod_paid_to_supplier) {
                        $("#cod_paid_to_supplier").html(cod_paid_to_supplier);
                        $("#row_cps").addClass("invalid");
                        setTimeout(function() {
                            $("#row_cps").removeClass("invalid");
                        }, timer);
                    }
                    var returned_to_supplier = responseObj.returned_to_supplier;
                    cur = document.getElementById('returned_to_supplier').innerText;
                    if(cur != returned_to_supplier) {
                        $("#returned_to_supplier").html(returned_to_supplier);
                        $("#row_rts").addClass("invalid");
                        setTimeout(function() {
                            $("#row_rts").removeClass("invalid");
                        }, timer);
                    }
                }
            });
        }, 60000);

    </script>
    
@stop
