@if( Auth::user()->role < 4)

  <script>window.location = "/warehouse/403";</script>
  
@endif
@extends(Session::get('admin_role') == 9 ? 'warehouse.cslayout' : 'warehouse.layout')


@section('content')
<style>
#map_canvas {
  height: 300px;
  width: 100%;
  margin: 0;
}

#map_canvas .centerMarker {
  position: absolute;
  /*url of the marker*/
  background: url(../web/images/marker.png) no-repeat;
  /*center the marker*/
  top: 50%;
  left: 50%;
  z-index: 1;
  /*fix offset when needed*/
  margin-left: -10px;
  margin-top: -34px;
  /*size of the image*/
  height: 34px;
  width: 20px;
  cursor: pointer;
}
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
    <div class="page-title">
        <div class="title_left">
        <h3>Company Detail</h3>
        </div>
        
    </div>
        
            
        <div class="row">
           
          <!-- table start -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Companies information</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    Companies data
                    </p>
                    <!--<p> <a class="alert1" ><button>Set District</button></a></p> -->
                  
                    
                    <table id="datatable_captains" class="table table-striped table-bordered ">
                        <tbody>
                            <tr>
                                <td>Company Name</td>
                                <td><?= $company['company_name'] ?></td>
                                <td>Username</td>
                                <td><?= $company['username'] ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?= $company['email'] ?></td>
                                <td>Phone</td>
                                <td><?= $company['phone'] ?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?= $company['address'] ?></td>
                                <td>City</td>
                                <td><?= $company['city'] ?></td>
                            </tr>
                            <tr>
                                <td>Transportation Fee</td>
                                <td><?= $company['transportation_fee'] ?></td>
                                <td>Max Regular Weight</td>
                                <td><?= $company['max_regular_weight'] ?></td>
                            </tr>
                            <tr>
                                <td>Pick Up Fee</td>
                                <td><?= $company['pickup_fee']  ?></td>
                                <td>Delivery Fee</td>
                                <td><?php echo $company['delivery_fee']  ?></td>
                            </tr>
                            <tr>
                                <td>Delivery Fee 2</td>
                                <td><?= $company['delivery_fee2']  ?></td>
                                <td>Delivery Fee 3</td>
                                <td><?php echo $company['delivery_fee3']  ?></td>
                            </tr>
                            <tr>
                                <td>Excess Weight Fee</td>
                                <td><?= $company['excess_weight_fees'] ?></td>
                                <td>Secret API Key</td>
                                <td><?= $company['secret'] ?></td>
                            </tr>
                            <tr>
                                <td colspan='6'>
                                    <div id = 'map_canvas'></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>   
            <!-- !table start -->
            
        <?php //echo $captainsdata ?>



        </div>
    </div>
</div>
<!-- /page content -->
                    

<!-- jstables script 

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
<script>
function initMap() {
    var company = JSON.parse('<?php echo $company?>');
    var  latitude=21.2854;
    var  longitude=39.2376;
    if(company.latitude!=0&&company.longitude!=0){
        latitude=company.latitude;
        longitude=company.longitude;
    }
    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(latitude, longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
    var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map,
        title: 'Company Location'
    });
}
google.maps.event.addDomListener(window, 'load', initMap);</script>
@stop
