<?php
$admin_id = Session::get('admin_id');
$admin_role = Session::get('admin_role');
$layout = 'warehouse.layout';
if($admin_role == '9')
    $layout = 'warehouse.cslayout';
if($admin_role == '14')
    $layout = 'warehouse.portahub_layout';
?>
@extends($layout)
@section('content')

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Bulk Captain Assignment Shipments</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Assign a Group of Shipments to a Captain</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <div class="row">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            Main City <span class="required">*</span>
                                        </label>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <?php $admin = Admin::where('id', '=', $admin_id)->first(); ?>
                                            <select name="city" id="city" class="form-control" disabled>
                                                @foreach($adminCities as $adcity)
                                                    <option value="{{$adcity->city}}"<?php if ($admin->default_city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span
                                                    class="required">*</span>
                                        </label>

                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <textarea id="shipmentIDs" required="required" class="form-control" type="text" rows="20"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <p>
                                                <button id="submitForm" class="btn btn-success">Assign</button>
                                                <button id="unAssignForm" class="btn btn-success">Un Assign</button>
                                                <button id="scanform" class="btn btn-success">Scan</button>
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="x_title">
                            <h2>Failed Items</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <table id="datatable_failed" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Receiver Name</th>
                                        <th>COD</th>
                                        <th>Planned Captain</th>
                                        <th>Confirmed Captain</th>
                                        <th>Error</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Receiver Name</th>
                                        <th>COD</th>
                                        <th>Planned Captain</th>
                                        <th>Confirmed Captain</th>
                                        <th>Error</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                        <div class="x_title">
                            <h2>Success Items</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">

                                <table id="datatable_success" class="table table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Order Number</th>
                                        <th>Order Reference</th>
                                        <th>Main City</th>
                                        <th>Sub City</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Mobile 1</th>
                                        <th>Receiver Mobile 2</th>
                                        <th>Cash On Delivery</th>
                                        <th>Quantity</th>
                                        <th>Planned Captain</th>
                                        <th>Confirmed Captain</th>
                                    </tr>
                                    </thead>


                                    <tfoot>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Order Number</th>
                                        <th>Order Reference</th>
                                        <th>Main City</th>
                                        <th>Sub City</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Mobile 1</th>
                                        <th>Receiver Mobile 2</th>
                                        <th>Cash On Delivery</th>
                                        <th>Quantity</th>
                                        <th>Planned Captain</th>
                                        <th>Confirmed Captain</th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="modal" class="modal">
        <!-- Modal content -->
        <div id="modal-content" class="modal-content">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <input name="waybill_id" id="waybill_id" type="hidden"/>
                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label>Way Bill :</label>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                        <label id="waybilllabel"></label>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                    <div class='col-md-6 col-sm-6 col-xs-6'>
                    </div>
                </div>
                <label class="control-label col-md-12 col-sm-12 col-xs-12">Pin <span class="required">*</span></label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input id="pin" name="pin" class="form-control" type="text"/>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p>
                        <button id="markAsDeliveredButton" class="btn btn-success">Mark As Delivered</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#datatable_failed').DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

            $('#datatable_success').DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });
        })
    </script>

    <script>

        $(document).ready(function () {
            $("#submitForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            /*if (/[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('JC' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }
                            else if (/C[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('J' + ids[i]);
                                correctedIDs.push(ids[i]);
                            } */

                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /*if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }*/


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to assign selcted items to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $('#datatable_success').DataTable().clear().draw();
                                            $('#datatable_failed').DataTable().clear().draw();
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/portahub/setplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (response) {

                                                console.log(response);
                                                responseObj = JSON.parse(response);
                                                console.log(responseObj);
                                                if (!responseObj.success) {
                                                    if(responseObj.error_code == 1) {
                                                        bootbox.alert(responseObj.error);
                                                    }
                                                    else {
                                                        bootbox.alert('Unknown error');
                                                    }
                                                    return;
                                                }

                                                alert(responseObj.success_items.length + ' items have been updated');

                                                if (responseObj.success_items.length) {
                                                    $('#datatable_success td').parent().remove();
                                                    var success_items = responseObj.success_items;
                                                    $.each(success_items, function (i, one_item) {
                                                        $("#datatable_success tbody").append(
                                                            "<tr id='" + one_item.jollychic + "'>"
                                                            + "<td>" + one_item.jollychic + "</td>"
                                                            + "<td>" + one_item.order_number + "</td>"
                                                            + "<td>" + (!one_item.order_reference ? "" : one_item.order_reference) + "</td>"
                                                            + "<td>" + one_item.main_city + "</td>"
                                                            + "<td>" + one_item.d_city + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.receiver_phone + "</td>"
                                                            + "<td>" + one_item.receiver_phone2 + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.d_quantity + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "</tr>")
                                                    });
                                                }


                                                if(responseObj.failed_items.length) {
                                                    $('#datatable_failed td').parent().remove();
                                                    var failed_items = responseObj.failed_items;
                                                    $.each(failed_items, function (i, one_item) {
                                                        $("#datatable_failed tbody").append(
                                                            "<tr id='" + one_item.waybill + "'>"
                                                            + "<td>" + one_item.waybill + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "<td>" + one_item.error + "</td>"
                                                            + "</tr>")
                                                    });
                                                }

                                            });

                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );


            /////// Un Assign Captain Code ////////


            $("#unAssignForm").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            /*if (/[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('JC' + ids[i]);
                                correctedIDs.push(ids[i]);
                            }
                            else if (/C[0-9]{8}KS/.test(ids[i])) {
                                correctIDs.push('J' + ids[i]);
                                correctedIDs.push(ids[i]);
                            } */

                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }

                    /*if (correctedIDs.length) {
                        bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
                    }*/


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to un assign captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $('#datatable_success').DataTable().clear().draw();
                                            $('#datatable_failed').DataTable().clear().draw();
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/portahub/unsetplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (response) {
                                                
                                                console.log(response);
                                                responseObj = JSON.parse(response);
                                                console.log(responseObj);
                                                if (!responseObj.success) {
                                                    if(responseObj.error_code == 1) {
                                                        bootbox.alert(responseObj.error);
                                                    }
                                                    else {
                                                        bootbox.alert('Unknown error');
                                                    }
                                                    return;
                                                }

                                                alert(responseObj.success_items.length + ' items have been updated');

                                                if (responseObj.success_items.length) {
                                                    $('#datatable_success td').parent().remove();
                                                    var success_items = responseObj.success_items;
                                                    $.each(success_items, function (i, one_item) {
                                                        $("#datatable_success tbody").append(
                                                            "<tr id='" + one_item.jollychic + "'>"
                                                            + "<td>" + one_item.jollychic + "</td>"
                                                            + "<td>" + one_item.order_number + "</td>"
                                                            + "<td>" + (!one_item.order_reference ? "" : one_item.order_reference) + "</td>"
                                                            + "<td>" + one_item.main_city + "</td>"
                                                            + "<td>" + one_item.d_city + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.receiver_phone + "</td>"
                                                            + "<td>" + one_item.receiver_phone2 + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.d_quantity + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "</tr>")
                                                    });
                                                }

                                                if(responseObj.failed_items.length) {
                                                    $('#datatable_failed td').parent().remove();
                                                    var failed_items = responseObj.failed_items;
                                                    $.each(failed_items, function (i, one_item) {
                                                        $("#datatable_failed tbody").append(
                                                            "<tr id='" + one_item.waybill + "'>"
                                                            + "<td>" + one_item.waybill + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "<td>" + one_item.error + "</td>"
                                                            + "</tr>")
                                                    });
                                                }

                                            });

                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );
            ///////////////////////////
            $("#datatable_plan").DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ],
                iDisplayLength: 25,

                "data": [],
                "autoWidth": false,
                dom: "lfrtip",

                responsive: true,
                'order': [[0, 'desc']]
            });

            /////// Scan Code ///////

            $("#scanform").click(function () {
                    var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");
                    var city = document.getElementById("city").value;


                    if (shipIDs == '') {
                        bootbox.alert('Please enter IDs!');
                        return;
                    }

                    var ids = shipIDs.split(',');
                    for(i = 0; i < ids.length;++i){
                        if (/jc[0-9]{8}ks/.test(ids[i]) || /os[0-9]{8}ks/.test(ids[i]) )
                            ids[i] = ids[i].toUpperCase();
                    }

                    var wrongIDs = Array();
                    var correctIDs = Array();
                    var correctedIDs = Array();
                    var finalArray = [];
                    for (var i = 0; i < ids.length; i++) {
                        if (!ids[i].replace(/\s/g, '').length) {
                            continue;
                        }
                        if (/JC[0-9]{8}KS/.test(ids[i]) || /OS[0-9]{8}KS/.test(ids[i]) || /^[a-z0-9]+$/i.test(ids[i])) {
                            correctIDs.push(ids[i]);
                        }
                        else {
                            wrongIDs.push(ids[i]);
                        }
                    }

                    $.each(correctIDs, function (i, el) {
                        if ($.inArray(el, finalArray) === -1) {
                            finalArray.push(el);
                        }
                    });


                    if (wrongIDs.length) {
                        bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
                    }


                    if (finalArray.length) {
                        if (confirm("Total unique items entered is: " + finalArray.length + ", confirm?") == true) {
                            var captainNumber = prompt("Please captain ID:");
                            var responseObj;
                            if (captainNumber != null && captainNumber != "") {
                                $.ajax({
                                    type: 'POST',
                                    url: '/warehouse/getcaptaininfo',
                                    dataType: 'text',
                                    data: {captain_id: captainNumber},
                                }).done(function (response) {
                                    console.log(response);
                                    if (response == 0) {
                                        bootbox.alert('No captain is registred with entered ID');
                                        return;
                                    }
                                    else {
                                        responseObj = JSON.parse(response);

                                        if (confirm("Are you sure you want to update with captain status for selcted items to captain: " + responseObj.first_name + " " + responseObj.last_name + " of city :" + responseObj.city) == true) {
                                            $('#datatable_success').DataTable().clear().draw();
                                            $('#datatable_failed').DataTable().clear().draw();
                                            $.ajax({
                                                type: 'POST',
                                                url: '/warehouse/portahub/scanplannedcaptain',
                                                dataType: 'text',
                                                data: {captainid: responseObj.id, city: city, ids: finalArray.join(",")},
                                            }).done(function (response) {
                                                
                                                console.log(response);
                                                responseObj = JSON.parse(response);
                                                console.log(responseObj);
                                                if (!responseObj.success) {
                                                    if(responseObj.error_code == 1) {
                                                        bootbox.alert(responseObj.error);
                                                    }
                                                    else {
                                                        bootbox.alert('Unknown error');
                                                    }
                                                    return;
                                                }

                                                alert(responseObj.success_items.length + ' items have been updated');

                                                if (responseObj.success_items.length) {
                                                    $('#datatable_success td').parent().remove();
                                                    var success_items = responseObj.success_items;
                                                    $.each(success_items, function (i, one_item) {
                                                        $("#datatable_success tbody").append(
                                                            "<tr id='" + one_item.jollychic + "'>"
                                                            + "<td>" + one_item.jollychic + "</td>"
                                                            + "<td>" + one_item.order_number + "</td>"
                                                            + "<td>" + (!one_item.order_reference ? "" : one_item.order_reference) + "</td>"
                                                            + "<td>" + one_item.main_city + "</td>"
                                                            + "<td>" + one_item.d_city + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.receiver_phone + "</td>"
                                                            + "<td>" + one_item.receiver_phone2 + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.d_quantity + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "</tr>")
                                                    });
                                                }

                                                if(responseObj.failed_items.length) {
                                                    $('#datatable_failed td').parent().remove();
                                                    var failed_items = responseObj.failed_items;
                                                    $.each(failed_items, function (i, one_item) {
                                                        $("#datatable_failed tbody").append(
                                                            "<tr id='" + one_item.waybill + "'>"
                                                            + "<td>" + one_item.waybill + "</td>"
                                                            + "<td>" + one_item.receiver_name + "</td>"
                                                            + "<td>" + one_item.cash_on_delivery + "</td>"
                                                            + "<td>" + one_item.planned_walker + "</td>"
                                                            + "<td>" + one_item.confirmed_walker + "</td>"
                                                            + "<td>" + one_item.error + "</td>"
                                                            + "</tr>")
                                                    });
                                                }
                                                
                                            });
                                        }
                                    }
                                }); // Ajax Call

                            }
                        }
                        else {
                            bootbox.alert('You cancled!');
                        }

                    }
                    else {
                        bootbox.alert('Nothing to return!');
                        location.reload();
                    }

                }
            );

        });

    </script>

@stop