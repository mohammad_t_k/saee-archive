<?php
?>

        <!doctype html>


<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>KasperCab</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.amber-orange.min.css" />
    <link rel="stylesheet" href="/Dev-Server-Resources/new-css/styles.css">







    <link href="https://cdn.jsdelivr.net/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.1.0/css/font-awesome.min.css" />




    <!-- BootstrapValidator CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"/>

    <!-- jQuery and Bootstrap JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap/3.2.0/js/bootstrap.min.js"></script>




    <!-- changes for datapickers-->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>




    <!--
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGtVJygYQSd3qi2IRuQpmZ-KY6yRBm16Q"></script>
    -->


    <!-- BootstrapValidator JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>

    <!-- Animated Loading Icon -->
    <style type="text/css">
        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -webkit-animation: spin2 .7s infinite linear;
        }
        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg);}
            to { -webkit-transform: rotate(360deg);}
        }
        @keyframes spin {
            from { transform: scale(1) rotate(0deg);}
            to { transform: scale(1) rotate(360deg);}
        }
    </style>


    <style>
        #view-source {
            position: fixed;
            display: block;
            right: 0;
            bottom: 0;
            margin-right: 40px;
            margin-bottom: 40px;
            z-index: 900;
        }
        #map {
            width: 100%;
            height: 25%;
        }

        .controls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 270px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }


        #pac-input2 {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 270px;
        }

        #pac-input2:focus {
            border-color: #4d90fe;
        }

        #pac-input3 {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 270px;
        }

        #pac-input3:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        #type-selector {
            color: #fff;
            background-color: #4d90fe;
            padding: 5px 11px 0px 11px;
        }

        #type-selector label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }
        #target {
            width: 345px;
        }
    </style>
</head>
<body>
<input id="pac-input" class="controls" type="text" placeholder="ابحث عن المنزل">
<input id="pac-input2" class="controls" type="text" placeholder="ابحث عن موقع العمل">
<input id="pac-input3" class="controls" type="text" placeholder="ابحث عن موقع توصيل الطفل">


<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <img align="left" src="/Dev-Server-Resources/new-img/klogo.png" style="width:234px;height:70px;">

            <!--  <span class="mdl-layout-title">Apply for Monthly Service:</span>-->
            <div class="mdl-layout-spacer"></div>

            <!--
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
                <li class="mdl-menu__item" href="about.html">About</li>
                <li class="mdl-menu__item" href="contactus.html" >Contact</li>
                <li class="mdl-menu__item" id="legal" href="termsandcondtions.html">Terms and Conditions</li>
            </ul>
             -->
        </div>
    </header>




    <div class="demo-drawer mdl-layout__drawer ">
        <!--<header class="demo-drawer-header">-->

        <!--<img src="images/user.jpg" class="demo-avatar">-->
        <div class="demo-avatar-dropdown">
            <!-- <span> /* */</span>-->
            <div class="mdl-layout-spacer"></div>

            <!--
             <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                 <i class="material-icons" role="presentation">arrow_drop_down</i>
                 <span class="visuallyhidden">Accounts</span>
             </button>
    -->
            <!--
            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
                <a class="mdl-menu__item" href="logout.php"><i class="material-icons">exit_to_app</i>Logout</a>
            </ul>-->
        </div>
        <!--       </header>-->




        <nav class="demo-navigation mdl-navigation">

            <!-- <a class="mdl-navigation__link" href="profile.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>-->



            <a class="mdl-navigation__link" href="https://www.kasper-cab.com/request/aboutus.html"><i class="mdl-color-text--red-400 material-icons" role="presentation">info_outline</i><span style="color: red">About us</span></a>
            <a class="mdl-navigation__link" href="https://www.kasper-cab.com/request/whykasper.html"><i class="mdl-color-text--orange-400 material-icons" role="presentation">info</i><span style="color: orange">Why Kapser</span></a>
            <a class="mdl-navigation__link" href="https://www.kasper-cab.com/request/contactus.html"><i class="mdl-color-text--yellow-400 material-icons" role="presentation">help_outline</i><span style="color: rebeccapurple">Help</span></a>
        </nav>





    </div>


    <main  dir="rtl" lang="ar" class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">


            <div id="contractform" style="height:2800px; width: 1030px" class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">



                <br>
                <br>


                <form id="errormsg">
                    <div align="center">
                        <ul id="errormsg" class="errorMessages"></ul>
                    </div>
                </form>

                <div id="final" align="middle">
                    <div style="width: 380px; height: 290px"
                         class="demo-options mdl-card   mdl-shadow--4dp mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--12-col-desktop">
                        <div class="mdl-card__title mdl-card--expand ">
                            <h3 style="  font-size: 35px" class="mdl-card__title-text mdl-color-text--grey-600">
                            </h3>
                        </div>
                        <div class="mdl-card__supporting-text mdl-color-text--grey-600">

                            <p style="font-size: 30px; color: darkgray">تمت العملية بنجاح ستقوم خدمة العملاء بالتواصل معكم خلال 48 ساعة عمل !!!</p>
                            <img align="center" src="/Dev-Server-Resources/new-img/suc.png">

                            <!-- for later -->
                            <!--
                            <p style="font-size: 25px; font: bolder; color: black;" id="fromlocation"></p>
                            <p style="font-size: 25px; font: bolder; color: black;" id="tolocation"></p>
                             <p style="font-size: 25px; font: bolder; color: black;" id="sharedtripinfo_price"></p> -->
                        </div>
                        <!--
                        <div align="center">
                            <div class="mdl-card__actions mdl-card--border">
                                <button   id="confirm"
                                          class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  mdl-button--accent  " onclick="populateDB( )">
                                    <b style="font-size: larger ">قدم الطلب</b>
                                </button>

                                <div id="editform"  >
                                    <button  style="padding-right: 20px" id="editform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="editForm()">
                                        <b style="font-size: larger ">تعديل المعلومات</b>                                    </button>
                                </div>

                            </div>
                        </div>
                         -->
                    </div>
                </div>
                <div id="prices" align="middle">


                    <!--
                    <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                    -->
                    <!--
                    <div class="mdl-spinner mdl-js-spinner is-active"></div>
                    -->

                    <div style="width: 280px;height: 250px "
                         class="demo-options mdl-card   mdl-shadow--3dp mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--12-col-desktop">
                        <div class="mdl-card__title mdl-card--expand  ">
                            <h3 style=" font-size: 35px" class="mdl-card__title-text mdl-color-text--grey-600">
                                سيارة خاصة</h3>
                        </div>
                        <div id="privatetripinfo" class="mdl-card__supporting-text mdl-color-text--grey-600">


                            <p style="font-size: 25px; font: bolder; color: black;" id="privatetripinfo_kms"></p>
                            <p style="font-size: 25px; font: bolder; color: black;" id="privatetripinfo_mins"></p>
                            <p style="font-size: 25px; font: bolder; color: black;" id="privatetripinfo_price"></p>


                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <button
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " onclick="populateDB(1,Costs[0])">
                                قدم طلب
                            </button>


                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                            <div id="editform" align="right">
                                <button id="editform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="editForm()">
                                    تعديل الطلب
                                </button>
                            </div>


                        </div>
                    </div>

                    <div id="sharedcard" style="width: 280px; height: 250px "
                         class="demo-options mdl-card   mdl-shadow--4dp mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--12-col-desktop">
                        <div class="mdl-card__title mdl-card--expand ">
                            <h3 style=" font-size: 35px" class="mdl-card__title-text mdl-color-text--grey-600">
                                سيارة مشتركة</h3>
                        </div>
                        <div class="mdl-card__supporting-text mdl-color-text--grey-600">


                            <p style="font-size: 25px; font: bolder; color: black;" id="sharedtripinfo_kms"></p>
                            <p style="font-size: 25px; font: bolder; color: black;" id="sharedtripinfo_mins"></p>
                            <p style="font-size: 25px; font: bolder; color: black;" id="sharedtripinfo_price"></p>


                        </div>
                        <div class="mdl-card__actions mdl-card--border">

                            <button
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  mdl-button--accent  " onclick="populateDB(2,Costs[1])">
                                قدم طلب

                            </button>

                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                            <div id="editform" align="right">
                                <button id="editform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="editForm()">
                                    تعديل الطلب
                                </button>
                            </div>

                        </div>
                    </div>



                    <div id="customprice" style="width: 280px; height: 250px "
                         class="demo-options mdl-card   mdl-shadow--4dp mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--12-col-desktop">
                        <div class="mdl-card__title mdl-card--expand ">
                            {{--<h3 style=" font-size: 35px" class="mdl-card__title-text mdl-color-text--grey-600">
                               Custom</h3>--}}
                        </div>
                        <div class="mdl-card__supporting-text mdl-color-text--grey-600">

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="cprice" >
                                <label class="mdl-textfield__label" for="sample4">Price</label>
                                <span class="mdl-textfield__error">Input is not a number!</span>
                            </div>


                            {{--   Price: <input type="text" id="customprice" ><br>--}}

                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                <input type="radio" id="option-1" class="mdl-radio__button" name="contracttype" value="1" checked>
                                <span class="mdl-radio__label">Private</span>
                            </label>
                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                <input type="radio" id="option-2" class="mdl-radio__button" name="contracttype" value="2">
                                <span class="mdl-radio__label">Shared</span>
                            </label>



                        </div>
                        <div class="mdl-card__actions mdl-card--border">


                            <button
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  mdl-button--accent  " onclick="customprice()">
                                قدم طلب
                                <script>

                                    function customprice( ){


                                        console.log($('#cprice').val());

                                        if($('#cprice').val()==""|| isNaN($('#cprice').val())){
                                            alert("please enter a valid custom price");
                                        }else  {
                                            populateDB($('input[name=contracttype]:checked').val(),$('#cprice').val());

                                        }


                                    }

                                </script>

                            </button>

                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                            <div id="editform" align="right">
                                <button id="editform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="editForm()">
                                    تعديل الطلب
                                </button>
                            </div>

                        </div>
                    </div>



                </div>






                <form id="tripform" >

                </form>

                <div class="container">







                    <form   role="form" id="kaspercab-form">

                        <div  >
                            <div  >
                                <div   >
                                    <!-- <a target="_blank" href="https://www.kasper-cab.com/request/req/CreateContract.php"> <h2>ENGLISH</h2> </a> -->
                                </div>
                                <h4>نموذج طلب خدمة شهرية:</h4>
                                <p>
                                    فضلا تأكد من ان المعلومات المدخلة صحيحة
                                <p>
                            </div>
                        </div>

                        <hr>


                    <!--
                        <div class="form-group">
                            <label class="col-lg-3 control-label">First Name <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="firstName" placeholder="First Name"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Last Name <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="lastName" placeholder="Last Name"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Email <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="email" placeholder="name@domain.com"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Mobile Number: <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="mobileNumber" placeholder="0505000111"/>
                                </div>
                            </div>
                        </div>
                         -->


                        <script>

                            //get today date.
                            function min() {

                                var date = new Date();

                                date.setDate(date.getDate()+1);

                                var day = date.getDate();
                                var month = date.getMonth() + 1;
                                var year = date.getFullYear();

                                if (month < 10) month = "0" + month;
                                if (day < 10) day = "0" + day;


                                return  year + "-" + month + "-" + day;

                            }

                            //today date +10 days ahead.
                            function max() {

                                var date = new Date();

                                date.setDate(date.getDate()+9);

                                var  day = date.getDate();
                                var month = date.getMonth()+1;
                                var year = date.getFullYear();

                                if (month < 10) month = "0" + month;
                                if (day < 10) day = "0" + day;

                                return  year + "-" + month + "-" + day;

                            }
                        </script>





                        <div class="form-group">
                            <label class="  control-label">الإسم <abbr title="الادخال اجباري"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="firstAndLastName" id="firstAndLastName" placeholder="مثال: اسماء محمد"/>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="  control-label">البريد الإلكتروني <abbr title="الادخال اجباري"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="name@domain.com"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="  control-label">رقم الجوال <abbr title="الادخال اجباري"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">
                                    <input type="tel" class="form-control" name="mobileNumber" id="mobileNumber" placeholder="0505000111"/>
                                </div>
                            </div>
                        </div>








                        <div class="form-group">
                            <label class="  control-label">بدأ الخدمة:<abbr    title="This field is mandatory. You can't start today"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">
                                    <input type="date" id="startingdate"  >
                                </div>
                            </div>
                            <script>
                                // document.getElementById('startingdate').setAttribute('max', max());
                                document.getElementById('startingdate').setAttribute('min', min());
                            </script>

                        </div>









                        <div class="form-group">
                            <label class="  control-label">ايام الخدمة <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">


                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="days" id="days1" value="Saturday"><span style="color: white"> بsي</span>السبت</label>

                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days2" value="Sunday"><span style="color: white"> بsي</span>الاحد</label>
                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days3" value="Monday"><span style="color: white"> بsي</span>الاثنين</label>
                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days4" value="Tuesday"><span style="color: white"> بsي</span>الثلاثاء
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days5" value="Wednesday"><span style="color: white"> بsي</span>الاربعاء
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days6" value="Thursday"><span style="color: white"> بsي</span>الخميس
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" name="days" id="days7" value="Friday"><span style="color: white"> بsي</span>الجمعة
                                    </label>

                                    <input type="text" class="form-control" style='display:none' name="ServiceDays" id="ServiceDays" />
                                </div>

                            </div>
                        </div>



                        <!--
                       <div class="form-group">
                           <label class="col-lg-3 control-label">What is the name of your work place? <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                           <div class="col-lg-3 inputGroupContainer">
                               <div class="input-group">
                                   <input type="text" class="form-control" name="workPlace" placeholder="e.g. Red Sea Mall"/>
                               </div>
                           </div>
                       </div>
                          -->




                        <!--
                        <div class="form-group">
                            <label class="col-lg-3 control-label">For how many days per week you want the service?</label>
                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group">
                                    <input type="radio" name="daysPerWeek" id="optionsRadio5" value="no" checked> 5
                                    <input type="radio" name="daysPerWeek" id="optionsRadio6" value="yes" > 6

                                    <!--
                                    <div id='show-me' style='display:none' > <label >Please enter your base salary is Saudi Riyals: </label>
                                        <div class="input-group col-lg-15">

                                            <input type="text" class="form-control" name="salaryNumber" id="salaryNumber" placeholder="0"/>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                          -->


                        <div class="form-group">
                            <label class="  control-label">نوع الرحلة</label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">
                                    <input   type="radio" name="tripType" id="oneway" value="1" > ذهاب
                                    <input checked type="radio" name="tripType" id="twoway" value="2" > ذهاب و عودة




                                    <!--
                                    <div id='show-me' style='display:none' > <label >Please enter your base salary is Saudi Riyals: </label>
                                        <div class="input-group col-lg-15">

                                            <input type="text" class="form-control" name="salaryNumber" id="salaryNumber" placeholder="0"/>
                                        </div>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="  control-label">وقت الذهاب؟ <abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                            <div class="  inputGroupContainer">
                                <div class="input-group">





                                    <select name="pickUp1ampm" id="pickUp1ampm">
                                        <option value="am">AM</option>
                                        <option value="pm">PM</option>
                                    </select>


                                    <select name="pickUp1minute" id="pickUp1minute">
                                        <option value="00">00</option>
                                        <option value="05">05</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="30">30</option>
                                        <option value="35">35</option>
                                        <option value="40">40</option>
                                        <option value="45">45</option>
                                        <option value="50">50</option>
                                        <option value="55">55</option>
                                    </select>
                                    :
                                    <select name="pickUp1hour" id="pickUp1hour">
                                        <option value=""></option selected>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>

                                </div>
                            </div>
                        </div>


                        <div id="rtime" >
                            <div class="form-group">
                                <label class="  control-label">وقت العودة؟<abbr title="This field is mandatory"> <font color="red">*</font> </abbr> </label>
                                <div class="  inputGroupContainer">
                                    <div class="input-group">




                                        <select name="pickUp2ampm" id="pickUp2ampm">
                                            <option value="am">AM</option>
                                            <option value="pm">PM</option>
                                        </select>



                                        <select name="pickUp2minute" id="pickUp2minute" >
                                            <option value="00">00</option>
                                            <option value="05">05</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="25">25</option>
                                            <option value="30">30</option>
                                            <option value="35">35</option>
                                            <option value="40">40</option>
                                            <option value="45">45</option>
                                            <option value="50">50</option>
                                            <option value="55">55</option>
                                        </select>
                                        :
                                        <select name="pickUp2hour" id="pickUp2hour">
                                            <option value=""></option selected>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">



                            <label class="  control-label">مكان المنزل:<abbr title="This field is mandatory"> <font color="red">*</font> </abbr></label>

                            <!-- home location is empty error -->


                            <div id="map_canvas" style="width:330px; height:400px; margin-left:15px;"></div>


                            <!--

                            <div class="col-lg-3 inputGroupContainer">
                                <div class="input-group col-lg-10">
                                    <input type="text" class="form-control" id="homeAddressLatitude" name="homeAddressLatitude" placeholder="latitude" size="10"/>
                                    <input type="text" class="form-control" id="homeAddressLongitude" name="homeAddressLongitude" placeholder="longitude" size="10"/>
                                </div>
                            </div>

                        </div>

                             -->



                            <br>
                            <br>

                            <div class="form-group">

                                <div  id="end">
                                    <label class="  control-label">فضلا اختر عنوان العمل من القائمة المسدلة او يدويا من الخريطة:<abbr title="This field is mandatory"> <font color="red">*</font> </abbr></label>
                                    <select id="worklocation" style="height:30px; width:160px; border:6px solid #fff; background-color:#fff;
                        line-height: 38px; padding-left: 7px; padding-right: 7px; font-size: 14px; font-family:Roboto,Arial,sans-serif;
                        text-align: center; cursor: pointer; box-shadow:0 2px 6px rgba(0,0,0,.3); border-radius: 3px">
                                        <option value="0" >Custome</option>
                                        <option value="1">IKEA</option>
                                        <option value="2">BUPA</option>
                                        <option value="3">UBT</option>
                                        <option value="4">EY</option>
                                        <option value="5">Doroob</option>
                                        <option value="6">Al-Aghar Grouop</option>
                                        <option value="7">Dar Al-Hekma</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <label class="  control-label">عنوان العمل؟<abbr title="This field is mandatory"> <font color="red">*</font> </abbr></label>



                                <!-- work location is empty error -->



                                <div id="map_canvas2" style="width:330px; height:400px; margin-left:15px;"></div>






                                <!--

                                <div class="col-lg-3 inputGroupContainer">
                                    <div class="input-group col-lg-10"    >
                                        <input type="text" class="form-control" id="workAddressLatitude" name="workAddressLatitude" placeholder="latitude" size="10"   />
                                        <input type="text" class="form-control" id="workAddressLongitude" name="workAddressLongitude" placeholder="longitude" size="10"/>
                                    </div>
                                </div>

                            </div>

                           -->





                                <!--
                              <div class="form-group">
                                  <label class="col-lg-3 control-label">Notes</label>
                                  <div class="col-lg-3 inputGroupContainer">
                                      <div class="input-group col-lg-10">
                                          <textarea type="text" class="form-control" name="notes" placeholder="Comments or questions?" rows="4" style="resize: vertical;"></textarea>
                                      </div>
                                  </div>
                              </div>
                               -->






                                <!--
                                    <button  id="testvalidate" onclick="finilizeData( )">Submit</button>
                                 -->






                                <!--
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-3">
                                    <button type="submit" class="btn btn-default" id="postForm" onclick="Validate">Submit</button>
                                </div>
                            </div>
                              -->
                    </form>



                    <br>
                    <br>

                    <div class="form-group">
                        <label style="font-size: 10px" class="  control-label">اضافة طفل للاشتراك "مجانا فقط للعقود الخاصة لا ينطبق العرض للعقود المشتركة:</label>
                        <div class="  inputGroupContainer">
                            <div class="input-group">
                                <input  checked type="radio" name="addchild" id="yes" value="yes" > نعم
                                <input  type="radio" name="addchild" id="no" value="no" > لا

                                <!--
                                <div id='show-me' style='display:none' > <label >Please enter your base salary is Saudi Riyals: </label>
                                    <div class="input-group col-lg-15">

                                        <input type="text" class="form-control" name="salaryNumber" id="salaryNumber" placeholder="0"/>
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>


                    <div id="map_canvas3" style="width:330px; height:400px; margin-left:15px;"></div>



                    </form>



                    <br>
                    <br>
                    <br>
                    &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <input  align="center" type="checkbox"  value="check" id="agree" /> <span style="font-size: 18px "><b>اقر بأن جميع المعلومات صحيحة.</b></span>

                    <br>
                    <br>
                    <br>

                    <div id="submit" align="center">
                        <button id="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect  "   onclick="finilizeData()">
                            احسب التسعيرة
                        </button>
                    </div>

                    <!--

           <div id="editform" align="right">
               <button id="editform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="editForm()">
                   Edit request
               </button>
           </div>
               -->

                </div>
            </div>


        </div >
    </main>

    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    -->
    <script>

        $("#prices").hide();
        $("#editform").hide();
        $("#final").hide();

        //no need for restricting starting date currently, might be for the future.
        var date = new Date();
        date.setDate(date.getDate()+1);


        //compatibility script for date.
        if ( $('#startingdate')[0].type != 'date' ) $('#startingdate').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '2017:2020',
            minDate: date
            //maxDate: date
        });


        var  numberoftripspermonth = 0;

        var Costs = null ;
        var triptype = 2;
        var daysChecked = "";
        var goingTime =null ;
        var returnTime =null;
        var CorrectDateFormat ;
        var address1;
        var address2;
        var address3 = "none";
        var extra = "yes";




        $('input:radio').on('change', function(){
            if($(this).val()==1){

                //one way.
                triptype = 1;
                $("#rtime").hide();


            }else if($(this).val()==2){


                $("#rtime").show();
                //two ways
                triptype =2;

            }else if($(this).val()=="yes"){

                console.log("yes");
                extra = "yes";
                $("#map_canvas3").show();

            }else if($(this).val()=="no"){

                console.log("no");
                $("#map_canvas3").hide();

                extra = "no";

            }

        });













        function Validate() {

            /*
             console.log("hello validate");

             console.log(marker2);

             console.log(marker);
             */



            var nameError = "الرجاء تعبئة حقل الاسم";
            var mobileError = "الرجاء تعبئة حقل رقم الجوال";
            var emailError = "الرجاء تعبئة حقل الايميل";

            var extraError = "الرجاء اختيار الموقع الثالث للطفل" ;


            var startingEror = "الرجاء اختيار تاريخ بدأ الخدمة";
            var homelocationerror = "الرجاء اختيار مكان المنزل من الخريطة";
            var worklocationerror = "الرجاء اختيار مكان العمل من الخريطة"
            var goingtimeError = "الرجاء اختيار وقت الذهاب";
            var returntimeError = "الرجاء اختيار وقت العودة";
            var daysError = "الرجاء اختيار ايام الخدمة";
            var termError = "الرجاء التأكيد ان جميع المعلومات المدخلة صحيحة من اسفل الصفحة";

            var form = $("#errormsg");
            error = $( "ul.errorMessages", form );
            error.empty();







            //validate
            if(triptype==1){
                if($('#pickUp1hour :selected').text()==""){
                    error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                        "'>"
                        +goingtimeError + "</span> ");
                    window.location.hash = '#errormsg';
                    return false;
                }

            }else if(triptype==2){
                if($('#pickUp1hour :selected').text()==""){
                    error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                        "'>"
                        +goingtimeError + "</span> ");
                    window.location.hash = '#errormsg';
                    return false;

                }
                if($('#pickUp2hour :selected').text()==""){
                    error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                        "'>"
                        +returntimeError + "</span> ");
                    window.location.hash = '#errormsg';
                    return false;
                }

            }



            if($("#startingdate").val()==""){


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +startingEror + "</span> ");
                window.location.hash = '#errormsg';
                return false;
            }





            daysChecked = "" ;
            for (var x = 1; x <= 7; x++) {


                if ($("#days" + x).prop('checked')) {

                    daysChecked = daysChecked + x;

                }

            }



            //no days are chosen
            if (daysChecked == "") {
                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +daysError + "</span> ");
                window.location.hash = '#errormsg';
                return false;

            }


            if(!marker){


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +homelocationerror + "</span> ");
                window.location.hash = '#errormsg';
                return false;


            }


            if(!marker2){


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +worklocationerror + "</span> ");
                window.location.hash = '#errormsg';
                return false;


            }


            if(!$("#agree").is(':checked')){
                error.show().append("<span style='  font-size: 25px; color: #D8000C; background-color: #FFBABA;" +
                    " '>"
                    +termError + "</span>");
                window.location.hash = '#errormsg';
                return false;

            }




            if($("#firstAndLastName").val()==""){


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +nameError + "</span> ");
                window.location.hash = '#errormsg';
                return false;
            }



            if($("#mobileNumber").val().length<10){


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +mobileError + "</span> ");
                window.location.hash = '#errormsg';
                return false;
            }


            if($("#email").val()==""){

                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +emailError + "</span> ");
                window.location.hash = '#errormsg';
                return false;
            }



            if(extra == "yes" && marker3==false )
            {


                error.show().append("<span style='  font-size: 22px; color: #D8000C; background-color: #FFBABA;" +
                    "'>"
                    +extraError + "</span> ");
                window.location.hash = '#errormsg';
                return false;
            }






            return true ;

        }


        function finilizeData( ) {




            if(Validate()==false){

                return;
            }

            $("#contractform").height(900);









            //TIME LOGIC:
            //do the time logic here

            if ($('#pickUp1ampm :selected').text() == "PM") {

                var goingtimehours = Number($('#pickUp1hour :selected').text()) + 12;

            } else {

                var goingtimehours = Number($('#pickUp1hour :selected').text());

            }

            var goiingtimeminutes = $('#pickUp1minute :selected').text();

            goingTime = goingtimehours + ":" + goiingtimeminutes + ":00";




            if(triptype == 2){


                if ($('#pickUp2ampm :selected').text() == "PM") {

                    var returntimehours = Number($('#pickUp2hour :selected').text()) + 12;


                } else {

                    var returntimehours = Number($('#pickUp2hour :selected').text());

                }

                var returntimeminutes = $('#pickUp2minute :selected').text();


                returnTime = returntimehours + ":" + returntimeminutes + ":00";


            }   else  {

                returnTime = "00:00:00";

            }





            if ( $('#startingdate')[0].type == 'date' ){

                CorrectDateFormat = $('#startingdate').val();

            }else {
                var strings = $('#startingdate').val().split("/");
                CorrectDateFormat = strings[2]+"/"+strings[0]+"/"+strings[1];
            }









            //Access start and end locations globally:
            calculateDistances({
                    lat: marker.getPosition().lat(),
                    lng: marker.getPosition().lng()
                },
                {
                    lat:  marker2.getPosition().lat(),
                    lng: marker2.getPosition().lng()
                });










            /*
             console.log("home lat"+marker.getPosition().lat()) ;
             console.log("home lan"+marker.getPosition().lng());
             console.log("work lat"+marker2.getPosition().lat());
             console.log("work lat"+marker2.getPosition().lng());

             console.log(Kilometers);
             console.log(Minutes);

             console.log("goingTime: "+goingTime);
             console.log("returnTime:" +returnTime );
             console.log("Date" +CorrectDateFormat);
             console.log("daysperweek "+daysChecked.toString().split("").length);


             //need to implemented !!!
             console.log(cost);

             //need to implement !!!
             console.log(contractType);


             console.log("daysChecked "+ daysChecked);


             console.log("employer"+ employer );
             */

















            //Callback inside calculateDistance is asynchronous call you have to wait for it to finish.
            //or call the functions inside callback




            /*

             1. Trip type.
             2. Goingtime and/or trturn time.
             3. Check checked boxes. "possibly new column in the database.
             4. Starting time.
             5. from and to location already defined in the maps api script.
             6. km.
             7. min.
             8.
             9. userid from the sesstion
             10. Ajax calls ----> create a new php file to do the quries and hable SQL calls and errors.

             */








        }


        function pricesCards(){


            //private price.
            //  $("#privatetripinfo_kms").text(Math.round(Kilometers)+" KM");
            //  $("#privatetripinfo_mins").text(Math.round(Minutes)+" MIN");
            $("#privatetripinfo_price").text(Math.round(Costs[0])+" SR");


            if(extra== "yes" ){

                $("#sharedcard").hide();

            }else {


                $("#sharedcard").show();


            }

            //shared prices
            //$("#sharedtripinfo_kms").text(Math.round(Kilometers)+" KM");
            // $("#sharedtripinfo_mins").text(Math.round(Minutes)+" MIN");
            $("#sharedtripinfo_price").text(Math.round(Costs[1])+" SR");


            //shared by two prices "for later".

        }




        function populateDB(contractType,cost) {

            /*
             console.log("contractty"+contractType);
             console.log("cost"+cost);

             console.log("Trips Per Month: "+numberoftripspermonth);
             return;*/



            var  extraLat ;
            var  extraLng  ;


            $("#editform").hide();
            $("#prices").hide();
            $("#final").show();


            console.log($("#firstAndLastName").val());
            console.log($("#mobileNumber").val());
            console.log($("#email").val());



            console.log("home lat"+marker.getPosition().lat());
            console.log("home lan"+marker.getPosition().lng());
            console.log("work lat"+marker2.getPosition().lat());
            console.log("work lan"+marker2.getPosition().lng());


            if(extra=="no"){


                extraLat = "none";
                extraLng  = "none";

            }else {

                console.log(marker3);

                extraLat = marker3.getPosition().lat();
                extraLng = marker3.getPosition().lng();


            }

            console.log("extra lat"+extraLat);
            console.log("extra lan"+extraLng);
            console.log(Kilometers);
            console.log(Minutes);
            console.log("goingTime: "+goingTime);
            console.log("returnTime:" +returnTime );
            console.log("daysperweek "+daysChecked.toString().split("").length);
            console.log(cost);
            console.log("Date" +CorrectDateFormat);
            console.log(contractType);
            console.log("daysChecked "+ daysChecked);
            console.log("employer"+ employer);


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //remove first 0 from the string.
            var mobile = $("#mobileNumber").val().replace("0","");


            geocodePosition(new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng()), function (location) {
                address1 = location; // Callback

                console.log(address1);

                geocodePosition(new google.maps.LatLng(marker2.getPosition().lat(), marker2.getPosition().lng()), function (location) {
                    address2 = location; // Callback

                    if (extraLat != "none") {
                        geocodePosition(new google.maps.LatLng(marker3.getPosition().lat(), marker3.getPosition().lng()), function (location) {
                            address3 = location; // Callback


                            console.log("trips per month " +numberoftripspermonth);

                            $.ajax({
                                type: 'POST',
                                url: 'newcontract/createmonthlycontract',
                                dataType: 'text',
                                data: {
                                    name: $("#firstAndLastName").val(),
                                    mobile: mobile,
                                    email: $("#email").val(),
                                    str_lat: marker.getPosition().lat(),
                                    str_lng: marker.getPosition().lng(),
                                    end_lat: marker2.getPosition().lat(),
                                    end_lng: marker2.getPosition().lng(),
                                    trip_one_address: address1,
                                    trip_two_address: address2,
                                    extra_address: address3,
                                    extra_lat: extraLat,
                                    extra_lng: extraLng,
                                    km: Kilometers,
                                    min: Minutes,
                                    going_time: goingTime,
                                    return_time: returnTime,
                                    cost: cost,
                                    starting_date: CorrectDateFormat,
                                    contract_type: contractType,
                                    days: daysChecked,
                                    employer: employer,
                                    numberofdays: numberoftripspermonth
                                },

                            }).done(function (text) {
                                alert(text);
                                console.log("done");
                                //  window.open("https://www.kasper-cab.com/request/req/mysubscriptions.php","_self");
                                //  alert(text);
                            }); // Ajax Call

                        });

                    } else {

                        console.log("trips per month " +numberoftripspermonth);


                        $.ajax({
                            type: 'POST',
                            url: 'newcontract/createmonthlycontract',
                            dataType: 'text',
                            data: {
                                name: $("#firstAndLastName").val(),
                                mobile: mobile,
                                email: $("#email").val(),
                                str_lat: marker.getPosition().lat(),
                                str_lng: marker.getPosition().lng(),
                                end_lat: marker2.getPosition().lat(),
                                end_lng: marker2.getPosition().lng(),
                                trip_one_address: address1,
                                trip_two_address: address2,
                                extra_address: address3,
                                extra_lat: extraLat,
                                extra_lng: extraLng,
                                km: Kilometers,
                                min: Minutes,
                                going_time: goingTime,
                                return_time: returnTime,
                                cost: cost,
                                starting_date: CorrectDateFormat,
                                contract_type: contractType,
                                days: daysChecked,
                                employer: employer,
                                numberofdays: numberoftripspermonth

                            },

                        }).done(function (text) {
                            alert(text);
                            console.log("done");
                            //  window.open("https://www.kasper-cab.com/request/req/mysubscriptions.php","_self");
                            //  alert(text);
                        }); // Ajax Call

                    }
                });
            });



            /*
             function reverseGeocodeAddress() {
             $.ajax({
             type: "POST",
             url: './geo-info-response',
             data: "",
             success: function() {
             console.log("Geodata sent");
             }
             })
             };
             */


            //call create monthly contract route here:


            /*
             $.ajax({
             type: 'GET',
             url: 'insertContract.php',
             dataType: 'text',
             data: {
             name: $("#firstAndLastName").val() ,
             mobile:$("#mobileNumber").val(),
             email: $("#email").val(),
             str_lat: marker.getPosition().lat() ,
             str_lng: marker.getPosition().lng(),
             end_lat: marker2.getPosition().lat(),
             end_lng: marker2.getPosition().lng(),
             extra_lat: extraLat,
             extra_lng: extraLng,
             km: Kilometers,
             min: Minutes,
             going_time: goingTime,
             return_time:returnTime,
             cost: cost,
             starting_date: CorrectDateFormat,
             contract_type: contractType,
             days: daysChecked,
             employer: employer
             },

             }).done(function(text){
             alert(text);
             console.log("done");
             //  window.open("https://www.kasper-cab.com/request/req/mysubscriptions.php","_self");
             //  alert(text);
             }); // Ajax Call

             //console.log("Data Inserted"); //wrong placement, you have to wait for the call back
             // window.open("https://www.kasper-cab.com/request_dev/kaspercab1/mysubscriptions.php","_self");
             //-> return to mycontracts.php
             */

        }









        function geocodePosition(pos, returns) {

            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {

                    returns(responses[0].formatted_address);

                } else {
                    returns("none");
                }
            });

        }

        //change here to include time and starting date.
        function calculateDistances(origin, destination) {

            service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix(
                {
                    origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                },  function callback(response, status) {

                    if (status != google.maps.DistanceMatrixStatus.OK) {
                        alert('Error was: ' + status);
                        //change this alret later.
                    } else {

                    }
                    //call other functions here
                    Kilometers = response.rows[0].elements[0].distance.value / 1000;
                    Minutes = response.rows[0].elements[0].duration.value / 60;

                    console.log(Kilometers);
                    console.log(Minutes);



                    viewPrices();

                });

        }


        function editForm(){


            $("#contractform").height(2800);
            $("#kaspercab-form").show();
            $("#submit").show();
            $("#map").show();
            $("#editform").hide();
            $("#prices").hide();

            $('#agree').attr('checked', false);


        }



        function viewPrices( ){


            Costs =  calculateCost (Minutes ,Kilometers,daysChecked.toString().split("").length,triptype);


            console.log("days" +daysChecked.toString().split("").length);

            console.log("triptype" +triptype);

            console.log("km" +Kilometers);

            console.log("min" +Minutes);

            console.log("Private cost "+ Costs[0]);
            console.log("Shared cost "+ Costs[1]);




            $("#kaspercab-form").hide();
            $("#submit").hide();
            $("#editform").show();
            $("#prices").show();
            /*
             $("#map").hide();
             $("#map2").hide();
             */


            pricesCards ();



        }





        var addresses =   [ "CUSTOM",
            "8583 الامير ماجد الفرعي, حي الفيحاء، Jeddah 22251",
            "Al-Khalidiyah, Jeddah 23423",
            "Al Rawdah, Jeddah 23435 21361",
            "King's Road Tower, King Abdul Aziz Rd, Al-Shate'a, Jeddah 23412",
            "Zahran Business Center, Jeddah, Makkah Province",
            "3148 Muhammad Ali Al Harkan, Al Andalus, Jeddah 23325, Saudi Arabia",
            "6702 Prince Majed-Al Faiha, Jeddah 22246"
        ];



        var service = null;
        var Kilometers = null;
        var Minutes = null;
        var custom = true;
        var custom2 = true;


        var employers = ["CUSTOM","IKEA","BUPA","UBT","KY","DOROOB","Al-Aghar","DAH"];

        var icons = [" mapIcons/workoffice.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/ikea.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/bupa.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/ubtg.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/ky.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/doroob.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/aghar.png",
            "/Dev-Server-Resources/new-img/mapIcons/mapIcons/dah.png"];

        var employer = "CUSTOM";

        var locations ;


        //Set up some of our variables.
        var map ; //Will contain map object.
        var map2;
        var map3;




        var marker = false; ////Has the user plotted their location marker?
        var marker2 = false; ////Has the user plotted their location marker?
        var marker3  = false;
        var geocoder ;

        //Function called to initialize / create the map.
        //This is called when the page has loaded.

        var companymarker ;

        function initMap() {


            geocoder = new google.maps.Geocoder;


            //location of fixed companies
            locations =  [ new google.maps.LatLng(21.529,39.179),
                new google.maps.LatLng(21.507414, 39.223138),
                new google.maps.LatLng(21.567162, 39.140060),
                new google.maps.LatLng(21.563645, 39.167442),
                new google.maps.LatLng(21.559087, 39.124810),
                new google.maps.LatLng(21.602869, 39.144125),
                new google.maps.LatLng(21.539519, 39.163873),
                new google.maps.LatLng(21.488493, 39.230245)];


            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(21.551343, 39.174972);


            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 10, //The zoom value.
                disableDefaultUI: true

            };

            var options2 = {
                center: centerOfMap, //Set center.
                zoom: 10, //The zoom value.
                disableDefaultUI: true

            };
            //Create the map object.
            map = new google.maps.Map(document.getElementById('map_canvas'), options);


            var centerControlDiv = document.createElement('span');
            var centerControl = new CenterControl(centerControlDiv, map);




            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);




            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });


            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }



                //Causation:  getPlaces() returns more than one location as object but for simplicity we have chosen the first place it returns.

                clearHomeMarker();

                marker = new google.maps.Marker({
                    position: places[0].geometry.location,
                    map: map,
                    draggable: true //make it draggable
                });



                map.setCenter(places[0].geometry.location);
                map.setZoom(15);
                //  console.log(places[0].geometry.location);
                //clear marker1 and push the new one.

            });


            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);





            map2 = new google.maps.Map(document.getElementById('map_canvas2'), options2);
            map3 = new google.maps.Map(document.getElementById('map_canvas3'), options2);


            // Create the search box and link it to the UI element.
            var input2 = document.getElementById('pac-input2');
            var searchBox2 = new google.maps.places.SearchBox(input2);
            map2.controls[google.maps.ControlPosition.TOP_RIGHT].push(input2);


            // Bias the SearchBox results towards current map's viewport.
            map2.addListener('bounds_changed', function() {
                searchBox2.setBounds(map2.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox2.addListener('places_changed', function() {
                var places = searchBox2.getPlaces();

                if (places.length == 0) {
                    return;
                }



                clearMarker();
                marker2 = new google.maps.Marker({
                    position: places[0].geometry.location,
                    map: map2,
                    draggable: true //make it draggable
                });

                map2.setCenter(places[0].geometry.location);

                map2.setZoom(15);

            });

            // Create the search box and link it to the UI element.
            var input3 = document.getElementById('pac-input3');
            var searchBox3 = new google.maps.places.SearchBox(input3);
            map3.controls[google.maps.ControlPosition.TOP_RIGHT].push(input3);


            // Bias the SearchBox results towards current map's viewport.
            map3.addListener('bounds_changed', function() {
                searchBox3.setBounds(map3.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox3.addListener('places_changed', function() {
                var places = searchBox3.getPlaces();

                if (places.length == 0) {
                    return;
                }



                ClearThirdMarker();
                marker3 = new google.maps.Marker({
                    position: places[0].geometry.location,
                    map: map3,
                    draggable: true //make it draggable
                });

                map3.setCenter(places[0].geometry.location);

                map3.setZoom(15);

            });

            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        // markerLocation1();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                // markerLocation1();
            });

            function CenterControl(controlDiv, map) {

                // Set CSS for the control border.
                var controlUI = document.createElement('span');
                controlUI.style.backgroundColor = '#fff';
                controlUI.style.border = '6px solid #fff';
                controlUI.style.borderRadius = '3px';
                controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
                controlUI.style.cursor = 'pointer';
                controlUI.style.marginBottom = '22px';
                controlUI.style.textAlign = 'center';
                controlUI.title = 'Click to pick your current location';
                controlDiv.appendChild(controlUI);

                //Set CSS for the control interior.
                var controlText = document.createElement('span');
                controlText.style.color = 'rgb(25,25,25)';
                controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
                controlText.style.fontSize = '16px';
                controlText.style.lineHeight = '38px';
                controlText.style.paddingLeft = '7px';
                controlText.style.paddingRight = '7px';
                controlText.innerHTML = 'My Location';
                controlUI.appendChild(controlText);
                controlUI.addEventListener('click'
                    , function() {
                        GetUserLocation();
                    });
            }

            function GetUserLocation() {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {

                        OriginOfUserLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        map.setCenter(OriginOfUserLocation);

                        clearHomeMarker();
                        addMycurrentLocation(OriginOfUserLocation);

                    });
                }
            }

            google.maps.event.addListener(map2, 'click', function(event) {

                if(custom){
                    console.log("custom is true?"+ custom);
                    //Get the location that the user clicked.
                    var clickedLocation = event.latLng;
                    //If the marker hasn't been added.
                    if(marker2 === false){
                        //Create the marker.
                        marker2 = new google.maps.Marker({
                            position: clickedLocation,
                            map: map2,
                            draggable: true //make it draggable
                        });
                        //Listen for drag events!
                        google.maps.event.addListener(marker2, 'dragend', function(event){

                            //  markerLocation2();
                        });
                    } else{
                        //Marker has already been added, so just change its location.

                        console.log(marker2);
                        marker2.setPosition(clickedLocation);

                    }

                } else {

                    console.log("custom is false?"+ custom);

                }

            });

            google.maps.event.addListener(map3, 'click', function(event) {

                if(custom2){
                    console.log("custom is true?"+ custom2);
                    //Get the location that the user clicked.
                    var clickedLocation = event.latLng;
                    //If the marker hasn't been added.
                    if(marker3 === false){
                        //Create the marker.
                        marker3 = new google.maps.Marker({
                            position: clickedLocation,
                            map: map3,
                            draggable: true //make it draggable
                        });
                        //Listen for drag events!
                        google.maps.event.addListener(marker3, 'dragend', function(event){

                            //  markerLocation2();
                        });
                    } else{
                        //Marker has already been added, so just change its location.

                        console.log(marker3);
                        marker3.setPosition(clickedLocation);

                    }

                } else {

                    console.log("custom is false?"+ custom2);

                }

            });


        }







        function clearHomeMarker() {



            try {
                console.log("worker marker "+marker.getPosition());

                console.log("home marker "+marker.getPosition());
                //  marker2.setMap(null);
            } catch (err) {
                console.log("Markers are not initialized ");
            }


            try {
                marker.setMap(null);
                marker = false ;
            } catch (err) {
                console.log("Marker is not initialized ");
            }





        }

        function clearMarker( ){

            try {
                console.log("worker marker "+marker2.getPosition());

                console.log("home marker "+marker.getPosition());
                //  marker2.setMap(null);
            } catch (err) {
                console.log("Markers are not initialized ");
            }


            try {
                //  marker2.setMap(null);
            } catch (err) {
                console.log("Marker is not initialized ");
            }

            try {
                marker2.setMap(null);
                marker2 = false ;

            } catch (err) {
                console.log("Marker is not initialized ");
            }


        }

        function ClearThirdMarker() {


            try {
                marker3.setMap(null);
                marker3 = false ;

            } catch (err) {
                console.log("Marker is not initialized ");
            }




        }

        function addMycurrentLocation(CurrentLocation) {



            marker = new google.maps.Marker({
                position: CurrentLocation,
                draggable: true,
                map: map

            });

            map.setZoom(15);


        }

        function addMarker(index) {


            if(index == 0 ) {
                marker2 = new google.maps.Marker({
                    position: locations[index],
                    icon: icons[index],
                    draggable: true,
                    map: map2
                });

            }else {

                marker2 = new google.maps.Marker({
                    position: locations[index],
                    icon: icons[index],
                    draggable: false,
                    map: map2
                });

            }

        }

        //dropdown menu listener.
        $('#worklocation').on('change', function() {

            // console.log("custom"+ custom);



            if(this.value == 0){


                clearMarker();
                custom = true ;



            } else {

                custom = false;

                employer = employers[this.value];
                // console.log("employer: " +employer);
                // console.log("address "+ addresses[this.value]);
                clearMarker();
                addMarker(this.value);
                map2.setCenter( locations[this.value]);



            }
            // clearMarker();
            //  custom = true;
            //  return ;




            //custom = false;
            //function to place marker on the map.

            //Redraw(startLocation.marker.getPosition(),endLocation.marker.getPosition(),this.value);
            //}else {
            // Redraw(startLocation.marker.getPosition(),addresses[this.value],this.value);
            // }


        });

        function calculateCost(mins, kim, daysperweek, numberofrounds) {



            numberoftripspermonth = (daysperweek * numberofrounds) * 4;



            console.log("days per week"+ daysperweek);
            console.log("number of rounds"+ numberofrounds);





            if (numberofrounds == 1) {

                numberoftripspermonth = numberoftripspermonth + 2;

            } else if (numberofrounds == 2) {

                numberoftripspermonth = numberoftripspermonth + 4;
            }


            console.log("Trips per month"+ numberoftripspermonth);




            //private and shared costs.
            var privateCost =    Math.round(numberoftripspermonth * (6 + (kim * 1.2) + (mins * 0.3)));
            var sharedCost = Math.round(((privateCost * 0.2 + privateCost) / 2));


            if(privateCost<777){
                privateCost = 777;
            }

            if (sharedCost<777){
                sharedCost = 777;
            }

            /*for later
             var sharedBy3Cost ;
             */

            //return array of shared and private costs
            return [privateCost, sharedCost];
        }



    </script>




</div>


<!-- Jquery library-->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->


<!-- changes for datapickers-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


<!-- changes for timepickers -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>


<script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
<!-- load google API-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDANKgBI3KcMeePhW7hnMdNnVBtVa4fmJo&libraries=places&callback=initMap"
        async defer></script>

<script src="/Dev-Server-Resources/new-Javascript/validation-functions.js"></script>


</body>
</html>


