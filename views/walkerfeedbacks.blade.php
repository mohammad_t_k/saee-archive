@extends('layout')

@section('content')
<div class="box box-info tbl-box">
    <div align="left" id="paglink"><?php echo $walkerfeedback->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <th>Feedback ID</th>
                <th>Captain Name</th>
                <th>Date/Time</th>
                <th>Message</th>
                <th>Version</th>
                <th>Device Type</th>
                <th>Action</th>
            </tr>
            <?php $i = 0; ?>

            <?php foreach ($walkerfeedback as $feedback) { ?>
                <tr>
                    <td><?= $feedback->id ?></td>
                    <td><?php   echo $feedback->walker_first_name . " " . $feedback->walker_last_name;?></td>
                    <td><?php echo date("d M Y", strtotime($feedback->created_at)); ?>/<?php echo date("g:iA", strtotime($feedback->created_at)); ?></td>
                    <td><?= $feedback->message ?></td>
                    <td><?= $feedback->version ?></td>
                    <td><?= $feedback->device_type ?></td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                        </div>  

                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <div align="left" id="paglink"><?php echo $walkerfeedback->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>




</div>
@stop