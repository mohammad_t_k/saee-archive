@extends('layout')

@section('content')

<script src="https://bitbucket.org/pellepim/jstimezonedetect/downloads/jstz-1.0.4.min.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="http://momentjs.com/downloads/moment-timezone-with-data.min.js"></script> 

<!-- <div class="col-md-6 col-sm-12">

    <div class="box box-danger">

        <form method="get" action="{{ URL::Route('/admin/sortreq') }}">
            <div class="box-header">
                <h3 class="box-title">Sort</h3>
            </div>
            <div class="box-body row">

                <div class="col-md-6 col-sm-12">

                    <select class="form-control" id="sortdrop" name="type">
                        <option value="reqid" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'reqid') {
                            echo 'selected="selected"';
                        }
                        ?>  id="reqid">Request ID</option>
                        <option value="owner" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'owner') {
                            echo 'selected="selected"';
                        }
                        ?>  id="owner">{{ trans('customize.User');}} Name</option>
                        <option value="walker" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'walker') {
                            echo 'selected="selected"';
                        }
                        ?>  id="walker">{{ trans('customize.Provider');}}</option>
                        <option value="payment" <?php
                        if (isset($_GET['type']) && $_GET['type'] == 'payment') {
                            echo 'selected="selected"';
                        }
                        ?>  id="payment">Payment Mode</option>
                    </select>

                    <br>
                </div>
                <div class="col-md-6 col-sm-12">
                    <select class="form-control" id="sortdroporder" name="valu">
                        <option value="asc" <?php
                        if (isset($_GET['type']) && $_GET['valu'] == 'asc') {
                            echo 'selected="selected"';
                        }
                        ?>  id="asc">Ascending</option>
                        <option value="desc" <?php
                        if (isset($_GET['type']) && $_GET['valu'] == 'desc') {
                            echo 'selected="selected"';
                        }
                        ?>  id="desc">Descending</option>
                    </select>

                    <br>
                </div>

            </div>

            <div class="box-footer">

                <button type="submit" id="btnsort" class="btn btn-flat btn-block btn-success">Sort</button>


            </div>
        </form>

    </div>
</div>


<div class="col-md-6 col-sm-12">

    <div class="box box-danger">

        <form method="get" action="{{ URL::Route('/admin/searchreq') }}">
            <div class="box-header">
                <h3 class="box-title">Filter</h3>
            </div>
            <div class="box-body row">

                <div class="col-md-6 col-sm-12">

                    <select class="form-control" id="searchdrop" name="type">
                        <option value="reqid" id="reqid">Request ID</option>
                        <option value="owner" id="owner">{{ trans('customize.User');}} Name</option>
                        <option value="walker" id="walker">{{ trans('customize.Provider');}}</option>
                        <option value="payment" id="payment">Payment Mode</option>
                    </select>

                    <br>
                </div>
                <div class="col-md-6 col-sm-12">

                    <input class="form-control" type="text" name="valu" value="<?php
                        if (Session::has('valu')) {
                            echo Session::get('valu');
                        }
                    ?>" id="insearch" placeholder="keyword"/>
                    <br>
                </div>

            </div>

            <div class="box-footer">

                <button type="submit" id="btnsearch" class="btn btn-flat btn-block btn-success">Search</button>


            </div>
        </form>

    </div>
</div>

-->
@if(Session::has('FlashMessage'))
    <div class="alert-box success" style="color:red;">
        {{ Session::get('FlashMessage') }}
    </div>
@endif

<div class="box box-info tbl-box">
    <div align="left" id="paglink"><?php echo $monthly->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>
    <div align="right"> <button type="button" onclick="window.location='{{ url("/admin/newcontract") }}'">Create new Contract</button></div>

    <table class="table table-bordered">
        <tbody>
            <tr>
                <th>Request ID</th>
                <th>User Name</th>
                <th>Driver</th>
                <th>Subscription Type</th>
                <th>Cab Type</th>
                <th>Status</th>
                <th>Amount</th>
                <th>Start Date</th>
                <th>Action</th>
            </tr>
            <?php $i = 0; ?>

            <?php foreach ($monthly as $month) { ?>
                <tr>
                    <td><?= $month->id ?></td>
                    <td><?= $month->owner_first_name . " " . $month->owner_last_name; ?> </td>
                    <td>
                        <?php
	                        if ($month->walker_first_name) {
	                            echo $month->walker_first_name . " " . $month->walker_last_name;
	                        } else {
	                            echo "Un Assigned";
	                        }
                        ?>
                    </td>
                    <td>
                    	<?php if($month->subscription_type == 1){ echo "Private"; }
                    	elseif($month->subscription_type == 2){ echo "Shared"; } ?>
                    </td>
                    <td>
                        <?= $month->cab_type; ?>
                    </td>
                    <td>
                        <?php
                        if ($month->status == 0) {
                            echo "<span class='badge bg-red'>Pending</span>";
                        } elseif ($month->status == 1) {
                            echo "<span class='badge bg-green'>Approved</span>";
                        } elseif ($month->status == 2) {
                            echo "<span class='badge bg-red'>Driver Not Found</span>";
                        } elseif ($month->status == 3) {
                            echo "<span class='badge bg-green'>Driver Accepted</span>";
                        } elseif ($month->status == 4	) {
                            echo "<span class='badge bg-red'>Driver Rejected</span>";
                        } elseif ($month->status == 5 ) {
                            echo "<span class='badge bg-green'>Customer Confirmed</span>";
                        } elseif ($month->status == 6 ) {
                            echo "<span class='badge bg-red'>Customer Cancled</span>";
                        } elseif ($month->status == 7 ) {
                            echo "<span class='badge bg-green'>Paid</span>";
                        } elseif ($month->status == 8 ) {
                            echo "<span class='badge bg-yellow'>Completed</span>";
                        } elseif ($month->status == 9 ) {
                            echo "<span class='badge bg-red'>CSR Declined</span>";
                        } elseif ($month->status == 10 ) {
                            echo "<span class='badge bg-green'>Pay by Cash</span>";
                        } elseif ($month->status == 11 ) {
                            echo "<span class='badge bg-green'>Pay by Sadad</span>";
                        }elseif ($month->status == 12 ) {
                            echo "<span class='badge bg-red'>User Cancled</span>";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                            if($month->total_cost != 0 && $month->total_cost != ''){
                                echo $month->total_cost . " SAR"; 
                            }
                            else{
                                echo "0.00 SAR";
                            }
                        ?>
                    </td>
                    <td>
                       <?= $month->starting_date; ?>
                    </td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                            <?php /* echo Config::get('app.generic_keywords.Currency'); */ ?>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                    <!-- <li role="presentation"><a role="menuitem" id="map" tabindex="-1" href="">View Details</a></li> -->
                                <?php if ($month->status == 0) { ?>
                                    <li role="presentation"><a role="menuitem" id="map" tabindex="-1" href="{{ URL::Route('AdminMonthlyRequest', $month->id) }}">Approve</a></li>
                                <?php }else if ($month->status == 2){ ?> 
                                    <!-- <li role="presentation"><a role="menuitem" id="map" tabindex="-1" href="">Retry</a></li> -->
                                <?php } ?>


                                <li role="presentation"><a role="menuitem" id="map" tabindex="-1" href="{{ URL::Route('AdminMonthlySubscriptionView', $month->id) }}">View Subscription</a></li>
                                <li role="presentation"><a role="menuitem" id="map" tabindex="-1" href="{{ URL::Route('AdminCancleMonthlyRequest', $month->id) }}">Cancel</a></li>

                            </ul>
                        </div>  

                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <div align="left" id="paglink"><?php echo $monthly->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>

</div>

<script type="text/javascript">
</script>
@stop