
@extends('companies.layout')

@section('content')

<?php
    $companyAliases = BBAliases::select('alias_id', 'alias_name')->where('company_id','=',$company_id)->get();

    ?>

<div class="box box-primary">
              
                                <!-- form start -->
                               <form method="post" id="bb-update-form" action="{{ URL::Route('bBUserUpdate') }}"  enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $user->id ?>">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input disabled required="required" type="text" class="form-control" name="username" value="<?= $user->username ?>" placeholder="Username" >

                                        </div>
                                                                          
                                    </div><!-- /.box-body -->


                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Password *</label>
                                            <input required="required" type="password" class="form-control" name="password" value="" placeholder="Password" >

                                        </div>
                                                                          
                                    </div><!-- /.box-body -->

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input required="required" type="text" class="form-control" id="email" name="email" value="<?= $user->email ?>" placeholder="Email" >

                                        </div>
                                                                          
                                    </div><!-- /.box-body -->

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Phone *</label>
                                            <input required="required" type="phone" class="form-control" id="phone" name="phone" value="<?= $user->phone ?>" placeholder="Phone" >

                                        </div>
                                                                          
                                    </div><!-- /.box-body -->


                                  <div class="box-body">
                                    <div class="form-group">
                                    <label style="padding-left: 0px;" class="control-label col-md-12 col-sm-12 col-xs-12" for="aliasnamee">Alias
                                        <span class="required">*</span>
                                    </label>

                                    <div style="padding-left: 0px" class="col-md-12 col-sm-12 col-xs-12">
                                        <select  name="alias" required="required" class="form-control col-md-12 col-xs-12">
                                            <option value="">Select</option>
                                            <?php foreach($companyAliases as $alias){ ?>

                                                <option value="<?php echo $alias->alias_id ?>"><?php echo $alias->alias_name; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                              </div>

                                    <div class="box-footer">

                                      <button style="background: #F4AA2F;border: none;" onclick="if(!validateAllInputs()) return false;" type="submit" id="edit" class="btn btn-primary btn-flat btn-block">Update Changes</button>
                                    </div>
                                </form>
                            </div>


<script type="text/javascript">
    function validateAllInputs() {
        var result = true;
        // Validating Sign Up Captain
        var phone = document.getElementById("phone");
        if (phone.value.substring(0, 4) != "+966") {
            alert("*Phone number must start with +966 e.g.+966501234567.<br>(??? ?? ???? ??? ?????? ?? +966 ???? +966501234567)");
            result = false;
        } else if (phone.value.length != 13) {
            alert("*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(??? ?? ???? ??? ????? 13 ???? ???? ? 966)");
            result = false;
        }
        var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        var mailvalue = document.getElementById('email');
        if (!mailformat.test(mailvalue.value)) {
            alert("Invalid Email");
            result = false;
        }
        return result;
    }
</script>


<script type="text/javascript">
    var frm = $('#bb-update-form');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({


            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            beforeSend: function(){
                // Handle the beforeSend event
             $("#wait").css("display", "block");
            },
            complete: function(){
            // Handle the complete event
            $("#wait").css("display", "none");
            },
            success: function (data) {
                console.log('Submission was successful.');

                    //success message here no object 
                alert('Successfully Registered');
                window.location = '/blackbox/allusers';
                
                
            },
            error: function (data) {
                console.log('An error occurred.');
                var response = JSON.parse(data);
                alert('something went wrong');
                console.log(response);
            },
        });
    });
</script>

@stop