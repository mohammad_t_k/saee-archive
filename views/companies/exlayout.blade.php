<!DOCTYPE html>
<html lang="en">
<head>
    <title>:. Saee :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/companiesdashboard/css/font-awesome.css">
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/badge.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">


    <!-- Datatables -->
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
          rel="stylesheet">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
          rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.css"> -->
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>


<body>
<!--header start-->
<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/company/dashboard"><img
                                src="<?php echo asset_url(); ?>/companiesdashboard/images/logo-w.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="column margintop20 rs-show">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="/company/dashboard">
                                    Welcome,<br> <?php echo $companydata->company_name; ?></a>
                            </li>
                            <li><a href="/company/shipments/all"><span class="glyphicon glyphicon-chevron-right"></span>All
                                    Shipments</a></li>
                            <?php $is_api = Session::get('is_api');
                            if ($is_api == 0 || $companydata->company_type == 1) {
                                echo "<li><a href=\"/company/generateshipment\"><span class=\"glyphicon glyphicon-chevron-right\"></span>Generate Shipment</a></li>";
                            }?>
                            <li><a href="/company/shipmentsExport"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Export Shipments</a></li>
                            <li><a href="/company/sameDaySuccessRate"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Same Day Success Rate Report</a></li>
                            <li><a href="/company/admintracking"><span class="glyphicon glyphicon-chevron-right"></span>Single
                                    Tracking Search</a></li>
                            <li><a href="/company/admintrackingbulk"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Multi Tracking Search</a>
                            </li>
                            <li> 
                                <a href="/company/ticket/create"><span class="glyphicon glyphicon-chevron-right"></span>Create Ticket</a>
                            </li>
                            <li> 
                                <a href="/company/ticket/export"><span class="glyphicon glyphicon-chevron-right"></span>Tickets Export</a>
                            </li>
                            <li> 
                                <a href="/company/kpiReport"><span class="glyphicon glyphicon-chevron-right"></span>KPI Report</a>
                            </li>
                            <li><a href="/company/orderimportxml"><span class="glyphicon glyphicon-chevron-right"></span>
                                    Data Import</a></li>
                            <li><a href="/company/companydetail"><span class="glyphicon glyphicon-chevron-right"></span>
                                    Company Profile</a></li>
                                    @if($companydata->company_type == 1)
                                <li><a href="/company/subcompanies"><span class="glyphicon glyphicon-chevron-right"></span>Manage Merchants</a></li>
                            @endif
                            <li><a href="/company/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a>
                            <li><a href="https://www.saee.sa/blackbox/login" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Black Box</a></li>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</div>

<!--header end-->
<div class="col-md-12 body-con">
    <div class="col-md-2 column margintop20 fix rs-none">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="/company/dashboard">
                    Welcome,<br> <?php echo $companydata->company_name; ?></a>
            </li>
            <li><a href="/company/shipments/all"><span class="glyphicon glyphicon-chevron-right"></span>All
                    Shipments</a></li>
            <?php $is_api = Session::get('is_api');
            if ($is_api == 0 || $companydata->company_type == 1) {
                echo "<li><a href=\"/company/generateshipment\"><span class=\"glyphicon glyphicon-chevron-right\"></span>Generate Shipment</a></li>";
            }?>
            <li><a href="/company/shipmentsExport"><span class="glyphicon glyphicon-chevron-right"></span>Export Shipments</a>
            </li>
            <li><a href="/company/sameDaySuccessRate"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Same Day Success Rate Report</a></li>
            <li><a href="/company/admintracking"><span class="glyphicon glyphicon-chevron-right"></span>Single Tracking
                    Search</a></li>
            <li><a href="/company/admintrackingbulk"><span class="glyphicon glyphicon-chevron-right"></span>Multi
                    Tracking Search</a></li>
            <li> 
                    <a href="/company/ticket/create"><span class="glyphicon glyphicon-chevron-right"></span>Create Ticket</a>
            </li>
            <li> 
                <a href="/company/ticket/export"><span class="glyphicon glyphicon-chevron-right"></span>Tickets Export</a>
            </li>
            <li> 
                <a href="/company/kpiReport"><span class="glyphicon glyphicon-chevron-right"></span>KPI Report</a>
            </li>
            <li><a href="/company/orderimportxml"><span class="glyphicon glyphicon-chevron-right"></span>
                    Data Import</a></li>        
            <li><a href="/company/companydetail"><span class="glyphicon glyphicon-chevron-right"></span> Company Profile</a>
            </li>
            @if($companydata->company_type == 1)
                <li><a href="/company/subcompanies"><span class="glyphicon glyphicon-chevron-right"></span>Manage Merchants</a></li>
            @endif
            <li><a href="/company/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a></li>
            <li><a href="https://www.saee.sa/blackbox/login" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Black Box</a></li>
        </ul>
    </div>


    <div class="col-md-10">
        @yield('shipmentsExport')
        @yield('content')
        @yield('trackingbulk')
        @yield('tracking')
    </div>
</div>

<!--footer start-->
<div class="container">

    <div class="footertext1">? All Rights Reserved Saee</div>
</div>


<!--footer end-->


<!-- jQuery k-w-h.com/app/views/warehouse/vendors -->


<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

<!-- Skycons -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Datatables -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/vfs_fonts.js"></script>
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
<script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
<!-- Custom Theme Script s-->
<script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/custom.js"></script>

</div>
</body>
</html>
 
    
