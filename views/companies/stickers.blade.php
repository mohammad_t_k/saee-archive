<table style="width:400px;border: 1px solid black;">
    <tr>
        <td style="border: 1px solid black;"><img src="../public/sticker/logo.jpg"></td>
        <td colspan="3" style="border: 1px solid black;"><?php echo $barcode ?><br><p style="text-align: center"><?php echo $packagedetails->waybill ?></p></td>
    </tr>
    <tr>
        <td style="border: 1px solid black;">
            <p>Destination</p>
            <h4><b>ABT</b></h4>
        </td>
        <td colspan="2" style="border: 1px solid black;">
            <p>Date</p>
            <h4><b><?php echo  $packagedetails->updated_at ?></b></h4>
        </td>
        <td colspan="1" rowspan="2" style="border: 1px solid black;" ><?php echo $cityimg ?></td>
    </tr>
    <tr>
        <td style="border: 1px solid black;">
            <h4><b>DOM</b></h4>
        </td>
        <td style="border: 1px solid black;">
            <h4><b>ONP</b></h4>
        </td>
        <td style="border: 1px solid black;">
            <h4><b>AC</b></h4>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="border: 1px solid black;">
            <h4><b>Pieces : </b><?php echo $packagedetails->d_quantity ?></h4><br>
            <h4><b>Weight : </b><?php echo $packagedetails->d_weight ?></h4>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 1px solid black;">
            <h4><?php echo $packagedetails->company_name ?></h4><br>

            <p><?php echo $packagedetails->address ?></p>

            <p><b><?php echo $packagedetails->city ?></b></p>

            <p style="text-align: left"><b><?php echo $packagedetails->country?></b></p>

            <p style="text-align: right"><?php echo $packagedetails->phone?></p>
        </td>
        <td rowspan="2" style="border: 1px solid black;">
            <?php echo $barcode90 ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 1px solid black;">
            <h4><?php echo $packagedetails->receiver_name ?></h4><br>

            <p><?php echo $packagedetails->d_address?></p>

            <p><b><?php echo $packagedetails->d_city?></b></p>

            <p><b><?php echo $packagedetails->d_district?></b></p>

            <p><b><span><?php echo $packagedetails->receiver_phone?></span></b></p>
        </td>
    </tr>
</table>
