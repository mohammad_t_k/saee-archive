@extends('companies.layout')

@section('dashboard')
    <div class="row ">
        <form method="get" action="/company/dashboard">
            <div class="col-md-5 col-sm-12 col-xs-12 form-group ">
                <label class="control-label colm col-md-2 col-sm-6 col-xs-12" for="sel1">City *</label>

                <div class="col-md-8 col-sm-6 col-xs-12">
                    <select name="city" id="city" class="form-control city1 ">
                        @foreach($adminCities as $adcity)
                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12  fil-css ">
                <button id="submitForm" class="book-btn btn-warning">FILTER</button>
            </div>

        </form>
    </div>
    
    <div class="">
        <div class="row">
            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Total Packages
                <h3><?php echo $allitems ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Total COD Packages
                <h3><?php echo $allitemscod ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Total Prepaid Packages
                <h3><?php echo $allitemsprepaid ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-user" aria-hidden="true"></i> Returned to Supplier
                <h3><?php echo $returnedtojcitems ?></h3>
        
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 package">
        
                <i class="fa fa-home" aria-hidden="true"></i> Created by Supplier
                <h3><?php echo $penddingitems ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-truck" aria-hidden="true"></i> In Route
                <h3><?php echo $onwayitems ?></h3>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-home" aria-hidden="true"></i> In Warehouse
                <h4><?php echo $arriveditems; ?></h4>
                <p>Returned <?php echo $returneditems; ?></p>
                <p>New <?php echo $newitems; ?></p>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-car" aria-hidden="true"></i> With Captains
                <h3><?php echo $outfordeliveryitems + $undelivreditems ?></h3>
        
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-3 package">
        
                <i class="fa fa-truck" aria-hidden="true"></i> Delivered
                <h4><?php echo $delivreditems ?></h4>
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-money" aria-hidden="true"></i> Cash Collected
                <h4><?php echo number_format($cashcollected, 2) ?></h4>
                <?php
                $res = 0;
                if($cashcollected > 0){
                    $res = $alreadyreansfared / $cashcollected * 100;
                    $res = number_format($res, 2);
                }
                ?>
                @if($company != 911)
                    <div><?php echo "Already Transferred " . number_format($alreadyreansfared, 2) . ' (' . $res . '%)';?></div>
                @endif
        
            </div>
        
            <div class="col-md-3 package">
        
                <i class="fa fa-money" aria-hidden="true"></i> Cash to be Collected
                <h5><?php echo number_format($cashtobecollected, 2) ?></h5>
        
            </div>
        </div>
    
    </div>

    <br/>

    <div class="clearF"></div>

    <hr/>

    
    <br/>

    @if($allitems != 0)
        <div class="row">
            <!-- bar chart -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Districts
                            <small>Histogram</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="districtschart" style="width:100%; height:280px;"></div>
                    </div>
                </div>
            </div>
            <!-- /bar charts -->
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12" style="margin-top: 34px;">

                <div class="x_panel tile fixed_height_580 overflow_hidden">
                    <div class="x_title">
                        <h2>Summary</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="pie">

                            <tr>
                                <td>
                                    <div class="chart-container" style="position: center; ">
                                        <canvas height="350" width="350" style="margin: 15px 10px 10px 0"
                                                id="homepagepiechart2"></canvas>
                                    </div>

                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
@section('dashboard-script')

    <?php

    $mydata_histogram = '';

    foreach ($merged as $order) {

        $mydata_histogram .= "{ district: \"$order->d_district\", all:\"$order->allitems\", delivered:\"$order->delivereditems\" }, ";

    }


    ?>



    <script>


        $(document).ready(function () {


            Morris.Bar({
                element: 'districtschart',
                data: [
                    <?php echo $mydata_histogram ?>
                  ],
                xkey: 'district',
                ykeys: ['all', 'delivered'],
                labels: ['All Items', 'Delivered Items'],
                barRatio: 0.6,
                barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                xLabelAngle: 75,
                hideHover: 'auto',
                resize: true
            });
            var totalitems = ' <?php echo $allitems ?>';
            if (totalitems > 0) {

                $('#homepagepiechart2').each(function () {


                    var chart_element = $(this);
                    var chart_doughnut = new Chart($(this), {
                        type: "doughnut",
                        tooltipFillColor: "rgba(51, 51, 51, 0.55)",


                        data: {
                            labels: ["Created by Supplier",
                                "In Route",
                                "In Warehouse",
                                "With Captains",
                                "Delivered",
                                "Returned to Supplier"],
                            datasets: [{
                                data: [
                                    <?php if($allitems==0)
                                            $allitems = 1;
                                      echo number_format(100.0*$penddingitems/$allitems,2) .
                                    ','. number_format(100.0*$onwayitems/$allitems,2) .
                                    ','. number_format(100.0*$arriveditems/$allitems,2) .
                                    ','. number_format(100.0*($outfordeliveryitems+$undelivreditems)/$allitems,2) .
                                    ','. number_format(100.0*$delivreditems/$allitems,2) .
                                    ','. number_format(100.0*$returnedtojcitems/$allitems,2) ;?>],
                                backgroundColor: [
                                    "#BDC3C7",
                                    "#9B59B6",
                                    "#037f3b",
                                    "#26B99A",
                                    "#3498DB",
                                    "#E74C3C"
                                ],
                                hoverBackgroundColor: [
                                    "#CFD4D8",
                                    "#B370CF",
                                    "#359862",
                                    "#36CAAB",
                                    "#49A9EA",
                                    "#eb6f62"
                                ]
                            }],
                            options: {
                                legend: true,
                                responsive: true
                            }
                        }

                    });

                });
            }

            var theme = {
                color: [
                    '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
                    '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
                ],

                title: {
                    itemGap: 8,
                    textStyle: {
                        fontWeight: 'normal',
                        color: '#408829'
                    }
                },

                dataRange: {
                    color: ['#1f610a', '#97b58d']
                },

                toolbox: {
                    color: ['#408829', '#408829', '#408829', '#408829']
                },

                tooltip: {
                    backgroundColor: 'rgba(0,0,0,0.5)',
                    axisPointer: {
                        type: 'line',
                        lineStyle: {
                            color: '#408829',
                            type: 'dashed'
                        },
                        crossStyle: {
                            color: '#408829'
                        },
                        shadowStyle: {
                            color: 'rgba(200,200,200,0.3)'
                        }
                    }
                },

                dataZoom: {
                    dataBackgroundColor: '#eee',
                    fillerColor: 'rgba(64,136,41,0.2)',
                    handleColor: '#408829'
                },
                grid: {
                    borderWidth: 0
                },

                categoryAxis: {
                    axisLine: {
                        lineStyle: {
                            color: '#408829'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    }
                },

                valueAxis: {
                    axisLine: {
                        lineStyle: {
                            color: '#408829'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    }
                },
                timeline: {
                    lineStyle: {
                        color: '#408829'
                    },
                    controlStyle: {
                        normal: {color: '#408829'},
                        emphasis: {color: '#408829'}
                    }
                },

                k: {
                    itemStyle: {
                        normal: {
                            color: '#68a54a',
                            color0: '#a9cba2',
                            lineStyle: {
                                width: 1,
                                color: '#408829',
                                color0: '#86b379'
                            }
                        }
                    }
                },
                map: {
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                color: '#ddd'
                            },
                            label: {
                                textStyle: {
                                    color: '#c12e34'
                                }
                            }
                        },
                        emphasis: {
                            areaStyle: {
                                color: '#99d2dd'
                            },
                            label: {
                                textStyle: {
                                    color: '#c12e34'
                                }
                            }
                        }
                    }
                },
                force: {
                    itemStyle: {
                        normal: {
                            linkStyle: {
                                strokeColor: '#408829'
                            }
                        }
                    }
                },
                chord: {
                    padding: 4,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                color: 'rgba(128, 128, 128, 0.5)'
                            },
                            chordStyle: {
                                lineStyle: {
                                    width: 1,
                                    color: 'rgba(128, 128, 128, 0.5)'
                                }
                            }
                        },
                        emphasis: {
                            lineStyle: {
                                width: 1,
                                color: 'rgba(128, 128, 128, 0.5)'
                            },
                            chordStyle: {
                                lineStyle: {
                                    width: 1,
                                    color: 'rgba(128, 128, 128, 0.5)'
                                }
                            }
                        }
                    }
                },
                gauge: {
                    startAngle: 225,
                    endAngle: -45,
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                            width: 8
                        }
                    },
                    axisTick: {
                        splitNumber: 10,
                        length: 12,
                        lineStyle: {
                            color: 'auto'
                        }
                    },
                    axisLabel: {
                        textStyle: {
                            color: 'auto'
                        }
                    },
                    splitLine: {
                        length: 18,
                        lineStyle: {
                            color: 'auto'
                        }
                    },
                    pointer: {
                        length: '90%',
                        color: 'auto'
                    },
                    title: {
                        textStyle: {
                            color: '#333'
                        }
                    },
                    detail: {
                        textStyle: {
                            color: 'auto'
                        }
                    }
                },
                textStyle: {
                    fontFamily: 'Arial, Verdana, sans-serif'
                }
            };


            /*$('#homepagepiechart').each(function(){

             var echartPieCollapse = echarts.init(document.getElementById('homepagepiechart'), theme);

             echartPieCollapse.setOption({
             tooltip: {
             trigger: 'item',
             formatter: "{a} <br/>{b} : {c} ({d}%)"
             },
             legend: {
             x: 'center',
             y: 'bottom',
             data: ['In Riyadh', 'Picked up from Riyadh', 'Kasper Warehouse', 'With captains', 'Delivered', 'Returned to JC']
             },
             toolbox: {
             show: true,
             feature: {
             magicType: {
             show: true,
             type: ['pie', 'funnel']
             },
             restore: {
             show: true,
             title: "Restore"
             },
             saveAsImage: {
             show: true,
             title: "Save Image"
             }
             }
             },
             calculable: true,
             series: [{
             name: 'Status',
             type: 'pie',
             radius: [10, 150],
             center: ['50%', 170],
             roseType: 'area',
             x: '50%',
             max: 100,
             sort: 'ascending',
             data: [{
             value: < ?php //echo number_format(100.0*$penddingitems/$allitems,2) ?>,
             name: 'In Riyadh'
             }, {
             value: < ?php //echo number_format(100.0*$onwayitems/$allitems,2) ?>,
             name: 'Picked up from Riyadh'
             }, {
             value: < ?php //echo number_format(100.0*$arriveditems/$allitems,2) ?>,
             name: 'Kasper Warehouse'
             }, {
             value: < ?php //echo number_format(100.0*($outfordeliveryitems+$undelivreditems)/$allitems,2) ?>,
             name: 'With captains'
             }, {
             value: < ?php //echo number_format(100.0*$delivreditems/$allitems,2) ?>,
             name: 'Delivered'
             }, {
             value: < ?php// echo number_format(100.0*$returnedtojcitems/$allitems,2) ?>,
             name: 'Returned to JC'
             }]
             }]
             });

             });  */


        });

    </script>

@stop