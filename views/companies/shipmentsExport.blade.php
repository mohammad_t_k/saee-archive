
@extends('companies.exlayout')



@section('shipmentsExport')

        <!-- page content -->
<style type="text/css">
    input[type="checkbox"] {
    display: block;
}
</style>
<!-- page content -->
<div class="" role="main">
    <div class="">

      <!--  <div class="page-title">
            <div class="title_left">
                <h3>Export Shipments</h3>
            </div>
        </div>
       -->

        <div class="clearfix"></div>

        @if(Session::has('success'))
        <div class="alert-box success">
        <h2>{{ Session::get('success') }}</h2>
        </div>
        @endif

        <!-- /top tiles -->

         <div class="row">

            <div class="tab_container">
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1" class='label2'><i
                            class="fa fa-car"></i><span>Export Specific Shipments</span></label>

<!---
                <input id="tab3" type="radio" name="tabs" <?php if ($tabFilter == 2) echo 'checked' ?>>
                <label for="tab3" class='label2'><i
                            class="fa fa-car"></i><span>Export Single Shipment</span></label>
-->

                <input id="tab2" type="radio" name="tabs" <?php if ($tabFilter == 3) echo 'checked' ?>>
                <label for="tab2" class='label2'><i
                            class="fa fa-pencil-square-o"></i><span> Export Shipments </span></label>

                <section id="content1" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Export Shipments by Waybill</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="multiple-ids" action="/company/shipmentsIdsExport" method="post">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="false">

                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment
                                                    IDs <span class="required">*</span>
                                                </label>

                                                <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <textarea  id="shipmentIDs" name="ids" required
                                                                   class="form-control" type="text" rows="10"></textarea>

                                                </div>
                                                <div class="theCount">(Max 100) Lines used: <span id="linesUsed">0</span><div>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                            <p>
                                                                <button id="submitForm" class="btn  btn-warning">Submit
                                                                </button>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                        
                                        <div class="clearfix"></div>
                                        <div style="margin-top: 20px;"></div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 col-md-offset-4" style="display: inline-block; margin-right: 20px;" >
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <button type="button" id="copy_waybills_bulk" class="btn btn-warning">Copy All Waybills</button>
                                            </div>
                                        </div>
                                        <div style="display:inline-block;" class="col-md-2 col-sm-offset-1">
                                            <form action="/company/stickers" method="post" target="_blank">
                                                <input type="hidden" id="zero_cod_summary_form" name="zero_cod" value="<?php echo $companydata->id == 919 ? 1 : 0; ?>" />
                                                <select name="waybills[]" id="waybills" style="display: none;" multiple>
                                                    @foreach($specificorders as $order)
                                                        <option value="{{ $order->jollychic }}" selected>{{ $order->jollychic }}</option>
                                                    @endforeach
                                                </select>

                                                <p>
                                                    <button class="btn  btn-warning">Print All Waybills</button>
                                                </p>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div style="margin-bottom: 20px;"></div>
                                        
                                        @if($companydata->id == 919)
                                        <div style="clear:both; margin-top: 2%; margin-bottom: 1%;"></div>

                                        <div style="display:inline-block;" class="col-md-6 col-md-offset-3">
                                            <label>
                                                <input type="checkbox" id="zero_cod_summary" name="zero_cod_summary" value="1" class="checkbox-inline form-check" 
                                                onclick="zero_cod_summary_work()" checked />
                                                Zero COD
                                            </label>
                                        </div>
                                        <script>
                                            function zero_cod_summary_work() {
                                                document.getElementById('zero_cod_summary_form').value = document.getElementById('zero_cod_summary').checked ? '1' : '0';
                                            }
                                        </script>
                                        @endif
                                         <p><button id="deletesingleselected">Delete Selected</button></p>
                                        <table id="datatable_summary" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Waybill</th>
                                                <th>Company Name</th>
                                                <th>Order No</th>
                                                @if($companydata->company_type == 1)
                                                    <th>Merchant</th>
                                                @endif
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                @if($companydata->id == 911)
                                                    <th>Reason Code</th>
                                                @endif
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                @if($companydata->id == 919)
                                                    <th>COD Currency</th>
                                                    <th>Customs Value Currency</th>
                                                @endif
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Sign in Date</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>Delivery Date</th>
                                                <th>Return To Owner Date</th>
                                                <th>Delete</th>
                                                <th>Details</th>

                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Waybill</th>
                                                <th>Company Name</th>
                                                <th>Order No</th>
                                                @if($companydata->company_type == 1)
                                                    <th>Merchant</th>
                                                @endif
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                @if($companydata->id == 911)
                                                    <th>Reason Code</th>
                                                @endif
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                @if($companydata->id == 919)
                                                    <th>COD Currency</th>
                                                    <th>Customs Value Currency</th>
                                                @endif
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Sign in Date</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>Delivery Date</th>
                                                <th>Return To Owner Date</th>
                                                <th>Delete</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>


                <section id="content3" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="row">
                            <!-- table start -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Export Shipment by Waybill</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form action="/company/shipmentsIdExport" method="post">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="2">

                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment
                                                    ID <span class="required">*</span>
                                                </label>

                                                <div class="col-md-2 col-sm-6 col-xs-12">
                                                    <input id="shipmentID" name="id" required
                                                           class="form-control" type="text" rows="10"></input>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                    <p>
                                                        <button id="submitForm" class="btn btn-success">Submit
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </form>

                                        <table id="datatable_single" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Delivery Date</th>
                                                <th>Details</th>

                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th>Waybill</th>
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Delivery Date</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>


                <section id="content2" class="tab-content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">

                                        <form method="get" action="/company/shipmentsExport">
                                            <input id="tabFilter" name='tabFilter' type="hidden" value="3">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                                            class="required">*</span>
                                                </label>

                                                <div class="col-md-8 col-sm-6  col-xs-12 input-css">
                                                    <select name="city" id="city" class="form-control">
                                                        @foreach($adminCities as $adcity)
                                                            <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Status <span
                                                            class="required">*</span>
                                                </label>

                                                <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="all">All</option>
                                                        <option value="0"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                                            Created by Supplier
                                                        </option>
                                                        <option value="2"<?php if (isset($status) && $status == '2') echo 'selected';?> >
                                                            In Route
                                                        </option>
                                                        <option value="3"<?php if (isset($status) && $status == '3') echo 'selected';?> >
                                                            In Warehouse
                                                        </option>
                                                        <option value="4"<?php if (isset($status) && $status == '4') echo 'selected';?> >
                                                            With Captains
                                                        </option>
                                                        <option value="5"<?php if (isset($status) && $status == '5') echo 'selected';?> >
                                                            Delivered
                                                        </option>
                                                        <option value="6"<?php if (isset($status) && $status == '6') echo 'selected';?> >
                                                            UnDelivered
                                                        </option>
                                                        <option value="7"<?php if (isset($status) && $status == '7') echo 'selected';?> >
                                                            Returned to Supplier
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Shipment
                                                    Date
                                                </label>

                                                <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                    <select name="shipment_date" id="shipment_date"
                                                            class="form-control">
                                                        <option value="all">All</option>
                                                        <?php

                                                        // $shipmentdates = DeliveryRequest::distinct()->where('company_id', '=', $company)->orderBy('scheduled_shipment_date', 'desc')->get(['scheduled_shipment_date']);

                                                        // $total_num_of_shipments = count($shipmentdates);

                                                        // foreach ($shipmentdates as $ashipmentdates) {
                                                        //     if ($ashipmentdates->scheduled_shipment_date != null) {
                                                        //         $selected = '';
                                                        //         $date = $ashipmentdates->scheduled_shipment_date;
                                                        //         if (isset($shipmentDate) && $shipmentDate == $date)
                                                        //             $selected = 'selected';
                                                        //         echo "<option value='$date' $selected>$date</option>";
                                                        //     }
                                                        // }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Start Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="start_date" name="start_date" value="{{$start_date}}"
                                       class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">End Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                       value="{{$end_date}}"
                                       type="date">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Number Of Delivery Attempts
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="num_delivery_attempts" name="num_delivery_attempts" class="date-picker form-control col-md-7 col-xs-12"
                                       value="{{ isset($num_delivery_attempts) ? $num_delivery_attempts : '2' }}"
                                       type="number">
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <hr>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Date Type</label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="date_type" id="date_type" class="form-control col-xs-12">
                                    <option value=""></option>
                                    <option value="created_at" {{ isset($date_type) && $date_type == 'created_at' ? 'selected' : '' }}>Creation Date</option>
                                    <option value="new_shipment_date" {{ isset($date_type) && $date_type == 'new_shipment_date' ? 'selected' : '' }}>Pick-up Date</option>
                                    <option value="dropoff_timestamp" {{ isset($date_type) && $date_type == 'dropoff_timestamp' ? 'selected' : '' }}>Delivery Date</option>
                                    <option value="return_to_supplier_date" {{ isset($date_type) && $date_type == 'return_to_supplier_date' ? 'selected' : '' }}>RTO Date</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">From</label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input type="date" name="datetype_from" id="datetype_from" value="{{ isset($datetype_from) ? $datetype_from : '' }}" class="form-control col-xs-12">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">To</label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input type="date" name="datetype_to" id="datetype_to" value="{{ isset($datetype_to) ? $datetype_to : '' }}" class="form-control col-xs-12">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr>
                        <br>

                        @if($companydata->company_type == 1)
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-4 col-sm-6 col-xs-12">Merchant
                                </label>

                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <select name="subcompany" id="subcompany" class="form-control">
                                        <option value="all">All</option>
                                        <option value="{{ $companydata->id }}" {{ isset($subcompany) && $companydata->id == $subcompany ? 'selected' : '' }}>Main Company</option>
                                        @foreach($subcompanies as $subcomp)
                                            <option value="{{ $subcomp->id }}" {{ isset($subcompany) && $subcomp->id == $subcompany ? 'selected' : '' }}>{{ $subcomp->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 btn-to form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                                    <p>
                                                        <button id="submitForm" class="btn btn-warning">Filter
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>

                                        </form>
                                        
                                        <div class="clearfix"></div>
                                        <div style="margin-top: 20px;"></div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 col-md-offset-4" style="display: inline-block; margin-right: 20px;" >
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <button type="button" id="copy_waybills" class="btn btn-warning">Copy All Waybills</button>
                                            </div>
                                        </div>
                                        
                                        <div style="display:inline-block;" class="col-md-2 col-sm-offset-1">
                                            <form action="/company/stickers" method="post" target="_blank">
                                                <input type="hidden" id="zero_cod_filtered_form" name="zero_cod" value="<?php echo $companydata->id == 919 ? 1 : 0; ?>" />
                                                <select name="waybills[]" id="waybills" style="display: none;" multiple>
                                                    @foreach($orders as $order)
                                                        <option value="{{ $order->jollychic }}" selected>{{ $order->jollychic }}</option>
                                                    @endforeach
                                                </select>

                                                <p>
                                                    <button class="btn  btn-warning">Print All Waybills</button>
                                                </p>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div style="margin-bottom: 20px;"></div>
                                        
                                        @if($companydata->id == 919)
                                        <div style="clear:both; margin-top: 2%; margin-bottom: 1%;"></div>
                                        <div style="display:inline-block;" class="col-md-6 col-md-offset-3">
                                            <label>
                                                <input type="checkbox" id="zero_cod_filtered" name="zero_cod_filtered" value="1" class="checkbox-inline form-check" 
                                                    onclick="zero_cod_filtered_work()" checked />
                                                Zero COD
                                            </label>
                                        </div>
                                        <script>
                                            function zero_cod_filtered_work() {
                                                document.getElementById('zero_cod_filtered_form').value = document.getElementById('zero_cod_filtered').checked ? '1' : '0';
                                            }
                                        </script>
                                        @endif
                                        <p><button id="deleteselected">Delete Selected</button></p>
                                        <table id="datatable_filtered" class="table table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th></th> <!--empty for checkbox -->
                                                <th>Waybill</th>
                                                <th>Company Name</th>
                                                <th>Order No</th>
                                                @if($companydata->company_type == 1)
                                                    <th>Merchant</th>
                                                @endif
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                @if($companydata->id == 911)
                                                    <th>Reason Code</th>
                                                @endif
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                @if($companydata->id == 919)
                                                    <th>COD Currency</th>
                                                    <th>Customs Value Currency</th>
                                                @endif
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Sign in Date</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>Delivery Date</th>
                                                <th>Return To Owner Date</th>
                                                <th>Delete</th>
                                                <th>Details</th>

                                            </tr>
                                            </thead>


                                            <tfoot>
                                            <tr>
                                                <th></th> <!--empty for checkbox -->
                                                <th>Waybill</th>
                                                <th>Company Name</th>
                                                <th>Order No</th>
                                                @if($companydata->company_type == 1)
                                                    <th>Merchant</th>
                                                @endif
                                                <th>Main City</th>
                                                <th>Sub City</th>
                                                <th>Delivery Attempts</th>
                                                <th>Reason</th>
                                                @if($companydata->id == 911)
                                                    <th>Reason Code</th>
                                                @endif
                                                <th>Name</th>
                                                <th>Mobile1</th>
                                                <th>Mobile2</th>
                                                <th>Street Addrs</th>
                                                <th>District</th>
                                                <th>COD</th>
                                                @if($companydata->id == 919)
                                                    <th>COD Currency</th>
                                                    <th>Customs Value Currency</th>
                                                @endif
                                                <th>Pincode</th>
                                                <th>Captain</th>
                                                <th>Status</th>
                                                <th>Pickup Date</th>
                                                <th>Sign in Date</th>
                                                <th>Scheduled Shipment Date</th>
                                                <th>Delivery Date</th>
                                                <th>Return To Owner Date</th>
                                                <th>Delete</th>
                                                <th>Details</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->





<!-- jstables script

<script src="/Dev-Server-Resources/tableassets/jquery.js">

</script>-->

<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


<!-- iCheck -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
      rel="stylesheet"/>
<script type="text/javascript"
        src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
<script>

    <?php

    

    $mydata = '';

    $singledata = '';

    foreach ($specificorders as $order) {
        $undelivered_reason = $order->undelivered_reason;
        if($undelivered_reason == 0)
            $undelivered_reason = 'Nothing';
        else
            $undelivered_reason = $UndeliveredReasonMap[$order->undelivered_reason]->english;
    
    if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
        $status = 'Warehouse/Res';
    else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
        $status = 'Warehouse/R';
    else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
        $status = 'Warehouse/New';
    else
        $status = $statusDef[$order->status];

    if($order->first_name)
    $cnf_cap = $order->first_name . ' '. $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
     else
      $cnf_cap =  'Not Set';
      $d_address = addslashes($order->d_address);
      if($companydata->id == 919){$cod=$order->cash_on_delivery/3.85;}
       else{$cod=$order->cash_on_delivery;}
       if ($order->sender_name)
            $company_name = $order->company_name.' ['.$order->sender_name.']';
            else
            $company_name = $order->company_name;

      $sticker = "<a onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>";
      $d_address = fixStringForDatatables($d_address);
      $order->receiver_name = fixStringForDatatables($order->receiver_name);
      $order->d_district = fixStringForDatatables($order->d_district);
      $mydata .= "[\"$order->jollychic\", \"$sticker\", \"$company_name\", \"$order->order_number\", \"$order->main_city\", \"$order->d_city\" , \"$order->num_faild_delivery_attempts\", \"$undelivered_reason\", ";
      if($companydata->id == 911)
        $mydata .= " \"$order->reason_code\", ";
        $mydata .= " \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$cod\",";
      if($companydata->id == 919)
        $mydata .= " \"$order->cod_currency\",\"USD\", ";
        $mydata .= " \"$order->pincode\", \" $cnf_cap\", \"$status\", \"$order->new_shipment_date\", \"$order->signin_date\", \"$order->scheduled_shipment_date\", \"$order->dropoff_timestamp\", \"$order->return_to_supplier_date\", \"<a href=\'/company/delete/$order->jollychic\'>Delete</a>\", \"<a href=\'/company/package/$order->jollychic\'>Show</a>\"], ";
     
    }

foreach ($singleorder as $order) {
    
    if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
        $status = 'Warehouse/Res';
    else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
        $status = 'Warehouse/R';
    else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
        $status = 'Warehouse/New';
    else
        $status = $statusDef[$order->status];

    if($order->first_name)
    $cnf_cap = $order->first_name . ' '. $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
     else
      $cnf_cap =  'Not Set';
      $d_address = addslashes($order->d_address);
      $d_address = fixStringForDatatables($d_address);
      $order->receiver_name = fixStringForDatatables($order->receiver_name);
      $order->d_district = fixStringForDatatables($order->d_district);
      $singledata .= "[ \"$order->jollychic\", \"$order->main_city\", \"$order->d_city\" , \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$order->cash_on_delivery\", \"$order->pincode\", \" $cnf_cap\", \"$status\", \"$order->sort_date\", \"$order->dropoff_timestamp\", \"<a href=\'/company/package/$order->jollychic\'>Show</a>\"], ";

    }


    $filter_data = '';
    foreach ($orders as $order) {
        $undelivered_reason = $order->undelivered_reason;
        if($undelivered_reason == 0)
            $undelivered_reason = 'Nothing';
        else
            $undelivered_reason = $UndeliveredReasonMap[$order->undelivered_reason]->english;
    
        if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
            $status = 'Warehouse/Res';
        else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
            $status = 'Warehouse/R';
        else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
            $status = 'Warehouse/New';
        else
            $status = $statusDef[$order->status];

        if ($order->sender_name)
            $company_name = $order->company_name.' ['.$order->sender_name.']';
            else
            $company_name = $order->company_name;
        
        if($order->first_name)
            $cnf_cap = $order->first_name . ' '. $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9);
        else
            $cnf_cap =  'Not Set';
        $d_address = addslashes($order->d_address);
        if($companydata->id == 919) {
            $cod=$order->cash_on_delivery/3.85;
        }
        else{
            $cod=$order->cash_on_delivery;
        }

        $d_address = fixStringForDatatables($d_address);
        $order->receiver_name = fixStringForDatatables($order->receiver_name);
        $order->d_district = fixStringForDatatables($order->d_district);

        $filter_data .= "[\"$order->jollychic\", \"<a onclick=opensticker(\'$order->jollychic\')>$order->jollychic</a>\", \"$company_name\", \"$order->order_number\", \"$order->main_city\" , \"$order->d_city\", \"$order->num_faild_delivery_attempts\", \"$undelivered_reason\", ";
        if($companydata->id == '911') {
            if(in_array($order->status, array(2, 3))) {
                if($order->company_id == '946')
                    $order->reason_code = 'NTC09';
                else 
                    $order->reason_code = 'NTC13';
            }
            if($status == 'Warehouse/New') {
                $order->reason_code = 'NTC08';
            }
            if($order->status == '4') {
                $order->reason_code = 'NTC06';
            }
            if($order->status == '5') {
                $order->reason_code = 'NTC07';
            }
            if($order->status == '7') {
                $order->reason_code = 'PTC06';
            }
            $filter_data .= " \"$order->reason_code\", ";
        }
        $filter_data .= " \"$order->receiver_name\",  \"$order->receiver_phone\", \"$order->receiver_phone2\", \" $d_address\", \"$order->d_district\", \"$cod\", ";
        if($companydata->id == 919)
            $filter_data .= " \"$order->cod_currency\",\"USD\", ";
        $filter_data .= " \"$order->pincode\", \" $cnf_cap\", \"$status\", \"$order->new_shipment_date\", \"$order->signin_date\", \"$order->scheduled_shipment_date\", \"$order->dropoff_timestamp\", \"$order->return_to_supplier_date\", \"<a href=\'/company/delete/$order->jollychic\'>Delete</a>\",  \"<a href=\'/company/package/$order->jollychic\'>Show</a>\"], ";
    }
    ?>

    $(document).ready(function () {

                var lines = 100;
                var linesUsed = $('#linesUsed');

                $('#shipmentIDs').keydown(function(e) {

                    newLines = $(this).val().split("\n").length;
                    linesUsed.text(newLines);

                    if(e.keyCode == 13 && newLines >= lines) {
                        linesUsed.css('color', 'red');
                        return false;
                    }
                    else {
                        linesUsed.css('color', '');
                    }
                });


               let singletable =  $("#datatable_summary").DataTable({

                    "data": [
                        <?php echo $mydata ?>

                      ],
                    "autoWidth": false,

                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,
                     'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                    'order': [[0, 'asc']]
                });

               $("#deletesingleselected").click(function () {
            
             var rows_selected = singletable.column(0).checkboxes.selected();
           
            if (confirm("Are you sure you want to delete selected items" ) == false) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/company/deletesingleselected',
                dataType: 'text',
                data: {
                    ids: rows_selected.join(','),
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    
                    var message = '0 items have been deleted';
                    if (responseObj.deletedCount > 0){
                        message = responseObj.deletedCount + '  items have been deleted successfully. ';

                    alert(message);
                    location.reload();
                }else{
                     alert(message);
                    location.reload();
                }
                } else {
                    alert(responseObj.error);
                }
            }); // Ajax Call
        });


                $("#datatable_single").DataTable({

                    "data": [
                        <?php echo $singledata; ?>

                      ],
                    "autoWidth": true,

                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,

                    'order': [[0, 'asc']]
                });


                 let table = $("#datatable_filtered").DataTable({

                    "data": [
                        <?php echo $filter_data ?>

                      ],
                    "autoWidth": false,

                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,
                   'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },

                    'order': [[1, 'asc']]
                });
 // Handle form submission event 
   $("#deleteselected").click(function () {
            
             var rows_selected = table.column(0).checkboxes.selected();
           
            if (confirm("Are you sure you want to delete selected items" ) == false) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/company/deleteselected',
                dataType: 'text',
                data: {
                    ids: rows_selected.join(','),
                },
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.success) {
                    
                    var message = '0 items have been deleted';
                    if (responseObj.deletedCount > 0){
                        message = responseObj.deletedCount + '  items have been deleted successfully. ';

                    alert(message);
                    location.reload();
                }else{
                     alert(message);
                    location.reload();
                }
                } else {
                    alert(responseObj.error);
                }
            }); // Ajax Call
        });

            });

    /*
     $("#submitForm").click(function () {
     var shipIDs = document.getElementById("shipmentIDs").value.replace(/\s/g, ",");


     if (shipIDs == '') {
     bootbox.alert('Please enter IDs!');
     return;
     }

     var ids = shipIDs.split(',');

     var wrongIDs = Array();
     var correctIDs = Array();
     var correctedIDs = Array();
     var finalArray = [];
     for (var i = 0; i < ids.length; i++) {
     if (!ids[i].replace(/\s/g, '').length) {
     continue;
     }
     if (/JC[0-9]{8}KS/.test(ids[i])) {
     correctIDs.push(ids[i]);
     }
     else {
     if (/[0-9]{8}KS/.test(ids[i])) {
     correctIDs.push('JC' + ids[i]);
     correctedIDs.push(ids[i]);
     }
     else if (/C[0-9]{8}KS/.test(ids[i])) {
     correctIDs.push('J' + ids[i]);
     correctedIDs.push(ids[i]);
     }

     wrongIDs.push(ids[i]);
     }
     }

     $.each(correctIDs, function (i, el) {
     if ($.inArray(el, finalArray) === -1) {
     finalArray.push(el);
     }
     });


     if (wrongIDs.length) {
     bootbox.alert("These items are filtered out:\n" + wrongIDs.toString());
     }

     if (correctedIDs.length) {
     bootbox.alert("These items were corrected:\n" + correctedIDs.toString());
     }


     if (finalArray.length) {
     document.getElementById("shipmentIDs").value = JSON.stringify(finalArray);
     return true;
     }
     else {
     alert('Nothing to check!');
     return false;
     //location.reload();
     }

     }
     );
     */
    function opensticker(waybill){
        var string = '/company/sticker/'+waybill;
        @if($companydata->id == 919 && count($specificorders) > 0)
            if(document.getElementById('zero_cod_summary').checked)
                string += '?zero_cod=1';
        @endif
        @if($companydata->id == 919 && count($orders) > 0)
            if(document.getElementById('zero_cod_filtered').checked)
                string += '?zero_cod=1';
        @endif
        window.open(string, 'newwindow', 'width=504px,height=597px,scrollbars=no');
        return false;
    }

    $("#copy_waybills_bulk").click(function(){
        <?php
        $waybills = "";
        foreach($specificorders as $order) {
            $waybills .= "$order->jollychic\n";
        }
        ?>
        let str = `{{ $waybills }}`;
        const el = document.createElement('textarea');
        el.value = str;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert("Copied");
    });

    $("#copy_waybills").click(function(){
        <?php
        $waybills = "";
        foreach($orders as $order) {
            $waybills .= "$order->jollychic\n";
        }
        ?>
        let str = `{{ $waybills }}`;
        const el = document.createElement('textarea');
        el.value = str;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert("Copied");
    });
    
</script>


@stop
