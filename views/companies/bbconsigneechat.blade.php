<!DOCTYPE html>
<html lang="en">
<head>
    <title>Black Box</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Black Box Chat interface" />
    <meta name="author" content="Danish Jamshed" />
    <meta name="keywords" content="xmpp chat blackbox" />
    <script
			  src="https://code.jquery.com/jquery-3.4.0.min.js"
			  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
			  crossorigin="anonymous"></script>
    <link rel="shortcut icon" type="image/ico" href="css/images/favicon.ico"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/website.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/converse.css" />
    <script src="<?php echo asset_url(); ?>/bbchat/converse/src/website.js"></script>
    <script type="text/javascript" src="<?php echo asset_url(); ?>/bbchat/converse/analytics.js"></script>
    <noscript><p><img src="//stats.opkode.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
    <![if gte IE 9]>
        <script src="<?php echo asset_url(); ?>/bbchat/converse/dist/converse.js"></script>
    <![endif]>
</head>



<body id="page-top" data-spy="scroll">

</body>



<script>

 function sortUsingNestedText(parent, childSelector, keySelector) {
    var items = parent.children(childSelector).sort(function(a, b) {
        var vA = $(keySelector, a).text();
        var vB = $(keySelector, b).text();
        return (vA > vB) ? -1 : (vA < vB) ? 1 : 0;
    });
    parent.append(items);
}

var waybill = '<?php echo $waybill;?>';
var receiver_name = '<?php echo $receiver_name;?>';
// custom logout plugin to redirect to custom page
converse.plugins.add('customLogout', {
    initialize: function () {
        var _converse = this._converse;

       _converse.api.listen.on('logout', function () {

            	window.location.replace("/blackbox/"+waybill);

          });    

        _converse.api.listen.on('message', function (messageXML) { 


setTimeout(function(){
$("div[data-from]").each(function(){
   var from = $(this).attr('data-from');
   from = from.split("/").pop();
   if(from == receiver_name){
   	$(this).children().css("background","white");
   }
   else{

   		$(this).children().css("background","#F7921E");
   }
});

}, 2000);



        });  
      
    }
});


   converse.initialize({
        authentication: 'anonymous',
        allow_registration: false,
        auto_away: 300,
        registration_domain: 'k-w-h.com',
        default_domain: 'k-w-h.com', // so on login page you don't have to specify username with domain
        auto_reconnect: true,
        bosh_service_url: 'http://www.k-w-h.com:5280/bosh/', // Please use this connection manager only for testing purposes
        message_archiving: 'always',
        play_sounds: true,
        notify_all_room_messages: true,
        sounds_path: '/bbchat/converse/sounds/',
        view_mode: 'fullscreen',
        auto_login: true,
        debug: false,
        jid: 'k-w-h.com',
        auto_join_rooms: [{'jid': waybill+'@conference.k-w-h.com', 'nick': receiver_name }],
            notify_all_room_messages: [
                waybill+'@conference.k-w-h.com',
            ],
        whitelisted_plugins: ['customLogout']
    }).then(function () {
        setTimeout(function () {
            document.querySelector('.converse-content').style = '';
        }, 1000);
    });
 

    		$(window).bind("load", function() {
  // replacing JID with receiver name top left cornor
  $( ".username" ).html( receiver_name );
  // replace upper group name // removing after @ part
  var groupName = $( ".chat-title" ).text();
  groupName = groupName.substring(0, groupName.indexOf("@"));
  $( ".chat-title" ).html( groupName );

setTimeout(function(){
$("div[data-from]").each(function(){
   var from = $(this).attr('data-from');
   from = from.split("/").pop();
   if(from == receiver_name){
   	$(this).children().css("background","white");
   }
   else{

   		$(this).children().css("background","#F7921E");
   }
});

}, 3000);



});

</script> 

</html>
