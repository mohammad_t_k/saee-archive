 @extends('companies.layout')


@section('content')

@if(Session::has('msg'))
<div class="alert alert-success"><b><?php
        echo Session::get('msg');
        Session::put('msg', NULL);
        ?></b></div>
@endif

<style type="text/css">
	
	.dropdown-menu li {

    width: 100%;

}
</style>

<h4>Users</h4>
<div class="box box-info tbl-box">
    <div align="left" id="paglink"><?php echo $users->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Email </th>
                <th>Phone</th>
                <th>Alias</th>
            </tr>

            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?= $user->id ?></td>
                    <td><?php echo $user->username; ?> </td>
                    <td><?php echo $user->email ?> </td>
                    <td><?php echo $user->phone ?> </td>
                    <td><?php echo $user->alias_id ?></td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" id="edit" href="{{ URL::Route('bBUserEdit', $user->id) }}">Edit {{ trans('customize.User'); }}</a></li>
                               
                                <li role="presentation"><a role="menuitem" tabindex="-1" id="add_req" href="{{ URL::Route('bBDeleteUser', $user->id) }}">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
<?php } ?>
        </tbody>
    </table>

    <div align="left" id="paglink"><?php echo $users->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>




</div>

<h4>Aliases</h4>

<div class="box box-info tbl-box">
    <div align="left" id="paglink"><?php echo $aliases->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <th>Alias ID</th>
                <th>Alias Name</th>
                <th>Register Date</th>
                <th>Country</th>
            </tr>

            <?php foreach ($aliases as $alias) { ?>
                <tr>
                    <td><?= $alias->alias_id ?></td>
                    <td><?php echo $alias->alias_name; ?> </td>
                    <td><?php echo $alias->created_at ?></td>
                    <td><?php echo $alias->country ?></td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" id="edit" href="{{ URL::Route('bBUserEdit', $alias->id) }}">Edit {{ trans('customize.User'); }}</a></li>
                               
                                <li role="presentation"><a role="menuitem" tabindex="-1" id="add_req" href="{{ URL::Route('bBDeleteAlias', $alias->id) }}">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
<?php } ?>
        </tbody>
    </table>

    <div align="left" id="paglink"><?php echo $aliases->appends(array('type' => Session::get('type'), 'valu' => Session::get('valu')))->links(); ?></div>




</div>




@stop