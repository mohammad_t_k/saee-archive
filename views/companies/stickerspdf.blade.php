<!DOCTYPE html>
<html style="margin: 0px; padding: 0; width:384px; height:571px;">
   
<head>
    <title>Saee</title>
    <meta charset="utf-8">
</head>
<style>
*{padding:0;
margin:0;}
div {
    page-break-after: always;
}
</style>

<body style="margin: 0px; padding: 0; width:384px; height:571px;">

@foreach($shipments as $shipment)
<div style="background-color: #fefefe; border: 0px;  solid #888; width:384px; height:571px;">
    <table cellpadding="0" cellspacing="0" font-size="10px"
           style="border: 1px solid black;border-collapse: collapse; 
           width:384px; height:571px; margin: 0px 0 0px 0px; padding: 0;">
        <tr>
            <td style="border: 1px solid black; text-align:left;"><img
                        src="/companiesdashboard/images/logo-b.png"></td>
            <td colspan="3" style="border: 1px solid black;"><br>

                <p style="padding-right:10px;text-align: right;"><?php echo $shipment->barcode ?></p>

                <p style="padding-left:3px;text-align: center"><?php echo $shipment->waybill ?></p></td>
        </tr>
        <tr>
            <td style="border: 1px solid black;">
                <p style="margin-top:5px; margin-left:5px;">Destination</p>
                <h4 style="font-size:10px; font-weight:bold; margin-left:5px;"><?php echo $shipment->destination ?></h4>
            </td>
            <td colspan="2" style="border: 1px solid black;">
                <p style="margin-left:5px; font-size:21px; font-weight:bold;">
                    COD:<br><?php echo $shipment->cod ?> SAR
                </p>
            </td>
            <td colspan="1" rowspan="2" align="left" style="border: 1px solid black;">
                <p style="font-size:14px; font-weight:bold; margin-left:10px;"><?php  echo $shipment->customer_city ?></p>
                <h1 style="font-size:24px; font-weight:bold; margin-left:5px;"><b><?php echo $shipment->destination ?></b></h1>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;">
                <p>DOM</p>
            </td>
            <td style="border: 1px solid black;">
                <p>ONP</p>
            </td>
            <td style="border: 1px solid black;">
                <p>AC</p>
            </td>

        </tr>
        <tr>
            <td colspan="4" bgcolor="#333333" style="border: 1px solid black;"><p style="color:#FFF;">Services:
                </p></td>

        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid black;"><p>Pieces<br><?php echo $shipment->pieces ?></p></td>
            <td style="border: 1px solid black;">Weight<br><?php echo $shipment->weight ?>KG</td>
            <td style="border: 1px solid black;">Date<br><?php echo $shipment->date ?> </td>

        </tr>


        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <p style="margin: 10px 0 10px 10px;">
                    Account: 6004767<br>
                    <?php echo $shipment->company_name ?>, <?php echo $shipment->company_address ?></p>

                <p style="margin: 10px 0 10px 10px; "><?php echo $shipment->company_city ?>
                    ,SA <?php echo $shipment->company_phone ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" border-collapse="collapse">
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $shipment->customer_name ?></p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $shipment->customer_address ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $shipment->customer_address2 ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $shipment->customer_district ?>
                            </p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php if (isset($main_city) && $main_city != '') {
                                    echo $main_city->city;
                                    echo " - ";
                                    echo $shipment->customer_city ;
                                } else  {echo $shipment->customer_city;} ?>
                                ,SA</p></td>
                    </tr>
                    <tr>
                        <td width="30%"><p style="margin: 0px 0 10px 10px; "><?php echo $shipment->customer_phone?></p>
                        </td>
                        <td width="30%" valign="top"><?php echo $shipment->customer_phone2?></td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td width="40%"><p style="margin: 0px 0 5px 5px; ">Shipper Ref:</p><strong
                                    style="margin-left:5px;"><?php echo $shipment->order_number ?></strong></td>
                        <td width="40%" valign="top">Consignee Ref:</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <div >{{--style="padding-left:8px;width:2cm; height:8cm;"--}}
                    <?php
                    if ($shipment->order_number != "") { ?>
                    <p style=" padding: 10px 50px;text-align: right;"><?php echo $shipment->barcode90 ?></p>
                    <p style="text-align: center"><?php echo $shipment->order_number; }?></p>{{--style="margin-top:60pt;padding:60pt;text-align: center"--}}
                </div>

            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <p style="margin: 0px 0 5px 5px; ">
                    Description: <?php echo $shipment->description?></p></td>
        </tr>
    </table>
</div>
@endforeach

</body>
</html>





