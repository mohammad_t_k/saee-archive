
@extends('companies.layout')

@section('shipments')

 <?php
    ?>

<div class="row">
<div class="page-title">
	<div class="col-md-6">
                <div class="title_left">
                    <h3><?php echo $shipmentDate ?> Shipments
                    </h3>

                </div>
    </div>

    <div class="col-md-6">
                <div class="title_right">
                       <!-- <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
                        </div> -->
                    
                </div>
            </div>
            </div>
</div>

        <div class="">

            
            <!-- top tiles -->
        <div class="col-md-3 package">

<i class="fa fa-user" aria-hidden="true"></i> Total Packages
<h3><?php echo $allitems ?></h3>

</div>

<div class="col-md-3 package">

<i class="fa fa-user" aria-hidden="true"></i> In Warehouse
<h3><?php echo $arriveditems ?></h3>

</div>

<div class="col-md-3 package">

<i class="fa fa-user" aria-hidden="true"></i> With Captains
<h3><?php echo $outfordeliveryitems ?></h3>

</div>

<div class="col-md-3 package">

<i class="fa fa-user" aria-hidden="true"></i> Delivered
<h3><?php echo $delivreditems ?></h3>

</div>

<div class="col-md-3 package">

<i class="fa fa-home" aria-hidden="true"></i> Cash Collected
<h3><?php echo number_format($cashcollected, 2) ?></h3>

</div>

<div class="col-md-3 package">

<i class="fa fa-truck" aria-hidden="true"></i> Cash to be Collected
<h3><?php echo number_format($cashtobecollected, 2) ?></h3>

</div>

            <!-- /top tiles -->
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <form method="get" action="/company/shipments/{{$shipmentDate}}">


                        <input id="shipmentDate" name="shipmentDate" class="form-control" type="hidden"
                               value="{{$shipmentDate}}">

                       <!-- <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Company <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select id="company" name="company" required="required" class="form-control"
                                        type="text">
                                    <option value="all">All Companies</option>
                                    @foreach($companies as $compan)
                                        <option value="{{$compan->id}}"<?php //if (isset($company) && $company == $compan->id) echo 'selected';?>>{{$compan->name}}</option>
                                    @endforeach
                                </select>
                            </div> 
                        </div>-->

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                               <select name="city" id="city" class="form-control">
                                    @foreach($adminCities as $adcity)
                                        <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Status <span
                                        class="required">*</span>
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="status" id="status" class="form-control">
                                    <option value="all">All</option>
                                    <option value="0"<?php if (isset($status) && $status == '0') echo 'selected';?> >
                                       Created by Supplier
                                    </option>
                                    <option value="1"<?php if (isset($status) && $status == '1') echo 'selected';?> >In
                                        Route
                                    </option>
                                    <option value="2"<?php if (isset($status) && $status == '2') echo 'selected';?> >In
                                        Warehouse
                                    </option>
                                    <option value="4"<?php if (isset($status) && $status == '4') echo 'selected';?> >
                                        With Captains
                                    </option>
                                    <option value="5"<?php if (isset($status) && $status == '5') echo 'selected';?> >
                                        Delivered
                                    </option>
                                    <option value="6"<?php if (isset($status) && $status == '6') echo 'selected';?> >
                                        UnDelivered
                                    </option>
                                    <option value="7"<?php if (isset($status) && $status == '7') echo 'selected';?> >
                                        Returned to Supplier
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Start Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="start_date" name="start_date"
                                       class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">End Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                       type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-12">Shipment Date
                            </label>

                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="shipment_date" id="shipment_date" class="form-control">
                                    <option value="all">All</option>
                                    <?php

                                    $shipmentdates = DeliveryRequest::distinct()->where('company_id', '=', $company)->orderBy('scheduled_shipment_date', 'desc')->get(['scheduled_shipment_date']);

                                    $total_num_of_shipments = count($shipmentdates);

                                    foreach ($shipmentdates as $ashipmentdates) {
                                        if ($ashipmentdates->scheduled_shipment_date != null) {
                                            $selected = '';
                                            $date = $ashipmentdates->scheduled_shipment_date;
                                            if (isset($shipmentDate) && $shipmentDate == $date)
                                                $selected = 'selected';
                                            echo "<option value='$date' $selected>$date</option>";
                                        }
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                            <p>
                                <button style="margin-bottom: 0px;" id="submitForm" class="btn-cs1 btn-cs btn-warning">Filter</button>
                            </p>
                        </div>



                    </form>

                </div>
            </div>
            <link href="<?php echo asset_url(); ?>/warehouseadmin/assets/admin/css/custom.css" rel="stylesheet">
            <hr/>
            <div class="row">
                <div class="col-xs-12">
                    <form action="/company/shipments/report" method="post">
                        <div class="col-md-4 col-xs-12">
                            <div class="col-md-3">
                                <label>From </label>
                            </div>
                            <div class="col-md-9">
                                <input type="date" name="from" id="from" class="form-control" value="{{ $from }}">
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="col-md-3">
                                <label>To </label>
                            </div>
                            <div class="col-md-9">
                                <input type="date" name="to" id="to" class="form-control" value="{{ $to }}">
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="col-md-3 text-center">
                                <button id="filterreport" class="btn btn-warning col-xs-12" style="text-align: center;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12">
                    <table class="table table-bordered table-striped" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">Date</th>
                                <th style="text-align: center;">Total Deliveries OFD</th>
                                <th style="text-align: center;">Delivered</th>
                                <th style="text-align: center;">Pending</th>
                                <th style="text-align: center;">Cancelled</th>
                                <th style="text-align: center;">Returned To Supplier</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($report as $one_record)
                                <tr>
                                    <td>{{ $one_record->new_shipment_date }}</td>
                                    <td>{{ $one_record->totalDeliveriesOFD }}</td>
                                    <td>{{ $one_record->delivered }}</td>
                                    <td>{{ $one_record->pending }}</td>
                                    <td>{{ $one_record->canceled }}</td>
                                    <td>{{ $one_record->returnToSupplier }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">

            	@section('shipments-fullwidth')
                <div style="paddint-top:0px;" class="tab_container">


                    <input id="tab1" type="radio" name="tabs" checked>
                    <label for="tab1" class='label2'><i class="fa fa-car"></i><span>All statues Shipments</span></label>


                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2" class='label2'><i
                                class="fa fa-pencil-square-o"></i><span>Dispatched </span></label>

                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3" class='label2'><i class="fa fa-bar-chart-o"></i><span>Delivered </span></label>

                    <section id="content1" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>All statues
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $orders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscount; }} off {{ $allitems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        جدول بجميع الحالات
                                    </p>

                                    <table id="datatable_summary" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>Mobile2</th>
                                            <th>Street Addrs</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Status</th>
                                            <th>Details</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($orders) > 0){ foreach ($orders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->receiver_phone ?></td>
                                            <td><?= $order->receiver_phone2 ?></td>
                                            <td><?= $order->d_address ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?= $order->pincode?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>

                                            <td><?php if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && $order->planned_walker == 0)
                                                    echo 'Warehouse/R';
                                                else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && $order->planned_walker != 0)
                                                    echo 'Warehouse/Res';
                                                else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                                                    echo 'Warehouse/New';
                                                else
                                                    echo $statusDef[$order->status];?></td>
                                            <td><a href='/company/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="12">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>Mobile2</th>
                                            <th>Street Addrs</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Status</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $orders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscount; }} off {{ $allitems; }} </p>
                            </div>
                        </div>
                    </section>

                    <section id="content2" class="tab-content">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Dispatched | مع الكباتن
                                        <small>In delivery process</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $withcaptainsorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountwithcaptain; }} off {{ $outfordeliveryitems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        Items that are out for delivery
                                    </p>

                                    <table id="datatable_inWithCaptains" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($withcaptainsorders) > 0){ foreach ($withcaptainsorders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->receiver_phone ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?= $order->pincode ?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>
                                            <td><a href='/warehouse/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="9">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>Mobile1</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Pincode</th>
                                            <th>Captain</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $withcaptainsorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountwithcaptain; }} off {{ $outfordeliveryitems; }} </p>
                            </div>
                        </div>
                    </section>

                    <section id="content3" class="tab-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Delivered
                                        <small>Shipments</small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div align="left"
                                           id="paglink"><?php echo $deliveredorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountdelivered; }} off {{ $delivreditems; }} </p>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        Items that are delivered
                                    </p>


                                    <table id="datatable_delivered" class="table table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Captain</th>
                                            <th>Delivery Date</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($deliveredorders) > 0){ foreach ($deliveredorders as $order) { ?>
                                        <tr>
                                            <td><?= $order->jollychic ?></td>
                                            <td><?= $order->d_city ?></td>
                                            <td><?= $order->receiver_name ?></td>
                                            <td><?= $order->d_district ?></td>
                                            <td><?= $order->cash_on_delivery ?></td>
                                            <td><?php if ($order->first_name) echo $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9); else echo 'Not Set';?></td>
                                            <td><?= $order->dropoff_timestamp ?></td>
                                            <td><a href='/warehouse/package/<?= $order->jollychic ?>'>Show</a></td>
                                        </tr>
                                        <?php }}else {
                                            echo '<tr><td colspan="8">No Data Found</td></tr>';
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Waybill</th>
                                            <th>City</th>
                                            <th>Name</th>
                                            <th>District</th>
                                            <th>COD</th>
                                            <th>Captain</th>
                                            <th>Delivery Date</th>
                                            <th>Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div align="left"
                                     id="paglink"><?php echo $deliveredorders->appends(array('company' => Request::get('company'),'city' => Request::get('city'),'status' => Request::get('status'),'start_date' => Request::get('start_date')
                                    ,'end_date' => Request::get('end_date'),'shipment_date' => Request::get('shipment_date')))->links(); ?></div>
                                <p>Showing 1 to {{ $orderscountdelivered; }} off {{ $delivreditems; }} </p>
                            </div>


                        </div>
                    </section>
                </div>
                @stop

            </div>

            <div class="clearfix"></div>
        </div>


@stop
