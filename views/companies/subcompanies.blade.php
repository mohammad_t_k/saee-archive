@extends('companies.layout')

@section('dashboard')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br/><br/><br/>
                <table class="table table-striped" style="width: 70%;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Merchant Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($subcompanies as $company)
                        <tr class="multi-cols">
                            <td style="width: 5%;">{{ $company->id }}</td>
                            <td style="width: 80%;">{{ $company->company_name }}</td>
                            <td style="width: 5%;">
                                <div title="Edit">
                                    <a  href="/company/subcompanies/edit/{{ $company->id }}"> <!-- edit -->
                                        <label style="float: right;"><i class="fa fa-pencil-square" style="font-size: 20px;"></i></label>
                                    </a>
                                </div>
                                <div style="clear: both;"></div>
                            </td>
                            <td style="width: 5%;">
                                <div title="Delete">
                                    <label style="float: right;"><i onclick="remove({{ $company->id }})" class="fa fa-times" style="font-size: 20px; color: red;"></i></label> <!-- remove -->
                                </div>
                                <div style="clear: both;"></div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="/company/subcompanies/add"><label class="btn btn-primary">Add Merchant</label></a>
                <style>
                    i:hover {
                        cursor: pointer;
                    }
                </style>
                <script>
                    function remove(subcompany_id){
                        if(confirm('Are you sure you want to remove this sub company?') == true) {
                            console.log('in');
                            $.ajax({
                                type: 'POST',
                                url: '/company/subcompanies/remove',
                                dataType: 'json',
                                data: {id: subcompany_id}
                            }).done(function (response) {
                                console.log(response);
                                return;
                                if(response.success)
                                    location.reload();
                                else
                                    alert('Error!');
                            });
                        }
                    }
                </script>
            </div>
        </div>
    </div>

@stop