@extends('companies.exlayout')


@section('content')
<style>
    .check {
        display: block;
    }
</style>

<!-- page content -->

<style>
    .mandatory {
        font-weight: bold;
        color: #0000CD;
    }
    .optional {
        color: red;
    }
</style>

<div class="right_col" role="main">

    <div class="container" style="margin-top: 30px;">
        <div class="row">
            <div class="col-xs-12">
                <br/>
                <h1>Import Excel Sheet Data </h1>
                <hr/>
                <br/>
                <h3 style="font-weight: bold;">Step 1: Get Template</h3>
                <br/>
                <h4>Download template from <a href="/company/orderimport/download">here</a>, it contains one record as an example.</h4>
                <br/><br/>
                <h3 style="font-weight: bold;">Step 2: Fill Data</h3>
                <br/>
                <h4>Fill excel sheet with your shipment's data according to the following legends:</h4>
                <table class="table table-striped table-bordered">
                    <thead class="text-center">
                        <tr>
                            <td colspan="5" style="color: red;"><h4><span style="font-weight: bold;">IMPORTANT NOTE:</span> Do not change columns names in the template.</h4></td>
                        </tr>
                        <tr>
                            <th class="text-center">Column Name</th>
                            <th class="text-center">Data Type</th>
                            <th class="text-center">Mandatory / Optional</th>
                            <th class="text-center">Introduction</th>
                            <th class="text-center">Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="mandatory">OrderNumber</td>
                            <td>Alphanumeric</td>
                            <td>Mandatory</td>
                            <td>Requester shipment number</td>
                            <td>SA_123</td>
                        </tr>
                        <tr>
                            <td class="mandatory">ReceiverName</td>
                            <td>String</td>
                            <td>Mandatory</td>
                            <td>Receiver Name</td>
                            <td>محمد الشقرة</td>
                        </tr>
                        <tr>
                            <td class="mandatory">ReceiverMobile</td>
                            <td>String</td>
                            <td>Mandatory</td>
                            <td>Receiver Mobile 1</td>
                            <td>966555555555</td>
                        </tr>
                        <tr>
                            <td class="optional">ReceiverMobile2</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Receiver Mobile 2</td>
                            <td>966444444444</td>
                        </tr>
                        <tr>
                            <td class="mandatory">City</td>
                            <td>String</td>
                            <td>Mandatory</td>
                            <td>Receiver City</td>
                            <td>Jeddah</td>
                        </tr>
                        <tr>
                            <td class="optional">ReceiverDistrict</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Receiver District</td>
                            <td>Al Khalidiyah</td>
                        </tr>
                        <tr>
                            <td class="mandatory">ReceiverAddress</td>
                            <td>String</td>
                            <td>Mandatory</td>
                            <td>Receiver Full Address</td>
                            <td>Nur خلف تموينات النور الشرقية  / طريق ابو حدرية</td>
                        </tr>
                        <tr>
                            <td class="optional">ReceiverAddress2</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Receiver Address 2</td>
                            <td>Faysalia ابو بكر </td>
                        </tr>
                        <tr>
                            <td class="optional">LatLong</td>
                            <td>Two Numbers, Comma Separated</td>
                            <td>Optional</td>
                            <td>Receiver latitude & longitude</td>
                            <td>21.23,32.32</td>
                        </tr>
                        <tr>
                            <td class="optional">ReceiverEmail</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Receiver Email</td>
                            <td>example@gmail.com</td>
                        </tr>
                        <tr>
                            <td class="mandatory">CODAmount</td>
                            <td>Number</td>
                            <td>Mandatory</td>
                            <td>Cash On Delivery (Value 0 Denotes No COD)</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td class="optional">Currency</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Currency (SAR or USD, Default: SAR)</td>
                            <td>SAR</td>
                        </tr>
                        <tr>
                            <td class="mandatory">Weight</td>
                            <td>Number</td>
                            <td>Mandatory</td>
                            <td>Shimpent Weight</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td class="mandatory">Quantity</td>
                            <td>Number</td>
                            <td>Mandatory</td>
                            <td>Shimpent Quantity</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td class="mandatory">Description</td>
                            <td>String</td>
                            <td>Mandatory</td>
                            <td>Shipment Description</td>
                            <td>Testing Import</td>
                        </tr>
                        <tr>
                            <td class="optional">SenderName</td>
                            <td>String</td>
                            <td>Optional</td>
                            <td>Sender Name</td>
                            <td>Test</td>
                        </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h3 style="font-weight: bold;">Step 3: Save as XML</h3>
                <br/>
                <h4>Choose <span style="font-weight: bold;">Save as</span> from excel, then choose <span style="font-weight: bold;">XML Spreadsheet 2003 (*xml)</span>, then choose <span style="font-weight: bold;">Save</span>.</h4>
                <br/>
                <div class="text-center">
                    <img src="/instructions/save_as_xml.png" />
                </div>
                <br/><br/>
                <h4>On this dialog, choose <span style="font-weight: bold;">Yes</span>.</h4>
                <br/>
                <div class="text-center">
                    <img src="/instructions/save_as_xml_confirm.png" />
                </div>

                <br/><br/>
                <h3 style="font-weight: bold;">Step 4: Upload XML File</h3>
                <br/>
                <form action="/company/xmlorderimportinDB" method="post" enctype="multipart/form-data">
                    <input type="file" name="file">
                    <br/>
                    <input type="submit" value="Upload File" name="submit" class="btn btn-success check">
                </form>
            </div>
        </div>
    </div>
    <br/>

    <h2 style="color: #990000">
        <?php
        if (!empty($success)) {

            echo $success;
        }
        ?></h2>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group"
         style="<?php if (count($successstatistics) == 0) echo 'display:none'; else echo 'display:block'; ?>;margin-top: 30px;">
        <table id="datatablenr" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Number of Shipments Imported</th>
                <th>Number of Shipments Not Imported</th>
                <th>Amount Imported</th>
                <th>Amount Not Imported</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($successstatistics as $item) { ?>
                <tr>
                    <td><?= $item['successful_entries'] ?></td>
                    <td><?= $item['unsuccessful_entries'] ?></td>
                    <td><?= $item['amountImported'] ?></td>
                    <td><?= $item['amountnotImported'] ?></td>
                </tr>
            <?php } ?>
            </tbody>

            <tfoot>
            <tr>
                <th>Number of Shipments Imported</th>
                <th>Number of Shipments Not Imported</th>
                <th>Amount Imported</th>
                <th>Amount Not Imported</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 form-group"
         style="<?php if (count($duplicateOrders) == 0) echo 'display:none'; else echo 'display:block'; ?>;margin-top: 30px;">
        <div class="x_panel">
            <h2>Following records are not created due to duplicate order number</h2>
            <table id="datatablenr" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Receiver Name</th>
                    <th>Receiver Mobile</th>
                    <th>Receiver City</th>
                    <th>COD</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($duplicateOrders as $item) { ?>
                    <tr>
                        <td><?= $item['order_number'] ?></td>
                        <td><?= $item['customer_name'] ?></td>
                        <td><?= $item['customer_moble'] ?></td>
                        <td><?= $item['customer_city'] ?></td>
                        <td><?= $item['cash_on_delivery'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>

                <tfoot>
                <tr>
                    <th>Order Number</th>
                    <th>Receiver Name</th>
                    <th>Receiver Mobile</th>
                    <th>Receiver City</th>
                    <th>COD</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group"
         style="<?php if (count($unCoveredOrders) == 0) echo 'display:none'; else echo 'display:block'; ?>;margin-top: 30px;">
        <div class="x_panel">
            <h2>Following records are not created due to out of coverage</h2>
            <table id="datatablenr" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Receiver Name</th>
                    <th>Receiver Mobile</th>
                    <th>Receiver City</th>
                    <th>COD</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($unCoveredOrders as $item) { ?>
                <tr>
                    <td><?= $item['order_number'] ?></td>
                    <td><?= $item['customer_name'] ?></td>
                    <td><?= $item['customer_moble'] ?></td>
                    <td><?= $item['customer_city'] ?></td>
                    <td><?= $item['cash_on_delivery'] ?></td>
                </tr>
                <?php } ?>
                </tbody>

                <tfoot>
                <tr>
                    <th>Order Number</th>
                    <th>Receiver Name</th>
                    <th>Receiver Mobile</th>
                    <th>Receiver City</th>
                    <th>COD</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- /page content -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
<!-- ECharts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>


@stop
