<!DOCTYPE html>
<html lang="en">
<head>
  <title>:. Saee :.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/companiesdashboard/css/font-awesome.css">
  <script src="<?php echo asset_url(); ?>/companiesdashboard/js/jquery.min.js"></script>
  <script src="<?php echo asset_url(); ?>/companiesdashboard/js/bootstrap.min.js"></script>
  <script src="<?php echo asset_url(); ?>/companiesdashboard/js/search.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/badge.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>



<body>

<!--header start-->

<div class="container-fluid topclr">
<div class="container">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img width="100" src="<?php echo asset_url(); ?>/bbchat/converse/logo/blackbox-logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <!--
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav navbar-right">
 
        <li class="border"><a href="#">LOG IN</a></li>
             
      </ul>
      
    </div>
    -->
    <!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>
</div>
</div>


<!--header end-->

<!--1st section start-->
<div class="container ">
<div class="row">
<div class="col-md-12 heading">

<h5>Verify Your Pin</h5>
<!--2nd section start-->

<div class="container">

<form action="{{ URL::Route('CompanyBlackBoxPinVerify') }}" method="post">
  

  <div class="container formwid">
 
    <label><b>PIN</b></label>
    <input type="text" placeholder="Enter Pincode" name="pin" required>
    <input type="hidden" name="waybill" id="waybill" value="<?php echo $waybill ?>">

    <button type="submit" class="button1">Verify</button><br>
   
  </div>
    
    
  </div>

  
</form>

<h4>
                @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                @endif
            </h4>

</div>

<!--2nd section end-->




</div>



</div>
</div>
<!--1st section end-->



<!--footer start-->

<div class="container">

<div class="footertext1">© All Rights Reserved Saee</div>
</div>

<!--footer end-->

<script src="<?php echo asset_url(); ?>/companiesdashboard/js/scroll.js"></script>

</body>
</html>