<div style="background-color: #fefefe;            border: 1px solid #888;            width: 351;            height: 511; ">
    <table cellpadding="1" cellspacing="0" style="border: 1px solid black;border-collapse: collapse; width: 100%; height: 100%;">
        <tr>
            <td style="border: 1px solid black; text-align:left;"><img
                        src="/companiesdashboard/images/logo.png"></td>
            <td colspan="3" style="border: 1px solid black;"><br>

                <p style="padding-right:10px;text-align: right;" ><?php echo $barcode ?></p>

                <p style="padding-left:3px;text-align: center"><?php echo $waybill ?></p></td>
        </tr>
        <tr>
            <td style="border: 1px solid black;">
                <p style="margin-top:5px; margin-left:5px;">Destination</p>
                <h4 style="font-size:10px; font-weight:bold; margin-left:5px;"><?php echo $destination ?></h4>
            </td>
            <td colspan="2" style="border: 1px solid black;">
                <p style="margin-left:5px; font-size:21px; font-weight:bold;">
                    COD:<br><?php echo $cod ?> SAR
                </p>
            </td>
            <td colspan="1" rowspan="2" align="left" style="border: 1px solid black;">
                <p style="font-size:14px; font-weight:bold; margin-left:10px;"><?php echo $customer_city ?></p>
                <?php echo $cityimg?>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;">
                <p>DOM</p>
            </td>
            <td style="border: 1px solid black;">
                <p>ONP</p>
            </td>
            <td style="border: 1px solid black;">
                <p>AC</p>
            </td>

        </tr>
        <tr>
            <td colspan="3" bgcolor="#333333" style="border: 1px solid black;"><p style="color:#FFF;">Services:
                </p></td>
            <td rowspan="4" style="border: 1px solid black;">
                <div style="width:2cm; height:8cm;">
                    <?php echo $barcode90 ?>
                    <p style="margin-top:75pt;padding:50pt;text-align: center;transform: rotate(270deg);"><?php echo $waybill ?></p>
                </div>

            </td>

        </tr>
        <tr>
            <td style="border: 1px solid black;"><p>Pieces<br><?php echo $pieces ?></p></td>
            <td style="border: 1px solid black;">Weight<br><?php echo $weight ?>KG</td>
            <td style="border: 1px solid black;">Date<br><?php echo $date ?> </td>

        </tr>


        <tr>
            <td colspan="3" style="border: 1px solid black;">
                <p style="margin: 10px 0 10px 10px;">
                    Account: 6004767<br>
                    <?php echo $company_name ?>, <?php echo $company_address ?></p>
                <p style="margin: 10px 0 10px 10px; "><?php echo $company_city ?>
                    ,SA <?php echo $company_phone ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid black;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" border-collapse="collapse">
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_name ?></p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_address ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_district ?>
                            </p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_city ?>
                                ,SA</p></td>
                    </tr>
                    <tr>
                        <td width="30%"><p style="margin: 0px 0 10px 10px; "><?php echo $customer_phone?></p>
                        </td>
                        <td width="30%" valign="top"><?php echo $customer_phone2?></td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td width="40%"><p style="margin: 0px 0 5px 5px; ">Shipper Ref:</p><strong style="margin-left:5px;"><?php echo $order_number ?></strong></td>
                        <td width="40%" valign="top">Consignee Ref:</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <p style="margin: 0px 0 5px 5px; ">
                    Description: <?php echo $description?></p></td>
            </td>
        </tr>
    </table>
</div>
