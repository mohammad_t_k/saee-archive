

    @extends('companies.layout')


    @section('bbregisteruser')

    <?php
    $companyAliases = BBAliases::select('alias_id', 'alias_name')->where('company_id','=',$company_id)->get();

    ?>

            <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Register User</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <!-- table start -->
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h4>Add Alias:</h4>
                    <div class="x_panel">
                        <div class="x_content">

                            <form id="bbaddalias-form" class="form-horizontal form-label-left" validate
                                  action="/blackbox/addaliaspost" method="post">

                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aliasid">Alias ID
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="aliasid" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aliasname">Alias Name
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="aliasname" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country">Country
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="country" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <input type='submit' value='Submit' class="btn  btn-warning"/>
                                    </div>
                                </div>
                            </form>
                            
 <div id="wait" style="display:none;width:auto;height:auto;border:1px solid black;top:50%;left:50%;padding:2px;text-align: center"><img src='<?php echo asset_url(); ?>/warehouseadmin/assets/admin/Loading_icon.gif' width="64" height="64" /><br>Adding User...</div>
                            

                            
                        </div>
                    </div>
                </div>
                <!-- !table start -->


                 <!-- table start -->
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h4>Add User:</h4>
                    <div class="x_panel">
                        <div class="x_content">

                            <form id="bbregisteruser-form" class="form-horizontal form-label-left" validate
                                  action="/blackbox/registeruserpost" method="post">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">User Name
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="username" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="password" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <span class="required">+966123456789</span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="phone" name="phone" required="required"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                                        
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="email" type="email" name="email"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aliasnamee">Alias
                                        <span class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="alias" required="required" class="form-control col-md-7 col-xs-12">
                                            <option value="">Select</option>
                                            <?php foreach($companyAliases as $alias){ ?>

                                                <option value="<?php echo $alias->alias_id?>"><?php echo $alias->alias_name; ?></option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <input onclick="if(!validateAllInputs()) return false;" type='submit' value='Submit' class="btn  btn-warning"/>
                                    </div>
                                </div>
                            </form>
                            
 <div id="wait" style="display:none;width:auto;height:auto;border:1px solid black;top:50%;left:50%;padding:2px;text-align: center"><img src='<?php echo asset_url(); ?>/warehouseadmin/assets/admin/Loading_icon.gif' width="64" height="64" /><br>Adding User...</div>
                            

                            
                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>

    <!-- /page content -->



    <!-- jstables script

    <script src="/Dev-Server-Resources/tableassets/jquery.js">

    </script>-->

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    
    <script type="text/javascript">
    function validateAllInputs() {
        var result = true;
        // Validating Sign Up Captain
        var phone = document.getElementById("phone");
        if (phone.value.substring(0, 4) != "+966") {
            alert("*Phone number must start with +966 e.g.+966501234567.<br>(??? ?? ???? ??? ?????? ?? +966 ???? +966501234567)");
            result = false;
        } else if (phone.value.length != 13) {
            alert("*Phone number must have 13 digits starting with +966 e.g.+966501234567.<br>+(??? ?? ???? ??? ????? 13 ???? ???? ? 966)");
            result = false;
        }
        var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        var mailvalue = document.getElementById('email');
        if (!mailformat.test(mailvalue.value)) {
            alert("Invalid Email");
            result = false;
        }
        return result;
    }
</script>

    <script type="text/javascript">
    var frm = $('#bbregisteruser-form');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({


            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            beforeSend: function(){
                // Handle the beforeSend event
             $("#wait").css("display", "block");
            },
            complete: function(){
            // Handle the complete event
            $("#wait").css("display", "none");
            },
            success: function (data) {
                console.log('Submission was successful.');

                var response = JSON.parse(data);
                console.log(response);
                if(response.message != undefined){
                    // error exception here
                alert(response.message);

                }else{

                    //success message here no object 
                    alert('Successfully Registered');
                    window.location = '/blackbox/allusers';
                }
                
            },
            error: function (data) {
                console.log('An error occurred.');
                var response = JSON.parse(data);
                alert('something went wrong');
                console.log(response);
            },
        });
    });
</script>

<script type="text/javascript">
    var form = $('#bbaddalias-form');

    form.submit(function (e) {

        e.preventDefault();

        $.ajax({

            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function(){
                // Handle the beforeSend event
             $("#wait").css("display", "block");
            },
            complete: function(){
            // Handle the complete event
            $("#wait").css("display", "none");
            },
            success: function (data) {

                console.log('Submission was successful.');

                alert(data.message);
                
            },
            error: function (data) {
                console.log('An error occurred.');
                alert('something went wrong');
                console.log(response);
            },
        });
    });
</script>

    @stop

