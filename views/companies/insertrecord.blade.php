@extends('companies.layout')


@section('insert')
    <style>
        #map_canvas {
            height: 150px;
            width: 75%;
            margin: 0;
        }

        #map_canvas .centerMarker {
            position: absolute;
            /*url of the marker*/
            background: url(../web/images/marker.png) no-repeat;
            /*center the marker*/
            top: 50%;
            left: 50%;
            z-index: 1;
            /*fix offset when needed*/
            margin-left: -10px;
            margin-top: -34px;
            /*size of the image*/
            height: 34px;
            width: 20px;
            cursor: pointer;
        }
    </style>
    <!-- page content -->

    <!-- page content -->
    <!-- page content -->
    <div class="right_col" role="main">
        <div>
            <div class="row">

                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" class="form" id="main-form" method="post" action="../company/generateshipmentpost" enctype="multipart/form-data">
                    <input id="latitude" name="latitude" required="required" type="hidden">
                    <input id="longitude" name="longitude" required="required" type="hidden">
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Customer Name (اسم العميل) <span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="name" name="name" class="form-control col-md-7 col-xs-12" required="required" placeholder=" " type="text" maxlength="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Mobile 1 (جوال 1) <span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="mobile" name="mobile" class="form-control col-md-7 col-xs-12" required="required" placeholder="e.g. 966512345678" type="text" onkeypress="return isNumber(event)" maxlength="15">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Mobile 2 (جوال 2) </label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="mobile2" name="mobile2" class="form-control col-md-7 col-xs-12" type="text" placeholder="e.g. 966512345678" onkeypress="return isNumber(event)" maxlength="15">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Email (البريد الإلكتروني) </label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="email" name="email" class="form-control col-md-7 col-xs-12" type="text" placeholder="" maxlength="200">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Shipment Description (وصف الشحنة)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="3" cols="45" id="description" name="description" class="form-control col-md-7 col-xs-12" required="required" type="text">Kitchen Accessories</textarea>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Main City (المدينة)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select required name="city" id="city" class="form-control" onchange="digest_maincity()">
                                <option value="empty"></option>
                                @foreach($main_cities as $one_city) 
                                    <option value="{{ $one_city->id }}">{{ $one_city->name }} - {{ $one_city->name_ar }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Sub City (المدينة)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select required name="subcity" id="subcity" class="form-control" onchange="digest_subcity()">
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Street Address (اسم الشارع)</label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="streetaddress2" name="streetaddress2" class="form-control col-md-7 col-xs-12" required="required" type="text" />
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">District (الحي)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select name="district" id="district" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Full Address (العنوان كامل)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <textarea rows="5" cols="45" id="streetaddress" name="streetaddress" class="form-control col-md-7 col-xs-12" required="required" type="text"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Cash on Delivery (الدفع عند التوصيل) <span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="cashondelivery" name="cashondelivery" class="form-control col-md-7 col-xs-12" required="required" placeholder="" type="number"  step="any" maxlength="20">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Order No. (رقم الشحنة) </label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="order_number" name="order_number" class="form-control col-md-7 col-xs-12"
                                   type="text" placeholder="" maxlength="30">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Quantity (العدد)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="quantity" name="quantity" class="form-control col-md-7 col-xs-12" placeholder="e.g. 1" required="required" type="text" onkeypress="return isNumber(event)" maxlength="5">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Weight (الوزن)<span class="required">*</span></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <input id="weight" name="weight" class="form-control col-md-7 col-xs-12" placeholder="e.g. 10" required="required" type="text" maxlength="5">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12"></label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <label><input type="checkbox" id="pickup_service" name="pickup_service" value="1" class="checkbox-inline form-check"> Add Pickup Service</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" style="display: none;" id="div_pickup_time_interval">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Pick Time Interval</label>
                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <select name="pickup_time_interval" id="pickup_time_interval" class="form-control">
                                @foreach($pickup_time_intervals as $one_record)
                                    <?php 
                                        $from = $one_record->from > 12 ? $one_record->from - 12 : $one_record->from;
                                        $to = $one_record->to > 12 ? $one_record->to - 12 : $one_record->to;
                                        $f = $one_record->from >= 12 ? 'PM' : 'AM';
                                        $t = $one_record->to >= 12 ? 'PM' : 'AM';
                                    ?>
                                    <option value="{{ $one_record->id }}">{{ "$from $f - $to $t" }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <script>
                        $("#pickup_service").change(function() {
                            if($("#pickup_service").is(":checked")) 
                                $("#div_pickup_time_interval").css('display', '');
                            else 
                                $("#div_pickup_time_interval").css('display', 'none');
                        });
                    </script>

                    @if($companydata->company_type == 1) 
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                            <label class="control-label col-md-5 col-sm-3 col-xs-12">Merchant (التاجر)<span
                                        class="required">*</span></label>

                            <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                                <select name="branch" id="branch" class="form-control">
                                    <option value="{{ $companydata->id }}">Main Company</option>
                                    @foreach($merchants as $merchant)
                                        <option value="{{ $merchant->id }}">{{ $merchant->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-5 col-sm-3 col-xs-12">Customer Map Location(عنوان جوجل)</label>

                        <div class="col-lg-7 col-md-2 col-sm-6 col-xs-12 ">
                            <div id="map_canvas"></div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 form-group">
                        <button type="submit" class="btn btn-warning btn-flat btn-block">Generate Shipment Sticker
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            z-index: 1; /* Sit on top */
            position: absolute;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.6); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-header {
            background-color: #fefefe;
            padding: 5px;
            border: 1px solid #888;
            width: 100%; /* Could be more or less, depending on screen size */
            height: 50px; /* Could be more or less, depending on screen size */
        }
        /* Modal Content/Box */
        #modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 300px; /* Could be more or less, depending on screen size */
            height: 150px; /* Could be more or less, depending on screen size */
        }

        #sticker-content {
            margin: 8% auto;
            width: 10.16cm;
        }

        /* The Close Button */
        #close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        #close:hover,
        #close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
    <div id="modal" class="modal">
        <!-- Modal content -->
        <div id="modal-content" class="modal-content">
            <div class='row'>
                <div class='col-md-6 col-sm-6 col-xs-6'>
                    <label>Your Way Bill  is:</label>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-6'>
                    <label id="waybilllabel"></label>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <p>
                        <button id="printsticker" class="btn btn-success">Print Waybill Sticker</button>
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div id="stickermodal" class="modal">

        <!-- Modal content -->
        <div id="sticker-content">
            <div class="modal-header">
                <button id='stickerclose' type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">WayBill Sticker</h4>
            </div>
            <div id="stickercontent">

            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap"
            async defer></script>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function initMap() {
            try {
                var latitude = 21.2854;
                var longitude = 39.2376;
                var mapOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(latitude, longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById('map_canvas'),
                        mapOptions);
                google.maps.event.addListener(map, 'center_changed', function () {
                    document.getElementById('latitude').value = map.getCenter().lat();
                    document.getElementById('longitude').value = map.getCenter().lng();
                });
                $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
                    //do something onclick
                        .click(function () {
                            var that = $(this);
                            if (!that.data('win')) {
                                that.data('win', new google.maps.InfoWindow({
                                    content: 'this is the center'
                                }));
                                that.data('win').bindTo('position', map, 'center');
                            }
                            that.data('win').open(map);
                        });
            }
            catch (e) {
                alert(e);
            }
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    
    <script>

        function getSubCities() {
            $("#subcity").empty();
            var maincity = $("#city").val();
            console.log('getSubCities function -> maincity: ' + maincity);
            let subcity = document.getElementById('subcity');
            console.log('hello {{ count($sub_cities) }}');
            @foreach($sub_cities as $one_subcity)
                if(maincity == "{{ $one_subcity->city_id }}") {
                    option = document.createElement('option');
                    option.setAttribute('value', "{{ $one_subcity->id }}");
                    option.appendChild(document.createTextNode("{{ $one_subcity->subcity }} - {{ $one_subcity->subcity_ar }}"));
                    subcity.appendChild(option);
                }
            @endforeach
        }

        function getDistricts() {
            $("#district").empty();
            let subcity = $("#subcity").val();
            console.log('getDistricts function -> subcity: ' + subcity);
            let district = document.getElementById('district');
            @foreach($districts as $one_district)
                if(subcity == "{{ $one_district->subcity_id }}") {
                    option = document.createElement('option');
                    option.setAttribute('value', "{{ $one_district->district_name }}");
                    option.appendChild(document.createTextNode("{{ $one_district->district_name }} | {{ $one_district->district_name_ar }}"));
                    district.appendChild(option);
                }
            @endforeach
        }

        function digest_maincity() {
            console.log('digest_maincity function');
            getSubCities();
            getDistricts();
        }

        function digest_subcity() {
            console.log('digest_subcity function');
            getDistricts();
        }
        
        var modal = document.getElementById('modal');
        var stickermodal = document.getElementById('stickermodal');
        var stickercontent = document.getElementById('stickercontent');
        var waybilllabel = document.getElementById('waybilllabel');
        var printsticker = document.getElementById("printsticker");
        var stickerclose = document.getElementById("stickerclose");
        var sticker = '<?php if(isset($sticker)) echo $sticker; else false;?>';
        var message = '<?php if(isset($error)) echo $error; else false;?>';
        var waybill = '<?php if(isset($waybill)) echo $waybill; else false;?>';
        if(message){
            alert(message);
        }
        var url = '';
        if(waybill){
            waybilllabel.innerHTML = waybill;
            url = "../company/sticker/"+waybill;
            modal.style.display = "block";
        }
        var doc = new jsPDF();
        var specialElementHandlers = {
            '#editor': function(element, renderer){
                return true;
            }
        };
        stickerclose.onclick = function () {
            window.open(url);
            stickermodal.style.display = "none";
        }
        printsticker.onclick = function () {
            //window.open(url);
            modal.style.display = "none";
            stickercontent.innerHTML = sticker;
            stickermodal.style.display = "block";
            /*
            doc.fromHTML(sticker, 15, 15, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.save(waybill+'.pdf');*/
        }

        function printElement(elem) {
            var domClone = elem.cloneNode(true);

            var stickercontent = document.getElementById("stickercontent");

            if (!stickercontent) {
                var stickercontent = document.createElement("div");
                stickercontent.id = "printSection";
                document.body.appendChild(stickercontent);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }
    </script>
@stop

