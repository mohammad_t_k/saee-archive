
@extends('companies.layout')


@section('packageDetails')


   
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Shipment Details</h3>
              </div>

              
            </div>

            <div class="clearfix"></div>
            <?php if(in_array($package->status, array(-3,0,1,2,3,6))) { ?>
            <div class="row">
              <div class="col-md-8 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $packagedetails->waybill ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                    <form class="form-horizontal form-label-right" action="/company/updatecontact" method="post" >
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Waybill</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="waybill" readonly value =<?php echo $packagedetails->waybill ?> class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Order No</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="order_number" value ="<?php echo $packagedetails->order_number ?>" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area Rep. Name </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="sender_name"  align="right" value ="<?php echo $packagedetails->sender_name ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area Rep. Mobile </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="sender_phone"  align="right" value ="<?php echo $packagedetails->sender_phone ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="receiver_name"  align="right" value ="<?php echo $packagedetails->receiver_name ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                      
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">{{ $companydata->id == '952' ? 'Store' : '' }} Mobile 1 <span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile1"  align="right" value ="<?php echo $packagedetails->receiver_phone ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">{{ $companydata->id == '952' ? 'Store' : '' }} Mobile 2 <span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile2"   align="right" value ="<?php echo $packagedetails->receiver_phone2 ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                     
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Address </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="street_address"  align="right" value ="<?php echo $packagedetails->d_address ?>" required="required" class="form-control col-md-2 col-xs-12">
                      </div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address</label>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="street_address_2"  align="right" value ="<?php echo $packagedetails->d_address2 ?>" required="required" class="form-control col-md-2 col-xs-12">
                      </div>

                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address3 ?> </label>
                      </div>
                      
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_district ?> </label>
                      </div>
                      
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Change district to</span>
                            </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="district">
                                <?php 

                                $grouped = 'select distinct grouped from districts a,city b where SUBSTRING_INDEX(district, "+", 1) = "'.$packagedetails->d_city.'" and a.city = b.name';

                                $groupedCity = DB::select($grouped);

                                
                                
                                if (count($groupedCity)) {

                                      $checkgroup = '1';
                                     
                                  } else {

        
                                      $checkgroup = '0';

                                  }

                                  if($checkgroup==1){

                                $districts = Districts::where('district','like','%'.$packagedetails->d_city.'%')->orderBy('district', 'asc')->get(['district']);

                                }else{

                                  $districts = Districts::where('city','like','%'.$packagedetails->d_city.'%')->orderBy('district', 'asc')->get(['district']);
                                }
                             
                              
                                echo "<option value=\"$packagedetails->d_district\">$packagedetails->d_district</option> ";

                                foreach ( $districts as $adistrict) 
                                {
                                    
                                    echo "<option value=\"$adistrict->district\">$adistrict->district</option> ";
                                    
                                }
                                
                              ?>
                                 
    
                            </select>
                        </div>
                        </div>
                       
                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_city ?> </label>
                      </div>


                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Change City to</span>
                            </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="city">
                                <?php 
                                
                                $cities = City::where('is_covered', '=', '1')->orderBy('id', 'asc')->get(['name']);
                                echo "<option value=\"$packagedetails->main_city\">$packagedetails->main_city</option> ";

                                foreach ( $cities as $acity) 
                                {
                                    if($acity->name =='All')
                                    {
                                      continue;
                                    }
                                    echo "<option value=\"$acity->name\">$acity->name</option> ";
                                    
                                }
                                
                              ?>
                                 
    
                            </select>
                        </div>
                        </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">State </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_state ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Created at </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->created_at ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php
                            
                                if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0 && strtotime($packagedetails->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                                    echo 'Warehouse/Res';
                                else if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0)
                                    echo 'Warehouse/R';
                                else if($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts == 0)
                                    echo 'Warehouse/New';
                                else
                                    echo $statusDef[$packagedetails->status];?> </label>
                      </div>
                      
                      <div class="form-group" style="display:<?php if($packagedetails->num_faild_delivery_attempts>0) echo 'block';else echo 'none';?>" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Undelivered reason </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->undelivered_reason ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">COD </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="cash_on_delivery"  align="right" value ="<?php echo $packagedetails->cash_on_delivery ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div> SAR
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pincode </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->pincode ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <textarea name="description"  align="right" required="required" class="form-control col-md-2 col-xs-12">
                          <?php echo $packagedetails->d_description ?>
                        </textarea>
                      </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="quantity"  align="right" value ="<?php echo $packagedetails->d_quantity ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="weight"  align="right" value ="<?php echo $packagedetails->d_weight ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tracking URL</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><a target="_blank" href="<?php echo URL::to('/').'/trackingpage?trackingnum='.$packagedetails->waybill ?>"><?php echo URL::to('/').'/trackingpage?trackingnum='.$packagedetails->waybill ?> </a></label>
                      </div>
                      
                      <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Comments</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea class="resizable_textarea form-control" name="notes" placeholder=""></textarea>
                    </div>
                    </div>
                    <br/>
                          <div class="col-md-6 col-md-offset-3">
                            <input type='submit'value='save' class="btn  btn-success" />
                        </div>

                    </form>
                      
                      
                      
                  </div>
                </div>
              </div>
            </div>

            <?php } else{ ?>

            <div class="row">
              <div class="col-md-8 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $packagedetails->waybill ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                    <form class="form-horizontal form-label-right" action="/company/updatecontact" method="post" >
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Waybill</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="waybill" readonly value =<?php echo $packagedetails->waybill ?> class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Order No</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="order_number" readonly value ="<?php echo $packagedetails->order_number ?>" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area Rep. Name </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="sender_name"  align="right" readonly value ="<?php echo $packagedetails->sender_name ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area Rep. Mobile </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="sender_phone"  align="right" readonly value ="<?php echo $packagedetails->sender_phone ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name </label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="receiver_name"  align="right" readonly value ="<?php echo $packagedetails->receiver_name ?>" required="required" class="form-control col-md-2 col-xs-12">
                          </div>
                        </div>
                      
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">{{ $companydata->id == '952' ? 'Store' : '' }} Mobile 1 <span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile1"  align="right" readonly value ="<?php echo $packagedetails->receiver_phone ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">{{ $companydata->id == '952' ? 'Store' : '' }} Mobile 2 <span class="required">*</span>
                            </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="mobile2"   align="right" readonly value ="<?php echo $packagedetails->receiver_phone2 ?>" required="required" class="form-control col-md-2 col-xs-12">
                        </div>
                        </div>
                        
                     
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Address </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="street_address"  align="right" readonly value ="<?php echo $packagedetails->d_address ?>" required="required" class="form-control col-md-2 col-xs-12">
                      </div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address</label>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="street_address_2"  align="right" readonly value ="<?php echo $packagedetails->d_address2 ?>" required="required" class="form-control col-md-2 col-xs-12">
                      </div>

                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_address3 ?> </label>
                      </div>
                      
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_district ?> </label>
                      </div>
                      
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Change district to</span>
                            </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select disabled="" name="district">
                                <?php 
                                
                                $districts = Districts::orderBy('district', 'asc')->get(['district']);
                                echo "<option value=\"$packagedetails->d_district\">$packagedetails->d_district</option> ";

                                foreach ( $districts as $adistrict) 
                                {
                                    
                                    echo "<option value=\"$adistrict->district\">$adistrict->district</option> ";
                                    
                                }
                                
                              ?>
                                 
    
                            </select>
                        </div>
                        </div>
                       
                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_city ?> </label>
                      </div>


                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Change City to</span>
                            </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select disabled="" name="city">
                                <?php 
                                
                                $cities = City::orderBy('id', 'asc')->get(['name']);
                                echo "<option value=\"$packagedetails->d_city\">$packagedetails->d_city</option> ";

                                foreach ( $cities as $acity) 
                                {
                                    if($acity->name =='All')
                                    {
                                      continue;
                                    }
                                    echo "<option value=\"$acity->name\">$acity->name</option> ";
                                    
                                }
                                
                              ?>
                                 
    
                            </select>
                        </div>
                        </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">State </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_state ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Created at </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->created_at ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php
                            
                            if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0 && strtotime($packagedetails->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                                echo 'Warehouse/Res';
                            else if ($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts > 0)
                                echo 'Warehouse/R';
                            else if($packagedetails->status == 3 && $packagedetails->num_faild_delivery_attempts == 0)
                                echo 'Warehouse/New';
                            else
                                echo $statusDef[$packagedetails->status];?> </label>
                      </div>
                      
                      <div class="form-group" style="display:<?php if($packagedetails->num_faild_delivery_attempts>0) echo 'block';else echo 'none';?>" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Undelivered reason </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->undelivered_reason ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">COD </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="cash_on_delivery"  align="right" readonly value ="<?php echo $packagedetails->cash_on_delivery ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div> SAR
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pincode </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->pincode ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $packagedetails->d_description ?> </label>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="quantity"  align="right" readonly value ="<?php echo $packagedetails->d_quantity ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" name="weight"  align="right" readonly value ="<?php echo $packagedetails->d_weight ?>" required="required" class="form-control col-md-2 col-xs-12"> 
                      </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tracking URL</label>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><a target="_blank" href="<?php echo URL::to('/').'/trackingpage?trackingnum='.$packagedetails->waybill ?>"><?php echo URL::to('/').'/trackingpage?trackingnum='.$packagedetails->waybill ?> </a></label>
                      </div>
                      
                      <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Comments</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea class="resizable_textarea form-control" name="notes" placeholder=""></textarea>
                    </div>
                    </div>
                    <br/>
                          <div class="col-md-6 col-md-offset-3">
                            <input type='submit'value='save' class="btn  btn-success" />
                        </div>

                    </form>
                      
                      
                      
                  </div>
                </div>
              </div>
            </div>

            <?php } ?>

          </div>
      
        <!-- /page content -->

 

@stop