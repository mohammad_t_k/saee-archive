<!DOCTYPE html>
<html lang="en">
<head>
    <title>:. Saee :.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/companiesdashboard/css/font-awesome.css">
    <link href="<?php echo asset_url(); ?>/warehouseadmin/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/bootstrap.min.css">
    <script src="<?php echo asset_url(); ?>/companiesdashboard/js/search.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/companiesdashboard/css/badge.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>


<body>

<!--header start-->

<div class="container-fluid topclr">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/company/dashboard"><img
                                src="<?php echo asset_url(); ?>/companiesdashboard/images/logo-w.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="column margintop20 rs-show">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#">
                                    Welcome,<br> <?php echo $companydata->company_name; ?></a>
                            </li>
                            <li><a href="/company/shipments/all"><span class="glyphicon glyphicon-chevron-right"></span>All
                                    Shipments</a></li>
                            <?php $is_api = Session::get('is_api');
                            if ($is_api == 0) {
                                echo "<li><a href=\"/company/generateshipment\"><span class=\"glyphicon glyphicon-chevron-right\"></span>Generate Shipment</a></li>";
                            }?>
                            <li><a href="/company/shipmentsExport"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Export Shipments</a></li>
                            <li><a href="/company/sameDaySuccessRate"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Same Day Success Rate Report</a></li>
                            <li><a href="/company/admintracking"><span class="glyphicon glyphicon-chevron-right"></span>Single
                                    Tracking Search</a></li>
                            <li><a href="/company/admintrackingbulk"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Multi Tracking Search</a>
                            </li>
                            <li> 
                                <a href="/company/ticket/create"><span class="glyphicon glyphicon-chevron-right"></span>Create Ticket</a>
                            </li>
                            <li> 
                                <a href="/company/ticket/export"><span class="glyphicon glyphicon-chevron-right"></span>Tickets Export</a>
                            </li>
                            <li> 
                                <a href="/company/kpiReport"><span class="glyphicon glyphicon-chevron-right"></span>KPI Report</a>
                            </li>
                            <li><a href="/company/orderimportxml"><span class="glyphicon glyphicon-chevron-right"></span>
                                    Data Import</a></li>
                            <li><a href="/company/companydetail"><span class="glyphicon glyphicon-chevron-right"></span>
                                    Company Profile</a></li>
                            <li><a href="/company/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a>
                            </li>
                            <li><a href="https://www.saee.sa/blackbox/login" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Black Box</a></li>
                            <li><span class="glyphicon glyphicon-chevron-right"></span>BB User Management</li>
             <a id="bbregister" style="padding: 20px;display: inline-block;" href="/blackbox/registeruser">Register User</a>

              <a id="bballusers" style="padding: 20px;display: inline-block;" href="/blackbox/allusers">All Users</a>
                        </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</div>

<!--header end-->
<div class="col-md-12 body-con">
    <div class="col-md-2 column margintop20 fix rs-none">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="/company/dashboard">
                    Welcome,<br> <?php echo $companydata->company_name; ?></a>
            </li>
            <li><a href="/company/shipments/all"><span class="glyphicon glyphicon-chevron-right"></span>All
                    Shipments</a></li>
            <?php $is_api = Session::get('is_api');
            if ($is_api == 0) {
                echo "<li><a href=\"/company/generateshipment\"><span class=\"glyphicon glyphicon-chevron-right\"></span>Generate Shipment</a></li>";
            }?>
            <li><a href="/company/shipmentsExport"><span class="glyphicon glyphicon-chevron-right"></span>Export
                    Shipments</a></li>
            <li><a href="/company/sameDaySuccessRate"><span
                                            class="glyphicon glyphicon-chevron-right"></span>Same Day Success Rate Report</a></li>
            <li><a href="/company/admintracking"><span class="glyphicon glyphicon-chevron-right"></span>Single Tracking
                    Search</a></li>
            <li><a href="/company/admintrackingbulk"><span class="glyphicon glyphicon-chevron-right"></span>Multi
                    Tracking Search</a></li>
            <li> 
                <a href="/company/ticket/create"><span class="glyphicon glyphicon-chevron-right"></span>Create Ticket</a>
            </li>
            <li> 
                <a href="/company/ticket/export"><span class="glyphicon glyphicon-chevron-right"></span>Tickets Export</a>
            </li>
            <li> 
                <a href="/company/kpiReport"><span class="glyphicon glyphicon-chevron-right"></span>KPI Report</a>
            </li>
            <li><a href="/company/orderimportxml"><span class="glyphicon glyphicon-chevron-right"></span>
                                    Data Import</a></li>
            <li><a href="/company/companydetail"><span class="glyphicon glyphicon-chevron-right"></span> Company Profile</a>
            </li>
            <li><a href="/company/logout"><span class="glyphicon glyphicon-chevron-right"></span>Logout</a></li>
            <li><a href="https://www.saee.sa/blackbox/login" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Black Box</a></li>
             <li><span class="glyphicon glyphicon-chevron-right"></span>BB User Management</li>
             <a id="bbregister" style="padding: 20px;display: inline-block;" href="/blackbox/registeruser">Register User</a>

              <a id="bballusers" style="padding: 20px;display: inline-block;" href="/blackbox/allusers">All Users</a>
        </ul>
    </div>


    <div class="col-md-10">

        @yield('dashboard')
        @yield('shipments')
        @yield('packageDetails')
        @yield('companydetails')
        @yield('customerservice')
        @yield('content')
        @yield('insert')
        @yield('editcompany')
        @yield('bbregisteruser')

    </div>
</div>

<div class="container">
    <div class="col-md-12">
        @yield('histogram')
    </div>
</div>

<!--footer start-->

<div class="container">

    <div class="footertext1">© All Rights Reserved Saee</div>
</div>

<!--footer end-->


<!-- Bootstrap -->
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- morris.js -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
<!-- ECharts -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/build/js/custom.js"></script>
<script src="<?php echo asset_url(); ?>/companiesdashboard/js/scroll.js"></script>

@yield('dashboard-script')

        <!--footer end-->


</body>
</html>
