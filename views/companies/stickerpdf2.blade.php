<!DOCTYPE html>
<html style="margin: 0px; padding: 0.5cm; ">
   
<head>
    <title>Saee</title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
</head>
<style>
*{padding:0;
margin:0;}
@page {
    size: 20cm 29cm;
    margin: 0mm 0mm 0mm 0mm;
}
table td {
    font-size: 30px;
    padding-left: 5px;
}
td img {
    display: block;
    margin-left: auto;
    margin-right: auto;
}
</style>

<body style="margin: 0px; padding: 0;" id="content">

<div style="background-color: #fefefe; border: 0px;  solid #888;">
    <table cellpadding="0" cellspacing="0" font-size="10px"
           style="border: 1px solid black;border-collapse: collapse; 
           width:21cm; height:29.7cm; margin: 0px 0 0px 0px; padding: 0px;">
        <tr>
            <td style="border: 1px solid black; text-align:left;" align="center"><img
                        src="/companiesdashboard/images/Saee-Logo.png" style="width: 4cm; height: 3cm;"></td>
            <td colspan="3" style="border: 1px solid black;" align="center"><br>

                <p style="padding-right:10px;text-align: right;"><?php echo $barcode ?></p>

                <p style="padding-left:3px;text-align: center"><?php echo $waybill ?></p></td>
        </tr>
        <tr>
            <td style="border: 1px solid black;">
                <p style="margin-top:5px; margin-left:5px;">Destination</p>
                <h4 style="font-size:50px; font-weight:bold; margin-left:5px;"><?php echo $destination ?></h4>
            </td>
            <td colspan="2" style="border: 1px solid black;">
                <p style="margin-left:5px; font-size:21px;">
                    COD:<br><span style="font-size: 40px; font-weight:bold;"><?php echo $cod ?> SAR </span>
                </p>
            </td>
            <td colspan="1" rowspan="2" align="left" style="border: 1px solid black;">
                <p style="font-weight:bold; margin-left:10px;"><?php  echo $customer_city ?></p>
                <h1 style="font-size:24px; font-weight:bold; margin-left:5px;"><b><?php echo $destination ?></b></h1>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black; padding-left: 5px;">
                <p>DOM</p>
            </td>
            <td style="border: 1px solid black; padding-left: 5px;">
                <p>ONP</p>
            </td>
            <td style="border: 1px solid black; padding-left: 5px;">
                <p>AC</p>
            </td>

        </tr>
        <tr>
            <td colspan="4" bgcolor="#333333" style="border: 1px solid black; padding-left: 5px;"><p style="color:#FFF;">Services:
                </p></td>

        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid black;"><p>Pieces<br><?php echo $pieces ?></p></td>
            <td style="border: 1px solid black;">Weight<br><?php echo $weight ?>KG</td>
            <td style="border: 1px solid black;">Date<br><?php echo $date ?> </td>

        </tr>


        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <p style="margin: 10px 0 10px 10px;">
                    Account: 6004767<br>
                    <?php echo $company_name ?>, <?php echo $company_address ?></p>

                <p style="margin: 10px 0 10px 10px; "><?php echo $company_city ?>
                    ,SA <?php echo $company_phone ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" border-collapse="collapse">
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_name ?></p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_address ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php echo $customer_district ?>
                            </p></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p style="margin-left:10px; "><?php if (isset($main_city) && $main_city != '') {
                                    echo $main_city->city;
                                    echo " - ";
                                    echo $customer_city ;
                                } else  {echo $customer_city;} ?>,
                                SA</p></td>
                    </tr>
                    <tr>
                        <td width="30%"><p style="margin: 0px 0 10px 10px; "><?php echo $customer_phone?></p>
                        </td>
                        <td width="30%" valign="top"><?php echo $customer_phone2?></td>
                    </tr>
                    <tr style="border: 1px solid black;">
                        <td width="40%"><p style="margin: 0px 0 5px 5px; ">Shipper Ref:</p><strong
                                    style="margin-left:5px;"><?php echo $order_number ?></strong></td>
                        <td width="40%" valign="top">Consignee Ref:</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <div >{{--style="padding-left:8px;width:2cm; height:8cm;"--}}
                    <?php
                    if ($order_number != "") { ?>
                    <p style=" padding: 10px 50px;text-align: right;"><?php echo $barcode90 ?></p>
                    <p style="text-align: center"><?php echo $order_number; }?></p>{{--style="margin-top:60pt;padding:60pt;text-align: center"--}}
                </div>

            </td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid black;">
                <p style="margin: 0px 0 5px 5px; ">
                    Description: <?php echo $description?></p></td>
        </tr>
    </table>
</div>

</body>
</html> 
