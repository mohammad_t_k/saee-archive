@extends('companies.layout')


@section('insert')

    <style>
        .background-title {
            background-color: #d1d1d1;
        }
        .background-sublist {
            background-color: #f2f2f2;
        }
        .background-sub-sublist {
            background-color: #fff;
        }
        body {
            color: black;
        }
        table {
            border: 1px solid orange;
        }
    </style>

    <div class="right_col" role="main">

        <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <form method="get" action="/company/kpiReport">
                        <input type="hidden" name="submitForm" value="1" />
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="cities[]" id="cities" class="form-control" multiple>
                                        @foreach($adminCities as $adcity)
                                            <option value="{{$adcity->city}}"<?php if (isset($cities) && in_array($adcity->city, $cities)) echo 'selected';?>>{{$adcity->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">From </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="from" id="from" class="form-control" value="{{ $from }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">To </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <input type="date" name="to" id="to" class="form-control" value="{{ $to }}" />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button id="submitForm" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- COD Transfer -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <table class="table table-striped table-bordered">
                    <caption>COD Transfer [{{ $from }}, {{ $to }}]</caption>
                    <tr>
                        <th style="width: 15%;"></th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <th>Paid shipments</th>
                        <td>{{ number_format($cod_transfered, 2) }}</td>
                    </tr>
                    <tr id="cod_transfer_not_paid" class="show_details rs">
                        <th>Delivered not paid shipments</th>
                        <td>{{ number_format($cod_delivered - $cod_transfered, 2) }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <!-- KPI -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php

                // success rate prepaid - success rate cod
                $srp_delivered = array();
                $srp_dispatched = array();
                $srcod_delivered = array();
                $srcod_dispatched = array();
                $srp_delivered['All'] = 0;
                $srp_dispatched['All'] = 0;
                $srcod_delivered['All'] = 0;
                $srcod_dispatched['All'] = 0;
                foreach($cities as $one_city) {
                    $srp_delivered[$one_city] = 0;
                    $srp_dispatched[$one_city] = 0;
                    $srcod_delivered[$one_city] = 0;
                    $srcod_dispatched[$one_city] = 0;
                }
                foreach($success_rate as $one_record) {
                    $main_city = $city_map[$one_record->city];
                    if($one_record->cod == 0) {
                        $srp_delivered[$main_city] += $one_record->delivered;
                        $srp_delivered['All'] += $one_record->delivered; // will not be a record with city = 'All', so this will be empty 
                        $srp_dispatched[$main_city] += $one_record->dispatched;
                        $srp_dispatched['All'] += $one_record->dispatched;
                    }
                    else if($one_record->cod == 1) {
                        $srcod_delivered[$main_city] += $one_record->delivered;
                        $srcod_delivered['All'] += $one_record->delivered;
                        $srcod_dispatched[$main_city] += $one_record->dispatched;
                        $srcod_dispatched['All'] += $one_record->dispatched;
                    }
                }

                // returned to owner
                $rto_view = array();
                $rto_view['All'] = 0;
                foreach($cities as $one_city) {
                    $rto_view[$one_city] = 0;
                }
                foreach($rto as $one_record) {
                    $main_city = $one_record->city;
                    $main_city = $city_map[$main_city];
                    $rto_view[$main_city] += $one_record->count;
                    $rto_view['All'] += $one_record->count;
                }

                // transit days
                $transit_days_t1_sum = array();
                $transit_days_t2_sum = array();
                $transit_days_t3_sum = array();
                $transit_days_t1_count = array();
                $transit_days_t2_count = array();
                $transit_days_t3_count = array();
                $transit_days_t1_sum['All'] = 0;
                $transit_days_t2_sum['All'] = 0;
                $transit_days_t3_sum['All'] = 0;
                $transit_days_t1_count['All'] = 0;
                $transit_days_t2_count['All'] = 0;
                $transit_days_t3_count['All'] = 0;
                foreach($cities as $one_city) {
                    $transit_days_t1_sum[$one_city] = 0;
                    $transit_days_t2_sum[$one_city] = 0;
                    $transit_days_t3_sum[$one_city] = 0;
                    $transit_days_t1_count[$one_city] = 0;
                    $transit_days_t2_count[$one_city] = 0;
                    $transit_days_t3_count[$one_city] = 0;
                }
                foreach($transit_tier as $one_record) {
                    $main_city = $one_record->city;
                    if($one_record->tier == 1) {
                        $transit_days_t1_sum[$main_city] += $one_record->sum;
                        $transit_days_t1_sum['All'] += $one_record->sum;
                        $transit_days_t1_count[$main_city] += $one_record->count;
                        $transit_days_t1_count['All'] += $one_record->count;
                    }
                    else if($one_record->tier == 2) {
                        $transit_days_t2_sum[$main_city] += $one_record->sum;
                        $transit_days_t2_sum['All'] += $one_record->sum;
                        $transit_days_t2_count[$main_city] += $one_record->count;
                        $transit_days_t2_count['All'] += $one_record->count;
                    }
                    else if($one_record->tier == 3) {
                        $transit_days_t3_sum[$main_city] += $one_record->sum;
                        $transit_days_t3_sum['All'] += $one_record->sum;
                        $transit_days_t3_count[$main_city] += $one_record->count;
                        $transit_days_t3_count['All'] += $one_record->count;
                    }
                }
                ?>
                <table class="table table-bordered">
                    <caption>KPI [{{ $from }}, {{ $to }}]</caption>
                    <tr>
                        <th style="width: 15%;"></th>
                        <th>Total</th>
                        @foreach($cities as $one_city)
                                <th>{{ $one_city == 'All' ? 'Total' : $one_city }}</th>
                        @endforeach
                    </tr>
                    <tr class="background-title">
                        <th colspan="{{ count($cities) + 2 }}">Success Rate</th>
                    </tr>
                    <tr id="kpi_prepaid_sr" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; Prepaid Success Rate</th>
                        <td class='All'>
                            {{ number_format($srp_delivered['All'] / max(1, $srp_dispatched['All']) * 100, 2) }}%
                        </td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>
                                {{ number_format($srp_delivered[$one_city] / max(1, $srp_dispatched[$one_city]) * 100, 2) }}%
                            </td>
                        @endforeach
                    </tr>
                    <tr id="kpi_cod_sr" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; COD Success Rate</th>
                        <td class='All'>
                            {{ number_format($srcod_delivered['All'] / max(1, $srcod_dispatched['All']) * 100, 2) }}%
                        </td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>
                                {{ number_format($srcod_delivered[$one_city] / max(1, $srcod_dispatched[$one_city]) * 100, 2) }}%
                            </td>
                        @endforeach
                    </tr>
                    <tr id="kpi_ttl_sr" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; Overall Success Rate</th>
                        <td class='All'>{{ number_format(($srp_delivered['All'] + $srcod_delivered['All']) / max(1, $srp_dispatched['All'] + $srcod_dispatched['All']) * 100, 2) }}%</td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>{{ number_format(($srp_delivered[$one_city] + $srcod_delivered[$one_city]) / max(1, $srp_dispatched[$one_city] + $srcod_dispatched[$one_city]) * 100, 2) }}%</td>
                        @endforeach
                    </tr>
                    <tr id="kpi_rto" class="show_details kpi background-title">
                        <th>RTO</th>
                        <td class='All'>{{ number_format($rto_view['All']) }}</td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>{{ number_format($rto_view[$one_city]) }}</td>
                        @endforeach
                    </tr>
                    <tr class="background-title">
                        <th colspan="{{ count($cities) + 2 }}">Transit Days (Average By Hours)</th>
                    </tr>
                    <tr id="kpi_tier1" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; Tier 1</th>
                        <td class='All'>
                            @if($transit_days_t1_count['All'] > 0)
                            <span style="font-weight: bold;">Count:</span> {{ $transit_days_t1_count['All'] }}
                            <br/>
                            <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t1_sum['All'] / max(1, $transit_days_t1_count['All']), 2) }}
                            @endif
                        </td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>
                                @if($transit_days_t1_count[$one_city] > 0)
                                <span style="font-weight: bold;">Count:</span> {{ $transit_days_t1_count[$one_city] }}
                                <br/>
                                <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t1_sum[$one_city] / max(1, $transit_days_t1_count[$one_city]), 2) }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                    <tr id="kpi_tier2" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; Tier 2</th>
                        <td class='All'>
                            @if($transit_days_t2_count['All'] > 0)
                            <span style="font-weight: bold;">Count:</span> {{ $transit_days_t2_count['All'] }}
                            <br/>
                            <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t2_sum['All'] / max(1, $transit_days_t2_count['All']), 2) }}
                            @endif
                        </td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>
                                @if($transit_days_t2_count[$one_city] > 0)
                                <span style="font-weight: bold;">Count:</span> {{ $transit_days_t2_count[$one_city] }}
                                <br/>
                                <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t2_sum[$one_city] / max(1, $transit_days_t2_count[$one_city]), 2) }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                    <tr id="kpi_tier3" class="show_details kpi background-sublist">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; Tier 3</th>
                        <td class='All'>
                            @if($transit_days_t3_count['All'] > 0)
                            <span style="font-weight: bold;">Count:</span> {{ $transit_days_t3_count['All'] }}
                            <br/>
                            <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t3_sum['All'] / max(1, $transit_days_t3_count['All']), 2) }}
                            @endif
                        </td>
                        @foreach($cities as $one_city)
                            <td class='{{ $one_city }}'>
                                @if($transit_days_t3_count[$one_city] > 0)
                                <span style="font-weight: bold;">Count:</span> {{ $transit_days_t3_count[$one_city] }}
                                <br/>
                                <span style="font-weight: bold;">Avegrage:</span> {{ number_format($transit_days_t3_sum[$one_city] / max(1, $transit_days_t3_count[$one_city]), 2) }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php

                // zero attempts
                $zero_attempts_t1_all = 0;
                $zero_attempts_t2_all = 0;
                $zero_attempts_t3_all = 0;
                $zero_attempts_t1_count = 0;
                $zero_attempts_t2_count = 0;
                $zero_attempts_t3_count = 0;
                foreach($zero_attempts as $one_record) {
                    if($one_record->tier == 1) {
                        $zero_attempts_t1_count += $one_record->count;
                    }
                    else if($one_record->tier == 2) {
                        $zero_attempts_t2_count += $one_record->count;
                    }
                    else if($one_record->tier == 3) {
                        $zero_attempts_t3_count += $one_record->count;
                    }
                }
                foreach($all_shipments_zero_attempts as $one_record) {
                    if($one_record->tier == 1) {
                        $zero_attempts_t1_all += $one_record->count;
                    }
                    else if($one_record->tier == 2) {
                        $zero_attempts_t2_all += $one_record->count;
                    }
                    else if($one_record->tier == 3) {
                        $zero_attempts_t3_all += $one_record->count;
                    }
                }
                ?>
                <table class="table table-striped table-bordered">
                    <caption>C.S [{{ $from }}, {{ $to }}]</caption>
                    <tr>
                        <th></th>
                        <th>Quantity</th>
                        <th>Percentage From Total Orders</th>
                    </tr>
                    <tr id="cs_zero_attempts_t1" class="show_details cs">
                        <th style="width: 40%;">Delivered From First Attempt For T1</th>
                        <td>{{ $zero_attempts_t1_count }}</td>
                        <td>{{ number_format($zero_attempts_t1_count / max(1, $all_delivered) * 100, 2) }}% (From Total Delivered)</td>
                    </tr>
                    <tr id="cs_zero_attempts_t2" class="show_details cs">
                        <th>Delivered From First Attempt For T2</th>
                        <td>{{ $zero_attempts_t2_count }}</td>
                        <td>{{ number_format($zero_attempts_t2_count / max(1, $all_delivered) * 100, 2) }}% (From Total Delivered)</td>
                    </tr>
                    <tr id="cs_zero_attempts_t3" class="show_details cs">
                        <th>Delivered From First Attempt For T3</th>
                        <td>{{ $zero_attempts_t3_count }}</td>
                        <td>{{ number_format($zero_attempts_t3_count / max(1, $all_delivered) * 100, 2) }}% (From Total Delivered)</td>
                    </tr>
                    <tr id="cs_returned" class="show_details cs">
                        <th>Total Returned After Second Delivery Attemts</th>
                        <td>{{ $returned_second_attempt }}</td>
                        <td>{{ number_format($returned_second_attempt / max(1, $total_returned) * 100, 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="cs_returned_cs" class="show_details cs">
                        <th>Total Retuned After Second Delivery And Reached Out By Saee C.S</th>
                        <td>{{ $returned_second_attempt_cs }}</td>
                        <td>{{ number_format($returned_second_attempt_cs / max(1, $total_returned) * 100, 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="cs_request_urgent_delivery" class="show_details cs">
                        <th>Request Urgent Delivery</th>
                        <td>{{ $urgent_delivery }}</td>
                        <td>{{ $urgent_delivery . ' (' . number_format($urgent_delivery / max(1, $all_shipments) * 100, 2) }}% (From Total)</td>
                    </tr>
                    <tr id="cs_request_investigation" class="show_details cs">
                        <th>Request Investigation</th>
                        <td>{{ $request_investigation }}</td>
                        <td>{{ number_format($request_investigation / max(1, $total_returned) * 100, 2) }}% (From Total Returned)</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php

                $reasons = UndeliveredReason::all();
                $rs_undelivered = array();
                $rs_undelivered['All'] = 0;
                foreach($reasons as $one_reason) {
                    $rs_undelivered[$one_reason->id] = 0;
                }
                foreach($returned_by_reasons as $one_record) {
                    $rs_undelivered[$one_record->undelivered_reason] += $one_record->count;
                    $rs_undelivered['All'] += $one_record->count;
                }

                $rs_statuses = array();
                $statuses = OrderStatuses::all();
                foreach($statuses as $one_status) {
                    $rs_statuses[$one_status->id] = 0;
                }
                foreach($total_by_status as $one_record) {
                    $rs_statuses[$one_record->status] += $one_record->count;
                }

                $rs_rescheduled = array();
                $rs_rescheduled['All'] = 0;
                foreach($reasons as $one_reason) {
                    $rs_rescheduled[$one_reason->id] = 0;
                }
                foreach($total_rescheduled as $one_record) {
                    $rs_rescheduled[$one_record->undelivered_reason] += $one_record->count;
                    $rs_rescheduled['All'] += $one_record->count;
                }

                ?>
                <table class="table table-bordered">
                    <caption>Returned Shipment [{{ $from }}, {{ $to }}]</caption>
                    <tr>
                        <th style="width: 40%;"></th>
                        <th>Quantity</th>
                        <th>Percentage From Total Orders</th>
                    </tr>
                    <tr id="rs_total_shipments" class="background-title show_details rs">
                        <th>Total Shipments (Starting From <b>Created By Owner</b>)</th> 
                        <td>{{ $all_shipments }}</td>
                        <td></td>
                    </tr>
                    <tr id="rs_total_delivered" class="background-title show_details rs">
                        <th>Delivered Shipments</th> 
                        <td>{{ $all_delivered }}</td>
                        <td>{{ number_format($all_delivered / max(1, $all_shipments), 2) }}% (From Total)</td>
                    </tr>
                    <tr id="rs_total_returned" class="background-title show_details rs">
                        <th>Total Returned (To Warehouse)</th>
                        <td>{{ $total_returned }}</td>
                        <td>{{ number_format($total_returned / max(1, $all_shipments), 2) }}% (From Total)</td>
                    </tr>
                    <tr id="rs_total_returned_unanswered" class="background-sublist show_details rs">
                        <th>Unanswered</th>
                        <td>{{ $rs_undelivered[1] }}</td>
                        <td>{{ number_format($rs_undelivered[1] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_total_returned_rejected" class="background-sublist show_details rs">
                        <th>Rejected</th>
                        <td>{{ $rs_undelivered[2] }}</td>
                        <td>{{ number_format($rs_undelivered[2] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_total_returned_canceled_customer" class="background-sublist show_details rs">
                        <th>Canceled By Custoemr</th>
                        <td>{{ $rs_undelivered[3] }}</td>
                        <td>{{ number_format($rs_undelivered[3] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_total_returned_wrong_address" class="background-sublist show_details rs">
                        <th>Wrong Address</th>
                        <td>{{ $rs_undelivered[8] }}</td>
                        <td>{{ number_format($rs_undelivered[8] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_total_returned_wrong_number" class="background-sublist show_details rs">
                        <th>Wrong Number</th>
                        <td>{{ $rs_undelivered[7] }}</td>
                        <td>{{ number_format($rs_undelivered[7] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_rescheduled_total" class="show_details rs">
                        <th>Reschedulaed</th>
                        <td>{{ $rs_rescheduled['All'] }}</td>
                        <td>{{ number_format($rs_rescheduled['All'] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_rescheduled_enduser" class="show_details rs">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; By End User</th>
                        <td>{{ $rs_rescheduled[15] }}</td>
                        <td>{{ number_format($rs_rescheduled[15] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_rescheduled_captain" class="show_details rs">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; By Captain</th>
                        <td>{{ $rs_rescheduled[14] }}</td>
                        <td>{{ number_format($rs_rescheduled[14] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_rescheduled_saee" class="show_details rs">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; By Saee</th>
                        <td>{{ $rs_rescheduled[6] }}</td>
                        <td>{{ number_format($rs_rescheduled[6] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_rescheduled_companycs" class="show_details rs">
                        <th>&nbsp;&nbsp;&nbsp;&nbsp; &rdca; &nbsp; By Company C.S</th>
                        <td>{{ $rs_rescheduled[16] }}</td>
                        <td>{{ number_format($rs_rescheduled[16] / max(1, $rs_undelivered['All']), 2) }}% (From Total Returned)</td>
                    </tr>
                    <tr id="rs_ofd" class="background-sublist show_details rs">
                        <th>OFD</th>
                        <td>{{ $rs_statuses[4] }}</td>
                        <td>{{ number_format($rs_statuses[4] / max(1, $all_shipments), 2) }}% (From Total)</td>
                    </tr>
                    <tr id="rs_inwarehouse" class="background-sublist show_details rs">
                        <th>In warehouse</th>
                        <td>{{ $rs_statuses[3] }}</td>
                        <td>{{ number_format($rs_statuses[3] / max(1, $all_shipments), 2) }}% (From Total)</td>
                    </tr>
                    <tr id="rs_passed_return_date" class="background-sublist show_details rs">
                        <th> Already passed return due date</th>
                        <td>{{ $passed_return_due_date }}</td>
                        <td>{{ number_format($passed_return_due_date / max(1, $all_shipments), 2) }}% (From Total)</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/sorttable/sorttable.js"></script>

    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css" rel="stylesheet"/>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

    <style>
        .show_details:hover {
            cursor: pointer;
        }
    </style>

    <script>
        // shipmentsIdsExport
        $(document).ready(function(){
            let kpi_row = $(".show_details.kpi");
            let cs_row = $(".show_details.cs");
            let rs_row = $(".show_details.rs");
            let from = $("#from").val();
            let to = $("#to").val();
            let city_selected = $("#cities").children('option:selected');
            let cities_array = [];
            for(let i = 0; i < city_selected.length; ++i)
                cities_array.push(city_selected[i].value);
            let cities = cities_array.join(',');
            console.log(cities);
            console.log('KPI ROW: ', kpi_row);
            console.log('CS ROW: ', cs_row);
            kpi_row.click(function(e) {
                let cell = $(e.target).get(0);
                console.log(cell.tagName + ' - ' + cell.className);
                if(cell.className == 'All') console.log(1);
                else console.log(2);
                if(cell.tagName == 'TD') {
                    if(cell.className != 'All')
                        cities = cell.className;
                    else {
                        let city_selected = $("#cities").children('option:selected');
                        let cities_array = [];
                        for(let i = 0; i < city_selected.length; ++i)
                            cities_array.push(city_selected[i].value);
                        cities = cities_array.join(',');
                    }
                }
                let id = $(this).attr('id');
                window.open('/company/shipmentsExport?for=kpi_report&tabFilter=3&id='+id+'&from='+from+'&to='+to+'&cities='+cities);
            });

            cs_row.click(function(e) {
                let id = $(this).attr('id');
                let city_selected = $("#cities").children('option:selected');
                let cities_array = [];
                for(let i = 0; i < city_selected.length; ++i)
                    cities_array.push(city_selected[i].value);
                cities = cities_array.join(',');
                window.open('/company/shipmentsExport?for=kpi_report&tabFilter=3&id='+id+'&from='+from+'&to='+to+'&cities='+cities);
            });

            rs_row.click(function(e) {
                let id = $(this).attr('id');
                let city_selected = $("#cities").children('option:selected');
                let cities_array = [];
                for(let i = 0; i < city_selected.length; ++i)
                    cities_array.push(city_selected[i].value);
                cities = cities_array.join(',');
                window.open('/company/shipmentsExport?for=kpi_report&tabFilter=3&id='+id+'&from='+from+'&to='+to+'&cities='+cities);
            });

        });
    </script>

@stop
