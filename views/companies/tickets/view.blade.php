@extends('companies.layout')


@section('insert')

    <style>
        .mark {
            color: black;
        }
        table {
            color: black;
        }
        table thead tr th {
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">
        <div class="" style="margin-top: 30px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<h3 class="text-center">Ticket Information</h3>
                	<div class="row" style="width: 80%; margin: auto; margin-top: 20px;">
						<table class="table table-bordered table-stripped" style="text-align: center;"> 
							<thead>
								<tr>
									<th>Created At</th>
									<th>Waybill</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>Type</th>
									<th>Creation Reason</th>
									<th>Last Update</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $ticket->created_at }}</td>
									<td>{{ $ticket->waybill }}</td>
                                    <td>{{ $ticket->priority }}</td>
                                    <td>{{ $status_map[$ticket->status]->english }}</td>
                                    <td>{{ $ticket->type == 0 ? '' : $ticket_types_map[$ticket->type]->english }}</td>
									<td>{{ $ticket->created_for }}</td>
									<td>{{ $ticket->updated_at }}</td>
								</tr>
							</tbody>
						</table>
                	</div>

                	<h3 class="text-center" style="margin-top: 20px;">Ticket Actions History</h3>

                	<div class="row" style="width: 95%; margin: auto; margin-top: 20px;">
                		<table class="table table-bordered table-stripped text-center">
                			<thead>
                				<tr>
                					<th>Date and Time</th>
                					<th>Made By</th>
                					<th>Ticket Priority</th>
                					<th>Status</th>
                					<th>Receiver Name</th>
                					<th>Mobile 1</th>
                					<th>Mobile 2</th>
                					<th>City</th>
                					<th>District</th>
                                    <th>Address</th>
                                    <th>Schedule Date</th>
                					<th>Cancelled</th>
                					<th>Urgent Delivery</th>
                					<th>Comment</th>
                				</tr>
                			</thead>
                			<tbody>
	                		@foreach($actions as $action)
	                			<tr>
	                				<td>{{ $action->created_at }}</td>
	                				<td>{{ $action->company_id == 0 ? 'Saee Team' : $companies_names[$action->company_id] }}</td>
	                				<td class="{{ $action->is_priority ? 'mark' : '' }}">{{ $action->ticket_priority }}</td>
	                				<td class="{{ $action->is_status ? 'mark' : '' }}">{{ $status_map[$action->status]->english }}</td>
	                				<td class="{{ $action->is_name ? 'mark' : '' }}">{{ $action->shipment_name }}</td>
	                				<td class="{{ $action->is_mobile1 ? 'mark' : '' }}">{{ $action->shipment_mobile1 }}</td>
	                				<td class="{{ $action->is_mobile2 ? 'mark' : '' }}">{{ $action->shipment_mobile2 }}</td>
	                				<td class="{{ $action->is_city ? 'mark' : '' }}">{{ $action->shipment_city }}</td>
	                				<td class="{{ $action->is_district ? 'mark' : '' }}">{{ $action->shipment_district }}</td>
	                				<td class="{{ $action->is_address ? 'mark' : '' }}">{{ $action->shipment_address }}</td>
                                    <td class="{{ $action->is_scheduled_date ? 'mark' : '' }}">{{ $action->shipment_scheduled_date == '0000-00-00' ? '' : $action->shipment_scheduled_date }}</td>
                                    <td class="{{ $action->is_is_cancelled ? 'mark' : '' }}">{{ $action->shipment_is_cancelled == 0 ? 'No' : 'Yes' }}</td>
                                    <td class="{{ $action->is_urgent_delivery ? 'mark' : '' }}">{{ $action->shipment_urgent_delivery == 0 ? 'No' : 'Yes' }}</td>
	                				<td>{{ $action->comment }}</td>
	                			</tr>
	                		@endforeach
	                		</tbody>
                		</table>
                	</div>

                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

    

@stop

