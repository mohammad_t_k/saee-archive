@extends('companies.layout')


@section('insert')

    <div class="right_col" role="main">
        <div class="col-xs-12">
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ticket Type <span class="required">*</span></label>
                        <div class="col-md-offset-3">
                            <label><input type="radio" name="type" class="checkbox-inline form-check" value="push_delivery" checked /> Push Delivery</label>
                            <br>
                            <label><input type="radio" name="type" class="checkbox-inline form-check" value="request_investigation" /> Request Investigation</label>
                        </div>
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">ShipmentIDs <span class="required">*</span></label>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <textarea  id="shipmentIDs" name="ids" required class="form-control" type="text" rows="10"></textarea>
                    </div>
                    <div class="theCount">Lines Used: <span id="linesUsed">0</span><div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="div_scheduled_shipment_date">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Schedule On Date <span class="required">(MM-DD-YYYY) *</span></label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input type="date" name="scheduled_shipment_date" id="scheduled_shipment_date" class="form-control col-md-4" value="push_delivery" checked /> 
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="display: none;" id="div_description">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripe the problem <span class="required">*</span></label>
                        <div class="">
                            <textarea name="description" id="description" cols="30" rows="10" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <p><button id="submitForm" class="btn  btn-warning">Submit</button></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="failed_items_section" style="display: none;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Failed Items <i class="fa fa-times" id="correct" aria-hidden="true"></i></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown"></li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable_faileditems" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Reason</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="success_items_section" style="display: none;"> 
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Updated Items <i class="fa fa-check" id="correct" aria-hidden="true"></i></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown"></li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></i>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable_successitems" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Order Number</th>
                                        <th>Order Reference</th>
                                        <th>City</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Mobile</th>
                                        <th>Receiver Mobile 2</th>
                                        <th>Cash On Delivery</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Waybill</th>
                                        <th>Order Number</th>
                                        <th>Order Reference</th>
                                        <th>City</th>
                                        <th>Receiver Name</th>
                                        <th>Receiver Mobile</th>
                                        <th>Receiver Mobile 2</th>
                                        <th>Cash On Delivery</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

    <script>
        $('input[type=radio][name=type]').change(function() {
            if (this.value == 'request_investigation') {
                $("#div_scheduled_shipment_date").css('display', 'none');
                $("#div_description").css('display', '');
            }
            else if(this.value == 'push_delivery') {
                $("#div_scheduled_shipment_date").css('display', '');
                $("#div_description").css('display', 'none');
            }
        });
        $(document).ready(function() {
            let date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
            if(date.getDay() == 5) // day of week (friday)
                date = new Date(new Date().getTime() + 2 * (24 * 60 * 60 * 1000));
            console.log('scheduled_shipment_date: ' + date);
            $("#scheduled_shipment_date").attr('value', date.getFullYear() + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2));
        });
    </script>

    <script>
        var linesUsed = $('#linesUsed');
        $('#shipmentIDs').keydown(function(e) {
            newLines = $(this).val().split("\n").length;
            linesUsed.text(newLines);
        });
        $("#submitForm").click(function() {
            let ids = $("#shipmentIDs").val().split("\n");
            let correct_ids = [];
            ids.forEach(function(e) {
                if(/JC[0-9]{8}KS/.test(e) || /OS[0-9]{8}KS/.test(e) || /^[a-z0-9]+$/i.test(e))
                    correct_ids.push(e);
            });
            if(correct_ids.length == 0) {
                alert("Please enter valid IDs");
                return false;
            }
            console.log('correct_ids', correct_ids);
            let type = $("input[type=radio][name=type]:checked").val();
            let scheduled_shipment_date = $("#scheduled_shipment_date").val();
            let description = '';
            if(type == 'request_investigation') {
                description = $("#description").val();
                if(!description) {
                    alert("Please provide description for your request of investigation");
                    return false;
                }
            }
            if (confirm("Total unique items entered is: " + correct_ids.length + ", confirm?") == true){
                $("#success_items_section").css("display", "none");
                $("#failed_items_section").css("display", "none");
                console.log('request');
                console.log(correct_ids.join(","));
                $.ajax({
                    type: 'POST',
                    url: '/company/ticket/create/post',
                    dataType: 'text',
                    data: {tabFilter:1,  ids:correct_ids.join(","), type: type, scheduled_shipment_date: scheduled_shipment_date, description: description},
                }).done(function (response) {
                    console.log('response');
                    console.log(response);
                    responseObj = JSON.parse(response);
                    if(responseObj.error_message) {
                        alert(responseObj.error_message);
                        return;
                    }
                    if (responseObj.success) {
                        alert(responseObj.success_items.length + ' items have been updated. You can start the disruption');
                        $("#datatable_successitems").DataTable().destroy();
                        if(responseObj.success_items.length) {
                            let rows = [];
                            responseObj.success_items.forEach(function(element) {
                                if(!element.order_reference)
                                    element.order_reference = ''
                                if(!element.d_city)
                                    element.d_city = '';
                                if(!element.receiver_name)
                                    element.receiver_name = '';
                                if(!element.receiver_phone)
                                    element.receiver_phone = '';
                                if(!element.receiver_phone2)
                                    element.receiver_phone2 == '';
                                rows.push([
                                    element.jollychic,
                                    element.order_number,
                                    element.order_reference,
                                    element.d_city,
                                    element.receiver_name,
                                    element.receiver_phone,
                                    element.receiver_phone2,
                                    element.cash_on_delivery
                                ]);
                            });
                            $("#datatable_successitems").DataTable({
                                "data": rows,
                                "autoWidth": false,
                                dom: "Blfrtip",
                                buttons: [
                                    {
                                        extend: "copy",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "csv",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "excel",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "pdfHtml5",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "print",
                                        className: "btn-sm"
                                    },
                                ],
                                responsive: true,
                                'order': [[0, 'asc']]
                            });
                            $("#success_items_section").css("display", "");
                        }
                        if(responseObj.failed_items.length) {
                            $("#datatable_faileditems").DataTable().destroy();
                            let rows = [];
                            responseObj.failed_items.forEach(function(element) {
                                rows.push([element.waybill, element.error]);
                            });
                            $("#datatable_faileditems").DataTable({
                                "data": rows,
                                "autoWidth": false,
                                dom: "Blfrtip",
                                buttons: [
                                    {
                                        extend: "copy",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "csv",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "excel",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "pdfHtml5",
                                        className: "btn-sm"
                                    },
                                    {
                                        extend: "print",
                                        className: "btn-sm"
                                    },
                                ],
                                responsive: true,
                                'order': [[0, 'asc']]
                            });
                            $("#failed_items_section").css("display", "");
                        }
                    }
                    else
                        alert('Something went wrong!'   );
                });
            }
        });
    </script>
    
@stop

