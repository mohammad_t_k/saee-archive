@extends('companies.layout')


@section('insert')

    <div class="right_col" role="main">
        <div class="tab_container">
            <input id="tab1" type="radio" name="tabs" checked>
            <label for="tab1" class='label2'><i class="fa fa-pencil-square-o"></i><span>Export Bulk Tickets</span></label>

            <input id="tab2" type="radio" name="tabs" <?php if ($tabFilter == 2) echo 'checked' ?> >
            <label for="tab2" class='label2'><i class="fa fa-level-down"></i><span> Filter Tickets </span></label>

            <section id="content1" class="tab-content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <!-- table start -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Export Tickets by Waybill</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <form class="multiple-ids" action="/company/ticket/export/bulk" method="post">
                                        <input id="tabFilter" name='tabFilter' type="hidden" value="1">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipment IDs <span class="required">*</span></label>
                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <textarea id="shipmentIDs" name="ids" required class="form-control" type="text" rows="10"></textarea>
                                            </div>
                                            <div class="theCount">(Max 100) Lines used: <span id="linesUsed">0</span><div></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <p>
                                                            <button id="submitForm" class="btn  btn-warning">Submit</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div style="margin-top: 20px;"></div>
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 col-md-offset-4" style="display: inline-block; margin-right: 20px;" >
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <button type="button" id="copy_waybills_bulk" class="btn btn-warning">Copy All Waybills</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin-bottom: 20px;"></div>
                                    <table id="datatable_bulk" class="table table-striped table-bordered" style="text-align: center;">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;">Created At</th>
                                            <th style="text-align: center;">Waybill</th>
                                            <th style="text-align: center;">Priority</th>
                                            <th style="text-align: center;">Status</th>
                                            <th style="text-align: center;">Creation Reason</th>
                                            <th style="text-align: center;">Last Comment</th>
                                            <th style="text-align: center;">Details</th>
                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th style="text-align: center;">Created At</th>
                                            <th style="text-align: center;">Waybill</th>
                                            <th style="text-align: center;">Priority</th>
                                            <th style="text-align: center;">Status</th>
                                            <th style="text-align: center;">Creation Reason</th>
                                            <th style="text-align: center;">Last Comment</th>
                                            <th style="text-align: center;">Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="content2" class="tab-content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <!-- table start -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <form method="get" action="/company/ticket/export/filter">
                                                <input id="tabFilter" name='tabFilter' type="hidden" value="2">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">City <span class="required">*</span></label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                        <select name="city" id="city" class="form-control">
                                                            @foreach($adminCities as $adcity)
                                                                <option value="{{$adcity->city}}" <?php if (isset($city) && $city == $adcity->city) echo 'selected';?> >{{$adcity->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label class="control-label col-md-4 col-sm-6 col-xs-12">Status <span class="required">*</span></label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12 input-css">
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="0" <?php if (isset($status) && $status == 0) echo 'selected';?> >All</option>
                                                            @foreach($all_statuses as $one_status)
                                                                <option value="{{$one_status->id}}" <?php if (isset($status) && $status == $one_status->id) echo 'selected';?> >{{$one_status->english}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" style="display: inline-block; margin-top: 15px;" >
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <input type="submit" value="Filter" class="btn btn-warning" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" style="display: inline-block; margin-top: 15px;" >
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <button type="button" id="copy_waybills_filter" class="btn btn-warning">Copy All Waybills</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable_filter" class="table table-striped table-bordered " style="text-align: center;">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;">Created At</th>
                                            <th style="text-align: center;">Waybill</th>
                                            <th style="text-align: center;">Priority</th>
                                            <th style="text-align: center;">Status</th>
                                            <th style="text-align: center;">Creation Reason</th>
                                            <th style="text-align: center;">Last Comment</th>
                                            <th style="text-align: center;">Details</th>
                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th style="text-align: center;">Created At</th>
                                            <th style="text-align: center;">Waybill</th>
                                            <th style="text-align: center;">Priority</th>
                                            <th style="text-align: center;">Status</th>
                                            <th style="text-align: center;">Creation Reason</th>
                                            <th style="text-align: center;">Last Comment</th>
                                            <th style="text-align: center;">Details</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/DateJS/build/date.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

    <script>
        @if(isset($_GET['error']) && $_GET['error'] == 1)
            bootbox.alert("Ticket does not exist!");
        @endif
    </script>

    <script>
        $(document).ready(function () {
            var lines = 100;
            var linesUsed = $('#linesUsed');
            $('#shipmentIDs').keydown(function(e) {
                newLines = $(this).val().split("\n").length;
                linesUsed.text(newLines);
                if(e.keyCode == 13 && newLines >= lines) {
                    linesUsed.css('color', 'red');
                    return false;
                }
                else {
                    linesUsed.css('color', '');
                }
            });
        });
    </script>

    <script>
    
        <?php
        
        $bulk_data = '';
        foreach($bulk_tickets as $record) {
            $status = $status_map[$record->status]->english;
            $created_for = addslashes($record->created_for);
            $last_comment = addslashes($record->comment);
            $showlink = "<a href='/company/ticket/view/$record->waybill' target='_blank'>Show</a>";
            $bulk_data .= "[\"$record->created_at\", \"$record->waybill\", \"$record->priority\", \"$status\", \"$created_for\", \"$last_comment\", \"$showlink\" ], ";
        }

        $filter_data = '';
        foreach($filter_tickets as $record) {
            $status = $status_map[$record->status]->english;
            $created_for = addslashes($record->created_for);
            $last_comment = addslashes($record->comment);
            $showlink = "<a href='/company/ticket/view/$record->waybill' target='_blank'>Show</a>";
            $filter_data .= "[\"$record->created_at\", \"$record->waybill\", \"$record->priority\", \"$status\", \"$created_for\", \"$last_comment\", \"$showlink\" ], ";
        }
        
        ?>

        $(document).ready(function () {
            $("#datatable_bulk").DataTable({
                "data": [
                    <?php echo $bulk_data ?>
                ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                'order': [[0, 'asc']]
            });
        });

        $(document).ready(function () {
            $("#datatable_filter").DataTable({
                "data": [
                    <?php echo $filter_data ?>
                ],
                "autoWidth": false,
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                'order': [[0, 'asc']]
            });
        });

        $("#copy_waybills_bulk").click(function(){
            <?php
            $waybills = "";
            foreach($bulk_tickets as $record) {
                $waybills .= "$record->waybill\n";
            }
            ?>
            let str = `{{ $waybills }}`;
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert("Copied");
        });

        $("#copy_waybills_filter").click(function(){
            <?php
            $waybills = "";
            foreach($filter_tickets as $record) {
                $waybills .= "$record->waybill\n";
            }
            ?>
            let str = `{{ $waybills }}`;
            const el = document.createElement('textarea');
            el.value = str;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            alert("Copied");
        });

    </script>
    
@stop

