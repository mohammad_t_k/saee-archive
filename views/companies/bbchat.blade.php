<!DOCTYPE html>
<html lang="en">
<head>
    <title>Black Box</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Black Box Chat interface" />
    <meta name="author" content="Danish Jamshed" />
    <meta name="keywords" content="xmpp chat blackbox" />

    <link rel="shortcut icon" type="image/ico" href="css/images/favicon.ico"/>
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/website.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo asset_url(); ?>/bbchat/converse/css/converse.css" />
    <script src="<?php echo asset_url(); ?>/bbchat/converse/src/website.js"></script>
    <script type="text/javascript" src="<?php echo asset_url(); ?>/bbchat/converse/analytics.js"></script>
    <noscript><p><img src="//stats.opkode.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
    <![if gte IE 9]>
        <script src="<?php echo asset_url(); ?>/bbchat/converse/dist/converse.js"></script>
    <![endif]>
</head>

<body id="page-top" data-spy="scroll">

</body>

<?php
// checking if user is saee agent or company agent
if($is_admin == 1){
$admin_username = 'saee_agent_'.$admin_username.'_'.$admin_id.'@k-w-h.com';
}else{

$admin_username = 'company_'.$admin_username.'_'.$admin_id.'@k-w-h.com';
}

 ?>

<script>
   converse.initialize({
        authentication: 'login',
        allow_registration: false,
        auto_away: 300,
        registration_domain: 'k-w-h.com',
        default_domain: 'k-w-h.com', // so on login page you don't have to specify username with domain
        auto_reconnect: true,
        bosh_service_url: 'http://www.k-w-h.com:5280/bosh/', // Please use this connection manager only for testing purposes
        message_archiving: 'always',
        play_sounds: true,
        notify_all_room_messages: true,
        sounds_path: '/bbchat/converse/sounds/',
        view_mode: 'fullscreen',
        auto_login: true,
        debug: true,
        jid: '<?php echo $admin_username ?>',
        password: '<?php echo $admin_password ?>'
    }).then(function () {
        setTimeout(function () {
            document.querySelector('.converse-content').style = '';
        }, 1000);
    });
</script> 
</html>
