@extends('companies.layout')

@section('dashboard')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                <br/><br/><br/>

                <form role="form" id="main-form" method="post" action="/company/subcompanies/addpost" enctype="multipart/form-data" onsubmit="return validate()">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />

                    <!------------------------------------------------------------------------ Personal Inforamtion ------------------------------------------------------------------------>
                    <br/>
                    <label class="control-label col-md-6 col-sm-3 col-xs-12"><h2><span class="fa fa-building"></span> &nbsp; <b>Sub Company Information</b> </h2> </label>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Username (إسم المستخدم) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="username" name="username" class="form-control col-md-7 col-xs-12" placeholder="Type Your Username" type="text" maxlength="200" required="required">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Password (كلمة المرور) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="password" name="password" class="form-control col-md-7 col-xs-12" type="password" maxlength="200" required="required">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">English Name (الاسم الإنجليزي) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="company_name" name="company_name" class="form-control col-md-7 col-xs-12" placeholder="English Name" type="text" maxlength="200" required="required">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Arabic Name (الاسم العربي) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="name_ar" name="name_ar" class="form-control col-md-7 col-xs-12"  placeholder="Arabic Name"type="text" maxlength="200" required="required" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Email (البريد الإلكتروني) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="Email" type="email" maxlength="200" required="required" >
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Phone Number (رقم الهاتف) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="phone" name="phone" class="form-control col-md-7 col-xs-12" placeholder="966555555555" type="number" maxlength="200" required="required" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12" required="required">City (المدينة) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <select name="city" id="city" class="form-control" onchange="onChangeCity()" required="required">
                                <option value="no_city" id="city_empty"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">District (الحي) </label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <select name="district" id="district" class="form-control">
                                <option value="" id="district_empty"></option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12" required="required">Sub Company Address (عنوان الفرع) <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="address" name="address" class="form-control col-md-7 col-xs-12" placeholder="Sub Company Address" type="text" maxlength="200" required="required">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Owner Name (إسم المالك)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="owner_name" name="owner_name" class="form-control col-md-7 col-xs-12" placeholder="Owner Full Name" type="text" maxlength="200" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Owner ID (رقم هوية المالك)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="owner_id" name="owner_id" class="form-control col-md-7 col-xs-12" placeholder="Owner ID" type="number" maxlength="200" >
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12" >Company Registration (رقم الرخصة)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="company_cr" name="company_cr" class="form-control col-md-7 col-xs-12" placeholder="Registration Number" type="number" maxlength="200" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">ZIP Code (صندوق بريدي) </label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="zipcode" name="zipcode" class="form-control col-md-7 col-xs-12" placeholder="" type="text" maxlength="200" >
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Customer Support Phone (رقم هاتف خدمة العملاء)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="customer_support_phone" name="customer_support_phone" class="form-control col-md-7 col-xs-12" placeholder="Customer Support Phone" type="number" maxlength="200" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Customer Support Email (البريد الإلكتروني لخدمة العملاء)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="customer_support_email" name="customer_support_email" class="form-control col-md-7 col-xs-12" placeholder="Customer Support Email" type="email" maxlength="200" >
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Bank Name (إسم البنك)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="bank_name" name="bank_name" class="form-control col-md-7 col-xs-12" placeholder="Bank Name" type="number" maxlength="200" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group" >
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">IBAN (الآيبان)</label>
                        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 ">
                            <input id="iban" name="iban" class="form-control col-md-7 col-xs-12" placeholder="IBAN" type="number" maxlength="200" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>

                    <!------------------------------------------------------------------------ Update Location ------------------------------------------------------------------------>
                    <br/>
                    <div class="clearfix"></div>
                    <label class="control-label col-md-8 col-sm-3 col-xs-12"><h2><span class="fa fa-map-marker"></span> &nbsp; <b>Location</b> </h2></label>
                    <div class="clearfix"></div>
                    <br/>
                    <div class=" col-md-12 col-lg-12 col-xs-12">
                        <div class="col-md-12 col-xs-12">
                            <div id="map_canvas" style="height: 500px; width: 100%; margin: 10px;"></div>
                            <input id="latitude" name="latitude" value="21.591406" hidden />
                            <input id="longitude" name="longitude" value="39.175951" hidden />
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXHuxvdi42weveUmkEpewcXH-Aj0i2fZs&callback=initMap" async defer></script>
                            <script>
                                function initMap() {
                                    try {
                                        var address_latitude=21.591406;
                                        var address_longitude=39.175951;
                                        var trackingnum = '';
                                        var mapOptions = {
                                            zoom: 15,
                                            center: new google.maps.LatLng(address_latitude, address_longitude),
                                            mapTypeId: 'terrain'
                                        };
                                        document.getElementById('map_canvas').style = 'height:200px';
                                        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                                        var marker = new google.maps.Marker({
                                            position: new google.maps.LatLng(address_latitude, address_longitude),
                                            map: map,
                                            title: 'Hello World!'
                                        });
                                        infoWindow = new google.maps.InfoWindow;
                                        google.maps.event.addListener(map, 'center_changed', function () {
                                            address_latitude= document.getElementById('latitude').value = map.getCenter().lat();
                                            address_longitude= document.getElementById('longitude').value = map.getCenter().lng();
                                            marker.setVisible(false);
                                        });
                                        $('#map_canvas').addClass('centerMarker').appendTo(map.getDiv())
                                        //do something onclick
                                            .click(function () {
                                                var that = $(this);
                                                if (!that.data('win')) {
                                                    that.data('win', new google.maps.Marker({
                                                        position: new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()),
                                                        map: map,
                                                        title: 'Place This Marker on You Location'
                                                    }));
                                                    that.data('win').bindTo('position', map, 'center');
                                                }
                                                that.data('win').open(map);
                                            });
                                    }
                                    catch (e) {
                                        alert(e);
                                    }
                                }
                            </script>
                            <script>google.maps.event.addDomListener(window, 'load', initMap);</script>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <!------------------------------------------------------------------------ Submit Form ------------------------------------------------------------------------>
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 col-sm-offset-4 col-xs-12 form-group">
                        <button type="submit" class="btn btn-primary btn-flat btn-block" style="text-align: center;">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- morris.js -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/morris.js/morris.min.js"></script>
    <!-- ECharts -->
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/echarts/map/js/world.js"></script>

    <script>
        // fill cities
        $(document).ready(function(){
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getallcities',
                dataType: 'text'
            }).done(function(response){
                responseObj = JSON.parse(response);
                if(responseObj.success) {
                    var cities = responseObj.cities;
                    var select = document.getElementById('city');
                    for(var i = 0; i < cities.length; ++i) {
                        option = document.createElement('option');
                        option.setAttribute('value', cities[i].name);
                        option.appendChild(document.createTextNode(cities[i].name + ' - ' + cities[i].name_ar));
                        select.appendChild(option);
                    }
                }
            });
        });
    </script>

    <script>
        function onChangeState() {
            document.getElementById('state_empty').style = 'display: none';
        }
        function onChangeCity() {
            $("#district").empty();
            var city = document.getElementById('city').value;
            document.getElementById('city_empty').style = 'display: none';
            $.ajax({
                type: 'GET',
                url: '/deliveryrequest/getdistrictsbycity',
                dataType: 'text',
                data: {city: city},
            }).done(function (response) {
                responseObj = JSON.parse(response);
                if (responseObj.city == city) {
                    var districts = responseObj.districts;
                    var select = document.getElementById('district');
                    option = document.createElement('option');
                    option.setAttribute('value', '');
                    option.appendChild(document.createTextNode(' '));
                    select.appendChild(option);
                    for (var i = 0; i < districts.length; i += 1) {
                        if(districts[i].district == 'No District')
                            continue;
                        option = document.createElement('option');
                        option.setAttribute('value', districts[i].district);
                        option.appendChild(document.createTextNode(districts[i].district));
                        select.appendChild(option);
                    }
                }
            });
        }
    </script>

    <script>
        function validate() {
            var city = document.getElementById("city").value;
            var district = document.getElementById("district").value;
            if(city == 'no_city'){
                alert('Please Fill City');
                return false;
            }
            if(district == 'no_district'){
                alert('Please Fill District');
                return false;
            }
            return true;
        }
    </script>

@stop