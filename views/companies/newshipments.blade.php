@extends('companies.exlayout')


@section('content')


<?php
//print_r($totalnewrecords);exit();
?>

        <!-- page content -->

<!-- page content -->
<div class="" role="main">
    <div class="rigtside">

        <div class="page-title">
            <div class="title_left">
                <h3>Total New Shipments
                </h3>
            </div>
        </div>

        <!-- /top tiles -->
        <div class="row">
            <div class="col-md-12 col-sm-12 new-ship">

                <form method="get" action="/company/totalNewShipments/">


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label colm col-md-2 col-sm-6 col-xs-12">City <span
                                    class="required">*</span>
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12 new-css">
                            <select name="city" id="city" class="form-control">
                                @foreach($adminCities as $adcity)
                                    <option value="{{$adcity->city}}"<?php if (isset($city) && $city == $adcity->city) echo 'selected';?>>{{$adcity->city}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6  col-sm-12 col-xs-12 form-group">
                        <label class="control-label colm col-md-2 col-sm-6 col-xs-12">Start Date
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12 new-css">
                            <input id="start_date" name="start_date" value="{{$start_date}}"
                                   class="date-picker form-control col-md-7 col-xs-12"
                                   type="date">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <label class="control-label colm col-md-2 col-sm-6 col-xs-12">End Date
                        </label>

                        <div class="col-md-8 col-sm-6 col-xs-12 new-css">
                            <input id="end_date" name="end_date" class="date-picker form-control col-md-7 col-xs-12"
                                   value="{{$end_date}}"
                                   type="date">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <p>
                                <button id="submitForm" class="btn-cs btn-warning ">Filter</button>
                            </p>
                        </div>
                    </div>


                </form>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <table id="datatable_summary" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Count</th>
                                <th>Amount</th>

                            </tr>
                            </thead>


                            <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>Count</th>
                                <th>Amount</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>
<!-- /page content -->


<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/nprogress/nprogress.js"></script>


<!-- iCheck -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo asset_url(); ?>/warehouseadmin/vendors/pdfmake/build/pdfmake.min.js"></script>

<script type="text/javascript"
        src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

<script>

    <?php
    $mydata = '';
    $singledata = '';
    $totalcount=0;
    $totalamount=0;
    foreach ($totalnewrecords as $record) {
            $shipdate = $record["shipmentdate"];
            $newcount = $record["newcount"];
            $totalcount+=$newcount;
            $newamount = round($record["newamount"],2);
            $totalamount+=$newamount;

              $mydata .= "[ \"$shipdate\", \"$newcount\" , \"$newamount\"], ";
            }
            $grandcount=$totalcount- $returnedcount;
            $grandamount=$totalamount- $returnedamount;
            $returnedamount=number_format($returnedamount,2);
            $grandamount=number_format($grandamount,2);
            $mydata.= "[ \"Supplier Returned \", \"$returnedcount\" , \"$returnedamount\"], ";
            $mydata.= "[ \"Sub Total\", \"$totalcount\" , \"$totalamount\"], ";
            $mydata.= "[ \"Total\", \"$grandcount\" , \"$grandamount\"], ";

        ?>


        $(document).ready(function () {

                $("#datatable_summary").DataTable({

                    "data": [
                        <?php echo $mydata ?>

                      ],
                    "autoWidth": false,

                    dom: "Blfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true,

                    'order': [[0, 'desc']]
                });

            });
</script>
@stop
