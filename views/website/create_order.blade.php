<!DOCTYPE html>
<html>
    <head>
        <title>:. Create Order :.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/newweb/css/custom.css">
        <link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/newweb/css/font-awesome.css">
        <link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/newweb/css/niceCountryInput.css">
        <style>
            .niceCountryInputMenu{
                height: 48px;
                padding-top: 12px;
            }
        </style>
    </head>
<body>
    <header class="topbgcolor">
        <div class="container">
            <div class="logotxt"><a href="/"><img src="newweb/images/logo.png" class="img-responsive"></a></div>
            <div class="navtop"><a href="../changelanguage?language=ar&amp;page=/registercompanyar">Arabic</a></div>
        </div>
    </header>
    <div id="Menu8" class="float-section company-registration">
        <div class="container">
            <div class="col-md-12" >
                <h2 class="company-heading">ORDER CREATION</h2>
                <h4>
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                    @endif
                </h4>
                <h4>
                    @if(Session::has('success'))
                        <div class="alert alert-danger greenclass">
                            <b>{{ Session::get('success') }}</b> 
                        </div>
                    @endif
                </h4>
            </div>
            <div style="color:white;padding-bottom: 15px;" id="form-messages"></div>
                <form  method="post" action="/registercompanypost" role="form" enctype="multipart/form-data" />
                    <div class="row">
                        <div class="col-md-4 input-group-feild-gray"><label>Sender Name</label><input required="" type="text" value="Sender Name*" name="sender_name" placeholder="Owner Name*"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Sender Phone</label><input required="" type="text" value="Sender Phone*" name="sender_phone" placeholder="Owner Phone*"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Sender Email</label><input required="" type="text" value="Sender Email*" name="sender_email" placeholder="Owner Email*"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Sender City</label><input required="" type="text" value="Sender City*" name="receiver_name" placeholder="Owner Email*"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Receiver Name</label><input required="" type="text" value="Receiver Name*" name="receiver_name" placeholder="Username *"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Receiver Phone</label><input required="" type="text" value="Receiver Phone*" name="receiver_phone" placeholder="Password *"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Receiver Email</label><input type="text" value="Receiver Email*" name="receiver_email" placeholder="Customer Support Phone *"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Receiver City</label><input type="text" value="Receiver City*" name="receiver_city" placeholder="Customer Support Phone *"></div>
                        <div class="col-md-4 input-group-feild-gray"><label>Shipment Description</label><textarea class="form-control" value="Shipment Description" name="iban" placeholder="What The Shipment Contains?"></textarea></div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 form-group"><input type="checkbox" value="true" id="pickup_option" name="pickup_option" class="checkbox-inline form-check">&nbsp;<label class="control-label">Need To Pickup Shipment?</label></div>
                        <div class="clear"></div>
                        <div style="display: none;" class="col-md-4 input-group-feild-gray pickup_info"><label>Pickup District</label><input type="text" value="Pickup District*" name="pickup_district" placeholder="Customer Support Phone *"></div>
                        <div style="display: none;" class="col-md-4 input-group-feild-gray pickup_info"><label>Pickup Address</label><input type="text" value="Pickup Address*" name="pickup_address" placeholder="Customer Support Phone *"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 input-group-feild text-center">
                            <button type="submit" class="submit-btn">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container">
                <div class="footertxt">
                    <p>&copy; 2018 Allright Reseverd</p>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScript Includes --> 
    <script src="<?php echo asset_url(); ?>/newweb/js/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
    <script src="<?php echo asset_url(); ?>/newweb/js/niceCountryInput.js"></script>
    <script>
        $(function() {
            // Get the form.
            var form = $('#ajax-contact');

            // Get the messages div.
            var formMessages = $('#form-messages');

            // TODO: The rest of the code will go here...
            // Set up an event listener for the contact form.
            $(form).submit(function(event) {
                // Stop the browser from submitting the form.
                event.preventDefault();

                // Serialize the form data.
                var formData = $(form).serialize();
        
                var country = document.getElementsByClassName("niceCountryInputMenuInputHidden");

                country = country[0].value;
                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: $(form).attr('action'),
                    data: formData
                }).done(function(response) {
                    // Make sure that the formMessages div has the 'success' class.
                    $(formMessages).removeClass('error');
                    $(formMessages).addClass('success');

                    // Set the message text.
                    $(formMessages).text(response);

                    // Clear the form.
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                }).fail(function(data) {
                    // Make sure that the formMessages div has the 'error' class.
                    $(formMessages).removeClass('success');
                    $(formMessages).addClass('error');

                    // Set the message text.
                    if (data.responseText !== '') {
                        $(formMessages).text(data.responseText);
                    } else {
                        $(formMessages).text('Oops! An error occured and your message could not be sent.');
                    }
                });
            });
        });
    </script>

    <script>
            function onChangeCallback(ctr){
                console.log("The country was changed: " + ctr);
                //$("#selectionSpan").text(ctr);
            }

            $(document).ready(function () {
                $(".niceCountryInputSelector").each(function(i,e){
                    new NiceCountryInput(e).init();
                });
            });
    </script>

    <script>
        $("#pickup_option").change(function() {
            let val = $("#pickup_option").is(":checked");
            if(val)
                $(".pickup_info").css("display", "");
            else
                $(".pickup_info").css("display", "none");
        });
    </script>
    
</body>
</html>
