<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href='https://fonts.googleapis.com/css?family=Sintony:400,700' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="<?php echo asset_url(); ?>/newweb/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="<?php echo asset_url(); ?>/newweb/css/style-arabic.css"> <!-- Resource style -->
    <link rel="stylesheet" href="<?php echo asset_url(); ?>/newweb/css/custom-arabic.css"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/newweb/css/custom-arabic.css">
<link type="text/css" rel="stylesheet" href="<?php URL::to('/'); ?>/newweb/css/font-awesome.css">
  <script src="<?php echo asset_url(); ?>/newweb/js/modernizr.js"></script> <!-- Modernizr -->
    
  <title>:. Saee :.</title>
</head>
<style type="text/css">
 
 .mobile-popup{
  display: none;
 }
@media only screen and (max-width: 768px) {

 .mobile-popup{
  height: 100%;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 99999999;
  background-color: rgba(0,0,0, 0.9);
  display: block;
}
.mobile-logo {

    text-align: center;
    margin-top: 60px;

}
.download-wrapper {

    background: white !important;
    min-height: 250px;
    margin-left: 30px;
    margin-right: 30px;
    margin-top: 50px;
    border-radius: 5px;

}
.get-saee-app {
    text-align: center;
    padding-top: 40px;
    text-transform: uppercase;
}
.on-store{
    text-align: center;
    line-height: 1.1;
    text-transform: uppercase;
    color:#F8931D;
}
.continue-button {
    background: #F8931D;
    color: white;
    text-transform: uppercase;
    padding: 15px;
    font-weight: bold;
    border-radius: 5px;
}

.continue-wrapper {
    text-align: center;
    margin-top: 40px;
}
.app-store {
    text-align: center;
}
.app-store img {
    width: 165px;
    margin-top: 20px;
    text-align: center;
}
.closebtn {
  position: absolute;
  top: 20px;
  right: 45px;
  font-size: 60px;
  color:#F8931D;
}

}

</style>
<body>

<nav class="cd-vertical-nav">
  <ul>   
   
    <li><a href="#section1" class="active"><span class="label">الرئيسية</span></a></li>
     <li><a href="#section2"><span class="label">كن ساعي</span></a></li>
    <li><a href="www.google.com" class="prei" target="_new"><span class="label">تتبع شحنتك</span></a></li>
    <li><a href="#section3"><span class="label">عملائنا</span></a></li>
    <li><a href="#section5"><span class="label">فروعنا</span></a></li>
  </ul>
</nav><!-- .cd-vertical-nav -->

<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section id="section1" class="cd-section">
  <div class="content-wrapper">
    <div class="about-section float-section" id="Menu1">
         <a href="../changelanguage?language=en&amp;page=/" class="menu-logo"> <img src="<?php echo asset_url(); ?>/newweb/images/english.png"></a>
  <h3 class="img-slide-box"><span>معظم منصات التجارة الالكترونية تواجه مشاكل التوصيل في الميل الأخير ساعي 
يقدم عمليات توصيل مبتكرة تدعم معدلات نجاح التوصيل وترفع العائدات</span><img src="<?php echo asset_url(); ?>/newweb/images/logo.png"></h3>

<div style="margin: 0 auto;width:75%" class="graybg2">
                <div class="input-wrap">
                    
                    <div class="tracking-field web-tracking">
                        <form action="/trackingpage" method="GET">
                    <input style="text-align: right;padding-right: 20px;" required="" placeholder="رقم الشحنة" type="text" name="trackingnum" id="trackingnum" value="">
                    <input class="submit-tracking btn-search" type="submit"  value="ابحث">
                        </form>
                </div>
            </div><!-- end input wrap -->
            </div>
<div class="img-slide-box2"><iframe width="100%" height="600" src="https://www.youtube.com/embed/U-7WpFbYEfc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>

  <div class="slide-caption caption-btn">
  <a href="/#section5" class="contact-btn-icon">
  <img src="<?php echo asset_url(); ?>/newweb/images/mgs-icon.png">
  <span>تواصل معنا</span>
  </a>
  
  <a href="<?php echo asset_url(); ?>/newweb/resources/Saee_profile.pdf" download class="updateright"><img src="<?php echo asset_url(); ?>/newweb/images/upload-btn.png">حمل الملف التعريفي</a>
   
    
  </div>
</div>
  </div>
</section><!-- cd-section -->

<section id="section2" class="cd-section">
  <div class="content-wrapper">
    <div class="container">
       
    <h2 class="main-heading">كن ساعي</h2>
    <p class="main-pre-light">حمل التطبيق الان وسجل وكن ساعي</p>
    <div class="col-md-7 rs-center"> <iframe width="100%" height="389" src="https://www.youtube.com/embed/1EkVbIMPm9Y?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <a style="margin-right: 15px;" href="/registercompany" class="regester-caption">تسجيل الشركات </a> <a href="/provider/signup" class="regester-caption">تسجيل الكباتن </a> </div>
    <div class="col-md-2 app-btns"> <a href="#"><img src="<?php echo asset_url(); ?>/newweb/images/google-btn.png"></a> <a target="_blank" href="https://itunes.apple.com/us/app/saee/id1332466731?ls=1&mt=8"><img src="<?php echo asset_url(); ?>/newweb/images/app-store-btn.png"></a> <a href="#"><img src="<?php echo asset_url(); ?>/newweb/images/share-btn.png"></a> </div>
    <div class="col-md-3 text-center mobright"><img src="<?php echo asset_url(); ?>/newweb/images/mob-bc2.png" width="85%">
    <div class="contact-btn-mb">
  
    
    </div>
    </div>
  </div>
  </div>
</section><!-- cd-section -->

<section id="section3" class="cd-section">
  <div class="content-wrapper">
    <div class="container">
  <h2 class="main-heading inline-h">عملاء<p class="main-pre-light inline-p">نفخر بخدمتهم</p></h2>
  
        <div class="brand-list">
        
        <div class="col-md-12">
        <div class="col-md-4"> <li><img src="<?php echo asset_url(); ?>/newweb/images/brandimg1.png" alt="brand"></li></div>
        <div class="col-md-4">  <li><img src="<?php echo asset_url(); ?>/newweb/images/brandimg6.png" alt="brand"></li></div>
        <div class="col-md-4">  <li><img src="<?php echo asset_url(); ?>/newweb/images/brandimg3.png" alt="brand"></li></div>
        </div>
        
        <div class="col-md-12">
        <div class="col-md-4"> <li><img src="<?php echo asset_url(); ?>/newweb/images/brandimg4.png" alt="brand"></li></div>
        <div class="col-md-4"> <li><img src="<?php echo asset_url(); ?>/newweb/images/brandimg5.png" alt="brand"></li></div>
       
        <div class="contact-btn-mb">
      
          </div>
        </div>
        </div>
        
         
        </div>
        
        
        
  </div>
  </div>
</section><!-- cd-section -->

<section id="section4" class="cd-section">
  <div class="content-wrapper">
  <div class="container">
<h2 class="main-heading inline-h">فروعنــا</h2>
<div class="row">
<div class="col-md-6 map-icon"><img src="<?php echo asset_url(); ?>/newweb/images/map-icpn.png" width="100%"></div>
<div class="col-md-6 map-detail">
<ul class="footer-list">

<li><p>المنطقة الشرقية</p>
<ul>
<li><a href="#">الدمام.</a></li>
<li><a href="#">الخبر.</a></li>
<li><a href="#">الظهران.</a></li>
<li><a href="#">سيهات.</a></li>
<li><a href="#">القطيف تارون .</a></li>
<li><a href="#">جعيمة .</a></li>
<li><a href="#">رأس  تنورة</a></li>

</ul></li>
<li><p>الباحه</p>
<ul>
<li><a href="#">الباحة .</a></li>
<li><a href="#">العقيق .</a></li>
<li><a href="#">المندق.</a></li>
<li><a href="#">الاطاولة .</a></li>
<li><a href="#">بلجرشي.</a></li>
<li><a href="#">المخواة المظيلف.</a></li>
<li><a href="#">قلوه .</a></li>
<li><a href="#">ناوان</a></li>

</ul></li>
<li><p>الطائف</p>
<ul>
<li><a href="#">السر.</a></li>
<li><a href="#">السديرة .</a></li>
<li><a href="#">-كلاخ .</a></li>
<li><a href="#">قيا .</a></li>
<li><a href="#">شقصان .</a></li>
<li><a href="#">العرفاء.</a></li>

</ul></li>
<li><p>مكة المكرمة </p>
<ul>
<li><a href="#">النوارية .</a></li>
<li><a href="#">الجعرانة.</a></li>


</ul></li>
<li><p>الرياض</p></li>
<li><p>جدة</p></li>
</ul>
<div class="contact-btn-mb">
     
          </div>
</div>
</div>
</div>



  </div>
</section><!-- cd-section -->

<section id="section6">
  <div class="content-wrapper">
  

<div id="Menu6" class="float-section bottom-section">
  <div class="container">
  <h2 class="main-heading inline-h">قصص نجاح من عملائنا</h2>
  
  
  <div class="row">
  <div class="col-md-12">
  
<section id="testimonial">
    <div id="carousel" class="carousel slide">
                     
                    <ol class="carousel-indicators" style="display:none;">
                        <li data-target="#carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel" data-slide-to="1"></li>
                        <li data-target="#carousel" data-slide-to="2"></li>
                    </ol>
                     
                    <div class="carousel-inner">
                        
                     <div class="item active">
                       <div class="row text-center">

                         
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 
                         
                            <blockquote>
                                       <p>ساعي من الشركات الواعدة في مجال التوصيل ولديهم فريق عمل مميز ومرن ويتطور بشكل سريع خصوصا في تحصيل وتحويل النقد من العملاء انصح <i class="fa fa-quote-right"></i><br><br> بالتعامل معهم
                                       </p>
                              <footer class="blockquote-footer">عمار واقنه, المدير التنفيذي لدكان أفكار</footer>
                                       
                              <img src="<?php echo asset_url(); ?>/newweb/images/img1.png" style="margin-top:50px;">
</blockquote>
                         </div>
                          
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 
                         
                            <blockquote>
                                       <p><i class="fa fa-quote-right"></i>تكمن روعه التعامل مع نظام و فريق عمل كاسبر في الانفتاح والمرونه في استقبال الاراء ، و حرصهم الدائم للتعلم و التطوير لتوصيل افضل مستويات الخدمة لجميع الفرقاء من مستهلك و مشغل و مستخدم،، و هذا قد ادى الى وجود مستودع فعال لدى شركتنا شكرا ساعي لادائك المتميز
                                       </p>
                              <footer class="blockquote-footer"> لولوة السديري, المدير التنفيذي لآرتيستيا</footer>
                                       
                              <img src="<?php echo asset_url(); ?>/newweb/images/img2.png" style="margin-top:50px;">
</blockquote>
                         </div>
                         
                         
                       </div>
                    </div>
                     
                    <div class="item">
                      <div class="row text-center">
                      
                           
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 
                         
                            <blockquote>
                                       <p>ساعي من الشركات الواعدة في مجال التوصيل ولديهم فريق عمل مميز ومرن ويتطور بشكل سريع خصوصا في تحصيل وتحويل النقد من العملاء انصح <i class="fa fa-quote-right"></i><br><br> بالتعامل معهم
                                       </p>
                              <footer class="blockquote-footer">عمار واقنه, المدير التنفيذي لدكان أفكار</footer>
                                       
                              <img src="<?php echo asset_url(); ?>/newweb/images/img1.png" style="margin-top:50px;">
</blockquote>
                         </div>
                          
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 
                         
                            <blockquote>
                                       <p><i class="fa fa-quote-right"></i>تكمن روعه التعامل مع نظام و فريق عمل كاسبر في الانفتاح والمرونه في استقبال الاراء ، و حرصهم الدائم للتعلم و التطوير لتوصيل افضل مستويات الخدمة لجميع الفرقاء من مستهلك و مشغل و مستخدم،، و هذا قد ادى الى وجود مستودع فعال لدى شركتنا شكرا ساعي لادائك المتميز
                                       </p>
                              <footer class="blockquote-footer"> لولوة السديري, المدير التنفيذي لآرتيستيا</footer>
                                       
                              <img src="<?php echo asset_url(); ?>/newweb/images/img2.png" style="margin-top:50px;">
</blockquote>
                         </div>
                         
                         
                             
                       </div>
                      </div>
                      
                    </div>   <!-- Carousel-inner-->
                    
                    <a data-slide="prev" href="#carousel" class="left carousel-control">‹</a>
                    <a data-slide="next" href="#carousel" class="right carousel-control">›</a>
                      
                </div>    
</section>
   
  
  </div>

  
  
  </div>
           
        
  </div>
</div>



  </div>
</section>

<section id="section5" class="cd-section">
  <div class="content-wrapper">
  

<div id="Menu4" class="float-section bottom-section">
  <div class="container">
  <h2 class="main-heading inline-h">تواصلو معنا</h2>
  
  
  <div class="row">
  <div class="col-md-6">
  <h3>+966 50 869 0555</h3>
  <h3 style="margin-top: 0px">+966 53 676 7373</h3>
  <div class="social-links-main"> <a target="_blank" href="https://www.facebook.com/SaeeKSA/"><i class="fa fa-facebook" aria-hidden="true"></i> </a> <a target="_blank" href="https://twitter.com/ksasaee"><i class="fa fa-twitter" aria-hidden="true"></i> </a> <a target="_blank" href="https://www.instagram.com/saeeksa/"><i class="fa fa-instagram" aria-hidden="true"></i> </a> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> </a>  <span>SAEEKSA</span></div> 
  
  </div>

  <div class="col-md-6">
  <h4>الفرع الرئيسي</h4>
  <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3709.8601083341205!2d39.17365671494217!3d21.591382985696278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3d1240b48737d%3A0xb2e9b1083713ab97!2sSaee!5e0!3m2!1sen!2s!4v1534413710062" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe></div>
  
  
  </div>
  
  </div>
           
        
  </div>
</div>

<div  class="float-section footer-section">

<div class="bottom-footer">
<div class="container">
<p class="copy-left">&copy; <?php echo date('Y') ?> Allright Reseverd</p>
</div>
</div>
</div>

  </div>
</section>
  
  <!-- only for mobile -->
<div id="mobile-popup" class="mobile-popup">
   <a href="javascript:void(0)" class="closebtn">&times;</a>
      <div class="mobile-logo">
        <img src="<?php echo asset_url(); ?>/newweb/images/logo.png">
      </div>
      <div class="download-wrapper">
        <div class="download-box">
          <h4 class="get-saee-app">Get the saee app</h4>
          <h4 class="on-store" id="on-store">On App Store</h4>

          <div class="continue-wrapper">
         <a class="continue-button" href="">Continue on Web</a>
        </div>

        <div class="app-store">
         <a id="store-url" href=""> <img id="store-logo" src=""></a>
        </div>
        </div>
      </div>

</div>
<script src="<?php echo asset_url(); ?>/newweb/js/jquery-2.1.4.js"></script>
<script src="<?php echo asset_url(); ?>/newweb/js/main.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> <!-- Resource jQuery -->

<script>



$(document).ready(function () {  
  document.querySelector('a.prei').onclick = function(e) {
    e.preventDefault()
    console.log('you shall not be redirected!');
  window.open('https://www.saee.sa/trackingpage','_blank');  
  }
});

$(function() {
    $(window).scroll(function(){
        if($(this).scrollTop() > 850) {

 $(".contact-btn-icon").addClass("fixes-contect");
    
        }
   if($(this).scrollTop() < 900) {

 $(".contact-btn-icon").removeClass("fixes-contect");
    
        }
  
  
    });
});
</script>

<script>
  $(document).ready(function() {

  if (getCookie('hide') == 'true') {
   
    $('#mobile-popup').hide();
  }
  else{
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $('#mobile-popup').show();
  }
  $('.closebtn').click(function() {
    $('#mobile-popup').hide();
    setCookie('hide', true, 5);
    return false;
  });

  $('.continue-button').click(function() {
    $('#mobile-popup').hide();
    setCookie('hide', true, 5);
    return false;
  });
  
}
});

 
</script>

<script>
function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var user=getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
     user = prompt("Please enter your name:","");
     if (user != "" && user != null) {
       setCookie("username", user, 30);
     }
  }
}

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

   

    if( isMobile.iOS() ){

      document.getElementById("store-logo").src="<?php echo asset_url(); ?>/newweb/images/app-store-btn.png";
      document.getElementById("store-url").href="https://itunes.apple.com/us/app/saee/id1332466731?ls=1&mt=8";
      document.getElementById("on-store").innerHTML="Get app on app store";
      
    }
    else if( isMobile.Android() ){
      document.getElementById("store-logo").src="<?php echo asset_url(); ?>/newweb/images/google-btn.png";
      document.getElementById("store-url").href="https://play.google.com/store/apps/details?id=org.khudwhat.barcodescanner&hl=en";
     document.getElementById("on-store").innerHTML="Get app on play store";
    }
</script>
</body>
</html>