@extends('website.layout')

@section('content')

<h2>{{$title}}</h2>
<hr>
<p>This Mobile Application Terms and Conditions of Use and End User License Agreement is a binding agreement between you (End User or you) and Kasper Cab Captain. This Agreement governs your use of the Taxi service mobile software application (including all related documentation, the Application or the App).BY USING THE SERVICE, YOU ACKNOWLEDGE AND AGREE TO THESE TERMS OF SERVICE, AND Kasper Cab PRIVACY POLICY</p>
<p>By selecting the checkbox button you 
  <ol type="a">
    <li>acknowledge that you have read and understand this agreement;</li>
    <li>represent that you are of legal age to enter into a binding agreement; and </li>
    <li>accept this agreement and agree that you are legally bound by its terms.</li>
  </ol>
    If you do not agree to these terms, do not download/install/use the application and delete it from your mobile device.</p>

<h4>Terms:</h4>
<p>
  <ol>
    <li>In order to access and use the features of the Service, you acknowledge and agree that you will have to provide Kasper Cab with your mobile phone number email , address . You expressly acknowledge and agree that in order to provide the Service, Kasper Cab access find and track of your location to use the Taxi Service when user want Taxi service at him/her location. When providing your mobile phone number, you must provide accurate and complete information. You hereby give your express consent to Kasper Cab to access your Location in order to provide and use the Service. We do collect names, addresses or email addresses, mobile phone numbers. You must notify Kasper Cab immediately of any breach of security or unauthorized use of your mobile phone. Although Kasper Cab will not be liable for your losses caused by any unauthorized use of your account, you may be liable for the losses of Kasper Cab or others due to such unauthorized use.</li>

    <li>The design of the Kasper Cab Service along with Kasper Cab created text, scripts, graphics, interactive features (as defined below), and the trademarks, service marks and logos contained therein ("Marks"), are owned by or licensed to Kasper Cab, subject to copyright and other intellectual property rights under United States and foreign laws and international conventions.</li>
  </ol>
</p>

<h4>Conditions:</h4>
<p>
  <ol class="inner-count">
    <li>YOU AGREE THAT YOUR USE OF THE Kasper Cab SERVICE SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, SG­Taxi, ITS OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICE AND YOUR USE THEREOF. Kasper Cab MAKES NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SERVICE'S CONTENT AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY.
      <ol class="inner-count">
        <li>ERRORS, MISTAKES, OR INACCURACIES OF CONTENT,</li>
        <li>PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICE,</li>
        <li>ANY UNAUTHORISED ACCESS TO OR USE OF OUR SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN,</li>
        <li>ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICE,</li>
        <li>ANY BUGS, VIRUSES, TROJAN HORSES, OR</li>
        <li>ANY ERRORS OR OMISSION SIN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE Kasper Cab SERVICE.</li>
        <li>Kasper Cab DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE Kasper Cab SERVICE OR ANY HYPERLINKED BSITE OR FEATURED IN ANY USER STATUS SUBMISSION OR OTHER ADVERTISING, AND Kasper Cab WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD­PARTY CaptainS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</li>
      </ol>
    </li>
  </ol>
</p>
@stop