<!DOCTYPE html>
<html>
<head>
<title>:. Kasper Business :.</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/newweb/css/custom.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>/newweb/css/font-awesome.css">
<link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>/newweb/css/niceCountryInput.css">
<style>
  .niceCountryInputMenu{
    height: 48px;
    padding-top: 12px;
  }
  label {
    direction: rtl !important;
    width: 100%;
}
input{
  direction: rtl;
}
</style>
</head>
<body>


<header class="topbgcolor">
  <div class="container">
  
    <div class="logotxt"><a href="/"><img src="newweb/images/logo.png" class="img-responsive"></a></div>
    <div class="navtop"><a href="../changelanguage?language=en&amp;page=/registercompany">English</a></div>
    
    
   
  </div>
</header>


<div id="Menu8" class="float-section company-registration">

  <div class="container">
  <div class="col-md-12" >
  <h2 class="company-heading">تسجيل الشركة</h2>
  <h4>
                @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <b>{{ Session::get('error') }}</b> 
                        </div>
                @endif
            </h4>

            <h4>
                @if(Session::has('success'))
                        <div class="alert alert-danger greenclass">
                            <b>{{ Session::get('success') }}</b> 
                        </div>
                @endif
            </h4>
  </div>
<div style="color:white;padding-bottom: 15px;" id="form-messages"></div>
  <form  method="post" action="/registercompanypost" role="form" enctype="multipart/form-data" />

  <div class="row">
    <div class="col-md-4 input-group-feild-gray"><label id="owner_id_label">هوية المالك</label><input style="height: auto; padding:10px;margin:0px;" type="file"  value="" required name="owner_id" id="owner_id" placeholder="* هوية المالك"></div>
    <div class="col-md-4 input-group-feild-gray"><label> اسم المالك</label><input required="" type="text" value="{{ Session::get('owner_name') }}" name="owner_name" placeholder="* اسم المالك"></div>
  <div class="col-md-4 input-group-feild-gray"><label> اسم الشركة</label><input required="" type="text" name="company_name" value="{{ Session::get('company_name') }}" placeholder="* اسم الشركة"></div>
   
    
    <div class="col-md-4 input-group-feild-gray"><label>إيميل المالك</label><input required="" type="text" value="{{ Session::get('email') }}" name="email" placeholder="* إيميل المالك"></div>
    <div class="col-md-4 input-group-feild-gray"><label>جوال المالك</label><input required="" type="text" value="{{ Session::get('phone') }}" name="phone" placeholder="* جوال المالك"></div>
    <div class="col-md-4 input-group-feild-gray"><label id="owner_id_label">السجل التجاري</label><input style="height: auto;padding:13px;" type="file"  value="" required name="company_cr" id="company_cr" placeholder="* السجل التجاري"></div>
   
    
    
    <div class="col-md-4 input-group-feild-gray"><label>هاتف خدمة العملاء</label><input type="text" value="{{ Session::get('customer_support_phone') }}" name="customer_support_phone" placeholder="هاتف خدمة العملاء"></div>
    <div class="col-md-4 input-group-feild-gray"><label>الرقم السري</label><input required="" type="password" name="password" placeholder="* الرقم السري"></div>
     <div class="col-md-4 input-group-feild-gray"><label>اسم المستخدم</label><input required="" type="text" value="{{ Session::get('username') }}" name="username" placeholder="* اسم المستخدم"></div>
   
    
    
    <div class="col-md-4 input-group-feild-gray"><label>الدولة</label><div required style="min-height: 76px" class="niceCountryInputSelector" style="width: 300px;" data-selectedcountry="SA" data-showspecial="false" data-showflags="true" data-i18nall="All selected"
            data-i18nnofilter="No selection" data-i18nfilter="Filter" data-onchangecallback="onChangeCallback" />
        </div></div>
         <div class="col-md-4 input-group-feild-gray"><label>العنوان على الشحنة</label><input required="" type="text" value="{{ Session::get('address') }}" name="address" placeholder="* العنوان على الشحنة"></div>
     <div class="col-md-4 input-group-feild-gray"><label>إيميل خدمة العملاء</label><input type="text" value="{{ Session::get('customer_support_email') }}" name="customer_support_email" placeholder="إيميل خدمة العملاء"></div>
  
    
    
    <div class="col-md-4 input-group-feild-gray"><label>آيبان</label><input type="text" value="{{ Session::get('iban') }}" name="iban" placeholder="آيبان"></div>
    <div class="col-md-4 input-group-feild-gray"><label> اسم البنك</label><input type="text" value="{{ Session::get('bank_name') }}" name="bank_name" placeholder=" اسم البنك"></div>
    <div class="col-md-4 input-group-feild-gray"><label>المدينة</label><input required="" type="text" value="{{ Session::get('city') }}" name="city" placeholder="* المدينة"></div>
   
    
    
    
  </div>
  <div class="row">
  
  <div class="col-md-12 input-group-feild text-center">
  <button type="submit" class="submit-btn">Submit</button>
  </div>
  </div>

</form>
  </div>
  
  <div class="container">
<div class="footertxt">
<p>&copy; 2018 Allright Reseverd</p>

</div>
</div>
  
</div>





<!-- JavaScript Includes --> 
<script src="<?php echo asset_url(); ?>/newweb/js/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
<script src="<?php echo asset_url(); ?>/newweb/js/niceCountryInput.js"></script>
<script>
  
  
  $(function() {
    // Get the form.
    var form = $('#ajax-contact');

    // Get the messages div.
    var formMessages = $('#form-messages');

    // TODO: The rest of the code will go here...
    // Set up an event listener for the contact form.
$(form).submit(function(event) {
    // Stop the browser from submitting the form.
    event.preventDefault();

    // Serialize the form data.
var formData = $(form).serialize();
 
var country = document.getElementsByClassName("niceCountryInputMenuInputHidden");

country = country[0].value;
// Submit the form using AJAX.
$.ajax({
    type: 'POST',
    url: $(form).attr('action'),
    data: formData
}).done(function(response) {
    // Make sure that the formMessages div has the 'success' class.
    $(formMessages).removeClass('error');
    $(formMessages).addClass('success');

    // Set the message text.
    $(formMessages).text(response);

    // Clear the form.
    $('#name').val('');
    $('#email').val('');
    $('#message').val('');
}).fail(function(data) {
    // Make sure that the formMessages div has the 'error' class.
    $(formMessages).removeClass('success');
    $(formMessages).addClass('error');

    // Set the message text.
    if (data.responseText !== '') {
        $(formMessages).text(data.responseText);
    } else {
        $(formMessages).text('Oops! An error occured and your message could not be sent.');
    }
});

});




});
  
</script>

<script>
        function onChangeCallback(ctr){
            console.log("The country was changed: " + ctr);
            //$("#selectionSpan").text(ctr);
        }

        $(document).ready(function () {
            $(".niceCountryInputSelector").each(function(i,e){
                new NiceCountryInput(e).init();
            });
        });
    </script>

    
</body>
</html>
