<!DOCTYPE html>
<html lang="en">
<head>
  <title>:. KASPER CAB :.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/search.js"></script>
  <link rel="stylesheet" type="text/css" href="css/custom.css">
  <link rel="stylesheet" type="text/css" href="css/badge.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>

<!--header start-->

<div class="container-fluid topclr">
<div class="container">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="images/logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav navbar-right">
  <li><a href="http://www.kasper-cab.com/provider/signin" style="color:#f7931e;">CAPTAIN</a></li>
        <li><a href="http://www.kasper-cab.com/user/signin">SIGN IN</a></li>
         <li><a href="http://www.kasper-cab.com/user/signup">SIGNUP</a></li> 
         <li class="border"><a href="http://www.kasper-cab.com/ar">عربى</a></li>
      </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
</div>

<div class="container-fluid topbg-home">



<div class="container">
<div class="col-md-12 slidertext">
<div class="col-md-6">

<img src="images/banner-logo.png" class="img-responsive">
<h2>Welcome to your own Personal Captain!</h2>
<h6>Always a friendly face...</h6>

<p>With our personal captain experience, you will always have the same dedicated captain  to ensure your  safety & convenience  at a  cost that can't be resisted!</p>

<h6>Download our App</h6>
<span><a href="https://play.google.com/store/apps/details?id=com.kaspercab.customer&hl=en"><img src="images/google.png" class="img-responsive btnmrg"></a></span>
<span><a href="https://itunes.apple.com/us/app/kasper-cab/id1151382026?mt=8"><img src="images/apple.png" class="img-responsive btnmrg"></a></span>
</div>

<div class="col-md-6">
  <img src="images/slider-img.png" class="sliderimg"></div>
</div>
</div>
</div>
<!--header end-->

<!--1st section start-->
<div class="container ">
<div class="row">
<div class="col-md-12 heading">

<h5>Try our awesome monthly deals!</h5>
<h4>Our monthly contracts are headache free.</h4>

<h2 class="background"><span>HOW IT WORKS!</span></h2>

<div class="col-md-3">
<h3>Choose your <br> daily route</h3>
<div class="img1"><img src="images/icon1.png"></div>


</div>

<div class="col-md-3">
<h3>Choose your starting <br>date & your timings</h3>
<div class="img1"><img src="images/icon2.png"></div>


</div>

<div class="col-md-3">
<h3>Decide your<br> budget</h3>
<div class="img1"><img src="images/icon3.png"></div>


</div>

<div class="col-md-3">
<h3>Select a captain<br> & enjoy the ride!</h3>
<div class="img1"><img src="images/icon4.png"></div>


</div>


</div>



</div>
</div>
<!--1st section end-->


<!--2nd section start-->
<div class="container ">
<div class="row">
<div class="col-md-12 heading">

<h2 class="background"><span>WHY RIDE WITH US?</span></h2>

<div class="col-md-3">
<div class="ride">
<img src="images/ride1.png" >
<h6>We are serious about your safety, all cars are inspected & captains are checked & personal nformation verified.</h6>
</div>


</div>

<div class="col-md-3">
<div class="ride">
<img src="images/ride2.png" >
<h6>Personalize your pick-up & drop-off schedule.</h6>
</div>


</div>

<div class="col-md-3">
<div class="ride">
<img src="images/ride3.png" >
<h6>Captain & car hygiene are regularly checked and maintained.</h6>
</div>


</div>

<div class="col-md-3">
<div class="ride">
<img src="images/ride4.png" >
<h6>Affordable and convenient!</h6>
</div>


</div>



</div>



</div>
</div>
<!--2nd section end-->


<!--3rd section start-->
<div class="container-fluid drkBG">
<div class="row">

<div class="container">
<h1 class="background2"><span>share rides</span></h1>

<p>With share ride, you can share more than just your car!  Once you have approved a new passenger to share your ride, immediately your cost will also be shared so that you can pay less.  Passengers who request to share a ride with you must have matching routs so that they don't inconvenience you but rather add a more engaging ride as you start your day.</p>



<div class="col-md-12">

<div class="col-md-3">
<h3>Save as mush as </h3>
<h4>40%</h4>
<h5>off your Price!</h5>


</div>

<div class="col-md-5">
<h3>Absolutly </h3>
<h4>NO DELAYS</h4>
<h5>what so ever!</h5>

</div>

<div class="col-md-4">
<h3>Make new </h3>
<h4>FRIENDS</h4>
<h5>& engage</h5>

</div>


</div>

</div>
</div>
</div>
<!--3rd section end-->


<!--4th section start-->
<div class="container-fluid orangebg">
<div class="row">
<div class="col-md-12">
<h2>Interested to drive with us!</h2>
<p>It's simple and easy!</p>

<div class="button1"><button type="button" class="btn btn-success">SIGN UP!</button></div>

</div>
</div>
</div>
<!--4th section end-->


<!--footer start-->

<div class="container">


<div class="col-md-12 context">
<div class="col-md-8">

<h4>Customer Service</h4>
<h5>920004954</h5>


</div>
<div class="col-md-4">

<div class="social">
    
<img href="https://www.facebook.com/KasperCab/"  src="images/facb.png">
<img href="https://twitter.com/kaspercab?lang=en" src="images/twi.png">
<!--<img src="images/insta.png"> -->
<img href="https://www.linkedin.com/company/kasper-cab" src="images/ink.png">
</div>

    <a href="#" id="back-to-top" title="Back to top">
      <img src="images/arrow.jpg">
    </a>
    
    
    
    </div>
    

    
    
</div>

<div class="col-md-12">
<div class="col-md-4 copuright">© 2017, KASPERCAB. ALL RIGHTS RESERVED </div>

<div class="col-md-6 footLinks">
<a href="#">CAPTAIN REQUIREMENTS</a>                    <a href="http://www.kasper-cab.com/termsncondition">TERMS OF SERVICE</a>                       <a href="#">PRIVACY POLICY</a>
</div>

</div>

</div>

<!--footer end-->

<script src="js/scroll.js"></script>
</body>
</html>