<?php

/*
 * handles delivery requests tracking and data manipulations
  */

use App\Classes\Curl;


class DeliveryController extends \BaseController
{

    //Status of request $request->status: we only need 4 functions for the whole flow-chart/interaction

    /*   -1 canceled or default
     *    0 pending searching for a walker
     *    1 walker accepted. "on the way"
     *    2 package picked from sender "package picked"  same as "arrived"
     *    3 delivered "completed cash posted or "cash taken from receiver and company credit updated "cash taken" same as completed.
     */

    //return the app main page
    public function Main()
    {
        return View::make("web.DeliveryApp.main");
       
    }


    public function isAdmin($token)
    {
        return false;
    }


    public function getWalkerData($walker_id, $token, $is_admin)
    {

        if ($walker_data = Walker::where('token', '=', $token)->where('id', '=', $walker_id)->first()) {
            return $walker_data;
        } elseif ($is_admin) {
            $walker_data = Walker::where('id', '=', $walker_id)->first();
            if (!$walker_data) {
                return false;
            }
            return $walker_data;
        } else {
            return false;
        }
    }
    //




    //this function is called every 0.5 second, time-interval can be adjusted later for improvement.
    //return walker current location if request is still active otherwise return false.
    public function track_active_request(){

        try{
            $request_id = Request::get("request_id");
            $request = DeliveryRequest::find($request_id);

            //trick: return walker current location in case of active request otherwise return false,
            //false is the stopping condition for the tracking function in JS. namely: updateLocation( );
            //case active request == 2 "on the way" or 3 "package picked".


            //pending searching for a driver..
            if ($request->status == 0) {

                return 0;

                //driver on the way..
            } else if ($request->status == 1) {


                $walker = Walker::find($request->current_walker);
                //return walker updated location
                return json_encode(array('On The Way',$walker->first_name ,$walker->phone , $walker->latitude, $walker->longitude));

            } else if ($request->status == 2) {

                //package picked on the way to the receiver..
                $walker = Walker::find($request->current_walker);
                //return walker updated location
                return json_encode(array('Package Picked',$walker->first_name ,$walker->phone , $walker->latitude, $walker->longitude));

                //started delivery request
            } else if ($request->status == 3) {

                //package delivered do something
                return 3;

            } else if ($request->status == -1) {

                //no driver found do something about it..
                return -1;
            }else if($request->stauts == 4){
                //delivered
                return 4;
            }


        }catch (Exception $e){
            $e->getMessage();
        }

    }


    public function resubmit_delivery_request()
    {

    try{
        $request_id = Request::get('request_id');
        $delivery_request = DeliveryRequest::find($request_id);


        $delivery_request->request_start_time = date("Y-m-d H:i:s");
        $delivery_request->confirmed_walker = 0;
        $delivery_request->current_walker = 0;
        $delivery_request->status = 0; //requested created and searching for driver status..
        $delivery_request->save();


        Log::info('in');
        $settings = Settings::where('key', 'default_search_radius')->first();
        $distance = $settings->value;
        $settings = Settings::where('key', 'default_distance_unit')->first();
        $unit = $settings->value;
        if ($unit == 0) {
            $multiply = 1.609344;
        } elseif ($unit == 1) {
            $multiply = 1;
        }
        $query = "SELECT walker.*, "
            . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$delivery_request->origin_latitude') ) * "
            . "cos( radians(latitude) ) * "
            . "cos( radians(longitude) - radians('$delivery_request->origin_longitude') ) + "
            . "sin( radians('$delivery_request->origin_latitude') ) * "
            . "sin( radians(latitude) ) ) ,8) as distance "
            . "FROM walker "
            . "where is_available = 1 and "
            . "is_active = 1 and "
            . "is_approved = 1 and "
            . "walker.id IN (455,876,778,864) and "
            // . "walker.id = 534 and "
            . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$delivery_request->origin_latitude') ) * "
            . "cos( radians(latitude) ) * "
            . "cos( radians(longitude) - radians('$delivery_request->origin_longitude') ) + "
            . "sin( radians('$delivery_request->origin_latitude') ) * "
            . "sin( radians(latitude) ) ) ) ,8) <= $distance and "
            . "walker.deleted_at IS NULL "
            . "order by distance limit 5";
        Log::info($query);
        $walkers = DB::select(DB::raw($query));
        $settings = Settings::where('key', 'provider_timeout')->first();
        $time_left = $settings->value;
        $company = Company::find($delivery_request->company_id);
        foreach ($walkers as $walker) {
            $delivery_request_meta = new DeliveryRequestMeta;
            $delivery_request_meta->request_id = $delivery_request->id;
            $delivery_request_meta->walker_id = $walker->id;
            $delivery_request_meta->save();
            Log::info("walker id " . $walker->id);
            send_walker_deliver_request($company, $delivery_request, $walker, $time_left);
        }

    } catch (Exception $e)
      {
      Log::info($e->getMessage() . " " . $e->getLine());
      }

     //now track this newly created request by its id.
     Log::info('New Delivery Request Id' . $delivery_request->id);
     return $delivery_request->id;

}



    //error msg -1 -> wrong email or password.
    //error msg -2 -> the call is missing information required to complete it.
    //error msg -3 .... for later
    //integer xxx of length 3 -> success request posted, here is the id.
    public function insertrequest()
    {

        try {

            //only initiaizing the company ID it should not = 0
            $companyid = 0;


            //now lets validate email and password, and get company ID.
            $email = Request::segment(3);
            $password = Request::segment(4);

            if ($company = Company::where('email', '=', $email)->first()) {
                if (Hash::check($password, $company->password)) {

                    $companyid = $company->id;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }


            $suppliername = Request::segment(5);
            $suppliermobile = Request::segment(6);

            $o_latitude = Request::segment(7);
            $o_longitude = Request::segment(8);
            $o_address = Request::segment(9);

            $immediatepickup = Request::segment(10);
            $pickupdate = Request::segment(11);
            $pickuptime = Request::segment(12);

            $customername = Request::segment(13);
            $customermobile = Request::segment(14);

            $d_latitude = Request::segment(15);
            $d_longitude = Request::segment(16);
            $d_address = Request::segment(17);

            $immediatedropoff = Request::segment(18);
            $dropoffdate = Request::segment(19);
            $dropofftime = Request::segment(20);

            $cashondelivery = Request::segment(21);


            //define empty delivery request model:
            $delivery_request = new DeliveryRequest;


            //below section is badly written hoping in the near future someone fix it:
            //in addition the database needs a normalization.

            if ($pickupdate == 0) {
                $delivery_request->scheduled_pickup_date = "";
            } else {
                $delivery_request->scheduled_pickup_date = "";

            }

            if ($pickuptime == 0) {
                $delivery_request->scheduled_pickup_time = 0;
            } else {
                $delivery_request->scheduled_pickup_time = $pickuptime;

            }


            if ($dropoffdate == 0) {
                $delivery_request->scheduled_dropoff_date = "";
            } else {
                $delivery_request->scheduled_dropoff_date = $dropoffdate;
            }

            if ($dropofftime == 0) {
                $delivery_request->scheduled_dropoff_time = 0;

            } else {
                $delivery_request->scheduled_dropoff_time = $dropofftime;

            }

            if ($o_address == 0) {
                $delivery_request->o_address = "";
            } else {
                $delivery_request->o_address = $o_address;
            }

            if ($d_address == 0) {
                $delivery_request->d_address = "";
            } else {
                $delivery_request->d_address = $d_address;
            }

////////////////////////////// end of badly written section ;) ///////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////

            $delivery_request = new DeliveryRequest;
            $delivery_request->company_id = $companyid;
            $delivery_request->request_start_time = date("Y-m-d H:i:s");
            $delivery_request->receiver_name = $suppliername;
            $delivery_request->receiver_phone = $suppliermobile;
            $delivery_request->sender_name = $customername;
            $delivery_request->sender_phone = $customermobile;
            $delivery_request->immediate_pickup = $immediatepickup;
            $delivery_request->scheduled_pickup_date = $pickupdate;
            $delivery_request->scheduled_pickup_time = $pickuptime;
            $delivery_request->immediate_dropoff = $immediatedropoff;
            $delivery_request->scheduled_dropoff_date = $dropoffdate;
            $delivery_request->scheduled_dropoff_time = $dropofftime;
            $delivery_request->cash_on_delivery = $cashondelivery;
            $delivery_request->o_address = $o_address;
            $delivery_request->d_address = $d_address;
            $delivery_request->origin_latitude = $o_latitude;
            $delivery_request->origin_longitude = $o_longitude;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;

            //requested created and searching for driver status..
            //note this is a default status not to confuse it with diff type of delivery request:
            $delivery_request->status = 0;

            $delivery_request->save();

            //success -> then return order/request ID.
            return $delivery_request->id;


        } catch (Exception $e) {
            return -2;
        }

    }

  public function request_delivery()
    {

        try {
         //data to make a delivery request
            $company_id = Request::get('id');

            $o_latitude = Request::get('latitude');
            $o_longitude = Request::get('longitude');

            $d_latitude = Request::get('d_latitude');
            $d_longitude = Request::get('d_longitude');

            $o_address = Request::get('o_address');
            $d_address = Request::get('d_address');

            $sender_name = Request::get('pickupname');
            $sender_mobile = Request::get('pickupmobile');

            $receiver_name = Request::get('dropoffname');
            $receiver_mobile = Request::get('dropoffmobile');

            $immediate_pickup =  Request::get('immediatepickup');
            $pickup_date =  Request::get('pickupdate');
            $pickup_time =  Request::get('pickuptime');

            $immediate_dropoff = Request::get('immediatedropoff');
            $dropoff_date = Request::get('dropoffdate');
            $dropoff_time = Request::get('dropofftime');


            $cash_on_delivery = Request::get('cash_on_delivery');
            $transpiration_fees = Request::get('fees');
            $fees_payed_by = Request::get('payed_by');

            $small_items = Request::get('small_items');
            $medium_items = Request::get('medium_items');
            $large_items = Request::get('large_items');

            //////////////////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////////////////

            $delivery_request = new DeliveryRequest;
            $delivery_request->company_id = $company_id;
            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            $delivery_request->receiver_name = $receiver_name;
            $delivery_request->receiver_phone = $receiver_mobile;

            $delivery_request->sender_name = $sender_name;
            $delivery_request->sender_phone = $sender_mobile;

            $delivery_request->immediate_pickup = $immediate_pickup;
            $delivery_request->scheduled_pickup_date = $pickup_date;
            $delivery_request->scheduled_pickup_time = $pickup_time;
            $delivery_request->immediate_dropoff = $immediate_dropoff;
            $delivery_request->scheduled_dropoff_date =$dropoff_date;
            $delivery_request->scheduled_dropoff_time =$dropoff_time;


            $delivery_request->small_items = $small_items;
            $delivery_request->medium_items = $medium_items;
            $delivery_request->large_items = $large_items;
            $delivery_request->cash_on_delivery = $cash_on_delivery;
            $delivery_request->transportation_fees = $transpiration_fees;
            $delivery_request->transportation_fees_payed_by = $fees_payed_by;
            $delivery_request->o_address = $o_address;
            $delivery_request->d_address = $d_address;
            $delivery_request->origin_latitude = $o_latitude;
            $delivery_request->origin_longitude = $o_longitude;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->status = 0; //requested created and searching for driver status..
            $delivery_request->save();


//            return $delivery_request;

            Log::info('in');
            $settings = Settings::where('key', 'default_search_radius')->first();
            $distance = $settings->value;
            $settings = Settings::where('key', 'default_distance_unit')->first();
            $unit = $settings->value;
            if ($unit == 0) {
                $multiply = 1.609344;
            } elseif ($unit == 1) {
                $multiply = 1;
            }
            $query = "SELECT walker.*, "
                . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$o_latitude') ) * "
                . "cos( radians(latitude) ) * "
                . "cos( radians(longitude) - radians('$o_longitude') ) + "
                . "sin( radians('$o_latitude') ) * "
                . "sin( radians(latitude) ) ) ,8) as distance "
                . "FROM walker "
                . "where is_available = 1 and "
                . "is_active = 1 and "
                . "is_approved = 1 and "
                // . "walker.id IN (619,787,519,864,468,877,820,737,455,867,534,224,225,226) and "
                // . "walker.id IN (432,224,225,226) and "
                // . "walker.id = 432 and "
                // . "walker.id = 534 and "
                . "walker.id IN (455,876,778,864) and "
                . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$o_latitude') ) * "
                . "cos( radians(latitude) ) * "
                . "cos( radians(longitude) - radians('$o_longitude') ) + "
                . "sin( radians('$o_latitude') ) * "
                . "sin( radians(latitude) ) ) ) ,8) <= $distance and "
                . "walker.deleted_at IS NULL "
                . "order by distance limit 5";
            Log::info($query);
            $walkers = DB::select(DB::raw($query));
            $settings = Settings::where('key', 'provider_timeout')->first();
            $time_left = $settings->value;
            $company = Company::find($company_id);
            foreach ($walkers as $walker) {
                $delivery_request_meta = new DeliveryRequestMeta;
                $delivery_request_meta->request_id = $delivery_request->id;
                $delivery_request_meta->walker_id = $walker->id;
                $delivery_request_meta->save();
                Log::info( "walker id ".$walker->id);
                send_walker_deliver_request($company, $delivery_request, $walker, $time_left);
            }


        } catch (Exception $e) {
            Log::info($e->getMessage() . " " . $e->getLine());
            return $e->getMessage() . " " . $e->getLine();
        }
        //now track this newly created request by its id.
        Log::info('New Delivery Request Id'.$delivery_request->id);
        return $delivery_request->id;

    }







    // public function request_delivery()
    // {
    //     try {
    //      //data to make a delivery request
    //         $company_id = Request::get('id');

    //         $o_latitude = Request::get('latitude');
    //         $o_longitude = Request::get('longitude');
    //         $d_latitude = Request::get('d_latitude');
    //         $d_longitude = Request::get('d_longitude');

    //         $o_address = Request::get('o_address');
    //         $d_address = Request::get('d_address');

    //         $receiver_name = Request::get('name');
    //         $receiver_phone = Request::get('phone');

    //         $cash_on_delivery = Request::get('cash_on_delivery');
    //         $transpiration_fees = Request::get('fees');
    //         $fees_payed_by = Request::get('payed_by');

    //         $small_items = Request::get('small_items');
    //         $medium_items = Request::get('medium_items');
    //         $large_items = Request::get('large_items');

    //         //////////////////////////////////////////////////////////////////////////////////////////////////////////

    //         $delivery_request = new DeliveryRequest;
    //         $delivery_request->company_id = $company_id;
    //         $delivery_request->request_start_time = date("Y-m-d H:i:s");
    //         $delivery_request->receiver_name = $receiver_name;
    //         $delivery_request->receiver_phone = $receiver_phone;
    //         $delivery_request->small_items = $small_items;
    //         $delivery_request->medium_items = $medium_items;
    //         $delivery_request->large_items = $large_items;
    //         $delivery_request->cash_on_delivery = $cash_on_delivery;
    //         $delivery_request->transportation_fees = $transpiration_fees;
    //         $delivery_request->transportation_fees_payed_by = $fees_payed_by;
    //         $delivery_request->o_address = $o_address;
    //         $delivery_request->d_address = $d_address;
    //         $delivery_request->origin_latitude = $o_latitude;
    //         $delivery_request->origin_longitude = $o_longitude;
    //         $delivery_request->D_latitude = $d_latitude;
    //         $delivery_request->D_longitude = $d_longitude;
    //         $delivery_request->status = 0; //requested created and searching for driver status..
    //         $delivery_request->save();



    //         Log::info('in');
    //         $settings = Settings::where('key', 'default_search_radius')->first();
    //         $distance = $settings->value;
    //         $settings = Settings::where('key', 'default_distance_unit')->first();
    //         $unit = $settings->value;
    //         if ($unit == 0) {
    //             $multiply = 1.609344;
    //         } elseif ($unit == 1) {
    //             $multiply = 1;
    //         }
    //         $query = "SELECT walker.*, "
    //             . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$o_latitude') ) * "
    //             . "cos( radians(latitude) ) * "
    //             . "cos( radians(longitude) - radians('$o_longitude') ) + "
    //             . "sin( radians('$o_latitude') ) * "
    //             . "sin( radians(latitude) ) ) ,8) as distance "
    //             . "FROM walker "
    //             . "where is_available = 1 and "
    //             . "is_active = 1 and "
    //             . "is_approved = 1 and "
    //             . "walker.id IN (619,787,519,864,468,877,820,737,455,867,534,224,225,226,876,778) and "
    //             // . "walker.id = 534 and "
    //             . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$o_latitude') ) * "
    //             . "cos( radians(latitude) ) * "
    //             . "cos( radians(longitude) - radians('$o_longitude') ) + "
    //             . "sin( radians('$o_latitude') ) * "
    //             . "sin( radians(latitude) ) ) ) ,8) <= $distance and "
    //             . "walker.deleted_at IS NULL "
    //             . "order by distance limit 5";
    //         Log::info($query);
    //         $walkers = DB::select(DB::raw($query));
    //         $settings = Settings::where('key', 'provider_timeout')->first();
    //         $time_left = $settings->value;
    //         $company = Company::find($company_id);
    //         foreach ($walkers as $walker) {
    //             $delivery_request_meta = new DeliveryRequestMeta;
    //             $delivery_request_meta->request_id = $delivery_request->id;
    //             $delivery_request_meta->walker_id = $walker->id;
    //             $delivery_request_meta->save();
    //             Log::info( "walker id ".$walker->id);
    //             send_walker_deliver_request($company, $delivery_request, $walker, $time_left);
    //         }

    //     } catch (Exception $e) {
    //         Log::info($e->getMessage() . " " . $e->getLine());
    //     }
    //     //now track this newly created request by its id.
    //     Log::info('New Delivery Request Id'.$delivery_request->id);
    //     return $delivery_request->id;

    // }
    
    
    
    // this function called by ajax to check for request status...
   public function get_delivery_request_status ( )
    {


        $request_id = Request::get("request_id");

        //get request...
        $request = DeliveryRequest::find($request_id);


        //case -1 service unavailable or the driver canceled or no driver found...canceled or service unavailanle.
        if ($request->status == -1) {
            return json_encode(-1);

            //case 0 searching for driver, pending.
        } else if ($request->status == 0) {
            return json_encode(0);

            //case 1 driver accepted the request,walker accepted. "on the way"
        } else if ($request->status == 1) {

            $walker = Walker::find($request->current_walker);
            //return walker updated location
            return json_encode(array('On The Way',$walker->first_name ,$walker->phone));

            //case 2 package picked from sender "package picked"  same as "arrived" to sender destination...
        } else if ($request->status == 2) {

            $walker = Walker::find($request->current_walker);
            //return walker information
            return json_encode(array('Package Picked',$walker->first_name ,$walker->phone));

            //case 3 package delivered and cash posted
        } else if ($request->status == 3) {
           return json_encode(3);

        }


    }


    /// I am not sure this whole procedure is needed ..same functionality achieved with get_delivery_request_status ()
    public function get_delivery_request()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrive and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {
                        if ($request->confirmed_walker != 0 && $request->status == 1 && $request->is_cancelled == 0) {

                            $company = Company::find($request->company_id);
                            $delivery_request = $request;
                            $msg_array = array();
                            $msg_array['unique_id'] = 51;
                            $msg_array['$delivery_request_id'] = $delivery_request_id->id;
                            $msg_array['payment_mode'] = $delivery_request->payment_mode;
                            $msg_array['request_start_time'] = $delivery_request->request_start_time;
                            $msg_array['receiver_name'] = $delivery_request->receiver_name;
                            $msg_array['receiver_phone'] = $delivery_request->receiver_phone;
                            $msg_array['small_items'] = $delivery_request->small_items;
                            $msg_array['medium_items'] = $delivery_request->medium_items;
                            $msg_array['large_items'] = $delivery_request->large_items;
                            $msg_array['cash_on_delivery'] = $delivery_request->cash_on_delivery;
                            $msg_array['transportation_fees'] = $delivery_request->transportation_fees;
                            $msg_array['transportation_fees_payed_by'] = $delivery_request->transportation_fees_payed_by;
                            $msg_array['origin_latitude'] = $delivery_request->origin_latitude;
                            $msg_array['origin_longitude'] = $delivery_request->origin_longitude;
                            $msg_array['D_latitude'] = $delivery_request->D_latitude;
                            $msg_array['D_longitude'] = $delivery_request->D_longitude;
                            $msg_array['status'] = $delivery_request->status;
                            $msg_array['is_walker_started'] = $delivery_request->status;
                            $msg_array['is_walker_arrived'] = $delivery_request->status;
                            $msg_array['is_started'] = $delivery_request->status;
                            $msg_array['is_completed'] = $delivery_request->status;
                            // add company details here
                            $msg_array['company'] = array();
                            $msg_array['company'] ['name'] = $company->name;
                            $msg_array['company'] ['phone'] = $company->phone;
                            $msg_array['company'] ['email'] = $company->email;
                            $msg_array['company'] ['products_type'] = $company->products_type;
                            $msg_array['company'] ['city'] = $company->city;
                            $msg_array['company'] ['latitude'] = $company->latitude;
                            $msg_array['company'] ['address'] = $company->address;
                            $response_array = array(
                                'success' => true,
                                'delivery_request_id' => $delivery_request_id,
                                'status' => $request->status,
                                'confirmed_walker' => $request->confirmed_walker,
                                'walker' => $walker_data,
                                'delivery_request' => $msg_array,
                            );
                            $response_code = 200;
                        } else {
                            if ($request->confirmed_walker == 0 && $request->status == -1 && $request->is_cancelled == 0) {
                                //if no captain responded in specified time
                                //background job will change status of delivery request to -1
                                //and free captains to whom we requested
                                $response_array = array('success' => true, 'message' => 'No Captains available for now please try again later.');
                                $response_code = 200;
                            } else {
                                //we are still waiting for captain to respond
                                $response_array = array('success' => true, 'message' => 'Waiting for Captains to respond  .');
                                $response_code = 200;
                            }
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    //walker canceling delivery request "after driver accepted it"
    //it must notify the customer its canceled..
    public function cancel_delivery_request ( ){

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrieve and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {

                        //update request status "Delivery canceled by the walker".
                        $request->status = -1;
                        $request->save();

                        $response_code = 200;
                        $response_array = array('success' => true, 'message' => 'Delivery Request Cancelled');

                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;


    }

  
    
   //get delivery request status
    public function respond_delivery_request()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');
        $accepted = Input::get('accepted');
        Log::info('respond_delivery_request called with id'.$delivery_request_id);

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
                'accepted' => $accepted,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'accepted' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrive and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {
                        if ($request->confirmed_walker == 0) {
                            if ($accepted == 1) {
                                //update status to 1 meaning walker accpeted the request and update table column accordingly..
                                DeliveryRequest::where('id', '=', $delivery_request_id)->update(array('confirmed_walker' => $walker_id,'current_walker' => $walker_id, 'status' => 1, 'request_start_time' => date('Y-m-d H:i:s')));

                                $company = Company::find($request->company_id);

                                //this section for testing only
                                $curl = new Curl();

                                $walker = Walker::find($walker_id);
                                $destinations = $walker->phone;
                                $destinations = str_replace("+","",$destinations);


                                $sms =  "http://saee.sa/delivery/walker/getupdatelink/".$request->id;


                                $url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper-Cab";
                                $urlDiv = explode("?", $url);
                                $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                                Log::info("SMSSERVICELOG".$result);

                                // confirm walker
                                DeliveryRequestMeta::where('request_id', '=', $request)->where('walker_id', '=', $walker_id)->update(array('status' => 1));

                                // Update Walker availability
                                //Walker::where('id', '=', $walker_id)->update(array('is_available' => 0));
                                // Update Other Walkers availability
                                $exmetas = DeliveryRequestMeta::where('request_id', '=', $delivery_request_id)->where('status', '=', 0)->get();
                                foreach($exmetas as $metaa){
                                    Walker::where('id', '=', $metaa->walker_id)->update(array('is_available' => 1));
                                    DeliveryRequestMeta::where('id', '=', $metaa->id)->update(array('status'=> 3));
                                }
                                //sending response to captain
                                $response_array = array(
                                    'success' => true,
                                    'delivery_request_id' => $delivery_request_id,
                                    'status' => $request->status,
                                    'confirmed_walker' => $request->confirmed_walker,
                                    'walker' => $walker_data,
                                );
                            } else {
                                Walker::where('id', '=', $walker_id)->update(array('is_available' => 1));
                                // Archiving Old Walker
                                DeliveryRequestMeta::where('request_id', '=', $delivery_request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 3));
                                $response_array = array('success' => true);
                            }
                            $response_code = 200;
                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Request ID does not matches' . $driver->keyword . ' ID', 'error_code' => 472); */
                            $response_array = array('success' => false, 'error' => 'Request already accepted by other ' . Config::get('app.generic_keywords.Provider') . ' ', 'error_code' => 472);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }
    
    
    
    
    
    
    
    
    
    
    

    //walker updates the status of the delivery request to status=2
    //means package is taken and its on its way to the receiver..
    //"package picked" = "arrived to sender".
    public function delivery_request_picked ( ){

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');
        Log::info('picked called with id'.$delivery_request_id);

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrieve and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {



                        //update request status "package picked"/ "arrived at sender".
                        $request->is_started = 1;
                        $request->status = 2;

                        $response_code = 200;
                        $request->save();

                        $response_code = 200;
                        $response_array = array('success' => true, 'message' => 'Delivery Request Cancelled');

                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;


    }



    //walker updates the company account when cash is taken from the receiver.
    // company.credit = company.credit + cash on delivery ;
    //status = 4
    public function delivery_request_cash_posted ( ){


        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrieve and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {

                        //get company object...
                        $company = Company::find($request->company_id);

                        //increment company credit
                        $company->credit = $company->credit + $request->cash_on_delivery;

                        //decrement company credit
                        if($request->transportation_fees_payed_by==0){
                            $company->credit = $company->credit - $request->transportation_fees;

                        }else {
                            //walker must took the cash of transportation fees already from the reciver..
                        }
                        $company->save();
                        //update request status "cash taken"/ delivery completed.
                        $request->status = 3;
                        $request->is_completed = 1;
                        $request->save();
                        $response_array = array('success' => true, 'message' => 'Package Delivered Successfully');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;

    }

    public function started_delivery_request ( ){
        Log::info('arrived called with id');

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');

        Log::info('arrived called with id'.$delivery_request_id);
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrieve and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {

                        //update request status "Delivery canceled by the walker".
                        $request->is_walker_started =1;
                        $request->status =5;
                        $request->save();

                        $response_code = 200;
                        $response_array = array('success' => true, 'message' => 'Walker Started to Pick Delivery');
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;


    }


    //status =2
    public function delivery_request_arrived ( ){
        Log::info('arrived called with id');

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $delivery_request_id = Input::get('delivery_request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'delivery_request_id' => $delivery_request_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrieve and validate the Request
                    if ($request = DeliveryRequest::find($delivery_request_id)) {

                        //update request status "Delivery canceled by the walker".
                        $request->is_walker_arrived=1;
                        $request->status =2;
                        $request->save();

                        $response_code = 200;
                        $response_array = array('success' => true, 'message' => 'Walker Started to Pick Delivery');
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;


    }

    //return boolean based on the request activation
    function is_active_request(){

        $request_id = Request::get("request_id");
        $request = DeliveryRequest::find($request_id);
        if($request->status == 1 or 2){
            return true;
        }else {
            return false;
        }
    }


    //simply returns request information
    function get_request(){

        return json_encode(DeliveryRequest::find(Request::get("request_id")));

    }
    

  //get update link for the captains
    public function get_update_link(){

        //this is the request ID.
        //get the request object and inject into the view below ;)
        $id = Request::segment(4);
        $request = DeliveryRequest::find($id);

        return View::make("web.DeliveryApp.updatelink")->with('request',$request);

    }
    
    public function update_order_status()

    {
        try {
            DeliveryRequest::where('id', Request::get('id'))->update(array('status' => Request::get('status')));
        } catch (Exception  $e) {
            return $e->getMessage();
        }
        return "Done";

    }
    
    
    


    
     //general function, sms content and mobile number
    public function send_sms(){

        try{
            //destinnation/mobile number:
            $destinations = Request::get('mobile');
            $sms = Request::get('sms');

            $curl = new Curl();
            $url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper-Cab";
            $urlDiv = explode("?", $url);
            $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            Log::info("SMSSERVICELOG".$result);


            if (strpos($result, 'Success') !== false) {
                return 1;
            }else {
                return 0;
            }


        }catch (Exception $e){
           return $e->getMessage();
        }

       

    }


    //make view to get custom location only, include random number as third segment. done.
    public function get_customer_location (){

        //use this unique randomnumber to search text file for updated customer location:
        $randomnumber = Request::segment(3);
        return View::make("web.DeliveryApp.customerlinkforlocation")->with('randomnumber',$randomnumber);

    }

    //write in dir: customerlocation.txt confirmed customer location latitude and longitude labeled by random number.
    public function confirm_customer_location (){

        try{

            $latitude = Request::get('latitude');
            $longitude = Request::get('longitude');
            $randomnumber = Request::get('randomnumber');

            file_put_contents(public_path()."/DeliveryApp2/customerlocation.txt",
                "\n".$randomnumber." ".$latitude." ".$longitude, FILE_APPEND);


        } catch (Exception  $e) {
            return $e->getMessage();
        }

        return 'done';
    }

    //read from dir: customerlocation.txt
    public function retrieve_customer_location()
    {

        $randomnumber = Request::get('randomnumber');

        try {

            $handle = fopen(public_path()."/DeliveryApp2/customerlocation.txt", "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (strpos($line, $randomnumber) !== false) {
                        return $line;
                    }
                }
                fclose($handle);
            } else {
                return "can't open the file I do not know why";
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
        // meaning no location provided by the customer.
        return 0;
    }


    //make view to get customer information, including dtae, time and location.
    public function get_customer_information (){

        //use this unique randomnumber to search text file for updated customer location:
        $randomnumber = Request::segment(3);
        return View::make("web.DeliveryApp.customerlinkforinformation")->with('randomnumber',$randomnumber);

    }

    //create and write to temporary txt..
    public function confirm_customer_information (){

       try{

           $randomnumber = Request::get('randomnumber');
           $time = Request::get('time');
           $date = Request::get('date');
           $latitude = Request::get('latitude');
           $longitude = Request::get('longitude');

           file_put_contents(public_path()."/DeliveryApp2/customerinformation.txt",
               "\n".$randomnumber." ".$latitude." ".$longitude." ".$date." ".$time, FILE_APPEND);


       } catch (Exception  $e) {
           return $e->getMessage();
       }

        return 'done';
    }

    //read from dir: customerinformation.txt
    public function retrieve_customer_information (){

        $randomnumber = Request::get('randomnumber');

        try {



            $handle = fopen(public_path()."/DeliveryApp2/customerinformation.txt", "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (strpos($line, $randomnumber) !== false) {
                        return $line;
                    }
                }
                fclose($handle);
            } else {
                return "can't open the file I do not know why";
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
        // meaning no location provided by the customer.
        return 0;
    }






}