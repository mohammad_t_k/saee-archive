<?php

class Ticket {

    /*
    error codes:
    1. shipment not exist
    2. missing mandatory fields
    3. status change not allowed
    4. unknown error
    5. ticket not exist
    */
    
    private $waybill;
    private $shipment;
    private $ticket;
    private $ticket_action; 
    private $admin; 
    private $company;
    
    public function __construct($waybill, $admin_id = 66, $company_id = 0) {
        $this->waybill = $waybill;
        $this->shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        $this->ticket = Tickets::where('waybill', '=', $waybill)->first();
        $this->ticket_action = new TicketsActions;
        $this->admin = Admin::where('id', '=', $admin_id)->first();
        $this->company = Companies::where('id', '=', $company_id)->first();
    }
    
    public function open($updates = array(), $comment = 'Ticket Created', $priority = 'as is', $created_for = '', $type = 'as is') {
        try {
            $waybill = $this->waybill;
            $shipment = $this->shipment;
            $ticket = $this->ticket;
            $ticket_action = $this->ticket_action;
            $admin = $this->admin;
            $company = $this->company;
            if(!isset($shipment)) {
                $response = array('success' => false, 'waybill' => $waybill, 'error_code' => '1', 'error' => 'Shipment does not exist.');
                goto response;
            }
            if(!isset($ticket)) {
                if(empty($created_for)) {
                    $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '2', 'error' => 'Creation reason is mandatory.');
                    goto response;
                }
                if($priority == 'as is') {
                    $priority = 'Low';
                }
                if($type == 'as is') {
                    $type = '1';
                }
                DB::transaction(function() use($shipment, $updates, $ticket, $ticket_action, $admin, $company, $created_for, $priority, $comment, $type) {
                    $ticket = new Tickets;
                    $ticket->waybill = $shipment->jollychic;
                    $ticket->status = 1;
                    $ticket->priority = $priority;
                    $ticket->type = $type;
                    $ticket->created_for = $created_for;
                    $ticket->save();
                    $tmp_ticket_action = new TicketsActions;
                    $tmp_ticket_action->ticket_id = $ticket->waybill;
                    $tmp_ticket_action->ticket_priority = $ticket->priority;
                    $tmp_ticket_action->ticket_type = $ticket->type;
                    $tmp_ticket_action->admin = $admin->username;
                    $tmp_ticket_action->admin_id = $admin->id;
                    $tmp_ticket_action->company_id = isset($company) ? $company->id : 0;
                    $tmp_ticket_action->status = $ticket->status;
                    $tmp_ticket_action->shipment_name = $shipment->receiver_name;
                    $tmp_ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $tmp_ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $tmp_ticket_action->shipment_city = $shipment->d_city;
                    $tmp_ticket_action->shipment_hub = $shipment->hub_id;
                    $tmp_ticket_action->shipment_district = $shipment->d_district;
                    $tmp_ticket_action->shipment_address = $shipment->d_address;
                    $tmp_ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $tmp_ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $tmp_ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $tmp_ticket_action->comment = 'Ticket Created';
                    $tmp_ticket_action->save();
                    if(count($updates) || !empty($comment) && $comment != 'Ticket Created') {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                        $ticket_action->ticket_id = $ticket->waybill;
                        $ticket_action->ticket_priority = $ticket->priority;
                        $ticket_action->ticket_type = $ticket->type;
                        $ticket_action->admin = $admin->username;
                        $ticket_action->admin_id = $admin->id;
                        $ticket_action->company_id = isset($company) ? $company->id : 0;
                        $ticket_action->status = $ticket->status;
                        $ticket_action->shipment_name = $shipment->receiver_name;
                        $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                        $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                        $ticket_action->shipment_city = $shipment->d_city;
                        $ticket_action->shipment_hub = $shipment->hub_id;
                        $ticket_action->shipment_district = $shipment->d_district;
                        $ticket_action->shipment_address = $shipment->d_address;
                        $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                        $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                        $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                        $keys = array_keys($updates);
                        foreach($keys as $key) {
                            if($key == 'receiver_name') $ticket_action->is_name = 1;
                            if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                            if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                            if($key == 'd_city') $ticket_action->is_city = 1;
                            if($key == 'hub_id') $ticket_action->is_hub = 1;
                            if($key == 'd_district') $ticket_action->is_district = 1;
                            if($key == 'd_address') $ticket_action->is_address = 1;
                            if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                            if($key == 'scheduled_shipment_date') {
                                $ticket_action->is_scheduled_date = 1;
                                if(isset($updates['undelivered_reason'])) {
                                    $reason = $updates['undelivered_reason'];
                                }
                                else {
                                    $reason = '6';
                                }
                                $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                                $order_history = new OrdersHistory;
                                $order_history->waybill_number = $shipment->jollychic;
                                $order_history->order_number = $shipment->order_number;
                                $order_history->company_id = $shipment->company_id;
                                $order_history->status = $shipment->status;
                                $order_history->reason_id = $reason;
                                $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                                $order_history->save();
                            } 
                            if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                        }
                        $ticket_action->comment = $comment;
                        $ticket_action->save();
                    }
                });
            }
            else if(in_array($ticket->status, array(1, 4)) && (count($updates) > 0 || $priority != 'as is' && $priority != $ticket->priority || $type != 'as is' && $type != $ticket->type || $comment != 'Ticket Created')) {
                DB::transaction(function() use($shipment, $updates, $ticket, $ticket_action, $admin, $company, $created_for, $priority, $comment, $type) {
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->waybill = $shipment->jollychic;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 3) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $created_for, $priority, $comment, $type) {
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->status = 4;
                    $ticket_action->is_status = 1;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 2) {
                $response = $this->in_action($updates, $comment, $priority, '', $type);
                goto response;
            }
            $response = array('success' => true, 'ticket_id' => $shipment->jollychic, 'message' => "Ticket is opened.");
        } catch (Exception $e) {
            $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '4', 'error' => $e->getMessage(), 'line' => $e->getLine());
        }
        response:
        return $response;
    }

    public function in_action($updates = array(), $comment = 'Ticket in action', $priority = 'as is', $created_for = '', $type = 'as is') {
        try {
            $waybill = $this->waybill;
            $shipment = $this->shipment;
            $ticket = $this->ticket;
            $ticket_action = $this->ticket_action;
            $admin = $this->admin;
            $company = $this->company;
            if(!isset($shipment)) {
                $response = array('success' => false, 'waybill' => $waybill, 'error_code' => '1', 'error' => 'Shipment does not exist.');
                goto response;
            }
            if(!isset($ticket)) {
                $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '5', 'error' => "Ticket does not exist.");
                goto response;
            }
            if($ticket->status == 3) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $comment, $priority, $type) {
                    $tmp_ticket_action = new TicketsActions;
                    $tmp_ticket_action->ticket_id = $ticket->waybill;
                    $tmp_ticket_action->ticket_priority = $ticket->priority;
                    $tmp_ticket_action->ticket_type = $ticket->type;
                    $tmp_ticket_action->admin = $admin->username;
                    $tmp_ticket_action->admin_id = $admin->id;
                    $tmp_ticket_action->company_id = isset($company) ? $company->id : 0;
                    $tmp_ticket_action->status = 4;
                    $tmp_ticket_action->is_status = 1;
                    $tmp_ticket_action->shipment_name = $shipment->receiver_name;
                    $tmp_ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $tmp_ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $tmp_ticket_action->shipment_city = $shipment->d_city;
                    $tmp_ticket_action->shipment_hub = $shipment->hub_id;
                    $tmp_ticket_action->shipment_district = $shipment->d_district;
                    $tmp_ticket_action->shipment_address = $shipment->d_address;
                    $tmp_ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $tmp_ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $tmp_ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $tmp_ticket_action->comment = 'Ticket Reopened.';
                    $tmp_ticket_action->save();
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->status = 2;
                    $ticket_action->is_status = 1;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 1 || $ticket->status == 4) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $comment, $priority, $type) {
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->status = 2;
                    $ticket_action->is_status = 1;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 2 && (count($updates) > 0 || $priority != 'as is' && $priority != $ticket->priority || $comment != 'Ticket in action')) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $comment, $priority, $type) {
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            $response = array('success' => true, 'ticket_id' => $shipment->jollychic, 'message' => "Ticket is in action.");
        } catch (Exception $e) {
            $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error' => $e->getMessage(), 'line' => $e->getLine());
        }
        response:
        return $response;
    }

    public function close($updates = array(), $comment = 'Ticket Closed', $priority = 'as is', $type = 'as is') {
        try {
            $waybill = $this->waybill;
            $shipment = $this->shipment;
            $ticket = $this->ticket;
            $ticket_action = $this->ticket_action;
            $admin = $this->admin;
            $company = $this->company;
            if(!isset($shipment)) {
                $response = array('success' => false, 'waybill' => $waybill, 'error_code' => '1', 'error' => 'Shipment does not exist.');
                goto response; 
            }
            if(!isset($ticket)) {
                $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '5', 'error' => "Ticket does not exist.");
                goto response; 
            }
            else if($ticket->status == 1 || $ticket->status == 2 || $ticket->status == 4) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $comment, $priority, $type) {
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->status = 3;
                    $ticket_action->is_status = 1;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->is_cancelled;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 3 && (count($updates) > 0 || $priority != 'as is' && $priority != $ticket->priority || $comment != 'Ticket Closed')) {
                DB::transaction(function () use ($shipment, $updates, $ticket, $ticket_action, $admin, $company, $comment, $priority, $type) {
                    $tmp_ticket_action = new TicketsActions;
                    $tmp_ticket_action->ticket_id = $ticket->waybill;
                    $tmp_ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $tmp_ticket_action->admin = $admin->username;
                    $tmp_ticket_action->admin_id = $admin->id;
                    $tmp_ticket_action->company_id = isset($company) ? $company->id : 0;
                    $tmp_ticket_action->status = 4;
                    $tmp_ticket_action->is_status = 1;
                    $tmp_ticket_action->shipment_name = $shipment->receiver_name;
                    $tmp_ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $tmp_ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $tmp_ticket_action->shipment_city = $shipment->d_city;
                    $tmp_ticket_action->shipment_hub = $shipment->hub_id;
                    $tmp_ticket_action->shipment_district = $shipment->d_district;
                    $tmp_ticket_action->shipment_address = $shipment->d_address;
                    $tmp_ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $tmp_ticket_action->shipment_is_cancelled = $shipment->undelivered_reason == 3 ? 1 : 0;
                    $tmp_ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $tmp_ticket_action->comment = 'Ticket Reopened.';
                    $tmp_ticket_action->save();
                    if(count($updates)) {
                        DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update($updates);
                        $shipment = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->first();
                    }
                    $ticket->status = 3;
                    $ticket_action->is_status = 1;
                    if($priority != 'as is' && $priority != $ticket->priority) {
                        $ticket->priority = $priority;
                        $ticket_action->is_priority = 1;
                    }
                    if($type != 'as is' && $type != $ticket->type) {
                        $ticket->type = $type;
                        $ticket_action->is_type = 1;
                    }
                    $ticket->save();
                    $ticket_action->ticket_id = $ticket->waybill;
                    $ticket_action->ticket_priority = $ticket->priority;
                    $ticket_action->ticket_type = $ticket->type;
                    $ticket_action->admin = $admin->username;
                    $ticket_action->admin_id = $admin->id;
                    $ticket_action->company_id = isset($company) ? $company->id : 0;
                    $ticket_action->status = $ticket->status;
                    $ticket_action->shipment_name = $shipment->receiver_name;
                    $ticket_action->shipment_mobile1 = $shipment->receiver_phone;
                    $ticket_action->shipment_mobile2 = $shipment->receiver_phone2;
                    $ticket_action->shipment_city = $shipment->d_city;
                    $ticket_action->shipment_hub = $shipment->hub_id;
                    $ticket_action->shipment_district = $shipment->d_district;
                    $ticket_action->shipment_address = $shipment->d_address;
                    $ticket_action->shipment_scheduled_date = isset($shipment->scheduled_shipment_date) ? $shipment->scheduled_shipment_date : '';
                    $ticket_action->shipment_is_cancelled = $shipment->is_cancelled;
                    $ticket_action->shipment_urgent_delivery = $shipment->urgent_delivery;
                    $keys = array_keys($updates);
                    foreach($keys as $key) {
                        if($key == 'receiver_name') $ticket_action->is_name = 1;
                        if($key == 'receiver_phone') $ticket_action->is_mobile1 = 1;
                        if($key == 'receiver_phone2') $ticket_action->is_mobile2 = 1;
                        if($key == 'd_city') $ticket_action->is_city = 1;
                        if($key == 'hub_id') $ticket_action->is_hub = 1;
                        if($key == 'd_district') $ticket_action->is_district = 1;
                        if($key == 'd_address') $ticket_action->is_address = 1;
                        if($key == 'undelivered_reason' && $updates[$key] == 3) $ticket_action->is_is_cancelled = 1;
                        if($key == 'scheduled_shipment_date') {
                            $ticket_action->is_scheduled_date = 1;
                            if(isset($updates['undelivered_reason'])) {
                                $reason = $updates['undelivered_reason'];
                            }
                            else {
                                $reason = '6';
                            }
                            $undelivered_reason = UndeliveredReason::where('id', '=', $reason)->first();
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $shipment->jollychic;
                            $order_history->order_number = $shipment->order_number;
                            $order_history->company_id = $shipment->company_id;
                            $order_history->status = $shipment->status;
                            $order_history->reason_id = $reason;
                            $order_history->notes = $undelivered_reason->english . ' on ' . $updates['scheduled_shipment_date'];
                            $order_history->save();
                        } 
                        if($key == 'urgent_delivery') $ticket_action->is_urgent_delivery = 1;
                    }
                    $ticket_action->comment = $comment;
                    $ticket_action->save();
                });
            }
            else if($ticket->status == 3) {
                $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '3', 'error' => "Ticket already closed.");
                goto response;
            }
            $response = array('success' => true, 'ticket_id' => $shipment->jollychic, 'message' => "Ticket is closed.");
        } catch (Exception $e) {
            $response = array('success' => false, 'ticket_id' => $shipment->jollychic, 'error_code' => '4', 'error' => $e->getMessage(), 'line' => $e->getLine());
        }
        response:
        return $response;
    }

    public static function check_status($waybill) {
        $result = Tickets::where('waybill', '=', $waybill)->first();
        if(!isset($result)) {
            $response = array('success' => false, 'ticket_id' => $waybill, 'error_code' => '5', 'error' => 'Ticket does not exist');
            goto response;
        }
        $status_name = TicketsStatuses::where('id', '=', $result->status)->first();
        $response = array('success' => true, 'ticket_id' => $waybill, 'message' => $status_name->english);
        response:
        return $response;
    }

    static public function make_action($waybill, $shipment_updates = array(), $ticket_updates = array()) {
        try {
            $shipment = $this->shipment;
            $ticket = $this->ticket;
            $ticket_action = $this->ticket_action;
            $admin_id = $this->admin_id;
            $status = $this->status;
            $comment = $this->comment;
            $priority = $this->priority;
            DB::transaction(function() use($shipment, $shipment_updates, $ticket_updates, $ticket, $ticket_action, $admin_id, $created_for, $priority, $comment) {

            });
            $response = array('success' => true, 'message' => "Ticket $shipment->jollychic updated successfully.");
        } catch (Exception $e) {
            $response = array('success' => false, 'error' => $e->getMessage(), $e->getLine());
        }
        response:
        return $response;
    }
        
}

