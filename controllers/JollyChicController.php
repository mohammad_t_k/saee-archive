<?php

/*
 * handles delivery requests tracking and data manipulations
 */

use App\Classes\Curl;


class JollyChicController extends \BaseController
{


    public function liveTests()
    {
        echo "hala";
        return 1;
    }


    public function post()
    {

        try {

            $delivery_request = new DeliveryRequest;
            $order_history = new OrdersHistory;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");


            //   shipmentnum int Optional
            if (Request::has('shipmentnum')) {
                $shipmentnum = Request::get('shipmentnum');
                //$delivery_request->d_shipmentnum = $shipmentnum;
            }

            //   waybill ints and chars Mandatory
            if (Request::has('waybill')) {
                $waybill = Request::get('waybill');

                if (preg_match('/^JC[0-9]{8}KS/', $waybill) != 1) {
                    return -1;
                }

                if (count(DeliveryRequest::select('jollychic')->where('jollychic', '=', $waybill)->get())) {
                    return $response_array = array('success' => false, 'error' => 'Waybill already exists', 'error_code' => -2, 'waybill' => $waybill);
                }
            } else {
                return '0';
            }

            $delivery_request->jollychic = $waybill;

            //   Email ints and chars Optional
            if (Request::has('email')) {
                $email = str_replace(PHP_EOL, ' ', Input::get('email'));

                $delivery_request->receiver_email = $email;
            }
			
			// company id Optional
            if (Request::has('companyid')) {
                $company_id = Request::get('companyid');
            } else {
                $company_id = 911;
            }


            //   Order_number ints and chars Optional
            if (Request::has('ordernumber')) {
                $order_number = Request::get('ordernumber');
                if (count(DeliveryRequest::select('order_number')->where('order_number', '=', $order_number)->where('company_id', '=', $company_id)->get())) {
                    return $response_array = array('success' => false, 'error' => 'order number  already exists, Please try again.', 'error_code' => -2, 'order number' => $order_number);
                }
                $delivery_request->order_number = $order_number;
            }


            //  cashondelivery float Mandatory
            if (Request::has('cashondelivery')) {
                $cash_on_delivery = Request::get('cashondelivery');
            } else {
                return '0';
            }

            $delivery_request->cash_on_delivery = $cash_on_delivery;

            //  name string Mandatory
            if (Request::has('name')) {
                $dropoff_name = Request::get('name');
            } else {
                return '0';
            }

            $delivery_request->receiver_name = $dropoff_name;


            //  mobile ints Mandatory
            if (Request::has('mobile')) {
                $dropoff_mobile = Request::get('mobile');
                $dropoff_mobile = format_phone($dropoff_mobile);
                /*$mobilelength = strlen((string)$dropoff_mobile);
                if ($mobilelength <= 11) {
                    $dropoff_mobile = preg_replace('/^0/', '966', $dropoff_mobile);
                }*/
            } else {
                return '0';
            }

            $delivery_request->receiver_phone = $dropoff_mobile;


            // mobile2 ints Optional
            if (Request::has('mobile2')) {
                $dropoff_mobile2 = Request::get('mobile2');
                $dropoff_mobile2 = format_phone($dropoff_mobile2);
                /*$mobile2length = strlen((string)$dropoff_mobile2);
                if ($mobile2length <= 11) {
                    $dropoff_mobile2 = preg_replace('/^0/', '966', $dropoff_mobile2);
                }*/
                $delivery_request->receiver_phone2 = $dropoff_mobile2;
            }


            //   streetaddress string Mandatory
            if (Request::has('streetaddress')) {
                $d_address = str_replace(PHP_EOL, ' ', Request::get('streetaddress'));
                $d_address = trim(preg_replace('/\s+/', ' ', $d_address));
            } else {
                return '0';
            }

            $delivery_request->d_address = $d_address;

            //  streetaddress2 string Optional
            if (Request::has('streetaddress2')) {
                $d_address2 = str_replace(PHP_EOL, '', Request::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/', ' ', $d_address2));
                $delivery_request->d_address2 = $d_address2;
            }

            //  streetaddress3 string Optional
            if (Request::has('streetaddress3')) {
                $d_address3 = str_replace(PHP_EOL, '', Request::get('streetaddress3'));
                $d_address3 = trim(preg_replace('/\s+/', ' ', $d_address3));
                $delivery_request->d_address3 = $d_address3;
            }

            //   district  string Mandatory
            if (Request::has('district')) {
                $d_district = Request::get('district');
                if (strpos($d_district, '+') !== false) {

                    $d_city = strtok($d_district, '+');

                    $d_district = strtok('');
                    $delivery_request->d_city = $d_city;
                }
                $delivery_request->d_district = $d_district;
            } else {
                $d_district = '';
            }


            //  city  string Mandatory
            if (Request::has('city')) {
                $d_city = Request::get('city');
                $delivery_request->d_city = $d_city;
            } else {
                return '0';
            }


            //  state  string Mandatory
            if (Request::has('state')) {
                $d_state = Request::get('state');
                if ($d_state == 'Makkah' || $d_state == 'Jeddah' || $d_state == 'Taif') {

                    $d_state = 'Makkah{Mecca}';
                }

                if ($d_state == 'Baha') {

                    $d_state = 'Al Baha';
                }

                if ($d_state == 'Dammam') {

                    $d_state = 'Eastern Region {Ash-Sharqiyah}';
                }
            } else {
                return '0';
            }

            $delivery_request->d_state = $d_state;


            //  zipcode ints Optional
            if (Request::has('zipcode')) {
                $d_zipcode = Request::get('zipcode');
                $delivery_request->d_zipcode = $d_zipcode;
            }


            //   latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Request::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            //  longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Request::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }


            //   weight float Mandatory
            if (Request::has('weight')) {
                $d_weight = Request::get('weight');
            } else {
                return '0';
            }

            $delivery_request->d_weight = $d_weight;

            // quantity ints Mandatory
            if (Request::has('quantity')) {
                $d_quantity = Request::get('quantity');
            } else {
                return '0';
            }

            $delivery_request->d_quantity = $d_quantity;

            //   description strings Optional
            if (Request::has('description')) {
                $d_description = Request::get('description');
                $delivery_request->d_description = $d_description;
            }


            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->origin_latitude = 21.575723;
            $delivery_request->origin_longitude = 39.148976;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->pincode2 = mt_rand(1000, 9999);
            $delivery_request->company_id = $company_id;
            $delivery_request->status = 0;

            $delivery_request->save();


            $order_history->waybill_number = $waybill;
            $order_history->order_number = $delivery_request->order_number;
            $order_history->status = 0;
            $order_history->notes = 'Label Created';
            $order_history->company_id = $company_id;
            $order_history->save();

            $dr = DeliveryRequest::select('id')->where('jollychic', '=', $waybill)->first();

            //insert customer tracking number:
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array('customer_tracking_number' => $dropoff_mobile . '-' . $dr->id));

            return 1;


        } catch
        (Exception $e) {

            return $e->getMessage();
        }


    }


    //API call to get item status need item ID only that is of jollychic ID.
    public function getitemstatus()
    {

        try {
            $jollychicid = Request::segment(3);
            Log::info($jollychicid);

            $item = DeliveryRequest::select('status')->where('jollychic', '=', $jollychicid)->first();
            Log::info($item);

            if (count($item)) {
                if ($item->status == -1) {
                    return 0;
                } else if ($item->status == 0) {
                    return 0;
                } else if ($item->status == 1) {
                    return 1;
                } else if ($item->status == 2) {
                    return 2;
                } else if ($item->status == 3) {
                    return 3;
                } else if ($item->status == 4) {
                    return 4;
                } else if ($item->status == 5) {
                    return 5;
                } else if ($item->status == 6) {
                    return 6;
                } else if ($item->status == 7) {
                    return 7;
                } else if ($item->stautus == 8) {
                    return 5;
                }

            } else {
                return -2;
            }


        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function update_item_status_now()
    {

        try {

            $jollychicid = Request::segment(3);
            $status = Request::segment(4);
            $captainid = Request::segment(5);
            $undeliveredreason = Request::segment(6);
            $pickup_time_interval = Request::get('pickup_time_interval');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');

            if ($status >= 0 && $status < 4 || is_null($captainid) || $captainid < 0) {
                $captainid = 0;
            }

            $item = DeliveryRequest::select('planned_walker','confirmed_walker','status','hub_id','d_city','new_shipment_date','signin_date','num_faild_delivery_attempts','order_number','company_id','pickup_walker','pincode','cash_on_delivery','sender_name','receiver_phone','receiver_phone2','scheduled_shipment_date')->where('jollychic', '=', $jollychicid)->first();
            $walker = Walker::find($captainid);
			$mission = Mission::select('id', 'status', 'reserve_captain', 'planned_captain', 'total_shipments', 'estimated_delivery_fee','scheduled_shipment_date')
                ->where('mission_waybill', '=', $jollychicid)->first();
            if (count($mission) && count($walker)) {
                $result = $this->missionshipmentsscan($mission, $walker, $status);
                return $result;
            } else {
            $oldWalker = $item->confirmed_walker;
            $oldStatus = $item->status;
            if (count($item) && count($walker)) {
                /*if(strtolower($item->d_city) != strtolower($walker->city)){
                    return -3;
                }*/
                if(-3 <= $status && $status <= -1) {
                    $update_status = new UpdateStatus($jollychicid, $status, $captainid, 0, 0, -1, $pickup_time_interval);
                    $response = $update_status->execute();
                    if(!$response['success'] && $response['error_code'] == '5') // (-3 => -2): reserved walker is different than pickup walker
                        return -4;
                    else if(!$response['success'])
                        return -1;
                    return 1;
                }
                else if (0 <= $status && $status <= 7) {
                    if ($status == 3 && ($oldStatus == 2 || $oldStatus == -1)) {

                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'status' => $status, 'confirmed_walker' => $captainid));
                        $order_history = new OrdersHistory;
                        $order_history->waybill_number = $jollychicid;
                        $order_history->order_number = $item->order_number;
                        $order_history->company_id = $item->company_id;
                        $order_history->status = $status;
                        $order_history->save();

                        /*if ($oldWalker) {

                            $delivery_request_payment = new Transactions;

                            $delivery_request_payment->captain_id = $oldWalker;
                            $delivery_request_payment->admin_id = Auth::user()->id;
                            $delivery_request_payment->op_type = 3;

                            $delivery_request_payment->op_method = 1;

                            $delivery_request_payment->op_amount = $item->cash_on_delivery;
                            $delivery_request_payment->delivery_request_id = $item->id;

                            if ($item->cash_on_delivery == 0) {
                                $delivery_request_payment->notes = 'No COD';
                            } else {
                                $delivery_request_payment->notes = '';
                            }

                            $delivery_request_payment->save();

                        }*/

                        if($oldStatus == -1)
                            Walker::where('id', '=', $captainid)->decrement('picked_up');


                    } else if ($status == 3 && $oldStatus == 2) {

                        // DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                        //     'status' => $status, 'confirmed_walker' => $captainid));
                        // $order_history = new OrdersHistory;
                        // $order_history->waybill_number = $jollychicid;
                        // $order_history->order_number = $item->order_number;
                        // $order_history->company_id = $item->company_id;
                        // $order_history->status = $status;
                        // $order_history->save();

                        /*if ($oldWalker) {

                            $delivery_request_payment = new Transactions;

                            $delivery_request_payment->captain_id = $oldWalker;
                            $delivery_request_payment->admin_id = Auth::user()->id;
                            $delivery_request_payment->op_type = 3;

                            $delivery_request_payment->op_method = 1;

                            $delivery_request_payment->op_amount = $item->cash_on_delivery;
                            $delivery_request_payment->delivery_request_id = $item->id;

                            if ($item->cash_on_delivery == 0) {
                                $delivery_request_payment->notes = 'No COD';
                            } else {
                                $delivery_request_payment->notes = '';
                            }

                            $delivery_request_payment->save();

                        }*/


                    } else if ($status == 4 && $oldStatus == 3) {
                        //if ($walker->city != 'Makkah') {

                            if ($walker->is_approved == 0) {
                                return -5;
                            }

                            $unscheduled = DeliveryRequest::select('scheduled_shipment_date')->where('jollychic', '=', $jollychicid)->whereNull('scheduled_shipment_date')->count();
                            if ($unscheduled > 0) {
                                return -6;
                            }
                            $pendingPayment = DeliveryRequest::select('is_money_received')->where('confirmed_walker', '=', $captainid)->where('status', '=', 5)->where('is_money_received', '=', 0)->count();
                            if ($pendingPayment > 0) {
                                return -7;
                            }
                            $pendingCount = DeliveryRequest::select('confirmed_walker')->where('confirmed_walker', '=', $captainid)->where('status', '=', 6)->count();
                            if ($pendingCount > 0) {
                                return -8;
                            }
                            $today = date('Y-m-d');
                            $pendingshipments = DeliveryRequest::select('confirmed_walker')->where('confirmed_walker', '=', $captainid)->where('scheduled_shipment_date', '<', $today)->where('status', '<>', 5)->count();
                            if ($pendingshipments > 0) {
                                return -9;
                            }
                            if ($item->scheduled_shipment_date > $today) {
                                return -10;
                            }
							$num_faild_delivery_attempts = $item->num_faild_delivery_attempts;
							$new_shipment_date = $item->new_shipment_date;
                            $company_id = $item->company_id;
                            $d_city = $item->d_city;
                            $maincity = DB::select("SELECT DISTINCT city from districts where SUBSTRING_INDEX(district, '+', 1) = '$d_city'")[0]->city;

                            $constraints = ControlPanel::select('returned_to_supplier','max_delivery_attempts')->where('company_id', '=', $company_id)->where('city','=',$maincity)->first();
                            $maxAge = $constraints->returned_to_supplier;
                            $max_delivery_attempts = $constraints->max_delivery_attempts;
                            //shipment exceed its maximum attempts
                            if ($num_faild_delivery_attempts > $max_delivery_attempts) {
                                return -20;
                            }

                            $today = date_create(date('Y-m-d'));
                            $date = date_create(date('Y-m-d', strtotime($new_shipment_date)));
                            $interval=date_diff($date,$today);
                            $interval = $interval->format('%R%a');

                            ////shipment exceed its maximum days to return
                            if ($interval > $maxAge) {
                                return -21;
                            }


                        $enableMissionCreation = Hub::select('enable_missions_creation')->where('id', '=', $item->hub_id)->first()->enable_missions_creation;
                        if ($enableMissionCreation == 0) {
                            $flag = DeliveryRequest::where('jollychic', '=', $jollychicid)->where('planned_walker', '=', 0)->update(array(
                                'status' => $status, 'planned_walker' => $captainid, 'confirmed_walker' => $captainid, 'scheduled_shipment_date' => date('Y-m-d')));

                        } else {
                            $unReserve_mission_shipment = DeliveryRequest::select('mission_id')->where('jollychic', '=', $jollychicid)->where('mission_id', '<>', 0)
                                ->where('planned_walker', '=', 0)->first();
                            if (count($unReserve_mission_shipment)) {
                                $flag = DeliveryRequest::where('jollychic', '=', $jollychicid)->where('planned_walker', '=', 0)
                                    ->update(array('mission_id' => 0, 'status' => '4', 'planned_walker' => $captainid, 'confirmed_walker' => $captainid, 'scheduled_shipment_date' => $today));
                                if ($flag > 0) {
                                    $mission = Mission::select('district', 'total_shipments')->where('id', '=', $unReserve_mission_shipment->mission_id)->where('planned_captain', '=', 0)->first();
                                    $new_total_shipments = $mission->total_shipments - 1;
                                    $misssion_name = $mission->district . ' District-' . $new_total_shipments;
                                    if ($new_total_shipments == 0) {
                                        Mission::where('id', '=', $unReserve_mission_shipment->mission_id)->where('planned_captain', '=', 0)
                                            ->update(array('status' => 6));

                                    } else {
                                        Mission::where('id', '=', $unReserve_mission_shipment->mission_id)->where('planned_captain', '=', 0)
                                            ->update(array('total_shipments' => $new_total_shipments, 'mission_name' => $misssion_name));
                                    }
                                }

                            } else {
                                if ($item->planned_walker != $captainid) {
                                    return -12;
                                } else {
                                    $flag = DeliveryRequest::where('jollychic', '=', $jollychicid)->where('planned_walker', '=', $captainid)->update(array(
                                        'status' => $status, 'confirmed_walker' => $captainid, 'scheduled_shipment_date' => date('Y-m-d')));
                                }
                            }

                        }
                    /*} else {
                            $flag = DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                                'status' => $status, 'confirmed_walker' => $captainid, 'scheduled_shipment_date' => date('Y-m-d')));
                        }*/
                        if($flag>0){
                        $order_history = new OrdersHistory;
                        $order_history->waybill_number = $jollychicid;
                        $order_history->order_number = $item->order_number;
                        $order_history->company_id = $item->company_id;
                        $order_history->status = $status;
                        $order_history->walker_id = $captainid;
                        $order_history->save();

                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'pickup_timestamp' => date('Y-m-d H:i:s')));

                        try {
                            if ($item->company_id == 917 || $item->company_id == 931) {
                                $sms = "عزيزي/زتي ";
                                $sms .= "يوجد لديكم شحنة عن طريق ";
                                $company = Companies::find($item->company_id);
                                if (isset($company)) {
                                    $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
                                }
                                $sms .= "  رقم ";
                                $sms .= $item->order_number;
                                $sms .= "  برقم كود ";
                                $sms .= $item->pincode;

                                if ($item->cash_on_delivery) {
                                    $sms .= " ومبلغ ";
                                    $sms .= $item->cash_on_delivery;
                                    $sms .= " ريال";
                                }
                                $sms .= "  مع المندوب ";

                                if ($captainid > 0) {
                                    $walker = Walker::find($captainid);
                                    $walker->total_scanned = $walker->total_scanned + 1;
                                    $walker->save();
                                    if (count($walker)) {
                                        $sms .= $walker->first_name;
                                        $sms .= " 0";
                                        $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                                    }
                                }
                            } else {
                                $sms = "عزيزي عميل ";
                                $company = Companies::find($item->company_id);
                                if (isset($company)) {
                                    $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
                                }

                                $sms .= " طلبكم سيصلكم اليوم مع المندوب ";
                                //get order:

                                //get walker:

                                if ($captainid > 0) {
                                    $walker = Walker::select('first_name', 'phone')->where('id', '=', $captainid)->first();

                                    if (count($walker)) {
                                        $sms .= $walker->first_name;
                                        $sms .= " 0";
                                        $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                                    }
                                }


                                $sms .= " رمز التسليم ";
                                $sms .= $item->pincode;

                                if ($item->cash_on_delivery) {
                                    $sms .= " ومبلغ الطلب ";
                                    $sms .= $item->cash_on_delivery;
                                    $sms .= " ريال";
                                }
                                $sms .= " مع تحيات ساعي";
                            }


                            $destinations1 = "966";
                            $destinations1 .= substr(str_replace(' ', '', $item->receiver_phone), -9);

                            if (isset($company) && $company->id != '952' || !isset($company)) {

                                $curl = new Curl();
                                //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                                $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

                                $urlDiv = explode("?", $url);
                                $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                                Log::info("SMSSERVICELOG" . $result);


                                if ($item->receiver_phone2 != '') {
                                    $destinations2 = "966";
                                    $destinations2 .= substr(str_replace(' ', '', $item->receiver_phone2), -9);
                                    $curl = new Curl();
                                    //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                                    $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

                                    $urlDiv = explode("?", $url);
                                    $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                                    Log::info("SMSSERVICELOG" . $result);
                                }

                            }

                            /*if ($oldWalker != $captainid) {
                                $delivery_request_payment = new Transactions;

                                $delivery_request_payment->captain_id = $captainid;
                                $delivery_request_payment->admin_id = 0;
                                $delivery_request_payment->op_type = 1;

                                $delivery_request_payment->op_method = 1;

                                $delivery_request_payment->op_amount = $item->cash_on_delivery;
                                $delivery_request_payment->delivery_request_id = $item->id;

                                if ($item->cash_on_delivery == 0) {
                                    $delivery_request_payment->notes = 'No COD';
                                } else {
                                    $delivery_request_payment->notes = '';
                                }

                                $delivery_request_payment->save();
                            }*/

                        } catch (Exception $e) {
                            'error:' . ' ' . $e->getMessage();
                        }
                    }

                    } else if ($status == 5 && $oldStatus == 4) {
                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'status' => $status, 'confirmed_walker' => $captainid));
                        if ($captainid == 0) {
                            $captainid = $item->confirmed_walker;
                        }
                        $walker = Walker::find($captainid);
                        $walker->total_delivered=$walker->total_delivered+1;
                        $walker->save();
                        $order_history = new OrdersHistory;
                        $order_history->waybill_number = $jollychicid;
                        $order_history->order_number = $item->order_number;
                        $order_history->company_id = $item->company_id;
                        $order_history->status = $status;
                        $order_history->walker_id = $captainid;
                        $order_history->save();


                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'dropoff_timestamp' => date('Y-m-d H:i:s')));

                        $ticket = new Ticket($jollychicid, 66);
                        $ticket->close(array(), "Ticket Closed On Delivery");

                        if ($oldStatus != 5) {
                            /*$delivery_request_payment = new Transactions;

                            $delivery_request_payment->captain_id = $captainid;
                            $delivery_request_payment->admin_id = '0';
                            $delivery_request_payment->op_type = '6';

                            $delivery_request_payment->op_method = '1';

                            $delivery_request_payment->op_amount = $item->cash_on_delivery;
                            $delivery_request_payment->delivery_request_id = $item->id;
                            $delivery_request_payment->notes = '';

                            $delivery_request_payment->save();*/

                            // 10 SAR
                            /*
                            $delivery_request_payment = new Transactions;

                            $delivery_request_payment->captain_id = $captainid;
                            $delivery_request_payment->admin_id= '0' ;
                            $delivery_request_payment->op_type= '5';

                            $delivery_request_payment->op_method= '1';

                            $delivery_request_payment->op_amount= '10.0';
                            $delivery_request_payment->delivery_request_id= $item->id;
                            $delivery_request_payment->notes= 'For delivery of shipment: '+ $jollychicid;

                            $delivery_request_payment->save();
                            */
                        }

                        //
                        /* echo 'status = 5555';
                        $file_name = time();
                        $file_name .= rand();
                        $file_name = sha1($file_name);

                        $s3_url = "";
                        if (Input::hasfile('picture')) {
                            $ext = Input::file('picture')->getClientOriginalExtension();
                            Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                            $local_url = $file_name . "." . $ext;

                            // Upload to S3
                            if (Config::get('app.s3_bucket') != "") {
                                $s3 = App::make('aws')->get('s3');
                                $pic = $s3->putObject(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'SourceFile' => public_path() . "/uploads/" . $local_url,
                                ));

                                $s3->putObjectAcl(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'ACL' => 'public-read'
                                ));

                                $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                            } else {
                                $s3_url = asset_url() . '/uploads/' . $local_url;
                            }

                            DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                                'signature_url' => $s3_url));

                        } else {
                        } */
                    } else if ($status == 6 && $oldStatus == 4) {


                         $maxAgeQuery = 'select `control_panel`.`returned_to_supplier`, `delivery_request`.`new_shipment_date` from `delivery_request` left join `control_panel` on `delivery_request`.`company_id` = `control_panel`.`company_id` and (delivery_request.d_city = control_panel.city or delivery_request.d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) as "city" from districts where districts.city = control_panel.city)) where `delivery_request`.`jollychic` = "'.$jollychicid.'" ';

                        $maxAge = DB::select($maxAgeQuery);

                        $new_shipment_date = $maxAge[0]->new_shipment_date;

                        $maxAge = $maxAge[0]->returned_to_supplier;

                        $today = new DateTime(); // get current date in date time formate // 

                        $date = new DateTime($new_shipment_date);

                        $interval = $today->diff($date); // diff function only works with datetime format
                       
                        $interval = $interval->format('%R%a');

                        if($interval > $maxAge){

                            return -16; // -16 not allowed captain to reschdule
                        }

                        // else allow reschdule scheduled_shipment_date
                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'status' => $status, 'confirmed_walker' => $captainid));
                        $delivery_failed_because = UndeliveredReason::select('english')->where('id', '=', $undeliveredreason)->get()->first();

                        $order_history = new OrdersHistory;
                        $order_history->waybill_number = $jollychicid;
                        $order_history->order_number = $item->order_number;
                        $order_history->company_id = $item->company_id;
                        $order_history->status = $status;


                        $order_history->walker_id = $captainid;
                        $order_history->notes = $delivery_failed_because->english;
                        $order_history->save();
                        
                        if($undeliveredreason == 14 && !empty($scheduled_shipment_date)){ // if reschule by captain

                            DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'undelivered_reason' => $undeliveredreason, 'scheduled_shipment_date' => $scheduled_shipment_date));


                        }else{

                        DeliveryRequest::where('jollychic', '=', $jollychicid)->update(array(
                            'undelivered_reason' => $undeliveredreason));

                        }

                        DeliveryRequest::where('jollychic', '=', $jollychicid)->increment('num_faild_delivery_attempts');
                    }

                    if($captainid > 0){
                        $this->updatecaptainid($captainid);
                    }
                    return 1;
                } else {
                    if($captainid > 0){
                        $this->updatecaptainid($captainid);
                    }
                    return -1;
                }
            } else {
                if($captainid > 0){
                    $this->updatecaptainid($captainid);
                }
                return -2;
            }
			}

            if($captainid > 0){
                $this->updatecaptainid($captainid);
            }

        } catch (Exception $e) {
            Log::info('error:' . ' ' . $e->getMessage());
        } finally {
            if($captainid > 0){
                $this->updatecaptainid($captainid);
            }
        }

    }    
	public function updatecaptainid($captainid) {
        if($captainid == 0 || $captainid == '')
            return false;
        $delivered = DB::select("select count(dr.id) 'delivered' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 5 and dr.is_money_received = 0")[0]->delivered;
        $with_captain = DB::select("select count(delivery_request.id) 'with_captain' from delivery_request where confirmed_walker = $captainid and status = 4")[0]->with_captain;
        $returned = DB::select("select count(dr.id) 'returned' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 6")[0]->returned;
        $lastscheduledshipmentdate = DB::select("select ifnull(max(scheduled_shipment_date), '') 'lastscheduledshipmentdate' from delivery_request where confirmed_walker = $captainid")[0]->lastscheduledshipmentdate;
        $last_payment_date = DB::select("SELECT max(date) 'last_payment_date' FROM captain_account_history as cahd WHERE cahd.captain_id = $captainid")[0]->last_payment_date;
        $balance = DB::select("select sum(cah.captain_amount) 'balance' from captain_account_history cah where cah.captain_id = $captainid and cah.paid_to_captain = 0")[0]->balance;
        $pending_since = "select min(PendingSince) 'pendingsince' from (
            select min(scheduled_shipment_date) 'PendingSince' from delivery_request where status in (4,6)  and confirmed_walker = $captainid
            union 
            select min(dropoff_timestamp) 'PendingSince' from delivery_request where status = 5 and is_money_received = 0  and confirmed_walker = $captainid
            ) A";
        $pending_since = DB::select($pending_since)[0]->pendingsince;
        if($pending_since == NULL)
            $pending_since = '0000-00-00 00:00:00';
        $delayedcountquery = "select waybill_number 'DelayedShipments' from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) union select waybill_number 'DelayedShipments' from orders_history where status = 5 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 5 and is_money_received = 0 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 union select waybill_number 'DelayedShipments' from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) ";
        $delayedcount = DB::select($delayedcountquery);
        $delayedcountarray = '(';
        for($i = 0; $i < count($delayedcount); ++$i)
            if($i == 0)
                $delayedcountarray .= "'".$delayedcount[$i]->DelayedShipments."'";
            else
                $delayedcountarray .= ','."'".$delayedcount[$i]->DelayedShipments."'";
        $delayedcountarray .= ')';
        if(count($delayedcount) > 0)
            $delayedamount = DB::select("select sum(cash_on_delivery) 'delayedamount' from delivery_request where jollychic in $delayedcountarray")[0]->delayedamount;
        else
            $delayedamount = 0;
        DB::update("update walker set delivered = ? , with_captain = ? , returned = ? , last_scheduled_shipment_date = ? , last_payment_date = ? , balance = ? , pending_since = ? , delayed_count = ? , delayed_amount = ? where id = ?", [$delivered, $with_captain, $returned, $lastscheduledshipmentdate, $last_payment_date, $balance, $pending_since, count($delayedcount), $delayedamount, $captainid]);
        return true;
    }

    public function update_item_status_admin()
    {

        try {

            $id = Input::get('id');
            $status = Input::get('status');
            $adminid = Input::get('admin_id');
            $newdate = Input::get('new_shipment_date');
            $admin = Admin::find($adminid);
            $city = Input::get('city');
            $company_id = Input::get('company_id');

            if (isset($admin) && ($admin->role == 5 || $admin->role == 12)) {
                $searchcolumn = 'jollychic';
                if (!preg_match('/^JC[0-9]{8}KS/', $id) && !preg_match('/^OS[0-9]{8}KS/', $id)) {
                    $searchcolumn = 'order_number';
                }
                $itemquery = DeliveryRequest::select('id','jollychic','status','confirmed_walker','order_number','company_id','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','d_address3','D_latitude','D_longitude','sender_name','sender_phone','cash_on_delivery','created_at','updated_at','new_shipment_date')->where($searchcolumn, '=', $id)->where('company_id', '=', $company_id);
                if (isset($city) && $city != 'All' && $status == 3) {
                    $itemquery = $itemquery->where('d_city', '=', $city);
                }
                $item = $itemquery->first();
                $company = Companies::find($item->company_id);
                if (isset($item)) {
                    $oldWalker = $item->confirmed_walker;
                    $oldStatus = $item->status;
                    $waybill = $item->jollychic;
                    if (0 <= $status && $status <= 3) { // status 2 change
                        if ($status == 2 && $oldStatus < 2) {
                            if (!isset($newdate)) {
                                $response_array = array('success' => false, 'error' => 'Invalid New Shipment update.');
                                $response = Response::json($response_array, 200);
                                return $response;
                            }
                            DeliveryRequest::where($searchcolumn, '=', $id)->where('company_id', '=', $company_id)->update(array(
                                'status' => $status, 'new_shipment_date' => $newdate));
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $waybill;
                            $order_history->order_number = $item->order_number;
                            $order_history->company_id = $item->company_id;
                            $order_history->status = $status;
                            $order_history->admin_id = $adminid;
                            $order_history->save();

                            /*if ($oldWalker) {

                                $delivery_request_payment = new Transactions;

                                $delivery_request_payment->captain_id = $oldWalker;
                                $delivery_request_payment->admin_id = $adminid;
                                $delivery_request_payment->op_type = 3;

                                $delivery_request_payment->op_method = 1;

                                $delivery_request_payment->op_amount = $item->cash_on_delivery;
                                $delivery_request_payment->delivery_request_id = $item->id;

                                if ($item->cash_on_delivery == 0) {
                                    $delivery_request_payment->notes = 'No COD';
                                } else {
                                    $delivery_request_payment->notes = '';
                                }

                                $delivery_request_payment->save();

                            }*/
                        } else if ($status == 3 && $oldStatus == 2) {
                            DeliveryRequest::where($searchcolumn, '=', $id)->where('company_id', '=', $company_id)->where('d_city', '=', $city)->update(array(
                                'status' => $status));
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $waybill;
                            $order_history->order_number = $item->order_number;
                            $order_history->company_id = $item->company_id;
                            $order_history->status = $status;
                            $order_history->admin_id = $adminid;
                            $order_history->save();
                        } else {
                            $response_array = array('success' => false, 'error' => 'Invalid status update.','error_code'=>603);
                            $response = Response::json($response_array, 200);
                            return $response;
                        }
                        $shipmentDetails = ['id' => $item->id,
                            'Waybill' => $item->jollychic,
                            'company_name'=> $company->username,
                            'order_number' => $item->order_number,
                            'receiver_name' => $item->receiver_name,
                            'receiver_phone' => $item->receiver_phone,
                            'receiver_phone2' => $item->receiver_phone2,
                            'receiver_state' => $item->d_state,
                            'receiver_city' => $item->d_city,
                            'receiver_zipcode' => $item->d_zipcode,
                            'receiver_district' => $item->d_district,
                            'receiver_address' => $item->d_address,
                            'receiver_address2' => $item->d_address2,
                            'receiver_address3' => $item->d_address3,
                            'receiver_lat' => $item->D_latitude,
                            'receiver_lng' => $item->D_longitude,
                            //'shipment_id' => $item->shipment_id,
                            'sender_name' => $item->sender_name,
                            'sender_phone' => $item->sender_phone,
                            'cash_on_delivery' => $item->cash_on_delivery,
                            //'scheduled_shipment_date' => $item->scheduled_shipment_date,
                            'created_at'=> date('Y-m-d H:i:s',strtotime($item->created_at)),
                            'updated_at'=> date('Y-m-d H:i:s',strtotime($item->updated_at)),
                            'new_shipment_date'=>date('Y-m-d H:i:s',strtotime($item->new_shipment_date)),
                            'status' => $status];
                        $response_array = array('success' => true, 'message' => 'Status updated successfully for Waybill :' . $waybill,'shipmentDetails'=> $shipmentDetails);
                        $response = Response::json($response_array, 200);
                        return $response;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid status update','error_code'=>603);
                        $response = Response::json($response_array, 200);
                        return $response;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Wrong Waybill/Order Number, Please try again','error_code'=>602);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Sorry, you are not authorized to perform the following action. Please contact admin.',error_code=>601);
                $response = Response::json($response_array, 200);
                return $response;
            }
        } catch (Exception $e) {
            Log::info($e);
            $response_array = array('success' => false, 'error' => 'Sorry, Failed due to an error. Contact System Admin immediately.','error_code'=>600);
            $response = Response::json($response_array, 200);
            return $response;
        }

    }


    /*
    returns JSON object encapsulates jollychic item information,
    as the purpose of using this method/route is general.
    I have assumed it will will be integrated with android app.
    return -2 in case item id not found or item id form is wrong, it must be of the form "jcxxxxxxxx" where x is an integer.
    */
    public function get_item()
    {


        try {
            $jollychicid = Request::segment(3);
            //make sure the item exist first, otherwise return error msg -1.

              $item = DeliveryRequest::select('jollychic','receiver_name','receiver_name','receiver_phone','cash_on_delivery','D_latitude','D_longitude','d_address','d_district','status')->where('jollychic', '=', $jollychicid)->first();
            if (count($item)) {

                //$item = DeliveryRequest::where('jollychic', '=', $jollychicid)->first();

                $iteminformation = array("created_at" => $item->created_at,
                    "jollychicid" => $item->jollychic,
                    "customer_name" => $item->receiver_name,
                    "customer_mobile" => $item->receiver_phone,
                    "customer_mobile2" => $item->receiver_phone,
                    "cash_on_delivery" => $item->cash_on_delivery,
                    "dropoff_latitude" => $item->D_latitude,
                    "dropoff_longitude" => $item->D_longitude,
                    "dropoff_address" => $item->d_address,
                    "dropoff_district" => $item->d_district,
                    "status" => $item->status
                );

                return json_encode($iteminformation);

            } else {
                return -2;
            }

        } catch (Exception $e) {
            'error:' . ' ' . $e->getMessage();
        }

    }


    public function getitempincode()
    {
        try {
            $jollychicid = Request::segment(3);

            $item = DeliveryRequest::select('pincode')->where('jollychic', '=', $jollychicid)->first();

            if (count($item) != 0) {
                return $item->pincode;
            } else {
                return -1;
            }


        } catch (Exception $e) {
            return $e->getMessage();
        }

    }


    public function isvalidpincode()
    {

        try {
            $jollychicid = Request::segment(3);
            $pincode = Request::segment(4);

            $item = DeliveryRequest::select('pincode')->where('jollychic', '=', $jollychicid)->first();

            if (count($item) != 0) {
                if ($item->pincode == $pincode) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    public function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPGET, 1);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($curl, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 2);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);


        if (curl_errno($curl)) {
            return 'Curl error: ' . curl_error($curl);
        }

        curl_close($curl);

        return $result;
    }


    public function login()
    {
        $device_token = Input::get('device_token');
        $device_type = Input::get('device_type');

        if (Input::has('email') && Input::has('password')) {
            $username = Input::get('email');
            $password = Input::get('password');
            $validator = Validator::make(
                array(
                    'password' => $password,
                    'email' => $username,
                    'device_token' => $device_token,
                    'device_type' => $device_type
                ), array(
                    'password' => 'required',
                    'email' => 'required',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios'
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                Log::error('Validation error during manual login for user = ' . print_r($error_messages, true));
            } else {
                if ($user = Admin::select('username','remember_token','password','role','id','email','bb_token')->where(function ($q) use ($username) {
                    $q->where('username', '=', $username)->orwhere('email', '=', $username);
                })->first()) {
                    if (Hash::check($password, $user->password)) {

                        $user->device_token = $device_token;
                        $user->device_type = $device_type;
                        $user->remember_token = generate_token();
                        // $user->token_expiry = generate_expiry();
                        $user->save();

                        // get user's role
                        $role = Role::select('title')->where('id', '=', $user->role)->first();
						$user_name = explode("@", $user->username);
						$username = $user_name[0];

                        if(empty($user->bb_token)){

                            $bb_token = '';
                        }else{

                            $bb_token = $user->bb_token;
                        }

                        $response_array = array(
                            'success' => true,
                            'id' => $user->id,
                            'username' => $username,
                            // 'phone' => $user->phone,
                            'role_id' => $user->role,
                            'role' => $role->title,
                            'email' => !empty($user->email)?$user->email:'',
                            'bb_token' => $bb_token,
                            // 'picture' => $user->picture,
                            // 'login_by' => $user->login_by,
                            // 'social_unique_id' => $user->social_unique_id,
                            // 'device_token' => $user->device_token,
                            // 'device_type' => $user->device_type,
                            'token' => $user->remember_token
                        );
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid Username and Password', 'error_code' => 403);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a Registered User', 'error_code' => 404);
                    $response_code = 200;
                }
            }
        } else {
            $response_array = array('success' => false, 'error' => 'Invalid input', 'error_code' => 404);
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }
	
    public function getRoles()
    {
        $token = Input::get('token');
        $id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'id' => $id
            ), array(
                'token' => 'required',
                'id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            if ($user = User::select('id')->where('token', '=', $token)
                ->where('id', '=', $id)
                ->first()) {
                // valid user
                $roles = Role::all();
                $response_array = array(
                    'success' => true,
                    'roles' => $roles
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function uploadSignature()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $jollychicid = Input::get('itemid');
        $picture = Input::file('picture');
        $longitude = Input::get('longitude');
        $latitude = Input::get('latitude');
        $validator = Validator::make(
            array(
                'token' => $token,
                'id' => $walker_id,
                'itemid' => $jollychicid,
                'picture' => $picture
            ), array(
                'token' => 'required',
                'id' => 'required|integer',
                'itemid' => 'required',
                'picture' => 'required'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            if ($walker_data = $this->getWalkerData($walker_id, $token, false)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {

                    $item = DeliveryRequest::select('signature_url','client_latitude','client_longitude')->where('jollychic', '=', $jollychicid)->first();

                    // upload image
                    $file_name = time();
                    $file_name .= rand();
                    $file_name = sha1($file_name);

                    $s3_url = "";
                    if (Input::hasfile('picture')) {
                        $ext = Input::file('picture')->getClientOriginalExtension();
                        Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                        $local_url = $file_name . "." . $ext;

                        // Upload to S3
//                         if (Config::get('app.s3_bucket') != "") {
//                             $s3 = App::make('aws')->get('s3');
//                             $pic = $s3->putObject(array(
//                                 'Bucket' => Config::get('app.s3_bucket'),
//                                 'Key' => $file_name,
//                                 'SourceFile' => public_path() . "/uploads/" . $local_url,
//                             ));
//
//                             $s3->putObjectAcl(array(
//                                 'Bucket' => Config::get('app.s3_bucket'),
//                                 'Key' => $file_name,
//                                 'ACL' => 'public-read'
//                             ));
//
//                             $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
//                         } else {
//                             $s3_url = asset_url() . '/uploads/' . $local_url;
//                         }
                        $s3_url = '/uploads/' . $local_url;

                        $item->signature_url = $s3_url;
                        if (isset($latitude) && isset($longitude)) {
                            $item->client_latitude = $latitude;
                            $item->client_longitude = $longitude;
                        }
                        $item->save();
                        $response_array = array(
                            'success' => true,
                            'message' => 'image uploaded successfully.'
                        );
                    } else {
                        $response_array = array('success' => false, 'error' => 'File not found', 'error_code' => 404);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                $response_code = 200;
            }
            $response_code = 200;
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function uploadSignaturev2()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $jollychicid = Input::get('itemid');
        $picture = Input::get('picture');
        $longitude = Input::get('longitude');
        $latitude = Input::get('latitude');
        $validator = Validator::make(
            array(
                'token' => $token,
                'id' => $walker_id,
                'itemid' => $jollychicid,
                'picture' => $picture
            ), array(
                'token' => 'required',
                'id' => 'required|integer',
                'itemid' => 'required',
                'picture' => 'required'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            if ($walker_data = $this->getWalkerData($walker_id, $token, false)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {

                    $item = DeliveryRequest::select('signature_url','client_latitude','client_longitude')->where('jollychic', '=', $jollychicid)->first();

                    if (isset($picture)) {
                        $item->signature_url = trim($picture);
                        if (isset($latitude) && isset($longitude)) {
                            $item->client_latitude = $latitude;
                            $item->client_longitude = $longitude;
                        }
                        $item->save();
                        $response_array = array(
                            'success' => true,
                            'message' => 'image uploaded successfully.'
                        );
                    } else {
                        $response_array = array('success' => false, 'error' => 'Image URL not found', 'error_code' => 404);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
            $response_code = 200;
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function getWalkerData($walker_id, $token, $is_admin)
    {

        if ($walker_data = Walker::select('token_expiry')->where('token', '=', $token)->where('id', '=', $walker_id)->first()) {
            return $walker_data;
        } elseif ($is_admin) {
            $walker_data = Walker::select('token_expiry')->where('id', '=', $walker_id)->first();
            if (!$walker_data) {
                return false;
            }
            return $walker_data;
        } else {
            return false;
        }
    }

    //API call to get item details need item ID only that is of jollychic ID.
    public function getitemDetails()
    {
        try {
            $jollychicid = Request::segment(3);
            $item = DeliveryRequest::leftJoin('companies', 'delivery_request.company_id', '=', 'companies.id')->where('jollychic', '=', $jollychicid)->select(['delivery_request.company_id','sender_name','company_name','order_number','cash_on_pickup','pickup_city','pickup_district','pickup_address','pickup_latitude','pickup_longitude','sender_phone','sender_email','sender_city','cash_on_delivery','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_latitude','D_longitude','d_weight','d_quantity','d_description','scheduled_shipment_date','timeslot','urgent_delivery','is_address_updated','status',
                'companies.company_name as company_name', 'companies.company_type'])->first();
            if (count($item)) {
                if ($item) {
                    if($item->company_id == '923'){

                      $company_name = !empty($item->sender_name)?$item->sender_name:'';

                    }else{

                        $company_name = !empty($item->sender_name)?$item->company_name.' ['.$item->sender_name.']':$item->company_name;
                    }
                    $response_array = array(

                        'success' => true,
                        'order_number' => $item->order_number,
                        'cash_on_pickup' => $item->cash_on_pickup,
                        'pickup_city' => $item->pickup_city,
                        'pickup_district' => $item->pickup_district,
                        'pickup_address' => $item->pickup_address,
                        'pickup_latitude' => $item->pickup_latitude,
                        'pickup_longitude' => $item->pickup_longitude,
                        'sender_name' => $item->sender_name,
                        'sender_phone' => $item->sender_phone,
                        'sender_email' => $item->sender_email,
                        'sender_city' => $item->sender_city,
                        'cash_on_delivery' => $item->cash_on_delivery,
                        'receiver_name' => $item->receiver_name,
                        'receiver_phone' => $item->receiver_phone,
                        'receiver_phone2' => $item->receiver_phone2,
                        'receiver_state' => $item->d_state,
                        'receiver_city' => $item->d_city,
                        'receiver_zipcode' => $item->d_zipcode,
                        'receiver_district' => $item->d_district,
                        'receiver_address' => $item->d_address,
                        'receiver_address2' => isset($item->d_address2) ? $item->d_address2 : '',
                        'receiver_address3' => isset($item->d_address3) ? $item->d_address3 : '',
                        'receiver_lat' => $item->D_latitude,
                        'receiver_lng' => $item->D_longitude,
                        'company_id' => $item->company_id,
                        'company_name' => $company_name,
                        'weight' => $item->d_weight,
                        'quantity' => $item->d_quantity,
                        'shipment_description' => $item->d_description,
                        'scheduled_shipment_date' => isset($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
                        'type' => $item->company_type,
                        'urgent_delivery' => $item->urgent_delivery,
                        'timeslot' => isset($item->timeslot) ? $item->timeslot : '',
                        'is_address_updated' => $item->is_address_updated,
                        'status' => $item->status
                    );
                } else {
                    $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }


    //API call to get items for planned captain.
    public function getPlannedDeliveryRequests()
    {
        try {
            $walker_id = Input::get('captain_id');
            Log::info('$walker_id' . $walker_id);
            $walker = Walker::find($walker_id);
            $d_reqs = DeliveryRequest::select('delivery_request.id','mission_id','mission_name','mission.suggested_pickup_time','jollychic','order_number','sender_name','company_name','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_latitude','D_longitude','shipment_id','sender_name','sender_phone','cash_on_delivery','delivery_request.status','urgent_delivery')->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
               ->leftJoin('mission', 'mission.id', '=', 'delivery_request.mission_id')->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', 0)->where('delivery_request.status', '<', 4)->get();
			Log::info('count' . count($d_reqs));
            if (count($d_reqs)) {
                $items = array();
                foreach ($d_reqs as $item) {
                    array_push($items, array(
                        'id' => $item->id,
						'mission_id' => $item->mission_id,
						'mission_name' => !empty($item->mission_name)?$item->mission_name:'',
                        'suggested_pickup_time' =>  !empty($item->suggested_pickup_time)?$item->suggested_pickup_time:'',
                        'jollychic_id' => $item->jollychic,
                        'order_number' => $item->order_number,
                        'company_name' => !empty($item->sender_name)?$item->company_name.' ['.$item->sender_name.']':$item->company_name,
                        'receiver_name' => $item->receiver_name,
                        'receiver_phone' => $item->receiver_phone,
                        'receiver_phone2' => $item->receiver_phone2,
                        'receiver_state' => $item->d_state,
                        'receiver_city' => $item->d_city,
                        'receiver_zipcode' => $item->d_zipcode,
                        'receiver_district' => $item->d_district,
                        'receiver_address' => $item->d_address,
                        'receiver_address2' => $item->d_address2,
                        'receiver_address3' => $item->d_address3,
                        'receiver_lat' => $item->D_latitude,
                        'receiver_lng' => $item->D_longitude,
                        'shipment_id' => $item->shipment_id,
                        'sender_name' => $item->sender_name,
                        'sender_phone' => $item->sender_phone,
                        'cash_on_delivery' => $item->cash_on_delivery,
                        'status' => $item->status,
                        'urgent_delivery' => $item->urgent_delivery
                    ));
                }
                $response_array = array(
                    'success' => true,
                    'suggested_pickup_time' => is_null($walker->suggested_pickup_time)?'':$walker->suggested_pickup_time,
                    'cancel_expiry_time' => is_null($walker->cancel_planned_shipments_expiry_timestamp)?'':$walker->cancel_planned_shipments_expiry_timestamp,
                    'delivery_requests' => $items
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    //API call to get items with captain.
    public function getScannedDeliveryRequests()
    {
        try {
            $walker_id = Input::get('captain_id');
            $walker = Walker::find($walker_id);
            Log::info('$walker_id' . $walker_id);
            $d_reqs = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->
            whereRaw("(planned_walker = $walker_id and confirmed_walker = $walker_id and status = 4) or (pickup_walker = $walker_id and status = -1)")
                ->orderBy('d_district', 'asc')->get(['delivery_request.id','jollychic','order_number','cash_on_pickup','pickup_city','pickup_district','pickup_district','pickup_address','pickup_latitude','pickup_longitude','sender_name','sender_phone','sender_email','sender_city','cash_on_delivery','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_latitude','D_longitude','d_weight','d_quantity','d_description','scheduled_shipment_date','company_type','urgent_delivery','timeslot','status','companies.company_name', 'companies.company_type']);
            Log::info('count' . count($d_reqs));
            if (count($d_reqs)) {
                $items = array();
                foreach ($d_reqs as $item) {
                    array_push($items, array(
                        'id' => $item->id,
                        'jollychic_id' => $item->jollychic,
                        'order_number' => isset($item->order_number) ? $item->order_number : '',
                        'cash_on_pickup' => $item->cash_on_pickup,
                        'pickup_city' => $item->pickup_city,
                        'pickup_district' => $item->pickup_district,
                        'pickup_address' => $item->pickup_address,
                        'pickup_latitude' => $item->pickup_latitude,
                        'pickup_longitude' => $item->pickup_longitude,
                        'sender_name' => $item->sender_name,
                        'sender_phone' => $item->sender_phone,
                        'sender_email' => $item->sender_email,
                        'sender_city' => $item->sender_city,
                        'cash_on_delivery' => $item->cash_on_delivery,
                        'receiver_name' => $item->receiver_name,
                        'receiver_phone' => $item->receiver_phone,
                        'receiver_phone2' => $item->receiver_phone2,
                        'receiver_state' => $item->d_state,
                        'receiver_city' => $item->d_city,
                        'receiver_zipcode' => $item->d_zipcode,
                        'receiver_district' => $item->d_district,
                        'receiver_address' => $item->d_address,
                        'receiver_address2' => isset($item->d_address2) ? $item->d_address2 : '',
                        'receiver_address3' => isset($item->d_address3) ? $item->d_address3 : '',
                        'receiver_lat' => $item->D_latitude,
                        'receiver_lng' => $item->D_longitude,
                        'company_name' => !empty($item->sender_name)?$item->company_name.' ['.$item->sender_name.']':$item->company_name,
                        'weight' => $item->d_weight,
                        'quantity' => $item->d_quantity,
                        'shipment_description' => $item->d_description,
                        'scheduled_shipment_date' => isset($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
                        'type' => $item->company_type,
                        'urgent_delivery' => $item->urgent_delivery,
                        'timeslot' => isset($item->timeslot) ? $item->timeslot : '',
                        'status' => $item->status
                    ));
                }
                $response_array = array(
                    'success' => true,
                    'suggested_pickup_time' => $walker->suggested_pickup_time,
                    'delivery_requests' => $items
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    //API call to get items against confirmed captain.
    public function getConfirmedDeliveryRequests()
    {
        try {
            $walker_id = Input::get('captain_id');
            Log::info('$walker_id' . $walker_id);
            $d_reqs = DeliveryRequest::select('id','jollychic','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_latitude','D_longitude','sender_name','sender_phone','cash_on_delivery','status')->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', $walker_id)->whereIn('status', array(4, 5, 6))->get();
            Log::info('count' . count($d_reqs));
            if (count($d_reqs)) {
                $items = array();
                foreach ($d_reqs as $item) {
                    array_push($items, array(
                        'id' => $item->id,
                        'jollychic_id' => $item->jollychic,
                        'receiver_name' => $item->receiver_name,
                        'receiver_phone' => $item->receiver_phone,
                        'receiver_phone2' => $item->receiver_phone2,
                        'receiver_state' => $item->d_state,
                        'receiver_city' => $item->d_city,
                        'receiver_zipcode' => $item->d_zipcode,
                        'receiver_district' => $item->d_district,
                        'receiver_address' => $item->d_address,
                        'receiver_address2' => $item->d_address2,
                        'receiver_address3' => $item->d_address3,
                        'receiver_lat' => $item->D_latitude,
                        'receiver_lng' => $item->D_longitude,

                        'sender_name' => $item->sender_name,
                        'sender_phone' => $item->sender_phone,

                        'cash_on_delivery' => $item->cash_on_delivery,

                        'status' => $item->status
                    ));
                }
                $response_array = array(
                    'success' => true,
                    'delivery_requests' => $items
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }


    public function getUndeliveredReasons()
    {
        $reasons = UndeliveredReason::whereNotIn('id', [0, 3, 6, 9]);
        $waybill = Request::get('waybill');
        if(isset($waybill)) {
            if(($shipment = DeliveryRequest::select('company_id')->where('jollychic', '=', $waybill)->first()) && $shipment->company_id != 917) {
                $reasons = $reasons->whereNotIn('id', [4, 5]);
            }
        }
        $reasons = $reasons->get();
        if (count($reasons)) {
            $reasonsArr = array();
            foreach ($reasons as $reason) {
                array_push($reasonsArr, array(
                    'id' => $reason->id,
                    'arabic' => $reason->arabic,
                    'english' => $reason->english
                ));
            }
            $response_array = array(
                'success' => true,
                'reasons' => $reasonsArr
            );
        } else {
            $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function get_app_version()
    {
        $ios_customer_app = Settings::select('value')->where("key", '=', 'ios_app_version_scanner')->first();
        $android_customer_app = Settings::select('value')->where("key", '=', 'android_app_version_scanner')->first();
        $processing_fee = Settings::select('value')->where("key", '=', 'processing_fee_percent')->first();
        $is_paymeant_enabled = Settings::select('value')->where("key", '=', 'is_payfort_enabled')->first();
        $response_array = array('success' => true, 'android' => $android_customer_app->value, 'ios' => $ios_customer_app->value, 'is_paymeant_enabled' => $is_paymeant_enabled->value, 'processing_fee' => $processing_fee->value);
        $response_code = 200;

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function getcaptaindeliverystatus()
    {
        try {
            $walker_id = Input::get('captain_id');
            Log::info('$walker_id' . $walker_id);
            $walker = Walker::find($walker_id);
            if (isset($walker)) {
                $plannedItems = DeliveryRequest::select('id')->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', 0)
                    ->where('status', '<', 4)->count();
                $scannedItems = DeliveryRequest::select('id')->whereRaw("(confirmed_walker = $walker_id and status = 4) or (pickup_walker = $walker_id and status = -1)")->count();

                $deliveredInfoQuery = 'select sum(cash_on_delivery) DeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $walker_id . ' and status = 5 and is_money_received = 0 ';
                $deliveredInfo = DB::select(DB::raw($deliveredInfoQuery));
                $undeliveredInfoQuery = 'select sum(cash_on_delivery) UndeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $walker_id . ' and status = 6';
                $undeliveredInfo = DB::select(DB::raw($undeliveredInfoQuery));
                $balanceInfoQuery = 'select sum(cah.captain_amount) Balance from captain_account_history cah where cah.captain_id = ' . $walker_id . ' and cah.paid_to_captain = 0 ';
                $balanceInfo = DB::select(DB::raw($balanceInfoQuery));
                $debtInfoQuery = 'select sum(cash_on_delivery) DebtAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $walker_id . ' and status = 5 and is_money_received = 1 and is_amount_covered = 1';
                $debtInfo = DB::select(DB::raw($debtInfoQuery));
                $new = DeliveryRequest::select('id')->where('status', '=', '-3')->count();
                $reserved = DeliveryRequest::select('id')->where('status', '=', '-2')->where('pickup_walker', '=', $walker_id)->count();
                $processing_fee_percent = Settings::select('value')->where("key", '=', 'processing_fee_percent')->first();
                $processing_fee = $processing_fee_percent->value;
                $balance_amount=isset($balanceInfo[0]->Balance) ? $balanceInfo[0]->Balance : 0;
                $processing_fee_percentage=($balance_amount/100)*$processing_fee;
                $unreadNotifications = Notification::select('id')->where('captain_id', '=', $walker_id)->where('is_read', '=', 0)->count();
                if($walker->city=='Makkah'){
                    $delivered_items = 0;
                    $undelivered_items = 0;
                } else{
                    $delivered_items = isset($deliveredInfo[0]->Pieces) ? $deliveredInfo[0]->Pieces : 0;
                    $undelivered_items = isset($undeliveredInfo[0]->Pieces) ? $undeliveredInfo[0]->Pieces : 0;
                }

                $response_array = array(
                    'success' => true,
                    'deliveredAmount' => isset($deliveredInfo[0]->DeliveredAmount) ? round($deliveredInfo[0]->DeliveredAmount, 2) : 0,
                    //'deliveredPieces' => isset($deliveredInfo[0]->Pieces) ? $deliveredInfo[0]->Pieces : 0,
                    'deliveredPieces' => $delivered_items,
                    'undeliveredAmount' => isset($undeliveredInfo[0]->UndeliveredAmount) ? round($undeliveredInfo[0]->UndeliveredAmount, 2) : 0,
                    //'undeliveredPieces' => isset($undeliveredInfo[0]->Pieces) ? $undeliveredInfo[0]->Pieces : 0,
                    'undeliveredPieces' => $undelivered_items,
                    'balanceAmount' => isset($balanceInfo[0]->Balance) ? $balanceInfo[0]->Balance : 0,
                    'debt_amount' => isset($debtInfo[0]->DebtAmount) ? round($debtInfo[0]->DebtAmount, 2) : 0,
                    'debt_pieces' => isset($debtInfo[0]->Pieces) ? $debtInfo[0]->Pieces : 0,
                    'plannedPieces' => $plannedItems,
                    'scannedPieces' => $scannedItems,
                    'pickupPieces' => $new,
                    'reservedPieces' => $reserved,
                    'captain_id' => $walker_id,
                    'phone' => $walker->phone,
                    'rank_value' => 2,
                    'rank_text' => 'Gold',
                    'rating' => $walker->rate,
                    'name' => $walker->first_name . ' ' . $walker->last_name,
                    'processing_fee' => $processing_fee_percentage,
                    'payable_amount' => $balance_amount + $processing_fee_percentage,
                    'notification_count' => isset($unreadNotifications) ? $unreadNotifications : 0
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Captain not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }
	
	public function getcaptaindeliverystatusV2()
    {
        try {
            $walker_id = Input::get('captain_id');
            Log::info('$walker_id' . $walker_id);
            $walker = Walker::select('first_name','last_name','rate','phone','unread_notifications')->where('id','=',$walker_id)->first();
            if (isset($walker)) {
                $balanceInfoQuery = 'select sum(cah.captain_amount) Balance from captain_account_history cah where cah.captain_id = ' . $walker_id . ' and cah.paid_to_captain = 0 ';
                $balanceInfo = DB::select(DB::raw($balanceInfoQuery));
                
                $processing_fee_percent = Settings::select('value')->where("key", '=', 'processing_fee_percent')->first();
                $processing_fee = $processing_fee_percent->value;
                $balance_amount=isset($balanceInfo[0]->Balance) ? $balanceInfo[0]->Balance : 0;
                $processing_fee_percentage=($balance_amount/100)*$processing_fee;
                $deliveryRequestQuery = 'SELECT
    sum(case when confirmed_walker =  ' . $walker_id . ' and status = 5 and is_money_received = 1 and is_amount_covered = 1  then cash_on_delivery else 0 end) DebtAmount,
    COUNT(CASE WHEN confirmed_walker =  ' . $walker_id . ' and status = 5 and is_money_received = 1 and is_amount_covered = 1 THEN 1 END) AS DebtPieces,
	sum(case when confirmed_walker =  ' . $walker_id . ' and status = 5 and is_money_received = 0   then cash_on_delivery else 0 end) DeliveredAmount,
    COUNT(CASE WHEN confirmed_walker =  ' . $walker_id . ' and status = 5 and is_money_received = 0 THEN 1 END) AS DeliveredPieces,
	sum(case when confirmed_walker =  ' . $walker_id . ' and status = 6   then cash_on_delivery else 0 end) UndeliveredAmount,
    COUNT(CASE WHEN confirmed_walker =  ' . $walker_id . ' and status = 6 THEN 1 END) AS UndeliveredPieces,
    COUNT(CASE WHEN planned_walker = ' . $walker_id . ' and confirmed_walker = 0 and status < 4 THEN 1 END) AS plannedItems,
    COUNT(CASE WHEN confirmed_walker = ' . $walker_id . ' AND status=4 or pickup_walker = ' . $walker_id . ' AND status=-1 THEN 1 END) AS scannedItems,
    COUNT(CASE WHEN status = -3 THEN 1 END) AS new,
    COUNT(CASE WHEN pickup_walker = ' . $walker_id . ' and status = -2 THEN 1 END) AS reserved
    FROM `delivery_request`';
                $deliveryRequestInfo = DB::select(DB::raw($deliveryRequestQuery));
                $DeliveredAmount = isset($deliveryRequestInfo[0]->DeliveredAmount) ? round($deliveryRequestInfo[0]->DeliveredAmount, 2) : 0;
                $DeliveredPieces = isset($deliveryRequestInfo[0]->DeliveredPieces) ? $deliveryRequestInfo[0]->DeliveredPieces : 0;
                $undeliveredAmount = isset($deliveryRequestInfo[0]->UndeliveredAmount) ? round($deliveryRequestInfo[0]->UndeliveredAmount, 2) : 0;
                $UndeliveredPieces = isset($deliveryRequestInfo[0]->UndeliveredPieces) ? $deliveryRequestInfo[0]->UndeliveredPieces : 0;
                $DebtAmount = isset($deliveryRequestInfo[0]->DebtAmount) ? round($deliveryRequestInfo[0]->DebtAmount, 2) : 0;
                $DebtPieces = isset($deliveryRequestInfo[0]->DebtPieces) ? $deliveryRequestInfo[0]->DebtPieces : 0;
                $plannedItems = isset($deliveryRequestInfo[0]->plannedItems) ? $deliveryRequestInfo[0]->plannedItems : 0;
                $new = isset($deliveryRequestInfo[0]->new) ? $deliveryRequestInfo[0]->new : 0;
                $scannedItems = isset($deliveryRequestInfo[0]->scannedItems) ? $deliveryRequestInfo[0]->scannedItems : 0;
                $reserved = isset($deliveryRequestInfo[0]->reserved) ? $deliveryRequestInfo[0]->reserved : 0;

                $response_array = array(
                    'success' => true,
                    'deliveredAmount' => $DeliveredAmount,
                    'deliveredPieces' => $DeliveredPieces,
                    'undeliveredAmount' => $undeliveredAmount,
                    'undeliveredPieces' => $UndeliveredPieces,
                    'balanceAmount' => isset($balanceInfo[0]->Balance) ? $balanceInfo[0]->Balance : 0,
                    'debt_amount' => $DebtAmount,
                    'debt_pieces' => $DebtPieces,
                    'plannedPieces' => $plannedItems,
                    'scannedPieces' => $scannedItems,
                    'pickupPieces' => $new,
                    'reservedPieces' => $reserved,
                    'captain_id' => $walker_id,
                    'phone' => $walker->phone,
                    'rank_value' => 2,
                    'rank_text' => 'Gold',
                    'rating' => $walker->rate,
                    'name' => $walker->first_name . ' ' . $walker->last_name,
                    'processing_fee' => $processing_fee_percentage,
                    'payable_amount' => $balance_amount + $processing_fee_percentage,
                    'notification_count' => $walker->unread_notifications
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Captain not found', 'error_code' => 410);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    function sendpincode()
    {
        //DB::select('SELECT * FROM delivery_request WHERE scheduled_shipment_date = \'2018-05-03\' and status = 4');
        $items = DeliveryRequest::select('company_id','sender_name','confirmed_walker','pincode','cash_on_delivery','receiver_phone','receiver_phone2')->where('scheduled_shipment_date', '=', '2018-05-03')->where('status', '=', '4')->get();
        foreach ($items as $item) {

            try {
                $sms = "عزيزي عميل ";
                $company = Companies::find($item->company_id);
                if (isset($company)) {
                    $sms .=!empty($item->sender_name)?" " . $company->name_ar .' ['.$item->sender_name.']':" " . $company->name_ar . " ";
                }

                $sms .= " طلبكم سيصلكم اليوم مع المندوب ";
                //get order:

                //get walker:

                if ($item->confirmed_walker > 0) {
                    $walker = Walker::where('id', '=', $item->confirmed_walker)->first();

                    if (count($walker)) {
                        $sms .= $walker->first_name;
                        $sms .= " 0";
                        $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                    }
                }


                $sms .= " رمز التسليم ";
                $sms .= $item->pincode;

                if ($item->cash_on_delivery) {
                    $sms .= " ومبلغ الطلب ";
                    $sms .= $item->cash_on_delivery;
                    $sms .= " ريال";
                }
                $sms .= " مع تحيات ساعي";


                $destinations1 = "966";
                $destinations1 .= substr(str_replace(' ', '', $item->receiver_phone), -9);

                if (isset($company) && $company->id != '952' || !isset($company)) {

                    $curl = new Curl();
                    //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                    $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

                    $urlDiv = explode("?", $url);
                    //$result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                    //Log::info("SMSSERVICELOG" . $result);


                    if ($item->receiver_phone2 != '') {
                        $destinations2 = "966";
                        $destinations2 .= substr(str_replace(' ', '', $item->receiver_phone2), -9);
                        $curl = new Curl();
                        //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                        $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

                        $urlDiv = explode("?", $url);
                        //$result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                        //Log::info("SMSSERVICELOG" . $result);
                    }

                }
                
            } catch (Exception $e) {
                'error:' . ' ' . $e->getMessage();
            }
        }
        return true;
    }

    public function getCompanies()
    {
        $companies = Companies::select(['id', 'company_name as name', 'name_ar'])->get();
        $response_array = array('success' => true, 'companies' => $companies);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getCitiesbAdmin()
    {
        $admin_id = Input::get('admin_id');
        $adminCities = CityPrivilege::select(['city.name as name','city.name_ar as name_ar'])->leftJoin('city','city_privilege.city' ,'=','city.name')->where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        $response_array = array('success' => true, 'adminCities' => $adminCities);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getscanneditemsadmin()
    {
        try {
            $admin_id = Input::get('admin_id');
            $status = Input::get('status');
            $city = Input::get('city');
            $company_id = Input::get('company_id');
            $date = date("Y-m-d");
            Log::info('admin_id = ' . $admin_id);
            $scannedItemsadminquery = OrdersHistory::select('delivery_request.id','jollychic','delivery_request.order_number','username','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_latitude','D_longitude','sender_name','sender_phone','cash_on_delivery','orders_history.created_at','orders_history.updated_at','new_shipment_date','orders_history.status')->leftJoin('delivery_request', 'orders_history.waybill_number', '=', 'delivery_request.jollychic')
                ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                ->where('admin_id', '=', $admin_id)
                ->where('orders_history.status', '=', $status)
                ->whereDate('orders_history.created_at', '=', $date);
            if (isset($company_id) && $company_id != 'All') {
                $scannedItemsadminquery = $scannedItemsadminquery->where('delivery_request.company_id', '=', $company_id);
            }
            if (isset($city) && $city != 'All') {
                $scannedItemsadminquery = $scannedItemsadminquery->where('d_city', '=', $city);
            }
            $scannedItemsadmin = $scannedItemsadminquery->get();
            Log::info('count = ' . count($scannedItemsadmin));
            if (count($scannedItemsadmin)) {
                $items = array();
                foreach ($scannedItemsadmin as $item) {
                    array_push($items, array(
                        'id' => $item->id,
                        'Waybill' => $item->jollychic,
                        'order_number' => $item->order_number,
                        'company_name'=> $item->username,
                        'receiver_name' => $item->receiver_name,
                        'receiver_phone' => $item->receiver_phone,
                        'receiver_phone2' => $item->receiver_phone2,
                        'receiver_state' => $item->d_state,
                        'receiver_city' => $item->d_city,
                        'receiver_zipcode' => $item->d_zipcode,
                        'receiver_district' => $item->d_district,
                        'receiver_address' => $item->d_address,
                        'receiver_address2' => $item->d_address2,
                        'receiver_address3' => $item->d_address3,
                        'receiver_lat' => $item->D_latitude,
                        'receiver_lng' => $item->D_longitude,
                       // 'shipment_id' => $item->shipment_id,
                        'sender_name' => $item->sender_name,
                        'sender_phone' => $item->sender_phone,
                        'cash_on_delivery' => $item->cash_on_delivery,
                       // 'scheduled_shipment_date' => $item->scheduled_shipment_date,
                        'created_at'=> date('Y-m-d H:i:s',strtotime($item->created_at)),
                        'updated_at'=> date('Y-m-d H:i:s',strtotime($item->updated_at)),
                        'new_shipment_date'=>date('Y-m-d H:i:s',strtotime($item->new_shipment_date)),
                        'status' => $item->status
                    ));
                }
                $response_array = array(
                    'success' => true,
                    'delivery_requests' => $items
                );
            } else {
                $response_array = array('success' => true, 'message' => 'No Scanned Item found');
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function cancelplanshipmentsbycaptain()
    {
        try {
            $walker_id = Input::get('captain_id');
            $reject = Input::get('reject');
            $walker = Walker::find($walker_id);
            if (isset($reject) && $reject == 1) {
                if (isset($walker)) {
                    $mission_ids = DeliveryRequest::select('mission_id')->where('mission_id','<>',0)->where('planned_walker', '=', $walker_id)
                        ->where('confirmed_walker', '=', 0)->where('status', '<', 4)->distinct()->get();
                    $count = DeliveryRequest::where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', 0)->where('status', '<', 4)->update(array('planned_walker' => 0));
                    if ($count>0) {
                        if(count($mission_ids)) {
                            $missionsids = array();
                            foreach ($mission_ids as $mission_id) {
                                array_push($missionsids, $mission_id->mission_id);
                            }
                            Mission::where('reserve_captain', '=', $walker_id)->where('planned_captain', '=', $walker_id)
                                ->wherein('id', $missionsids)
                                ->update(array('reserve_captain' => 0, 'planned_captain' => 0,'is_notify' => 0));
                        }
                        $walker->last_cancel_planned_shipments_timestamp= date('Y-m-d H:i:s');
                        $walker->suggested_pickup_time=0;
                        $walker->save();
                        $response_array = array('success' => true, 'message' => 'You have cancelled '.$count.' Shipments','cancelled_date' => date('Y-m-d H:i:s') );
                    } else {
                        $response_array = array('success' => false, 'error' => 'No item have been assign to you', 'error_code' => 410);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Captain not found with id ' . $walker_id, 'error_code' => 410);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Invalid Status', 'error_code' => 603);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }
	
    public function update_item()
    {
        try {

            if (Request::has('waybill')) {
                $waybill = Request::get('waybill');
                $item = DeliveryRequest::select('d_district','d_address','receiver_phone','receiver_phone2','status','scheduled_shipment_date','receiver_name','d_city')->where('jollychic', '=', $waybill)->whereIn('status', [0, 1, 2, 3, 4, 6])->first();
                if (!isset($item)) {
                    $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                } else {
                    $ticketaction = new TicketsActions;
                    $ticketaction->ticket_id = $waybill;
                    //  district float Optional
                    if (Request::has('district')) {
                        $district = Request::get('district');
                        $item->d_district = $district;
                        $ticketaction->shipment_district = $district;
                        $ticketaction->is_district = 1;
                    } else {
                        $ticketaction->shipment_district = $item->d_district;
                    }
                    //  address string Optional
                    if (Request::has('address')) {
                        $address = Request::get('address');
                        $item->d_address = $address;
                        $ticketaction->shipment_address = $address;
                        $ticketaction->is_address = 1;
                    } else {
                        $ticketaction->shipment_address = $item->d_address;
                    }
                    //  mobile ints Optional
                    if (Request::has('mobile')) {
                        $dropoff_mobile = Request::get('mobile');
                        $item->receiver_phone = $dropoff_mobile;
                        $ticketaction->shipment_mobile1 = $dropoff_mobile;
                        $ticketaction->is_mobile1 = 1;
                    } else {
                        $ticketaction->shipment_mobile1 = $item->receiver_phone;
                    }
                    // mobile2 ints Optional
                    if (Request::has('mobile2')) {
                        $dropoff_mobile2 = Request::get('mobile2');
                        $item->receiver_phone2 = $dropoff_mobile2;
                        $ticketaction->shipment_mobile2 = $dropoff_mobile2;
                        $ticketaction->is_mobile2 = 1;
                    } else {
                        $ticketaction->shipment_mobile2 = $item->receiver_phone2;
                    }
                    //   shipment_reschedule string Mandatory
                    if (Request::has('shipmentreschedule')) {
                        $reschedule = Request::get('shipmentreschedule');
                        if ($item->status == 3 || $item->status == 6) {
                            $item->scheduled_shipment_date = $reschedule;
                            $ticketaction->shipment_scheduled_date = $reschedule;
                            $ticketaction->is_scheduled_date = 1;
                        } else {
                            $response_array = array('success' => false, 'error' => 'Shipment can not be reschedule, please try again.', 'error_code' => 410);
                            $response = Response::json($response_array, 200);
                            return $response;
                        }
                    } else {
                        $ticketaction->shipment_scheduled_date = $item->scheduled_shipment_date;
                    }
                    $item->save();

                    $ticketaction->ticket_priority = 'High';
                    $ticketaction->admin = 'Test';
                    $ticketaction->shipment_name = $item->receiver_name;
                    $ticketaction->shipment_city = $item->d_city;
                    $ticketaction->shipment_is_cancelled = 'No';
                    $ticketaction->comment = 'Generated By Jollychic';
                    $ticketaction->status = 1;
                    if(!isset($ticketaction->shipment_scheduled_date))
                        $ticketaction->scheduled_shipment_date = '';
                    $ticketaction->save();

                    $ticket = Tickets::where('waybill', '=', $waybill)->count();
                    if($ticket == 0)
                        $ticket = new Tickets;
                    else 
                        $ticket = Tickets::where('waybill', '=', $waybill)->first();
                    $ticket->waybill = $waybill;
                    $ticket->priority = 'High';
                    $ticket->status = 1;
                    $ticket->created_for = 'Created By JollyChic';
                    $ticket->save();

                    $response_array = array('success' => true, 'message' => 'Changes updated successfully for Waybill :' . $waybill);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Field waybill is required, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

        } catch (Exception $e) {
            Log::info($e->getMessage());
            $response_array = array('success' => false, 'error' => 'Sorry, Failed due to an error.', 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
    }

    public function merchantregister() {

        try {

            $name = Input::get('name');
            $phone = Input::get('phone');
            $email = Input::get('email');
            $has_cr = Input::get('has_cr');
    
            $user = new Users;
            $user->name = $name;
            $user->phone = $phone;
            $user->email = $email;
            $user->has_cr = $has_cr;
            $user->save();

            $response_array = array('success' => true, 'id' => $user->id, 'message' => 'New merchant registered');
            
        } catch(Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage());
            
        }

        response:
        $response = Response::json($response_array, 200);
        return $response;
        
    }

    public function merchantregisterold() {

        try {

            $device_token = Input::get('device_token');
            if(!isset($device_token)) {
                $response_array = array('success' => false, 'error' => 'Device token required');
                goto response;
            } 
            $name = Input::get('name');
            if(!isset($name)) {
                $response_array = array('success' => false, 'error' => 'Name required');
                goto response;
            } 
            $name_ar = Input::get('name_ar');
            if(!isset($name_ar)) {
                $response_array = array('success' => false, 'error' => 'Arabic Name required');
                goto response;
            } 
            $email = Input::get('email');
            if(!isset($email)) {
                $response_array = array('success' => false, 'error' => 'Email required');
                goto response;
            } 
            $username = Input::get('username');
            if(!isset($username)) {
                $response_array = array('success' => false, 'error' => 'Username required');
                goto response;
            } 
            else {
                $check = Companies::select('id')->where('username', '=', $username)->count();
                if($check) {
                    $response_array = array('success' => false, 'error' => 'This username is used');
                    goto response;
                }
            }
            $password = Input::get('password');
            if(!isset($password)) {
                $response_array = array('success' => false, 'error' => 'Password required');
                goto response;
            } 
            $mobile = Input::get('mobile');
            if(!isset($mobile)) {
                $response_array = array('success' => false, 'error' => 'Mobile required');
                goto response;
            } 
            $city = Input::get('city');
            if(!isset($city)) {
                $response_array = array('success' => false, 'error' => 'City required');
                goto response;
            } 
            $district = Input::get('district');
            if(!isset($district)) {
                $response_array = array('success' => false, 'error' => 'District required');
                goto response;
            } 
            $address = Input::get('address');
            if(!isset($address)) {
                $response_array = array('success' => false, 'error' => 'Address required');
                goto response;
            } 
            $idnumber = Input::get('idnumber');
            if(!isset($idnumber)) {
                $response_array = array('success' => false, 'error' => 'Id Number required');
                goto response;
            } 
            $idphoto = Input::get('idphoto');
            if(!isset($idphoto)) {
                $response_array = array('success' => false, 'error' => 'Id Photo required');
                goto response;
            } 
                
            $telegram = Input::get('telegram');
            if(!isset($telegram))
                $telegram = '';
            $whatsapp = Input::get('whatsapp');
            if(!isset($whatsapp))
                $whatsapp = '';
            $instagram = Input::get('instagram');
            if(!isset($instagram))
                $instagram = '';
            $twitter = Input::get('twitter');
            if(!isset($twitter))
                $twitter = '';
            $googleplus = Input::get('googleplus');
            if(!isset($googleplus))
                $googleplus = '';
            $facebook = Input::get('facebook');
            if(!isset($facebook))
                $facebook = '';
            $snapchat = Input::get('snapchat');
            if(!isset($snapchat))
                $snapchat = '';
            $latitude = Input::get('address_latitude');
            if(!isset($latitude))
                $latitude = '';
            $longitude = Input::get('address_longitude');
            if(!isset($longitude))
                $longitude = '';

            $merchant = new Companies;
            $merchant->company_name = $name;
            $merchant->name_ar = $name_ar;
            $merchant->email = $email;
            $merchant->username = $username;
            $merchant->password = Hash::make($password);
            $merchant->phone = $mobile;
            $merchant->city = $city;
            $merchant->district = $district;
            $merchant->address = $address;
            $merchant->telegram = $telegram;
            $merchant->whatsapp = $whatsapp;
            $merchant->instagram = $instagram;
            $merchant->twitter = $twitter;
            $merchant->googleplus = $googleplus;
            $merchant->facebook = $facebook;
            $merchant->snapchat = $snapchat;
            $merchant->latitude = $latitude;
            $merchant->longitude = $longitude;
            $merchant->owner_id = $idnumber;
            $merchant->device_token = $device_token;
            $merchant->owner_id_scan = $idphoto;

            $merchant->is_api = 0;
            $merchant->company_type = 3;
            $merchant->transportation_fee = 25;
            $merchant->pickup_fee = 8;
            $merchant->delivery_fee = 8;
            $merchant->max_regular_weight = 12;
            $merchant->excess_weight_fees = 1;

            $merchant->save();
            $response_array = array('success' => true, 'id' => $merchant->id, 'message' => 'New merchant registered');

        } catch(Exeption $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function test() {
        $phone = Request::get('phone');
        $validatorPhone = Validator::make(
            array(
                'phone' => $phone,
            ), array(
                'phone' => 'phone'
            )
        );
        return $validatorPhone->fails() . ' --';
        return $validatorPhone->messages()->all();
        $merchant = Companies::where('username', '=', 'sdfsd')->first();
        if($merchant)
            return 'sfdd';
        return '324';
        return $merchant;
    }

    public function merchantlogin() {

        try {

            if (Input::has('username') && Input::has('password')) {

                $username = Input::get('username');
                $password = Input::get('password');
                $device_token = Input::get('device_token');

                $validator = Validator::make(
                    array(
                        'password' => $password,
                        'username' => $username,
                        'device_token' => $device_token,
                    ), array(
                        'password' => 'required',
                        'username' => 'required',
                        'device_token' => 'required',
                    )
                );

                if ($validator->fails()) {
                    $error_messages = $validator->messages();
                    Log::error('Validation error during manual login for walker = ' . print_r($error_messages, true));
                    $error_messages = $validator->messages()->all();
                    $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                    $response_code = 200;
                }
                else {

                    if($merchant = Companies::select('password','device_token','token','token_expiry')->where('username', '=', $username)->first()) {

                         if (Hash::check($password, $merchant->password)) {

                            if ($merchant->device_token != $device_token) {
                                $merchant->device_token = $device_token;
                            }
                            $merchant->token = generate_token();
                            $merchant->token_expiry = generate_expiry();
                            $merchant->save();
                            $response_array = array(
                                'success' => true,
                                'id' => $merchant->id,
                                'username' => $merchant->username,
                                'email' => $merchant->email,
                                'name' => $merchant->company_name,
                                'name_ar' => $merchant->name_ar == null ? '' : $merchant->name_ar,
                                'delivery_fee' => $merchant->delivery_fee,
                                'pickup_fee' => $merchant->pickup_fee,
                                'mobile' => $merchant->phone,
                                'address' => $merchant->address,
                                'district' => $merchant->district,
                                'city' => $merchant->city,
                                'country' => $merchant->country,
                                'id_number' => $merchant->owner_id,
                                'telegram' => $merchant->telegram,
                                'whatsapp' => $merchant->whatsapp,
                                'instagram' => $merchant->instagram,
                                'twitter' => $merchant->twitter,
                                'googleplus' => $merchant->googleplus,
                                'facebook' => $merchant->facebook,
                                'snapchat' => $merchant->snapchat,
                                'latitude' => $merchant->latitude,
                                'longitude' => $merchant->longitude,
                                'device_token' => $merchant->device_token,
                                'token' => $merchant->token,
                            );
                            $response_code = 200;

                        } else {
                            $response_array = array('success' => false, 'error' => 'Invalid Username or Password ', 'error_code' => 403);
                            $response_code = 200;
                        }
                    }
                    else {
                        $response_array = array('success' => false, 'error' => 'Merchant Not Found', 'error_code' => 404);
                        $response_code = 200;
                    }
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Invalid Username or Password', 'error_code' => 403);
                $response_code = 200;
            }

        } catch (Expception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response_code = 200;

        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function generateShipmentPost() {

        try {

            $merchant_id = Input::get('merchant_id');
            $merchant = Companies::where('id', '=', $merchant_id)->first();
            if(!$merchant) {
                $response_array = array('success' => false, 'error' => 'Merchnat is not exist');
                goto response;
            }
            if(!$merchant->allow_creation) {
                $response_array = array('success' => false, 'error' => 'You are not allowed to create orders. Please contact admin');
                goto response;
            }

            $customer_name = Input::get('customer_name');
            $mobile1 = Input::get('mobile1');
            $mobile2 = Input::get('mobile2') ? Input::get('mobile2') : '';
            $email = Input::get('email') ? Input::get('email') : '';
            $city = Input::get('city');
            $district = Input::get('district');
            $address = Input::get('address');
            $street_address = Input::get('street_address');
            $shipment_description = Input::get('shipment_description');
            $qunatity = Input::get('quantity');
            $weight = Input::get('weight');
            $cash_on_delivery = Input::get('cash_on_delivery');
            $order_number = Input::get('order_number');
            $address_latitude = Input::get('address_latitude') ? Input::get('address_latitude') : '';
            $address_longitude = Input::get('address_longitude') ? Input::get('address_longitude') : '';

            if(!isset($customer_name)) {
                $response_array = array('success' => false, 'error' => 'Customer name required');
                goto response;
            }

            if(!isset($mobile1)) {
                $response_array = array('success' => false, 'error' => 'Mobile required');
                goto response;
            }

            if(!isset($city)) {
                $response_array = array('success' => false, 'error' => 'City required');
                goto response;
            }

            if(!isset($address)) {
                $response_array = array('success' => false, 'error' => 'Address required');
                goto response;
            }

            if(!isset($street_address)) {
                $response_array = array('success' => false, 'error' => 'Street Address required');
                goto response;
            }

            if(!isset($shipment_description)) {
                $response_array = array('success' => false, 'error' => 'Shipment description required');
                goto response;
            }

            if(!isset($qunatity)) {
                $response_array = array('success' => false, 'error' => 'Qunatity required');
                goto response;
            }

            if(!isset($weight)) {
                $response_array = array('success' => false, 'error' => 'Weight required');
                goto response;
            }
            if (count(DeliveryRequest::select('id')->where('order_number', '=', $order_number)->where('company_id', '=', $merchant_id)->get())) {
                $response_array = array('success' => false, 'error' => 'order number  already exists, Please try again.');
                goto response;
            }

            $delivery_request = new DeliveryRequest;
            $delivery_request->order_number = $order_number;
            $delivery_request->d_description = $shipment_description;
            $delivery_request->d_quantity = $qunatity;
            $delivery_request->d_weight = $weight;
            $delivery_request->cash_on_delivery = $cash_on_delivery;
            $delivery_request->receiver_name = $customer_name;
            $delivery_request->receiver_phone = format_phone($mobile1);
            $delivery_request->receiver_phone2 = format_phone($mobile2);
            $delivery_request->receiver_email = $email;
            $delivery_request->d_city = $city;
            $delivery_request->d_district = $district;
            $delivery_request->d_address = $address;
            $delivery_request->d_address2 = $street_address;
            $delivery_request->D_latitude = $address_latitude;
            $delivery_request->D_longitude = $address_longitude;

            $delivery_request->company_id = $merchant_id;
            $delivery_request->pickup_city = $merchant->city;
            $delivery_request->pickup_district = $merchant->district;
            $delivery_request->pickup_address = $merchant->address;
            $delivery_request->pickup_latitude = $merchant->latitude;
            $delivery_request->pickup_longitude = $merchant->longitude;
            $delivery_request->sender_name = $merchant->company_name;
            $delivery_request->sender_phone = $merchant->phone;
            $delivery_request->sender_email = $merchant->email;
            $delivery_request->sender_city = $merchant->city;
            $delivery_request->o_address = $merchant->address;
            $delivery_request->sender_country = $merchant->country;
            $delivery_request->cash_on_delivery = 0;
            $delivery_request->service_type = 1;
            $delivery_request->status = -3;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->save();

            $dr = DeliveryRequest::find($delivery_request->id);
            $waybill = 'OS' . str_pad($dr->id,  8, "0", STR_PAD_LEFT).'KS';
            $dr->waybill = $waybill;
            $dr->jollychic = $waybill;
            $dr->save();

            $orders_history = new OrdersHistory;
            $orders_history->waybill_number = $dr->waybill;
            $order_history->order_number = $dr->order_number;
            $orders_history->status = $dr->status;
            $orders_history->company_id = $dr->company_id;
            $orders_history->save();

            $response_array = array('success' => true, 'waybill' => $waybill, 'message' => 'Order created successfully');

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage());
        }

        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatePickupInfo() {

        try {

            if(Input::has('waybill')) {
                $waybill = Input::get('waybill');
                $shipment = DeliveryRequest::select('company_id')->where('jollychic', '=', $waybill)->first();
                if(!$shipment) {
                    $response_array = array('success' => false, 'error' => 'Valid waybill required');
                    goto response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Waybill required');
                goto response;
            }

            if(Input::has('merchant_id')) {
                $merchant_id = Input::get('merchant_id');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant id required');
                goto response;
            }

            if($shipment->company_id != $merchant_id) {
                $response_array = array('success' => false, 'error' => 'Valid waybill required');
                goto response;
            }

            $updated_info = array();

            if(Input::has('pickup_city')) {
                $pickup_city = Input::get('pickup_city');
                $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                    'pickup_city' => $pickup_city
                ));
                if($res == 1)
                    array_push($updated_info, 'pickup_city');
            }

            if(Input::has('pickup_district')) {
                $pickup_district = Input::get('pickup_district');
                $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                    'pickup_district' => $pickup_district
                ));
                if($res == 1)
                    array_push($updated_info, 'pickup_district');
            }

            if(Input::has('pickup_address')) {
                $pickup_address = Input::get('pickup_address');
                $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                    'pickup_address' => $pickup_address
                ));
                if($res == 1)
                    array_push($updated_info, 'pickup_address');
            }

            if(Input::has('pickup_latitude')) {
                $pickup_latitude = Input::get('pickup_latitude');
                $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                    'pickup_latitude' => $pickup_latitude
                ));
                if($res == 1)
                    array_push($updated_info, 'pickup_latitude');
            }

            if(Input::has('pickup_longitude')) {
                $pickup_longitude = Input::get('pickup_longitude');
                $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                    'pickup_longitude' => $pickup_longitude
                ));
                if($res == 1)
                    array_push($updated_info, 'pickup_longitude');
            }

            $response_array = array('success' => true, 'waybill' => $waybill, 'message' => 'Pickup inforamtion updated successfully', 'updated_info' => $updated_info);

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => 'Faild due to an error, please try again!');

        }

        response:
        $response = Response::json($response_array, 200);
        return $response;

    }

    public function updateShipmentInfo() {
        try {

            $merchant_id = Input::get('merchant_id');
            if(!isset($merchant_id)) {
                $response_array = array('success' => false, 'error' => 'Merchant id required');
                goto response;
            }
            $merchant = Companies::select('id')->where('id', '=', $merchant_id)->first();
            if(!$merchant) {
                $response_array = array('success' => false, 'error' => 'Valid merchant id required');
                goto response;
            }

            $waybill = Input::get('waybill');

            if(!isset($waybill)) {
                $response_array = array('success' => false, 'error' => 'Waybill required');
            }
            else {

                $shipment = DeliveryRequest::select('company_id')->where('waybill', '=', $waybill)->first();
                if($shipment->company_id != $merchant_id) {
                    $response_array = array('success' => false, 'error' => 'Valid waybill required');
                    goto response;
                }

                $customer_name = Input::get('customer_name');
                $mobile1 = Input::get('mobile1');
                $mobile2 = Input::get('mobile2');
                $email = Input::get('email');
                $city = Input::get('city');
                $district = Input::get('district');
                $address = Input::get('address');
                $street_address = Input::get('street_address');
                $shipment_description = Input::get('shipment_description');
                $qunatity = Input::get('quantity');
                $weight = Input::get('weight');
                $order_number = Input::get('order_number');
                $address_latitude = Input::get('address_latitude');
                $address_longitude = Input::get('address_longitude');

                $updated_info = array();

                if(isset($order_number)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'order_number' => $order_number
                    ));
                    if($res == 1)
                        array_push($updated_info, 'order_number');
                }
                if(isset($shipment_description)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_description' => $shipment_description
                    ));
                    if($res == 1)
                        array_push($updated_info, 'shipment_description');
                }
                if(isset($qunatity)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_quantity' => $qunatity
                    ));
                    if($res == 1)
                        array_push($updated_info, 'quantity');
                }
                if(isset($weight)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_weight' => $weight
                    ));
                    if($res == 1)
                        array_push($updated_info, 'weight');
                }
                if(isset($customer_name)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'receiver_name' => $customer_name
                    ));
                    if($res == 1)
                        array_push($updated_info, 'customer_name');
                }
                if(isset($mobile1)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'receiver_phone' => $mobile1
                    ));
                    if($res == 1)
                        array_push($updated_info, 'mobile1');
                }
                if(isset($mobile2)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'receiver_phone2' => $mobile2
                    ));
                    if($res == 1)
                        array_push($updated_info, 'mobile2');
                }
                if(isset($email)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'receiver_email' => $email
                    ));
                    if($res == 1)
                        array_push($updated_info, 'email');
                }
                if(isset($city)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_city' => $city
                    ));
                    if($res == 1)
                        array_push($updated_info, 'city');
                }
                if(isset($district)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_district' => $district
                    ));
                    if($res == 1)
                        array_push($updated_info, 'district');
                }
                if(isset($address)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_address' => $address
                    ));
                    if($res == 1)
                        array_push($updated_info, 'address');
                }
                if(isset($street_address)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'd_address2' => $street_address
                    ));
                    if($res == 1)
                        array_push($updated_info, 'street_address');
                }
                if(isset($address_latitude)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'D_latitude' => $address_latitude
                    ));
                    if($res == 1)
                        array_push($updated_info, 'address_latitude');
                }
                if(isset($address_longitude)) {
                    $res = DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                        'D_longitude' => $address_longitude
                    ));
                    if($res == 1)
                        array_push($updated_info, 'address_longitude');
                }   

                $response_array = array('success' => true, 'waybill' => $waybill, 'message' => 'Shipment updated successfully', 'updated_info' => $updated_info);

            }
            
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
        }

        response:
        $response = Response::json($response_array, 200);
        return $response;

    }

    public function cancelShipmentPost() {

        try {

            if(Input::has('merchant_id')) {
                $merchant_id = Input::get('merchant_id');
                $merchant = Companies::select('id')->where('id', '=', $merchant_id)->first();
                if(!$merchant) {
                    $response_array = array('success' => false, 'error' => 'Valid merchant id required');
                    goto response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant id required');
                goto response;
            }

            if(Input::has('waybill')) {
                $waybill = Input::get('waybill');
                $shipment = DeliveryRequest::select('company_id','status')->where('jollychic', '=', $waybill)->first();
                if(!$shipment || $shipment->company_id != $merchant_id) {
                    $response_array = array('success' => false, 'error' => 'Valid waybill required');
                    goto response;
                }
                if($shipment->status != -3) {
                    $response_array = array('success' => false, 'error' => 'You can not cancell this shipment');
                    goto response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Waybill required');
                goto response;
            }

            DeliveryRequest::where('jollychic', '=', $waybill)->delete();
            OrdersHistory::where('waybill_number', '=', $waybill)->delete();

            $response_array = array('success' => true, 'waybill' => $waybill, 'message' => 'Shipment cancelled successfully');


        } catch(Exception $e) {

            $response_array = array('success' => false, 'error' => 'Failed due to an error, please try again!');

        }

        response:
        $response = Response::json($response_array, 200);
        return $response;

    }

    public function getorders () {

        try {

            $merchant_id = Input::get('merchant_id');
            if(Input::has('status')) {
                $status = Input::get('status');
            } else {
                $status = 'all';
            }
            
            $shipments = DeliveryRequest::where('company_id', '=', $merchant_id)->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->select([
                'jollychic as waybill',
                'status',
                DB::raw('ifnull(order_number, "") as order_number'),
                'd_quantity as quantity',
                'd_weight as weight',
                'd_description as shipment_description',
                'companies.company_type as type',
                'cash_on_pickup',
                'pickup_city',
                'pickup_district',
                'pickup_address',
                'pickup_latitude',
                'pickup_longitude',
                'pickup_walker',
                'sender_name',
                'sender_phone',
                'sender_email',
                'sender_city',
                'cash_on_delivery',
                'd_city as receiver_city',
                'd_district as receiver_district',
                'd_address as receiver_address',
                'd_address2 as receiver_street_address',
                'D_latitude as receiver_latitude',
                'D_longitude as receiver_longitude',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'receiver_email'
            ]);

            if($status != 'all') {
                $shipments = $shipments->where('status', '=', $status);
            }

            $shipments->orderBy('delivery_request.created_at', 'desc');
            $shipments = $shipments->get();

            $response_array = array('success' => true, 'shipments' => $shipments);

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => 'Faild due to an error, please try again!');

        }

        response:
        $response = Response::json($response_array, 200);
        return $response;

    }

    public function ordersPool() {

        try {

            if(Input::has('captain_id')) {
                $captain_id = Input::get('captain_id');
            } else {
                $captain_id = 0;
            }

            $newshipments = DeliveryRequest::where('status', '=', '-3')->leftJoin('companies','companies.id','=','delivery_request.company_id')->select([ 
                'jollychic as waybill',
                DB::raw('ifnull(order_number, "") as order_number'),
                'd_quantity as quantity',
                'd_weight as weight',
                'd_description as shipment_description',
                'companies.company_name',
                'companies.company_type as type',
                'cash_on_pickup',
                'pickup_city',
                'pickup_district',
                'pickup_address',
                'pickup_latitude',
                'pickup_longitude',
                'sender_name',
                'sender_phone',
                'sender_email',
                'sender_city',
                'cash_on_delivery',
                'd_city as receiver_city',
                'd_district as receiver_district',
                'd_address as receiver_address',
                'd_address2 as receiver_street_address',
                'D_latitude as receiver_latitude',
                'D_longitude as receiver_longitude',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'receiver_email'
            ])->get();

            $reservedshipments = DeliveryRequest::where('status', '=', '-2')->where('pickup_walker', '=', $captain_id)->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->select([
                'jollychic as waybill',
                DB::raw('ifnull(order_number, "") as order_number'),
                'd_quantity as quantity',
                'd_weight as weight',
                'd_description as shipment_description',
                'companies.company_name',
                'companies.company_type as type',
                'cash_on_pickup',
                'pickup_city',
                'pickup_district',
                'pickup_address',
                'pickup_latitude',
                'pickup_longitude',
                'sender_name',
                'sender_phone',
                'sender_email',
                'sender_city',
                'cash_on_delivery',
                'd_city as receiver_city',
                'd_district as receiver_district',
                'd_address as receiver_address',
                'd_address2 as receiver_street_address',
                'D_latitude as receiver_latitude',
                'D_longitude as receiver_longitude',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'receiver_email'
            ])->get();

            $response_array = array('success' => true, 'new_shipments' => $newshipments, 'reserved_shipments' => $reservedshipments);

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => 'Failed due to an error, please try again!');

        }

        response:
        $response = Response::json($response_array, 200);
        return $response;

    }

    public function pickup_time_intervals() {
        try {
            $intervals = PickupTimeInterval::select(["id", "from", "to"])->get();
            $response_array = array('success' => true, 'intervals' => $intervals);
        }
        catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed due to an error, please try again!');
        }
        response:
        return $response_array;
    }

    public function cancelPickupReasons() {
        try {
            $reasons = CanceledPickupReason::select(["id", "english", "arabic"])->get();
            $response_array = array('success' => true, 'reasons' => $reasons);
        }
        catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed due to an error, please try again!');
        }
        response:
        return $response_array;
    }

    public function freeReservedShipmentByCaptain() {
        try {
            if(Input::has('waybill')) {
                $waybill = Input::get('waybill');
            } else {
                $response_array = array('success' => false, 'error' => 'Waybill required');
                goto response;
            }
            if(Input::has('reason_id')) {
                $reason_id = Input::get('reason_id');
            } else {
                $response_array = array('success' => false, 'error' => 'Reason required');
                goto response;
            }
            $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();
            $reason = CanceledPickupReason::where('id', '=', $reason_id)->first();
            $response_array = DB::transaction(function() use ($shipment, $reason) {
                Walker::where('id', '=', $shipment->pickup_walker)->increment("canceled_pickup");
                $orders_history = new OrdersHistory;
                $orders_history->waybill_number = $shipment->jollychic;
                $orders_history->order_number = $shipment->order_number;
                $orders_history->company_id = $shipment->company_id;
                $orders_history->status = -3;
                $orders_history->notes = $reason->english;
                $orders_history->walker_id = $shipment->pickup_walker;
                $orders_history->reason_id = $reason->id;
                $orders_history->save();
                DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->update(array(
                    'status' => '-3', 'pickup_walker' => '0'
                ));
                $response_array = array('success' => true, 'waybill' => $shipment->jollychic, 'message' => 'Shipment cancelled succssfully');
                return $response_array;
            });
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed due to an error, please try again!');
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function freeReservedShipments() {

        try {

            $count = DeliveryRequest::where('status', '=', '-2')->whereRaw(DB::raw('timediff(time(now()), time(pickup_reserved_date)) >= "02:00:00"'))->update(array(
                'pickup_walker' => '0', 'status' => '-3' 
            ));

            if($count)
                $response_array = array('success' => true, 'shipments_count' => $count);
            else
                $response_array = array('success' => false, 'error' => 'Nothing updated');

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage());

        }

        response:
        $response = Response::json($response_array, 200);
        return $response;
        
    }

    public function maroof_merchants() {
        try {
            ///////////////////
            // $request = "https://maroof.sa/8599";
            // $client = new GuzzleHttp\Client();
            // $result = $client->get($request, ['auth' => ['user', 'pass']]);
            // $result = (string) $result->getBody();
            // return $result;
            ///////////////////
            ini_set('max_execution_time', 90000);
            $time1 = new DateTime();
            $threads = Users::where('id', '=', '13')->first(['role_id'])->role_id;
            $starter = Users::where('id', '=', 13)->first()->captain_id;
            if($threads) {
                $string = date('Y-m-d,H:i:s') . ' 00:00:00 ' . $starter . '->' . ($starter + 99) . ' 000 ' . 'Failed open-thread';
                return $string;
            }
            $id = $starter;
            Users::where('id', '=', '13')->update(array('owner_id' => date('i')));
            Users::where('id', '=', '13')->increment('role_id');
            $counter = 0;
            for($id = $starter; $id < $starter + 100; ++$id) {
                $request = "https://maroof.sa/$id";
                $client = new GuzzleHttp\Client();
                $result = $client->get($request, ['auth' => ['user', 'pass']]);
                $result = (string) $result->getBody();
                $array = str_split($result);

                $pos = strpos($result, 'جميع الأعمال');
                if($pos)
                    continue;

                ++$counter;
                
                $instagram = '';
                if($pos = strpos($result, $url = 'https://instagram.com/')) {
                    $url_len = strlen($url);
                    $i = $pos + $url_len;
                    $username = '';
                    while($i < count($array) && $array[$i] != '"')
                        $username .= $array[$i++];
                    if($username != 'maroof_sa' && $username != 'maroof_sa/')
                        $instagram = $url . $username;
                }
                
                $facebook = '';
                if($pos = strpos($result, $url = 'https://www.facebook.com/')) {
                    $url_len = strlen($url);
                    $i = $pos + $url_len;
                    $username = '';
                    while($i < count($array) && $array[$i] != '"')
                        $username .= $array[$i++];
                    $facebook = $url . $username;
                }

                $googleplus = '';
                if($pos = strpos($result, $url = 'https://plus.google.com/')) {
                    $url_len = strlen($url);
                    $i = $pos + $url_len;
                    $username = '';
                    while($i < count($array) && $array[$i] != '"')
                        $username .= $array[$i++];
                    $googleplus = $url . $username;
                }

                $twitter = '';
                if($pos = strpos($result, $url = 'https://twitter.com/')) {
                    $url_len = strlen($url);
                    $i = $pos + $url_len;
                    $username = '';
                    while($i < count($array) && $array[$i] != '"')
                        $username .= $array[$i++];
                    if($username != 'maroof_sa' && $username != 'maroof_sa/')
                        $twitter = $url . $username;
                }

                $website = '';
                if($pos = strpos($result, $label = 'الموقع الإلكتروني')) {
                    $pos = strpos($result, 'http', $pos);
                    while($pos < count($array) && $array[$pos] != '"')
                        $website .= $array[$pos++];
                }

                $mobile = '';
                if($pos = strpos($result, $label = 'رقم الجوال')) {
                    $pos = strpos($result, $label = 'tel:', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '"')
                        $mobile .= $array[$pos++];
                }

                $phone = '';
                if($pos = strpos($result, $label = 'رقم الهاتف')) {
                    $pos = strpos($result, $label = 'tel:', $pos);
                    $pos = strpos($result, $label = '">', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $phone .= $array[$pos++];
                }

                $email = '';
                if($pos = strpos($result, $label = 'البريد الإلكتروني')) {
                    $pos = strpos($result, $label = 'mailto:', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '"')
                        $email .= $array[$pos++];
                }

                $cr = '';
                if($pos = strpos($result, $label = 'السجل التجاري')) {
                    $pos = strpos($result, $label = '<span>', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $cr .= $array[$pos++];
                }

                $about = '';
                if($pos = strpos($result, $label = 'عن العمل')) {
                    $pos = strpos($result, $label = 'col-xs-12', $pos);
                    $pos += strlen($label) + 2;
                    while($pos < count($array) && $array[$pos] != '<' && $array[$pos] != '>')
                        $about .= $array[$pos++];
                }

                $rating = '';
                if($pos = strpos($result, $label = 'span class="rating-num">')) {
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rating .= $array[$pos++];  
                }

                $ratecount = '';
                if($pos = strpos($result, $label = 'مُقيّــم')) {
                    --$pos;
                    while($pos >= 0 && $array[$pos] != '>')
                        $ratecount .= $array[$pos--];  
                    $ratecount = strrev($ratecount); 
                }

                $rate_5 = '';
                if($pos = strpos($result, $label = 'bar-five')) {
                    $pos = strpos($result, $label = 'style="width:70px; float: left;">%', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rate_5 .= $array[$pos++];
                }

                $rate_4 = '';
                if($pos = strpos($result, $label = 'bar-four')) {
                    $pos = strpos($result, $label = 'style="width:70px; float: left;">%', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rate_4 .= $array[$pos++];
                }

                $rate_3 = '';
                if($pos = strpos($result, $label = 'bar-three')) {
                    $pos = strpos($result, $label = 'style="width:70px; float: left;">%', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rate_3 .= $array[$pos++];
                }

                $rate_2 = '';
                if($pos = strpos($result, $label = 'bar-two')) {
                    $pos = strpos($result, $label = 'style="width:70px; float: left;">%', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rate_2 .= $array[$pos++];
                }

                $rate_1 = '';
                if($pos = strpos($result, $label = 'bar-one')) {
                    $pos = strpos($result, $label = 'style="width:70px; float: left;">%', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $rate_1 .= $array[$pos++];
                }

                $name = '';
                if($pos = strpos($result, $label = 'media-body')) {
                    $pos = strpos($result, $label = '<span>', $pos);
                    $pos = strpos($result, $label = 'media-heading text-primary">', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $name .= $array[$pos++];
                }

                $name_ar = '';
                if($pos = strpos($result, 'media-body')) {
                    $pos = strpos($result, $label = '<span>', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $name_ar .= $array[$pos++];
                }

                $category = '';
                if($pos = strpos($result, $label = 'media-body')) {
                    $pos = strpos($result, $label = '<span>', $pos);
                    $pos = strpos($result, $label = 'media-heading text-primary">', $pos);
                    $pos = strpos($result, $label = 'margin-bottom:0px;">', $pos);
                    $pos += strlen($label);
                    while($pos < count($array) && $array[$pos] != '<')
                        $category .= $array[$pos++];
                }

                $goldaccount = 0;
                if($pos = strpos($result, $label = 'headerProfileDiv')) {
                    $pos = strpos($result, $label = 'col-sm-4 col-xs-12 hidden-lg hidden-md text-center', $pos);
                    $pos = strpos($result, $label = '<img src="data:image/png;', $pos);
                    $pos = strpos($result, $label = '"', $pos);
                    ++$pos;
                    $tmp = '';
                    while($array[$pos] != '"')
                        $tmp .= $array[$pos++];
                    $pos = strpos($tmp, '==');
                    if($pos)
                        $goldaccount = 1;
                }

                $thumbnail = '';
                if($pos = strpos($result, $label = 'media')) {
                    $pos = strpos($result, $label = 'media-body', $pos);
                    $pos = strpos($result, $label = 'https://maroof.sa/images/view', $pos);
                    while($pos < count($array) && $array[$pos] != '"')
                        $thumbnail .= $array[$pos++];
                }

                $comments = '';
                if(strpos($result, $label = 'AddCommentForm')) {
                    while($pos = strpos($result, $label = 'media comment-row', $pos)) {
                        $pos = strpos($result, $label = '<span>', $pos); // name
                        $pos += strlen($label);
                        if($array[$pos] == '{')
                            break;
                        $user = '(';
                        while($pos < count($array) && $array[$pos] != '<')
                            $user .= $array[$pos++];
                        $pos = strpos($result, $label = '<span>', $pos); // comment
                        $pos += strlen($label);
                        $comment = '';
                        while($pos < count($array) && $array[$pos] != '<')
                            $comment .= $array[$pos++];
                        $pos = strpos($result, $label = 'small text-info">', $pos); // timestamp
                        $pos += strlen($label);
                        $user .= ' | ';
                        while($pos < count($array) && $array[$pos] != '<')
                            $user .= $array[$pos++];
                        $user .= ')';
                        $comments .= $user . $comment . '***';
                    }
                }

                $maroof = new Maroof;
                $maroof->maroof_id = $id;
                $maroof->name = $name;
                $maroof->name_ar = $name_ar;
                $maroof->category = $category;
                $maroof->about = $about;
                $maroof->mobile = $mobile;
                $maroof->phone = $phone;
                $maroof->email = $email;
                $maroof->cr = $cr;
                $maroof->is_golden = $goldaccount;
                $maroof->instagram = $instagram;
                $maroof->twitter = $twitter;
                $maroof->facebook = $facebook;
                $maroof->googleplus = $googleplus;
                $maroof->website = $website;
                $maroof->comments = $comments;
                $maroof->rate = $rating;
                $maroof->rate_count = $ratecount;
                $maroof->rate_five = $rate_5;
                $maroof->rate_four = $rate_4;
                $maroof->rate_three = $rate_3;
                $maroof->rate_two = $rate_2;
                $maroof->rate_one = $rate_1;
                $maroof->thumbnail_image = $thumbnail;
                $maroof->save();
                
                // $current = $instagram . '<br/>' . $facebook . '<br/>' . $googleplus . 
                // '<br/>' . $twitter . '<br/>' . $website . '<br/>' . $mobile . 
                // '<br/>' . $email . '<br/>' . $cr . '<br/>' . $about . '<br/>' . $rating .
                // '<br/>' . "$ratecount <br/> $rate_5 $rate_4 $rate_3 $rate_2 $rate_1" . 
                // '<br/>' . $phone . '<br/>' . $name . '<br/>' . $name_ar . '<br/>' . $category .
                // '<br/>' . $goldaccount . '<br/>' . $thumbnail . '<br/>' . $comments;

                // echo $current . '<br/>========================================================================<br/>';

                sleep(mt_rand() / mt_getrandmax() + 1);
            }
            Users::where('id', '=', 13)->update(array(
                'captain_id' => $id
            ));
            Users::where('id', '=', '13')->decrement('role_id');
            $time2 = new DateTime();
            $diff = $time1->diff($time2);
            $string = date('Y-m-d,H:i:s') . ' ' . str_pad($diff->h,2,'0',STR_PAD_LEFT) . ':' . str_pad($diff->i,2,'0',STR_PAD_LEFT) . ':' . str_pad($diff->s,2,'0',STR_PAD_LEFT) . ' ' . $starter . '->' . ($starter + 99) . ' ' . str_pad($counter,3,'0',STR_PAD_LEFT) . ' Succeeded';
            return $string;
        } catch(Exception $e) {
            Users::where('id', '=', '13')->update(array('role_id'=>'0'));
            Users::where('id', '=', '13')->update(array('captain_id'=>$id));
            $st = $en = '#';
            if(isset($starter)) {
                $st = $starter;
                $en = $starter + 99;
            }
            if(!isset($counter))
                $counter = 0;
            $string = date('Y-m-d,H:i:s') . ' 00:00:00 ' . $st . '->' . $en . ' ' . str_pad($counter,3,'0',STR_PAD_LEFT) . ' Failed exception ' . $e->getMessage() . '-' . $e->getLine();
            return $string;
        }
    }

    //Api Call to send sms
    public function send_sms()
    {
        try {
            $destination = Input::get('mobile');

            $pincode=mt_rand(1000, 9999);
            $sms = $pincode;

            $curl = new Curl();
            $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destination&text=$sms&msg_type=2";

            $urlDiv = explode("?", $url);
            $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            Log::info("SMSSERVICELOG" . $result);

            /*if (strpos($result, 'Success') !== false) {
                return  array('success' => true, 'pincode' => $pincode);

            } else {
                return array('success' => false, 'error' => 'sms not sent due to error');;
            }*/
            //company agent
            $company_agent = BBCompanyUsers::leftjoin('companies', 'companies.id', '=', 'bb_company_users.company_id')
                ->select('bb_company_users.id', 'bb_company_users.username', 'bb_company_users.email', 'bb_company_users.phone','bb_company_users.device_type','bb_company_users.company_id','bb_company_users.company_type',  'bb_company_users.device_token', 'bb_company_users.token','bb_company_users.bb_token',
                    'jid','company_name', 'name_ar', 'delivery_fee', 'pickup_fee')->where('bb_company_users.phone', 'like', '%' . $destination . '%')->first();

            //company  admin
            $company_admin = Companies::select('id', 'username', 'email', 'company_name', 'name_ar', 'delivery_fee', 'pickup_fee', 'phone', 'address', 'district', 'city', 'country', 'owner_id', 'telegram', 'whatsapp', 'instagram', 'twitter', 'googleplus', 'facebook', 'snapchat', 'latitude', 'longitude', 'device_token', 'token')->where('company_type', '=', 0)->where('phone', 'like', '%' . $destination . '%')->first();

            //Marchant
            $marchent = Companies::select('id','username','email','company_name','name_ar','delivery_fee','pickup_fee','phone','address','district','city','country','owner_id','telegram','whatsapp','instagram','twitter','googleplus','facebook','snapchat','latitude','longitude','device_token','token')->where('company_type', '=', 3)->where('phone', 'like', '%' . $destination . '%')->first();
            if (count($company_agent)) {
                //company agent
                return array('success' => true,
                    'type' => 1,
                    'pincode' => $pincode,
                    'id' => $company_agent->id,
                    'username' => $company_agent->username,
                    'email' => $company_agent->email,
                    'jid' => $company_agent->jid,
                    'companay_id' => $company_agent->company_id,
                    'company_name' => $company_agent->company_name,
                    'name_ar' => $company_agent->name_ar == null ? '' : $company_agent->name_ar,
                    'delivery_fee' => $company_agent->delivery_fee,
                    'pickup_fee' => $company_agent->pickup_fee,
                    'mobile' => $company_agent->phone,
                    'device_type' => $company_agent->device_type,
                    'device_token' => $company_agent->device_token,
                    'bb_token'     => $company_agent->bb_token,
                    'token' => $company_agent->token);
            }  else if (count($company_admin)) {
                //company  admin
                return array('success' => true,
                    'pincode' => $pincode,
                    'type' => 2,
                    'id' => $company_admin->id,
                    'username' => $company_admin->username,
                    'email' => $company_admin->email,
                    'name' => $company_admin->company_name,
                    'name_ar' => $company_admin->name_ar == null ? '' : $company_admin->name_ar,
                    'delivery_fee' => $company_admin->delivery_fee,
                    'pickup_fee' => $company_admin->pickup_fee,
                    'mobile' => $company_admin->phone,
                    'address' => $company_admin->address,
                    'district' => $company_admin->district,
                    'city' => $company_admin->city,
                    'country' => $company_admin->country,
                    'id_number' => $company_admin->owner_id,
                    'telegram' => $company_admin->telegram,
                    'whatsapp' => $company_admin->whatsapp,
                    'instagram' => $company_admin->instagram,
                    'twitter' => $company_admin->twitter,
                    'googleplus' => $company_admin->googleplus,
                    'facebook' => $company_admin->facebook,
                    'snapchat' => $company_admin->snapchat,
                    'latitude' => $company_admin->latitude,
                    'longitude' => $company_admin->longitude,
                    'device_token' => $company_admin->device_token,
                    'token' => $company_admin->token);
            }  else if (count($marchent)) {
                $is_marchant = 1;
                return array('success' => true,
                    'pincode' => $pincode,
                    'is_marchant' => $is_marchant,
                    'type' => 3,
                    'id' => $marchent->id,
                    'username' => $marchent->username,
                    'email' => $marchent->email,
                    'name' => $marchent->company_name,
                    'name_ar' => $marchent->name_ar == null ? '' : $marchent->name_ar,
                    'delivery_fee' => $marchent->delivery_fee,
                    'pickup_fee' => $marchent->pickup_fee,
                    'mobile' => $marchent->phone,
                    'address' => $marchent->address,
                    'district' => $marchent->district,
                    'city' => $marchent->city,
                    'country' => $marchent->country,
                    'id_number' => $marchent->owner_id,
                    'telegram' => $marchent->telegram,
                    'whatsapp' => $marchent->whatsapp,
                    'instagram' => $marchent->instagram,
                    'twitter' => $marchent->twitter,
                    'googleplus' => $marchent->googleplus,
                    'facebook' => $marchent->facebook,
                    'snapchat' => $marchent->snapchat,
                    'latitude' => $marchent->latitude,
                    'longitude' => $marchent->longitude,
                    'device_token' => $marchent->device_token,
                    'token' => $marchent->token);



            } else {
                //this is for customer
                $is_marchant = 0;
                return array('success' => true ,'type' => 4 , 'pincode' => $pincode, 'is_marchant' => $is_marchant );
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    //API call to get covered items for captain.
    public function getCoveredDeliveryRequests()
    {
        try {
            $walker_id = Input::get('captain_id');
            $token = Input::get('token');
            $validator = Validator::make(
                array(
                    'walker_id' => $walker_id,
                    'token' => $token,
                ), array(
                    'walker_id' => 'required|integer',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {

                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {

                        $d_reqs = DeliveryRequest::select('id','jollychic','order_number','cash_on_pickup','pickup_city','pickup_district','pickup_address','pickup_longitude','pickup_latitude','sender_name','sender_phone','sender_email','sender_city','cash_on_delivery','receiver_name','receiver_phone','receiver_phone2','d_state','d_city','d_zipcode','d_district','d_address','d_address2','D_longitude','D_latitude','d_weight','d_quantity','d_description','scheduled_shipment_date','urgent_delivery','status','timeslot')->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', $walker_id)->where('status', '=', 5)
                            ->where('is_amount_covered', '=', 1)->get();
                        if (count($d_reqs)) {
                            $items = array();
                            foreach ($d_reqs as $item) {
                                array_push($items, array(
                                    'id' => $item->id,
                                    'jollychic_id' => $item->jollychic,
                                    'order_number' => isset($item->order_number) ? $item->order_number : '',
                                    'cash_on_pickup' => $item->cash_on_pickup,
                                    'pickup_city' => $item->pickup_city,
                                    'pickup_district' => $item->pickup_district,
                                    'pickup_address' => $item->pickup_address,
                                    'pickup_latitude' => $item->pickup_latitude,
                                    'pickup_longitude' => $item->pickup_longitude,
                                    'sender_name' => $item->sender_name,
                                    'sender_phone' => $item->sender_phone,
                                    'sender_email' => $item->sender_email,
                                    'sender_city' => $item->sender_city,
                                    'cash_on_delivery' => $item->cash_on_delivery,
                                    'receiver_name' => $item->receiver_name,
                                    'receiver_phone' => $item->receiver_phone,
                                    'receiver_phone2' => $item->receiver_phone2,
                                    'receiver_state' => $item->d_state,
                                    'receiver_city' => $item->d_city,
                                    'receiver_zipcode' => $item->d_zipcode,
                                    'receiver_district' => $item->d_district,
                                    'receiver_address' => $item->d_address,
                                    'receiver_address2' => isset($item->d_address2) ? $item->d_address2 : '',
                                    'receiver_address3' => isset($item->d_address3) ? $item->d_address3 : '',
                                    'receiver_lat' => $item->D_latitude,
                                    'receiver_lng' => $item->D_longitude,
                                    'company_name' => !empty($item->sender_name)?$item->company_name.' ['.$item->sender_name.']':$item->company_name,
                                    'weight' => $item->d_weight,
                                    'quantity' => $item->d_quantity,
                                    'shipment_description' => isset($item->d_description) ? $item->d_description : '',
                                    'scheduled_shipment_date' => isset($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
                                    'type' => isset($item->company_type) ? $item->company_type : '',
                                    'urgent_delivery' => isset($item->urgent_delivery) ? $item->urgent_delivery : '',
                                    'timeslot' => isset($item->timeslot) ? $item->timeslot : '',
                                    'status' => $item->status
                                ));
                            }
                            $response_array = array(
                                'success' => true,
                                'delivery_requests' => $items
                            );
                        } else {
                            $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    }

                } else {
                    if ($is_admin) {
                        $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function isAdmin($token)
    {
        return false;
    }
    public function getCaptainData($walker_id, $token, $is_admin)
    {

        if ($walker_data = Walker::select('token_expiry')->where('token', '=', $token)->where('id', '=', $walker_id)->first()) {
            return $walker_data;
        } elseif ($is_admin) {
            $walker_data = Walker::select('token_expiry')->where('id', '=', $walker_id)->first();
            if (!$walker_data) {
                return false;
            }
            return $walker_data;
        } else {
            return false;
        }

    }

    public function updateshipment_address()
    {
        try {
            $walker_id = Input::get('captain_id');
            $token = Input::get('token');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            $address = Input::get('address');
            $waybill = Input::get('waybill');

            $validator = Validator::make(
                array(
                    'walker_id' => $walker_id,
                    'token' => $token,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'waybill' => $waybill,
                ), array(
                    'walker_id' => 'required',
                    'token' => 'required',
                    'latitude' => 'required',
                    'longitude' => 'required',
                    'waybill' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error while during captain registration = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {

                    DeliveryRequest::where('jollychic', '=', $waybill)->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', $walker_id)
                        ->update(array('D_latitude' => $latitude, 'D_longitude' => $longitude));
                    //d_address Optional
                    if (isset($address)) {
                        DeliveryRequest::where('jollychic', '=', $waybill)->where('planned_walker', '=', $walker_id)->where('confirmed_walker', '=', $walker_id)
                            ->update(array('d_address' => $address));
                    }
                    $response_array = array(
                        'success' => true, 'message' => 'Record updated successfully for Waybill :' . $waybill);
                    $response_code = 200;
                } else {
                    if ($is_admin) {
                        $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }

            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }



    public function allStackeHolders()
    {
        try {
            $waybill = Input::get('waybill');
            
            $validator = Validator::make(
                array(
                    'waybill' => $waybill,
                    
                ), array(
                    'waybill' => 'required',
                    
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                
                $order = DeliveryRequest::select('confirmed_walker','company_id','receiver_phone','receiver_name')->where('jollychic','=', $waybill)->first();

                if(!$order){

                    $response_array = array('success' => false, 'error' => 'waybill mismatch !');
                    $response = Response::json($response_array, 200);
                    return $response; 
                }

                $confirmedWalker = $order->confirmed_walker;

                $captain = Walker::select('email','first_name','last_name')->where('id','=', $confirmedWalker)->first();

                if($captain){
                $captainEmail = $captain->email;
                $captainName  = $captain->first_name.' '.$captain->last_name;
                //getting username from email before @ 
                $captainUsername = strstr($captainEmail, '@', true);
                $captain =  'captain_'.$captainUsername.'_'.$confirmedWalker;

                }
                else
                {

                    $captainEmail = '';
                    $captainName = '';
                    $captainUsername = '';
                    $captain = '';
                }

                $companyId = $order->company_id;
                $customerName = $order->receiver_name;

                $receiverPhone = $order->receiver_phone;

                // remove country code from phone //
                $receiverPhone = substr($receiverPhone, -9);

                //jabber users
                
                $customer = 'customer_'.$receiverPhone;
                

                $companyArray = array();
                $captainArray = array();
                $customerArray = array();
                $saeeAgentArray = array();
                $participantsArray = array();

                
                
                $captainArray['role'] = 'captain';
                $captainArray['jid'] = $captain;
                $captainArray['name'] = $captainName;


                $customerArray['role'] = 'customer';
                $customerArray['jid'] = $customer;
                $customerArray['name'] = $customerName;

                $allSaeeAgents = Admin::select('id','username')->where('is_bb_user','=','1')->get();

                if(!empty($allSaeeAgents)){ // if have saee agents add to particepants array
                foreach ($allSaeeAgents as $key => $value) {
                    
                    if(filter_var($value->username, FILTER_VALIDATE_EMAIL)) {
        
                     $username = strstr($value->username, '@', true); //"if email get username"
                 }else{

                    $username = $value->username;
                 }
                    $saeeAgentArray['role'] = 'saee agent';
                    $saeeAgentArray['jid']  = 'saee_agent_'.$username.'_'.$value->id;
                    $saeeAgentArray['name'] = 'Saee Agent';

                    array_push($participantsArray, $saeeAgentArray);// push each entry to participants
                }
            }


                 $allCompanyAgents = BBCompanyUsers::select('jid','username')->where('company_id','=',$companyId)->get();

                if(!empty($allCompanyAgents)){ // if have saee agents add to particepants array
                foreach ($allCompanyAgents as $key => $value) {
                    
                    $companyArray['role'] = 'agent';
                    $companyArray['jid']  =  $value->jid;
                    $companyArray['name'] =  $value->username;

                    array_push($participantsArray, $companyArray);// push each entry to participants
                }
            }

                
                // if captain not set do not push to participants array
                if(!empty($captain)){
                array_push($participantsArray, $captainArray);
                }
                array_push($participantsArray, $customerArray);

                $response_array = array('success' => true, 'participants' => $participantsArray);
                $response_code = 200;
            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

     

     public function shipmentDetailsBb(){

         try {

            $waybills = Input::get('waybills');

            $validator = Validator::make(
                array(
                    'waybills' => $waybills,
                    
                ), array(
                    'waybills' => 'required',
                    
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;

                $response = Response::json($response_array,$response_code);
                return $response;
            }

            $count = count(explode(',',$waybills));

            if($count > 10){

                $response_array = array('success' => false, 'message' => 'Waybills greater than 10 please send less than 10 waybills');
                $response_code = 200;                 
                $response = Response::json($response_array, $response_code);
                return $response;
            }

            $waybills = "'" . str_replace(",", "','", $waybills) . "'"; // enclosing in single quotes

            // selecting updated_at but named it as created_at/// change this for future build
            $shipmentDetailsQuery = "SELECT jollychic as waybill, receiver_name,status, cash_on_delivery,confirmed_walker, updated_at as created_at,company_id FROM delivery_request WHERE jollychic in (".$waybills.") GROUP BY jollychic"; 

                $shipmentDetails = DB::select($shipmentDetailsQuery);

                foreach($shipmentDetails as $detail){

                    if($detail->confirmed_walker == 0){

                        $detail->confirmed_walker = '';
                        $detail->walker_phone = '';
                    }
                    else{

                        $walker = Walker::select('first_name','last_name','phone')->where('id','=',$detail->confirmed_walker)->first();

                        $detail->confirmed_walker = $walker->first_name.' '.$walker->last_name;
                        $detail->walker_phone     = $walker->phone;
                    }

                   
                    $company = Companies::select('company_name')->where('id','=',$detail->company_id)->first();

                    $statueses = 'select status, updated_at from orders_history where waybill_number = "'.$detail->waybill.'" and status in(0,1,2,3,4,5,6,7) group by status order by status asc ';

                    $distinctStatusHistory = DB::select($statueses);

                    $detail->company_name = $company->company_name;
                    $detail->order_history = $distinctStatusHistory;
                    
                }

                $result = json_decode(json_encode($shipmentDetails, true), true);


                $response_array = array('success' => true, 'shipmentDetails' => $result);
                $response_code = 200;

                $response = Response::json($response_array,$response_code);
                return $response;
         }
         catch (Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage(), 'line' => $e->getLine());
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
         }

    }
	
	public function missionshipmentsscan($mission, $walker, $status)
    {
        $captain_id = $walker->id;

        if ($status == 4) {
            if (0 <= $mission->status && $mission->status <= 2) {
                if ($mission->status == 0 && $mission->reserve_captain == 0) {
                    //Mission is not reserve for captain
                    return -13;
                } else if ($mission->status == 1 && $mission->planned_captain == 0) {
                    //Mission is not planned for captain
                    return -14;
                } else if ($mission->status == 2 && $mission->planned_captain == $captain_id) {

                    $unscheduled = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id', '=', $mission->id)->whereNull('scheduled_shipment_date')->count();
                    if ($unscheduled > 0) {
                        //Scheduled_shipment_date not set for Mission
                        return -17;
                    }
                    $mission_shipments_old_status = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id', '=', $mission->id)->where('status', '<>', 3)->count();
                    if ($mission_shipments_old_status > 0) {
                        //Mission shipments are not ready scanning
                        return -18;
                    }
                    $scheduled_dates = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id', '=', $mission->id)->distinct()->get();
                   
					if (count($scheduled_dates) > 1) {
                        //Scheduled shipment date must be same for all shipments in mission
                        return -19;
                    }

                    $pendingPayment = DeliveryRequest::select('is_money_received')->where('confirmed_walker', '=', $captain_id)->where('status', '=', 5)->where('is_money_received', '=', 0)->count();
                    if ($pendingPayment > 0) {
                        //Captain still has delivered/pending cash on delivery.
                        return -7;
                    }
                    $pendingCount = DeliveryRequest::select('confirmed_walker')->where('confirmed_walker', '=', $captain_id)->where('status', '=', 6)->count();
                    if ($pendingCount > 0) {
                        //Captain still has undelivered pending shipments.
                        return -8;
                    }
                    $today = date('Y-m-d');
                    $pendingshipments = DeliveryRequest::select('confirmed_walker')->where('confirmed_walker', '=', $captain_id)->where('scheduled_shipment_date', '<', $today)->where('status', '<>', 5)->count();
                    if ($pendingshipments > 0) {
                        //Captain still has undelivered pending shipments with expired scheduled shipment date
                        return -9;
                    }
                    if ($mission->scheduled_shipment_date > $today || $scheduled_dates[0]->scheduled_shipment_date > $today) {
						//Scheduled shipment date is greater than today date
                        return -10;
                    }
                    $jollycic_ids = DeliveryRequest::select('jollychic')->where('mission_id', '=', $mission->id)->where('status', '=', 3)
                        ->where('planned_walker', '=', $captain_id)->where('confirmed_walker', '=', 0)->get();

                    $scan_shipments = 0;
                    for ($a = 0; $a < count($jollycic_ids); ++$a) {
                        $jollychic = $jollycic_ids[$a]->jollychic;

                        $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->where('status', '=', 3)
                            ->where('mission_id', '=', $mission->id)->where('planned_walker', '=', $captain_id)
                            ->update(array('status' => $status, 'confirmed_walker' => $captain_id));
                        $item = DeliveryRequest::select('order_number', 'company_id')->where('jollychic', '=', $jollychic)->first();
                        if ($flag > 0) {
                            $scan_shipments++;
                            $order_history = new OrdersHistory;
                            $order_history->waybill_number = $jollychic;
                            $order_history->order_number = $item->order_number;
                            $order_history->company_id = $item->company_id;
                            $order_history->status = $status;
                            $order_history->walker_id = $captain_id;
                            $order_history->save();

                            DeliveryRequest::where('jollychic', '=', $jollychic)->update(array(
                                'pickup_timestamp' => date('Y-m-d H:i:s')));
                            $this->sendsmstocustomer($item, $captain_id);

                        }
                    }
                    if ($scan_shipments == $mission->total_shipments) {
                        $mission_in_progress = Mission::where('id', '=', $mission->id)->where('city', '=', $walker->city)
                            ->where('reserve_captain', '=', $captain_id)->where('reserve_captain', '=', $captain_id)
                            ->update(array('status' => 3,'scheduled_shipment_date' => $scheduled_dates[0]->scheduled_shipment_date));
                    }
                    if ($mission_in_progress > 0) {
                        //Status updated successfully
                        return 1;
                    }

                } else {
                    return -1;
                }
            } else {
                if ($mission->status == 3) {
                    //mission already scanned
                    return -15;
                } else {
                    return -1;
                }
            }
        } else {
            //Desired status not allowed
            return -1;
        }

    }

    function sendsmstocustomer($item, $captain_id)
    {
        if ($item->company_id == 917 || $item->company_id == 931) {
            $sms = "عزيزي/زتي ";
            $sms .= "يوجد لديكم شحنة عن طريق ";
            $company = Companies::find($item->company_id);
            if (isset($company)) {
                $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
            }
            $sms .= "  رقم ";
            $sms .= $item->order_number;
            $sms .= "  برقم كود ";
            $sms .= $item->pincode;

            if ($item->cash_on_delivery) {
                $sms .= " ومبلغ ";
                $sms .= $item->cash_on_delivery;
                $sms .= " ريال";
            }
            $sms .= "  مع المندوب ";

            if ($captain_id > 0) {
                $walker = Walker::find($captain_id);
                $walker->total_scanned = $walker->total_scanned + 1;
                $walker->save();
                if (count($walker)) {
                    $sms .= $walker->first_name;
                    $sms .= " 0";
                    $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                }
            }
        } else {
            $sms = "عزيزي عميل ";
            $company = Companies::find($item->company_id);
            if (isset($company)) {
                $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
            }

            $sms .= " طلبكم سيصلكم اليوم مع المندوب ";
            //get order:

            if ($captain_id > 0) {
                $walker = Walker::select('first_name', 'phone')->where('id', '=', $captain_id)->first();

                if (count($walker)) {
                    $sms .= $walker->first_name;
                    $sms .= " 0";
                    $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                }
            }

            $sms .= " رمز التسليم ";
            $sms .= $item->pincode;
            if ($item->cash_on_delivery) {
                $sms .= " ومبلغ الطلب ";
                $sms .= $item->cash_on_delivery;
                $sms .= " ريال";
            }
            $sms .= " مع تحيات ساعي";
        }


        $destinations1 = "966";
        $destinations1 .= substr(str_replace(' ', '', $item->receiver_phone), -9);

        if (isset($company) && $company->id != '952' || !isset($company)) {

            $curl = new Curl();
            //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

            $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            $urlDiv = explode("?", $url);
            $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            Log::info("SMSSERVICELOG" . $result);


            if ($item->receiver_phone2 != '') {
                $destinations2 = "966";
                $destinations2 .= substr(str_replace(' ', '', $item->receiver_phone2), -9);
                $curl = new Curl();
                //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

                $urlDiv = explode("?", $url);
                $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                Log::info("SMSSERVICELOG" . $result);
            }

        }
    }

}


