<?php

/*
 * This class handles the same data as WebProviderController except it deals with simpler form, no login required to register.
 *  
 */


class WebProviderSimpleController extends \BaseController {


     public function providerRegisterSimple()
    {

        try {

            $name = Input::get('name');
            $mobile = Input::get('mobile');
            $email = Input::get('email');
            $password = Input::get('password');
            $city = Input::get('city');
            $userdata = array($name, $mobile, $email, $password, $city);


            return View::make('web.captainregi')->with("userdata", $userdata);


        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

      //welcome page
           public function welcomeCaptain( ){

              return View::make('web.welcomecaptain');

          }

          //parms Document,docID and walkerID
          public function uploadDoc(){


               try{

                   $doc =Request::file("file");
                   $docid = Request::get("docid");
                   $walker_id = Request::get("walkerid");


                    $walker_document = new WalkerDocument;
                    $walker_document->walker_id = $walker_id;
                    $walker_document->document_id = $docid;



                   $file_name = time();
                   $file_name .= rand();
                   $file_name = sha1($file_name);

                   $ext = $doc->getClientOriginalExtension();
                   $doc->move(public_path() . "/uploads", $file_name . "." . $ext);
                   $local_url = $file_name . "." . $ext;




                   // Upload to S3
                   if (Config::get('app.s3_bucket') != "") {
                       $s3 = App::make('aws')->get('s3');
                       $pic = $s3->putObject(array(
                           'Bucket' => Config::get('app.s3_bucket'),
                           'Key' => $file_name,
                           'SourceFile' => public_path() . "/uploads/" . $local_url,
                       ));

                       $s3->putObjectAcl(array(
                           'Bucket' => Config::get('app.s3_bucket'),
                           'Key' => $file_name,
                           'ACL' => 'public-read'
                       ));

                       $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                   } else {
                       $s3_url = asset_url() . '/uploads/' . $local_url;
                   }

                   $walker_document->url = $s3_url;
                   $walker_document->save();



                   return "Done !!";

               }catch (Exception $exception){

                   return $exception->getMessage();
               }


          }








    //create init user from web.testpage...
    public function createNewWalker()
    {



        try{

            $first_name = Request::get('firstname');
            $last_name = Request::get('lastname');
            $email = Request::get('email');
            $phone = Request::get('mobile');
            $password = Request::get('password');
            $government_id_number = Request::get('govID');
            $date_of_birth = Request::get('BirthDate');
            $license_expiry_date = Request::get('lExpiryDate');
            $city = Request::get('city');
            $carmodel = Request::get('carmodel');
            $vehicle_sequence_number = Request::get('SequenceNumber');
            $car_registration_expiry = Request::get('RegisterationExpiry');
            $car_number_plate_number = Request::get('PlateNumber');
            $car_number_plate_letter_1 = Request::get('PlateLetter1');
            $car_number_plate_letter_2 = Request::get('PlateLetter2');
            $car_number_plate_letter_3 = Request::get('PlateLetter3');


            //no need
            /*list($letters,$car_number_plate_number) = sscanf($plate, "%[A-Z]%d");*/

            $activation_code = uniqid();
            $walker = new Walker;
            $walker->first_name = $first_name;
            $walker->last_name = $last_name;
            $walker->email = $email;
            $walker->phone = $phone;
            $walker->activation_code = $activation_code;
            $walker->password = Hash::make($password);


            $walker->email_activation = 1;
            $walker->government_id_number = $government_id_number;
            $walker->date_of_birth = $date_of_birth;
            $walker->license_expiry_date = $license_expiry_date;
            $walker->vehicle_sequence_number = $vehicle_sequence_number;
            $walker->car_number_plate_number = $car_number_plate_number;
            $walker->car_number_plate_letter_1 = $car_number_plate_letter_1;
            $walker->car_number_plate_letter_2 = $car_number_plate_letter_2;
            $walker->car_number_plate_letter_3 = $car_number_plate_letter_3;
            if ($carmodel != "") {
                $walker->car_model = $carmodel;
            }
            $walker->car_registration_expiry = $car_registration_expiry;
            $walker->token = generate_token();
            $walker->token_expiry = generate_expiry();


            $walker->credit = 0.00;
            $walker->picture ="";
            $walker->bio="";
            $walker->address="";
            $walker->state=$city;
            $walker->zipcode ="";
            $walker->device_token ="";
            $walker->social_unique_id="";
            $walker->is_active=0;
            $walker-> is_available =0;
            $walker->latitude=0;
            $walker->longitude=0;
            $walker->is_approved=0;
            $walker->type=0;
            $walker->merchant_id=0;
            $walker->account_id=0;
            $walker->type=0;
            $walker->activation_code=0;
            $walker->timezone=0;
/*            $walker->deleted_at=0;*/
            $walker->old_latitude=0;
            $walker->old_longitude=0;
            $walker->bearing=0;
            $walker->car_number=0;
            $walker->rate=0;
            $walker->country="ksa";
            $walker->rate_count=0;
            $walker->plate_type="0";

            $walker->waslReferenceNumber=0;
	 	    $walker->vehicleReferenceNumber=0;
            $walker->address_latitude =0;
            $walker->address_longitude =0;
            $walker->is_active_monthly	=0;
            $walker->flagisact_user	=0;
            $walker->flagisactmonthly_user	=0;
            $walker->save();


            //wait for this....
            /*$walker->address_latitude = $address_latitude;
            $walker->address_longitude = $address_longitude;*/
            /*$walker->save();*/





            $savedwalker = Walker::where('email', '=', $email)->first();

            $walker_id = $savedwalker->id;


            /* $subject = "Welcome On Board";
              $email_data['name'] = $walker->first_name;
              $url = URL::to('/provider/activation') . '/' . $activation_code;
              $email_data['url'] = $url;

                       send_email($walker->id, 'walker', $email_data, $subject, 'providerregister'); */


          /*  $settings = Settings::where('key', 'admin_email_address')->first();
            $admin_email = $settings->value;
            $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($walker->first_name . " " . $walker->last_name), 'web_url' => web_url());
            $subject = "Welcome to " . ucwords(Config::get('app.website_title')) . ", " . ucwords($walker->first_name . " " . $walker->last_name) . "";
            email_notification($walker_id, 'walker', $pattern, $subject, 'walker_register', "imp");*/

            return  $walker_id;



        } catch (Exception $exception){
            return $exception->getMessage();
        }


    }




}



