<?php

class OwnerController extends BaseController
{

    public function isAdmin($token)
    {
        return false;
    }

    public function getOwnerData($owner_id, $token, $is_admin)
    {

        if ($owner_data = Owner::where('token', '=', $token)->where('id', '=', $owner_id)->first()) {
            return $owner_data;
        } elseif ($is_admin) {
            $owner_data = Owner::where('id', '=', $owner_id)->first();
            if (!$owner_data) {
                return false;
            }
            return $owner_data;
        } else {
            return false;
        }
    }

    public function get_app_version()
    {
        $ios_customer_app = Settings::where("key", '=', 'ios_app_version_user')->first();
        $android_customer_app = Settings::where("key", '=', 'android_app_version_user')->first();
        $response_array = array('success' => true, 'android' => $android_customer_app->value, 'ios' => $ios_customer_app->value);
        $response_code = 200;

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_braintree_token()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if (Config::get('app.default_payment') == 'braintree') {

                        Braintree_Configuration::environment(Config::get('app.braintree_environment'));
                        Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
                        Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
                        Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
                        $clientToken = Braintree_ClientToken::generate();
                        $response_array = array('success' => true, 'token' => $clientToken);
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Please change braintree as default gateway', 'error_code' => 440);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID is not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID is not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    /* public function apply_referral_code() {
      $referral_code = Input::get('referral_code');
      $token = Input::get('token');
      $owner_id = Input::get('id');

      $validator = Validator::make(
      array(
      'referral_code' => $referral_code,
      'token' => $token,
      'owner_id' => $owner_id,
      ), array(
      'referral_code' => 'required',
      'token' => 'required',
      'owner_id' => 'required|integer'
      )
      );

      if ($validator->fails()) {
      $error_messages = $validator->messages();
      $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
      } else {
      $is_admin = $this->isAdmin($token);
      if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
      // check for token validity
      if (is_token_active($owner_data->token_expiry) || $is_admin) {
      if ($ledger = Ledger::where('referral_code', $referral_code)->where('owner_id', '!=', $owner_id)->first()) {
      $referred_by = $ledger->owner_id;
      $settings = Settings::where('key', 'default_referral_bonus_to_refered_user')->first();
      $referral_bonus = $settings->value;

      $ledger = Ledger::find($ledger->id);
      if ($ledger->referral_code != NULL) {
      $ledger->referral_code = $referral_code;
      $ledger->total_referrals = $ledger->total_referrals + 1;
      $ledger->amount_earned = $ledger->amount_earned + $referral_bonus;
      $ledger->save();

      $owner = Owner::find($owner_id);
      $owner->referred_by = $ledger->owner_id;
      $owner->save();

      $response_array = array('success' => true);
      } else {
      $response_array = array('success' => false, 'error' => 'Already applied referral code', 'error_code' => 482);
      }
      } elseif ($ledger = Ledger::where('referral_code', $referral_code)->where('owner_id', $owner_id)->first()) {
      $response_array = array('success' => false, 'error' => 'Can not add your own Referral code', 'error_code' => 483);
      } elseif ($pcode = PromoCodes::where('coupon_code', $referral_code)->where('type', 2)->where('state', 1)->where('uses', '>', 0)->first()) {
      $promohistory = PromoHistory::where('user_id', $owner_id)->where('promo_code', $referral_code)->first();
      if (!$promohistory) {
      $promo_code = $pcode->id;
      $pcode->uses = $pcode->uses - 1;
      $pcode->save();
      $phist = new PromoHistory();
      $phist->user_id = $owner_id;
      $phist->promo_code = $referral_code;
      // Assuming all are absolute discount
      $phist->amount_earned = $pcode->value;
      $phist->save();
      // Add to ledger amount
      $led = Ledger::where('owner_id', $owner_id)->first();
      if ($led) {
      $led->amount_earned = $led->amount_earned + $pcode->value;
      $led->save();
      } else {
      $ledger = new Ledger();
      $ledger->owner_id = $owner_id;
      $ledger->referral_code = "0";
      $ledger->total_referrals = 0;
      $ledger->amount_earned = $pcode->value;
      $ledger->amount_spent = 0;
      $ledger->save();
      }
      $response_array = array('success' => true);
      } else {
      $response_array = array('success' => false, 'error' => 'Promo Code Already Applied.', 'error_code' => 495);
      }
      } elseif ($pcode = PromoCodes::where('coupon_code', $referral_code)->where('uses', 0)->first()) {
      $response_array = array('success' => false, 'error' => 'Invalid promo code', 'error_code' => 496);
      } elseif ($pcode = PromoCodes::where('coupon_code', $referral_code)->where('type', 1)->first()) {
      $response_array = array('success' => false, 'error' => 'Percentage discount can not be applied here.', 'error_code' => 465);
      } elseif ($pcode = PromoCodes::where('coupon_code', '!=', $referral_code)->first()) {
      $response_array = array('success' => false, 'error' => 'Invalid promo code', 'error_code' => 475);
      } elseif ($pcode = PromoCodes::where('coupon_code', $referral_code, 'state', '!=', 1)->first()) {
      $response_array = array('success' => false, 'error' => 'Invalid promo code', 'error_code' => 485);
      } else {
      $response_array = array('success' => false, 'error' => 'Invalid referral code', 'error_code' => 455);
      }
      } else {
      $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
      }
      } else {
      if ($is_admin) {
      // $var = Keywords::where('id', 2)->first();
      // $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID is not Found', 'error_code' => 410);
      $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID is not Found', 'error_code' => 410);
      } else {
      $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
      }
      }
      }
      $response_code = 200;
      $response = Response::json($response_array, $response_code);
      return $response;
      } */


    public function apply_referral_code()
    {
        $referral_code = Input::get('referral_code');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $is_skip = Input::get('is_skip');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'is_skip' => $is_skip,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'is_skip' => 'required',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if ($is_skip != 1) {
                        if ($ledger = Ledger::where('referral_code', $referral_code)->first()) {
                            $referred_by = $ledger->owner_id;
                            if ($referred_by != $owner_id) {
                                if ($owner_data->is_referee) {
                                    $owner = Owner::find($owner_id);
                                    $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                    $response_array = array(
                                        'success' => false,
                                        'error' => "Sorry, You have apready apply the refereel code.",
                                        'error_code' => 405,
                                        'id' => $owner->id,
                                        'first_name' => $owner->first_name,
                                        'last_name' => $owner->last_name,
                                        'phone' => $owner->phone,
                                        'email' => $owner->email,
                                        'picture' => $owner->picture,
                                        'bio' => '',
                                        'address' => '',
                                        'state' => '',
                                        'country' => '',
                                        'zipcode' => '',
                                        'login_by' => $owner->login_by,
                                        'social_unique_id' => $owner->social_unique_id,
                                        'device_token' => $owner->device_token,
                                        'device_type' => $owner->device_type,
                                        'token' => $owner->token,
                                        'referral_code' => $code_data->referral_code,
                                        'is_referee' => $owner->is_referee,
                                        'promo_count' => $owner->promo_count,
                                    );
                                    $response_code = 200;
                                } else {
                                    $settings = Settings::where('key', 'default_referral_bonus_to_refered_user')->first();
                                    $refered_user = $settings->value;

                                    $settings = Settings::where('key', 'default_referral_bonus_to_refereel')->first();
                                    $referral = $settings->value;

                                    $ledger = Ledger::find($ledger->id);
                                    $ledger->total_referrals = $ledger->total_referrals + 1;
                                    $ledger->amount_earned = $ledger->amount_earned + $refered_user;
                                    $ledger->save();

                                    $ledger1 = Ledger::where('owner_id', $owner_id)->first();
                                    $ledger1 = Ledger::find($ledger1->id);
                                    $ledger1->amount_earned = $ledger1->amount_earned + $referral;
                                    $ledger1->save();

                                    $owner = Owner::find($owner_id);
                                    $owner->referred_by = $ledger->owner_id;
                                    $owner->is_referee = 1;
                                    $owner->save();
                                    $owner = Owner::find($owner_id);
                                    $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                    $response_array = array(
                                        'success' => true,
                                        'error' => 'Referral process successfully completed.',
                                        'id' => $owner->id,
                                        'first_name' => $owner->first_name,
                                        'last_name' => $owner->last_name,
                                        'phone' => $owner->phone,
                                        'email' => $owner->email,
                                        'picture' => $owner->picture,
                                        'bio' => '',
                                        'address' => '',
                                        'state' => '',
                                        'country' => '',
                                        'zipcode' => '',
                                        'login_by' => $owner->login_by,
                                        'social_unique_id' => $owner->social_unique_id,
                                        'device_token' => $owner->device_token,
                                        'device_type' => $owner->device_type,
                                        'token' => $owner->token,
                                        'referral_code' => $code_data->referral_code,
                                        'is_referee' => $owner->is_referee,
                                        'promo_count' => $owner->promo_count,
                                    );
                                    $response_code = 200;
                                }
                            } else {
                                $owner = Owner::find($owner_id);
                                $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                $response_array = array(
                                    'success' => false,
                                    'error' => "Sorry, You can't apply your refereel code.",
                                    'error_code' => 405,
                                    'id' => $owner->id,
                                    'first_name' => $owner->first_name,
                                    'last_name' => $owner->last_name,
                                    'phone' => $owner->phone,
                                    'email' => $owner->email,
                                    'picture' => $owner->picture,
                                    'bio' => '',
                                    'address' => '',
                                    'state' => '',
                                    'country' => '',
                                    'zipcode' => '',
                                    'login_by' => $owner->login_by,
                                    'social_unique_id' => $owner->social_unique_id,
                                    'device_token' => $owner->device_token,
                                    'device_type' => $owner->device_type,
                                    'token' => $owner->token,
                                    'referral_code' => $code_data->referral_code,
                                    'is_referee' => $owner->is_referee,
                                    'promo_count' => $owner->promo_count,
                                );
                                $response_code = 200;
                            }
                        } else {
                            $owner = Owner::find($owner_id);
                            $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                            $response_array = array(
                                'success' => false,
                                'error' => 'Invalid referral code',
                                'error_code' => 405,
                                'id' => $owner->id,
                                'first_name' => $owner->first_name,
                                'last_name' => $owner->last_name,
                                'phone' => $owner->phone,
                                'email' => $owner->email,
                                'picture' => $owner->picture,
                                'bio' => '',
                                'address' => $owner->address,
                                'state' => '',
                                'country' => '',
                                'zipcode' => '',
                                'login_by' => $owner->login_by,
                                'social_unique_id' => $owner->social_unique_id,
                                'device_token' => $owner->device_token,
                                'device_type' => $owner->device_type,
                                'token' => $owner->token,
                                'referral_code' => $code_data->referral_code,
                                'is_referee' => $owner->is_referee,
                                'promo_count' => $owner->promo_count,
                            );
                            $response_code = 200;
                        }
                    } else {
                        $owner = Owner::find($owner_id);
                        $owner->is_referee = 1;
                        $owner->save();
                        $owner = Owner::find($owner_id);
                        $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                        $response_array = array(
                            'success' => true,
                            'error' => 'You have skipped for referral process',
                            'id' => $owner->id,
                            'first_name' => $owner->first_name,
                            'last_name' => $owner->last_name,
                            'phone' => $owner->phone,
                            'email' => $owner->email,
                            'picture' => $owner->picture,
                            'bio' => '',
                            'address' => $owner->address,
                            'state' => '',
                            'country' => '',
                            'zipcode' => '',
                            'login_by' => $owner->login_by,
                            'social_unique_id' => $owner->social_unique_id,
                            'device_token' => $owner->device_token,
                            'device_type' => $owner->device_type,
                            'token' => $owner->token,
                            'referral_code' => $code_data->referral_code,
                            'is_referee' => $owner->is_referee,
                            'promo_count' => $owner->promo_count,
                        );
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Owner ID is not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function apply_promo_code()
    {
        $promo_code = strtoupper(Input::get('promo_code'));
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $request_id = 0;
            $is_apply_on_trip = 0;
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $request = Requests::where('owner_id', '=', $owner_id)->where('status', '=', 1)->where('is_completed', '=', 0)->where('is_cancelled', '=', 0)->orderBy('created_at', 'desc')->first();
                    if ($request) {
                        if (isset($request->id)) {
                            if ($request->promo_id) {
                                $response_array = array('success' => FALSE, 'error' => 'You can not apply multiple code for single trip.', 'error_code' => 411);
                                $response_code = 200;
                            } else {
                                $settings = Settings::where('key', 'promotional_code_activation')->first();
                                $prom_act = $settings->value;
                                if ($prom_act) {
                                    if ($request->payment_mode == 0) {
                                        $settings = Settings::where('key', 'get_promotional_profit_on_card_payment')->first();
                                        $prom_act_card = $settings->value;
                                        if ($prom_act_card) {
                                            /* if ($ledger = PromotionalCodes::where('promo_code', $promo_code)->first()) { */
                                            if ($promos = PromoCodes::where('coupon_code', $promo_code)->where('uses', '>', 0)->where('state', '=', 1)->first()) {
                                                $owner = Owner::find($owner_id);
                                                $ownerCreatedAt = $owner->created_at;
                                                if ((date("Y-m-d H:i:s", strtotime(trim($ownerCreatedAt))) >= date("Y-m-d H:i:s", strtotime(trim($promos->expiry)))) || (date("Y-m-d H:i:s", strtotime(trim($ownerCreatedAt))) <= date("Y-m-d H:i:s", strtotime(trim($promos->start_date))))) {
                                                    $response_array = array('success' => FALSE, 'error' => 'Promotional code not valid for old user.', 'error_code' => 513);
                                                    $response_code = 200;
                                                    Log::info('Promotional code not valid for old user');
                                                    return Response::json($response_array, $response_code);
                                                }
                                                if ((date("Y-m-d H:i:s") >= date("Y-m-d H:i:s", strtotime(trim($promos->expiry)))) || (date("Y-m-d H:i:s") <= date("Y-m-d H:i:s", strtotime(trim($promos->start_date))))) {
                                                    $response_array = array('success' => FALSE, 'error' => 'Promotional code is not available.', 'error_code' => 505);
                                                    $response_code = 200;
                                                } else {
                                                    /* echo $promos->id;
                                                      echo $owner_id;
                                                      $promo_is_used = 0; */
                                                    $promo_is_used = UserPromoUse::where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count();
                                                    /* $promo_is_used = DB::table('user_promo_used')->where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count(); */
                                                    if ($promo_is_used) {
                                                        $response_array = array('success' => FALSE, 'error' => 'Promotional code already used.', 'error_code' => 512);
                                                        $response_code = 200;
                                                    } else {
                                                        $promo_update_counter = PromoCodes::find($promos->id);
                                                        $promo_update_counter->uses = $promo_update_counter->uses - 1;
                                                        $promo_update_counter->save();

                                                        $user_promo_entry = new UserPromoUse;
                                                        $user_promo_entry->code_id = $promos->id;
                                                        $user_promo_entry->user_id = $owner_id;
                                                        $user_promo_entry->save();

                                                        $owner = Owner::find($owner_id);
                                                        $owner->promo_count = $owner->promo_count + 1;
                                                        $owner->save();

                                                        $request = Requests::find($request->id);
                                                        $request->promo_id = $promos->id;
                                                        $request->promo_code = $promos->coupon_code;
                                                        $request->save();

                                                        $owner = Owner::find($owner_id);
                                                        $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                                        $response_array = array(
                                                            'success' => true,
                                                            'error' => 'Promotional code successfully applied.',
                                                            'id' => $owner->id,
                                                            'first_name' => $owner->first_name,
                                                            'last_name' => $owner->last_name,
                                                            'phone' => $owner->phone,
                                                            'email' => $owner->email,
                                                            'picture' => $owner->picture,
                                                            'bio' => '',
                                                            'address' => $owner->address,
                                                            'state' => '',
                                                            'country' => '',
                                                            'zipcode' => '',
                                                            'login_by' => $owner->login_by,
                                                            'social_unique_id' => $owner->social_unique_id,
                                                            'device_token' => $owner->device_token,
                                                            'device_type' => $owner->device_type,
                                                            'token' => $owner->token,
                                                            'referral_code' => $code_data->referral_code,
                                                            'is_referee' => $owner->is_referee,
                                                            'promo_count' => $owner->promo_count,
                                                            'request_id' => $request->id,
                                                        );
                                                        $response_code = 200;
                                                    }
                                                }
                                            } else {
                                                $response_array = array('success' => FALSE, 'error' => 'Promotional code is not available.', 'error_code' => 505);
                                                $response_code = 200;
                                            }
                                        } else {
                                            $response_array = array('success' => FALSE, 'error' => 'Promotion feature is not active on card payment.', 'error_code' => 505);
                                            $response_code = 200;
                                        }
                                    } else if ($request->payment_mode == 1) {
                                        $settings = Settings::where('key', 'get_promotional_profit_on_cash_payment')->first();
                                        $prom_act_cash = $settings->value;
                                        if ($prom_act_cash) {
                                            /* if ($ledger = PromotionalCodes::where('promo_code', $promo_code)->first()) { */
                                            if ($promos = PromoCodes::where('coupon_code', $promo_code)->where('uses', '>', 0)->where('state', '=', 1)->first()) {
                                                $owner = Owner::find($owner_id);
                                                if ($promos->coupon_code = 'MTW5' || $promos->coupon_code = 'MTW6') {
                                                    if ((date("Y-m-d H:i:s") >= date("Y-m-d H:i:s", strtotime(trim($promos->expiry)))) || (date("Y-m-d H:i:s") <= date("Y-m-d H:i:s", strtotime(trim($promos->start_date))))) {
                                                        $response_array = array('success' => FALSE, 'error' => 'Promotional code is not available', 'error_code' => 505);
                                                        $response_code = 200;
                                                    } else {
                                                        $promo_is_used = UserPromoUse::where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count();
                                                        /* $promo_is_used = DB::table('user_promo_used')->where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count(); */
                                                        $promo_update_counter = PromoCodes::find($promos->id);
                                                        $promo_update_counter->uses = $promo_update_counter->uses - 1;
                                                        $promo_update_counter->save();

                                                        $user_promo_entry = new UserPromoUse;
                                                        $user_promo_entry->code_id = $promos->id;
                                                        $user_promo_entry->user_id = $owner_id;
                                                        $user_promo_entry->save();

                                                        $owner = Owner::find($owner_id);
                                                        $owner->promo_count = $owner->promo_count + 1;
                                                        $owner->save();

                                                        $request = Requests::find($request->id);
                                                        $request->promo_id = $promos->id;
                                                        $request->promo_code = $promos->coupon_code;
                                                        $request->save();

                                                        $owner = Owner::find($owner_id);
                                                        $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                                        $response_array = array(
                                                            'success' => true,
                                                            'error' => 'Promotional code successfully applied.',
                                                            'id' => $owner->id,
                                                            'first_name' => $owner->first_name,
                                                            'last_name' => $owner->last_name,
                                                            'phone' => $owner->phone,
                                                            'email' => $owner->email,
                                                            'picture' => $owner->picture,
                                                            'bio' => '',
                                                            'address' => '',
                                                            'state' => '',
                                                            'country' => '',
                                                            'zipcode' => '',
                                                            'login_by' => $owner->login_by,
                                                            'social_unique_id' => $owner->social_unique_id,
                                                            'device_token' => $owner->device_token,
                                                            'device_type' => $owner->device_type,
                                                            'token' => $owner->token,
                                                            'referral_code' => $code_data->referral_code,
                                                            'is_referee' => $owner->is_referee,
                                                            'promo_count' => $owner->promo_count,
                                                            'request_id' => $request->id,
                                                        );
                                                        $response_code = 200;
                                                    }
                                                } else {
                                                    $ownerCreatedAt = $owner->created_at;
                                                    if ((date("Y-m-d H:i:s", strtotime(trim($ownerCreatedAt))) >= date("Y-m-d H:i:s", strtotime(trim($promos->expiry)))) || (date("Y-m-d H:i:s", strtotime(trim($ownerCreatedAt))) <= date("Y-m-d H:i:s", strtotime(trim($promos->start_date))))) {
                                                        $response_array = array('success' => FALSE, 'error' => 'Promotional code not valid for old user.', 'error_code' => 513);
                                                        $response_code = 200;
                                                        Log::info('Promotional code not valid for old user');
                                                        return Response::json($response_array, $response_code);
                                                    }
                                                    if ((date("Y-m-d H:i:s") >= date("Y-m-d H:i:s", strtotime(trim($promos->expiry)))) || (date("Y-m-d H:i:s") <= date("Y-m-d H:i:s", strtotime(trim($promos->start_date))))) {
                                                        $response_array = array('success' => FALSE, 'error' => 'Promotional code is not available', 'error_code' => 505);
                                                        $response_code = 200;
                                                    } else {
                                                        /* echo $promos->id;
                                                          echo $owner_id;
                                                          $promo_is_used = 0; */
                                                        $promo_is_used = UserPromoUse::where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count();
                                                        /* $promo_is_used = DB::table('user_promo_used')->where('user_id', '=', $owner_id)->where('code_id', '=', $promos->id)->count(); */
                                                        if ($promo_is_used) {
                                                            $response_array = array('success' => FALSE, 'error' => 'Promotional code already used.', 'error_code' => 512);
                                                            $response_code = 200;
                                                        } else {
                                                            $promo_update_counter = PromoCodes::find($promos->id);
                                                            $promo_update_counter->uses = $promo_update_counter->uses - 1;
                                                            $promo_update_counter->save();

                                                            $user_promo_entry = new UserPromoUse;
                                                            $user_promo_entry->code_id = $promos->id;
                                                            $user_promo_entry->user_id = $owner_id;
                                                            $user_promo_entry->save();

                                                            $owner = Owner::find($owner_id);
                                                            $owner->promo_count = $owner->promo_count + 1;
                                                            $owner->save();

                                                            $request = Requests::find($request->id);
                                                            $request->promo_id = $promos->id;
                                                            $request->promo_code = $promos->coupon_code;
                                                            $request->save();

                                                            $owner = Owner::find($owner_id);
                                                            $code_data = Ledger::where('owner_id', '=', $owner->id)->first();
                                                            $response_array = array(
                                                                'success' => true,
                                                                'error' => 'Promotional code successfully applied.',
                                                                'id' => $owner->id,
                                                                'first_name' => $owner->first_name,
                                                                'last_name' => $owner->last_name,
                                                                'phone' => $owner->phone,
                                                                'email' => $owner->email,
                                                                'picture' => $owner->picture,
                                                                'bio' => '',
                                                                'address' => '',
                                                                'state' => '',
                                                                'country' => '',
                                                                'zipcode' => '',
                                                                'login_by' => $owner->login_by,
                                                                'social_unique_id' => $owner->social_unique_id,
                                                                'device_token' => $owner->device_token,
                                                                'device_type' => $owner->device_type,
                                                                'token' => $owner->token,
                                                                'referral_code' => $code_data->referral_code,
                                                                'is_referee' => $owner->is_referee,
                                                                'promo_count' => $owner->promo_count,
                                                                'request_id' => $request->id,
                                                            );
                                                            $response_code = 200;
                                                        }
                                                    }
                                                }
                                            } else {
                                                $response_array = array('success' => FALSE, 'error' => 'Promotional code is not available', 'error_code' => 505);
                                                $response_code = 200;
                                            }
                                        } else {
                                            $response_array = array('success' => FALSE, 'error' => 'Promotion feature is not active on cash payment.', 'error_code' => 505);
                                            $response_code = 200;
                                        }
                                    } else {
                                        $response_array = array('success' => FALSE, 'error' => 'Payment mode is paypal', 'error_code' => 505);
                                        $response_code = 200;
                                    }
                                } else {
                                    $response_array = array('success' => FALSE, 'error' => 'Promotion feature is not active.', 'error_code' => 505);
                                    $response_code = 200;
                                }
                            }
                        } else {
                            $response_array = array('success' => FALSE, 'error' => 'You can\'t apply promotional code without creating request.', 'error_code' => 506);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => FALSE, 'error' => 'You can\'t apply promotional code without creating request.', 'error_code' => 506);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Owner ID is not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // test
    public function register()
    {

        $first_name = ucwords(trim(Input::get('first_name')));
        $last_name = ucwords(trim(Input::get('last_name')));
        $email = Input::get('email');
        $phone = Input::get('phone');
        $password = Input::get('password');
        $picture = "";
        if (Input::hasfile('picture')) {
            $picture = Input::file('picture');
        }

        $device_token = Input::get('device_token');
        $device_type = Input::get('device_type');
        $bio = "";
        if (Input::has('bio')) {
            $bio = Input::get('bio');
        }
        $address = "";
        if (Input::has('address')) {
            $address = ucwords(trim(Input::get('address')));
        }
        $state = "";
        if (Input::has('state')) {
            $state = ucwords(trim(Input::get('state')));
        }
        $country = "";
        if (Input::has('country')) {
            $country = ucwords(trim(Input::get('country')));
        }
        $zipcode = "";
        if (Input::has('zipcode')) {
            $zipcode = Input::get('zipcode');
        }
        $login_by = Input::get('login_by');
        $social_unique_id = Input::get('social_unique_id');

        if ($password != "" and $social_unique_id == "") {
            $validator = Validator::make(
                array(
                    'password' => $password,
                    'email' => $email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'picture' => $picture,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'zipcode' => $zipcode,
                    'login_by' => $login_by
                ), array(
                    'password' => 'required',
                    'email' => 'required|email',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'picture' => 'mimes:jpeg,bmp,png',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'zipcode' => 'integer',
                    'login_by' => 'required|in:manual,facebook,google',
                )
            );

            $validatorPhone = Validator::make(
                array(
                    'phone' => $phone,
                ), array(
                    'phone' => 'phone'
                )
            );
            $social = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                ), array(
                    'social_unique_id' => 'unique:owner'
                )
            );
        } elseif ($social_unique_id != "" and $password == "") {
            $validator = Validator::make(
                array(
                    'email' => $email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'picture' => $picture,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'zipcode' => $zipcode,
                    'login_by' => $login_by,
                    /*  'social_unique_id' => $social_unique_id*/
                ), array(
                    'email' => 'required|email',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'picture' => 'mimes:jpeg,bmp,png',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'zipcode' => 'integer',
                    'login_by' => 'required|in:manual,facebook,google',
                    /*   'social_unique_id' => 'required|unique:owner'*/
                )
            );

            $social = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                ), array(
                    'social_unique_id' => 'required|unique:owner'
                )
            );


            $validatorPhone = Validator::make(
                array(
                    'phone' => $phone,
                ), array(
                    'phone' => 'phone'
                )
            );
        } elseif ($social_unique_id != "" and $password != "") {
            $response_array = array('success' => false, 'error' => 'Invalid Input - either social_unique_id or password should be passed', 'error_code' => 401);
            $response_code = 200;
            goto response;
        }

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();

            Log::info('Error while during owner registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else if ($validatorPhone->fails()) {
            $error_messages = $validatorPhone->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Phone Number', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else if ($social->fails()) {
            $error_messages = $social->messages()->all();
            $response_array = array('success' => false, 'error' => 'The social unique id has already been taken.', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            if (Owner::where('email', '=', $email)->first()) {
                $response_array = array('success' => false, 'error' => 'Email ID already Registred', 'error_code' => 402);
                $response_code = 200;
            } else {
                /* SEND REFERRAL & PROMO INFO */
                $settings = Settings::where('key', 'referral_code_activation')->first();
                $referral_code_activation = $settings->value;
                if ($referral_code_activation) {
                    $referral_code_activation_txt = "referral on";
                } else {
                    $referral_code_activation_txt = "referral off";
                }

                $settings = Settings::where('key', 'promotional_code_activation')->first();
                $promotional_code_activation = $settings->value;
                if ($promotional_code_activation) {
                    $promotional_code_activation_txt = "promo on";
                } else {
                    $promotional_code_activation_txt = "promo off";
                }

                /* SEND REFERRAL & PROMO INFO */
                $owner = new Owner;
                $owner->first_name = $first_name;
                $owner->last_name = $last_name;
                $owner->email = $email;
                $owner->phone = $phone;
                if ($password != "") {
                    $owner->password = Hash::make($password);
                }
                $owner->token = generate_token();
                $owner->token_expiry = generate_expiry();

                // upload image
                $file_name = time();
                $file_name .= rand();
                $file_name = sha1($file_name);
                if ($picture) {
                    $ext = Input::file('picture')->getClientOriginalExtension();
                    Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;

                    // Upload to S3
                    if (Config::get('app.s3_bucket') != "") {
                        $s3 = App::make('aws')->get('s3');
                        $pic = $s3->putObject(array(
                            'Bucket' => Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                        ));

                        $s3->putObjectAcl(array(
                            'Bucket' => Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'ACL' => 'public-read'
                        ));

                        $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                    } else {
                        $s3_url = asset_url() . '/uploads/' . $local_url;
                    }
                    $owner->picture = $s3_url;
                }
                $owner->device_token = $device_token;
                $owner->device_type = $device_type;
                //$owner->zipcode = "0";
                //if (Input::has('zipcode'))
                // $owner->zipcode = $zipcode;
                $password = my_random6_number();
                if ($social_unique_id != "") {
                    $owner->social_unique_id = $social_unique_id;
                    $owner->password = Hash::make($password);
                }
                $owner->timezone = 'UTC';
                If (Input::has('timezone')) {
                    $owner->timezone = Input::get('timezone');
                }
                $owner->is_referee = 0;
                $owner->promo_count = 0;
                $owner->save();
                $data = array('unique_id' => "26", 'message_type' => "Promo Free");
                $message = 'Enter promo code FREE and get free credit upto 30 SAR for your first ride with Kasper Cab, Offer expires on 17-May-2017.';
                send_notifications($owner->id, "owner", $message, $data);
                //send_notifications($owner->id,$title,$message,"owner");
                if ($social_unique_id != "") {
                    $pattern = "Hello... ! " . ucwords($first_name) . " . Your " . Config::get('app.website_title') . " Web Login Password is : " . $password;
                    sms_notification($owner->id, 'owner', $pattern);
                    $subject = "Your " . Config::get('app.website_title') . " Web Login Password";
                    email_notification($owner->id, 'owner', $pattern, $subject);
                }

                /* $zero_in_code = Config::get('app.referral_zero_len') - strlen($owner->id);
                  $referral_code = Config::get('app.referral_prefix');
                  for ($i = 0; $i < $zero_in_code; $i++) {
                  $referral_code .= "0";
                  }
                  $referral_code .= $owner->id; */
                regenerate:
                $referral_code = my_random6_number();
                if (Ledger::where('referral_code', $referral_code)->count()) {
                    goto regenerate;
                }
                /* Referral entry */
                $ledger = new Ledger;
                $ledger->owner_id = $owner->id;
                $ledger->referral_code = $referral_code;
                $ledger->save();
                /* Referral entry end */

                // send email
                /* $subject = "Welcome On Board";
                  $email_data['name'] = $owner->first_name;

                  send_email($owner->id, 'owner', $email_data, $subject, 'userregister'); */

                if ($owner->picture == NULL) {
                    $owner_picture = "";
                } else {
                    $owner_picture = $owner->picture;
                }
                if ($owner->timezone == NULL) {
                    $owner_time = Config::get('app.timezone');
                } else {
                    $owner_time = $owner->timezone;
                }

                $settings = Settings::where('key', 'admin_email_address')->first();
                $admin_email = $settings->value;
                $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($owner->first_name . " " . $owner->last_name), 'web_url' => web_url());
                $subject = "Welcome to " . ucwords(Config::get('app.website_title')) . ", " . ucwords($owner->first_name . " " . $owner->last_name) . "";
                email_notification($owner->id, 'owner', $pattern, $subject, 'user_register', null);

                $response_array = array(
                    'success' => true,
                    'id' => $owner->id,
                    'first_name' => $owner->first_name,
                    'last_name' => $owner->last_name,
                    'phone' => $owner->phone,
                    'email' => $owner->email,
                    'picture' => $owner_picture,
                    'bio' => '',
                    'address' => '',
                    'state' => '',
                    'country' => '',
                    //'zipcode' => $owner_zipcode,
                    'login_by' => $owner->login_by,
                    'social_unique_id' => $owner->social_unique_id ? $owner->social_unique_id : "",
                    'device_token' => $owner->device_token,
                    'device_type' => $owner->device_type,
                    'timezone' => $owner_time,
                    'token' => $owner->token,
                    'referral_code' => $referral_code,
                    'is_referee' => $owner->is_referee,
                    'promo_count' => $owner->promo_count,
                    'is_referral_active' => $referral_code_activation,
                    'is_referral_active_txt' => $referral_code_activation_txt,
                    'is_promo_active' => $promotional_code_activation,
                    'is_promo_active_txt' => $promotional_code_activation_txt,
                );

                $response_code = 200;
            }
        }

        response:
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function login()
    {
        $login_by = Input::get('login_by');
        $device_token = Input::get('device_token');
        $device_type = Input::get('device_type');

        if (Input::has('email') && Input::has('password')) {
            $email = Input::get('email');
            $password = Input::get('password');
            $validator = Validator::make(
                array(
                    'password' => $password,
                    'email' => $email,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'password' => 'required',
                    'email' => 'required|email',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google'
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                Log::error('Validation error during manual login for owner = ' . print_r($error_messages, true));
            } else {
                if ($owner = Owner::where('email', '=', $email)->first()) {
                    if (Hash::check($password, $owner->password)) {
                        if ($login_by !== "manual") {
                            $response_array = array('success' => false, 'error' => 'Login by mismatch', 'error_code' => 417);
                            $response_code = 200;
                        } else {
                            if ($owner->device_type != $device_type) {
                                $owner->device_type = $device_type;
                            }
                            if ($owner->device_token != $device_token) {
                                $owner->device_token = $device_token;
                            }
                            $owner->token = generate_token();
                            $owner->token_expiry = generate_expiry();
                            $owner->save();
                            /* SEND REFERRAL & PROMO INFO */
                            $settings = Settings::where('key', 'referral_code_activation')->first();
                            $referral_code_activation = $settings->value;
                            if ($referral_code_activation) {
                                $referral_code_activation_txt = "referral on";
                            } else {
                                $referral_code_activation_txt = "referral off";
                            }

                            $settings = Settings::where('key', 'promotional_code_activation')->first();
                            $promotional_code_activation = $settings->value;
                            if ($promotional_code_activation) {
                                $promotional_code_activation_txt = "promo on";
                            } else {
                                $promotional_code_activation_txt = "promo off";
                            }
                            /* SEND REFERRAL & PROMO INFO */
                            $code_data = Ledger::where('owner_id', '=', $owner->id)->first();


                            $monthly = Monthly::where('owner_id', $owner->id)->orderBy('id', 'DESC')->first();
                            //echo "<pre>";print_r($monthly);echo "</pre>";die;

                            $response_array = array(
                                'success' => true,
                                'id' => $owner->id,
                                'first_name' => $owner->first_name,
                                'last_name' => $owner->last_name,
                                'phone' => $owner->phone,
                                'email' => $owner->email,
                                'credit' => $owner->credit,
                                'use_credit_first' => $owner->use_credit_first,
                                'payment_mode' => $owner->payment_mode,
                                'picture' => $owner->picture,
                                'bio' => '',
                                'address' => '',
                                'state' => '',
                                'country' => '',
                                'zipcode' => '',
                                'login_by' => $owner->login_by,
                                'social_unique_id' => $owner->social_unique_id,
                                'device_token' => $owner->device_token,
                                'device_type' => $owner->device_type,
                                'timezone' => $owner->timezone,
                                'token' => $owner->token,
                                'referral_code' => $code_data->referral_code,
                                'is_referee' => $owner->is_referee,
                                'promo_count' => $owner->promo_count,
                                'is_referral_active' => $referral_code_activation,
                                'is_referral_active_txt' => $referral_code_activation_txt,
                                'is_promo_active' => $promotional_code_activation,
                                'is_promo_active_txt' => $promotional_code_activation_txt,
                                'monthly_id' => (!empty($monthly->id) ? $monthly->id : ''),
                                'new_request_status' => (!empty($monthly->status) ? (string)$monthly->status : '')
                            );

                            $dog = Dog::find($owner->dog_id);
                            if ($dog !== NULL) {
                                $response_array = array_merge($response_array, array(
                                    'dog_id' => $dog->id,
                                    'age' => $dog->age,
                                    'name' => $dog->name,
                                    'breed' => $dog->breed,
                                    'likes' => $dog->likes,
                                    'image_url' => $dog->image_url,
                                ));
                            }

                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid Username and Password', 'error_code' => 403);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a Registered User', 'error_code' => 404);
                    $response_code = 200;
                }
            }
        } elseif (Input::has('social_unique_id')) {
            $social_unique_id = Input::get('social_unique_id');
            $socialValidator = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'social_unique_id' => 'required|exists:owner,social_unique_id',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google'
                )
            );

            $socialid = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id

                ), array(
                    'social_unique_id' => 'required|exists:owner,social_unique_id',

                )
            );

            if ($socialValidator->fails()) {
                $error_messages = $socialValidator->messages();
                Log::error('Validation error during social login for owner = ' . print_r($error_messages, true));
                $error_messages = $socialValidator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } elseif ($socialid->fails()) {
                $error_messages = $socialid->messages();
                Log::error('Validation error during social login for owner = ' . print_r($error_messages, true));
                $error_messages = $socialid->messages()->all();
                $response_array = array('success' => false, 'error' => 'The social unique id has already been taken.', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;

            } else {
                if ($owner = Owner::where('social_unique_id', '=', $social_unique_id)->first()) {
                    if (!in_array($login_by, array('facebook', 'google'))) {
                        $response_array = array('success' => false, 'error' => 'Login by mismatch', 'error_code' => 417);
                        $response_code = 200;
                    } else {
                        if ($owner->device_type != $device_type) {
                            $owner->device_type = $device_type;
                        }
                        if ($owner->device_token != $device_token) {
                            $owner->device_token = $device_token;
                        }
                        $owner->token_expiry = generate_expiry();
                        $owner->save();

                        $response_array = array(
                            'success' => true,
                            'id' => $owner->id,
                            'first_name' => $owner->first_name,
                            'last_name' => $owner->last_name,
                            'phone' => $owner->phone,
                            'email' => $owner->email,
                            'credit' => $owner->credit,
                            'use_credit_first' => $owner->use_credit_first,
                            'payment_mode' => $owner->payment_mode,
                            'picture' => $owner->picture,
                            'bio' => '',
                            'address' => '',
                            'state' => '',
                            'country' => '',
                            'zipcode' => '',
                            'login_by' => $owner->login_by,
                            'social_unique_id' => $owner->social_unique_id,
                            'device_token' => $owner->device_token,
                            'device_type' => $owner->device_type,
                            'timezone' => $owner->timezone,
                            'token' => $owner->token,
                        );

                        $dog = Dog::find($owner->dog_id);
                        if ($dog !== NULL) {
                            $response_array = array_merge($response_array, array(
                                'dog_id' => $dog->id,
                                'age' => $dog->age,
                                'name' => $dog->name,
                                'breed' => $dog->breed,
                                'likes' => $dog->likes,
                                'image_url' => $dog->image_url,
                            ));
                        }

                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid social registration User', 'error_code' => 404);
                    $response_code = 200;
                }
            }
        } else {
            $response_array = array('success' => false, 'error' => 'Invalid input', 'error_code' => 404);
            $response_code = 200;
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function details()
    {
        if (Request::isMethod('post')) {
            $address = Input::get('address');
            $state = Input::get('state');
            $zipcode = Input::get('zipcode');
            $token = Input::get('token');
            $owner_id = Input::get('id');

            $validator = Validator::make(
                array(
                    'address' => $address,
                    'state' => $state,
                    'token' => $token,
                    'owner_id' => $owner_id,
                ), array(
                    'address' => 'required',
                    'state' => 'required',
                    'zipcode' => 'required|integer',
                    'token' => 'required',
                    'owner_id' => 'required|integer'
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($owner_data->token_expiry) || $is_admin) {
                        // Do necessary operations

//                        $owner = Owner::find($owner_data->id);
//                        $owner->address = $address;
//                        $owner->state = $state;
//                        $owner->zipcode = $zipcode;
//                        $owner->save();

                        $response_array = array('success' => true);
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 2)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        } else {
            //handles get request
            $token = Input::get('token');
            $owner_id = Input::get('id');
            $validator = Validator::make(
                array(
                    'token' => $token,
                    'owner_id' => $owner_id,
                ), array(
                    'token' => 'required',
                    'owner_id' => 'required|integer'
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {

                $is_admin = $this->isAdmin($token);
                if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($owner_data->token_expiry) || $is_admin) {

                        $response_array = array(
                            'success' => true,
                            'address' => $owner_data->address,
                            'state' => $owner_data->state,
                            'zipcode' => $owner_data->zipcode,
                        );
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 2)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function update_credit()
    {
        //handles get request
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $amount = Input::get('amount');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'amount' => $amount,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'amount' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $owner = Owner::find($owner_id);
                    $owner->credit = $owner->credit + $amount;
                    $owner->save();
                    $owner = Owner::find($owner_id);

                    $response_array = array(
                        'success' => true,
                        'credit' => $owner->credit,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function update_use_credit_first()
    {
        //handles get request
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $use_credit_first = Input::get('use_credit_first');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'use_credit_first' => $use_credit_first,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'use_credit_first' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $owner = Owner::find($owner_id);
                    $owner->use_credit_first = $use_credit_first;
                    $owner->save();
                    $owner = Owner::find($owner_id);

                    $response_array = array(
                        'success' => true,
                        'use_credit_first' => $owner->use_credit_first,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function update_payment_mode()
    {
        //handles get request
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $payment_mode = Input::get('payment_mode');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'payment_mode' => $payment_mode,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'payment_mode' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                if ($owner_data->email != "payfort@kasper-cab.com" && $owner_data->email != "fahed@kc.com" && $owner_data->email != "umar@kasper-cab.com" && $owner_data->email != "zeeshan@kasper-cab.com") {
                    $response_array = array('success' => false, 'error' => 'Card Payments are not allowed Yet. Please use cash.', 'error_code' => 411);
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                }
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $owner = Owner::find($owner_id);
                    $owner->payment_mode = $payment_mode;
                    $owner->save();
                    $request = Requests::where('status', '=', 1)->where('service_type', '!=', 2)->where('is_completed', '=', 0)->where('is_cancelled', '=', 0)->where('owner_id', '=', $owner_id)->where('confirmed_walker', '!=', 0)->orderBy('created_at', 'desc')->first();
                    if (isset($request)) {
                        $request->payment_mode = $owner->payment_mode;
                        $request->save();
                    }
                    $owner = Owner::find($owner_id);

                    $response_array = array(
                        'success' => true,
                        'payment_mode' => $owner->payment_mode,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_credit()
    {
        //handles get request
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {

                    $response_array = array(
                        'success' => true,
                        'credit' => $owner_data->credit,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function confirm_cash_amount()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $request_id = Input::get('request_id');
        $accepted = Input::get('accepted');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'request_id' => $request_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                if (is_token_active($owner_data->token_expiry)) {
                    $request = Requests::where('id', $request_id)->first();

                    if (ISSET($request)) {
                        if ($request->owner_id = $owner_data->id) {
                            if ($accepted == 1) {
                                Requests::where('id', '=', $request_id)->update(array('is_invoice_verified_user' => 1));
                                $owner = Owner::find($request->owner_id);
                                $owner->credit = $owner->credit + $request->added_to_credit_user - $request->used_credit_user;
                                $owner->save();
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Amount Confirmed Successfully',
                                    'request_id' => $request_id,
                                    'unique_id' => 27
                                );
                                $title = 'Amount Confirmed Successfully';
                                $message = $response_array;

                                send_notifications($request->confirmed_walker, "walker", $title, $message);

                                // Send SMS and EMAIL
                                $actual_total = 0;
                                $price_per_unit_distance = 0;
                                $price_per_unit_time = 0;
                                $base_price = 0;
                                $walker = Walker::find($request->confirmed_walker);
                                $owner = Owner::find($request->owner_id);
                                $requestserv = RequestServices::where('request_id', $request->id)->first();
                                $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();
                                $providertype = ProviderType::where('id', $walker->type)->first();
                                $bill = array();
                                /* $currency_selected = Keywords::find(5); */
                                if ($request->is_completed == 1) {
                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    $bill['payment_mode'] = $request->payment_mode;
                                    $bill['distance'] = (string)$request->distance;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $bill['unit'] = $unit_set;
                                    $bill['time'] = floatval(sprintf2($request->time, 2));
                                    $bill['base_distance'] = $request_typ->base_distance;
                                    $bill['base_price'] = $request_typ->base_price;
                                    $bill['price_per_unit_distance'] = $request_typ->price_per_unit_distance;
                                    $bill['price_per_unit_time'] = $request_typ->price_per_unit_time;
                                    if ($requestserv->base_price != 0) {
                                        $bill['distance_cost'] = ($requestserv->distance_cost);
                                        $bill['time_cost'] = (floatval(sprintf2($requestserv->time_cost, 2)));
                                    } else {
                                        /* $setbase_price = Settings::where('key', 'base_price')->first();
                                          $bill['base_price'] = ($setbase_price->value); */
                                        /* $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                          $bill['distance_cost'] = ($setdistance_price->value); */
                                        $bill['distance_cost'] = ($providertype->price_per_unit_distance);
                                        /* $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                          $bill['time_cost'] = (floatval(sprintf2($settime_price->value, 2))); */
                                        $bill['time_cost'] = (floatval(sprintf2($providertype->price_per_unit_time, 2)));
                                    }
                                    $admins = Admin::first();
                                    $bill['walker']['email'] = $walker->email;
                                    $bill['admin']['email'] = $admins->username;
                                    if ($request->transfer_amount != 0) {
                                        $bill['walker']['amount'] = ($request->total - $request->transfer_amount);
                                        $bill['admin']['amount'] = ($request->transfer_amount);
                                    } else {
                                        $bill['walker']['amount'] = ($request->transfer_amount);
                                        $bill['admin']['amount'] = ($request->total - $request->transfer_amount);
                                    }
                                    /* $bill['currency'] = $currency_selected->keyword; */
                                    $bill['currency'] = Config::get('app.generic_keywords.Currency');
                                    $bill['actual_total'] = $request->total;
                                    $bill['total'] = ($request->total);
                                    $bill['is_paid'] = $request->is_paid;
                                    $bill['promo_discount'] = $request->promo_payment;
                                    $bill['main_total'] = ($request->total);
                                    $total_amount = $request->total - $request->ledger_payment - $request->promo_payment;
                                    /*if($total_amount < 0)
                                        $total_amount = 0;*/
                                    $bill['total'] = ($total_amount);
                                    $bill['referral_bonus'] = ($request->ledger_payment);
                                    $bill['promo_bonus'] = ($request->promo_payment);
                                    $bill['payment_type'] = $request->payment_mode;
                                    $bill['is_paid'] = $request->is_paid;
                                }

                                $rservc = RequestServices::where('request_id', $request->id)->get();
                                $typs = array();
                                $typi = array();
                                $typp = array();
                                foreach ($rservc as $typ) {
                                    $typ1 = ProviderType::where('id', $typ->type)->first();
                                    $typ_price = ProviderServices::where('provider_id', $request->confirmed_walker)->where('type', $typ->type)->first();

                                    if ($typ_price->base_price > 0) {
                                        $typp1 = 0.00;
                                        $typp1 = $typ_price->base_price;
                                    } elseif ($typ_price->price_per_unit_distance > 0) {
                                        $typp1 = 0.00;
                                        foreach ($rservc as $key) {
                                            $typp1 = $typp1 + $key->distance_cost;
                                        }
                                    } else
                                        $typp1 = 0.00;

                                    $typs['name'] = $typ1->name;
                                    // $typs['icon']=$typ1->icon;
                                    $typs['price'] = $typp1;

                                    array_push($typi, $typs);
                                }
                                $bill['type'] = $typi;
                                $rserv = RequestServices::where('request_id', $request_id)->get();
                                $typs = array();
                                foreach ($rserv as $typ) {
                                    $typ1 = ProviderType::where('id', $typ->type)->first();
                                    array_push($typs, $typ1->name);
                                }
                                $settings = Settings::where('key', 'sms_when_provider_completes_job')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);
                                $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                $pattern = str_replace('%amount%', $request->total, $pattern);
                                //sms_notification($request->owner_id, 'owner', $pattern);
                                $id = $request->id;
                                // send email
                                /* $settings = Settings::where('key', 'email_request_finished')->first();
                                  $pattern = $settings->value;
                                  $pattern = str_replace('%id%', $request->id, $pattern);
                                  $pattern = str_replace('%url%', web_url() . "/admin/request/map/" . $request->id, $pattern);
                                  $subject = "Request Completed";
                                  email_notification(2, 'admin', $pattern, $subject); */
                                // $settings = Settings::where('key','email_invoice_generated_user')->first();
                                // $pattern = $settings->value;
                                // $pattern = str_replace('%id%', $request->id, $pattern);
                                // $pattern = str_replace('%amount%', $request->total, $pattern);

                                $email_data = array();

                                $email_data['name'] = $owner->first_name;
                                $email_data['emailType'] = 'user';
                                $email_data['base_price'] = $bill['base_price'];
                                $email_data['distance'] = $bill['distance'];
                                $email_data['time'] = $bill['time'];
                                $email_data['unit'] = $bill['unit'];
                                $email_data['total'] = $bill['total'];
                                $email_data['payment_mode'] = $bill['payment_mode'];
                                $email_data['actual_total'] = $request->total;
                                $email_data['is_paid'] = $request->is_paid;
                                $email_data['promo_discount'] = $request->promo_payment;

                                $request_services = RequestServices::where('request_id', $request->id)->first();

                                $locations = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id')
                                    ->get();
                                $count = round(count($locations) / 50);
                                $start = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id')
                                    ->first();
                                $end = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id', 'desc')
                                    ->first();

                                $map = "https://maps-api-ssl.google.com/maps/api/staticmap?size=249x249&style=feature:landscape|visibility:off&style=feature:poi|visibility:off&style=feature:transit|visibility:off&style=feature:road.highway|element:geometry|lightness:39&style=feature:road.local|element:geometry|gamma:1.45&style=feature:road|element:labels|gamma:1.22&style=feature:administrative|visibility:off&style=feature:administrative.locality|visibility:on&style=feature:landscape.natural|visibility:on&scale=2&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-start@2x.png|$start->latitude,$start->longitude&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-finish@2x.png|$end->latitude,$end->longitude&path=color:0x2dbae4ff|weight:4";
                                $skip = 0;
                                foreach ($locations as $location) {
                                    if ($skip == $count) {
                                        $map .= "|$location->latitude,$location->longitude";
                                        $skip = 0;
                                    }
                                    $skip++;
                                }

                                $start_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$start->latitude,$start->longitude"), TRUE);
                                $start_address = "Address not found";
                                if (isset($start_location['results'][0]['formatted_address'])) {
                                    $start_address = $start_location['results'][0]['formatted_address'];
                                }
                                $end_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$end->latitude,$end->longitude"), TRUE);
                                $end_address = "Address not found";
                                if (isset($end_location['results'][0]['formatted_address'])) {
                                    $end_address = $end_location['results'][0]['formatted_address'];
                                }

                                $email_data['start_location'] = $start_location;
                                $email_data['end_location'] = $end_location;

                                $walker_review = WalkerReview::where('request_id', $id)->first();
                                if ($walker_review) {
                                    $rating = round($walker_review->rating);
                                } else {
                                    $rating = 0;
                                }

                                $email_data['map'] = $map;
                                $settings = Settings::where('key', 'admin_email_address')->first();
                                $admin_email = $settings->value;
                                $requestserv = RequestServices::where('request_id', $request->id)->orderBy('id', 'DESC')->first();
                                $get_type_name = ProviderType::where('id', $requestserv->type)->first();
                                $detail = array(
                                    'admin_eamil' => $admin_email,
                                    'email_type' => 'user',
                                    'request' => $request,
                                    'start_address' => $start_address,
                                    'end_address' => $end_address,
                                    'start' => $start,
                                    'end' => $end,
                                    'map_url' => $map,
                                    'walker' => $walker,
                                    'rating' => $rating,
                                    'base_price' => $requestserv->base_price,
                                    'price_per_time' => $request_services->time_cost,
                                    'price_per_dist' => $request_services->distance_cost,
                                    'ref_bonus' => $request->ledger_payment,
                                    'promo_bonus' => $request->promo_payment,
                                    'dist_cost' => $requestserv->distance_cost,
                                    'time_cost' => $requestserv->time_cost,
                                    'type_name' => ucwords($get_type_name->name)
                                );
                                //send email to owner
                                /* $subject = "Invoice Generated";
                                  send_email($request->owner_id, 'owner', $email_data, $subject, 'invoice'); */

                                $subject = "Invoice Generated";
                                //Log::info('email detail'. print_r($detail,true));
                                //email_notification($request->owner_id, 'owner', $detail, $subject, 'invoice');
                            } else {
                                Requests::where('id', '=', $request_id)->update(array('is_cash_posted_cap' => 0));
                                Requests::where('id', '=', $request_id)->update(array('is_invoice_verified_user' => -1));
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Customer did not confirmed amount, Please reenter.',
                                    'request_id' => $request_id,
                                    'unique_id' => 27);
                                $title = 'Customer did not confirmed amount, Please reenter.';
                                $message = $response_array;
                                send_notifications($request->confirmed_walker, "walker", $title, $message);
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Owner Id not match to service id', 'error_code' => 407);
                        }

                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function addcardtoken()
    {
        Log::info('addcardtoken');
        $payment_token = Input::get('payment_token');
        $last_four = Input::get('last_four');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $payfort_ref_id = Input::get('payfort_ref_id');
        if (Input::has('card_type')) {
            $card_type = strtoupper(Input::get('card_type'));
        } else {
            $card_type = strtoupper("VISA");
        }
        $validator = Validator::make(
            array(
                'last_four' => $last_four,
                'payment_token' => $payment_token,
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'last_four' => 'required',
                'payment_token' => 'required',
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );
        $payments = array();

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'payments' => $payments);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if ($owner_data->email != "payfort@kasper-cab.com" && $owner_data->email != "fahed@kc.com" && $owner_data->email != "umar@kasper-cab.com" && $owner_data->email != "zeeshan@kasper-cab.com") {
                    $response_array = array('success' => false, 'error' => 'Card Payments are not allowed Yet. Please use cash.', 'error_code' => 411);
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                }
                if (is_token_active($owner_data->token_expiry) || $is_admin) {

                    try {

                        if (Config::get('app.default_payment') == 'payfort') {
                            if (true) {
                                $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();

                                $payment = new Payment;
                                $payment->owner_id = $owner_id;
                                $payment->customer_id = $payfort_ref_id;
                                $payment->last_four = $last_four;
                                $payment->card_type = $card_type;
                                $payment->card_token = $payment_token;
                                if ($card_count > 0) {
                                    $payment->is_default = 0;
                                } else {
                                    $payment->is_default = 1;
                                }
                                $payment->save();

                                $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($payment_data as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['id'] = $data1->id;
                                    $data['owner_id'] = $data1->owner_id;
                                    $data['customer_id'] = $data1->customer_id;
                                    $data['last_four'] = $data1->last_four;
                                    $data['card_token'] = $data1->card_token;
                                    $data['card_type'] = $data1->card_type;
                                    $data['card_id'] = $data1->card_token;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }
                                $response_array = array(
                                    'success' => true,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            } else {
                                $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($payment_data as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['id'] = $data1->id;
                                    $data['owner_id'] = $data1->owner_id;
                                    $data['customer_id'] = $data1->customer_id;
                                    $data['last_four'] = $data1->last_four;
                                    $data['card_token'] = $data1->card_token;
                                    $data['card_type'] = $data1->card_type;
                                    $data['card_id'] = $data1->card_token;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }
                                $response_array = array(
                                    'success' => false,
                                    'error' => 'Could not create client ID',
                                    'error_code' => 450,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            }
                        } else {
                            Braintree_Configuration::environment(Config::get('app.braintree_environment'));
                            Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
                            Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
                            Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
                            $result = Braintree_Customer::create(array(
                                'paymentMethodNonce' => $payment_token
                            ));
                            Log::info('result = ' . print_r($result, true));
                            if ($result->success) {
                                $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();

                                $customer_id = $result->customer->id;
                                $payment = new Payment;
                                $payment->owner_id = $owner_id;
                                $payment->customer_id = $customer_id;
                                $payment->last_four = $last_four;
                                $payment->card_type = $card_type;
                                $payment->card_token = $result->customer->creditCards[0]->token;
                                if ($card_count > 0) {
                                    $payment->is_default = 0;
                                } else {
                                    $payment->is_default = 1;
                                }
                                $payment->save();

                                $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($payment_data as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['id'] = $data1->id;
                                    $data['owner_id'] = $data1->owner_id;
                                    $data['customer_id'] = $data1->customer_id;
                                    $data['last_four'] = $data1->last_four;
                                    $data['card_token'] = $data1->card_token;
                                    $data['card_type'] = $data1->card_type;
                                    $data['card_id'] = $data1->card_token;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }

                                $response_array = array(
                                    'success' => true,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            } else {
                                $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($payment_data as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['id'] = $data1->id;
                                    $data['owner_id'] = $data1->owner_id;
                                    $data['customer_id'] = $data1->customer_id;
                                    $data['last_four'] = $data1->last_four;
                                    $data['card_token'] = $data1->card_token;
                                    $data['card_type'] = $data1->card_type;
                                    $data['card_id'] = $data1->card_token;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }
                                $response_array = array(
                                    'success' => false,
                                    'error' => 'Could not create client ID',
                                    'error_code' => 450,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            }
                        }
                    } catch (Exception $e) {
                        $response_array = array('success' => false, 'error' => $e, 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function chargepayment()
    {
        Log::info('chargepayment');
        $card_id = Input::get('card_id');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $amount = Input::get('amount');
        $customer_ip = Request::ip();
        Log::info("customer ip" . $customer_ip);

        $validator = Validator::make(
            array(
                'card_id' => $card_id,
                'token' => $token,
                'owner_id' => $owner_id,
                'amount' => $amount,
            ), array(
                'card_id' => 'required',
                'token' => 'required',
                'owner_id' => 'required|integer',
                'amount' => 'required|numeric'
            )
        );

        /* $var = Keywords::where('id', 2)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {

                Log::info($owner_data);
                // check for token validity
                Log::info('owner data');
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if ($owner_data->email != "payfort@kasper-cab.com" && $owner_data->email != "fahed@kc.com" && $owner_data->email != "umar@kasper-cab.com" && $owner_data->email != "zeeshan@kasper-cab.com") {
                        $response_array = array('success' => false, 'error' => 'Card Payments are not allowed Yet. Please use cash.', 'error_code' => 411);
                        $response_code = 200;
                        $response = Response::json($response_array, $response_code);
                        return $response;
                    }
                    Log::info('is token active' . $card_id);
                    if ($payment = Payment::find($card_id)) {
                        Log::info('payment found');
                        if ($payment->owner_id == $owner_id) {
                            Log::info('payment owner id');
                            // Set your secret key: remember to change this to your live secret key in production
                            //// See your keys here: https://dashboard.stripe.com/account/apikeys

                            Start::setApiKey('test_sec_k_Z6ouzQeUwoNwpTxcqRkz');
                            //Start::setApiKey(Config::get('app.payfort_secret_key'));
                            try {
                                $merchant_reference = 'kcondm' . $owner_data->id . '-' . ($amount * 100) . '_' . microtime(true);
                                $requestparameterstring = 'asdaredcsreaccess_code=Z6ouzQeUwoNwpTxcqRkzamount=' . ($amount * 100) . 'command=PURCHASEcurrency=SARcustomer_email=' . $owner_data->email . 'customer_ip=' . $customer_ip .
                                    'eci=MOTOlanguage=enmerchant_identifier=fLPmDVuxmerchant_reference=' . $merchant_reference . 'token_name=' . $payment->card_token . 'asdaredcsre';
                                $signature = hash("sha256", $requestparameterstring, false);
                                $client = new GuzzleHttp\Client();
                                $response = $client->post("https://sbpaymentservices.payfort.com/FortAPI/paymentApi",
                                    ['json' => [
                                        'access_code' => 'Z6ouzQeUwoNwpTxcqRkz',
                                        'amount' => ($amount * 100),
                                        'command' => 'PURCHASE',
                                        'currency' => 'SAR',
                                        'customer_email' => $owner_data->email,
                                        'customer_ip' => $customer_ip,
                                        'eci' => 'MOTO',
                                        'language' => 'en',
                                        'merchant_identifier' => 'fLPmDVux',
                                        'merchant_reference' => $merchant_reference,
                                        'signature' => $signature,
                                        'token_name' => $payment->card_token
                                    ]]);
                                $resobjPayfort = $response->getBody(true);
                                $objPayfort = json_decode($resobjPayfort, true);
                                $response_code = $objPayfort['response_code'];
                                $response_message = $objPayfort['response_message'];
                                $fort_id = $objPayfort['fort_id'];
                                $status = $objPayfort['status'];
                                $command='purchase';
                                $transaction_history = new TransactionHistory();
                                $transaction_history->request_id = 0;
                                $transaction_history->fort_id = $fort_id;
                                $transaction_history->amount = $amount;
                                $transaction_history->response_message = $response_message;
                                $transaction_history->command = $command;
                                $transaction_history->merchant_reference = $merchant_reference;
                                $transaction_history->response_code = $response_code;
                                $transaction_history->status = $status;
                                $transaction_history->save();
                                if ($response_code == '14000') {
                                    $owner = Owner::find($owner_id);
                                    $owner->credit = $owner->credit + $amount;
                                    $owner->save();
                                    $response_array = array(
                                        'success' => true,
                                        'credit' => $owner->credit,
                                        'message' => 'Card charge successfully.'
                                    );
                                    $response_code = 200;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'payfort Charge Payment Failed', 'error_code' => 405);
                                    $response_code = 200;
                                }
                            } catch (Error $e) {
                                Log::info($e);
                                $response_array = array('success' => false, 'error' => 'Payfort Error', 'error_code' => 442);
                                $response_code = 200;
                            }

                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Card ID and ' . $var->keyword . ' ID Doesnot matches', 'error_code' => 440); */
                            $response_array = array('success' => false, 'error' => 'Card ID and ' . Config::get('app.generic_keywords.User') . ' ID Doesnot matches', 'error_code' => 440);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Card not found', 'error_code' => 441);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function deletecardtoken()
    {
        $card_id = Input::get('card_id');
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'card_id' => $card_id,
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'card_id' => 'required',
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        /* $var = Keywords::where('id', 2)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if ($payment = Payment::find($card_id)) {
                        if ($payment->owner_id == $owner_id) {

                            $pdn = Payment::where('id', $card_id)->first();
                            $check = trim($pdn->is_default);
                            Payment::find($card_id)->delete();
                            if ($check == 1) {
                                $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();
                                if ($card_count) {
                                    $paymnt = Payment::where('owner_id', $owner_id)->first();
                                    $paymnt->is_default = 1;
                                    $paymnt->save();
                                }
                            }

                            $payments = array();
                            $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();
                            if ($card_count) {
                                $paymnt = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($paymnt as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['id'] = $data1->id;
                                    $data['customer_id'] = $data1->customer_id;
                                    $data['card_id'] = $data1->card_token;
                                    $data['last_four'] = $data1->last_four;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }
                                $response_array = array(
                                    'success' => true,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            } else {
                                $response_code = 200;
                                $response_array = array(
                                    'success' => true,
                                    'error' => 'No Card Found',
                                    'error_code' => 541,
                                );
                            }
                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Card ID and ' . $var->keyword . ' ID Doesnot matches', 'error_code' => 440); */
                            $response_array = array('success' => false, 'error' => 'Card ID and ' . Config::get('app.generic_keywords.User') . ' ID Doesnot matches', 'error_code' => 440);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Card not found', 'error_code' => 441);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function set_referral_code()
    {
        $code = Input::get('referral_code');
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                /* 'code' => $code, */
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                /* 'code' => 'required', */
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations

                    /* $ledger_count = Ledger::where('referral_code', $code)->count();
                      if ($ledger_count > 0) {
                      $response_array = array('success' => false, 'error' => 'This Code already is taken by another user', 'error_code' => 484);
                      } else {
                      $led = Ledger::where('owner_id', $owner_id)->first();
                      if ($led) {
                      $ledger = Ledger::where('owner_id', $owner_id)->first();
                      } else {
                      $ledger = new Ledger;
                      $ledger->owner_id = $owner_id;
                      }
                      $ledger->referral_code = $code;
                      $ledger->save();

                      $response_array = array('success' => true);
                      } */
                    /* $zero_in_code = Config::get('app.referral_zero_len') - strlen($owner_id);
                      $referral_code = Config::get('app.referral_prefix');
                      for ($i = 0; $i < $zero_in_code; $i++) {
                      $referral_code .= "0";
                      }
                      $referral_code .= $owner_id; */
                    regenerate:
                    $referral_code = my_random6_number();
                    if (Ledger::where('referral_code', $referral_code)->count()) {
                        goto regenerate;
                    }
                    /* $referral_code .= my_random6_number(); */
                    if (Ledger::where('owner_id', $owner_id)->count()) {
                        Ledger::where('owner_id', $owner_id)->update(array('referral_code' => $referral_code));
                    } else {
                        $ledger = new Ledger;
                        $ledger->owner_id = $owner_id;
                        $ledger->referral_code = $referral_code;
                        $ledger->save();
                    }
                    /* $ledger = Ledger::where('owner_id', $owner_id)->first();
                      $ledger->referral_code = $code;
                      $ledger->save(); */
                    /* SEND REFERRAL & PROMO INFO */
                    $settings = Settings::where('key', 'referral_code_activation')->first();
                    $referral_code_activation = $settings->value;
                    if ($referral_code_activation) {
                        $referral_code_activation_txt = "referral on";
                    } else {
                        $referral_code_activation_txt = "referral off";
                    }

                    $settings = Settings::where('key', 'promotional_code_activation')->first();
                    $promotional_code_activation = $settings->value;
                    if ($promotional_code_activation) {
                        $promotional_code_activation_txt = "promo on";
                    } else {
                        $promotional_code_activation_txt = "promo off";
                    }
                    /* SEND REFERRAL & PROMO INFO */
                    $response_array = array(
                        'success' => true,
                        'id' => $owner_data->id,
                        'first_name' => $owner_data->first_name,
                        'last_name' => $owner_data->last_name,
                        'phone' => $owner_data->phone,
                        'email' => $owner_data->email,
                        'picture' => $owner_data->picture,
                        'bio' => '',
                        'address' => $owner_data->address,
                        'state' => $owner_data->state,
                        'credit' => $owner_data->credit,
                        'use_credit_first' => $owner_data->use_credit_first,
                        'payment_mode' => $owner_data->payment_mode,
                        'country' => $owner_data->country,
                        'zipcode' => $owner_data->zipcode,
                        'login_by' => $owner_data->login_by,
                        'social_unique_id' => $owner_data->social_unique_id,
                        'device_token' => $owner_data->device_token,
                        'device_type' => $owner_data->device_type,
                        'timezone' => $owner_data->timezone,
                        'token' => $owner_data->token,
                        'referral_code' => $referral_code,
                        'is_referee' => $owner_data->is_referee,
                        'promo_count' => $owner_data->promo_count,
                        'is_referral_active' => $referral_code_activation,
                        'is_referral_active_txt' => $referral_code_activation_txt,
                        'is_promo_active' => $promotional_code_activation,
                        'is_promo_active_txt' => $promotional_code_activation_txt,
                    );

                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_referral_code()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations

                    $ledger = Ledger::where('owner_id', $owner_id)->first();
                    if ($ledger) {
                        $response_array = array(
                            'success' => true,
                            'referral_code' => $ledger->referral_code,
                            'total_referrals' => $ledger->total_referrals,
                            'amount_earned' => $ledger->amount_earned,
                            'amount_spent' => $ledger->amount_spent,
                            'balance_amount' => $ledger->amount_earned - $ledger->amount_spent,
                        );
                    } else {
                        $response_array = array('success' => false, 'error' => 'This user does not have a referral code');
                    }


                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_cards()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        if (Input::has('card_id')) {
            $card_id = Input::get('card_id');
            Payment::where('owner_id', $owner_id)->update(array('is_default' => 0));
            Payment::where('owner_id', $owner_id)->where('id', $card_id)->update(array('is_default' => 1));
        }

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    $payments = array();
                    $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();
                    if ($card_count) {
                        $paymnt = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                        foreach ($paymnt as $data1) {
                            $default = $data1->is_default;
                            if ($default == 1) {
                                $data['is_default_text'] = "default";
                            } else {
                                $data['is_default_text'] = "not_default";
                            }
                            $data['id'] = $data1->id;
                            $data['owner_id'] = $data1->owner_id;
                            $data['customer_id'] = $data1->customer_id;
                            $data['last_four'] = $data1->last_four;
                            $data['card_token'] = $data1->card_token;
                            $data['card_type'] = $data1->card_type;
                            $data['card_id'] = $data1->id;
                            $data['is_default'] = $default;
                            array_push($payments, $data);
                        }
                        $response_array = array(
                            'success' => true,
                            'payments' => $payments,
                            'credit' => $owner_data->credit,
                            'use_credit_first' => $owner_data->use_credit_first,
                            'payment_mode' => $owner_data->payment_mode
                        );
                    } else {
                        $response_array = array(
                            'success' => true,
                            'credit' => $owner_data->credit,
                            'use_credit_first' => $owner_data->use_credit_first,
                            'payment_mode' => $owner_data->payment_mode,
                            'error' => 'No Card Found'
                        );
                    }


                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function card_selection()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $default_card_id = Input::get('default_card_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'default_card_id' => $default_card_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'default_card_id' => 'required'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $payments = array();
            /* $payments['none'] = ""; */
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {

                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    Payment::where('owner_id', $owner_id)->update(array('is_default' => 0));
                    Payment::where('owner_id', $owner_id)->where('id', $default_card_id)->update(array('is_default' => 1));
                    $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                    foreach ($payment_data as $data1) {
                        $default = $data1->is_default;
                        if ($default == 1) {
                            $data['is_default_text'] = "default";
                        } else {
                            $data['is_default_text'] = "not_default";
                        }
                        $data['id'] = $data1->id;
                        $data['owner_id'] = $data1->owner_id;
                        $data['customer_id'] = $data1->customer_id;
                        $data['last_four'] = $data1->last_four;
                        $data['card_token'] = $data1->card_token;
                        $data['card_type'] = $data1->card_type;
                        $data['is_default'] = $default;
                        array_push($payments, $data);
                    }
                    $owner = Owner::find($owner_id);

                    $response_array = array(
                        'success' => true,
                        'id' => $owner->id,
                        'first_name' => $owner->first_name,
                        'last_name' => $owner->last_name,
                        'phone' => $owner->phone,
                        'email' => $owner->email,
                        'picture' => $owner->picture,
                        'bio' => '',
                        'address' => '',
                        'state' => '',
                        'country' => '',
                        'zipcode' => '',
                        'login_by' => $owner->login_by,
                        'social_unique_id' => $owner->social_unique_id,
                        'device_token' => $owner->device_token,
                        'device_type' => $owner->device_type,
                        'token' => $owner->token,
                        'default_card_id' => $default_card_id,
                        'payment_type' => 0,
                        'is_referee' => $owner->is_referee,
                        'promo_count' => $owner->promo_count,
                        'credit' => $owner->credit,
                        'use_credit_first' => $owner->use_credit_first,
                        'payment_mode' => $owner->payment_mode,
                        'payments' => $payments
                    );


                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Owner ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_completed_requests()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $from = Input::get('from_date'); // 2015-03-11 07:45:01
        $to_date = Input::get('to_date') . ' 23:59:59'; //2015-03-11 07:45:01

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    if ($from != "" && $to_date != "") {
                        $request_data = DB::table('request')
                            ->where('request.owner_id', $owner_id)
                            ->where('is_completed', 1)
                            ->where('is_cancelled', 0)
                            ->where('monthly_id', 0)
                            ->whereBetween('request_start_time', array($from, $to_date))
                            ->leftJoin('walker', 'request.confirmed_walker', '=', 'walker.id')
                            ->leftJoin('walker_services', 'walker.id', '=', 'walker_services.provider_id')
                            ->leftJoin('walker_type', 'walker_type.id', '=', 'walker_services.type')
                            ->leftJoin('request_services', 'request_services.request_id', '=', 'request.id')
                            ->select('request.*', 'request.request_start_time', 'request.promo_code', 'walker.first_name', 'walker.id as walker_id', 'walker.last_name', 'walker.phone', 'walker.email', 'walker.picture', 'walker.rate', 'walker.car_model', 'walker_type.name as type', 'walker_type.icon', 'request.distance', 'request.time', 'request_services.base_price as req_base_price', 'request_services.distance_cost as req_dis_cost', 'request_services.time_cost as req_time_cost', 'request_services.type as req_typ', 'request.total')
                            ->groupBy('request.id')
                            ->get();
                    } else {
                        $request_data = DB::table('request')
                            ->where('request.owner_id', $owner_id)
                            ->where('is_completed', 1)
                            ->where('is_cancelled', 0)
                            ->where('monthly_id', 0)
                            ->leftJoin('walker', 'request.confirmed_walker', '=', 'walker.id')
                            ->leftJoin('walker_services', 'walker.id', '=', 'walker_services.provider_id')
                            ->leftJoin('walker_type', 'walker_type.id', '=', 'walker_services.type')
                            ->leftJoin('request_services', 'request_services.request_id', '=', 'request.id')
                            ->select('request.*', 'request.request_start_time', 'request.promo_code', 'walker.first_name', 'walker.id as walker_id', 'walker.last_name', 'walker.phone', 'walker.email', 'walker.picture', 'walker.rate', 'walker.car_model', 'walker_type.name as type', 'walker_type.icon', 'request.distance', 'request.time', 'request_services.base_price as req_base_price', 'request_services.distance_cost as req_dis_cost', 'request_services.time_cost as req_time_cost', 'request_services.type as req_typ', 'request.total')
                            ->groupBy('request.id')
                            ->get();
                    }

                    $requests = array();

                    $settings = Settings::where('key', 'default_distance_unit')->first();
                    $unit = $settings->value;
                    if ($unit == 0) {
                        $unit_set = 'kms';
                    } elseif ($unit == 1) {
                        $unit_set = 'miles';
                    }

                    /* $currency_selected = Keywords::find(5); */
                    foreach ($request_data as $data) {
                        $request_typ = ProviderType::where('id', '=', $data->req_typ)->first();

                        /* $setbase_price = Settings::where('key', 'base_price')->first();
                          $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                          $settime_price = Settings::where('key', 'price_per_unit_time')->first(); */
                        $setbase_distance = $request_typ->base_distance;
                        $setbase_price = $request_typ->base_price;
                        $setdistance_price = $request_typ->price_per_unit_distance;
                        $settime_price = $request_typ->price_per_unit_time;

                        $request['base_distance'] = $setbase_distance;
                        $request['base_price'] = $setbase_price;
                        $request['price_per_unit_distance'] = $setdistance_price;
                        $request['price_per_unit_time'] = $settime_price;

                        $locations = WalkLocation::where('request_id', $data->id)->orderBy('id')->get();
                        $count = round(count($locations) / 50);
                        $start = $end = $map = "";
                        $id = $data->id;
                        if (count($locations) >= 1) {
                            $start = WalkLocation::where('request_id', $id)
                                ->orderBy('id')
                                ->first();
                            $end = WalkLocation::where('request_id', $id)
                                ->orderBy('id', 'desc')
                                ->first();
                            $map = "https://maps-api-ssl.google.com/maps/api/staticmap?size=249x249&scale=2&markers=shadow:true|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-start@2x.png|$start->latitude,$start->longitude&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-finish@2x.png|$end->latitude,$end->longitude&path=color:0x2dbae4ff|weight:4";
                            $skip = 0;
                            foreach ($locations as $location) {
                                if ($skip == $count) {
                                    $map .= "|$location->latitude,$location->longitude";
                                    $skip = 0;
                                }
                                $skip++;
                            }
                            /* $map.="&key=" . Config::get('app.gcm_browser_key'); */
                        }
                        $request['start_lat'] = "";
                        if (isset($start->latitude)) {
                            $request['start_lat'] = $start->latitude;
                        }
                        $request['start_long'] = "";
                        if (isset($start->longitude)) {
                            $request['start_long'] = $start->longitude;
                        }
                        $request['end_lat'] = "";
                        if (isset($end->latitude)) {
                            $request['end_lat'] = $end->latitude;
                        }
                        $request['end_long'] = "";
                        if (isset($end->longitude)) {
                            $request['end_long'] = $end->longitude;
                        }
                        $request['map_url'] = $map;

                        $walker = Walker::where('id', $data->walker_id)->first();

                        if ($walker != NULL) {
                            $user_timezone = $walker->timezone;
                        } else {
                            $user_timezone = 'UTC';
                        }

                        $default_timezone = Config::get('app.timezone');

                        $date_time = get_user_time($default_timezone, $user_timezone, $data->request_start_time);

                        $dist = number_format($data->distance, 2, '.', '');
                        $request['id'] = $data->id;
                        $request['date'] = $date_time;
                        $request['distance'] = (string)$dist;
                        $request['unit'] = $unit_set;
                        $request['time'] = $data->time;
                        $discount = 0;
                        if ($data->promo_code != "") {
                            if ($data->promo_code != "") {
                                $promo_code = PromoCodes::where('id', $data->promo_code)->first();
                                if ($promo_code) {
                                    $promo_value = $promo_code->value;
                                    $promo_type = $promo_code->type;
                                    if ($promo_type == 1) {
                                        // Percent Discount
                                        $discount = $data->total * $promo_value / 100;
                                    } elseif ($promo_type == 2) {
                                        // Absolute Discount
                                        $discount = $promo_value;
                                    }
                                }
                            }
                        }

                        $request['promo_discount'] = ($discount);

                        $is_multiple_service = Settings::where('key', 'allow_multiple_service')->first();
                        if ($is_multiple_service->value == 0) {

                            if ($data->req_base_price) {
                                $request['base_price'] = ($data->req_base_price);
                            } else {
                                /* $request['base_price'] = ($setbase_price->value); */
                                $request['base_price'] = ($setbase_price);
                            }

                            if ($data->req_dis_cost) {
                                $request['distance_cost'] = ($data->req_dis_cost);
                            } else {
                                /* $request['distance_cost'] = ($setdistance_price->value * $data->distance); */
                                if ($data->distance <= $setbase_distance) {
                                    $request['distance_cost'] = 0;
                                } else {
                                    $request['distance_cost'] = ($setdistance_price * ($data->distance - $setbase_distance));
                                }
                            }

                            if ($data->req_time_cost) {
                                $request['time_cost'] = ($data->req_time_cost);
                            } else {
                                /* $request['time_cost'] = ($settime_price->value * $data->time); */
                                $request['time_cost'] = ($settime_price * $data->time);
                            }
                            $request['setbase_distance'] = $setbase_distance;
                            $request['total'] = ($data->total);
                            $request['actual_total'] = ($data->total + $data->ledger_payment + $discount);
                            $request['type'] = $data->type;
                            $request['type_icon'] = $data->icon;
                        } else {
                            $rserv = RequestServices::where('request_id', $data->id)->get();
                            $typs = array();
                            $typi = array();
                            $typp = array();
                            $total_price = 0;

                            foreach ($rserv as $typ) {
                                $typ1 = ProviderType::where('id', $typ->type)->first();
                                $typ_price = ProviderServices::where('provider_id', $data->walker_id)->where('type', $typ->type)->first();

                                if ($typ_price->base_price > 0) {
                                    $typp1 = 0.00;
                                    $typp1 = $typ_price->base_price;
                                } elseif ($typ_price->price_per_unit_distance > 0) {
                                    $typp1 = 0.00;
                                    foreach ($rserv as $key) {
                                        $typp1 = $typp1 + $key->distance_cost;
                                    }
                                } else {
                                    $typp1 = 0.00;
                                }
                                $typs['name'] = $typ1->name;
                                $typs['price'] = ($typp1);
                                $total_price = $total_price + $typp1;
                                array_push($typi, $typs);
                            }
                            $request['type'] = $typi;
                            $base_price = 0;
                            $distance_cost = 0;
                            $time_cost = 0;
                            foreach ($rserv as $key) {
                                $base_price = $base_price + $key->base_price;
                                $distance_cost = $distance_cost + $key->distance_cost;
                                $time_cost = $time_cost + $key->time_cost;
                            }
                            $request['base_price'] = ($base_price);
                            $request['distance_cost'] = ($distance_cost);
                            $request['time_cost'] = ($time_cost);
                            $request['total'] = ($total_price);
                        }

                        $rate = WalkerReview::where('request_id', $data->id)->where('walker_id', $data->confirmed_walker)->first();
                        if ($rate != NULL) {
                            $request['walker']['rating'] = $rate->rating;
                        } else {
                            $request['walker']['rating'] = '0.0';
                        }

                        /* $request['currency'] = $currency_selected->keyword; */

                        $request['pickup_address'] = $data->pickup_address;
                        $request['drop_address'] = $data->drop_address;
                        $request['base_price'] = ($data->req_base_price);
                        $request['distance_cost'] = ($data->req_dis_cost);
                        $request['time_cost'] = ($data->req_time_cost);
                        $request['total'] = ($data->total - $data->ledger_payment - $data->promo_payment);
                        $request['main_total'] = ($data->total);
                        $request['referral_bonus'] = ($data->ledger_payment);
                        $request['promo_bonus'] = ($data->promo_payment);
                        $request['payment_type'] = $data->payment_mode;
                        $request['is_paid'] = $data->is_paid;
                        $request['promo_id'] = $data->promo_id;
                        $request['promo_code'] = $data->promo_code;
                        $request['currency'] = Config::get('app.generic_keywords.Currency');
                        $request['walker']['first_name'] = $data->first_name;
                        $request['walker']['last_name'] = $data->last_name;
                        $request['walker']['phone'] = $data->phone;
                        $request['walker']['email'] = $data->email;
                        $request['walker']['picture'] = $data->picture;
                        $request['walker']['bio'] = '';
                        $request['walker']['type'] = $data->type;
                        $walker_cab_photo = WalkerDocument::where('walker_id', $data->confirmed_walker)->where('document_id', 4)->first();
                        if (isset($walker_cab_photo)) {
                            $request['walker']['cab_picture'] = $walker_cab_photo->url;
                        } else {
                            $request['walker']['cab_picture'] = null;
                        }
                        $request['walker']['cab_model'] = $data->car_model;
                        $request['bill'] = $data->bill;
                        $request['current_credit'] = ($data->current_credit_cap);
                        $request['used_credit'] = ($data->used_credit_cap);
                        $request['added_to_credit'] = ($data->added_to_credit_cap);
                        $request['cash_paid'] = ($data->cash_paid);
                        /* $request['walker']['rating'] = $data->rate; */
                        array_push($requests, $request);
                    }

                    $response_array = array(
                        'success' => true,
                        'requests' => $requests
                    );

                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function update_profile()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $first_name = $last_name = $phone = $password = $picture = $bio = $address = $state = $country = $zipcode = 0;
        if (Input::has('first_name'))
            $first_name = Input::get('first_name');
        if (Input::has('last_name'))
            $last_name = Input::get('last_name');
        if (Input::has('phone'))
            $phone = Input::get('phone');
        if (Input::has('password'))
            $password = Input::get('password');
        if (Input::hasFile('picture')) {
            $picture = Input::file('picture');
        }
        $new_password = Input::get('new_password');
        $old_password = Input::get('old_password');
        if ($picture == 0) {
            $validator = Validator::make(
                array(
                    'token' => $token,
                    'owner_id' => $owner_id
                ), array(
                    'token' => 'required',
                    'owner_id' => 'required|integer'
                )
            );
        } else {
            $validator = Validator::make(
                array(
                    'token' => $token,
                    'owner_id' => $owner_id,
                    'picture' => $picture
                ), array(
                    'token' => 'required',
                    'owner_id' => 'required|integer',
                    'picture' => 'mimes:jpeg,bmp,png'
                )
            );
        }


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if ($new_password != "" || $new_password != NULL) {
                        if ($old_password != "" || $old_password != NULL) {
                            if (Hash::check($old_password, $owner_data->password)) {
                                // Do necessary operations
                                $owner = Owner::find($owner_id);
                                if ($first_name) {
                                    $owner->first_name = $first_name;
                                }
                                if ($last_name) {
                                    $owner->last_name = $last_name;
                                }
                                if ($phone) {
                                    $owner->phone = $phone;
                                }

                                if ($new_password) {
                                    $owner->password = Hash::make($new_password);
                                }
                                if (Input::hasFile('picture')) {
                                    if ($owner->picture != "") {
                                        $path = $owner->picture;
                                        Log::info($path);
                                        $filename = basename($path);
                                        Log::info($filename);
                                        if (file_exists($path)) {
                                            unlink(public_path() . "/uploads/" . $filename);
                                        }
                                    }
                                    // upload image
                                    $file_name = time();
                                    $file_name .= rand();
                                    $file_name = sha1($file_name);

                                    $ext = Input::file('picture')->getClientOriginalExtension();
                                    Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                                    $local_url = $file_name . "." . $ext;

                                    // Upload to S3
                                    if (Config::get('app.s3_bucket') != "") {
                                        $s3 = App::make('aws')->get('s3');
                                        $pic = $s3->putObject(array(
                                            'Bucket' => Config::get('app.s3_bucket'),
                                            'Key' => $file_name,
                                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                                        ));

                                        $s3->putObjectAcl(array(
                                            'Bucket' => Config::get('app.s3_bucket'),
                                            'Key' => $file_name,
                                            'ACL' => 'public-read'
                                        ));

                                        $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                                    } else {
                                        $s3_url = asset_url() . '/uploads/' . $local_url;
                                    }

                                    if (isset($owner->picture)) {
                                        if ($owner->picture != "") {
                                            $icon = $owner->picture;
                                            unlink_image($icon);
                                        }
                                    }

                                    $owner->picture = $s3_url;
                                }
                                If (Input::has('timezone')) {
                                    $owner->timezone = Input::get('timezone');
                                }
                                $owner->save();
                                $code_data = Ledger::where('owner_id', '=', $owner->id)->first();

                                /* SEND REFERRAL & PROMO INFO */
                                $settings = Settings::where('key', 'referral_code_activation')->first();
                                $referral_code_activation = $settings->value;
                                if ($referral_code_activation) {
                                    $referral_code_activation_txt = "referral on";
                                } else {
                                    $referral_code_activation_txt = "referral off";
                                }

                                $settings = Settings::where('key', 'promotional_code_activation')->first();
                                $promotional_code_activation = $settings->value;
                                if ($promotional_code_activation) {
                                    $promotional_code_activation_txt = "promo on";
                                } else {
                                    $promotional_code_activation_txt = "promo off";
                                }
                                /* SEND REFERRAL & PROMO INFO */

                                $response_array = array(
                                    'success' => true,
                                    'id' => $owner->id,
                                    'first_name' => $owner->first_name,
                                    'last_name' => $owner->last_name,
                                    'phone' => $owner->phone,
                                    'email' => $owner->email,
                                    'credit' => $owner->credit,
                                    'use_credit_first' => $owner->use_credit_first,
                                    'payment_mode' => $owner->payment_mode,
                                    'picture' => $owner->picture,
                                    'bio' => '',
                                    'address' => '',
                                    'state' => '',
                                    'country' => '',
                                    'zipcode' => '',
                                    'login_by' => $owner->login_by,
                                    'social_unique_id' => $owner->social_unique_id,
                                    'device_token' => $owner->device_token,
                                    'device_type' => $owner->device_type,
                                    'timezone' => $owner->timezone,
                                    'token' => $owner->token,
                                    'referral_code' => $code_data->referral_code,
                                    'is_referee' => $owner->is_referee,
                                    'promo_count' => $owner->promo_count,
                                    'is_referral_active' => $referral_code_activation,
                                    'is_referral_active_txt' => $referral_code_activation_txt,
                                    'is_promo_active' => $promotional_code_activation,
                                    'is_promo_active_txt' => $promotional_code_activation_txt,
                                );
                                $response_code = 200;
                            } else {
                                $response_array = array('success' => false, 'error' => 'Invalid Old Password', 'error_code' => 501);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Old Password must not be blank', 'error_code' => 502);
                            $response_code = 200;
                        }
                    } else {
                        // Do necessary operations
                        $owner = Owner::find($owner_id);
                        if ($first_name) {
                            $owner->first_name = $first_name;
                        }
                        if ($last_name) {
                            $owner->last_name = $last_name;
                        }
                        if ($phone) {
                            $owner->phone = $phone;
                        }
                        /*if ($zipcode) {
                            $owner->zipcode = $zipcode;
                        }*/
                        if (Input::hasFile('picture')) {
                            if ($owner->picture != "") {
                                $path = $owner->picture;
                                Log::info($path);
                                $filename = basename($path);
                                Log::info($filename);
                                if (file_exists($path)) {
                                    unlink(public_path() . "/uploads/" . $filename);
                                }
                            }
                            // upload image
                            $file_name = time();
                            $file_name .= rand();
                            $file_name = sha1($file_name);

                            $ext = Input::file('picture')->getClientOriginalExtension();
                            Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                            $local_url = $file_name . "." . $ext;

                            // Upload to S3
                            if (Config::get('app.s3_bucket') != "") {
                                $s3 = App::make('aws')->get('s3');
                                $pic = $s3->putObject(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'SourceFile' => public_path() . "/uploads/" . $local_url,
                                ));

                                $s3->putObjectAcl(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'ACL' => 'public-read'
                                ));

                                $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                            } else {
                                $s3_url = asset_url() . '/uploads/' . $local_url;
                            }

                            if (isset($owner->picture)) {
                                if ($owner->picture != "") {
                                    $icon = $owner->picture;
                                    unlink_image($icon);
                                }
                            }

                            $owner->picture = $s3_url;
                        }
                        If (Input::has('timezone')) {
                            $owner->timezone = Input::get('timezone');
                        }
                        $owner->save();
                        $code_data = Ledger::where('owner_id', '=', $owner->id)->first();

                        /* SEND REFERRAL & PROMO INFO */
                        $settings = Settings::where('key', 'referral_code_activation')->first();
                        $referral_code_activation = $settings->value;
                        if ($referral_code_activation) {
                            $referral_code_activation_txt = "referral on";
                        } else {
                            $referral_code_activation_txt = "referral off";
                        }

                        $settings = Settings::where('key', 'promotional_code_activation')->first();
                        $promotional_code_activation = $settings->value;
                        if ($promotional_code_activation) {
                            $promotional_code_activation_txt = "promo on";
                        } else {
                            $promotional_code_activation_txt = "promo off";
                        }
                        /* SEND REFERRAL & PROMO INFO */

                        $response_array = array(
                            'success' => true,
                            'id' => $owner->id,
                            'first_name' => $owner->first_name,
                            'last_name' => $owner->last_name,
                            'phone' => $owner->phone,
                            'email' => $owner->email,
                            'picture' => $owner->picture,
                            'bio' => '',
                            'address' => '',
                            'state' => '',
                            'country' => '',
                            'zipcode' => '',
                            'login_by' => $owner->login_by,
                            'social_unique_id' => $owner->social_unique_id,
                            'device_token' => $owner->device_token,
                            'device_type' => $owner->device_type,
                            'timezone' => $owner->timezone,
                            'token' => $owner->token,
                            'referral_code' => $code_data->referral_code,
                            'is_referee' => $owner->is_referee,
                            'promo_count' => $owner->promo_count,
                            'is_referral_active' => $referral_code_activation,
                            'is_referral_active_txt' => $referral_code_activation_txt,
                            'is_promo_active' => $promotional_code_activation,
                            'is_promo_active_txt' => $promotional_code_activation_txt,
                        );
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function payment_type()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $request_id = Input::get('request_id');
        $cash_or_card = Input::get('cash_or_card');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'cash_or_card' => $cash_or_card,
                'request_id' => $request_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'cash_or_card' => 'required',
                'request_id' => 'required',
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $payments = array();
            /* $payments['none'] = ""; */
            $def_card = 0;
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    if ($cash_or_card != 1) {
                        $card_count = Payment::where('owner_id', '=', $owner_id)->count();
                        if ($card_count <= 0) {
                            $response_array = array('success' => false, 'error' => "Please add card first for payment.", 'error_code' => 417);
                            $response_code = 200;
                            $response = Response::json($response_array, $response_code);
                            return $response;
                        }
                    }
                    // Do necessary operations
                    $owner = Owner::find($owner_id);
                    $payment_data = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                    foreach ($payment_data as $data1) {
                        $default = $data1->is_default;
                        if ($default == 1) {
                            $def_card = $data1->id;
                            $data['is_default_text'] = "default";
                        } else {
                            $data['is_default_text'] = "not_default";
                        }
                        $data['id'] = $data1->id;
                        $data['owner_id'] = $data1->owner_id;
                        $data['customer_id'] = $data1->customer_id;
                        $data['last_four'] = $data1->last_four;
                        $data['card_token'] = $data1->card_token;
                        $data['card_type'] = $data1->card_type;
                        $data['card_id'] = $data1->card_token;
                        $data['is_default'] = $default;
                        array_push($payments, $data);
                    }
                    if ($request = Requests::find($request_id)) {
                        $request->payment_mode = $cash_or_card;
                        $request->save();

                        $walker = Walker::where('id', $request->confirmed_walker)->first();
                        if ($walker) {
                            $msg_array = array();
                            $msg_array['unique_id'] = 3;
                            $response_array = array(
                                'success' => true,
                                'id' => $owner->id,
                                'first_name' => $owner->first_name,
                                'last_name' => $owner->last_name,
                                'phone' => $owner->phone,
                                'email' => $owner->email,
                                'picture' => $owner->picture,
                                'bio' => '',
                                'address' => '',
                                'state' => '',
                                'country' => '',
                                'zipcode' => '',
                                'login_by' => $owner->login_by,
                                'social_unique_id' => $owner->social_unique_id,
                                'device_token' => $owner->device_token,
                                'device_type' => $owner->device_type,
                                'token' => $owner->token,
                                'default_card_id' => $def_card,
                                'payment_type' => $request->payment_mode,
                                'is_referee' => $owner->is_referee,
                                'promo_count' => $owner->promo_count,
                                'payments' => $payments,
                            );
                            $response_array['unique_id'] = 3;
                            $response_code = 200;
                            $msg_array['owner_data'] = $response_array;
                            $title = "Payment Type Change";
                            $message = $msg_array;
                            if ($request->confirmed_walker == $request->current_walker) {
                                send_notifications($request->confirmed_walker, "walker", $title, $message);
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Driver Not Found', 'error_code' => 421);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Owner ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function select_card()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $card_token = Input::get('card_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'card' => $card_token
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'card' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {

                    Payment::where('owner_id', $owner_id)->update(array('is_default' => 0));
                    Payment::where('owner_id', $owner_id)->where('id', $card_token)->update(array('is_default' => 1));

                    $payments = array();
                    $card_count = DB::table('payment')->where('owner_id', '=', $owner_id)->count();
                    if ($card_count) {
                        $paymnt = Payment::where('owner_id', $owner_id)->orderBy('is_default', 'DESC')->get();
                        foreach ($paymnt as $data1) {
                            $default = $data1->is_default;
                            if ($default == 1) {
                                $data['is_default_text'] = "default";
                            } else {
                                $data['is_default_text'] = "not_default";
                            }
                            $data['id'] = $data1->id;
                            $data['owner_id'] = $data1->owner_id;
                            $data['customer_id'] = $data1->customer_id;
                            $data['last_four'] = $data1->last_four;
                            $data['card_token'] = $data1->card_token;
                            $data['card_type'] = $data1->card_type;
                            $data['card_id'] = $data1->card_token;
                            $data['is_default'] = $default;
                            array_push($payments, $data);
                        }
                        $response_array = array(
                            'success' => true,
                            'payments' => $payments
                        );
                    } else {
                        $response_array = array(
                            'success' => false,
                            'error' => 'No Card Found'
                        );
                    }
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function pay_debt()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $total = $owner_data->debt;
                    if ($total == 0) {
                        $response_array = array('success' => true);
                        $response_code = 200;
                        $response = Response::json($response_array, $response_code);
                        return $response;
                    }
                    $payment_data = Payment::where('owner_id', $owner_id)->where('is_default', 1)->first();
                    // if (!$payment_data)
                    //   $payment_data = Payment::where('owner_id', $request->owner_id)->first();

                    if ($payment_data) {
                        $customer_id = $payment_data->customer_id;

                        if (Config::get('app.default_payment') == 'stripe') {
                            Stripe::setApiKey(Config::get('app.stripe_secret_key'));

                            try {
                                Stripe_Charge::create(array(
                                        "amount" => $total * 100,
                                        "currency" => "usd",
                                        "customer" => $customer_id)
                                );
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $ownr = Owner::find($owner_id);
                                $ownr->debt = $total;
                                $ownr->save();
                                $response_array = array('error' => $e->getMessage());
                                $response_code = 200;
                                $response = Response::json($response_array, $response_code);
                                return $response;
                            }
                            $owner_data->debt = 0;
                            $owner_data->save();
                        } else {
                            $amount = $total;
                            Braintree_Configuration::environment(Config::get('app.braintree_environment'));
                            Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
                            Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
                            Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
                            $card_id = $payment_data->card_token;
                            $result = Braintree_Transaction::sale(array(
                                'amount' => $amount,
                                'paymentMethodToken' => $card_id
                            ));

                            Log::info('result = ' . print_r($result, true));
                            if ($result->success) {
                                $owner_data->debt = $total;
                            } else {
                                $owner_data->debt = 0;
                            }
                            $owner_data->save();
                        }
                    }
                    $response_array = array('success' => true);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function paybypaypal()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $request_id = Input::get('request_id');
        $paypal_id = Input::get('paypal_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'paypal_id' => $paypal_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'paypal_id' => 'required'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    Log::info('paypal_id = ' . print_r($paypal_id, true));
                    $req = Requests::find($request_id);
                    Log::info('req = ' . print_r($req, true));
                    $req->is_paid = 1;
                    $req->payment_id = $paypal_id;
                    $req->save();
                    $response_array = array('success' => true);
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function paybybitcoin()
    {
        // $token = Input::get('token');
        // $owner_id = Input::get('id');
        // $request_id = Input::get('request_id');
        // $validator = Validator::make(
        //  array(
        //    'token' => $token,
        //    'owner_id' => $owner_id,
        //  ),
        //  array(
        //    'token' => 'required',
        //    'owner_id' => 'required|integer',
        //  )
        // );
        // if ($validator->fails()) {
        //  $error_messages = $validator->messages()->all();
        //    $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages );
        //    $response_code = 200;
        // } else {
        //  $is_admin = $this->isAdmin($token);
        //  if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
        //    // check for token validity
        //    if (is_token_active($owner_data->token_expiry) || $is_admin) {
        $coinbaseAPIKey = Config::get('app.coinbaseAPIKey');
        $coinbaseAPISecret = Config::get('app.coinbaseAPISecret');
        // coinbase
        $coinbase = Coinbase::withApiKey($coinbaseAPIKey, $coinbaseAPISecret);
        // $balance = $coinbase->getBalance() . " BTC";
        $user = $coinbase->getUser();
        // $contacts = $coinbase->getContacts("user");
        // $currencies = $coinbase->getCurrencies();
        // $rates = $coinbase->getExchangeRate();
        // $paymentButton = $coinbase->createButton(
        //     "Request ID",
        //     "19.99", 
        //     "USD", 
        //     "TRACKING_CODE_1", 
        //     array(
        //            "description" => "My 19.99 USD donation to PL",
        //            "cancel_url" => "http://localhost:8000/user/acceptbitcoin",
        //            "success_url" => "http://localhost:8000/user/acceptbitcoin"
        //        )
        // );

        Log::info('user = ' . print_r($user, true));

        $response_array = array('success' => true);
        //    }else{
        //      $response_array = array('success' => false);
        //      Log::error('1');
        //    }
        //  }else{
        //    $response_array = array('success' => false);
        //    Log::error('2');
        //  }
        // }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function acceptbitcoin()
    {
        $response = Input::get('response');
        /*
          Sample Response
          {
          "order": {
          "id": "5RTQNACF",
          "created_at": "2012-12-09T21:23:41-08:00",
          "status": "completed",
          "event": {
          "type": "completed"
          },
          "total_btc": {
          "cents": 100000000,
          "currency_iso": "BTC"
          },
          "total_native": {
          "cents": 1253,
          "currency_iso": "USD"
          },
          "total_payout": {
          "cents": 2345,
          "currency_iso": "USD"
          },
          "custom": "order1234",
          "receive_address": "1NhwPYPgoPwr5hynRAsto5ZgEcw1LzM3My",
          "button": {
          "type": "buy_now",
          "name": "Alpaca Socks",
          "description": "The ultimate in lightweight footwear",
          "id": "5d37a3b61914d6d0ad15b5135d80c19f"
          },
          "transaction": {
          "id": "514f18b7a5ea3d630a00000f",
          "hash": "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b",
          "confirmations": 0
          },
          "refund_address": "1HcmQZarSgNuGYz4r7ZkjYumiU4PujrNYk"
          },
          "customer": {
          "email": "coinbase@example.com",
          "shipping_address": [
          "John Smith",
          "123 Main St.",
          "Springfield, OR 97477",
          "United States"
          ]
          }
          }
         */
        Log::info('response = ' . print_r($response, true));
        return Response::json(200, $response);
    }

    public function send_eta()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $phones = Input::get('phone');
        $request_id = Input::get('request_id');
        $eta = Input::get('eta');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'phones' => $phones,
                'eta' => $eta,
            ), array(
                'token' => 'required',
                'phones' => 'required',
                'owner_id' => 'required|integer',
                'eta' => 'required'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // If phones is not an array
                    if (!is_array($phones)) {
                        $phones = explode(',', $phones);
                    }

                    Log::info('phones = ' . print_r($phones, true));

                    foreach ($phones as $key) {

                        $owner = Owner::where('id', $owner_id)->first();
                        $secret = str_random(6);

                        $request = Requests::where('id', $request_id)->first();
                        $request->security_key = $secret;
                        $request->save();
                        $msg = $owner->first_name . ' ' . $owner->last_name . ' ETA : ' . $eta;
                        send_eta($key, $msg);
                        Log::info('Send ETA MSG  = ' . print_r($msg, true));
                    }

                    $response_array = array('success' => true);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function payment_options_allowed()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    // Payment options allowed
                    $payment_options = array();

                    $payments = Payment::where('owner_id', $owner_id)->count();

                    if ($payments) {
                        $payment_options['stored_cards'] = 1;
                    } else {
                        $payment_options['stored_cards'] = 0;
                    }
                    $codsett = Settings::where('key', 'cod')->first();
                    if ($codsett->value == 1) {
                        $payment_options['cod'] = 1;
                    } else {
                        $payment_options['cod'] = 0;
                    }

                    $paypalsett = Settings::where('key', 'paypal')->first();
                    if ($paypalsett->value == 1) {
                        $payment_options['paypal'] = 1;
                    } else {
                        $payment_options['paypal'] = 0;
                    }

                    Log::info('payment_options = ' . print_r($payment_options, true));
                    /* SEND REFERRAL & PROMO INFO */
                    $settings = Settings::where('key', 'referral_code_activation')->first();
                    $referral_code_activation = $settings->value;
                    if ($referral_code_activation) {
                        $referral_code_activation_txt = "referral on";
                    } else {
                        $referral_code_activation_txt = "referral off";
                    }

                    $settings = Settings::where('key', 'promotional_code_activation')->first();
                    $promotional_code_activation = $settings->value;
                    if ($promotional_code_activation) {
                        $promotional_code_activation_txt = "promo on";
                    } else {
                        $promotional_code_activation_txt = "promo off";
                    }
                    /* SEND REFERRAL & PROMO INFO */

                    // Promo code allowed
                    /* $promosett = Settings::where('key', 'promo_code')->first(); */
                    if ($promotional_code_activation == 1) {
                        $promo_allow = 1;
                    } else {
                        $promo_allow = 0;
                    }

                    $response_array = array(
                        'success' => true,
                        'payment_options' => $payment_options,
                        'promo_allow' => $promo_allow,
                        'is_referral_active' => $referral_code_activation,
                        'is_referral_active_txt' => $referral_code_activation_txt,
                        'is_promo_active' => $promotional_code_activation,
                        'is_promo_active_txt' => $promotional_code_activation_txt,
                    );
                } else {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_credits()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $ledger = Ledger::where('owner_id', $owner_id)->first();
                    if ($ledger) {
                        $credits['balance'] = ($ledger->amount_earned - $ledger->amount_spent);
                        /* $credits['currency'] = $currency_selected->keyword; */
                        $credits['currency'] = Config::get('app.generic_keywords.Currency');
                        $response_array = array('success' => true, 'credits' => $credits);
                    } else {
                        $response_array = array('success' => false, 'error' => 'No Credit Found', 'error_code' => 475);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function logout()
    {
        if (Request::isMethod('post')) {
            $token = Input::get('token');
            $owner_id = Input::get('id');

            $validator = Validator::make(
                array(
                    'token' => $token,
                    'owner_id' => $owner_id,
                ), array(
                    'token' => 'required',
                    'owner_id' => 'required|integer'
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($owner_data->token_expiry) || $is_admin) {

                        $owner_data->latitude = 0;
                        $owner_data->longitude = 0;
                        $owner_data->device_token = 0;
                        /* $owner_data->is_login = 0; */
                        $owner_data->save();

                        $response_array = array('success' => true, 'error' => 'Successfully Log-Out');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        $response_array = array('success' => false, 'error' => 'Owner ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function register_monthly_subscription()
    {
        $specific_days = array();

        $token = Input::get('token');
        $owner_id = Input::get('id');

        $subscription_type = Input::get('subscription_type');
        $trip_type = Input::get('trip_type');
        //$gender = Input::get('gender');
        //$is_full_month = Input::get('is_full_month');
        $specific_days = Input::get('specific_days');
        $trip_one_from_address = Input::get('trip_one_from_address');
        $trip_one_to_address = Input::get('trip_one_to_address');
        $trip_two_from_address = Input::get('trip_two_from_address');
        $trip_two_to_address = Input::get('trip_two_to_address');
        $trip_one_from_lat = Input::get('trip_one_from_lat');
        $trip_one_from_long = Input::get('trip_one_from_long');
        $trip_one_to_lat = Input::get('trip_one_to_lat');
        $trip_one_to_long = Input::get('trip_one_to_long');
        $trip_one_pickup_time = Input::get('trip_one_pickup_time');
        $trip_two_from_lat = Input::get('trip_two_from_lat');
        $trip_two_from_long = Input::get('trip_two_from_long');
        $trip_two_to_lat = Input::get('trip_two_to_lat');
        $trip_two_to_long = Input::get('trip_two_to_long');
        $trip_two_pickup_time = Input::get('trip_two_pickup_time');
        $starting_date = Input::get('starting_date');
        //$cab_type = Input::get('cab_type');

        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'subscription_type' => $subscription_type,
                'trip_type' => $trip_type,
                //'gender' => $gender,
                //'is_full_month' => $is_full_month,
                'trip_one_from_address' => $trip_one_from_address,
                'trip_one_to_address' => $trip_one_to_address,
                'trip_two_from_address' => $trip_two_from_address,
                'trip_two_to_address' => $trip_two_to_address,
                'trip_one_from_lat' => $trip_one_from_lat,
                'trip_one_from_long' => $trip_one_from_long,
                'trip_one_to_lat' => $trip_one_to_lat,
                'trip_one_to_long' => $trip_one_to_long,
                'trip_one_pickup_time' => $trip_one_pickup_time,
                'trip_two_from_lat' => $trip_two_from_lat,
                'trip_two_from_long' => $trip_two_from_long,
                'trip_two_to_lat' => $trip_two_to_lat,
                'trip_two_to_long' => $trip_two_to_long,
                'trip_two_pickup_time' => $trip_two_pickup_time,
                'starting_date' => $starting_date
                //'cab_type' => $cab_type
            ), array(
            'owner_id' => 'required|integer',
            'subscription_type' => 'required|integer',
            'trip_type' => 'required|integer',
            //'gender' => 'required|integer',
            //'is_full_month' => 'required|integer',
            'trip_one_from_address' => 'required',
            'trip_one_to_address' => 'required',
            'trip_one_from_lat' => 'required',
            'trip_one_from_long' => 'required',
            'trip_one_to_lat' => 'required',
            'trip_one_to_long' => 'required',
            'trip_one_pickup_time' => 'required',
            'starting_date' => 'required',
            //'cab_type' => 'required|integer'
        ));


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();

            Log::info('Error while during monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $request_count = Monthly::where('owner_id', '=', $owner_id)->whereNotIn('status', [2, 4, 6, 7, 8, 9, 10, 11, 12, 14, 15])->count();

            if ($request_count == 0) {
                $monthly = new Monthly;
                $monthly->owner_id = $owner_id;
                $monthly->subscription_type = $subscription_type;
                $monthly->trip_type = $trip_type;
                //$monthly->gender = $gender;
                //$monthly->is_full_month = $is_full_month;
                $monthly->trip_one_from_address = $trip_one_from_address;
                $monthly->trip_one_to_address = $trip_one_to_address;
                $monthly->trip_two_from_address = $trip_two_from_address;
                $monthly->trip_two_to_address = $trip_two_to_address;
                $monthly->trip_one_from_lat = $trip_one_from_lat;
                $monthly->trip_one_from_long = $trip_one_from_long;
                $monthly->trip_one_to_lat = $trip_one_to_lat;
                $monthly->trip_one_to_long = $trip_one_to_long;
                $monthly->trip_one_pickup_time = $trip_one_pickup_time;
                $monthly->trip_two_from_lat = $trip_two_from_lat;
                $monthly->trip_two_from_long = $trip_two_from_long;
                $monthly->trip_two_to_lat = $trip_two_to_lat;
                $monthly->trip_two_to_long = $trip_two_to_long;
                $monthly->trip_two_pickup_time = $trip_two_pickup_time;
                $monthly->starting_date = date("Y-m-d", strtotime($starting_date));
                $monthly->cab_type = 5;
                $monthly->status = '0';
                $monthly->created_at = date("Y-m-d h:i:s");
                $monthly->save();
                /*if ($is_full_month == 2) {
                    $days = explode(',', $specific_days);

                    foreach ($days as $day) {
                        DB::table('specific_days')->insert(
                            array('monthly_id' => $monthly->id, 'owner_id' => $owner_id, 'days' => $day, 'created_at' => date("Y-m-d h:i:s")));
                    }
                }*/
                //$captains = get_walker_monthly($monthly);
                /*if ($monthly->subscription_type == 1)
                    request_walker_monthly($monthly);
                else
                    Monthly::where('id', $monthly->id)->update(array('status' => 17));*/
                request_walkers_monthly($monthly);
                $monthly = Monthly::find($monthly->id);
                $response_array = array(
                    'success' => true,
                    'message' => "Your monthly subscription registered successfully",
                    'monthly_id' => $monthly->id,
                    'total' => 'Kasper Cab Customer Service will contact you.',
                    'time' => $monthly->duration,
                    'distance' => $monthly->distance
                );
                $response_code = 200;
            } else {
                $monthly = Monthly::where('owner_id', '=', $owner_id)->whereNotIn('status', [2, 4, 6, 7, 8, 9, 10, 11, 12, 14, 15])->first();
                $response_array = array('success' => false, 'monthly_in_progress' => $monthly->id, 'error' => 'Your current subcription contract is under process.You cannot make a new subscription until it get processed.', 'error_code' => 505);
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function confirm_monthly_walker()
    {
        $owner_id = Input::get('id');
        $token = Input::get('token');
        $monthly_id = Input::get('monthly_id');
        $walker_id = Input::get('walker_id');
        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'token' => $token,
                'monthly_id' => $monthly_id,
                'walker_id' => $walker_id,
            ), array(
                'owner_id' => 'required|integer',
                'token' => 'required',
                'monthly_id' => 'required|integer',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    //Do necessary operations
                    $monthly = Monthly::find($monthly_id);
                    if (isset($monthly) && $monthly->owner_id == $owner_id) {
                        $request = Requests::find($monthly->request_id);
                        $request->current_walker = $walker_id;
                        $request->confirmed_walker = $walker_id;
                        $request->status = 1;
                        $request->save();

                        $monthly->status = 3;
                        $monthly->save();
                        RequestMeta::where('request_id', $request->id)->where('walker_id', $walker_id)->update(array('status' => 1));
                        RequestMeta::where('request_id', $request->id)->where('status', '0')->update(array('status' => 2));
                        $response_array = array('success' => true, 'message' => 'Captain Confirmed',
                            'total' => $monthly->cost,
                            'time' => $monthly->duration,
                            'distance' => $monthly->distance);
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'No Monthly found', 'error_code' => 411);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function captain_details()
    {
        $owner_id = Input::get('id');
        $token = Input::get('token');
        $walker_id = Input::get('walker_id');
        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'owner_id' => 'required|integer',
                'token' => 'required',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    //Do necessary operations

                    $walker = Walker::find($walker_id);
                    if ($walker) {
                        $walker_data = array();
                        $walker_data['first_name'] = $walker->first_name;
                        $walker_data['last_name'] = $walker->last_name;
                        $walker_cab_photo = WalkerDocument::where('walker_id', $walker_id)->where('document_id', 4)->first();
                        if (isset($walker_cab_photo)) {
                            $walker_data['cab_picture'] = $walker_cab_photo->url;
                        } else {
                            $walker_data['cab_picture'] = null;
                        }
                        $walker_data['cab_model'] = $walker->car_model;
                        $walker_data['rating'] = $walker->rate;
                        $walker_ratings = WalkerReview::where('walker_id', $walker_id)->get();
                        $walker_ratings = DB::table('review_walker')->where('review_walker.walker_id', '=', $walker_id)->join('owner', 'owner.id', '=', 'review_walker.owner_id')->orderBy('review_walker.id', 'desc')->get(['review_walker.*', 'owner.first_name', 'owner.last_name']);

                        $walker_rating = array();
                        $i = 0;
                        foreach ($walker_ratings as $key) {
                            $walker_rating[$i]['date'] = $key->created_at;
                            $walker_rating[$i]['customer_name'] = $key->first_name . ' ' . $key->first_name;
                            $walker_rating[$i]['rating'] = $key->rating;
                            $walker_rating[$i]['comments'] = $key->comment;
                            $i++;
                        }
                        $walker_data['ratings'] = $walker_rating;
                        $response_array = array('success' => true, 'walker_ratings' => $walker_rating);
                        $response_code = 200;

                    } else {
                        $response_array = array('success' => false, 'error' => 'No Captain found', 'error_code' => 411);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function accept_monthly_as_private()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */

                    $monthly = Monthly::where('id', $monthly_id)->first();
                    if (ISSET($monthly)) {
                        request_walker_monthly($monthly);
                        $response_array = array(
                            'success' => true,
                            'message' => "Your monthly subscription registred successfully and waiting for CSR approvel",
                            'monthly_id' => $monthly->id);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function get_assigned_walker_detail()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');


        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */

                    $monthly = Monthly::where('id', $monthly_id)->first();

                    if (ISSET($monthly)) {
                        $request = Requests::where('id', $monthly->request_id)->first();

                        if (ISSET($request)) {
                            $walker = Walker::where('id', $request->confirmed_walker)->first();

                            if (ISSET($walker)) {

                                $request_data = array();
                                $request_data['request_id'] = $monthly->request_id;


                                $request_data['id'] = $monthly->owner_id;

                                $request_data['subscription_type'] = $monthly->subscription_type;
                                $request_data['trip_type'] = $monthly->trip_type;
                                $request_data['is_full_month'] = $monthly->is_full_month;

                                $request_data['trip_one_from_address'] = $monthly->trip_one_from_address;
                                $request_data['trip_one_to_address'] = $monthly->trip_one_to_address;
                                $request_data['trip_two_from_address'] = $monthly->trip_two_from_address;
                                $request_data['trip_two_to_address'] = $monthly->trip_two_to_address;

                                $request_data['trip_one_from_latitude'] = $monthly->trip_one_from_lat;
                                $request_data['trip_one_from_longitude'] = $monthly->trip_one_from_long;
                                $request_data['trip_one_to_latitude'] = $monthly->trip_one_to_lat;
                                $request_data['trip_one_to_longitude'] = $monthly->trip_one_to_long;

                                $request_data['trip_two_from_latitude'] = $monthly->trip_two_from_lat;
                                $request_data['trip_two_from_longitude'] = $monthly->trip_two_from_long;
                                $request_data['trip_two_to_latitude'] = $monthly->trip_two_to_lat;
                                $request_data['trip_two_to_longitude'] = $monthly->trip_two_to_long;

                                $request_data['trip_one_pickup_time'] = $monthly->trip_one_pickup_time;
                                $request_data['trip_two_pickup_time'] = $monthly->trip_two_pickup_time;

                                $request_data['starting_date'] = $monthly->starting_date;
                                $request_data['end_date'] = date('Y-m-d', strtotime($monthly->starting_date . ' + 29 days'));

                                $request_data['total_cost'] = $request->total . " SAR";
                                $request_data['distance'] = $request->distance;
                                $request_data['duration'] = $request->time;

                                $request_data['walker'] = array();
                                $request_data['walker']['name'] = $walker->first_name . " " . $walker->last_name;
                                $request_data['walker']['picture'] = $walker->picture;
                                $walker_cab_photo = WalkerDocument::where('walker_id', $request->confirmed_walker)->where('document_id', 4)->first();
                                if (isset($walker_cab_photo)) {
                                    $request_data['walker']['cab_picture'] = $walker_cab_photo->url;
                                } else {
                                    $request_data['walker']['cab_picture'] = null;
                                }
                                $request_data['walker']['phone'] = $walker->phone;
                                $request_data['walker']['email'] = $walker->email;
                                $request_data['walker']['address'] = $walker->address;
                                $request_data['walker']['car_model'] = $walker->car_model;
                                $request_data['walker']['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;

                                $request_data['walker']['rating'] = $walker->rate;
                                $response_array = array('success' => true, 'subscription' => $request_data);

                            } else {
                                $response_array = array('success' => false, 'error' => 'Walker not assigned for the subscription', 'error_code' => 402);
                            }

                        } else {
                            $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid monthly ID', 'error_code' => 402);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function monthly_confirmation_respond()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $payment_mode = Input::get('payment_mode');
        $accepted = Input::get('accepted');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $monthly = Monthly::where('id', $monthly_id)->first();

                    if (ISSET($monthly)) {
                        if ($accepted == 1) {


                            Monthly::where('id', '=', $monthly_id)->update(array('status' => 5));

                            if ($monthly->request_id != '0') {
                                $request = Requests::find($monthly->request_id);
                                $request->status = 0;
                                $request->save();
                            }

                            $monthly = Monthly::where('id', $monthly_id)->first();

                            $request = Requests::where('id', $monthly->request_id)->first();
                            if (isset($payment_mode)) {
                                if ($payment_mode == 2) {
                                    Monthly::where('id', '=', $monthly_id)->update(array('status' => 11));

                                    if ($monthly->request_id != '0')
                                        Requests::where('id', '=', $monthly->request_id)->update(array('status' => 0));
                                } else {
                                    Monthly::where('id', '=', $monthly_id)->update(array('status' => 10));

                                    if ($monthly->request_id != '0')
                                        Requests::where('id', '=', $monthly->request_id)->update(array('status' => 0));
                                }
                            }

                            $response_array = array('success' => true, 'message' => 'Subscription Confirmed Successfully',
                                'total_cost' => $request->total . " SAR",
                                'distance' => $request->distance,
                                'duration' => $request->time,
                                'new_request_status' => (string)$monthly->status,
                                'accepted' => $accepted);
                            $response_code = 200;

                            /* By saravanan
                            $msg_array['monthly_id'] = $monthly->id;
                            $msg_array['new_request_status'] = $monthly->status;
                            $msg_array['montlhy_or_daily'] = $request->service_type;

                            $title = "User Confirmed";

                            $message = $msg_array;

                            send_notifications($request->current_walker, "walker", $title, $message);
                             By saravanan */


                        } else if ($accepted == 2) {
                            Monthly::where('id', '=', $monthly_id)->update(array('status' => 6));

                            if ($monthly->request_id != '0') {
                                Requests::where('id', '=', $monthly->request_id)->update(array('status' => 0, 'is_cancelled' => 1));
                            }

                            $monthly = Monthly::where('id', $monthly_id)->first();
                            /* By saravanan */
                            $msg_array['monthly_id'] = $monthly->id;
                            $msg_array['new_request_status'] = (string)$monthly->status;
                            $request = Requests::where('id', $monthly->request_id)->first();
                            $msg_array['montlhy_or_daily'] = $request->service_type;
                            $msg_array['request_id'] = $monthly->request_id;

                            $owne = Owner::where('id', $request->owner_id)->first();
                            $ownerName = $owne->first_name . " " . $owne->last_name;
                            $sDate = date('d/m/Y', strtotime($monthly['starting_date']));
                            //$title = "User Cancelled";
                            $title = "Customer " . $ownerName . " has cancelled the ride which starts on " . $sDate;

                            $message = $msg_array;

                            send_notifications($request->current_walker, "walker", $title, $message);
                            /* By saravanan */


                            $response_array = array('success' => true, 'message' => 'Subscription Cancelled Successfully');
                            $response_code = 200;
                        } else {

                            Monthly::where('id', '=', $monthly_id)->update(array('status' => 12));

                            //if($monthly->request_id != '0')
                            Requests::where('id', '=', $monthly->request_id)->update(array('status' => 0, 'current_walker' => 0, 'is_cancelled' => 1));
                            RequestMeta::where('request_id', $monthly->request_id)->where('status', 0)->update(array('status' => 3));

                            $response_array = array('success' => true, 'message' => 'User cancel the monthly contract request.');
                            $response_code = 200;

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function user_ongoing_subscription()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
            )
        );

        /* $var = Keywords::where('id', 2)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {

                    $ongoing_service = DB::table('monthly')
                        ->where('monthly.owner_id', $owner_id)
                        ->whereIn('monthly.status', array(7, 10, 11))
                        ->where('request.is_completed', 0)
                        ->where('request.service_type', 2)
                        ->where('request.is_cancelled', 0)
                        ->where(DB::raw('CURDATE()'), '<', DB::raw('DATE_ADD(monthly.starting_date, INTERVAL 1 MONTH)'))
                        ->leftJoin('request', 'request.id', '=', 'monthly.request_id')
                        ->leftJoin('walker', 'walker.id', '=', 'request.confirmed_walker')
                        ->select('monthly.*', 'walker.first_name as walker_first_name', 'walker.last_name as walker_last_name', 'walker.phone as walker_phone', 'walker.email as walker_email', 'walker.picture as walker_picture',
                            'walker.last_name as walker_last_name', 'walker.car_model as car_model', 'walker.car_number_plate_number as car_number_plate_number', 'walker.car_number_plate_letter_1 as car_number_plate_letter_1',
                            'walker.car_number_plate_letter_2 as car_number_plate_letter_2', 'walker.car_number_plate_letter_3 as car_number_plate_letter_3', 'walker.car_number as car_number', 'walker.rate as walker_rate', 'walker.rate_count as walker_rate_count',
                            'request.id as request_id', 'request.payment_mode')
                        ->orderBy('monthly.id', 'desc')
                        ->get();

                    $all_requests = array();


                    foreach ($ongoing_service as $ongoing) {
                        $owner = Owner::find($ongoing->owner_id);


                        $request_data = array();
                        $request_data['request_id'] = $ongoing->request_id;
                        $request_data['owner_id'] = $ongoing->owner_id;
                        $request_data['monthly_id'] = $ongoing->id;

                        $request_data['owner'] = array();
                        $request_data['owner']['new_request_status'] = (string)$ongoing->status;
                        $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                        $request_data['owner']['picture'] = $owner->picture;
                        $request_data['owner']['phone'] = $owner->phone;
                        $request_data['owner']['address'] = '';

                        $request_data['owner']['subscription_type'] = $ongoing->subscription_type;
                        $request_data['owner']['trip_type'] = $ongoing->trip_type;
                        $request_data['owner']['is_full_month'] = $ongoing->is_full_month;

                        $request_data['owner']['trip_one_from_address'] = $ongoing->trip_one_from_address;
                        $request_data['owner']['gender'] = $ongoing->gender;
                        $request_data['owner']['trip_one_to_address'] = $ongoing->trip_one_to_address;
                        $request_data['owner']['trip_two_from_address'] = $ongoing->trip_two_from_address;
                        $request_data['owner']['trip_two_to_address'] = $ongoing->trip_two_to_address;

                        $request_data['owner']['trip_one_from_latitude'] = $ongoing->trip_one_from_lat;
                        $request_data['owner']['trip_one_from_longitude'] = $ongoing->trip_one_from_long;
                        $request_data['owner']['trip_one_to_latitude'] = $ongoing->trip_one_to_lat;
                        $request_data['owner']['trip_one_to_longitude'] = $ongoing->trip_one_to_long;

                        $request_data['owner']['trip_two_from_latitude'] = $ongoing->trip_two_from_lat;
                        $request_data['owner']['trip_two_from_longitude'] = $ongoing->trip_two_from_long;
                        $request_data['owner']['trip_two_to_latitude'] = $ongoing->trip_two_to_lat;
                        $request_data['owner']['trip_two_to_longitude'] = $ongoing->trip_two_to_long;

                        $request_data['owner']['trip_one_pickup_time'] = $ongoing->trip_one_pickup_time;
                        $request_data['owner']['trip_two_pickup_time'] = $ongoing->trip_two_pickup_time;

                        $request_data['owner']['starting_date'] = $ongoing->starting_date;
                        $request_data['owner']['end_date'] = date('Y-m-d', strtotime($ongoing->starting_date . ' + 29 days'));

                        $request_data['owner']['rating'] = $owner->rate;
                        $request_data['owner']['num_rating'] = $owner->rate_count;
                        /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                          $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                        $request_data['owner']['payment_type'] = $ongoing->payment_mode;

                        $request_data['walker'] = array();
                        $request_data['walker']['name'] = $ongoing->walker_first_name . " " . $ongoing->walker_last_name;
                        $request_data['walker']['phone'] = $ongoing->walker_phone;
                        $request_data['walker']['email'] = $ongoing->walker_email;
                        $request_data['walker']['picture'] = $ongoing->walker_picture;
                        $request_data['walker']['car_model'] = $ongoing->car_model;
                        $request_data['walker']['car_number'] = $ongoing->car_number_plate_letter_1 . " " . $ongoing->car_number_plate_letter_2 . " " . $ongoing->car_number_plate_letter_3 . " " . $ongoing->car_number_plate_number;;
                        $request_data['walker']['rate'] = $ongoing->walker_rate;
                        $request_data['walker']['rate_count'] = $ongoing->walker_rate_count;

                        $request_data['payment_mode'] = $ongoing->payment_mode;

                        $data['request_data'] = $request_data;

                        array_push($all_requests, $data);
                    }

                    $is_onging = '';

                    $response_array = array('success' => true, 'is_onging' => '', 'is_new_subscription' => '', 'ongoing' => $all_requests);
                    $response_code = 200;


                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }


        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function monthly_payment_mode()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $payment_mode = Input::get('payment_mode');
        $card_id = Input::get('card_id');
        $customer_ip = Request::ip();
        Log::info("customer ip" . $customer_ip);

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
                'payment_mode' => $payment_mode
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer',
                'payment_mode' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {

                    $monthly = Monthly::where('id', $monthly_id)->first();

                    if ($monthly) {

                        if ($payment_mode == 1) {
                            //
                            Monthly::where('id', '=', $monthly_id)->update(array('status' => 10));
                            $response_array = array('success' => true, 'message' => 'Payment will be paid in cash to captain');
                            $response_code = 200;
                            $msg_array['monthly_id'] = $monthly->id;
                            $msg_array['new_request_status'] = (string)$monthly->status;

                            $request = Requests::where('id', $monthly->request_id)->first();

                            $msg_array['request_id'] = $monthly->request_id;

                            $msg_array['montlhy_or_daily'] = $request->service_type;

                            $owne = Owner::where('id', $request->owner_id)->first();
                            $ownerName = $owne->first_name . " " . $owne->last_name;
                            $sDate = date('d/m/Y', strtotime($monthly['starting_date']));
                            //$title = "User confirmation";
                            $title = "Customer " . $ownerName . " has confirmed the ride which starts on " . $sDate . '. Customer will pay amount in cash.';

                            $message = $msg_array;

                            send_notifications($request->current_walker, "walker", $title, $message);

                        } else {

                            $request = Requests::find($monthly->request_id);

                            $payment_data = Payment::find($card_id);
                            if (!isset($payment_data)) {
                                $payment_data = Payment::where('owner_id', $owner_id)->where('is_default', 1)->first();
                                $monthlys = Monthly::where('id', $monthly_id)->first();

                                if (!$payment_data)
                                    $payment_data = Payment::where('owner_id', $owner_id)->first();
                            }
                            if ($payment_data) {
                                $setransfer = Settings::where('key', 'transfer')->first();
                                $transfer_allow = $setransfer->value;
                                //dd($customer_id);
                                try {
                                    $merchant_reference = 'kc' . $owner_data->id . '-' . ($monthly->cost * 100) . '_' . microtime(true);
                                    $requestparameterstring = 'asdaredcsreaccess_code=Z6ouzQeUwoNwpTxcqRkzamount=' . ($monthly->cost * 100) . 'command=PURCHASEcurrency=SARcustomer_email=' . $owner_data->email . 'customer_ip=' . $customer_ip .
                                        'eci=MOTOlanguage=enmerchant_identifier=fLPmDVuxmerchant_reference=' . $merchant_reference . 'token_name=' . $payment_data->card_token . 'asdaredcsre';
                                    $signature = hash("sha256", $requestparameterstring, false);
                                    $client = new GuzzleHttp\Client();
                                    $response = $client->post("https://sbpaymentservices.payfort.com/FortAPI/paymentApi",
                                        ['json' => [
                                            'access_code' => 'Z6ouzQeUwoNwpTxcqRkz',
                                            'amount' => ($monthly->cost * 100),
                                            'command' => 'PURCHASE',
                                            'currency' => 'SAR',
                                            'customer_email' => $owner_data->email,
                                            'customer_ip' => $customer_ip,
                                            'eci' => 'MOTO',
                                            'language' => 'en',
                                            'merchant_identifier' => 'fLPmDVux',
                                            'merchant_reference' => $merchant_reference,
                                            'signature' => $signature,
                                            'token_name' => $payment_data->card_token
                                        ]]);
                                    Log::info("loooog payfort");
                                    $resobjPayfort = $response->getBody(true);
                                    $objPayfort = json_decode($resobjPayfort, true);
                                    Log::info('$resobjPayfort  = ' . print_r($objPayfort, true));
                                    $response_code = $objPayfort['response_code'];
                                    $response_message = $objPayfort['response_message'];
                                    $fort_id = $objPayfort['fort_id'];
                                    $status = $objPayfort['status'];
                                    $transaction_history = new TransactionHistory();
                                    $transaction_history->request_id = $monthly->request_id;
                                    $transaction_history->fort_id = $fort_id;
                                    $transaction_history->amount = $monthly->cost;
                                    $transaction_history->response_message = $response_message;
                                    $transaction_history->response_code = $response_code;
                                    $transaction_history->status = $status;
                                    $transaction_history->save();
                                    Log::info('$response_code  = ' . print_r($response_code, true));
                                    if ($response_code == '14000') {
                                        Monthly::where('id', '=', $monthly_id)->update(array('status' => 11));
                                        Log::info('payement succeded ');
                                        $request->is_paid = 1;
                                        $request->card_payment = $request->total;
                                        $payment_type = "Creditcard Card Payment Successfully";
                                        Log::info('card payment ' . $payment_type);
                                        $request->save();
                                        $response_array = array('success' => true, 'message' => $payment_type);
                                        $response_code = 200;
                                        $monthlys = Monthly::where('id', $monthly_id)->first();
                                        $msg_array['monthly_id'] = $monthlys->id;
                                        $msg_array['new_request_status'] = (string)$monthlys->status;

                                        $request = Requests::where('id', $monthly->request_id)->first();

                                        $msg_array['request_id'] = $monthly->request_id;

                                        $msg_array['montlhy_or_daily'] = $request->service_type;

                                        $owne = Owner::where('id', $request->owner_id)->first();
                                        $ownerName = $owne->first_name . " " . $owne->last_name;
                                        $sDate = date('d/m/Y', strtotime($monthly['starting_date']));
                                        //$title = "User confirmation";
                                        $title = "Customer " . $ownerName . " has confirmed the ride which starts on " . $sDate;

                                        $message = $msg_array;

                                        send_notifications($request->current_walker, "walker", $title, $message);
                                    } else {
                                        Log::info('payement not succeded ');
                                        $request->is_paid = 0;
                                        $request->save();
                                        $payment_type = "Creditcard Card Payment Fail";
                                        /* Client Side Push */
                                        $message = 'Your card is declined.';
                                        /*send_notifications($request->owner_id, "owner", $title, $message);*/
                                        /* Client Side Push END */
                                        /* Driver Side Push */
                                        $response_array = array('success' => false, 'error' => $message);
                                        $response_code = 200;
                                    }
                                } catch (Exception $e) {
                                    Log::info($e->getMessage());
                                    $request->is_paid = 0;
                                    // Invalid parameters were supplied to Stripe's API
                                    $response_array = array('error' => $e->getMessage());
                                    $response_code = 200;
                                }

                            } else {
                                $response_array = array('success' => false, 'error' => 'No card found', 'error_code' => 405);
                                $response_code = 200;
                            }
                        }


                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid Monthly ID', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }


        $response = Response::json($response_array, $response_code);
        return $response;
    }

    /*public function user_monthly_status() {

        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
                        array(
                    'token' => $token,
                    'walker_id' => $walker_id
                        ), array(
                    'token' => 'required',
                    'walker_id' => 'required|integer'
                        )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                         $ongoing_service = DB::table('request')
                                ->where('request.confirmed_walker', $walker_id)
                                ->where('request.is_completed', 0)
                                ->where('request.status', 1)
                                ->where('request.service_type', 2)
                                ->where('monthly.status', 3)
                                ->leftJoin('monthly', 'request.id', '=', 'monthly.request_id')
                                ->select('monthly.*', 'request.id as request_id', 'request.payment_mode')
                                ->orderBy('request.created_at', 'desc')
                                ->get();

                        $all_requests = array();

                        foreach($ongoing_service as $ongoing)
                        {
                            $owner = Owner::find($ongoing->owner_id);


                            $request_data = array();
                            $request_data['request_id'] = $ongoing->request_id;
                            $request_data['id'] = $ongoing->owner_id;

                            $request_data['owner'] = array();
                            $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                            $request_data['owner']['picture'] = $owner->picture;
                            $request_data['owner']['phone'] = $owner->phone;
                            $request_data['owner']['address'] = '';


                            $request_data['owner']['subscription_type'] = $ongoing->subscription_type;
                            $request_data['owner']['trip_type'] = $ongoing->trip_type;
                            $request_data['owner']['is_full_month'] = $ongoing->is_full_month;
                            $request_data['owner']['trip_one_from_latitude'] = $ongoing->trip_one_from_lat;
                            $request_data['owner']['trip_one_from_longitude'] = $ongoing->trip_one_from_long;
                            $request_data['owner']['trip_one_to_latitude'] = $ongoing->trip_one_to_lat;
                            $request_data['owner']['trip_one_to_longitude'] = $ongoing->trip_one_to_long;

                            $request_data['owner']['trip_two_from_latitude'] = $ongoing->trip_two_from_lat;
                            $request_data['owner']['trip_two_from_longitude'] = $ongoing->trip_two_from_long;
                            $request_data['owner']['trip_two_to_latitude'] = $ongoing->trip_two_to_lat;
                            $request_data['owner']['trip_two_to_longitude'] = $ongoing->trip_two_to_long;

                                $request_data['owner']['trip_one_pickup_time'] = $ongoing->trip_one_pickup_time;
                                $request_data['owner']['trip_two_pickup_time'] = $ongoing->trip_two_pickup_time;

                                $request_data['owner']['starting_date'] = $ongoing->starting_date;
                                $request_data['owner']['end_date'] = date('Y-m-d', strtotime($ongoing->starting_date. '+ 30 days'));
                          
                            $request_data['owner']['rating'] = $owner->rate;
                            $request_data['owner']['num_rating'] = $owner->rate_count;
                            /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                              $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
    /* $request_data['owner']['payment_type'] = $ongoing->payment_mode;
    $request_data['payment_mode'] = $ongoing->payment_mode;
    $request_data['dog'] = array();

    if ($dog = Dog::find($owner->dog_id)) {

        $request_data['dog']['name'] = $dog->name;
        $request_data['dog']['age'] = $dog->age;
        $request_data['dog']['breed'] = $dog->breed;
        $request_data['dog']['likes'] = $dog->likes;
        $request_data['dog']['picture'] = $dog->image_url;
    }
    $data['request_data'] = $request_data;
    array_push($all_requests, $data);


}

$response_array = array('success' => true, 'ongoing' => $all_requests);
$response_code = 200;

}else {
$response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
$response_code = 200;
}
}
else {
if ($is_admin) {
/* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
    /* $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
 } else {
     $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
 }
 $response_code = 200;
}
}

$response = Response::json($response_array, $response_code);
return $response;
} */


    public function respond_monthly_subscription()
    {
        Log::info('respond_monthly_subscription');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $request_id = Input::get('request_id');
        $monthly_id = Input::get('monthly_id');
        $accepted = Input::get('accepted');
        $validator = Validator::make(
            array(
                'token' => $token,
                '$owner_id' => $owner_id,
                'request_id' => $request_id,
                'accepted' => $accepted,
            ), array(
                'token' => 'required',
                '$owner_id' => 'required|integer',
                'accepted' => 'required|integer',
                'request_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            //$is_admin = $this->isAdmin($token);
            /*if ($request = Requests::find($request_id)){
                 $is_admin = $this->isAdmin($token);
                     if ($walker_data = $this->getWalkerData($owner_id, $token, $is_admin)) {
                         // check for token validity
                         if (is_token_active($walker_data->token_expiry)) {*/
            if ($accepted == 0) {


                $is_admin = $this->isAdmin($token);

                if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($owner_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {

                            if ($request->owner_id == $owner_data->id) {

                                Requests::where('id', $request_id)->update(array('is_cancelled' => 1));
                                RequestMeta::where('request_id', $request_id)->update(array('is_cancelled' => 1));

//                                if ($request->promo_id) {
//                                    $promo_update_counter = PromoCodes::find($request->promo_id);
//                                    $promo_update_counter->uses = $promo_update_counter->uses + 1;
//                                    $promo_update_counter->save();
//
//                                    UserPromoUse::where('user_id', '=', $owner_id)->where('code_id', '=', $request->promo_id)->delete();
//
//                                    $owner = Owner::find($owner_id);
//                                    $owner->promo_count = $owner->promo_count - 1;
//                                    $owner->save();
//
//                                    $request = Requests::find($request_id);
//                                    $request->promo_id = 0;
//                                    $request->promo_code = "";
//                                    $request->save();
//                                }

                                if ($request->confirmed_walker) {
                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker->is_available = 1;
                                    $walker->save();
                                }

                                $monthly = Monthly::where('id', $monthly_id)->first();
                                $request = Requests::where('id', $monthly->request_id)->first();
                                $owner = Owner::find($request->owner_id);
                                $owner_id = $request->owner_id;
                                $walker_id = $request->confirmed_walker;
                                $walker = Walker::find($walker_id);
                                $msg_array = array();
                                $msg_array['id'] = $walker_id;
                                $msg_array['unique_id'] = 19;
                                $msg_array['time_left_to_respond'] = 10;
                                $request_data = array();
                                $request_data['owner'] = array();
                                $request_data['request_id'] = $request->id;
                                $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                                $request_data['owner']['picture'] = $owner->picture;
                                $request_data['owner']['phone'] = $owner->phone;
                                $request_data['owner']['address'] = '';
                                $request_data['owner']['latitude'] = $owner->latitude;
                                $request_data['owner']['longitude'] = $owner->longitude;
                                $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                                $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                                $request_data['owner']['payment_type'] = $request->payment_mode;
                                $request_data['owner']['rating'] = $owner->rate;
                                $request_data['owner']['num_rating'] = $owner->rate_count;
                                $request_data['dog'] = array();
                                $request_data['owner']["starting_date"] = "2016-11-07";
                                $request_data['owner']["subscription_type"] = $request->service_type;
                                $request_data['owner']["total_cost"] = $request->total;
                                $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                                $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                                $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                                $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                                $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                                $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                                $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                                $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                                $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                                $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                                $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                                $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                                $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                                $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                                $request_data['owner']["trip_type"] = $monthly->cab_type;
                                $request_data['success'] = 1;

                                $msg_array = array('success' => true);
                                $msg_array['payment_mode'] = $request->payment_mode;
                                $msg_array['monthly_id'] = $monthly->id;
                                $msg_array['request_data'] = $request_data;
                                $msg_array['id'] = $request->confirmed_walker;
                                $msg_array['unique_id'] = 21;
                                //$title = "New Monthly Subscription Request";
                                $title = "Customer " . $owner->first_name . " declined for today's monthly subscription?";
                                $message = $msg_array;
                                Log::info('response = ' . print_r($message, true));
                                Log::info('New request = ' . print_r($message, true));
                                send_notifications($request->confirmed_walker, "walker", $title, $message);
                                $response_array = array('success' => true, 'message' => 'Pust Sent to Captain on customer rejection');

                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . Config::get('app.generic_keywords.User') . ' ID', 'error_code' => 407);

                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }
            } else {

                $monthly = Monthly::where('id', $monthly_id)->first();
                if (ISSET($monthly)) {
                    $request = Requests::where('id', $monthly->request_id)->first();

                    if (ISSET($request)) {
                        $walker = Walker::where('id', $request->confirmed_walker)->first();
                        Requests::where('id', $request_id)->update(array('status' => 1));

                        if (ISSET($walker)) {
                            $owner = Owner::find($request->owner_id);
                            $request_data = array();
                            $walker_timezone = $walker->timezone;
                            $default_timezone = Config::get('app.timezone');

                            $date_time = get_user_time($default_timezone, $walker_timezone, $request->request_start_time);

                            $request_data['accepted_time'] = $date_time;
                            $request_data['payment_mode'] = $request->payment_mode;
                            $request_data['payment_type'] = $request->payment_mode;
                            $request_data['card_details'] = "";
                            $request_data['is_walker_started'] = $request->is_walker_started;
                            $request_data['is_walker_arrived'] = $request->is_walker_arrived;
                            $request_data['is_started'] = $request->is_started;
                            $request_data['is_completed'] = $request->is_completed;
                            $request_data['is_dog_rated'] = $request->is_dog_rated;
                            $request_data['is_cancelled'] = $request->is_cancelled;
                            $request_data['dest_latitude'] = $request->D_latitude;
                            $request_data['dest_longitude'] = $request->D_longitude;
                            $request_data['owner'] = array();
                            $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                            $request_data['owner']['picture'] = $owner->picture;
                            $request_data['owner']['phone'] = $owner->phone;
                            $request_data['owner']['address'] = '';
                            $request_data['owner']['latitude'] = $monthly->trip_one_from_lat;
                            $request_data['owner']['longitude'] = $monthly->trip_one_from_long;
                            if ($request->D_latitude != NULL) {
                                $request_data['owner']['d_latitude'] = $request->D_latitude;
                                $request_data['owner']['d_longitude'] = $request->D_longitude;
                            }
                            $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                            $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                            $request_data['owner']['dest_latitude'] = $request->D_latitude;
                            $request_data['owner']['dest_longitude'] = $request->D_longitude;
                            $request_data['owner']['rating'] = $owner->rate;
                            $request_data['owner']['num_rating'] = $owner->rate_count;
                            /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                              $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                            $charge = array();

                            /* $settings = Settings::where('key', 'default_distance_unit')->first();
                              $unit = $settings->value;
                              if ($unit == 0) {
                              $unit_set = 'kms';
                              } elseif ($unit == 1) {
                              $unit_set = 'miles';
                              } */
                            $bill = array();
                            $dog = array();
                            $request_data['bill'] = $bill;
                            $request_data['dog'] = $dog;
                            $settings = Settings::where('key', 'default_distance_unit')->first();
                            $unit = $settings->value;
                            if ($unit == 0) {
                                $unit_set = 'kms';
                            } elseif ($unit == 1) {
                                $unit_set = 'miles';
                            }
                            $requestserv = RequestServices::where('request_id', $request->id)->first();
                            $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();
                            $setbase_distance = $request_typ->base_distance;
                            $base_price = $request_typ->base_price;
                            $price_per_unit_distance = $request_typ->price_per_unit_distance;
                            $price_per_unit_time = $request_typ->price_per_unit_time;
                            $charge['unit'] = $unit_set;
                            $charge['base_distance'] = $setbase_distance;
                            $charge['base_price'] = ($base_price);
                            $charge['distance_price'] = ($price_per_unit_distance);
                            $charge['price_per_unit_time'] = ($price_per_unit_time);
                            $charge['total'] = $request->total;
                            $charge['is_paid'] = $request->is_paid;
                            $request_data['charge_details'] = $charge;

                            $request_data['success'] = 1;
                            $request_data['payment_mode'] = $request->payment_mode;
                            $request_data['monthly_id'] = $monthly->id;
                            $msg_array = array('success' => true);
                            $msg_array['id'] = $request->confirmed_walker;
                            $msg_array['bill'] = $bill;
                            $msg_array['is_approved'] = 1;
                            $msg_array['is_approved_txt'] = "Approved";
                            $msg_array['is_available'] = 1;
                            $msg_array['is_available'] = 1;
                            $msg_array['is_available'] = 1;
                            $msg_array['request_data'] = $request_data;
                            $msg_array['time_left_to_respond'] = 10;
                            $msg_array['request_id'] = $request_id;
                            $msg_array['unique_id'] = 22;

                            //$title = "New Monthly Subscription Request";
                            $title = "Customer " . $owner->first_name . " accepted today's monthly subscription?";
                            $message = $msg_array;
                            Log::info('response = ' . print_r($message, true));
                            Log::info('New request = ' . print_r($message, true));
                            send_notifications($request->confirmed_walker, "walker", $title, $message);
                            $response_array = array('success' => true, 'message' => 'Pust Sent to Captain on customer acceptance');

                        } else {
                            $response_array = array('success' => false, 'error' => 'Walker not assigned for the subscription', 'error_code' => 402);
                        }

                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Invalid monthly ID', 'error_code' => 402);
                }
            }


            /*} else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
        } else {
            $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
        }
    }*/
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function invite_friend()
    {
        Log::info('invite friend');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $username = Input::get('username');
        $monthly_id = Input::get('monthly_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'user_name' => $username,
                'monthly_id' => $monthly_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'user_name' => 'required',
                'monthly_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            //$is_admin = $this->isAdmin($token);
            /*if ($request = Requests::find($request_id)){
                 $is_admin = $this->isAdmin($token);
                     if ($walker_data = $this->getWalkerData($owner_id, $token, $is_admin)) {
                         // check for token validity
                         if (is_token_active($walker_data->token_expiry)) {*/
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    if ($monthly = Monthly::find($monthly_id)) {
                        //$request = Requests::find($monthly->request_id);
                        if ($monthly->owner_id == $owner_data->id) {
                            $totalinvites = Invite::where("monthly_id", '=', $monthly->id)->where("owner_id", '=', $owner_data->id)->where("is_cancelled", '=', 0)->where("invite_type", '=', 0)->count();
                            Log::info('Total Invitation = ' . $totalinvites);
                            if ($totalinvites < 1) {


                                $friend = Owner::where("email", '=', $username)->first();

                                if ($friend) {
                                    if ($friend->id != $owner_data->id) {

                                        Monthly::where('id', $monthly->id)->update(array('status' => 19));
                                        $owner_id = $monthly->owner_id;
                                        //$walker_id = $monthly->confirmed_walker;
                                        //$walker = Walker::find($walker_id);
                                        $invite = new Invite();
                                        $invite->owner_id = $owner_id;
                                        $invite->friend_id = $friend->id;
                                        $invite->status = 0;
                                        $invite->monthly_id = $monthly->id;
                                        $invite->is_cancelled = 0;
                                        $invite->invite_type = 0;
                                        $invite->save();
                                        Log::info('New Invitation = ' . print_r($invite, true));
                                        $request_data = array();
                                        $request_data['owner'] = array();
                                        $request_data['request_id'] = 0;
                                        $request_data['owner']['name'] = $owner_data->first_name . " " . $owner_data->last_name;
                                        $request_data['owner']['picture'] = $owner_data->picture;
                                        $request_data['owner']['phone'] = $owner_data->phone;
                                        $request_data['owner']['address'] = $owner_data->address;
                                        $request_data['owner']['latitude'] = $owner_data->latitude;
                                        $request_data['owner']['longitude'] = $owner_data->longitude;
                                        $request_data['owner']['owner_dist_lat'] = $monthly->trip_one_to_lat;
                                        $request_data['owner']['owner_dist_long'] = $monthly->trip_one_to_long;
                                        $request_data['owner']['payment_type'] = 1;
                                        $request_data['owner']['rating'] = $owner_data->rate;
                                        $request_data['owner']['num_rating'] = $owner_data->rate_count;
                                        $request_data['dog'] = array();
                                        $request_data['owner']["starting_date"] = "2016-11-07";
                                        $request_data['owner']["subscription_type"] = $monthly->subscription_type;
                                        $request_data['owner']["total_cost"] = 0;
                                        $request_data['owner']["gender"] = $monthly->gender;
                                        $request_data['owner']["is_full_month"] = $monthly->is_full_month;
                                        $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                                        $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                                        $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                                        $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                                        $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                                        $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                                        $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                                        $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                                        $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                                        $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                                        $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                                        $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                                        $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                                        $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                                        $request_data['owner']["trip_type"] = $monthly->trip_type;
                                        $request_data['owner']["cab_type"] = $monthly->cab_type;
                                        $request_data['success'] = 1;

                                        $msg_array = array('success' => true);
                                        $msg_array['payment_mode'] = 1;
                                        $msg_array['monthly_id'] = $monthly->id;
                                        $msg_array['invite_id'] = $invite->id;
                                        $msg_array['request_data'] = $request_data;
                                        $msg_array['walker_id'] = 0;
                                        $msg_array['unique_id'] = 31;
                                        //$title = "New Monthly Subscription Request";
                                        $title = "Customer " . $owner_data->first_name . " invited you to join following monthly subscription?";
                                        $message = $msg_array;
                                        //Log::info('response = ' . print_r($message, true));
                                        //Log::info('New Invite for request = ' . print_r($message, true));
                                        send_notifications($friend->id, "owner", $title, $message);
                                        $response_array = array('success' => true, 'invite_id' => $invite->id, 'friend_name' => $friend->first_name . ' ' . $friend->last_name, 'message' => 'Pust Sent to customer on customer inviation');

                                    } else {
                                        /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                        $response_array = array('success' => false, 'error' => 'You can not invite yourself.', 'error_code' => 450);
                                    }
                                } else {
                                    /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                    $response_array = array('success' => false, 'error' => 'Friend ID not found', 'error_code' => 450);

                                }
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Max Invitations Exceed 3', 'error_code' => 451);
                            }

                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                            $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . Config::get('app.generic_keywords.User') . ' ID', 'error_code' => 407);

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);

                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            /*} else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
        } else {
            $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
        }
    }*/
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    // Get Invites
    public function get_invites()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
            ), array(
            'token' => 'required',
            'owner_id' => 'required|integer',
            'monthly_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    if ($monthly = Monthly::find($monthly_id)) {
                        if ($monthly->owner_id == $owner_data->id) {
                            //$invites = Invite::where('owner_id','=',$owner_id)->where('monthly_id','=',$monthly_id)->where('is_cancelled','=',0)->get();
                            $invites = DB::table('invite')->where('invite.owner_id', '=', $owner_id)->where('invite.monthly_id', '=', $monthly_id)->join('owner', 'owner.id', '=', 'invite.friend_id')->orderBy('invite.id', 'desc')->get(['invite.*', 'owner.first_name', 'owner.last_name']);
                            $pusharr = array();
                            foreach ($invites as $invite) {
                                $data['invite_id'] = $invite->id;
                                $data['monthly_id'] = $invite->monthly_id;
                                $data['owner_id'] = $invite->owner_id;
                                $data['friend_id'] = $invite->friend_id;
                                $data['status'] = $invite->status;
                                $data['is_cancelled'] = $invite->is_cancelled;
                                $data['friend_name'] = $invite->first_name . " " . $invite->last_name;
                                array_push($pusharr, $data);
                            }
                            $response_array = array('success' => true, 'invites' => $pusharr, 'success_code' => 200);

                        } else {
                            $response_array = array('success' => false, 'error' => 'Monthly ID does not match with user id', 'error_code' => 411);

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Monthly ID Not Found', 'error_code' => 408);

                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }

            } else {
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Join details
    public function get_join()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $base_monthly_id = Input::get('base_monthly_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'base_monthly_id' => $base_monthly_id,
            ), array(
            'token' => 'required',
            'owner_id' => 'required|integer',
            'base_monthly_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    if ($monthly = Monthly::find($base_monthly_id)) {
                        if ($monthly->owner_id == $owner_data->id) {
                            //$invites = Invite::where('owner_id','=',$owner_id)->where('monthly_id','=',$monthly_id)->where('is_cancelled','=',0)->get();
                            $joins = DB::table('joins')->where('joins.owner_id', '=', $owner_id)
                                ->where('joins.is_cancelled', '=', 0)
                                ->where('joins.base_monthly_id', '=', $base_monthly_id)
                                ->join('walker', 'walker.id', '=', 'joins.walker_id')->orderBy('joins.id', 'desc')
                                ->get(['joins.*', 'walker.first_name', 'walker.last_name']);
                            $pusharr = array();
                            foreach ($joins as $join) {
                                $data['join_id'] = $join->id;
                                $data['monthly_id'] = $join->monthly_id;
                                $data['owner_id'] = $join->owner_id;
                                $data['walker_id'] = $join->walker_id;
                                $data['status'] = $join->status;
                                $data['is_cancelled'] = $join->is_cancelled;
                                $data['walker_name'] = $join->first_name . " " . $join->last_name;
                                array_push($pusharr, $data);
                            }
                            $response_array = array('success' => true, 'joins' => $pusharr, 'success_code' => 200);

                        } else {
                            $response_array = array('success' => false, 'error' => 'Monthly ID does not match with user id', 'error_code' => 411);

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Monthly ID Not Found', 'error_code' => 408);

                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }

            } else {
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // respond invite
    public function respond_invite()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $invite_id = Input::get('invite_id');
        $accepted = Input::get('accepted');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'invite_id' => $invite_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'invite_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $invite = Invite::where('id', $invite_id)->first();

                    if (ISSET($invite)) {
                        if ($accepted == 1) {
                            Monthly::where('id', $invite->monthly_id)->update(array('status' => 20));
                            Invite::where('id', '=', $invite_id)->update(array('status' => 1));
                            $msg_array['monthly_id'] = $invite->monthly_id;
                            $msg_array['invite_id'] = $invite->id;
                            $msg_array['friend_id'] = $invite->friend_id;
                            $msg_array['status'] = 1;
                            $msg_array['unique_id'] = 32;
                            $monthly = Monthly::find($invite->monthly_id);
                            if ($monthly->request_id == 0) {
                                request_walker_monthly($monthly);
                            }
                            $ownerName = $owner_data->first_name . " " . $owner_data->last_name;
                            //$title = "User Cancelled";
                            $title = "Customer " . $ownerName . " accepted your invitation to share ride";
                            $message = $msg_array;
                            send_notifications($invite->owner_id, "owner", $title, $message);
                            $response_array = array('success' => true, 'message' => 'Subscription Confirmed Successfully',
                                'invite_id' => $invite->id,
                                'monthly_id' => $invite->monthly_id,
                                'status' => $accepted);
                            $response_code = 200;
                        } else {
                            Monthly::where('id', '=', $invite->monthly_id)->update(array('status' => 23));
                            Invite::where('id', '=', $invite_id)->update(array('is_cancelled' => 1));
                            $msg_array['monthly_id'] = $invite->monthly_id;
                            $msg_array['invite_id'] = $invite->id;
                            $msg_array['is_cancelled'] = 1;
                            $msg_array['friend_id'] = $invite->friend_id;
                            $msg_array['unique_id'] = 33;


                            $ownerName = $owner_data->first_name . " " . $owner_data->last_name;
                            //$title = "User Cancelled";
                            $title = "Customer " . $ownerName . " did not accepted your invitation to share ride";

                            $message = $msg_array;

                            send_notifications($invite->owner_id, "owner", $title, $message);

                            $response_array = array('success' => true, 'message' => 'Invitation Cancelled Successfully');
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // respond invite
    public function cancel_invite()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $invite_id = Input::get('invite_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'invite_id' => $invite_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'invite_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $invite = Invite::where('id', $invite_id)->first();

                    if (ISSET($invite)) {
                        if ($invite->invite_type == 1)
                            Monthly::where('id', '=', $invite->base_monthly_id)->update(array('status' => 17));
                        else
                            Monthly::where('id', '=', $invite->monthly_id)->update(array('status' => 17));
                        Invite::where('id', '=', $invite_id)->update(array('is_cancelled' => 1));
                        $msg_array['monthly_id'] = $invite->monthly_id;
                        $msg_array['invite_id'] = $invite->id;
                        $msg_array['is_cancelled'] = 1;
                        $msg_array['friend_id'] = $invite->friend_id;
                        $msg_array['unique_id'] = 34;
                        $ownerName = $owner_data->first_name . " " . $owner_data->last_name;
                        //$title = "User Cancelled";
                        $title = "Customer " . $ownerName . " cancelled invitation to share ride";

                        $message = $msg_array;

                        send_notifications($invite->friend_id, "owner", $title, $message);

                        $response_array = array('success' => true, 'message' => 'Invitation Cancelled Successfully');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Invites
    public function invite_in_progress()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
            'token' => 'required',
            'owner_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    $invites = DB::table('invite')->where('invite.friend_id', '=', $owner_id)->where('invite.status', '=', 0)->where('invite.is_cancelled', '=', 0)->join('owner', 'owner.id', '=', 'invite.owner_id')->orderBy('invite.id', 'desc')->get(['invite.*', 'owner.first_name', 'owner.last_name']);
                    $pusharr = array();
                    foreach ($invites as $invite) {
                        $data['invite_id'] = $invite->id;
                        $data['monthly_id'] = $invite->monthly_id;
                        $data['owner_id'] = $invite->owner_id;
                        $data['friend_id'] = $invite->friend_id;
                        $data['status'] = $invite->status;
                        $data['invite_type'] = $invite->invite_type;
                        $data['is_cancelled'] = $invite->is_cancelled;
                        $data['friend_name'] = $invite->first_name . " " . $invite->last_name;
                        $monthly = Monthly::find($invite->monthly_id);
                        Log::info('$invite->monthly_id' . $invite->monthly_id);
                        $request = Requests::find($monthly->request_id);
                        $inviter = Owner::find($invite->owner_id);
                        $request_data = array();
                        $request_data['owner'] = array();
                        if (isset($request)) {
                            $request_data['request_id'] = $request->id;
                            $request_data['owner']["subscription_type"] = $request->service_type;
                            $request_data['payment_mode'] = $request->payment_mode;
                            $request_data['confirmed_walker'] = $request->confirmed_walker;
                            $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                            $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                            $request_data['owner']['payment_type'] = $request->payment_mode;
                        }
                        $request_data['owner']['name'] = $inviter->first_name . " " . $inviter->last_name;
                        $request_data['owner']['picture'] = $inviter->picture;
                        $request_data['owner']['phone'] = $inviter->phone;
                        $request_data['owner']['address'] = $inviter->address;
                        $request_data['owner']['latitude'] = $inviter->latitude;
                        $request_data['owner']['longitude'] = $inviter->longitude;
                        $request_data['owner']['rating'] = $inviter->rate;
                        $request_data['owner']['num_rating'] = $inviter->rate_count;
                        $request_data['dog'] = array();
                        $request_data['owner']["starting_date"] = "2016-11-07";
                        $request_data['owner']["total_cost"] = $monthly->cost;
                        $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                        $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                        $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                        $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                        $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                        $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                        $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                        $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                        $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                        $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                        $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                        $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                        $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                        $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                        $request_data['owner']["trip_type"] = $monthly->cab_type;
                        $request_data['success'] = 1;
                        $request_data['invite_id'] = $invite->id;
                        $data['request_data'] = $request_data;
                        array_push($pusharr, $data);
                    }
                    $response_array = array('success' => true, 'invites' => $pusharr, 'success_code' => 200);
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }

            } else {
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    //Ask to join shared monthly contract
    public function ask_to_join()
    {
        Log::info('ask_to_join ');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $base_monthly_id = Input::get('base_monthly_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
                'base_monthly_id' => $base_monthly_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer',
                'base_monthly_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    if ($monthly = Monthly::find($monthly_id)) {
                        if (!$request = Requests::where('id', '=', $monthly->request_id)->first()) {
                            /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                            $response_array = array('success' => false, 'error' => 'join reqest in progress', 'error_code' => 455);
                        }
                        //$request = Requests::find($monthly->request_id);
                        if ($monthly->owner_id != $owner_data->id) {
                            $totalinvites = Invite::where("monthly_id", '=', $monthly->id)->where("owner_id", '=', $owner_data->id)->where("is_cancelled", '=', 0)->where("invite_type", '=', 1)->count();
                            Log::info('Total Invitation = ' . $totalinvites);
                            if ($totalinvites == 0) {
                                //$walker_id = $request->confirmed_walker;
                                //$walker = Walker::find($walker_id);
                                $join = new Join();
                                $join->owner_id = $owner_id;
                                $join->walker_id = $request->confirmed_walker;
                                $join->status = 0;
                                $join->monthly_id = $monthly->id;
                                $join->is_cancelled = 0;
                                $join->base_monthly_id = $base_monthly_id;
                                $join->save();
                                Monthly::where('id', $join->base_monthly_id)->update(array('status' => 18));
                                Log::info('New Join Request = ' . print_r($join, true));
                                $request_data = array();
                                $request_data['owner'] = array();
                                $request_data['request_id'] = 0;
                                $request_data['owner']['name'] = $owner_data->first_name . " " . $owner_data->last_name;
                                $request_data['owner']['picture'] = $owner_data->picture;
                                $request_data['owner']['phone'] = $owner_data->phone;
                                $request_data['owner']['address'] = $owner_data->address;
                                $request_data['owner']['latitude'] = $owner_data->latitude;
                                $request_data['owner']['longitude'] = $owner_data->longitude;
                                $request_data['owner']['owner_dist_lat'] = $monthly->trip_one_to_lat;
                                $request_data['owner']['owner_dist_long'] = $monthly->trip_one_to_long;
                                $request_data['owner']['payment_type'] = 1;
                                $request_data['owner']['rating'] = $owner_data->rate;
                                $request_data['owner']['num_rating'] = $owner_data->rate_count;
                                $request_data['dog'] = array();
                                $request_data['owner']["starting_date"] = "2016-11-07";
                                $request_data['owner']["subscription_type"] = 2;
                                $request_data['owner']["total_cost"] = 0;
                                $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                                $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                                $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                                $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                                $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                                $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                                $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                                $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                                $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                                $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                                $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                                $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                                $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                                $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                                $request_data['owner']["trip_type"] = $monthly->trip_type;
                                $request_data['owner']["cab_type"] = $monthly->cab_type;
                                $request_data['success'] = 1;

                                $msg_array = array('success' => true);
                                $msg_array['payment_mode'] = 1;
                                $msg_array['monthly_id'] = $monthly->id;
                                $msg_array['invite_id'] = $join->id;
                                $msg_array['request_data'] = $request_data;
                                $msg_array['confirmed_walker'] = 0;
                                $msg_array['time_left_to_respond'] = 120;
                                $msg_array['unique_id'] = 35;
                                //$title = "New Monthly Subscription Request";
                                $title = "Customer " . $owner_data->first_name . " asked you to join following monthly subscription?";
                                $message = $msg_array;
                                //Log::info('response = ' . print_r($message, true));
                                //::info('New Invite for request = ' . print_r($message, true));
                                send_notifications($request->confirmed_walker, "walker", $title, $message);

                                $confirmed_walker = Walker::find($join->walker_id);
                                $response_array = array('success' => true, 'join_id' => $join->id, 'walker_name' => $confirmed_walker->first_name . ' ' . $confirmed_walker->last_name, 'message' => 'Pust Sent to Captain on Customer join request');

                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'join reqest in progress', 'error_code' => 455);
                            }

                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                            $response_array = array('success' => false, 'error' => 'Request ID matches with ' . Config::get('app.generic_keywords.User') . ' ID', 'error_code' => 407);

                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 408);

                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            /*} else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
        } else {
            $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
        }
    }*/
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    // cancel join
    public function cancel_join()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $join_id = Input::get('join_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'join_id' => $join_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'join_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $join = Join::where('id', $join_id)->first();

                    if (ISSET($join)) {
                        Monthly::where('id', '=', $join->base_monthly_id)->update(array('status' => 17));
                        Join::where('id', '=', $join_id)->update(array('is_cancelled' => 1));
                        $msg_array['monthly_id'] = $join->monthly_id;
                        $msg_array['join_id'] = $join->id;
                        $msg_array['is_cancelled'] = 1;
                        $msg_array['walker_id'] = $join->walker_id;
                        $msg_array['unique_id'] = 34;
                        $ownerName = $owner_data->first_name . " " . $owner_data->last_name;
                        //$title = "User Cancelled";
                        $title = "Customer " . $ownerName . " cancelled invitation to share ride";

                        $message = $msg_array;

                        send_notifications($join->walker_id, "walker", $title, $message);

                        $response_array = array('success' => true, 'message' => 'Invitation Cancelled Successfully');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_cost_estimate()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $trip_info['trip_type'] = Input::get('trip_type');
        $trip_info['cab_type'] = Input::get('cab_type');

        $trip_info['trip_one_from_lat'] = Input::get('trip_one_from_lat');
        $trip_info['trip_one_from_long'] = Input::get('trip_one_from_long');
        $trip_info['trip_one_to_lat'] = Input::get('trip_one_to_lat');
        $trip_info['trip_one_to_long'] = Input::get('trip_one_to_long');

        $trip_info['trip_two_from_lat'] = Input::get('trip_two_from_lat');
        $trip_info['trip_two_from_long'] = Input::get('trip_two_from_long');
        $trip_info['trip_two_to_lat'] = Input::get('trip_two_to_lat');
        $trip_info['trip_two_to_long'] = Input::get('trip_two_to_long');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    $trip_info['walker_id'] = 135;

                    $trip_details = cost_calculation($trip_info);
                    $sharedcost = '';
                    $actualcost = '';
                    $discount = '';
                    $distance = '';
                    $duration = '';

                    if ($trip_details) {
                        $sharedcost = $trip_details['total_cost'] * 0.8;
                        $discount = $trip_details['total_cost'] * 0.2;
                        $actualcost = $trip_details['total_cost'];
                        $distance = $trip_details['distance_km'];
                        $duration = $trip_details['duration_min'];
                    }
                    if ($sharedcost < 777) {
                        $discount = 0;
                        $sharedcost = 777;
                    }
                    $response_array = array(
                        'success' => true,
                        'sharedcost' => $sharedcost,
                        'discount' => $discount,
                        'actualcost' => $actualcost,
                        'distance' => $distance,
                        'duration' => $duration,
                        'message' => "Following is the estimated cost");
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function register_sub_monthly()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');

        $gender = Input::get('gender');
        $trip_one_from_address = Input::get('trip_one_from_address');
        $trip_one_to_address = Input::get('trip_one_to_address');
        $trip_two_from_address = Input::get('trip_two_from_address');
        $trip_two_to_address = Input::get('trip_two_to_address');
        $trip_one_from_lat = Input::get('trip_one_from_lat');
        $trip_one_from_long = Input::get('trip_one_from_long');
        $trip_one_to_lat = Input::get('trip_one_to_lat');
        $trip_one_to_long = Input::get('trip_one_to_long');
        $trip_two_from_lat = Input::get('trip_two_from_lat');
        $trip_two_from_long = Input::get('trip_two_from_long');
        $trip_two_to_lat = Input::get('trip_two_to_lat');
        $trip_two_to_long = Input::get('trip_two_to_long');

        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
                'gender' => $gender,
                'trip_one_from_address' => $trip_one_from_address,
                'trip_one_to_address' => $trip_one_to_address,
                'trip_one_from_lat' => $trip_one_from_lat,
                'trip_one_from_long' => $trip_one_from_long,
                'trip_one_to_lat' => $trip_one_to_lat,
                'trip_one_to_long' => $trip_one_to_long
            ), array(
            'owner_id' => 'required|integer',
            'monthly_id' => 'required|integer',
            'gender' => 'required|integer',
            'trip_one_from_address' => 'required',
            'trip_one_to_address' => 'required',
            'trip_one_from_lat' => 'required',
            'trip_one_from_long' => 'required',
            'trip_one_to_lat' => 'required',
            'trip_one_to_long' => 'required'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            Log::info('Error while during sub monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $monthly = Monthly::find($monthly_id);

            if (isset($monthly)) {
                $submonthly = new SubMonthly;
                $submonthly->owner_id = $owner_id;
                $submonthly->trip_type = $monthly->trip_type;
                $submonthly->gender = $gender;
                $submonthly->is_full_month = $monthly->is_full_month;
                $submonthly->trip_one_from_address = $trip_one_from_address;
                $submonthly->trip_one_to_address = $trip_one_to_address;
                $submonthly->trip_two_from_address = $trip_two_from_address;
                $submonthly->trip_two_to_address = $trip_two_to_address;
                $submonthly->trip_one_from_lat = $trip_one_from_lat;
                $submonthly->trip_one_from_long = $trip_one_from_long;
                $submonthly->trip_one_to_lat = $trip_one_to_lat;
                $submonthly->trip_one_to_long = $trip_one_to_long;
                $submonthly->trip_one_pickup_time = $monthly->trip_one_pickup_time;
                $submonthly->trip_two_from_lat = $trip_two_from_lat;
                $submonthly->trip_two_from_long = $trip_two_from_long;
                $submonthly->trip_two_to_lat = $trip_two_to_lat;
                $submonthly->trip_two_to_long = $trip_two_to_long;
                $submonthly->trip_two_pickup_time = $monthly->trip_two_pickup_time;
                $submonthly->starting_date = date("Y-m-d", strtotime($monthly->starting_date));
                $submonthly->status = '1';
                $submonthly->created_at = date("Y-m-d h:i:s");
                $submonthly->save();
                $trip_info['trip_type'] = $monthly->trip_type;
                $trip_info['cab_type'] = $monthly->cab_type;
                $trip_info['walker_id'] = 135;

                $trip_info['trip_one_from_lat'] = $submonthly->trip_one_from_lat;
                $trip_info['trip_one_from_long'] = $submonthly->trip_one_from_long;
                $trip_info['trip_one_to_lat'] = $submonthly->trip_one_to_lat;
                $trip_info['trip_one_to_long'] = $submonthly->trip_one_to_long;

                $trip_info['trip_two_from_lat'] = $submonthly->trip_two_from_lat;
                $trip_info['trip_two_from_long'] = $submonthly->trip_two_from_long;
                $trip_info['trip_two_to_lat'] = $submonthly->trip_two_to_lat;
                $trip_info['trip_two_to_long'] = $submonthly->trip_two_to_long;

                $trip_details = cost_calculation($trip_info);

                $sharedcost = '';
                $actualcost = '';
                $discount = '';
                $distance = '';
                $duration = '';

                if ($trip_details) {
                    $sharedcost = $trip_details['total_cost'] * 0.8;
                    $discount = $trip_details['total_cost'] * 0.2;
                    $actualcost = $trip_details['total_cost'];
                    $distance = $trip_details['distance_km'];
                    $duration = $trip_details['duration_min'];
                }
                if ($sharedcost < 777) {
                    $discount = 0;
                    $sharedcost = 777;
                }
                $submonthly->distance = $distance;
                $submonthly->duration = $duration;
                $submonthly->cost = $sharedcost;
                $submonthly->save();
                $response_array = array(
                    'success' => true,
                    'sharedcost' => $sharedcost,
                    'discount' => $discount,
                    'actualcost' => $actualcost,
                    'distance' => $distance,
                    'duration' => $duration,
                    'message' => "Following is the estimated cost",
                    'sub_monthly_id' => $submonthly->id);
                $response_code = 200;
            } else {
                $response_array = array('success' => false, 'monthly_not_found' => $monthly->id, 'error' => 'Monthly ID not found', 'error_code' => 505);
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function add_favorite_location()
    {
        Log::info('add_favorite_location');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $latitude = Input::get('latitude');
        $longitude = Input::get('longitude');
        $label = Input::get('name');
        $details = Input::get('details');
        $address = Input::get('address');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'name' => $label,
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'name' => 'required',
                'address' => 'required',
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric'
            )
        );
        if ($validator->fails() || $longitude == 0 || $latitude == 0) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    $favoriteLocation = new FavoriteLocation();
                    $favoriteLocation->owner_id = $owner_id;
                    $favoriteLocation->label = $label;
                    $favoriteLocation->details = $details;
                    $favoriteLocation->address = $address;
                    $favoriteLocation->latitude = $latitude;
                    $favoriteLocation->longitude = $longitude;
                    $favoriteLocation->status = 1;
                    $favoriteLocation->save();
                    $response_array = array('success' => true, 'favoriteLocation_id' => $favoriteLocation->id, 'label' => $label, 'message' => 'Favorite location saved');
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function delete_favorite_location()
    {
        Log::info('delete_favorite_location');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $favorite_location_id = Input::get('favorite_location_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'favorite_location_id' => $favorite_location_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'favorite_location_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations

                    DB::delete("delete from favorite_location where id = '" . $favorite_location_id . "';");
                    $response_array = array('success' => true, 'favoriteLocation_id' => $favorite_location_id, 'message' => 'Favorite location deleted');
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    // Get Invites
    public function get_locations()
    {

        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
            ), array(
            'token' => 'required',
            'owner_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry)) {
                    //$invites = Invite::where('owner_id','=',$owner_id)->where('monthly_id','=',$monthly_id)->where('is_cancelled','=',0)->get();
                    $favoriteLocations = DB::table('favorite_location')->where('favorite_location.owner_id', '=', $owner_id)->get();
                    $pusharr = array();
                    foreach ($favoriteLocations as $favoriteLocation) {
                        $data['id'] = $favoriteLocation->id;
                        $data['owner_id'] = $favoriteLocation->owner_id;
                        $data['name'] = $favoriteLocation->label;
                        $data['details'] = $favoriteLocation->details;
                        $data['address'] = $favoriteLocation->address;
                        $data['latitude'] = $favoriteLocation->latitude;
                        $data['longitude'] = $favoriteLocation->longitude;
                        $data['status'] = $favoriteLocation->status;
                        array_push($pusharr, $data);
                    }
                    $response_array = array('success' => true, 'locations' => $pusharr, 'success_code' => 200);
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }

            } else {
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function add_feedback()
    {
        Log::info('add feedback');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $device_type = Input::get('device_type');
        $message = Input::get('message');
        $version = Input::get('version');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'message' => $message,
                'version' => $version,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'message' => 'required',
                'version' => 'required'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    $feedback = new Feedback();
                    $feedback->user_id = $owner_id;
                    $feedback->customer_type = 'owner';
                    $feedback->message = $message;
                    $feedback->device_type = $device_type;
                    $feedback->version = $version;
                    $feedback->save();
                    $response_array = array('success' => true, 'feedback_id' => $feedback->id, 'label' => $message, 'message' => 'Feedback saved');
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function request_monthly_walker()
    {
        Log::info('request_monthy_walker');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $walker_id = Input::get('walker_id');
        $monthly_id = Input::get('monthly_id');
        $base_monthly_id = Input::get('base_monthly_id');
        $subscription_type = Input::get('subscription_type');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'walker_id' => $walker_id,
                'monthly_id' => $monthly_id,
                'base_monthly_id' => $base_monthly_id,
                'subscription_type' => $subscription_type,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'walker_id' => 'required|integer',
                'monthly_id' => 'required|integer',
                'base_monthly_id' => 'required|integer',
                'subscription_type' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $monthly = Monthly::find($base_monthly_id);

                    if (ISSET($monthly)) {
                        if ($subscription_type == 1) {
                            // Private Subscription Needed to send request to walker
                            $monthly->subscription_type = 1;
                            $monthly->save();
                            request_walker_for_private_contract($monthly, $walker_id);
                            $response_array = array('success' => true, 'message' => 'private contract request sent to captain');
                            $response_code = 200;
                        } else if ($subscription_type == 2) {
                            //Shared Request Call ask to join
                            $monthly->subscription_type = 2;
                            $monthly->save();
                            if ($monthly_id == 0) {
                                request_walker_for_private_contract($monthly, $walker_id);
                                $monthly->cost = minCost777($monthly->cost * 0.6);
                                $monthly->save();
                                $monthly = Monthly::find($base_monthly_id);
                                $request = Requests::find($monthly->request_id);
                                $request->total = minCost777($request->total * 0.6);
                                $request->save();
                                $response_array = array('success' => true, 'message' => 'shared contract request sent to captain');
                                $response_code = 200;
                            } else {
                                $monthly = Monthly::find($monthly_id);

                                if (!$request = Requests::where('id', '=', $monthly->request_id)->first()) {
                                    /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                    $response_array = array('success' => false, 'error' => 'join reqest in progress', 'error_code' => 455);
                                }
                                //$request = Requests::find($monthly->request_id);
                                if ($monthly->owner_id != $owner_data->id) {
                                    $totalinvites = Join::where("base_monthly_id", '=', $monthly->id)->where("owner_id", '=', $owner_data->id)->where("is_cancelled", '=', 0)->where("status", '=', 0)->count();
                                    //Log::info('Total Invitation = ' . $totalinvites);
                                    if ($totalinvites == 0) {
                                        //$walker_id = $request->confirmed_walker;
                                        //$walker = Walker::find($walker_id);
                                        $join = new Join();
                                        $join->owner_id = $owner_id;
                                        $join->walker_id = $request->confirmed_walker;
                                        $join->status = 0;
                                        $join->monthly_id = $monthly->id;
                                        $join->is_cancelled = 0;
                                        $join->base_monthly_id = $base_monthly_id;
                                        $join->save();
                                        Monthly::where('id', $join->base_monthly_id)->update(array('status' => 18));
                                        Log::info('New Join Request = ' . print_r($join, true));
                                        $request_data = array();
                                        $request_data['owner'] = array();
                                        $request_data['request_id'] = 0;
                                        $request_data['owner']['name'] = $owner_data->first_name . " " . $owner_data->last_name;
                                        $request_data['owner']['picture'] = $owner_data->picture;
                                        $request_data['owner']['phone'] = $owner_data->phone;
                                        $request_data['owner']['address'] = $owner_data->address;
                                        $request_data['owner']['latitude'] = $owner_data->latitude;
                                        $request_data['owner']['longitude'] = $owner_data->longitude;
                                        $request_data['owner']['owner_dist_lat'] = $monthly->trip_one_to_lat;
                                        $request_data['owner']['owner_dist_long'] = $monthly->trip_one_to_long;
                                        $request_data['owner']['payment_type'] = 1;
                                        $request_data['owner']['rating'] = $owner_data->rate;
                                        $request_data['owner']['num_rating'] = $owner_data->rate_count;
                                        $request_data['dog'] = array();
                                        $request_data['owner']["starting_date"] = "2016-11-07";
                                        $request_data['owner']["subscription_type"] = 2;
                                        $request_data['owner']["total_cost"] = 0;
                                        $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                                        $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                                        $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                                        $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                                        $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                                        $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                                        $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                                        $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                                        $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                                        $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                                        $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                                        $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                                        $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                                        $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                                        $request_data['owner']["trip_type"] = $monthly->trip_type;
                                        $request_data['owner']["cab_type"] = $monthly->cab_type;
                                        $request_data['success'] = 1;

                                        $msg_array = array('success' => true);
                                        $msg_array['payment_mode'] = 1;
                                        $msg_array['monthly_id'] = $monthly->id;
                                        $msg_array['invite_id'] = $join->id;
                                        $msg_array['request_data'] = $request_data;
                                        $msg_array['confirmed_walker'] = 0;
                                        $msg_array['time_left_to_respond'] = 120;
                                        $msg_array['unique_id'] = 35;
                                        //$title = "New Monthly Subscription Request";
                                        $title = "Customer " . $owner_data->first_name . " asked you to join following monthly subscription?";
                                        $message = $msg_array;
                                        //Log::info('response = ' . print_r($message, true));
                                        //::info('New Invite for request = ' . print_r($message, true));
                                        send_notifications($request->confirmed_walker, "walker", $title, $message);

                                        $confirmed_walker = Walker::find($join->walker_id);
                                        $response_array = array('success' => true, 'join_id' => $join->id, 'walker_name' => $confirmed_walker->first_name . ' ' . $confirmed_walker->last_name, 'message' => 'Pust Sent to Captain on Customer join request');

                                    } else {
                                        /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                        $response_array = array('success' => false, 'error' => 'join reqest in progress', 'error_code' => 455);
                                    }

                                } else {
                                    /* $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                    $response_array = array('success' => false, 'error' => 'Request ID matches with ' . Config::get('app.generic_keywords.User') . ' ID', 'error_code' => 407);

                                }
                            }

                            /*return Redirect::to('user/asktojoin')
                                ->with('id', $owner_id)
                                ->with('token', $token)
                                ->with('monthly_id', $monthly_id)
                                ->with('base_monthly_id', $base_monthly_id);*/
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }


    public function refresh_monthly_walkers()
    {
        Log::info('refresh');
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'monthly_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {
                    $monthly = Monthly::find($monthly_id);

                    if (ISSET($monthly)) {
                        if ($monthly->status != 1) {
                            $response_array = array(
                                'success' => true,
                                'message' => "Status changed",
                                'monthly_id' => $monthly->id,
                                'total' => $monthly->cost,
                                'time' => $monthly->duration,
                                'distance' => $monthly->distance,
                                'status' => $monthly->status);
                            $response_code = 200;
                            $response = Response::json($response_array, $response_code);
                            return $response;
                        }
                        $captainsRes = DB::table('request_meta')->where('request_meta.request_id', '=', $monthly->request_id)->where('request_meta.status', '=', 1)->
                        join('walker', 'walker.id', '=', 'request_meta.walker_id')->get(['request_meta.*', 'walker.first_name',
                            'walker.last_name', 'walker.car_model', 'walker.phone', 'walker.picture', 'walker.rate']);
                        $captains = array();
                        $i = 0;
                        foreach ($captainsRes as $captain) {
                            $captains[$i]['walker_id'] = $captain->walker_id;
                            $captains[$i]['first_name'] = $captain->first_name;
                            $captains[$i]['last_name'] = $captain->last_name;
                            $captains[$i]['car_model'] = $captain->car_model;
                            $captains[$i]['phone'] = $captain->phone;
                            $captains[$i]['picture'] = $captain->picture;
                            $captains[$i]['rate'] = $captain->rate;
                            $walker_cab_photo = WalkerDocument::where('walker_id', $captain->walker_id)->where('document_id', 4)->first();
                            if (isset($walker_cab_photo)) {
                                $captains[$i]['cab_picture'] = $walker_cab_photo->url;
                            } else {
                                $captains[$i]['cab_picture'] = null;
                            }
                            $i++;
                        }

                        //$captains = get_walker_monthly($monthly);
                        /*if ($monthly->subscription_type == 1)
                            request_walker_monthly($monthly);
                        else
                            Monthly::where('id', $monthly->id)->update(array('status' => 17));*/
                        $response_array = array(
                            'success' => true,
                            'message' => "Following captains accepted request",
                            'monthly_id' => $monthly->id,
                            'total' => $monthly->cost,
                            'time' => $monthly->duration,
                            'distance' => $monthly->distance,
                            'captains' => $captains,
                            'status' => $monthly->status);

                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function change_captain_monthly()
    {
        $specific_days = array();

        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $message = Input::get('message');

        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
                'message' => $message
            ), array(
            'owner_id' => 'required|integer',
            'monthly_id' => 'required|integer',
            'message' => 'required'
        ));


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            Log::info('Error while during monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $monthly = Monthly::find($monthly_id);
            $request = Requests::find($monthly->request_id);
            $request->status = 2;
            $request->is_cancelled = 1;
            $request->message = $message;
            $request->save();
            request_walkers_monthly($monthly);
            $monthly = Monthly::find($monthly->id);
            $response_array = array(
                'success' => true,
                'message' => "Your monthly subscription captain changing",
                'monthly_id' => $monthly->id,
                'total' => $monthly->cost,
                'time' => $monthly->duration,
                'distance' => $monthly->distance
            );
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function cancel_monthly_trip()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');
        $message = Input::get('message');

        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id,
                'message' => $message
            ), array(
            'owner_id' => 'required|integer',
            'monthly_id' => 'required|integer',
            'message' => 'required'
        ));


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            Log::info('Error while during monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $monthly = Monthly::find($monthly_id);
            $monthly->status = 14;
            $monthly->save();
            $request = Requests::find($monthly->request_id);
            $request->is_cancelled = 1;
            $request->message = $message;
            $request->save();
            $response_array = array(
                'success' => true,
                'message' => "Your monthly subscription has been canceled",
                'monthly_id' => $monthly->id
            );
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function freeze_monthly()
    {
        $token = Input::get('token');
        $owner_id = Input::get('id');
        $monthly_id = Input::get('monthly_id');

        $validator = Validator::make(
            array(
                'owner_id' => $owner_id,
                'monthly_id' => $monthly_id
            ), array(
            'owner_id' => 'required|integer',
            'monthly_id' => 'required|integer'
        ));


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            Log::info('Error while during monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $monthly = Monthly::find($monthly_id);
            $monthly->status = 15;
            $monthly->save();
            $request = Requests::find($monthly->request_id);
            $request->status = 2;
            $request->is_cancelled = 0;
            $request->save();
            $response_array = array(
                'success' => true,
                'message' => "Your monthly subscription freezed",
                'monthly_id' => $monthly->id
            );
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function payfort_feedback()
    {
        Log::info("payfort_feedback");
        $amount = Input::get('amount');
        $sdk_token = Input::get('sdk_token');
        $merchant_reference = Input::get('merchant_reference');
        $customer_email = Input::get('customer_email');
        $language = Input::get('language');
        $currency = Input::get('currency');
        $command = Input::get('command');

        $validator = Validator::make(
            array(
                'customer_email' => $customer_email,
                'amount' => $amount,
                'command' => $command
            ), array(
            'customer_email' => 'required',
            'amount' => 'required',
            'command' => 'required'
        ));


        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            Log::info('Error while during monthly subscription registration = ' . print_r($error_messages, true));
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $command='purchase';
            $transactionhistory=TransactionHistory::where('merchant_reference','=',$merchant_reference)->first();
            if(!isset($transactionhistory)){
                $transaction_history = new TransactionHistory();
                $transaction_history->request_id = 0;
                $transaction_history->amount = $amount;
                $transaction_history->response_message = "";
                $transaction_history->command = $command;
                $transaction_history->merchant_reference = $merchant_reference;
                $transaction_history->response_code = "";
                $transaction_history->status = "";
                $transaction_history->save();
            }
            $response_array = array(
                'success' => true,
                'message' => "Payfort Feedback Response Received"
            );
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }
}
