<?php

/*
 * Company related data manipulation Controller
  */

use App\Classes\Curl;
class CompanyController extends \BaseController
{


    //if email and password match return view of request page for testing currently it MUST be profile page, otherwise return 0.

    public function __construct()
    {
        if (Config::get('app.production')) {
            echo "Something cool is going to be here soon.";
            die();
        }

        $this->beforeFilter(function () {
            if (!Session::has('company_id')) {
                return Redirect::to('/company/login');
            } else {
                $company_id = Session::get('company_id');

                $company = Companies::find($company_id);
                Session::put('company_name', $company->company_name);
                Session::put('company_city', $company->city);
                Session::put('is_api', $company->is_api);
            }
        }, array('except' => array(
            'companyLogin',
            'companyVerify',
            'companyForgotPassword',
            'companyRegister',
            'companySave',
            'registercompany',
            'changelanguage',
            'companyActivation',
        )));
    }

    public function loginRedirect() {
        return Redirect::to('/company/login');
    }

    public function companyLogin()
    {
        if (!Session::has('language')) {
            Session::put('language', 'en');
            $language = 'en';
        } else {
            $language = Session::get('language');
            Log::info($language);
        }
        $page = 'companies.login';
        if ($language == 'ar') {
            $page = 'companies.login';
        }
        $error = Request::get('error');
        $message = Request::get('message');
        Log::info($error . $message);
        if ($error)
            return View::make($page)->with('error', $error);
        else if ($message)
            return View::make($page)->with('message', $message);
        else
            return View::make($page);
    }


    public function companyVerify()
    {
        $email = Input::get('username');
        $password = Input::get('password');
        $company = Companies::where('email', '=', $email)->orWhere('username', '=', $email)->first();

        if ($company) {
            if ($company && Hash::check($password, $company->password)) {
                Session::put('company_id', $company->id);
                Session::put('company_name', $company->company_name);
                Session::put('company_city', $company->city);
                Session::put('is_api', $company->is_api);
                return Redirect::to('company/shipmentsExport');
            } else {
                return Redirect::to('company/login')->with('error', 'Invalid Username and password')->with('email', $email);
            }

        } else {
            return Redirect::to('company/login')->with('error', 'Invalid Username')->with('email', $email);
        }
    }


    public function companyLogout()
    {
        Session::flush();
        return Redirect::to('/company/login');
    }


    public function companyDashboard()
    {
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->distinct()->get();
        $city = Request::get('city');
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);

        //$city = $companydata->city;

        if (!isset($city)) {
            /* if (isset($adminCities[0]))
                 $city = $adminCities[0]->city;
             else */
            $city = 'All';
        }

        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();

        if (count($grouped)) {

            $checkgroup = 1;
        } else {

            $checkgroup = 0;

        }

        $transfared_query = "select sum(amount) 'alreadyreansfared' from banks_deposit where paid_to_supplier = 1 ";
        if($company_id != 'all') {
            $transfared_query .= "and company_id = $company_id ";
        }
        if($city != 'All') {
            $transfared_query .= "and city = '$city'";
        }
        $alreadyreansfared = DB::select($transfared_query);
        $alreadyreansfared = $alreadyreansfared[0]->alreadyreansfared;

        $allitemsquery = DeliveryRequest::select(['delivery_request.id'])->where('company_id', '=', $company_id);
        $allitemscodquery = DeliveryRequest::where('cash_on_delivery', '>', '0')->where('company_id', '=', $company_id);

        $penddingitemsquery = DeliveryRequest::where('status', '<', '1')->where('company_id', '=', $company_id);


        $onwayitemsquery = DeliveryRequest::whereIn('status', array(2))->where('company_id', '=', $company_id);


        $arriveditemsquery = DeliveryRequest::whereIn('status', ['3'])->where('company_id', '=', $company_id);

        $returnedquery = DeliveryRequest::whereIn('status', ['3'])->where('num_faild_delivery_attempts', '>', '0')->where('company_id', '=', $company_id);

        $newquery = DeliveryRequest::whereIn('status', ['3'])->where('num_faild_delivery_attempts', '=', '0')->where('company_id', '=', $company_id);

        $outfordeliveryitemsquery = DeliveryRequest::where('status', '=', '4')->where('company_id', '=', $company_id);


        $delivreditemsquery = DeliveryRequest::where('status', '=', '5')->where('company_id', '=', $company_id);


        $delivreditemscodquery = DeliveryRequest::where('cash_on_delivery', '>', '0')->where('status', '=', '5')->where('company_id', '=', $company_id);


        $undelivreditemsquery = DeliveryRequest::where('status', '=', '6')->where('company_id', '=', $company_id);


        $returnedtojcitemsquery = DeliveryRequest::where('status', '=', '7')->where('company_id', '=', $company_id);

        $cashcollectedquery = DeliveryRequest::where('status', '=', '5')->where('company_id', '=', $company_id);
        $cashtobecollectedquery = DeliveryRequest::where('status', '!=', '5')->where('status', '!=', '7')->where('company_id', '=', $company_id);

        $mergedQuery = '';

        if ($city != 'All') {

             if ($checkgroup == 0) {
                $allitemsquery = $allitemsquery->where('d_city', 'like', $city);
            $allitemscodquery = $allitemscodquery->where('d_city', 'like', $city);
            $penddingitemsquery = $penddingitemsquery->where('d_city', 'like', $city);
            $onwayitemsquery = $onwayitemsquery->where('d_city', 'like', $city);
            $arriveditemsquery = $arriveditemsquery->where('d_city', 'like', $city);
            $returnedquery    = $returnedquery->where('d_city', '=', $city);
            $newquery    = $newquery->where('d_city', '=', $city);
            $outfordeliveryitemsquery = $outfordeliveryitemsquery->where('d_city', 'like', $city);
            $delivreditemsquery = $delivreditemsquery->where('d_city', 'like', $city);
            $delivreditemscodquery = $delivreditemscodquery->where('d_city', 'like', $city);
            $undelivreditemsquery = $undelivreditemsquery->where('d_city', 'like', $city);
            $returnedtojcitemsquery = $returnedtojcitemsquery->where('d_city', 'like', $city);
            $cashcollectedquery = $cashcollectedquery->where('d_city', 'like', $city);
            $cashtobecollectedquery = $cashtobecollectedquery->where('d_city', 'like', $city);
            $mergedQuery = empty($mergedQuery) ? ' delivery_request.d_city = "' . $city . '"' : $mergedQuery . ' and delivery_request.d_city = "' . $city . '"';
            } else {

                $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';

                
                $allitemsquery = $allitemsquery->whereRaw($subcities);
                $allitemscodquery = $allitemscodquery->whereRaw($subcities);
                $penddingitemsquery = $penddingitemsquery->whereRaw($subcities);
                $onwayitemsquery = $onwayitemsquery->whereRaw($subcities);
                $arriveditemsquery = $arriveditemsquery->whereRaw($subcities);
                $returnedquery = $returnedquery->whereRaw($subcities);
                 $newquery    = $newquery->whereRaw($subcities);
                $outfordeliveryitemsquery = $outfordeliveryitemsquery->whereRaw($subcities);
                $delivreditemsquery = $delivreditemsquery->whereRaw($subcities);
                $delivreditemscodquery = $delivreditemscodquery->whereRaw($subcities);
                $undelivreditemsquery = $undelivreditemsquery->whereRaw($subcities);
                $returnedtojcitemsquery = $returnedtojcitemsquery->whereRaw($subcities);
                $cashcollectedquery = $cashcollectedquery->whereRaw($subcities);
                $cashtobecollectedquery = $cashtobecollectedquery->whereRaw($subcities);
                $mergedQuery = empty($mergedQuery) ? ' delivery_request.d_city = "' . $city . '"' : $mergedQuery . ' and delivery_request.d_city = "' . $city . '"';

            }  


            
        }
        $mergedQueryFinal = ' select * from (SELECT d_district, COUNT(*) AS allitems FROM delivery_request where company_id ='.$company_id;
        $mergedQueryFinal .= empty($mergedQuery) ? '' : ' and ' . $mergedQuery;
        $mergedQueryFinal .= ' GROUP BY delivery_request.d_district) t1 left join(SELECT d_district as dummy, COUNT(*) AS delivereditems FROM delivery_request WHERE delivery_request.status=5 ';
        $mergedQueryFinal .= empty($mergedQuery) ? '' : 'and ' . $mergedQuery;
        $mergedQueryFinal .= ' GROUP BY delivery_request.d_district) t2 on t1.d_district = t2.dummy';
        $totalNewPackagesQuery = "SELECT count(*) TotalNewPackages from delivery_request ";
        $totalNewPackagesQuery .= empty($mergedQuery) ? ' where ' : ' where ' . $mergedQuery . ' and ';
        $totalNewPackagesQuery .= "and scheduled_shipment_date = ''and shipment_date NOT IN (select distinct shipment_date from delivery_request where scheduled_shipment_date < '2018-03-20' and shipment_date is not null)";
        $merged = DB::select($mergedQueryFinal);
        foreach ($merged as $item) {
            if ($item->delivereditems == NULL) {
                $item->delivereditems = 0;
            }
        }

        $allitems = $allitemsquery->count();
        $allitemscod = $allitemscodquery->count();
        $allitemsprepaid = $allitems - $allitemscod;

        $penddingitems = $penddingitemsquery->count();

        $onwayitems = $onwayitemsquery->count();

        $arriveditems = $arriveditemsquery->count();
        $returneditems = $returnedquery->count();
        $newitems     = $newquery->count();
        $outfordeliveryitems = $outfordeliveryitemsquery->count();

        $delivreditems = $delivreditemsquery->count();
        $delivreditemscod = $delivreditemscodquery->count();

        $delivreditemsprepaid = $delivreditems - $delivreditemscod;

        $undelivreditems = $undelivreditemsquery->count();
        $returnedtojcitems = $returnedtojcitemsquery->count();

        $cashcollected = '0.0';

        $cashcollected = $cashcollectedquery->sum('cash_on_delivery');
        $cashtobecollected = '0.0';


        $cashtobecollected = $cashtobecollectedquery->sum('cash_on_delivery');


        if ($allitems != 0) {
            $successrate = $delivreditems / $allitems;
            $successrate = $successrate * 100.0;
            if($allitemscod != 0) {
            $successratecod = $delivreditemscod / $allitemscod;
            }else{
                $successratecod = 0 ;
            }
            $successratecod = $successratecod * 100.0;
            if ($allitemsprepaid != 0) {
                $successrateprepaid = $delivreditemsprepaid / $allitemsprepaid;
                $successrateprepaid = $successrateprepaid * 100.0;
            } else
                $successrateprepaid = 0;
        } else {
            $successrate = 0;
            $successratecod = 0;
            $successrateprepaid = 0;
        }
        $companies = Companies::all(array('id', 'company_name as name'));

        /////////
        /// months chart
        $monthschart = array();
        $x_val = 1;
        for($day = 7; $day < 29; $day += 7){

            $query = DeliveryRequest::select([
                DB::raw('new_shipment_date AS inroutedate'),
                DB::raw('ifnull(sum(status=5),0) AS delivered'),
                DB::raw('ifnull(sum(status=3),0)+ifnull(sum(status=4),0)+ifnull(sum(status=5),0)+ifnull(sum(status=6),0)+ifnull(sum(status=7),0) AS dispatched')
            ])->where('company_id', '=', $company_id);

            if($city != 'All') {
                $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                if($checkgroup == '0')
                    $query = $query->where('d_city', '=', $city);
                else
                    $query = $query->whereRaw($subcities);
            }

            $query = $query->whereRaw("DATEDIFF(date(now()),new_shipment_date) <= $day")->havingRaw('new_shipment_date is not null')->get();

            $delivered = 0;
            $dispatched = 0;
            for($i = 0; $i < count($query); ++$i){
                $delivered += $query[$i]->delivered;
                $dispatched += $query[$i]->dispatched;
            }

            ///////////
            // check if there is dispatched items in the current month
            $curquery = DeliveryRequest::select([
                DB::raw('new_shipment_date AS inroutedate'),
                DB::raw('ifnull(sum(status=5),0) AS delivered'),
                DB::raw('ifnull(sum(status=3),0)+ifnull(sum(status=4),0)+ifnull(sum(status=5),0)+ifnull(sum(status=6),0)+ifnull(sum(status=7),0) AS dispatched')
            ])->where('company_id', '=', $company_id);

            if($city != 'All') {
                $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                if($checkgroup == '0')
                    $curquery = $curquery->where('d_city', '=', $city);
                else
                    $curquery = $curquery->whereRaw($subcities);
            }

            $var = $day - 7;
            $curquery = $curquery->whereRaw("DATEDIFF(date(now()),new_shipment_date) > $var")->havingRaw('new_shipment_date is not null')->get();

            if(count($curquery) == 0)
                continue;
            ////////////

            $successrate =  (1.0 * $delivered / max($dispatched, 1)) * 100.0;

            $obj = (object)array(
                "x" => $x_val++,
                "successrate" => number_format($successrate, 2),
                "label" => $day . ' Days',
                "delivered" => $delivered,
                "dispatched" => $dispatched
            );

            array_push($monthschart, $obj);
        }
        for($month = 1; $month < 3; ++$month){

            $query = DeliveryRequest::select([
                DB::raw('new_shipment_date AS inroutedate'),
                DB::raw('ifnull(sum(status=5),0) AS delivered'),
                DB::raw('ifnull(sum(status=3),0)+ifnull(sum(status=4),0)+ifnull(sum(status=5),0)+ifnull(sum(status=6),0)+ifnull(sum(status=7),0) AS dispatched')
            ])->where('company_id', '=', $company_id);

            if($city != 'All') {
                $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                if($checkgroup == '0')
                    $query = $query->where('d_city', '=', $city);
                else
                    $query = $query->whereRaw($subcities);
            }

            $var = 30 * ($month + 1);
            $query = $query->whereRaw("DATEDIFF(date(now()),new_shipment_date) <= $var")->havingRaw('new_shipment_date is not null')->get();

            $delivered = 0;
            $dispatched = 0;
            for($i = 0; $i < count($query); ++$i){
                $delivered += $query[$i]->delivered;
                $dispatched += $query[$i]->dispatched;
            }

            ///////////
            // check if there is dispatched items in the current month
            $curquery = DeliveryRequest::select([
                DB::raw('new_shipment_date AS inroutedate'),
                DB::raw('ifnull(sum(status=5),0) AS delivered'),
                DB::raw('ifnull(sum(status=3),0)+ifnull(sum(status=4),0)+ifnull(sum(status=5),0)+ifnull(sum(status=6),0)+ifnull(sum(status=7),0) AS dispatched')
            ])->where('company_id', '=', $company_id);

            if($city != 'All') {
                $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                if($checkgroup == '0')
                    $curquery = $curquery->where('d_city', '=', $city);
                else
                    $curquery = $curquery->whereRaw($subcities);
            }

            $var = 30 * $month;
            if($month == 1)
                $var -= 2;
            $curquery = $curquery->whereRaw("DATEDIFF(date(now()),new_shipment_date) > $var")->havingRaw('new_shipment_date is not null')->get();

            if(count($curquery) == 0)
                continue;
            ////////////

            $monthsuccessrate = (1.0 * $delivered / max($dispatched, 1)) * 100.0;

            $obj = (object) array(
                "x" => $x_val++,
                "successrate" => number_format($monthsuccessrate, 2),
                "label" => ($month + 1) . ' Months',
                "delivered" => $delivered,
                "dispatched" => $dispatched
            );
            array_push($monthschart, $obj);
        }
        /////////

        return View::make('companies.dashboard')
            ->with('allitems', $allitems)
            ->with('returneditems', $returneditems)
            ->with('newitems', $newitems)
            ->with('allitemscod', $allitemscod)
            ->with('allitemsprepaid', $allitemsprepaid)
            ->with('penddingitems', $penddingitems)
            ->with('onwayitems', $onwayitems)
            ->with('arriveditems', $arriveditems)
            ->with('outfordeliveryitems', $outfordeliveryitems)
            ->with('delivreditems', $delivreditems)
            ->with('delivreditemscod', $delivreditemscod)
            ->with('delivreditemsprepaid', $delivreditemsprepaid)
            ->with('undelivreditems', $undelivreditems)
            ->with('returnedtojcitems', $returnedtojcitems)
            ->with('cashcollected', $cashcollected)
            ->with('cashtobecollected', $cashtobecollected)
            ->with('successrate', $successrate)
            ->with('successratecod', $successratecod)
            ->with('successrateprepaid', $successrateprepaid)
            ->with('merged', $merged)
            ->with('adminCities', $adminCities)
            ->with('companydata', $companydata)
            ->with('city', $city)
            ->with('company', $company_id)
            ->with('alreadyreansfared', $alreadyreansfared)
            ->with('monthschart', $monthschart);
    }
    //shipmentsExport Function
    public function editcompany()
    {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $company = Companies::where('id', '=', $company_id)->get();
        return View::make('companies.editcompany')->with('company', $company)->with('companydata',$companydata);

    }
    public function editcompanypost()
    {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $company = Companies::where('id', '=', $company_id)->first();


        $name = Request::get('name');
        $username = Request::get('username');
        $email = Request::get('email');
        $phone = Request::get('phone');
        //$password = Request::get('password');
        $address = Request::get('address');
        $city = Request::get('city');
        $country = Request::get('country');
        $zipcode = Request::get('zipcode');
        
       
        $latitude = Request::get('latitude');
        $longitude = Request::get('longitude');

        $company->company_name = $name;
        $company->username = $username;
        $company->email = $email;
        $company->phone = $phone;
        //$company->password = Hash::make($password);
        $company->address = $address;
        $company->city = $city;
        $company->zipcode = $zipcode;
        $company->country = $country;
        
        $company->latitude = $latitude;
        $company->longitude = $longitude;
        $company->secret = Hash::make($name);

        $company->save();
        return View::make('companies.companydetail')
            ->with('company', $company)
            ->with('companydata', $companydata);
    }

    //shipmentsExport Function
    public function shipmentsExport()
    {
        $start_date = Request::get('start_date');
        $end_date = Request::get('end_date');
        $admin_id = Session::get('admin_id');
        $adminCities = City::select('name as city')->orderBy('id', 'asc')->distinct()->get();
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $city = Request::get('city');
        $subcompany = Request::get('subcompany');
        $num_delivery_attempts = Request::get('num_delivery_attempts');

        if(!isset($num_delivery_attempts) || $num_delivery_attempts == '') {
            if(in_array($companydata->id, array('911', '946', '974', '975')))
                $num_delivery_attempts = 2;
            else 
                $num_delivery_attempts = 'all';
        }

        $UndeliveredReason = UndeliveredReason::all();
        $UndeliveredReasonMap = array();
        foreach($UndeliveredReason as $one_reason) 
            $UndeliveredReasonMap[$one_reason->id] = $one_reason;

        if(!isset($subcompany) || $subcompany == '')
            $subcompany = 'all';

        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
            else
                $city = 'All';
        }

        if (!isset($start_date) || empty($start_date) || !isset($end_date) || empty($end_date)) {
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-d');
        }

        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();


        if (count($grouped)) {

            $checkgroup = 1;
        } else {

            $checkgroup = 0;

        }

        $order_statuses = OrderStatuses::all();
        $statusDef = array();
        foreach($order_statuses as $or_st) 
            $statusDef[$or_st->id] = $or_st->english;

            // ->with('statusDef', $statusDef)


        //$companies = Companies::all(array('id', 'company_name as name'));

        $shipmentDate = Request::get('shipment_date');
        $status = Request::get('status');/*
        $start_date = Request::get('start_date');
        $end_date = Request::get('end_date');*/
        $tabFilter = Request::get('tabFilter');

        $subcompanies = Companies::where('parent_id', '=', $company_id)->get();

        if (!isset($tabFilter)) {
            $tabFilter = false;
            return View::make('companies.shipmentsExport')
                ->with('orders', [])
                ->with('statusDef', $statusDef)
                ->with('specificorders', [])
                ->with('singleorder', [])
                ->with('tabFilter', $tabFilter)
                //->with('companies', $companies)
                ->with('statusDef', $statusDef)
                ->with('company', $company_id)
                ->with('adminCities', $adminCities)
                ->with('companydata', $companydata)
                ->with('subcompanies', $subcompanies)
                ->with('num_delivery_attempts', $num_delivery_attempts)
                ->with('start_date', $start_date)
                ->with('end_date', $end_date);
        }
        if (!isset($status)) {
            $status = 'all';
        }
        if (!isset($shipmentDate) || empty($shipmentDate)) {
            $shipmentDate = 'all';
        }

        /*
     if (!isset($start_date) || empty($start_date) || !isset($end_date) || empty($end_date)) {
         $start_date = 'all';
         $end_date = 'all';
     }*/

        $ordersquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->leftJoin('undelivered_reasons','undelivered_reasons.id','=','delivery_request.undelivered_reason')->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->select(
            ['delivery_request.id',
                'jollychic',
                'waybill',
                'order_number',
                'companies.company_name',
                'delivery_request.sender_name',
                'undelivered_reason',
                'undelivered_reasons.reason_code',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_district',
                'd_city',
                'cash_on_delivery',
                'cod_currency',
                'custom_value',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                'status',
                DB::raw('DATE(sort_date) as sort_date'),
                DB::raw('DATE(new_shipment_date) as new_shipment_date'),
                DB::raw('DATE(signin_date) as signin_date'),
                DB::raw('DATE(scheduled_shipment_date) as scheduled_shipment_date'),
                'dropoff_timestamp',
                DB::raw('DATE(return_to_supplier_date) as return_to_supplier_date')]);

        if(Request::has('for')) {
            $for = Request::get('for');
            if($for == 'kpi_report') {
                $id = Request::get('id');
                $from = Request::get('from');
                $to = Request::get('to');
                $cities = explode(',', Request::get('cities')); 
                $companies = array($companydata->id);
                if(in_array($companydata->id, array('911', '946', '974', '975')))
                    $companies = array('911', '946', '974', '975');
                $ordersquery = $ordersquery->leftJoin('subcity', 'delivery_request.d_city', '=', 'subcity.subcity')
                ->whereIn('delivery_request.company_id', $companies)
                ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
                ->whereIn('subcity.city', $cities);
                if($id == 'cod_transfer_not_paid') {
                    $ordersquery = $ordersquery->leftJoin('captain_account', 'delivery_request.jollychic', '=', 'captain_account.waybill')
                    ->leftJoin('captain_account_history', 'captain_account.captain_account_history_id', '=', 'captain_account_history.id')
                    ->leftJoin('banks_deposit', 'captain_account_history.banks_deposit_id', '=', 'banks_deposit.id')
                    ->where('delivery_request.status', '=', '5')
                    ->where(function($query) {
                        $query->where('is_money_received', '=', '0')
                        ->orWhere('captain_account_history.banks_deposit_id', '=', '0')
                        ->orWhere('banks_deposit.paid_to_supplier', '=', '0');
                    });
                }
                else if($id == 'kpi_prepaid_sr') {
                    $ordersquery = $ordersquery->whereRaw("!(cash_on_delivery > 0)")
                    ->whereIn('delivery_request.status', array(3,4,5,6,7));
                }
                else if($id == 'kpi_cod_sr') {
                    $ordersquery = $ordersquery->where('cash_on_delivery', '>', '0')
                    ->whereIn('delivery_request.status', array(3,4,5,6,7));
                }
                else if($id == 'kpi_ttl_sr') {

                }
                else if($id == 'kpi_rto') {
                    $ordersquery = $ordersquery->where('status', '=', '7');
                }
                else if($id == 'kpi_tier1') {
                    $ordersquery = $ordersquery->where('status', '=', '5')
                    ->where('subcity.tier', '=', '1');
                }
                else if($id == 'kpi_tier2') {
                    $ordersquery = $ordersquery->where('status', '=', '5')
                    ->where('subcity.tier', '=', '2');
                }
                else if($id == 'kpi_tier3') {
                    $ordersquery = $ordersquery->where('status', '=', '5')
                    ->where('subcity.tier', '=', '3');
                }
                else if($id == 'cs_zero_attempts_t1') {
                    $ordersquery = $ordersquery->where('num_faild_delivery_attempts', '=', '0')
                    ->where('delivery_request.status', '=', '5')
                    ->where('subcity.tier', '=', '1');
                }
                else if($id == 'cs_zero_attempts_t2') {
                    $ordersquery = $ordersquery->where('num_faild_delivery_attempts', '=', '0')
                    ->where('delivery_request.status', '=', '5')
                    ->where('subcity.tier', '=', '2');
                }
                else if($id == 'cs_zero_attempts_t3') {
                    $ordersquery = $ordersquery->where('num_faild_delivery_attempts', '=', '0')
                    ->where('delivery_request.status', '=', '5')
                    ->where('subcity.tier', '=', '3');
                }
                else if($id == 'cs_returned') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '1');
                }
                else if($id == 'cs_returned_cs') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '1')
                    ->whereRaw('jollychic in (select waybill from tickets)');
                }
                else if($id == 'cs_request_urgent_delivery') {
                    $ordersquery = $ordersquery->where('urgent_delivery', '=', '1');
                }
                else if($id == 'cs_request_investigation') {
                    $ordersquery = $ordersquery->whereRaw("jollychic in (select waybill from tickets where type = 7)");
                }
                else if($id == 'rs_total_delivered') {
                    $ordersquery = $ordersquery->where('status', '=', '5');
                }
                else if($id == 'rs_total_returned') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0');
                }
                else if($id == 'rs_total_returned_unanswered') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('undelivered_reason', '=', '1');
                }
                else if($id == 'rs_total_returned_rejected') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('undelivered_reason', '=', '2');
                }
                else if($id == 'rs_total_returned_canceled_customer') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('undelivered_reason', '=', '3');
                }
                else if($id == 'rs_total_returned_wrong_address') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('undelivered_reason', '=', '8');
                }
                else if($id == 'rs_total_returned_wrong_number') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('undelivered_reason', '=', '7');
                }
                else if($id == 'rs_rescheduled_total') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('scheduled_shipment_date', '>=', date('Y-m-d'));
                }
                else if($id == 'rs_rescheduled_enduser') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('scheduled_shipment_date', '>=', date('Y-m-d'))
                    ->where('undelivered_reason', '=', '15'); 
                }
                else if($id == 'rs_rescheduled_captain') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('scheduled_shipment_date', '>=', date('Y-m-d'))
                    ->where('undelivered_reason', '=', '14'); 
                }
                else if($id == 'rs_rescheduled_saee') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('scheduled_shipment_date', '>=', date('Y-m-d'))
                    ->where('undelivered_reason', '=', '6'); 
                }
                else if($id == 'rs_rescheduled_companycs') {
                    $ordersquery = $ordersquery->where('status', '=', '3')
                    ->where('num_faild_delivery_attempts', '>', '0')
                    ->where('scheduled_shipment_date', '>=', date('Y-m-d'))
                    ->where('undelivered_reason', '=', '16'); 
                }
                else if($id == 'rs_ofd') {
                    $ordersquery = $ordersquery->where('status', '=', '4');
                }
                else if($id == 'rs_inwarehouse') {
                    $ordersquery = $ordersquery->where('status', '=', '3');
                }
                else if($id == 'rs_passed_return_date') {
                    $ordersquery = $ordersquery->whereNotIn('status', array(5,7))
                    ->whereRaw("DATEDIFF(now(), new_shipment_date) > companies.age_limit");
                }
                $ordersquery = $ordersquery->get();
                $timestamp = date('Y-m-d_H-i-s');
                $filename = "KPIReport__" . $timestamp . '__' . rand(1, 1000) . ".csv";
                $file = fopen(public_path() . '/import/' . $filename, 'w+');
                fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
                fputcsv($file, array(
                    'Waybill', 
                    'Company Name', 
                    'Order Number', 
                    'City', 
                    'Reason', 
                    'Reason Code',
                    'Name', 
                    'Mobile1', 
                    'Mobile2',
                    'Street Address',
                    'District',
                    'COD',
                    'Pincode',
                    'Captain',
                    'Status',
                    'Pickup Date', 
                    'Delivery Date'
                ));
                foreach($ordersquery as $order) {
                    // undelivered reason
                    $undelivered_reason = $order->undelivered_reason;
                    if($undelivered_reason == 0)
                        $undelivered_reason = 'Nothing';
                    else
                        $undelivered_reason = $UndeliveredReasonMap[$order->undelivered_reason]->english;
                    // captain name
                    if ($order->first_name)
                        $cnf_cap = $order->first_name . ' ' . $order->last_name . ' 0' . substr(str_replace(' ', '', $order->phone), -9) . ' /C';
                    else if($order->planned_walker != '0') {
                        $cnf_cap = 'Not Set';
                        $cap_info = Walker::select(['first_name', 'last_name', 'phone'])->where('id', '=', $order->planned_walker)->get();
                        if(count($cap_info) > 0) {
                            $cap_info = $cap_info[0];
                            $cnf_cap = $cap_info->first_name . ' ' . $cap_info->last_name . ' 0' . substr(str_replace(' ', '', $cap_info->phone), -9) . ' /P';
                        }
                    }
                    else
                        $cnf_cap = 'Not Set';
                    // status
                    if ($order->status == 3 && $order->num_faild_delivery_attempts > 0 && strtotime($order->scheduled_shipment_date) >= strtotime(date('Y-m-d')))
                        $status = 'Warehouse/Res';
                    else if ($order->status == 3 && $order->num_faild_delivery_attempts > 0)
                        $status = 'Warehouse/R';
                    else if($order->status == 3 && $order->num_faild_delivery_attempts == 0)
                        $status = 'Warehouse/New';
                    else
                        $status = $statusDef[$order->status];
                    // delivery date
                    if($order->delivery_date)
                        $delivery_date = date('Y-m-d', strtotime($order->delivery_date));
                    else
                        $delivery_date =  'Not Delivered';
                    fputcsv($file, array(
                        $order->jollychic,
                        $order->sender_name ? $order->company_name . ' [' . $order->sender_name . ']' : $order->company_name,
                        $order->order_number,
                        $order->d_city,
                        $undelivered_reason,
                        $order->reason_code, 
                        $order->receiver_name,
                        $order->receiver_phone,
                        $order->receiver_phone2,
                        $order->d_address,
                        $order->d_district,
                        $order->cash_on_delivery,
                        $order->pincode,
                        $cnf_cap,
                        $status,
                        $order->sort_date,
                        $delivery_date
                    )); 
                }
                fclose($file);
                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return Response::download(public_path() . '/import/' . $filename, $filename, $headers);
            }
        }

        $ordersquery = $ordersquery->whereBetween(DB::raw('date(delivery_request.created_at)'), array($start_date, $end_date));

        if ($status != 'all') {
            $ordersquery = $ordersquery->where('status', '=', $status);
        }

        if ($city != 'All') {
            if ($checkgroup == 0) {
            $ordersquery = $ordersquery->where('d_city', '=', $city);
        }
        else{
            $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                $ordersquery = $ordersquery->whereRaw($subcities);
        }
        
        }

        if ($shipmentDate != 'all') {
            $ordersquery = $ordersquery->where('scheduled_shipment_date', '=', $shipmentDate);
        } /*else if ($start_date != 'all' && $end_date != 'all') {
            $allorderquery = $ordersquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
        }*/

        if($subcompany != 'all') {
            $ordersquery = $ordersquery->where('company_id', '=', $subcompany);
        } else {
            $sub = array($company_id);
            if(in_array($company_id, array('911', '946', '974', '975')))
                $sub = array('911', '946', '974', '975');
            foreach($subcompanies as $subcomp) {
                array_push($sub, $subcomp->id);
            }
            $ordersquery = $ordersquery->whereIn('company_id', $sub); 
        }

        if(isset($num_delivery_attempts) && $num_delivery_attempts != 'all') {
            $ordersquery = $ordersquery->where('num_faild_delivery_attempts', '=', $num_delivery_attempts);
        }

        $orders = $ordersquery->get();

        for($i = 0; $i < count($orders); ++$i)
            if($orders[$i]->company_name == 'salasa')
                $orders[$i]->company_name = $orders[$i]->sender_name;
            else $orders[$i]->company_name = $orders[$i]->company_name;

        return View::make('companies.shipmentsExport')
            ->with('orders', $orders)
            ->with('specificorders', [])
            ->with('statusDef', $statusDef)
            ->with('singleorder', [])
            ->with('tabFilter', $tabFilter)
            ->with('city', $city)
            ->with('status', $status)
            ->with('statusDef', $statusDef)
            ->with('shipmentDate', $shipmentDate)
            ->with('company', $company_id)
            ->with('companydata', $companydata)
            ->with('subcompanies', $subcompanies)
            ->with('subcompany', $subcompany)
            ->with('UndeliveredReasonMap',$UndeliveredReasonMap)
            ->with('num_delivery_attempts',$num_delivery_attempts)
            ->with('adminCities', $adminCities)
            ->with('start_date', $start_date)
            ->with('end_date', $end_date);
    }

    //shipmentsIdExport functin
    public function shipmentsIdExport()
    {

        $UndeliveredReason = UndeliveredReason::all();
        $UndeliveredReasonMap = array();
        foreach($UndeliveredReason as $one_reason) 
            $UndeliveredReasonMap[$one_reason->id] = $one_reason;

        $order_statuses = OrderStatuses::all();
        $statusDef = array();
        foreach($order_statuses as $or_st) 
            $statusDef[$or_st->id] = $or_st->english;
        
        $admin_id = Session::get('admin_id');
        $adminCities =City::select('name as city')->orderBy('name', 'asc')->distinct()->get();
        $ids = Request::get('id');
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $ids = str_replace(' ', '', $ids);

         if (preg_match('/^JC[0-9]{8}KS/', $ids)) {
                    $waybillnumber = Request::get('trackingnum');
                }
                elseif (preg_match('/^OS[0-9]{8}KS/',$ids)) {
                    $waybillnumber = Request::get('trackingnum');
                }
                else{
                     //if its order number get the jollychic first by order number
                     $newwaybill = DeliveryRequest::select('jollychic')->where('order_number', '=', $ids)->get();
                 
                     $waybillnumber = $newwaybill[0]->jollychic;
                }

        //$ids =  explode(PHP_EOL, $ids);  // fetching end of line records

        //$ids = array_filter($ids);      // filtering empty values


        $tabFilter = Request::get('tabFilter');

        $ordersquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->select(
            ['delivery_request.id',
                'jollychic',
                'order_number',
                'undelivered_reason',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_district',
                'd_city',
                'cash_on_delivery',
                'cod_currency',
                'custom_value',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                DB::raw('DATE(sort_date) as sort_date'),
                DB::raw('DATE(new_shipment_date) as new_shipment_date'),
                DB::raw('DATE(signin_date) as signin_date'),
                DB::raw('DATE(scheduled_shipment_date) as scheduled_shipment_date'),
                'dropoff_timestamp',
                DB::raw('DATE(return_to_supplier_date) as return_to_supplier_date'),
                'status'])->where('company_id', '=', $company_id)->where('jollychic', 'like', '%' . $waybillnumber . '%');


        $singleorder = $ordersquery->get();


        $companies = Companies::all(array('id', 'company_name as name'));
        $subcompanies = Companies::where('parent_id', '=', $company_id)->get();

        for($i = 0; $i < count($singleorder); ++$i)
            if($singleorder[$i]->company_name == 'salasa')
                $singleorder[$i]->company_name = $singleorder[$i]->sender_name;

        return View::make('companies.shipmentsExport')
            ->with('singleorder', $singleorder)
            ->with('specificorders', [])
            ->with('orders', [])
            ->with('statusDef', $statusDef)
            ->with('tabFilter', $tabFilter)
            ->with('companies', $companies)
            ->with('companydata', $companydata)
            ->with('UndeliveredReasonMap',$UndeliveredReasonMap)
            ->with('subcompanies', $subcompanies)
            ->with('adminCities', $adminCities);
    }


// shipmentsIdsExport functiom
    public function shipmentsIdsExport()
    {

        $UndeliveredReason = UndeliveredReason::all();
        $UndeliveredReasonMap = array();
        foreach($UndeliveredReason as $one_reason) 
            $UndeliveredReasonMap[$one_reason->id] = $one_reason;

        $order_statuses = OrderStatuses::all();
        $statusDef = array();
        foreach($order_statuses as $or_st) 
            $statusDef[$or_st->id] = $or_st->english;

        $admin_id = Session::get('admin_id');
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->distinct()->get();

        $ids = Request::get('ids');
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $ids = explode(PHP_EOL, $ids);  // fetching end of line records

        $ids = array_values(array_map('trim', array_filter($ids)));

        $tabFilter = Request::get('tabFilter');
        $ordersquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->leftJoin('undelivered_reasons','undelivered_reasons.id','=','delivery_request.undelivered_reason')->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->select(
            ['delivery_request.id',
                'jollychic',
                'waybill',
                'order_number',
                'companies.company_name',
                'delivery_request.sender_name',
                'undelivered_reason',
                'undelivered_reasons.reason_code',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_district',
                'd_city',
                'cash_on_delivery',
                'cod_currency',
                'custom_value',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                DB::raw('DATE(sort_date) as sort_date'),
                DB::raw('DATE(new_shipment_date) as new_shipment_date'),
                DB::raw('DATE(signin_date) as signin_date'),
                DB::raw('DATE(scheduled_shipment_date) as scheduled_shipment_date'),
                'dropoff_timestamp',
                DB::raw('DATE(return_to_supplier_date) as return_to_supplier_date'),
                'status'])->where('company_id', '=', $company_id)->where(function ($ordersquery) use ($ids) {
                    for ($i = 0; $i < count($ids); $i++) {
                         if (preg_match('/^JC[0-9]{8}KS/', $ids[$i])) {
                            $searchcolumn = 'jollychic';
                        }

                        elseif (preg_match('/^OS[0-9]{8}KS/', $ids[$i])) {
                            $searchcolumn = 'waybill';
                        }
                        else {
                            $searchcolumn = 'order_number';
                        }
                        if (!empty($ids[$i]))
                            $ordersquery->orwhere($searchcolumn, 'like', '%' . $ids[$i] . '%');
                    }
                });


        $specificorders = $ordersquery->get();

        $companies = Companies::all(array('id', 'company_name as name'));
        $subcompanies = Companies::where('parent_id', '=', $company_id)->get();

        for($i = 0; $i < count($specificorders); ++$i)
            if($specificorders[$i]->company_name == 'salasa')
                $specificorders[$i]->company_name = $specificorders[$i]->sender_name;

        return View::make('companies.shipmentsExport')
            ->with('specificorders', $specificorders)
            ->with('orders', [])
            ->with('singleorder', [])
            ->with('statusDef', $statusDef)
            ->with('tabFilter', $tabFilter)
            ->with('companies', $companies)
            ->with('companydata', $companydata)
            ->with('subcompanies', $subcompanies)
            ->with('adminCities', $adminCities)
            ->with('company',$company_id)
            ->with('UndeliveredReasonMap',$UndeliveredReasonMap)
            ->with('start_date', '')
            ->with('end_date', '');
    }

//totalNewShipments Functiom
public function totalNewShipments()
{

    $start_date = Request::get('start_date');
    $end_date = Request::get('end_date');
    $admin_id = Session::get('admin_id');
    $adminCities = City::select('name as city')->orderBy('name', 'asc')->distinct()->get();
    $company_id = Session::get('company_id');
    $companydata = Companies::find($company_id);
    $city = Request::get('city');


    if (!isset($city)) {
        if (isset($adminCities[0]))
            $city = $adminCities[0]->city;
        else
            $city = 'All';
    }
    if (!isset($status)) {
        $status = 'all';
    }
    if (!isset($start_date) || empty($start_date) || !isset($end_date) || empty($end_date)) {
        $start_date = '2017-11-01';
        $end_date = '2018-12-31';
    }
    $distintdatequeries = "select distinct new_shipment_date from delivery_request where company_id = 911 and  d_city = 'Jeddah' and new_shipment_date between \"$start_date\" and \"$end_date\" order by new_shipment_date desc";

    $scheduledshipmentdates = DB::select("select distinct new_shipment_date from delivery_request where company_id = '$company_id' and  d_city = '$city' and new_shipment_date between ? and ? order by new_shipment_date desc", [$start_date, $end_date]);
    //print_r($scheduledshipmentdates);exit;
    /*
     *  $scheduledshipmentdates = 'select distinct new_shipment_date from delivery_request';
     $scheduledshipmentdates .=' where company_id = "'.$company_id.'"';
     $scheduledshipmentdates .=' where d_city = "'.$city.'"';
     $scheduledshipmentdates .=' where new_shipment_date between "'.$start_date.'" and "'.$end_date.'"';

     Print_r($scheduledshipmentdates);exit();
*/
    Log::info(count($scheduledshipmentdates));
    $totalnewrecords = array();
    $returnedamount = 0;
    $returnedcount = 0;

    $totalreturnedamount = DeliveryRequest::select('cash_on_delivery')->where('company_id', '=', $company_id)->whereBetween('created_at', array($start_date, $end_date));

    $totalreturnedamount = $totalreturnedamount->where('status', '=', '7');


    foreach ($scheduledshipmentdates as $shipmentdate) {
        $totalnewpackagesamount = DeliveryRequest::select('cash_on_delivery')->where('new_shipment_date', '=', $shipmentdate->new_shipment_date);


        /*$totalnewpackagesamount = $totalnewpackagesamount->whereNotIn('shipment_date', function ($query)use ($shipmentdate) {
            $query->select('shipment_date')->distinct()->from('delivery_request')
                ->whereDate('scheduled_shipment_date', '<', $shipmentdate->scheduled_shipment_date)->whereNotNull('shipment_date');
        });


        if ($company != 'all') {
            // $allorderquery = $allorderquery->where('company_id', '=', $company);
            $totalnewpackagesamount = $totalnewpackagesamount->where('company_id', '=', $company);


        }
*/
        if ($city != 'All') {
            // $allorderquery = $allorderquery->where('d_city', 'like', $city);
            $totalnewpackagesamount = $totalnewpackagesamount->where('d_city', '=', $city);

        }
        //$allitems = $allorderquery->count();
        $totalnewpackagescount = $totalnewpackagesamount->count('cash_on_delivery');
        $totalnewpackagesamount = $totalnewpackagesamount->sum('cash_on_delivery');

        array_push($totalnewrecords, array('shipmentdate' => $shipmentdate->new_shipment_date, "newcount" => $totalnewpackagescount, "newamount" => $totalnewpackagesamount));

        Log::info('dd');

    }
    //print_r($totalnewrecords);exit;
    /* $allorderquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->select(
         ['delivery_request.id',
             'jollychic',
             'receiver_name',
             'receiver_phone',
             'receiver_phone2',
             'd_address',
             'd_city',
             'd_district',
             'cash_on_delivery',
             'pincode',
             'planned_walker',
             'confirmed_walker',
             'walker.first_name as first_name',
             'walker.last_name as last_name',
             'walker.phone as phone',
             'num_faild_delivery_attempts',
             'status'])->whereNotIn('shipment_date', function ($query) {
         $query->select('shipment_date')->distinct()->from('delivery_request')
             ->where('scheduled_shipment_date', '<', '2018-03-20')->whereNotNull('shipment_date');

     });*/
    $totalnewpackagesamount = DeliveryRequest::select('cash_on_delivery');

    //$totalreturnedamount = DeliveryRequest::select('cash_on_delivery');

    /*$totalnewpackagesamount = $totalnewpackagesamount->whereNotIn('shipment_date', function ($query) {
        $query->select('shipment_date')->distinct()->from('delivery_request')
            ->where('scheduled_shipment_date', '<', '2018-03-20')->whereNotNull('shipment_date');

    });


    if ($company != 'all') {
        // $allorderquery = $allorderquery->where('company_id', '=', $company);
        $totalnewpackagesamount = $totalnewpackagesamount->where('company_id', '=', $company);
        $totalreturnedamount = $totalreturnedamount->where('company_id', '=', $company);

    }*/


    if ($city != 'All') {
        // $allorderquery = $allorderquery->where('d_city', 'like', $city);
        $totalnewpackagesamount = $totalnewpackagesamount->where('d_city', '=', $city);
        $totalreturnedamount = $totalreturnedamount->where('d_city', '=', $city);
    }
    //$allitems = $allorderquery->count();
    $totalnewpackagesamount = $totalnewpackagesamount->sum('cash_on_delivery');
    //$totalreturnedamount = $totalreturnedamount->sum('cash_on_delivery');

    $returnedcount = $totalreturnedamount->count('cash_on_delivery');
    $returnedamount = $totalreturnedamount->sum('cash_on_delivery');

    $cashcollected = '0.0';
    $cashtobecollected = '0.0';

    //$orders = $allorderquery->paginate(10);
    //$orderscount = $orders->count();
    // $companies = Companies::all(array('id', 'company_name as name'));

    $order_statuses = OrderStatuses::all();
    $statusDef = array();
    foreach($order_statuses as $or_st) 
        $statusDef[$or_st->id] = $or_st->english;     

    return View::make('companies.newshipments')
        //->with('orders', $orders)
        //->with('allitems', $allitems)
        //->with('orderscount', $orderscount)
        ->with('statusDef', $statusDef)
        ->with('totalnewrecords', $totalnewrecords)
        ->with('returnedamount', $returnedamount)
        ->with('returnedcount', $returnedcount)
        //->with('companies', $companies)
        //->with('company', $company)
        ->with('city', $city)
        ->with('start_date', $start_date)
        ->with('end_date', $end_date)
        ->with('companydata', $companydata)
        ->with('adminCities', $adminCities);


}

    public function shipmentsDetails() {

        $order_statuses = OrderStatuses::all();
        $statusDef = array();
        foreach($order_statuses as $or_st) 
            $statusDef[$or_st->id] = $or_st->english;

        $shipmentDate = Request::get('shipment_date');
        if (!isset($shipmentDate))
            $shipmentDate = Request::segment(3);
        if($shipmentDate == 'report')
            $shipmentDate = 'all';
        // $company = Request::get('company');
        $city = Request::get('city');
        $status = Request::get('status');
        $start_date = Request::get('start_date');
        $end_date = Request::get('end_date');
        $company_id = Session::get('company_id');
        $adminCities = City::select('name as city')->orderBy('id', 'asc')->distinct()->get();
        $companydata = Companies::find($company_id);
        $from = Request::get('from');
        $to = Request::get('to');

        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
            else
                $city = 'All';
        }

        if (!isset($status)) {
            $status = 'all';
        }
        if (!isset($start_date) || empty($start_date) || !isset($end_date) || empty($end_date)) {
            $start_date = 'all';
            $end_date = 'all';
        }
        if(!isset($from))
            $from = date('Y-m-01');
        if(!isset($to))
            $to = date('Y-m-d');

        $query = DeliveryRequest::select([
            DB::raw('date(new_shipment_date) as "new_shipment_date"'),
            DB::raw('sum(status=1)+sum(status=2)+sum(status=3)+sum(status=4)+sum(status=5)+sum(status=6)+sum(status=7) as "totalDeliveriesOFD"'),
            DB::raw('sum(status=5) as "delivered"'),
            DB::raw('sum(status=3 and undelivered_reason <> 3 or status = 4 or status = 6) as "pending"'),
            DB::raw('sum(status=3 and undelivered_reason = 3) as "canceled"'),
            DB::raw('sum(status=7) as "returnToSupplier"')
        ])
        ->where('company_id', '=', $companydata->id)
        ->whereBetween(DB::raw("date(new_shipment_date)"), array($from, $to))
        ->groupBy(DB::raw("date(new_shipment_date)"))
        ->orderBy('new_shipment_date', 'desc')
        ->get();

        //$maxstshipmentdate =  DeliveryRequest::where('jollychic','!=','0')->max('shipment_date');

        $allitems = 0;
        $penddingitems = 0;
        $onwayitems = 0;
        $arriveditems = 0;
        $outfordeliveryitems = 0;
        $delivreditems = 0;
        $undelivreditems = 0;
        $returnedtojcitems = 0;
        $cashcollected = '0.0';
        $cashtobecollected = '0.0';
        $numberOfShipments = 0;
        $allorderquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->select(
            ['delivery_request.id',
                'jollychic',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_city',
                'd_district',
                'cash_on_delivery',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                'status']);
        $withcaptainsordersquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->whereIn('status', [4, 6])->select(
            ['delivery_request.id',
                'jollychic',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_city',
                'd_district',
                'cash_on_delivery',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                'status']);

        $deliveredordersquery = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')->where('status', '=', '5')->select(
            ['delivery_request.id',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_city',
                'd_district',
                'cash_on_delivery',
                'pincode',
                'planned_walker',
                'confirmed_walker',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'walker.phone as phone',
                'num_faild_delivery_attempts',
                'status']);
        $arriveditemsquery = DeliveryRequest::whereIn('status', [3]);

        $cashtobecollectedquery = DeliveryRequest::whereNotIn('status', [5, 7]);
        if ($company_id) {
            $allorderquery = $allorderquery->where('company_id', '=', $company_id);
            $withcaptainsordersquery = $withcaptainsordersquery->where('company_id', '=', $company_id);
            $deliveredordersquery = $deliveredordersquery->where('company_id', '=', $company_id);
            $arriveditemsquery = $arriveditemsquery->where('company_id', '=', $company_id);
            $cashtobecollectedquery = $cashtobecollectedquery->where('company_id', '=', $company_id);
        }


        if ($city != 'All') {
            $allorderquery = $allorderquery->where('d_city', 'like', $city);
            $withcaptainsordersquery = $withcaptainsordersquery->where('d_city', 'like', $city);
            $deliveredordersquery = $deliveredordersquery->where('d_city', 'like', $city);
            $cashtobecollectedquery = $cashtobecollectedquery->where('d_city', 'like', $city);
            $arriveditemsquery = $arriveditemsquery->where('d_city', '=', $city);
        }
        if ($status != 'all') {
            $allorderquery = $allorderquery->where('status', $status);
        }
        if ($shipmentDate != 'all') {
            $allorderquery = $allorderquery->where('scheduled_shipment_date', '=', $shipmentDate);
            $withcaptainsordersquery = $withcaptainsordersquery->where('scheduled_shipment_date', '=', $shipmentDate);
            $deliveredordersquery = $deliveredordersquery->where('scheduled_shipment_date', '=', $shipmentDate);
            $arriveditemsquery = $arriveditemsquery->where('scheduled_shipment_date', '=', $shipmentDate);
            $cashtobecollectedquery = $cashtobecollectedquery->where('scheduled_shipment_date', '=', $shipmentDate);
        } else if ($start_date != 'all' && $end_date != 'all') {
            $allorderquery = $allorderquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
            $withcaptainsordersquery = $withcaptainsordersquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
            $deliveredordersquery = $deliveredordersquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
            $arriveditemsquery = $arriveditemsquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
            $cashtobecollectedquery = $cashtobecollectedquery->where('scheduled_shipment_date', '>', $start_date)->where('scheduled_shipment_date', '<', $end_date);
        }

        
        $arriveditems = $arriveditemsquery->count();
        $allitems = $allorderquery->count();
    
        $outfordeliveryitems = $withcaptainsordersquery->count();

        $delivreditems = $deliveredordersquery->count();
        $cashcollected = '0.0';
        $cashcollected = $deliveredordersquery->sum('cash_on_delivery');
        $cashtobecollected = '0.0';
        $cashtobecollected = $cashtobecollectedquery->sum('cash_on_delivery');

        if ($allitems != 0) {
            $successrate = $delivreditems / $allitems;
            $successrate = $successrate * 100.0;
        } else {
            $successrate = 0;
        }
        $orders = $allorderquery->paginate(10);
        $orderscount = $orders->count();
        $withcaptainsorders = $withcaptainsordersquery->paginate(10);
        $orderscountwithcaptain = $withcaptainsorders->count();
        $deliveredorders = $deliveredordersquery->paginate(10);
        $orderscountdelivered = $deliveredorders->count();

        $companies = Companies::all(array('id', 'company_name as name'));
        return View::make('companies.shipments')
            ->with('orders', $orders)
            ->with('statusDef', $statusDef)
            ->with('withcaptainsorders', $withcaptainsorders)
            ->with('deliveredorders', $deliveredorders)
            ->with('shipmentDate', $shipmentDate)
            ->with('allitems', $allitems)
            ->with('arriveditems', $arriveditems)
            ->with('outfordeliveryitems', $outfordeliveryitems)
            ->with('delivreditems', $delivreditems)
            ->with('cashcollected', $cashcollected)
            ->with('cashtobecollected', $cashtobecollected)
            ->with('successrate', $successrate)
            ->with('companies', $companies)
            ->with('company', $company_id)
            ->with('city', $city)
            ->with('status', $status)
            ->with('shipmentDate', $shipmentDate)
            ->with('from', $from)
            ->with('to', $to)
            ->with('report', $query)
            ->with('orderscount', $orderscount)
            ->with('orderscountwithcaptain', $orderscountwithcaptain)
            ->with('orderscountdelivered', $orderscountdelivered)
            ->with('companydata', $companydata)
            ->with('adminCities', $adminCities);


    }

    public function packageDetails()
    {
        $packageID = Request::segment(3);

        $package = DeliveryRequest::where('jollychic', '=', $packageID)->get()->first();

        $company_id = Session::get('company_id');

        $companydata = Companies::find($company_id);

        $order_statuses = OrderStatuses::all();
        $statusDef = array();
        foreach($order_statuses as $or_st) 
            $statusDef[$or_st->id] = $or_st->english;     

        $reason = UndeliveredReason::find($package->undelivered_reason);

        if (isset($reason)) {
            $package->undelivered_reason = $reason->english;
        }
        if (count($package)) {
            $package->d_address = addslashes($package->d_address);
            $walkerdata = Walker::find($package->confirmed_walker);
            $package->confirmed_walker = 'notset';
            if (isset($walkerdata)) {
                $package->confirmed_walker = $walkerdata->first_name;
                $package->confirmed_walker .= " 0";
                $package->confirmed_walker .= substr(str_replace(' ', '', $walkerdata->phone), -9);
            }
            //$package->status = $statusDef[$package->status];
        }

        // Edit only if status is 0 else return flash message

        return View::make('companies.packageDetails')
            ->with('packagedetails', $package)
            ->with('statusDef', $statusDef)
            ->with('companydata', $companydata)
            ->with('package', $package);


    }



    public function updatecontact()
    {

        $company_id = Session::get('company_id');
        if (Request::has('waybill')) {
            $waybill = Request::get('waybill');

            $item = DeliveryRequest::where('jollychic', '=', $waybill)->get()->first();
            if (!count($item)) {
                $response_array = array('success' => false, 'error' => 'Need a valid waybill number');
                $response = Response::json($response_array, 200);
                return $response;
            }

        } else {
            $response_array = array('success' => false, 'error' => 'Need a valid waybill number');
            $response = Response::json($response_array, 200);
            return $response;
        }

        $sender_name = Request::get('sender_name');
        $sender_phone = Request::get('sender_phone');
        $name =    Request::get('receiver_name');
        $mobile1 = Request::get('mobile1');
        $mobile2 = Request::get('mobile2');
        $streetaddress = Request::get('street_address');
        $streetaddress2 = Request::get('street_address_2');
        $district = Request::get('district');
        $city     = Request::get('city');
        $cod      = Request::get('cash_on_delivery');
        $quantity = Request::get('quantity');
        $weight   = Request::get('weight');
        $description = Request::get('description');
        $order_number = Request::get('order_number');

        $notes = Request::get('notes');

        $is_district_updated = 0;

        if (strpos($district, '+') !== false) {

                $d_cityifneedtoupdate = strtok($district, '+');

                $district = strtok( '' ); 
            }
            
        $changes = '';

        if ($item->sender_name != $sender_name) {
            $changes .= ' of area rep. name to ' . $sender_name;
        }

        if ($item->sender_phone != $sender_phone) {
            $changes .= ' of area rep. mobile to ' . $sender_phone;
        }

        if ($item->receiver_name != $name) {
            $changes .= ' of name to ' . $name;
        }

        if ($item->d_district != $district) {
            $changes .= ' of address to ' . $district;
             $is_district_updated = 1;
        }

        if ($item->receiver_phone != $mobile1) {
            $changes .= ' of mobile1 to ' . $mobile1;
        }

        if ($item->receiver_phone2 != $mobile2) {
            $changes .= ' of mobile2 to ' . $mobile2;
        }

        if ($item->d_address != $streetaddress) {
            $changes .= ' of street address to ' . $streetaddress;
        }

        if ($item->d_address2 != $streetaddress2) {
            $changes .= ' of street address 2 to ' . $streetaddress2;
        }

        if ($item->d_city != $city) {
            $changes .= ' of city to ' . $city;
        }

        if ($item->cash_on_delivery != $cod) {
            $changes .= ' of COD to ' . $cod;
        }

        if ($item->d_quantity != $quantity) {
            $changes .= ' of quantity to ' . $quantity;
        }

        if ($item->d_weight != $weight) {
            $changes .= ' of weight to ' . $weight;
        }

        if ($item->d_description != $description) {
            $changes .= ' of description to ' . $description;
        }

        if ($item->order_number != $order_number) {
            $changes .= ' of orderNumber to ' . $order_number;
        }

        $notes .= ' | changes ' . $changes;

        $order_history = new OrdersHistory;
        $order_history->waybill_number = $waybill;
        $order_history->status = $item->status;

        $order_history->admin_id = $company_id;
        $order_history->notes = $notes;
        $order_history->save();

        DeliveryRequest::where('jollychic', '=', $waybill)->update(
            array(
                'sender_name' => $sender_name,
                'sender_phone' => $sender_phone,
                'receiver_name' => $name,
                'd_district' => $district,
                'order_number' => $order_number,
                'receiver_phone' => $mobile1,
                'receiver_phone2' => $mobile2,
                'd_address' => $streetaddress,
                'd_address2' => $streetaddress2,
                'd_city'     => $city,
                'd_description' => $description,
                'cash_on_delivery' => $cod,
                'd_quantity'  => $quantity,
                'd_weight'  => $weight,
                'is_district_updated' => $is_district_updated
            ));


        header("Location: /company/package/$waybill");
        die();
        //return  redirect()->action('WarehouseController@customerservice'); redirect('/warehouse/customerservice');

    }


    public function companyDetail()
    {
        $id = Session::get('company_id');
        $companydata = Companies::find($id);
        $company = Companies::where('id', '=', $id)->first();
        return View::make('companies.companydetail')
            ->with('company', $company)
            ->with('companydata', $companydata);
    }




    //register new company and check if the email is already register in company table.
    /*data coming from ajax request*/
    public function signup()
    {
        try {

            //Get the params from the ajax request
            $name = Request::get('name');
            $email = Request::get('email');
            $password = Request::get('password');
            $phone = Request::get('phone');
            $product_type = Request::get('product_type');
            $city = Request::get('city');
            $latitude = Request::get('latitude');
            $longitude = Request::get('longitude');
            $address = Request::get('address');


            //is !(the given email already in the database)?
            if (!$this->isUser($email)) {

                try {
                    //Create company object.....
                    $newCompany = new Company();
                    $newCompany->name = $name;
                    $newCompany->phone = $phone;
                    $newCompany->email = $email;
                    $newCompany->products_type = $product_type;
                    $newCompany->password = Hash::make($password);
                    $newCompany->city = $city;
                    $newCompany->latitude = $latitude;
                    $newCompany->longitude = $longitude;
                    $newCompany->address = $address;
                    $newCompany->credit = 0;

                    //save!!
                    $newCompany->save();
                    //done get and return the id to use as a session when user loggedin
                    return 1;

                    //error unable to create a new account for unknown reason....
                } catch (Exception $e) {
                    return $e->getMessage();
                }
            } else {
                //email already taken or exist in the system, forgot your password?
                return 0;
            }
        } catch (Exception $e) {
            return $e->getMessage();

        }

    }

    //must send a link for password reset.
    public function forgotPassword()
    {

        try {
            $email = Request::get('email');
            $company = Company::where('email', '=', $email)->first();
            if ($company) {

                return $company->password;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    //update profile info and return the updated object
    public function updateProfile()
    {

        try {

            $id = Request::get('id');
            $phone = Request::get('phone');
            $name = Request::get('name');

            $latitude = Request::get('latitude');
            $longitude = Request::get('longitude');
            $address = Request::get('address');


            $company = Company::where('id', '=', $id)->first();

            $company->phone = $phone;
            $company->name = $name;
            $company->latitude = $latitude;
            $company->longitude = $longitude;
            $company->address = $address;
            $company->save();


        } catch (Exception $e) {
            return $e->getMessage();
        }

        //return company information to update session with the new updated fields
        return json_encode($company, JSON_FORCE_OBJECT);

    }


    //unnecessary function
    public function statusToString($status)
    {

        if ($status == -1) {
            return "Canceled";
        } else if ($status == 0) {

            return "Pending";
        } else if ($status == 1) {

            return "On the way";
        } else if ($status == 2) {

            return "package picked";
        } else if ($status == 3) {

            return "Delivred";
        }
    }

    public function orderimportxml()
    {
        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $duplicateOrders = array();
        $successstatistics = array();
        return View::make('companies.orderimportxml')->with('success', '')->with('duplicateOrders', $duplicateOrders)->with('successstatistics', $successstatistics)->with('companydata', $companydata);
    }

    public function XMLToArray($file)
    {
        try {
            
            $fileContents = file_get_contents($file);
            $array = str_split($fileContents); 
            $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            $xml .= "<Items>";
            $row_start = strpos($fileContents, "Worksheet");
            $columns = array();
            $flag = 1; // to put first row in columns array
            while($row_start = strpos($fileContents, "<Row", $row_start)) {
                $ss_flag = 1; // to skip some columns 
                $row_end = strpos($fileContents, "</Row>", $row_start);
                $element_start = $row_start;

                // save current row in $current_row and later put in $xml if $has_data flag = 1
                $current_row = "";
                $has_data = 0; 
                if(!$flag)
                    $current_row .= "<Item>";

                $i = 0;
                while(($element_start = strpos($fileContents, $len = "<Cell", $element_start)) && $element_start < $row_end) {
                    $element_start += strlen($len);
                    $element_tag_end = strpos($fileContents, ">", $element_start - 1);
                    // check column index for this cell
                    if(($index_pos = strpos($fileContents, $len = "ss:Index=\"", $element_start)) && $index_pos < $element_tag_end) {
                        $index_pos += strlen($len);
                        $index = "";
                        while($array[$index_pos] != '"') 
                            $index .= $array[$index_pos++];
                        $ss_flag = 0;
                        $i = $index - 1 - $ss_flag;
                    }
                    $element_start = $element_tag_end + 1;
                    // check if there is data  
                    if(($element_start = strpos($fileContents, $len = "<Data", $element_start)) && $element_start < $row_end) {
                        $has_data = 1;
                        $element_start = strpos($fileContents, $len = ">", $element_start) + strlen($len);
                        $element_end = strpos($fileContents, $len = "</Data></Cell>", $element_start);
                        // $cur_result saves cur_cell value
                        $cur_result = "";
                        while($element_start < count($array) && $element_start < $element_end) {
                            $cur_result .= $array[$element_start];
                            ++$element_start;
                        }
                        if($flag)
                            array_push($columns, $cur_result);
                        if(!$flag)
                            $current_row .= "<" . $columns[$i] . ">$cur_result</" . $columns[$i] . ">";
                        $element_start = $element_end;
                        ++$i;
                    }
                    else {
                        break;
                    }
                }
                if(!$flag)
                    $current_row .= "</Item>";
                if($has_data)
                    $xml .= $current_row;
                $row_start = $row_end;
                $flag = 0;
            }
            $xml .= "</Items>";
            
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $xmlArr = json_decode($json,true);

            if(array_key_exists("Item", $xmlArr)){
                $customerArr = $xmlArr['Item'];
            } else{
                $customerArr = '';
            };

        } catch (Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
        return $customerArr;
    }

    public function xmlorderimportinDB()
    {
        try {
            $company_id = Session::get('company_id');
            $companydata = Companies::find($company_id);
            $duplicateOrders = array();
            $successstatistics = array();

            $target_dir = "import/";
            $target_file = $target_dir . $company_id . '-' . time() . '-' . basename($_FILES["file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            // Check if file already exists
            if (file_exists($target_file)) {
                $error = "Sorry, file already exists.";
                $uploadOk = 0;
            }
            // Check file size
            // if ($_FILES["file"]["size"] > 500000) {
            //     $error = "Sorry, your file is too large.";
            //     $uploadOk = 0;
            // }

            // Allow certain file formats
            if ($imageFileType != "xml") {
                $error = "Sorry, only XML files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                $success = $error . " your file was not uploaded, Please try again.";
                return View::make('companies.orderimportxml')->with('success', $success)->with('duplicateOrders', $duplicateOrders)->with('successstatistics', $successstatistics)->with('companydata', $companydata);
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    $success = "The file " . basename($_FILES["file"]["name"]) . " has been uploaded.";
                } else {
                    $success = "Sorry, there was an error uploading your file.";
                }
            }

            $customerArr = $this->XmlToArray($target_file);

            if (empty($customerArr)) {
                return View::make('companies.orderimportxml')->with('success', 'File is not parsable, please try again.')->with('duplicateOrders', $duplicateOrders)
                    ->with('successstatistics', $successstatistics)->with('companydata', $companydata);
            }
            //check all mandatory fields

            $columnsNotFound = '';
            if (!array_key_exists("OrderNumber", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Order Number,";
            }
            if (!array_key_exists("ReceiverName", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Receiver Name,";
            }
            if (!array_key_exists("ReceiverMobile", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Receiver Mobile,";
            }
            if (!array_key_exists("City", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " City,";
            }
            if (!array_key_exists("ReceiverAddress", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Receiver Address,";
            }
            if (!array_key_exists("CODAmount", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " COD Amount,";
            }
            if (!array_key_exists("Weight", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Weight,";
            }
            if (!array_key_exists("Quantity", $customerArr[0])) {
                $columnsNotFound = $columnsNotFound . " Quantity,";
            }
            if (isset($columnsNotFound) && !empty($columnsNotFound)) {
                $success = 'Sorry some mandatory fields are not available (' . $columnsNotFound . " )";
                return View::make('companies.orderimportxml')->with('success', $success)->with('duplicateOrders', $duplicateOrders)->with('companydata', $companydata)->with('successstatistics', $successstatistics);
            }
            //Log::info($columnsNotFound);exit;
            $columns_name = 'Order Number, Receiver Name, Receiver Mobile, City, Receiver Address, COD Amount, Weight, Quantity';
            //check all optional fields
            // Receiver Mobile2 Optional
            if (array_key_exists("ReceiverMobile2", $customerArr[0])) {
                $receiver_phone2 = "ReceiverMobile2";
                $columns_name = $columns_name . ' , ' . $receiver_phone2;
            }
            //Receiver District optional
            if (array_key_exists("ReceiverDistrict", $customerArr[0])) {
                $d_district = "ReceiverDistrict";
                $columns_name = $columns_name . ' , ' . $d_district;
            }
            //Receiver Address 2 optional
            if (array_key_exists("ReceiverAddress2", $customerArr[0])) {
                $d_address2 = "ReceiverAddress2";
                $columns_name = $columns_name . ' , ' . $d_address2;
            }
            //Currency optional
            if (array_key_exists("Currency", $customerArr[0])) {
                $currency = "Currency";
                $columns_name = $columns_name . ' , ' . $currency;
            }
            //State optional
            if (array_key_exists("State", $customerArr[0])) {
                $d_state = "State";
                $columns_name = $columns_name . ' , ' . $d_state;
            }
            //Description optional
            if (array_key_exists("Description", $customerArr[0])) {
                $d_description = "Description";
                $columns_name = $columns_name . ' , ' . $d_description;
            }
            //Receiver Email optional
            if (array_key_exists("ReceiverEmail", $customerArr[0])) {
                $ConsigneeEmail = "ReceiverEmail";
                $columns_name = $columns_name . ' , ' . $ConsigneeEmail;
            }
            //Sender Name optional
            if (array_key_exists("SenderName", $customerArr[0])) {
                $sender_name = "SenderName";
                $columns_name = $columns_name . ' , ' . $sender_name;
            }
            //Lat Long optional
            if (array_key_exists("LatLong", $customerArr[0])) {
                $latlong = "LatLong";
                $columns_name = $columns_name . ' , ' . $latlong;
            }
            //Order Reference optional
            if (array_key_exists("OrderReference", $customerArr[0])) {
                $order_reference = "OrderReference";
                $columns_name = $columns_name . ' , ' . $order_reference;
            }

            $import_history = new ImportHistory;
            $import_history->company_id = $company_id;
            $import_history->admin_id = 0;
            $import_history->columns_name = $columns_name;
            $import_history->file_name = $_FILES["file"]["name"];
            $import_history->save();

            $successful_entries = 0;
            $unsuccessful_entries = 0;
            $amountImported = 0;
            $amountnotImported = 0;
            for ($i = 0; $i < count($customerArr); $i++) {

                $delivery_request = new DeliveryRequest;
                //  order number string Mandatory
                $ordernumber = trim(preg_replace('/\s+/', ' ', $customerArr[$i]["OrderNumber"]));
                if (count(DeliveryRequest::where('order_number', '=', $ordernumber)->where('company_id', '=', $company_id)->get())) {
                    array_push($duplicateOrders, array(
                        'order_number' => $ordernumber,
                        'customer_name' => $customerArr[$i]["ReceiverName"],
                        'customer_moble' => $customerArr[$i]["ReceiverMobile"],
                        'customer_city' => $customerArr[$i]["City"],
                        'cash_on_delivery' => $customerArr[$i]["CODAmount"],
                    ));
                    $codnotimport=preg_replace('/\,/', '', $customerArr[$i]["CODAmount"]);
					$codnotimport = preg_replace('/\٫/', '',preg_replace('/\٬/', '',$codnotimport));
                    //$amountnotImported += $customerArr[$i]["CODAmount"];
                    $amountnotImported += $codnotimport;
                    $unsuccessful_entries = $unsuccessful_entries + 1;
                    continue;
                }
                $delivery_request->order_number = $ordernumber;

                //  name string Mandatory
                $dropoff_name = $customerArr[$i]["ReceiverName"];
                $dropoff_name = trim(preg_replace('/\s+/', ' ', $dropoff_name));
                $delivery_request->receiver_name = $dropoff_name;
                //  mobile ints Mandatory
                $dropoff_mobile = trim(preg_replace('/\s+/', ' ', $customerArr[$i]["ReceiverMobile"]));
                if (strpos($dropoff_mobile, ' ') !== false) {
                    $dropoff_mobile = strtok($dropoff_mobile, ' ');
                    $dropoff_mobile2 = strtok('');
                    $dropoff_mobile = format_phone($dropoff_mobile);

                    $delivery_request->receiver_phone = $dropoff_mobile;

                    $dropoff_mobile2 = format_phone($dropoff_mobile2);

                    $delivery_request->receiver_phone2 = $dropoff_mobile2;


                } else {
                    $dropoff_mobile = format_phone($dropoff_mobile);

                    $delivery_request->receiver_phone = $dropoff_mobile;
                }
                // mobile2 ints Optional
                if (isset($receiver_phone2) && !empty($customerArr[$i]["$receiver_phone2"])) {
                    $dropoff_mobile2 = trim(preg_replace('/\s+/', ' ', $customerArr[$i]["$receiver_phone2"]));
                    $dropoff_mobile2 = format_phone($dropoff_mobile2);

                    $delivery_request->receiver_phone2 = $dropoff_mobile2;
                }
                //  Email ints Optional
                if (isset($ConsigneeEmail) && !empty($customerArr[$i]["$ConsigneeEmail"])) {
                    $dropoff_email = str_replace(PHP_EOL, ' ', $customerArr[$i]["$ConsigneeEmail"]);
                    if (strpos($dropoff_email, '@') !== false) {
                        $delivery_request->receiver_email = $dropoff_email;
                    }
                }
                //  city  string Mandatory
                $city = trim(preg_replace('/\s+/', ' ', $customerArr[$i]["City"]));
                $map = CityMap::where('input', '=', $city)->first();
                if(isset($map))
                    $city = $map->output;
                $delivery_request->d_city = $city;
                //   streetaddress string Mandatory
                $address = str_replace(PHP_EOL, ' ', $customerArr[$i]["ReceiverAddress"]);
                $address = trim(preg_replace('/\s+/', ' ', $address));
                $delivery_request->d_address = $address;
                //   streetaddress-2 string optional
                if (isset($d_address2) && !empty($customerArr[$i]["$d_address2"])) {

                    //str_replace('{}', '""', $d_address2);
                    $address2 = str_replace(PHP_EOL, ' ', $customerArr[$i]["$d_address2"]);
                    $address2 = trim(preg_replace('/\s+/', ' ', $address2));
                    $delivery_request->d_address2 = $address2;
                }
                // currency string Optional
                if (isset($currency) && !empty($customerArr[$i]["$currency"])) {
                    $check_currency = $customerArr[$i]["$currency"];
                    $cod = preg_replace('/\,/', '', $customerArr[$i]["CODAmount"]);
					$cod = preg_replace('/\٫/', '',preg_replace('/\٬/', '',$cod));
                    if ($check_currency == 'USD') {
                        $totalcod = 3.85 * $cod;
                        $new_cod = round($totalcod, 2);
                        $delivery_request->cash_on_delivery = $new_cod;
                        $delivery_request->cod_currency = $check_currency;
                    } else if ($check_currency == 'SAR') {
                        $delivery_request->cash_on_delivery = $cod;
                    } else {
                        $delivery_request->cash_on_delivery = $cod;
                    }
                    $amountImported += $cod;

                } else {
                    //  cashondelivery float Mandatory
                    $cod = preg_replace('/\,/', '', $customerArr[$i]["CODAmount"]);
					$cod = preg_replace('/\٫/', '',preg_replace('/\٬/', '',$cod));
                    $delivery_request->cash_on_delivery = $cod;
                    $amountImported += $cod;
                }

                // latitude longitude Optional
                if (isset($latlong) && !empty($customerArr[$i]["$latlong"])) {
                    $lat_long = $customerArr[$i]["$latlong"];

                    if (strpos($lat_long, ',') !== false) {
                        $latitude = strtok($lat_long, ',');
                        $longitude = strtok('');

                        $latitude = trim(preg_replace('/\s+/', ' ', $latitude));
                        $longitude = trim(preg_replace('/\s+/', ' ', $longitude));
                        $delivery_request->D_latitude = $latitude;
                        $delivery_request->D_longitude = $longitude;
                    }
                }
                //   $order_referenc  string Optional
                if (isset($order_reference) && !empty($customerArr[$i]["$order_reference"])) {
                    $order_referenc = $customerArr[$i]["$order_reference"];
                    $delivery_request->order_reference = $order_referenc;
                }

                //   d_description  string Optional
                if (isset($d_description) && !empty($customerArr[$i]["$d_description"])) {
                    $description = $customerArr[$i]["$d_description"];
                    $delivery_request->d_description = $description;
                }
                //   weight float Mandatory

                $weight = $customerArr[$i]["Weight"];
                $delivery_request->d_weight = $weight;

                // quantity ints Mandatory
                $quantity = $customerArr[$i]["Quantity"];
                $delivery_request->d_quantity = $quantity;

                // d_state  string Optional
                if (isset($d_state) && !empty($customerArr[$i]["$d_state"])) {
                    $state = $customerArr[$i]["$d_state"];
                    if ($state == 'Makkah' || $state == 'Jeddah' || $state == 'Taif') {
                        $state = 'Makkah{Mecca}';
                    }
                    if ($state == 'Baha') {
                        $state = 'Al Baha';
                    }
                    if ($state == 'Dammam') {
                        $state = 'Eastern Region {Ash-Sharqiyah}';
                    }
                    $delivery_request->d_state = $state;
                }
                //  sender_name  string Optional
                if (isset($sender_name) && !empty($customerArr[$i]["$sender_name"])) {
                    $sendername = $customerArr[$i]["$sender_name"];
                    $delivery_request->sender_name = $sendername;
                }
                //  district  string Optional
                if (isset($d_district) && !empty($customerArr[$i]["$d_district"])) {
                    $district = $customerArr[$i]["$d_district"];
                    $delivery_request->d_district = $district;
                }

                $delivery_request->request_start_time = date("Y-m-d H:i:s");
                $delivery_request->company_id = $company_id;
                $delivery_request->immediate_dropoff = 1;
                $delivery_request->scheduled_dropoff_date = 0;
                $delivery_request->scheduled_dropoff_time = 0;
                $delivery_request->transportation_fees = 25;
                $delivery_request->origin_latitude = 21.575723;
                $delivery_request->origin_longitude = 39.148976;
                $delivery_request->pincode = mt_rand(1000, 9999);
                $delivery_request->pincode2 = mt_rand(1000, 9999);
                $delivery_request->status = 0;
                $delivery_request->import_history_id = $import_history->id;

                $delivery_request->save();

                $dr = DeliveryRequest::find($delivery_request->id);
                $waybill = 'OS' . str_pad($dr->id, 8, "0", STR_PAD_LEFT) . 'KS';
                $dr->waybill = $waybill;
                $dr->jollychic = $waybill;
                $dr->customer_tracking_number = $dr->receiver_phone . '-' . $dr->id;
                $dr->save();

                $order_history = new OrdersHistory();
                $order_history->waybill_number = $waybill;
                $order_history->order_number = $dr->order_number;
                $order_history->status = 0;
                $order_history->notes = 'Label Created';
                $order_history->company_id = $company_id;
                $order_history->save();
                $successful_entries = $successful_entries + 1;

            }
            $import_history = ImportHistory::find($import_history->id);
            $import_history->successful_entries = $successful_entries;
            $import_history->unsuccessful_entries = $unsuccessful_entries;
            $import_history->save();
            array_push($successstatistics, array(
                'successful_entries' => $successful_entries,
                'unsuccessful_entries' => $unsuccessful_entries,
                'amountImported' => $amountImported,
                'amountnotImported' => $amountnotImported
            ));

        } catch (Exception $e) {
            Log::info($e->getMessage());
            return View::make('companies.orderimportxml')->with('success', 'File is not parsable, please try again.')->with('duplicateOrders', $duplicateOrders)
                ->with('successstatistics', $successstatistics)->with('companydata', $companydata);
        }

        return View::make('companies.orderimportxml')->with('success', $success)->with('duplicateOrders', $duplicateOrders)
            ->with('companydata', $companydata)->with('successstatistics', $successstatistics);

    }

    public function orderimport_template() {
        return Response::download(public_path() . '/downloads/SaeeShipmentsTemplate.xlsx');
    }

    //returns all requests given the company ID
    public function getAllRequests()
    {

        try {
            $company_id = Request::get('company_id');
            $requests = DeliveryRequest::
            orderBy('created_at', 'desc')
                ->where('company_id', '=', $company_id)->get();

            $i = 0;
            $allRequests = array();

            foreach ($requests as $request) {

                $request->status = $this->statusToString($request->status);

                if ($walker = Walker::find($request->confirmed_walker)) {
                    $allRequests[$i] = array($request, $walker);
                } else {
                    $allRequests[$i] = array($request, "unavailable");
                }
                $i++;
            }

        } catch (Exception $e) {
            $e->getMessage();
        }
        return json_encode($allRequests);


    }



    //returns active requests given the company ID,
    // to do add order by date.

    public function getActiveRequests()
    {

        try {
            $company_id = Request::get('company_id');
            $requests = DeliveryRequest::
            //order the result descendingly
            orderBy('created_at', 'desc')
                ->where('company_id', '=', $company_id)
                //function of $q the requests statuses are active
                ->where(function ($q) {
                    $q->where('status', 0)//request just created searching for driver.
                    ->orWhere('status', 1)//walker accepted/ on the way.
                    ->orWhere('status', 2)//package picked from sender .*/
                    ->orWhere('status', 3);//package picked from sender .*/
                })->get();


            $i = 0;
            $activeRequests = array();

            foreach ($requests as $request) {

                $request->status = $this->statusToString($request->status);

                if ($walker = Walker::find($request->confirmed_walker)) {
                    $activeRequests[$i] = array($request, $walker);
                } else {
                    $activeRequests[$i] = array($request, "unavailable");
                }
                $i++;
            }

        } catch (Exception $e) {
            $e->getMessage();
        }
        return json_encode($activeRequests);
    }


    //returns delivered requests given the company ID.
    public function getDeliveredRequests()
    {

        try {

            $company_id = Request::get("company_id");
            //in this way only payed requests returned
            $requests = DeliveryRequest::
            orderBy('created_at', 'desc')
                ->where('company_id', "=", $company_id)
                ->where('status', '=', 3)->get();

            $i = 0;
            $deliveredRequests = array();

            foreach ($requests as $request) {

                $request->status = $this->statusToString($request->status);

                $deliveredRequests[$i] = array($request, Walker::find($request->confirmed_walker));
                $i++;
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
        return json_encode($deliveredRequests);

    }


    public function get_supplier_addresses()
    {

        try {
            $company_id = Request::get("company_id");
            $supplieraddresses = SupplierAddress::where('company_id', "=", $company_id)->get();

            $i = 0;
            $addresses = array();

            foreach ($supplieraddresses as $address) {

                $addresses[$i] = $address;
                $i++;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }

        return json_encode($addresses);

    }

    public function get_customer_addresses()
    {

        try {
            $company_id = Request::get("company_id");
            $customeraddresses = CustomerAddress::where('company_id', "=", $company_id)->get();

            $i = 0;
            $addresses = array();

            foreach ($customeraddresses as $address) {

                $addresses[$i] = $address;
                $i++;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }

        return json_encode($addresses);

    }


    public function isSupplier()
    {

        $supplier_mobile = Request::get('supplier_mobile');
        $company_id = Request::get('company_id');

        $supplier_address = SupplierAddress::where('company_id', "=", $company_id)
            ->where('mobile', '=', $supplier_mobile)->first();

        //return the address if it does exist in the address book, otherwise return 0.
        if ($supplier_address) {
            return $supplier_address;
        } else {
            return 0;
        }

    }

    public function isCustomer()
    {

        $customer_mobile = Request::get('customer_mobile');
        $company_id = Request::get('company_id');

        $customer_address = CustomerAddress::where('company_id', "=", $company_id)
            ->where('mobile', '=', $customer_mobile)->first();

        if ($customer_address) {
            return $customer_address;
        } else {
            return 0;
        }

    }


    public function save_supplier_address()
    {

        try {

            $company_id = Request::get('company_id');
            $supplier_name = Request::get('supplier_name');
            $supplier_mobile = Request::get('supplier_mobile');
            $supplier_address = Request::get('supplier_address');
            $supplier_latitude = Request::get('supplier_latitude');
            $supplier_longitude = Request::get('supplier_longitude');

            $supplier = new SupplierAddress();

            $supplier->company_id = $company_id;
            $supplier->name = $supplier_name;
            $supplier->mobile = $supplier_mobile;
            $supplier->address = $supplier_address;
            $supplier->latitude = $supplier_latitude;
            $supplier->longitude = $supplier_longitude;


            //save!!
            $supplier->save();

        } catch (Exception $e) {
            return $e->getMessage();
        }

        //1 means its done/saved!!
        return 1;
    }


    public function save_customer_address()
    {

        try {

            $company_id = Request::get('company_id');
            $customer_name = Request::get('customer_name');
            $customer_mobile = Request::get('customer_mobile');
            $customer_address = Request::get('customer_address');
            $customer_latitude = Request::get('customer_latitude');
            $customer_longitude = Request::get('customer_longitude');


            $customer = new CustomerAddress();

            $customer->company_id = $company_id;
            $customer->name = $customer_name;
            $customer->mobile = $customer_mobile;
            $customer->address = $customer_address;
            $customer->latitude = $customer_latitude;
            $customer->longitude = $customer_longitude;

            //save!!
            $customer->save();


        } catch (Exception $e) {
            return $e->getMessage();
        }

        //1 means its done/saved!!
        return 1;


    }


    //Check if email already registered in "company" table.
    public function isUser($email)
    {
        $company = Company::where('email', '=', $email)->first();

        if ($company == null) {
            return false;
        } else {
            return true;
        }
    }

    //Check if username already registered in "companies" table.
    public function isUsername($username)
    {
        $company = Companies::where('username', '=', $username)->first();

        if ($company == null) {
            return false;
        } else {
            return true;
        }
    }


    public function customerservice()
    {
        $company_id = Session::get('company_id');
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->distinct()->get();
        $company = Request::get('company');
        $city = Request::get('city');


        $companydata = Companies::find($company_id);

        if (!isset($company)) {
            $company = 'all';
        }
        if (!isset($city)) {
            $city = 'all';
        }
        $statusDef = array(
            "-1" => 'Created By Supplier',
            "0" => 'Created By Supplier',
            "1" => 'In Route',
            "2" => 'In Warehouse',
            "3" => 'In Warehouse',
            "4" => 'With Captains',
            "5" => 'Delivered',
            "6" => 'UnDelivered',
            "7" => 'Returned to Supplier'
        );
        $undeliveredordersQuery = DeliveryRequest::where('num_faild_delivery_attempts', '>', '0')->
        where('status', '!=', '7')->
        where('status', '!=', '6')->
        where('status', '!=', '-1')->
        where('status', '!=', '0')->
        where('status', '!=', '2')->
        where('status', '!=', '4')->
        where('status', '!=', '5')->select(
            ['jollychic',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_district',
                'd_city',
                'cash_on_delivery',
                'scheduled_shipment_date',
                'undelivered_reason']);

        $returnedordersQuery = DeliveryRequest::where('status', '=', '7')->select(
            ['jollychic',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'd_address',
                'd_district',
                'd_city',
                'cash_on_delivery',
                'undelivered_reason']);

        $cashtobecollectedQuery = DeliveryRequest::where('num_faild_delivery_attempts', '>', '0')->
        where('status', '!=', '7')->
        where('status', '!=', '6')->
        where('status', '!=', '-1')->
        where('status', '!=', '0')->
        where('status', '!=', '2')->
        where('status', '!=', '4')->
        where('status', '!=', '5');
        if ($company != 'all') {
            $undeliveredordersQuery = $undeliveredordersQuery->where('company_id', '=', $company);
            $returnedordersQuery = $returnedordersQuery->where('company_id', '=', $company);
            $cashtobecollectedQuery = $cashtobecollectedQuery->where('company_id', '=', $company);
        }
        if ($city != 'all') {
            $undeliveredordersQuery = $undeliveredordersQuery->where('d_city', 'like', $city);
            $returnedordersQuery = $returnedordersQuery->where('d_city', 'like', $city);
            $cashtobecollectedQuery = $cashtobecollectedQuery->where('d_city', 'like', $city);
        }

        $undeliveredorders = $undeliveredordersQuery->get();
        $undelivreditems = count($undeliveredorders);

        $returnedorders = $undeliveredordersQuery->get();
        $returnedtojcitems = count($returnedorders);


        $cashtobecollected = '0.0';
        $cashtobecollected = $cashtobecollectedQuery->sum('cash_on_delivery');


        foreach ($undeliveredorders as $order) {
            $order->d_address = addslashes($order->d_address);
            $order->receiver_name = addslashes($order->receiver_name);
            $order->undelivered_reason = UndeliveredReason::where('id', '=', $order->undelivered_reason)->get()->first();
            $order->undelivered_reason = $order->undelivered_reason->english;

            if ($order->scheduled_shipment_date == NULL) {
                $order->scheduled_shipment_date = 'NotSet';
            }

        }

        foreach ($returnedorders as $order) {
            $order->d_address = addslashes($order->d_address);
            $order->receiver_name = addslashes($order->receiver_name);
            $order->undelivered_reason = UndeliveredReason::where('id', '=', $order->undelivered_reason)->get()->first();
            $order->undelivered_reason = $order->undelivered_reason->english;

        }

        $companies = Companies::all(array('id', 'company_name as name'));
        return View::make('companies.customerService')
            ->with('undeliveredorders', $undeliveredorders)
            ->with('returnedorders', $returnedorders)
            ->with('companydata', $companydata)
            ->with('undelivreditems', $undelivreditems)
            ->with('returnedtojcitems', $returnedtojcitems)
            ->with('cashtobecollected', $cashtobecollected)
            ->with('companies', $companies)
            ->with('adminCities', $adminCities)
            ->with('company', $company)
            ->with('city', $city);

    }

    //Tracking
    public function admintracking()
    {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        if (Request::has('trackingnum')) {
            try {
                $waybillnumber = Request::get('trackingnum');
                $finalstausnum = 0;
                // $order_all = OrdersHistory::where('waybill_number', '=', $waybillnumber)->orderBy('id', 'asc')->get();;
                if (preg_match('/^JC[0-9]{8}KS/', $waybillnumber)) {
                    $waybillnumber = Request::get('trackingnum');
                }
                elseif (preg_match('/^OS[0-9]{8}KS/',$waybillnumber)) {
                    $waybillnumber = Request::get('trackingnum');
                }
                else{
                     //if its order number get the jollychic first by order number
                     $newwaybill = DeliveryRequest::select('jollychic')->where('order_number', '=', $waybillnumber)->get();
                 
                     $waybillnumber = $newwaybill[0]->jollychic;
                }
                $order_all = OrdersHistory::select('orders_history.created_at', 'orders_history.status as statusnum',
                    'companies.company_name','delivery_request.sender_name','delivery_request.receiver_name', 'delivery_request.d_city', 'delivery_request.order_number', 'walker.phone', 'walker.first_name', 'walker.last_name', 'english as statustxt', 'notes')
                    ->leftJoin('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->leftJoin('walker', 'walker.id', '=', 'orders_history.walker_id')
                    ->leftJoin('delivery_request', 'delivery_request.jollychic', '=', 'orders_history.waybill_number')
		    ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();


                if (count($order_all)) {
                    $finalstausnum = $order_all[0]->statusnum;
                    $finalstaustxt = $order_all[0]->statustxt;

                } else {
                    $finalstausnum = 0;
                    $waybillnumber = 'Not Found!';
                    $finalstaustxt = ' ';
                    $order_number = '';
                    $order_all = array();
                }
                foreach ($order_all as $order) {

                    if ($order->statusnum) {
                        $order->city = $order->d_city;
                    } else {
                        $order->city = 'Riyadh';
                    }

                }
                if ($finalstausnum == 0) {
                    $finalstausnum = 5;
                } else if ($finalstausnum == 1) {
                    $finalstausnum = 25;
                } else if ($finalstausnum == 2 || $finalstausnum == 3) {
                    $finalstausnum = 50;
                } else if ($finalstausnum == 4 || $finalstausnum == 6) {
                    $finalstausnum = 75;
                } else {
                    $finalstausnum = 100;
                }


            } catch (Exception $e) {

                return $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            }


        } else {

            $finalstausnum = 0;
            $waybillnumber = 'Enter a tracking number';
            $finalstaustxt = ' ';
            $order_number = '';
            $order_all = array();

        }
        return View::make('companies.tracking')
            ->with('waybill', $waybillnumber)
            ->with('finalstausnum', $finalstausnum)
            ->with('finalstaustxt', $finalstaustxt)
            ->with('history', $order_all)
            ->with('companydata',$companydata);
    }

    //
    public function admintrackingbulk()
    {
        $count = 0;

        $searchcolumn = 'waybill';
        $items = array();
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        try {
            if (Request::get('ids')) {

                $ids = explode(',', Request::get('ids'));

                // var_dump($ids);
                //die();

                $i = 0;


                foreach ($ids as $id) {
                    //$anitem = DeliveryRequest::where($searchcolumn, '=',$id )->get()->first();

                     if (preg_match('/^JC[0-9]{8}KS/', $ids[$i])) {
                    $searchcolumn = 'jollychic';
                }
                elseif (preg_match('/^OS[0-9]{8}KS/', $ids[$i])) {
                    $searchcolumn = 'waybill';
                }

                else{

                    $searchcolumn = 'order_number';
                }


                    if($searchcolumn == 'order_number'){

                    $newwaybill = DeliveryRequest::select('jollychic')->where('order_number', '=', $id)->get();
                 
                     $newid = $newwaybill[0]->jollychic;

                 }else{

                    $newid = $id;
                 }


                    $lastUpdate = OrdersHistory::select('updated_at')->where('waybill_number', '=', $newid)->get()->first();

                    $lastUpdate = $lastUpdate->updated_at;

                    $anitem = DeliveryRequest::select('delivery_request.jollychic as waybill', 'delivery_request.num_faild_delivery_attempts','delivery_request.undelivered_reason', 'undelivered_reasons.reason_code', 'delivery_request.d_city as d_city', 'delivery_request.status as statusnum', 'order_statuses.english as statustxt',
                        'companies.company_name','delivery_request.sender_name','delivery_request.receiver_name', 'delivery_request.order_number', 'walker.phone', 'walker.first_name', 'walker.last_name')
                        ->leftJoin('order_statuses', 'order_statuses.id', '=', 'delivery_request.status')
                        ->leftJoin('walker', 'walker.id', '=', 'delivery_request.confirmed_walker')
			->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                        ->leftJoin('undelivered_reasons', 'undelivered_reasons.id', '=', 'delivery_request.undelivered_reason')
                        ->where($searchcolumn, '=', $id)->where('company_id','=',$company_id)->get()->first();

                   
                    if (count($anitem)) {

                        $confirmed_walker = $anitem->first_name . ' ' . $anitem->last_name;
                        $confirmed_walker .= " 0";
                        $confirmed_walker .= substr(str_replace(' ', '', $anitem->phone), -9);
                        array_push($items, array(
                            'waybill' => $anitem->waybill,
			    'company_name' => !empty($anitem->sender_name) ? $anitem->company_name . ' [' . $anitem->sender_name . ']' : $anitem->company_name,
                            'd_city' => $anitem->d_city,
                            'captain' => $confirmed_walker,
                            'customer_name' => $anitem->receiver_name,
                            'order_number' => $anitem->order_number,
                            'current_status' => $anitem->statustxt,
                            'num_faild_delivery_attempts' => $anitem->num_faild_delivery_attempts,
                            'undelivered_reason' => $anitem->undelivered_reason,
                            'reason_code' => $anitem->reason_code,
                            'lastUpdate' => $lastUpdate
                        ));

                    } else {

                        array_push($items, array(
                            'waybill' => $id,
                            'd_city' => '',
                            'captain' => '',
                            'customer_name' => '',
                            'order_number' => '',
                            'current_status' => 'Not Found',
                            'num_faild_delivery_attempts' => '',
                            'undelivered_reason' => '',
			    'company_name' => '',
                            'reason_code' => '',
                            'lastUpdate' => ''
                        ));
                    }
                    $i++;
                }

            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        return View::make('companies.trackingbulk')
            ->with('items', $items)
            ->with('companydata',$companydata);
    }


    public function generateshipment()
    {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $adminCities = City::where('name','<>','All')->select('name as city')->orderBy('name', 'asc')->get();
        $merchants = Companies::Where('parent_id', '=', $company_id)->get();
        return View::make('companies.insertrecord')->with('companydata', $companydata)->with('adminCities', $adminCities)->with('merchants', $merchants);
    }
    public function getdistrictsbycity()
    {
        $city = Request::get('city');
        $districts = Districts::where('city','=',$city)->select(['district'])->get();
        $response_array = array('success' => true, 'city' => $city, 'districts' => $districts);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function generateshipmentpost()
    {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $adminCities = City::where('name','<>','All')->select('name as city')->orderBy('name', 'asc')->get();
        try {
            $company_id = Session::get('company_id');
            $companydata = Companies::find($company_id);

            if(Request::has('branch')) {
                $company_id = Request::get('branch');
            }

            if(!$companydata->allow_creation) {
                $merchants = Companies::where('parent_id', '=', Session::get('company_id'))->get();
                return View::make('companies.insertrecord')->with('companydata', $companydata)->with('adminCities', $adminCities)
                    ->with('error', 'You are not allowed to create orders. Please contact admin.')->with('merchants', $merchants);
            }

            if (Request::has('order_number')) {
                $order_number = Request::get('order_number');
                if (count(DeliveryRequest::where('order_number', '=', $order_number)->where('company_id', '=', $company_id)->get())) {
                    $merchants = Companies::where('parent_id', '=', Session::get('company_id'))->get();
                    return View::make('companies.insertrecord')->with('companydata', $companydata)->with('adminCities', $adminCities)
                        ->with('error', 'order number ' . $order_number . ' already exists, Please try again.')->with('merchants', $merchants);
                }
            } 
            else
                $order_number = '';
            $cash_on_delivery = Request::get('cashondelivery');
            $dropoff_name = Request::get('name');
            $dropoff_name = trim(preg_replace('/\s+/',' ', $dropoff_name));
            $dropoff_mobile = Request::get('mobile');
            $dropoff_mobile2 = Request::get('mobile2');
            $dropoff_email = "";
            if (Request::has('email')) {
                $dropoff_email = str_replace(PHP_EOL,' ',Request::get('email'));
            }

	    $dropoff_mobile = format_phone($dropoff_mobile);
            $dropoff_mobile2 = format_phone($dropoff_mobile2);
            /*$mobilelength = strlen((string)$dropoff_mobile);
            $mobile2length = strlen((string)$dropoff_mobile2);

            if($mobilelength <= 11){
            $dropoff_mobile = preg_replace('/^0/','966',$dropoff_mobile);
            }

            if($mobile2length <= 11){
            $dropoff_mobile2 = preg_replace('/^0/','966',$dropoff_mobile2);
            }*/

            $d_address = str_replace(PHP_EOL,' ',Request::get('streetaddress'));

            $d_address = trim(preg_replace('/\s+/',' ', $d_address));
            $d_address2 = '';
            if(Request::has('streetaddress2')){
                $d_address2 = str_replace(PHP_EOL,'',Request::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/',' ', $d_address2));
            }
            $d_address3 = '';
            if(Request::has('streetaddress3')){
                $d_address3 = Request::get('streetaddress3');
            }
            $d_city = Request::get('city');
            $d_district = Request::get('district');

            if (strpos($d_district, '+') !== false) {

                $d_city = strtok($d_district, '+');

                $d_district = strtok( '' ); 
            }

            $d_state = '';

            if($d_city == 'Makkah' || $d_city == 'Jeddah' || $d_city =='Taif'){

                $d_state = 'Makkah{Mecca}';
            }

            if($d_city =='Baha'){

                $d_state = 'Al Baha';
            }

            if($d_city == 'Riyadh'){
                $d_state = 'Al-Riyadh';
            }

            if($d_city =='Dammam'){

                $d_state = 'Eastern Region {Ash-Sharqiyah}';
            }

            $d_zipcode = '';
            if(Request::has('zipcode')){
                $d_zipcode = Request::get('zipcode');
            }
            $d_weight = Request::get('weight');
            $d_quantity = Request::get('quantity');
            $d_description = '';
            if(Request::has('description')){
                $d_description = Request::get('description');
            }
            //   latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Request::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            //  longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Request::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }

            $delivery_request = new DeliveryRequest;
            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            $delivery_request->cash_on_delivery = $cash_on_delivery;
            $delivery_request->receiver_name = $dropoff_name;
            $delivery_request->receiver_phone = $dropoff_mobile;
            $delivery_request->receiver_phone2 = $dropoff_mobile2;
            $delivery_request->receiver_email = $dropoff_email;
            $delivery_request->d_address = $d_address;
            $delivery_request->d_address2 = $d_address2;
            //$delivery_request->d_address3 = $d_address3;
            $delivery_request->d_district = $d_district;
            $delivery_request->d_city = $d_city;
            $delivery_request->d_state = $d_state;
            $delivery_request->d_zipcode = $d_zipcode;
            $delivery_request->d_weight = $d_weight;
            $delivery_request->d_quantity = $d_quantity;
            $delivery_request->d_description = $d_description;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            $delivery_request->order_number = $order_number;
            $delivery_request->company_id = $company_id;

            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->origin_latitude = 21.575723;
            $delivery_request->origin_longitude = 39.148976;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->pincode2 = mt_rand(1000, 9999);
            $delivery_request->status = 0;

            $delivery_request->save();


            $dr = DeliveryRequest::find($delivery_request->id);
            $waybill = 'OS' . str_pad($delivery_request->id, 8, "0", STR_PAD_LEFT).'KS';
            $dr->jollychic = $waybill;
            $dr->waybill = $waybill;
            $dr->customer_tracking_number = $dropoff_mobile . '-' . $dr->id;
            $dr->save();

            $OrdersHistory = new OrdersHistory;

            $OrdersHistory->waybill_number = $waybill;
            $OrdersHistory->status = 0;

            $OrdersHistory->company_id = $company_id;

            $OrdersHistory->save();

            // $curl = new Curl();
            // $sms = 'عزيزي/زتي ';
            // $sms .= $dropoff_name;
            // $sms .= "،";
            // $sms .= "طلبكم من ";
            // $company = Companies::find($dr->company_id);
            // if (isset($company)) {
            //     $sms .= " " . $company->name_ar . " ";
            // }

            // $sms .= " سيخرج قريبًا إلى ";
            // $sms .= $d_city;
            // $sms .= " وسيتواصل معكم مندوب التوزيع في حيكم في أقرب ";
            // $sms .= "وقت مع تحيات ساعي";

            // $destinations1 = "966";
            // $destinations1 .= substr(str_replace(' ', '', $dropoff_mobile), -9);

            // $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            // $urlDiv = explode("?", $url);
            // $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            // Log::info("SMSSERVICELOG" . $result);

            // if (Request::has('mobile2') && $dropoff_mobile != $dropoff_mobile2) {
            //     $destinations2 = "966";
            //     $destinations2 .= substr(str_replace(' ', '', $dropoff_mobile2), -9);
            //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

            //     $urlDiv = explode("?", $url);
            //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            //     Log::info("SMSSERVICELOG" . $result);
            // }


        } catch (Exception $e) {
            Log::info($e->getMessage());
            $merchants = Companies::where('parent_id', '=', Session::get('company_id'))->get();
            return View::make('companies.insertrecord')->with('companydata', $companydata)->with('adminCities', $adminCities)->with('error','Failed, Please try again.')->with('merchants', $merchants);
        }

        $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="55" height="69">';
        $destination = 'JED';
        /////////////////
        $nonGroupedCities = City::where('grouped', '=', '0')->get();
        $groupedCities = City::where('grouped', '=', '1')->get();
        $flag = false;
        $package = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        foreach($nonGroupedCities as $city) {
            if($city->name == $package->d_city) {
                $cityimg = "<img src='/companiesdashboard/images/$city->img_url' width='55' height='69'>";
                $destination = $city->c_code;
                $flag = true;
                break;
            }
        }
        foreach($groupedCities as $curcity) {
            if($flag)
                break;
            $city = $curcity->name;
            $query = DB::select("select jollychic from delivery_request where jollychic = '$waybill' and d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) as 'a' from districts where city = '$city') limit 1");
            if(count($query) == 0)
                continue;
            $cityimg = "<img src='/companiesdashboard/images/$curcity->img_url' width='55' height='69'>";
            $destination = $curcity->c_code;
            break;
        }
        ////////////////

        $data = array(
            'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($dr->waybill, "C39+", 1, 33) . '" alt="barcode"   style=\'padding:1px;\'/>',
            'barcode90' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($dr->waybill, "C39+", 1, 33) . '" alt="barcode" style=\'transform-origin:6% 90%;transform: rotate(90deg);\'/>',
            'city' => $dr->d_city,
            'cityimg' => $cityimg,
            'cod' => round($dr->cash_on_delivery,2),
            'pieces' => $dr->d_quantity,
            'weight' => $dr->d_weight,
            'company_name' => $companydata->company_name,  
            'company_address' => $companydata->address,
            'company_city' => $companydata->city,
            'company_country' => $companydata->country,
            'company_phone' => $companydata->phone,
            'customer_name' => $dr->receiver_name ,
            'customer_address' => $dr->d_address,
            'customer_city' => $dr->d_city,
            'customer_district' => $dr->d_district,
            'customer_phone' => $dr->receiver_phone,
            'customer_phone2' => isset($dr->receiver_phone2)?$dr->receiver_phone2:"",
            'waybill' => $dr->waybill,
            'description' => $dr->d_description,
            'destination'=>$destination,
            'order_number'=>$order_number,
            'date' => date("Y-m-d")
        );
        $view = View::make('companies.sticker', $data);
        $contents = $view->render();
        $merchants = Companies::where('parent_id', '=', Session::get('company_id'))->get();
        return View::make('companies.insertrecord')->with('sticker',addslashes(str_replace(PHP_EOL,'',$contents)))->with('companydata', $companydata)->with('adminCities', $adminCities)->with('waybill',$waybill)->with('merchants', $merchants);
    }

    public function test() {
        
    }

    // public function one order only
    public function sticker()
    {
        $packageID = Request::segment(3);
        $zero_cod = Request::get('zero_cod');
        $package = DeliveryRequest::where('jollychic', '=', $packageID)->first();
        $main_city = Districts::select('city')->distinct()->from('districts')->where('district', 'like', $package->d_city.'%')->first();
        $companydata = Companies::find($package->company_id);

        $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="55" height="69">';
        $destination = 'JED';
        
        /////////////////
        $nonGroupedCities = City::where('grouped', '=', '0')->get();
        $groupedCities = City::where('grouped', '=', '1')->get();
        $flag = false;
        foreach($nonGroupedCities as $city) {
            if($city->name == $package->d_city) {
                $cityimg = "<img src='/companiesdashboard/images/$city->img_url' width='55' height='69'>";
                $destination = $city->c_code;
                $flag = true;
                break;
            }
        }
        foreach($groupedCities as $curcity) {
            if($flag)
                break;
            $city = $curcity->name;
            $query = DB::select("select jollychic from delivery_request where jollychic = '$packageID' and d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) as 'a' from districts where city = '$city') limit 1");
            if(count($query) == 0)
                continue;
            $cityimg = "<img src='/companiesdashboard/images/$curcity->img_url' width='55' height='69'>";
            $destination = $curcity->c_code;
            break;
        }
        ////////////////

        if(empty($package->order_number))
            $barcode90='';
        else
            $barcode90='<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->order_number, "C128", 1, 33) . '" alt="barcode" style=\'width: 6.4cm;height: 1.3cm;padding:1px;\'/>';
        $data = array(
            'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->jollychic, "C128", 1, 33) . '" alt="barcode"   style=\'width: 6.4cm;height: 1.3cm;padding:1px;\'/>',
            'barcode90' => $barcode90,
            'city' => $package->d_city,
            'cityimg' => $cityimg,
            'cod' => isset($zero_cod) && $zero_cod == 1 ? '0' : round($package->cash_on_delivery,2),
            'pieces' => $package->d_quantity,
            'weight' => $package->d_weight,
            'company_name' => $companydata->company_name,
            'company_address' => $companydata->address,
            'company_city' => $companydata->city,
            'company_country' => $companydata->country,
            'company_phone' => $companydata->phone,
            'customer_name' => $package->receiver_name ,
            'customer_address' => $package->d_address,
            'customer_address2' => $package->d_address2,
            'customer_city' => $package->d_city,
            'customer_district' => $package->d_district,
            'customer_phone' => $package->receiver_phone,
            'customer_phone2' => isset($package->receiver_phone2)?$package->receiver_phone2:"",
            'waybill' => $package->jollychic,
            'description' => $package->d_description,
            'destination'=>$destination,
            'order_number'=>$package->order_number,
            'main_city' =>$main_city,
            'date' => date("Y-m-d")
        );
        return View::make('companies.stickerpdf', $data);
        /*$view = View::make('companies.sticker', $data);
        $contents = $view->render();
        $pdf = App::make( 'dompdf' );
        $pdf->loadHTML( $contents );
        return $pdf->stream();*/
        //$pdf = PDF::loadView('companies.sticker', $data);
        //return $pdf->download($packageID.'.pdf');
    }

    public function stickers() {
        
        try {
            
            $waybills = Request::get('waybills');
            $zero_cod = Request::get('zero_cod');
            $packages = DeliveryRequest::whereIn('jollychic', $waybills)->get();
            $results = array();
            foreach($packages as $package) {
                $main_city = Districts::select('city')->distinct()->from('districts')->where('district', 'like', $package->d_city.'%')->first();
                $companydata = Companies::find($package->company_id);
    
                $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="55" height="69">';
                $destination = 'JED';
                /////////////////
                $nonGroupedCities = City::where('grouped', '=', '0')->get();
                $groupedCities = City::where('grouped', '=', '1')->get();
                $flag = false;
                foreach($nonGroupedCities as $city) {
                    if($city->name == $package->d_city) {
                        $cityimg = "<img src='/companiesdashboard/images/$city->img_url' width='55' height='69'>";
                        $destination = $city->c_code;
                        $flag = true;
                        break;
                    }
                }
                foreach($groupedCities as $curcity) {
                    if($flag)
                        break;
                    $city = $curcity->name;
                    $query = DB::select("select jollychic from delivery_request where jollychic = '$package->jollychic' and d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) as 'a' from districts where city = '$city') limit 1");
                    if(count($query) == 0)
                        continue;
                    $cityimg = "<img src='/companiesdashboard/images/$curcity->img_url' width='55' height='69'>";
                    $destination = $curcity->c_code;
                    break;
                }
                ////////////////
    
                if(empty($package->order_number))
                    $barcode90='';
                else
                    $barcode90='<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->order_number, "C128", 1, 33) . '" alt="barcode" style=\'width: 6.4cm;height: 1.3cm;padding:1px;\'/>';
                $data = (object) array(
                    'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->jollychic, "C128", 1, 33) . '" alt="barcode"   style=\'width: 6.4cm;height: 1.3cm;padding:1px;\'/>',
                    'barcode90' => $barcode90,
                    'city' => $package->d_city,
                    'cityimg' => $cityimg,
                    'cod' => isset($zero_cod) && $zero_cod == 1 ? '0' : round($package->cash_on_delivery,2),
                    'pieces' => $package->d_quantity,
                    'weight' => $package->d_weight,
                    'company_name' => $companydata->company_name,
                    'company_address' => $companydata->address,
                    'company_city' => $companydata->city,
                    'company_country' => $companydata->country,
                    'company_phone' => $companydata->phone,
                    'customer_name' => $package->receiver_name ,
                    'customer_address' => $package->d_address,
                    'customer_address2' => $package->d_address2,
                    'customer_city' => $package->d_city,
                    'customer_district' => $package->d_district,
                    'customer_phone' => $package->receiver_phone,
                    'customer_phone2' => isset($package->receiver_phone2)?$package->receiver_phone2:"",
                    'waybill' => $package->jollychic,
                    'description' => $package->d_description,
                    'destination'=>$destination,
                    'order_number'=>$package->order_number,
                    'main_city' =>$main_city,
                    'date' => date("Y-m-d")
                );
                array_push($results, $data);
            }
            return View::make('companies.stickerspdf')
                ->with('shipments', $results);

        } catch (Exception $e) {

            return View::make('error')->with('log', $e->getMessage())->with('line', $e->getLine());
            
        }
        
    }

    public function deleteRecord(){

        $id = Request::segment(3);
        
        $data = DeliveryRequest::where('jollychic', '=', $id)->get()->first();

        if($data->status==0){

            DeliveryRequest::where('jollychic', '=', $id)->where('status', '=', '0')->delete();
            
            return Redirect::to('/company/shipmentsExport')->with('success','Deleted Successfully !');
        }

    return Redirect::to('/company/shipmentsExport')->with('success','Sorry the shipment is not on its creation stage');
    }
    public function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPGET, 1);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($curl, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 2);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);


        if (curl_errno($curl)) {
            return 'Curl error: ' . curl_error($curl);
        }

        curl_close($curl);

        return $result;
    }

    public function subcompanies() {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $subcompanies = Companies::where('parent_id', '=', $company_id)->get();
        return View::make('companies.subcompanies')
            ->with('companydata', $companydata)
            ->with('subcompanies', $subcompanies);
    }

    public function addsubcompany() {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $states = States::where('active', '=', '1')->get();
        return View::make('companies.addsubcompanies')
            ->with('companydata', $companydata)
            ->with('states', $states);
    }

    public function subcompaniesaddpost() {

        $company_id = Session::get('company_id');

        $username = Request::get('username');
        $password = Request::get('password');
        $company_name = Request::get('company_name');
        $name_ar = Request::get('name_ar');
        $email = Request::get('email');
        $phone = Request::get('phone');
        $city = Request::get('city');
        $district = Request::get('district');
        $state = Request::get('state');
        $address = Request::get('address');
        $owner_name = Request::get('owner_name');
        $owner_id  = Request::get('owner_id');
        $company_cr = Request::get('company_cr');
        $zipcode = Request::get('zipcode');
        $customer_support_phone = Request::get('customer_support_phone');
        $customer_support_email = Request::get('customer_support_email');
        $bank_name = Request::get('bank_name');
        $iban = Request::get('iban');
        $latitude = Request::get('latitude');
        $longitude = Request::get('longitude');

        $main_city = Districts::where('city', '=', $city)->orWhereRaw('SUBSTRING_INDEX(district, "+", 1) = "' . $city . '"')->select(['city'])->first();
        $state = City::wherE('name', '=', $main_city->city)->select(['state'])->first();

        $company = new Companies;
        $company->parent_id = $company_id;
        $company->username = $username;
        $company->password = Hash::make($password);
        $company->company_name = $company_name;
        $company->name_ar = $name_ar;
        $company->email = $email;
        $company->phone = $phone;
        $company->city = $city;
        $company->district = $district;
        $company->state = $state;
        $company->address = $address;
        $company->owner_name = $owner_name;
        $company->owner_id = $owner_id;
        $company->company_cr = $company_cr;
        $company->zipcode = $zipcode;
        $company->customer_support_phone = $customer_support_phone;
        $company->customer_support_email = $customer_support_email;
        $company->bank_name = $bank_name;
        $company->iban = $iban;
        $company->latitude = $latitude;
        $company->longitude = $longitude;

        $company->country = 'Saudi Arabia';
        $company->state = $state->state;
        $company->company_type = 2; // subcomany
        $company->transportation_fee = 25;
        $company->pickup_fee = 8;
        $company->delivery_fee = 8;
        $company->max_regular_weight = 12;
        $company->excess_weight_fees = 1;
        $company->is_api = 0;

        $company->save();

        $subcompanies = Companies::where('parent_id', '=', $company_id)->get();

        return Redirect::to('/company/subcompanies');
    }

    public function subcompaniesedit() {
        $company_id = Session::get('company_id');
        $subcompany_id = Request::segment(4);
        $companydata = Companies::find($company_id);
        $subcompany = Companies::find($subcompany_id);
        $states = States::where('active', '=', '1')->get();
        return View::make('companies.editsubcompanies')
            ->with('companydata', $companydata)
            ->with('states', $states)
            ->with('subcompany_id', $subcompany)
            ->with('subcompany', $subcompany);
    }

    public function subcompanieseditpost() {
        $company_id = Session::get('company_id');
        $subcompany_id = Request::get('subcompany_id');
        $subcompany = Companies::find($subcompany_id);

        $username = Request::get('username');
        $password = Request::get('password');
        $company_name = Request::get('company_name');
        $name_ar = Request::get('name_ar');
        $email = Request::get('email');
        $phone = Request::get('phone');
        $city = Request::get('city');
        $district = Request::get('district');
        $state = Request::get('state');
        $address = Request::get('address');
        $owner_name = Request::get('owner_name');
        $owner_id  = Request::get('owner_id');
        $company_cr = Request::get('company_cr');
        $zipcode = Request::get('zipcode');
        $customer_support_phone = Request::get('customer_support_phone');
        $customer_support_email = Request::get('customer_support_email');
        $bank_name = Request::get('bank_name');
        $iban = Request::get('iban');
        $latitude = Request::get('latitude');
        $longitude = Request::get('longitude');

        $subcompany->parent_id = $company_id;
        $subcompany->username = $username;
        $subcompany->password = Hash::make($password);
        $subcompany->company_name = $company_name;
        $subcompany->name_ar = $name_ar;
        $subcompany->email = $email;
        $subcompany->phone = $phone;
        $subcompany->city = $city;
        $subcompany->district = $district;
        $subcompany->state = $state;
        $subcompany->address = $address;
        $subcompany->owner_name = $owner_name;
        $subcompany->owner_id = $owner_id;
        $subcompany->company_cr = $company_cr;
        $subcompany->zipcode = $zipcode;
        $subcompany->customer_support_phone = $customer_support_phone;
        $subcompany->customer_support_email = $customer_support_email;
        $subcompany->bank_name = $bank_name;
        $subcompany->iban = $iban;
        $subcompany->latitude = $latitude;
        $subcompany->longitude = $longitude;
        $subcompany->save();

        return Redirect::to('/company/subcompanies');
    }

    public function subcompanyremove() {
        try {
            $subcompany_id = Request::get('id');
            return Companies::where('id', '=', $subcompany_id)->delete();
            if(Companies::where('id', '=', '3')->delete());
                return Response::json(array('success' => true), 200);
            return 'error';
        } catch (Exception $e) {
            return Response::json(array('success' => false, 'error' => 'Failed! Please try again'));
        }
    }

    public function subcompanyshipments() {
        try {
            $subcompany_id = Request::segment(4);

        } catch (Exception $e) {
            return Redirect::to('/company/subcompanies?error=Failed! Please try again.');
        }
    }

    public function branches() {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $branches = Branches::all();
        $cities = City::where('name', '<>', 'All')->get();
        return View::make('companies.branches')
            ->with('companydata', $companydata)
            ->with('branches', $branches)
            ->with('cities', $cities);
    }

    public function branchadd(){
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $cities = City::where('name', '<>', 'All')->get();
        $states = States::where('active', '=', '1')->get();
        return View::make('companies.addbranch')
            ->with('companydata', $companydata)
            ->with('cities', $cities)
            ->with('districts', [])
            ->with('states', $states);
    }

    public function branchaddpost() {

        $company_id = Session::get('company_id');
        $name = Request::get('name');
        $name_ar = Request::get('name_ar');
        $email = Request::get('email');
        $phone1 = Request::get('phone1');
        $phone2 = Request::get('phone2');
        $address = Request::get('address');
        $city = Request::get('city');
        $district = Request::get('district');
        $zip_code = Request::get('zip_code');
        $state = Request::get('state');
        $latitude = Request::get('latitude');
        $longitude = Request::get('longitude');

        $branch = new Branches;
        $branch->company_id = $company_id;
        $branch->name = $name;
        $branch->name_ar = $name_ar;
        $branch->email = $email;
        $branch->phone1 = $phone1;
        $branch->phone2 = $phone2;
        $branch->address = $address;
        $branch->city = $city;
        $branch->district = $district;
        $branch->zip_code = $zip_code;
        $branch->state = $state;
        $branch->latitude = $latitude;
        $branch->longitude = $longitude;
        $branch->save();

        return Redirect::to('../company/branches');
    }

    public function branchedit(){
        $branch_id = Request::segment(3);
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $branch = Branches::find($branch_id);
        $cities = City::where('name', '<>', 'All')->where('state', '=', $branch->state)->get();
        $city = Branches::select('city')->where('id', '=', $branch_id)->first()->city;
        $districts = Districts::where('city','=',$city)->select(['district'])->get();
        $states = States::where('active', '=', '1')->get();
        return View::make('companies.editbranch')
            ->with('companydata', $companydata)
            ->with('branch', $branch)
            ->with('states', $states)
            ->with('cities', $cities)
            ->with('districts', $districts);
    }

    public function brancheditpost() {

        $company_id = Session::get('company_id');
        $id = Request::get('id');
        $name = Request::get('name');
        $name_ar = Request::get('name_ar');
        $email = Request::get('email');
        $phone1 = Request::get('phone1');
        $phone2 = Request::get('phone2');
        $address = Request::get('address');
        $city = Request::get('city');
        $district = Request::get('district');
        $zip_code = Request::get('zip_code');
        $state = Request::get('state');
        $latitude = Request::get('latitude');
        $longitude = Request::get('longitude');

        $branch = Branches::find($id);
        $branch->company_id = $company_id;
        $branch->name = $name;
        $branch->name_ar = $name_ar;
        $branch->email = $email;
        $branch->phone1 = $phone1;
        $branch->phone2 = $phone2;
        $branch->address = $address;
        $branch->city = $city;
        $branch->district = $district;
        $branch->zip_code = $zip_code;
        $branch->state = $state;
        $branch->latitude = $latitude;
        $branch->longitude = $longitude;
        $branch->save();

        return Redirect::to('../company/branches');
    }

    public function branchremove() {
        $company_id = Session::get('company_id');
        $branch_id = Request::get('id');
        Branches::where('id', '=', $branch_id)->delete();
        return Response::json(['status'=>'success']);
    }

    public function getcitiesbystate() {
        $state = Request::get('state');
        $cities = City::where('state','=',$state)->select(['name', 'name_ar'])->get();
        $response_array = array('success' => true, 'state' => $state, 'cities' => $cities);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function addnewpickup(){
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $adminCities = City::where('name','<>','All')->select('name as city')->orderBy('name', 'asc')->get();
        $branches = Branches::where('company_id', '=', $company_id)->get();
        $states = States::where('active', '=', '1')->get();
        return View::make('companies.addnewpickup')
            ->with('companydata', $companydata)
            ->with('adminCities', $adminCities)
            ->with('states', $states)
            ->with('branches', $branches);
    }

    public function addnewpickuppost() {
        $company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);
        $adminCities = City::where('name','<>','All')->select('name as city')->orderBy('name', 'asc')->get();
        $branches = Branches::where('company_id', '=', $company_id)->get();
        $states = States::where('active', '=', '1')->get();
        try {
            $company_id = Session::get('company_id');
            $companydata = Companies::find($company_id);

            if (Request::has('order_number')) {
                $order_number = Request::get('order_number');
                if (count(DeliveryRequest::where('order_number', '=', $order_number)->where('company_id', '=', $company_id)->get())) {
                    return View::make('companies.addnewpickup')->with('companydata', $companydata)->with('adminCities', $adminCities)
                        ->with('error', 'order number ' . $order_number . ' already exists, Please try again.')->with('branches', $branches)->with('states', $states);
                }
            }
            $cash_on_delivery = Request::get('cashondelivery');
            $dropoff_name = Request::get('name');
            $dropoff_name = trim(preg_replace('/\s+/',' ', $dropoff_name));
            $dropoff_mobile = Request::get('mobile');
            $dropoff_mobile2 = Request::get('mobile2');
            $branch_id = Request::get('branch_id');
            $dropoff_email = "";
            if (Request::has('email')) {
                $dropoff_email = str_replace(PHP_EOL,' ',Request::get('email'));
            }

	    $dropoff_mobile = format_phone($dropoff_mobile);
            $dropoff_mobile2 = format_phone($dropoff_mobile2);
            /*$mobilelength = strlen((string)$dropoff_mobile);
            $mobile2length = strlen((string)$dropoff_mobile2);

            if($mobilelength <= 11){
                $dropoff_mobile = preg_replace('/^0/','966',$dropoff_mobile);
            }

            if($mobile2length <= 11){
                $dropoff_mobile2 = preg_replace('/^0/','966',$dropoff_mobile2);
            }*/

            $d_address = str_replace(PHP_EOL,' ',Request::get('streetaddress'));

            $d_address = trim(preg_replace('/\s+/',' ', $d_address));
            $d_address2 = '';
            if(Request::has('streetaddress2')){
                $d_address2 = str_replace(PHP_EOL,'',Request::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/',' ', $d_address2));
            }
            $d_address3 = '';
            if(Request::has('streetaddress3')){
                $d_address3 = Request::get('streetaddress3');
            }
            $d_city = Request::get('city');
            $d_district = Request::get('district');

            if (strpos($d_district, '+') !== false) {

                $d_city = strtok($d_district, '+');

                $d_district = strtok( '' );
            }

            $d_state = '';

            if($d_city == 'Makkah' || $d_city == 'Jeddah' || $d_city =='Taif'){

                $d_state = 'Makkah{Mecca}';
            }

            if($d_city =='Baha'){

                $d_state = 'Al Baha';
            }

            if($d_city == 'Riyadh'){
                $d_state = 'Al-Riyadh';
            }

            if($d_city =='Dammam'){

                $d_state = 'Eastern Region {Ash-Sharqiyah}';
            }

            $d_zipcode = '';
            if(Request::has('zipcode')){
                $d_zipcode = Request::get('zipcode');
            }
            $d_weight = Request::get('weight');
            $d_quantity = Request::get('quantity');
            $d_description = '';
            if(Request::has('description')){
                $d_description = Request::get('description');
            }
            //   latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Request::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            //  longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Request::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }

            $delivery_request = new DeliveryRequest;
            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            $delivery_request->cash_on_delivery = $cash_on_delivery;
            $delivery_request->receiver_name = $dropoff_name;
            $delivery_request->receiver_phone = $dropoff_mobile;
            $delivery_request->receiver_phone2 = $dropoff_mobile2;
            $delivery_request->receiver_email = $dropoff_email;
            $delivery_request->d_address = $d_address;
            $delivery_request->d_address2 = $d_address2;
            //$delivery_request->d_address3 = $d_address3;
            $delivery_request->d_district = $d_district;
            $delivery_request->d_city = $d_city;
            $delivery_request->d_state = $d_state;
            $delivery_request->d_zipcode = $d_zipcode;
            $delivery_request->d_weight = $d_weight;
            $delivery_request->d_quantity = $d_quantity;
            $delivery_request->d_description = $d_description;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            $delivery_request->order_number = $order_number;
            $delivery_request->company_id = $company_id;

            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->origin_latitude = 21.575723;
            $delivery_request->origin_longitude = 39.148976;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->status = 0;

            $delivery_request->branch_id = $branch_id;

            $delivery_request->save();


            $dr = DeliveryRequest::find($delivery_request->id);
            $waybill = 'OS' . str_pad($delivery_request->id, 8, "0", STR_PAD_LEFT).'KS';
            $dr->jollychic = $waybill;
            $dr->waybill = $waybill;
            $dr->customer_tracking_number = $dropoff_mobile . '-' . $dr->id;
            $dr->save();

            $OrdersHistory = new OrdersHistory;

            $OrdersHistory->waybill_number = $waybill;
            $OrdersHistory->status = 0;

            $OrdersHistory->company_id = $company_id;

            $OrdersHistory->save();

            // $curl = new Curl();
            // $sms = 'عزيزي/زتي ';
            // $sms .= $dropoff_name;
            // $sms .= "،";
            // $sms .= "طلبكم من ";
            // $company = Companies::find($dr->company_id);
            // if (isset($company)) {
            //     $sms .= " " . $company->name_ar . " ";
            // }

            // $sms .= " سيخرج قريبًا إلى ";
            // $sms .= $d_city;
            // $sms .= " وسيتواصل معكم مندوب التوزيع في حيكم في أقرب ";
            // $sms .= "وقت مع تحيات ساعي";

            // $destinations1 = "966";
            // $destinations1 .= substr(str_replace(' ', '', $dropoff_mobile), -9);

            // $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            // $urlDiv = explode("?", $url);
            // $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            // Log::info("SMSSERVICELOG" . $result);

            // if (Request::has('mobile2') && $dropoff_mobile != $dropoff_mobile2) {
            //     $destinations2 = "966";
            //     $destinations2 .= substr(str_replace(' ', '', $dropoff_mobile2), -9);
            //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

            //     $urlDiv = explode("?", $url);
            //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            //     Log::info("SMSSERVICELOG" . $result);
            // }


        } catch (Exception $e) {
            Log::info($e->getMessage());
            return View::make('companies.addnewpickup')
                ->with('companydata', $companydata)
                ->with('adminCities', $adminCities)
                ->with('error','Failed, Please try again.')
                ->with('branches', $branches)
                ->with('states', $states);
        }
        $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="110" height="138">';
        $destination = 'JED';
        if ($dr->d_city == 'Makkah'){
            $cityimg = '<img src="/companiesdashboard/images/makkah.png"width="110" height="138">';
            $destination ='MAK';
        }
        else if ($dr->d_city == 'Taif'){
            $cityimg = '<img src="/companiesdashboard/images/taif.png"width="110" height="138">';
            $destination ='TAI';
        }
        $data = array(
            'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($dr->waybill, "C39+", 1, 33) . '" alt="barcode"   style=\'padding:1px;\'/>',
            'barcode90' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($dr->waybill, "C39+", 1, 33) . '" alt="barcode" style=\'transform-origin:6% 90%;transform: rotate(90deg);\'/>',
            'city' => $dr->d_city,
            'cityimg' => $cityimg,
            'cod' => round($dr->cash_on_delivery,2),
            'pieces' => $dr->d_quantity,
            'weight' => $dr->d_weight,
            'company_name' => $companydata->company_name,
            'company_address' => $companydata->address,
            'company_city' => $companydata->city,
            'company_country' => $companydata->country,
            'company_phone' => $companydata->phone,
            'customer_name' => $dr->receiver_name ,
            'customer_address' => $dr->d_address,
            'customer_city' => $dr->d_city,
            'customer_district' => $dr->d_district,
            'customer_phone' => $dr->receiver_phone,
            'customer_phone2' => isset($dr->receiver_phone2)?$dr->receiver_phone2:"",
            'waybill' => $dr->waybill,
            'description' => $dr->d_description,
            'destination'=>$destination,
            'order_number'=>$order_number,
            'date' => date("Y-m-d")
        );
        $view = View::make('companies.sticker', $data);
        $contents = $view->render();
        return View::make('companies.addnewpickup')
            ->with('sticker',addslashes(str_replace(PHP_EOL,'',$contents)))
            ->with('companydata', $companydata)
            ->with('adminCities', $adminCities)
            ->with('waybill',$waybill)
            ->with('branches', $branches)
            ->with('states', $states);

    }

    public function view_ticket() {
        $company = Session::get('company_id');
        $waybill = Request::segment(4);
        $companydata = Companies::find($company);
        $check_waybill = DeliveryRequest::where('jollychic', '=', $waybill)->where('company_id', '=', $companydata->id)->count();
        if(!$check_waybill)
            return Redirect::to('/company/ticket/export?error=1'); // waybill does not exist
        $ticket = Tickets::where('waybill', '=', $waybill)->first();
        $actions = TicketsActions::where('ticket_id', '=', $waybill)->orderBy('created_at', 'desc')->get();
        $all_statuses = TicketsStatuses::all();
        $status_map = array();
        foreach($all_statuses as $record)
            $status_map[$record->id] = $record;
        $ticket_types = TicketType::all();
        $ticket_types_map = array();
        foreach($ticket_types as $one_record) {
            $ticket_types_map[$one_record->id] = $one_record;
        }
        $companies = Companies::all();
        $companies_names = array();
        foreach($companies as $one_record)
            $companies_names[$one_record->id] = $one_record->company_name;
        return View::make('companies.tickets.view')
            ->with('ticket', $ticket)
            ->with('actions', $actions)
            ->with('status_map', $status_map)
            ->with('ticket_types_map', $ticket_types_map)
            ->with('companies_names', $companies_names)
            ->with('companydata', $companydata);
    }

    public function create_ticket() {
        
        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $tabFilter = Request::get('tabFilter');
        if(!isset($tabFilter)) {
            return View::make('companies.tickets.create')
            ->with('companydata', $companydata);
        }
        try {
            $ids = explode(',', Request::get('ids'));
            $type = Request::get('type');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');
            $description = Request::get('description');
            $shipments = DeliveryRequest::leftJoin('order_statuses', 'delivery_request.status', '=', 'order_statuses.id')
            ->where('company_id', '=', $companydata->id)
            ->where(function($query) use($ids) {
                $query->whereIn('jollychic', $ids)
                ->orWhereIn('order_number', $ids)
                ->orWhereIn('order_reference', $ids);
            })
            ->select(['jollychic', 'status', 'order_statuses.english'])
            ->get();
            $success_items = array();
            $failed_items = array();
            $failed_waybills = array();
            foreach($shipments as $one_record) { 
                if($type == 'push_delivery') {
                    if(!in_array($one_record->status, array(1,2,3))) { 
                        array_push($failed_items, array(
                            'waybill' => $one_record->jollychic,
                            'error' => "You can not push delivery for shipment in '$one_record->english' status"
                        )); 
                        array_push($failed_waybills, $one_record->jollychic);
                        continue;
                    }
                    // can not schedule before the limit
                    $limit = date('Y-m-d');
                    $max_time_same_day_scheduling = Settings::where('key', '=', 'max_time_same_day_scheduling')->first()->value;
                    $ampm = $max_time_same_day_scheduling >= 12 ? "PM" : "AM";
                    if(date('H') >= $max_time_same_day_scheduling) {
                        $limit = date('Y-m-d', strtotime('+1 day'));
                        if(date('D', strtotime($limit)) == 'Fri')
                            $limit = date('Y-m-d', strtotime('+2 day'));
                    }
                    if(strtotime($scheduled_shipment_date) < strtotime($limit)) {
                        if($scheduled_shipment_date == date('Y-m-d'))
                            $message = "You can not schedule shipments in the same day after $max_time_same_day_scheduling:00 $ampm";
                        else 
                            $message = "You can not schedule shipments before $limit";
                        array_push($failed_items, array(
                            'waybill' => $one_record->jollychic,
                            'error' => $message
                        )); 
                        array_push($failed_waybills, $one_record->jollychic);
                        continue;
                    }
                    $ticket = new Ticket($one_record->jollychic, 66, $companydata->id);
                    $response = $ticket->open(array(
                        'urgent_delivery' => 1,
                        'scheduled_shipment_date' => $scheduled_shipment_date,
                        'undelivered_reason' => '16'
                    ), 'Push Delivery', 'High', "Created by $companydata->company_name for urgent delivery", '6');
                    if($response['success'])
                        array_push($success_items, $one_record->jollychic);
                    else 
                        array_push($failed_items, array(
                            'waybill' => $one_record->jollychic,
                            'error' => 'Failed due to an error. Please contact admin.',
                            'error_' => $response['error']
                        ));
                } 
                else if($type == 'request_investigation') {
                    $ticket = new Ticket($one_record->jollychic, 66, $companydata->id);
                    $response = $ticket->open(array(), "$description", 'High', "Request Investigation by $companydata->company_name", '7');
                    if($response['success'])
                        array_push($success_items, $one_record->jollychic);
                    else 
                        array_push($failed_items, array(
                            'waybill' => $one_record->jollychic,
                            'error' => 'Failed due to an error. Please contact admin.',
                            'error_' => $response['error']
                        ));
                }
            }
            foreach($ids as $one_id) {
                if(!in_array($one_id, $success_items) && !in_array($one_id, $failed_waybills)) 
                    array_push($failed_items, array(
                        'waybill' => $one_id,
                        'error' => 'Waybill does not exist'
                    ));
            }
            $success_items = DeliveryRequest::whereIn('jollychic', $success_items)
            ->select([
                'jollychic',
                'order_number',
                'order_reference',
                'd_city',
                'receiver_name',
                'receiver_phone',
                'receiver_phone2',
                'cash_on_delivery'
            ])
            ->get();
            $response_array = array(
                'success' => true,
                'success_items' => $success_items,
                'failed_items' => $failed_items
            );
        }
        catch (Exception $e) {
            $response_array = array(
                'success' => false, 
                'error_message' => 'Failed due to an error. Please try again',
                'line' => $e->getLine(),
                'error' => $e->getMessage()
            );
        }
        response:
        return $response_array;
    }

    public function export_ticket() {
        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->get();
        $all_statuses = TicketsStatuses::all();
        $status_map = array();
        foreach($all_statuses as $record)
            $status_map[$record->id] = $record;
        $tabFilter = Request::get('tabFilter');
        if(!isset($tabFilter)) {
            return View::make('companies.tickets.export')
            ->with('bulk_tickets', [])
            ->with('filter_tickets', [])
            ->with('tabFilter', 1)
            ->with('all_statuses', $all_statuses)
            ->with('status_map', $status_map)
            ->with('adminCities', $adminCities)
            ->with('companydata', $companydata);
        }
    }

    public function export_bulk_ticket() {
        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->get();
        $all_statuses = TicketsStatuses::all();
        $status_map = array();
        foreach($all_statuses as $record)
            $status_map[$record->id] = $record;

        $ids = explode(PHP_EOL, Request::get('ids'));
        for($i = 0; $i < count($ids); ++$i)
            $ids[$i] = trim($ids[$i]);

        $tickets = Tickets::leftJoin(DB::raw("(select d1.* from tickets_actions as d1 left join tickets_actions as d2 on d1.ticket_id = d2.ticket_id and d1.created_at < d2.created_at where d2.ticket_id is null) t_join"), 't_join.ticket_id', '=', 'tickets.waybill')
        ->leftJoin('delivery_request', 'delivery_request.jollychic', '=', 'tickets.waybill')
        ->where('delivery_request.company_id', '=', $companydata->id)
        ->whereIn('tickets.waybill', $ids)
        ->select([
            DB::raw("tickets.*"),
            't_join.comment as comment',
        ])
        ->get();
        
        return View::make('companies.tickets.export')
            ->with('bulk_tickets', $tickets)
            ->with('filter_tickets', [])
            ->with('tabFilter', 1)
            ->with('all_statuses', $all_statuses)
            ->with('status_map', $status_map)
            ->with('adminCities', $adminCities)
            ->with('companydata', $companydata);
    }

    public function export_filter_ticket() {
        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->get();
        $all_statuses = TicketsStatuses::all();
        $status_map = array();
        foreach($all_statuses as $record)
            $status_map[$record->id] = $record;

        $city = Request::get('city');
        $status = Request::get('status');
        
        $tickets = Tickets::leftJoin(DB::raw("(select d1.* from tickets_actions as d1 left join tickets_actions as d2 on d1.ticket_id = d2.ticket_id and d1.created_at < d2.created_at where d2.ticket_id is null) t_join"), 't_join.ticket_id', '=', 'tickets.waybill')
        ->leftJoin('delivery_request', 'delivery_request.jollychic', '=', 'tickets.waybill')
        ->leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->where('delivery_request.company_id', '=', $companydata->id)
        ->select([
            DB::raw("tickets.*"),
            't_join.comment as comment',
        ]);
        if($city != 'All')
            $tickets = $tickets->where('subcity.city', '=', $city);
        if($status != 0)
            $tickets = $tickets->where('tickets.status', '=', $status);
        $tickets = $tickets->get();
        return View::make('companies.tickets.export')
            ->with('bulk_tickets', [])
            ->with('filter_tickets', $tickets)
            ->with('tabFilter', 2)
            ->with('city', $city)
            ->with('status', $status)
            ->with('all_statuses', $all_statuses)
            ->with('status_map', $status_map)
            ->with('adminCities', $adminCities)
            ->with('companydata', $companydata);
    }

    public function sameDaySuccessRate() {

        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $adminCities = City::select('name as city')->orderBy('name', 'asc')->get();

        $company = $companydata->id;
        $city = Request::get('city');
        $from = Request::get('from');
        $to = Request::get('to');

        if(!isset($from))
            $from = date('Y-m-d', strtotime('-3 days'));
        if(!isset($to))
            $to = date('Y-m-d');

        $datesQuery = "select distinct date(signin_date) 'date' from delivery_request where date(signin_date) between '$from' and '$to' ";
        if(isset($company) && $company != 'all')
            $datesQuery .= "and company_id = $company ";
        if(isset($city) && $city != 'All')
            $datesQuery .= "and (d_city = '$city' or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')) ";
        $datesQuery .= "order by 1 desc";
        $dates = DB::select($datesQuery);

        $finalResult = array();
        foreach($dates as $date) {
            
            $curDate = $date->date;
            
            $resultQuery = "select jollychic,order_number,english,LastStatusDate,ifnull(comment, '') 'comment' from (
                select jollychic,order_number,english,
                CASE
                   WHEN status = 5 THEN dropoff_timestamp
                   WHEN status = 0 THEN created_at
                   WHEN status = 2 THEN new_shipment_date
                   WHEN status = 3 THEN scheduled_shipment_date
                   ELSE updated_at
                END 'LastStatusDate'
                from delivery_request a,order_statuses b where a.status = b.id ";
            if(isset($company) && $company != 'all')
                $resultQuery .= "and company_id = $company ";
            if(isset($city) && $city != 'All')
                $resultQuery .= "and (d_city = '$city' or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')) ";
            $resultQuery .= "and CASE
                                   WHEN status = 0 THEN date(created_at) = '$curDate'
                                   WHEN status = 2 THEN date(new_shipment_date) = '$curDate'
                                   ELSE date(signin_date) = '$curDate'
                                 END) A LEFT OUTER JOIN (select ticket_id,comment from tickets_actions where id in (
                select id from (
                SELECT ticket_id, max(id) AS id
                FROM tickets_actions GROUP BY ticket_id ) A
                ) ) B on jollychic = ticket_id";
            $result = DB::select($resultQuery);

            $sameDayDispatchedQuery = "select (SameDayDispatched/ReceivedOnDay)*100 'SameDayDispatched' from
                (select count(distinct jollychic) 'SameDayDispatched'
                from delivery_request a,orders_history b where date(new_shipment_date) = '$curDate' ";
            if(isset($company) && $company != 'all')
                $sameDayDispatchedQuery .= "and a.company_id = $company ";
            if(isset($city) && $city != 'All')
                $sameDayDispatchedQuery .= "and (d_city = '$city' or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')) ";
            $sameDayDispatchedQuery .= " and jollychic = waybill_number
                and date(signin_date) = date(b.created_at) and b.status = 4) A,
                (select count(jollychic) 'ReceivedOnDay'
                from delivery_request where date(signin_date) = '$curDate' ";
            if(isset($company) && $company != 'all')
                $sameDayDispatchedQuery .= "and company_id = $company ";
            if(isset($city) && $city != 'All')
                $sameDayDispatchedQuery .= "and (d_city = '$city' or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')) ";
            $sameDayDispatchedQuery .= ") B";
            $sameDayDispatched = DB::select($sameDayDispatchedQuery);

            $sameDaySuccessRateQuery = "select (sum(status = 5 and date(dropoff_timestamp) = '$curDate' and date(signin_date) = '$curDate')/
                sum(status in (1,2,3,4,5,6,7) and date(signin_date) = '$curDate'))*100 'SameDaySuccessRate'
                ,(sum(status = 5 and date(signin_date) = '$curDate')/sum(status in (1,2,3,4,5,6,7) and date(signin_date) = '$curDate'))*100 'CurrentSuccessRate'
                from delivery_request where 1 ";
            if(isset($company) && $company != 'all')
                $sameDaySuccessRateQuery .= "and company_id = $company ";
            if(isset($city) && $city != 'All')
                $sameDaySuccessRateQuery .= "and (d_city = '$city' or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')) ";
            $sameDaySuccessRate = DB::select($sameDaySuccessRateQuery);

            array_push($finalResult, array(
                'date' => $curDate,
                'result' => $result,
                'sameDayDispatched' => $sameDayDispatched[0]->SameDayDispatched,
                'sameDaySuccessRate' => $sameDaySuccessRate[0]->SameDaySuccessRate,
                'CurrentSuccessRate' => $sameDaySuccessRate[0]->CurrentSuccessRate
            ));
        }

        // $finalResult structure
        // array_push($finalResult, array(
        //     'date' => '2018-12-11',
        //     'result' => array(
        //         (object) array(
        //             'jollychic' => '11',
        //             'order_number' => '22',
        //             'english' => '33',
        //             'LastStatusDate' => '44',
        //             'comment' => '55',
        //         ),
        //         (object) array(
        //             'jollychic' => '66',
        //             'order_number' => '77',
        //             'english' => '88',
        //             'LastStatusDate' => '99',
        //             'comment' => '00',
        //         )
        //     ),
        //     'sameDayDispatched' => '3.45',
        //     'sameDaySuccessRate' => '4.56', 
        //     'overallSuccessRate' => '7987.54'
        // ));

        return View::make('companies.sameDaySuccessRateReport')
            ->with('companydata', $companydata)
            ->with('adminCities', $adminCities)
            ->with('company', $company)
            ->with('city', $city)
            ->with('from', $from)
            ->with('to', $to)
            ->with('finalResult', $finalResult);
    }

    public function kpiReport() {

        // if(!in_array('All', $cities)) {
        //     $rto = $rto->where(function($query) use($cities, $cities_string) {
        //         $query->whereIn('d_city', $cities)
        //         ->orWhereRaw("d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city in ('$cities_string'))");
        //     });
        // }

        $company = Session::get('company_id');
        $companydata = Companies::find($company);
        $adminCities = City::select('name as city')->where('name', '<>', 'All')->orderBy('name', 'asc')->get();
        $subcities = Districts::where('district', '<>', 'No District')->selectRaw("distinct SUBSTRING_INDEX(district,'+',1) 'subcity', city")->get();
        $city_map = array(); 
        foreach($subcities as $one_city) 
            $city_map[$one_city->subcity] = $one_city->city;

        $company = $companydata->id;
        $companies = array($company);
        if(in_array($company, array('911', '946', '974', '975')))
            $companies = array('911', '946', '974', '975');
        $submitForm = Request::get('submitForm');
        $cities = Request::get('cities');
        $from = Request::get('from');
        $to = Request::get('to');

        if(!isset($cities)) {
            $cities = [];
        }
        if(!isset($from))
            $from = date('Y-m-01');
        if(!isset($to))
            $to = date('Y-m-d');

        $cities_string = implode("','", $cities); 
        $companies_string = implode(",", $companies);

        $total_returned = $returned_second_attempt = 0;
        $returned_second_attempt_cs = $all_shipments = 0;
        $urgent_delivery = $all_delivered = $request_investigation = 0;
        $passed_return_due_date = 0;
        $total_rescheduled  = (object) array();
        $success_rate = (object) array();
        $rto = (object) array();
        $transit_tier = (object) array();
        $all_shipments_zero_attempts = (object) array();
        $zero_attempts = (object) array();
        $returned_by_reasons = (object) array();
        $total_by_status = (object) array();

        $cod_transfered = "select ifnull(sum(a.atm_transfer+a.cash_transfer),0) 'amount' 
            from captain_account a, delivery_request b 
            where a.waybill = b.jollychic 
            and DATE(b.new_shipment_date) between '$from' and '$to' 
            and b.company_id in ($companies_string) 
            and b.status = 5 
            and a.captain_account_history_id in ( 
                select id from captain_account_history where banks_deposit_id in ( 
                    select id from banks_deposit where paid_to_supplier = 1 
                ) 
            ) 
            ";
        $cod_transfered = DB::select($cod_transfered)[0]->amount;
        $cod_delivered = DeliveryRequest::where('status', '=', '5')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('DATE(delivery_request.new_shipment_date)'), array($from, $to))
        ->sum('cash_on_delivery');

        if(empty($cities)) {
            goto response;
        }
        
        // $all_cod = (object) array();
        // $all_cod = DeliveryRequest::where('company_id', '=', $companydata->id)
        // ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        // ->sum('cash_on_delivery');

        $success_rate = DeliveryRequest::leftJoin('subcity', 'delivery_request.d_city', '=', 'subcity.subcity')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->select([ 
            DB::raw("sum(status = 5) 'delivered'"),
            DB::raw("sum(status = 3)+sum(status = 4)+sum(status = 5)+sum(status = 6)+sum(status = 7) 'dispatched'"),
            DB::raw("CASE WHEN cash_on_delivery > 0 THEN 1 ELSE 0 END AS cod"),
            'subcity.city'
            // DB::raw("ifnull(round(sum(status = 5)/GREATEST(1, sum(status = 3)+sum(status = 4)+sum(status = 5)+sum(status = 6)+sum(status = 7))*100, 2), 0) 'success_rate'")
        ])
        ->groupBy(['subcity.city','cod']);
        $success_rate = $success_rate->get();

        $rto = DeliveryRequest::leftJoin('subcity', 'delivery_request.d_city', '=', 'subcity.subcity')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', 7);
        $rto = $rto->selectRaw("count(*) 'count', subcity.city");
        $rto = $rto->groupBy('subcity.city');
        $rto = $rto->get();

        $transit_tier = DeliveryRequest::leftJoin('subcity', 'delivery_request.d_city', '=', 'subcity.subcity')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '5')
        ->selectRaw("sum(TIMESTAMPDIFF(HOUR, new_shipment_date, dropoff_timestamp)) 'sum', count(*) 'count', subcity.city, tier");
        $transit_tier = $transit_tier->groupBy(['subcity.city', 'tier'])->get();

        $all_shipments_zero_attempts = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('num_faild_delivery_attempts', '=', '0')
        ->groupBy(['subcity.tier'])
        ->selectRaw("count(*) 'count', subcity.tier")
        ->whereNotNull('subcity.subcity')
        ->get();

        $zero_attempts = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '5')
        ->where('num_faild_delivery_attempts', '=', '0')
        ->groupBy(['subcity.tier'])
        ->selectRaw("count(*) 'count', subcity.tier")
        ->get(); 

        $total_returned = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '3')
        ->where('num_faild_delivery_attempts', '>', '0')
        ->count();
        $returned_second_attempt = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '3')
        ->where('num_faild_delivery_attempts', '>', '1')
        ->count();
        $returned_second_attempt_cs = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '3')
        ->where('num_faild_delivery_attempts', '>', '1')
        ->whereRaw('jollychic in (select waybill from tickets)')
        ->count();

        $all_shipments = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->count();
        $all_delivered = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '5')
        ->count();
        $urgent_delivery = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('urgent_delivery', '=', '1')
        ->count();
        $request_investigation = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->whereRaw("jollychic in (select waybill from tickets where type = 7)")
        ->count();

        $returned_by_reasons = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '3')
        ->where('num_faild_delivery_attempts', '>', '0')
        ->selectRaw("count(*) 'count', undelivered_reason")
        ->groupBy(['undelivered_reason'])
        ->get();
        $total_rescheduled = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->where('status', '=', '3')
        ->where('num_faild_delivery_attempts', '>', '0')
        ->where('scheduled_shipment_date', '>=', date('Y-m-d'))
        ->selectRaw("count(*) 'count', undelivered_reason")
        ->groupBy(['undelivered_reason'])
        ->get();
        $total_by_status = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->whereIn('status', array(3,4))
        ->selectRaw("count(*) 'count', status")
        ->groupBy('status')
        ->get();

        $passed_return_due_date = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city')
        ->leftJoin('companies', 'delivery_request.company_id', '=', 'companies.id')
        ->whereIn('company_id', $companies)
        ->whereBetween(DB::raw('date(delivery_request.new_shipment_date)'), array($from, $to))
        ->whereIn('subcity.city', $cities)
        ->whereNotIn('status', array(5,7))
        ->whereRaw("DATEDIFF(now(), new_shipment_date) > companies.age_limit")
        ->count();

        response: 

        return View::make('companies.reports.kpi_report')
            ->with('adminCities', $adminCities)
            ->with('cities', $cities)
            ->with('city_map', $city_map)
            ->with('from', $from)
            ->with('to', $to)
            ->with('cod_transfered', $cod_transfered)
            ->with('cod_delivered', $cod_delivered)
            // ->with('all_cod', $all_cod)
            ->with('success_rate', $success_rate)
            ->with('rto', $rto)
            ->with('transit_tier', $transit_tier)
            ->with('all_shipments_zero_attempts', $all_shipments_zero_attempts)
            ->with('zero_attempts', $zero_attempts)
            ->with('total_returned', $total_returned)
            ->with('returned_second_attempt', $returned_second_attempt)
            ->with('returned_second_attempt_cs', $returned_second_attempt_cs)
            ->with('all_shipments', $all_shipments)
            ->with('all_delivered', $all_delivered)
            ->with('urgent_delivery', $urgent_delivery)
            ->with('request_investigation', $request_investigation)
            ->with('returned_by_reasons', $returned_by_reasons)
            ->with('total_rescheduled', $total_rescheduled)
            ->with('total_by_status', $total_by_status)
            ->with('passed_return_due_date', $passed_return_due_date)
            ->with('companydata', $companydata);
    }

    public function delete_selected(){

        $ids = explode(',', Request::get('ids'));
        $deletedCount = 0;
        $deletedCount = DB::table('delivery_request')->wherein('jollychic', $ids)->where('status','=','0')->delete();
        
        $response_array = array(
            'success' => true,
            'deletedCount' => $deletedCount,
        );
        $response = Response::json($response_array, 200);
        return $response;

}
 public function delete_single_selected(){

        $ids = explode(',', Request::get('ids'));
        $deletedCount = 0;
        $deletedCount = DB::table('delivery_request')->wherein('jollychic', $ids)->where('status','=','0')->delete();
        
        $response_array = array(
            'success' => true,
            'deletedCount' => $deletedCount,
        );
        $response = Response::json($response_array, 200);
        return $response;

}

}