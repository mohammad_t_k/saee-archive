<?php

/**
 * Created by PhpStorm.
 * User: umar
 * Date: 1/12/2017
 * Time: 6:18 PM
 */

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

class PayFortAPIController extends \BaseController
{

    public function isAdmin($token)
    {
        return false;
    }

    public function getOwnerData($owner_id, $token, $is_admin)
    {

        if ($owner_data = Owner::where('token', '=', $token)->where('id', '=', $owner_id)->first()) {
            return $owner_data;
        } elseif ($is_admin) {
            $owner_data = Owner::where('id', '=', $owner_id)->first();
            if (!$owner_data) {
                return false;
            }
            return $owner_data;
        } else {
            return false;
        }
    }

    public function generatePayfortToken()
    {
        $device_id = trim(Input::get('device_id'));
        $token = Input::get('token');
        $owner_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $owner_id,
                'device_id' => $device_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'device_id' => 'required'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($owner_data = $this->getOwnerData($owner_id, $token, $is_admin)) {
                if($owner_data->email!="payfort@kasper-cab.com"&&$owner_data->email!="fahed@kc.com"&&$owner_data->email!="umar@kasper-cab.com"){
                    $response_array = array('success' => false, 'error' => 'Card Payments are not allowed Yet. Please use cash.', 'error_code' => 411);
                    $response_code = 200;
                    $response = \Illuminate\Support\Facades\Response::json($response_array, $response_code);
                    return $response;
                }
                // check for token validity
                if (is_token_active($owner_data->token_expiry) || $is_admin) {//Payfort Token Generation
                    try {
                        //{"response_code":"00040","response_message":"Invalid transaction source","signature":"bd6647f2e1b6fb09311bd01487f90eb134d232cdc257302d07b14d1cd1ce4ac1","merchant_identifier":"fLPmDVux","access_code":"Z6ouzQeUwoNwpTxcqRkz","language":"en","command":"AUTHORIZATION","status":"00"}
                        $requestparameterstring='asdaredcsreaccess_code=Z6ouzQeUwoNwpTxcqRkzdevice_id='.$device_id.'language=enmerchant_identifier=fLPmDVuxservice_command=SDK_TOKENasdaredcsre';
                        //command=AUTHORIZATION
                        //echo 'Parametters '.$requestparameterstring.'<br>';
                        $signature = hash("sha256",$requestparameterstring,false);
                        //echo 'Sginatures '.$signature.'<br>';
                        $client = new Client();
                        $response = $client->post("https://sbpaymentservices.payfort.com/FortAPI/paymentApi",
                            ['json' => [
                                'access_code' => 'Z6ouzQeUwoNwpTxcqRkz',
                                //'command' => 'AUTHORIZATION',
                                'device_id' => $device_id,
                                'language' => 'en',
                                'merchant_identifier' => 'fLPmDVux',
                                'service_command' => 'SDK_TOKEN',
                                'signature' => $signature
                            ]]);
                        $resobjPayfort = $response->getBody(true);
                        //echo $resobjPayfort;
                        //return ;
                        $objPayfort = json_decode($resobjPayfort, true);
                        $response_code=$objPayfort['response_code'];
                        $response_message=$objPayfort['response_message'];
                        $status=$objPayfort['status'];
                        if($response_code=='22000'){
                            $sdk_token=$objPayfort['sdk_token'];
                            $response_array = array('success' => true, 'sdk_token' => $sdk_token,'response_message'=>$response_message,'status'=>$status);
                            $response_code = 200;
                        }else{
                            $response_array = array('success' => false, 'error_code' => $response_code,'response_message'=>$response_message,'status'=>$status);
                            $response_code = 200;
                        }
                        //var_dump($objPayfort);
                    } catch (Exception $e) {
                        Log::info('Payfort Try = ' . $e->getMessage());
                        $response_array = array('success' => false, 'error' => 'Payfort API Call error.Contact Admin', 'error_code' => 411);
                        $response_code = 200;
                    }
                }else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = \Illuminate\Support\Facades\Response::json($response_array, $response_code);
        return $response;


    }

}