<?php

use App\Classes\Curl;

class UpdateStatus {

    // error_code: 
    /*
        1.duplicate shipment  
        2.shipment not found  
        3.update not allowed  
        4.exception
        5.pickup shipment reserved by another captain
    */
    private $waybill;
    private $valid; // 1: valid, 2: duplicate, 3: not found
    private $now_timestamp;
    private $now_date;
    private $now_time;
    private $shipment; // make it array
    private $order_history; 
    private $new_status;
    private $old_status;
    private $captainid;
    private $admin_id;
    private $hub_id;
    private $pickup_time_interval_id;
    private $undelivered_reason;
    private $override_firstwalker;
    private $all_statuses;

    public function __construct($code, $new_status, $captainid = 0, $admin_id = 0, $hub_id = 0, $company_id = -1, $pickup_time_interval_id = 0, $undelivered_reason = 0, $override_firstwalker = 'No') {
        $this->waybill = $code;
        $this->valid = 1; 
        $this->now_timestamp = date('Y-m-d H:i:s');
        $this->now_date = mb_split(' ', $this->now_timestamp)[0];
        $this->now_time = mb_split(' ', $this->now_timestamp)[1];
        $this->shipment = DeliveryRequest::select("*");
        if (preg_match('/^JC[0-9]{8}KS/', $code) || preg_match('/^OS[0-9]{8}KS/', $code))
            $searchcolumn = 'jollychic';
        else 
            $searchcolumn = 'order_number';
        $this->shipment = $this->shipment->where($searchcolumn, '=', $code);
        // if($company_id == -1)
        //     $this->shipment = $this->shipment->where('jollychic', '=', $code);
        // else
        //     $this->shipment = $this->shipment->whereRaw("company_id = $company and (order_number = '$code' or order_reference = '$code')");
        $count = $this->shipment->count();
        if($count > 1)
            $this->valid = 2; // ADD COMPANY FILTER TO ALL PAGES
        else if($count < 1)
            $this->valid = 3; 
        $this->shipment = $this->shipment->first();
        $this->order_history = new OrdersHistory;
        $this->new_status = $new_status;
        $this->captainid = $captainid;
        $this->admin_id = $admin_id;
        $this->hub_id = $hub_id;
        $this->pickup_time_interval_id = $pickup_time_interval_id;
        $this->undelivered_reason = $undelivered_reason;
        $this->override_firstwalker = $override_firstwalker;
        $all_statuses = OrderStatuses::all();
        $this->all_statuses = array();
        foreach($all_statuses as $one_status)
            $this->all_statuses[$one_status->id] = $one_status->english;
    }

    public function execute() {
        if($this->valid == 2) { 
            $response = array('success' => false, 'waybill' => $this->waybill, 'error_code' => '1', 'error' => 'Duplicate shipments');
            goto response;
        }
        if($this->valid == 3) { 
            $response = array('success' => false, 'waybill' => $this->waybill, 'error_code' => '2', 'error' => 'Shipment not found');
            goto response;
        }
        $this->old_status = $this->shipment->status;
        $this->order_history->waybill_number = $this->shipment->jollychic;
        $this->order_history->order_number = $this->shipment->order_number;
        $this->order_history->company_id = $this->shipment->company_id;
        if($this->new_status == -2) 
            $response = $this->status_minus_2();
        else if($this->new_status == -1)
            $response = $this->status_minus_1();
        else if($this->new_status == 1)
            $response = $this->status_1();
        else if($this->new_status == 2)
            $response = $this->status_2();
        else if($this->new_status == 3)
            $response = $this->status_3();
        else if($this->new_status == 4)
            $response = $this->status_4();
        else if($this->new_status == 5)
            $response = $this->status_5();
        else if($this->new_status == 6) {
            if($this->undelivered_reason != 0)
                $response = $this->status_6(); 
            else 
                $response = array('success' => false, 'error' => 'Missing undelivered reason');
        }
        else if($this->new_status == 7)
            $response = $this->status_7();
        else 
            $response = array('success' => false, 'error_code' => '3', 'error' => "Update not allowed");
        $this->updatecaptainid($this->captainid);
        response:
        return $response;
    }

    private function status_minus_2() {
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $pickup_time_interval_id = $this->pickup_time_interval_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == -3) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $pickup_time_interval_id, $now_timestamp, $now_date, $now_time) {
                    $pickup_time_interval = PickupTimeInterval::where('id', '=', $pickup_time_interval_id)->first();
                    $from = $pickup_time_interval->from;
                    $to = $pickup_time_interval->to;
                    $time_interval = $from . "-" . $to;
                    $shipment->status = $new_status;
                    $shipment->pickup_walker = $captainid;
                    $shipment->pickup_reserved_date = $now_timestamp;
                    $shipment->pickup_time_interval = $time_interval;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->walker_id = $captainid;
                    $order_history->pickup_time_interval = $time_interval;
                    $order_history->save();
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage() . ' - ' . $e->getLine());
        }
        response:
        return $response;
    }

    private function status_minus_1() {
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == -2) {
                if($shipment->pickup_walker != $captainid) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '5', 'error' => 'Shipment reserved by another captain');
                    goto response;
                }
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $company = Companies::where('id', '=', $shipment->company_id)->first();
                    if($company->company_type == 3) // merchant 
                        $cash_on_pickup = $company->transportation_fee;
                    else // -------------------------- company 
                        $cash_on_pickup = 0;
                    $shipment->status = $new_status;
                    $shipment->pickup_date = $now_timestamp;
                    $shipment->cash_on_pickup = $cash_on_pickup;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->walker_id = $captainid;
                    $order_history->save();
                    Walker::where('id', '=', $captainid)->increment('picked_up');
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    // sort
    private function status_1() { 
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == 0) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $shipment->status = $new_status;
                    $shipment->sort_date = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                });
            }
            else if($old_status == -1) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $shipment->status = $new_status;
                    $shipment->sort_date = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                    Walker::where('id', '=', $captainid)->decrement('picked_up');
                    Walker::where('id', '=', $captainid)->increment('delivered');
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    // in route
    private function status_2() {
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == 1) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $shipment->status = $new_status;
                    $shipment->hub_id = $hub_id;
                    $shipment->new_shipment_date = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                else if($old_status == 0 && $new_status == 2)
                    $error = "Sort Shipment First";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            if($shipment->company_id != '952') { // abudawood
                $curl = new Curl();
                $sms = "سلام، شحنتكم $shipment->jollychic في الطريق إلينا، تابعها على https://saee.sa/trackingpage%3Ftrackingnum=$shipment->jollychic وتواصل مباشرة معنا على https://www.saee.sa/blackbox/$shipment->jollychic برمز الدخول $shipment->pincode2";
                // $company = Companies::find($shipment->company_id);
                // if($shipment->company_id == 923) { // salasa
                //     if(!empty($shipment->sender_name))
                //         $sms .= "$shipment->sender_name ";
                //     else 
                //         $sms .= "$company->name_ar ";
                // }
                // else if (isset($company)) {
                //     $sms .= !empty($shipment->sender_name) ? " " . $company->name_ar . ' [' . $shipment->sender_name . ']' : " " . $company->name_ar . " ";
                // }
                // $sms .= "قادمة إلى ";
                // $sms .= $shipment->d_city . ' ';
                // $sms .= "حدث عنوان التسليم لتصلك أسرع ";
                // $sms .= "https://saee.sa/trackingpage%3Ftrackingnum=$shipment->jollychic";
                $destinations1 = "966";
                $destinations1 .= substr(str_replace(' ', '', $shipment->receiver_phone), -9); 
                $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";
                $urlDiv = explode("?", $url);
                $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                Log::info("SMSSERVICELOG" . $result);
                if ($shipment->receiver_phone != $shipment->receiver_phone2) {
                    $destinations2 = "966";
                    $destinations2 .= substr(str_replace(' ', '', $shipment->receiver_phone2), -9);
                    $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";
                    $urlDiv = explode("?", $url);
                    $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                    Log::info("SMSSERVICELOG" . $result);
                }
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    // in warehouse
    private function status_3() { 
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            $admin = Admin::where('id', '=', $admin_id)->first();
            $admin_hub = $admin->hub_id;
            $admin_city = $admin->default_city;
            $check = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)
                ->whereRaw("(d_city = \"$admin_city\" or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = \"$admin_city\"))")
                ->count();
            if(!$check) {
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "You can not Sign-in Shipments not in $admin_city");
                goto response;
            }
            if($old_status == 0 && $shipment->company_id == '952') {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $now_timestamp, $now_date, $now_time, $admin_hub) {
                    // update for status 1
                    $shipment->sort_date = $now_timestamp;
                    $tmp_order_history = new OrdersHistory;
                    $tmp_order_history->waybill_number = $shipment->jollychic;
                    $tmp_order_history->order_number = $shipment->order_number;
                    $tmp_order_history->company_id = $shipment->company_id;
                    $tmp_order_history->status = '1';
                    $tmp_order_history->admin_id = $admin_id;
                    $tmp_order_history->save();
                    // update for status 2
                    $shipment->new_shipment_date = $now_timestamp;
                    $tmp_order_history = new OrdersHistory;
                    $tmp_order_history->waybill_number = $shipment->jollychic;
                    $tmp_order_history->order_number = $shipment->order_number;
                    $tmp_order_history->company_id = $shipment->company_id;
                    $tmp_order_history->status = '2';
                    $tmp_order_history->admin_id = $admin_id;
                    $tmp_order_history->save();
                    // update for status 3
                    $shipment->status = $new_status;
                    $shipment->signin_date = $now_timestamp;
                    $shipment->hub_id = $admin_hub;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                });
            }
            else if($old_status == 2) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $now_timestamp, $now_date, $now_time, $admin_hub) {
                    $shipment->status = $new_status;
                    $shipment->signin_date = $now_timestamp;
                    $shipment->hub_id = $admin_hub;
                    $shipment->planned_walker = '0';
                    $shipment->confirmed_walker = '0';
                    $shipment->group_id = '0';
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                });
            }
            else if($old_status == 6 && $shipment->undelivered_reason != 0) { // message
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $shipment->status = $new_status;
                    $shipment->planned_walker = '0';
                    $shipment->confirmed_walker = '0';
                    $shipment->group_id = '0';
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                    $ticket = Tickets::where('waybill', '=', $shipment->jollychic)->first(); 
                    $action = new TicketsActions;
                    $admin_username = Admin::where('id', '=', $admin_id)->first()->username;
                    if($ticket) {
                        $old_ticket = clone $ticket;
                        if($ticket->status == 1)
                            $ticket->status = 2;
                        else if($ticket->status == 3)
                            $ticket->status = 4;
                        else if($ticket->status == 4)
                            $ticket->status = 2;
                        $ticket->priority = 'High';
                        $action->ticket_id = $ticket->waybill;
                        $action->ticket_priority = $ticket->priority;
                        $action->admin = $admin_username;
                        $action->status = $ticket->status;
                        $action->shipment_name = $shipment->receiver_name;
                        $action->shipment_mobile1 = $shipment->receiver_phone;
                        $action->shipment_mobile2 = $shipment->receiver_phone2;
                        $action->shipment_city = $shipment->d_city;
                        $action->shipment_district = $shipment->d_district;
                        $action->shipment_address = $shipment->d_address;
                        $action->shipment_scheduled_date = $shipment->scheduled_shipment_date;
                        $action->shipment_is_cancelled = $shipment->is_cancelled;
                        $action->shipment_urgent_delivery = $shipment->urgent_delivery;
                        if($old_ticket->priority != $ticket->priority);
                            $action->is_priority = 1;
                        if($old_ticket->status != $ticket->status)
                            $action->is_status = 1;
                        $action->comment = 'Shipment returned to warehouse';
                    }
                    else {
                        $ticket = new Tickets;
                        $ticket->waybill = $shipment->jollychic;
                        $ticket->priority = 'High';
                        $ticket->status = 1;
                        $ticket->created_for = "Shipment returned to warehouse";
                        $action->ticket_id = $ticket->waybill;
                        $action->ticket_priority = $ticket->priority;
                        $action->admin = $admin_username;
                        $action->status = 1;
                        $action->shipment_name = $shipment->receiver_name;
                        $action->shipment_mobile1 = $shipment->receiver_phone;
                        $action->shipment_mobile2 = $shipment->receiver_phone2;
                        $action->shipment_city = $shipment->d_city;
                        $action->shipment_district = $shipment->d_district;
                        $action->shipment_address = $shipment->d_address;
                        $action->shipment_scheduled_date = $shipment->scheduled_shipment_date;
                        $action->shipment_is_cancelled = $shipment->is_cancelled;
                        $action->shipment_urgent_delivery = $shipment->urgent_delivery;
                        $action->comment = 'Ticket Created';
                    }
                    $ticket->save();
                    $action->save();
                });
                // returns message
                // if($anitem->company_id != '952') {
                //     $curl = new Curl();
                //     $sms1 = "عزيزي عميل";
                //     $company = Companies::find($anitem->company_id);
                //     if (isset($company)) {
                //         $sms1 .= " " . $company->name_ar . " ";
                //     }
                //     $sms1 .= "لم نستطع الوصول إليك برجاء الاتصال/واتساب على 0550518190 للتأكد من عنوان شحنتكم رقم";
                //     $sms1 .= "،";
                //     $sms1 .= $anitem->jollychic;
                //     $destinations1 = "966";
                //     $destinations1 .= substr(str_replace(' ', '', $anitem->receiver_phone), -9);
                //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms1&msg_type=2";
                //     $urlDiv = explode("?", $url);
                //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                //     Log::info("SMSSERVICELOG" . $result);
                // }
            }
            else if($old_status == 4) {
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "You can not sign-in 'Out For Delivery' shipments.");
                goto response;
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    // update for status 6
                    $tmp_order_history = new OrdersHistory;
                    $tmp_order_history->waybill_number = $shipment->jollychic;
                    $tmp_order_history->order_number = $shipment->order_number;
                    $tmp_order_history->company_id = $shipment->company_id;
                    $tmp_order_history->status = '6';
                    $tmp_order_history->walker_id = $shipment->confirmed_walker;
                    $tmp_order_history->save();
                    $shipment->num_faild_delivery_attempts = $shipment->num_faild_delivery_attempts + 1;
                    // update for status 3
                    $shipment->status = $new_status;
                    $shipment->planned_walker = '0';
                    $shipment->confirmed_walker = '0';
                    $shipment->group_id = '0';
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                    $ticket = Tickets::where('waybill', '=', $shipment->jollychic)->first(); 
                    $action = new TicketsActions;
                    $admin_username = Admin::where('id', '=', $admin_id)->first()->username;  
                    if($ticket) {
                        $old_ticket = clone $ticket;
                        if($ticket->status == 1)
                            $ticket->status = 2;
                        else if($ticket->status == 3)
                            $ticket->status = 4;
                        else if($ticket->status == 4)
                            $ticket->status = 2;
                        $ticket->priority = 'High';
                        $action->ticket_id = $ticket->waybill;
                        $action->ticket_priority = $ticket->priority;
                        $action->admin = $admin_username;
                        $action->status = $ticket->status;
                        $action->shipment_name = $shipment->receiver_name;
                        $action->shipment_mobile1 = $shipment->receiver_phone;
                        $action->shipment_mobile2 = $shipment->receiver_phone2;
                        $action->shipment_city = $shipment->d_city;
                        $action->shipment_district = $shipment->d_district;
                        $action->shipment_address = $shipment->d_address;
                        $action->shipment_scheduled_date = $shipment->scheduled_shipment_date;
                        $action->shipment_is_cancelled = $shipment->is_cancelled;
                        $action->shipment_urgent_delivery = $shipment->urgent_delivery;
                        if($old_ticket->priority != $ticket->priority);
                            $action->is_priority = 1;
                        if($old_ticket->status != $ticket->status)
                            $action->is_status = 1;
                        $action->comment = 'Shipment returned to warehouse';
                    }
                    else {
                        $ticket = new Tickets;
                        $ticket->waybill = $shipment->jollychic;
                        $ticket->priority = 'High';
                        $ticket->status = 1;
                        $ticket->created_for = "Shipment returned to warehouse";
                        $action->ticket_id = $ticket->waybill;
                        $action->ticket_priority = $ticket->priority;
                        $action->admin = $admin_username;
                        $action->status = 1;
                        $action->shipment_name = $shipment->receiver_name;
                        $action->shipment_mobile1 = $shipment->receiver_phone;
                        $action->shipment_mobile2 = $shipment->receiver_phone2;
                        $action->shipment_city = $shipment->d_city;
                        $action->shipment_district = $shipment->d_district;
                        $action->shipment_address = $shipment->d_address;
                        $action->shipment_scheduled_date = $shipment->scheduled_shipment_date;
                        $action->shipment_is_cancelled = $shipment->is_cancelled;
                        $action->shipment_urgent_delivery = $shipment->urgent_delivery;
                        $action->comment = 'Ticket Created';
                    }
                    $ticket->save();
                    $action->save();
                });
                // returns message
                // if($anitem->company_id != '952') {
                //     $curl = new Curl();
                //     $sms1 = "عزيزي عميل";
                //     $company = Companies::find($anitem->company_id);
                //     if (isset($company)) {
                //         $sms1 .= " " . $company->name_ar . " ";
                //     }
                //     $sms1 .= "لم نستطع الوصول إليك برجاء الاتصال/واتساب على 0550518190 للتأكد من عنوان شحنتكم رقم";
                //     $sms1 .= "،";
                //     $sms1 .= $anitem->jollychic;
                //     $destinations1 = "966";
                //     $destinations1 .= substr(str_replace(' ', '', $anitem->receiver_phone), -9);
                //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms1&msg_type=2";
                //     $urlDiv = explode("?", $url);
                //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                //     Log::info("SMSSERVICELOG" . $result);
                // }
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    private function status_4() { 
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            $override_firstwalker = $this->override_firstwalker;
            $captain = Walker::find($captainid);
            if($old_status == 3) {
                $unscheduled = DeliveryRequest::where('jollychic', '=', $shipment->jollychic)->whereNull('scheduled_shipment_date')->count();
                if($unscheduled > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "$shipment->jollychic Scheduled shipment date is empty. Please try again.");
                    goto response;
                }
                $pendingPayment = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('status', '=', 5)->where('is_money_received', '=', 0)->count();
                if($pendingPayment > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "Sorry, captain still has $pendingPayment delivered/pending cash on delivery. Please release them first.");
                    goto response;
                }
                $pendingCoveredAmount = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('status', '=', 5)->where('is_amount_covered', '=', 1)->count();
                if ($pendingCoveredAmount > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => 'Sorry, captain ' . $captainid . ' still has pending covered amount. Please release them first.');
                    goto response;
                }
                $pendingCount = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('status', '=', 6)->count();
                if($pendingCount > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "Sorry, captain still has $pendingCount undelivered pending shipments. Please release them first.");
                    goto response;
                }
                $pendingshipments = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('scheduled_shipment_date', '<', $now_date)->where('status', '<>', 5)->count();
                if($pendingshipments > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "Sorry, captain still has $pendingCount undelivered pending shipments. Please release them first.");
                    goto response;
                }
                $check_district = DeliveryRequest::whereRaw("jollychic = '$shipment->jollychic' and d_district <> 'No District' and d_district in (select distinct SUBSTRING_INDEX(district,'+',-1) from districts)")->count();
                $riyadh_baha = DeliveryRequest::whereRaw("jollychic = '$shipment->jollychic' and d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = 'Riyadh' or city = 'Baha')")->count();
                if($check_district == 0 && $riyadh_baha == 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error' => "Sorry, shipment have incorrect district. Please fix it from plan distribution");
                    goto response;
                }
                // in pickup shipments: pay the money then sign in shipment
                $pending_pickup_count = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('status', '=', -1)->count();
                if($pending_pickup_count > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'captain_id' => $captainid, 'error' => "Sorry, captain still has $pending_pickup_count pending picked up shipments.");
                    goto response;
                }
                $pending_pickup_amount = DeliveryRequest::where('confirmed_walker', '=', $captainid)->where('status', '=', -1)->where('is_pickup_cash_received', '=', 0)->count();
                if($pending_pickup_amount > 0) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'captain_id' => $captainid, 'error' => "Sorry, captain still has $pending_pickup_amount pending pickup shipments money.");
                    goto response;
                }
                $planned_walker = $shipment->planned_walker;
                if($planned_walker != $captainid) {
                    $response = array('success' => false, 'waybill' => $shipment->jollychic, 'captain_id' => $captainid, 'error' => "shipment is not assigned to this captain.");
                    goto response;
                }
                $response = DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time, $override_firstwalker, $captain) {
                    if ($shipment->planned_walker != 0 && $shipment->planned_walker != $captainid && $override_firstwalker == 'No') {
                        return array('success' => false, 'first_planned' => $shipment);
                    }
                    $captainAccount = new CaptainAccount();
                    $captainAccount->waybill = $shipment->jollychic;
                    $captainAccount->captain_id = $captainid;
                    $captainAccount->type = 0;
                    if ($shipment->cash_on_delivery > 0) {
                        $captainAccount->payment_method = 1;
                        $captainAccount->amount = $shipment->cash_on_delivery;
                    } else {
                        $captainAccount->payment_method = 0;
                        $captainAccount->amount = 0;
                    }
                    $captainAccount->admin_id = $admin_id;
                    $captainAccount->save();
                    $shipment->status = $new_status;
                    $shipment->planned_walker = $captainid;
                    $shipment->confirmed_walker = $captainid;
                    $shipment->scheduled_shipment_date = $now_date;
                    $shipment->pickup_timestamp = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->walker_id = $captainid;
                    $order_history->save();
                    $captain->total_scanned = $captain->total_scanned + 1;
                    $captain->with_captain = $captain->with_captain + 1;
                    $captain->save();
                    return array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
                });
                if($response['success']) {
                    // $company = Companies::find($shipment->company_id);
                    // if ($shipment->company_id == 917 || $shipment->company_id == 931) {
                    //     $sms = "عزيزي/زتي ";
                    //     $sms .= "يوجد لديكم شحنة عن طريق ";
                    //     if (isset($company)) {
                    //         $sms .= !empty($shipment->sender_name) ? " " . $company->name_ar . ' [' . $shipment->sender_name. ']' : " " . $company->name_ar . " ";
                    //     }
                    //     $sms .= "  رقم ";
                    //     $sms .= $shipment->shipment_number;
                    //     $sms .= "  برقم كود ";
                    //     $sms .= $shipment->pincode;
                    //     if ($shipment->cash_on_delivery) {
                    //         $sms .= " ومبلغ ";
                    //         $sms .= $shipment->cash_on_delivery;
                    //         $sms .= " ريال";
                    //     }
                    //     $sms .= "  مع المندوب ";
                    //     if (isset($captain)) {
                    //         $sms .= $captain->first_name;
                    //         $sms .= " 0";
                    //         $sms .= substr(str_replace(' ', '', $captain->phone), -9);
                    //     }
                    // } else {
                    //     $sms = "عزيزي عميل ";
                    //     if(isset($company) && $company->id == 923) { // salasa
                    //         if(!empty($shipment->sender_name))
                    //             $sms .= "$shipment->sender_name ";
                    //         else 
                    //             $sms .= "$company->name_ar ";
                    //     }
                    //     else if (isset($company)) {
                    //         $sms .= !empty($shipment->sender_name) ? " " . $company->name_ar . ' [' . $shipment->sender_name . ']' : " " . $company->name_ar . " ";
                    //     }
                    //     $sms .= " طلبكم سيصلكم اليوم مع المندوب ";
                    //     if (isset($captain)) {
                    //         $sms .= $captain->first_name;
                    //         $sms .= " 0";
                    //         $sms .= substr(str_replace(' ', '', $captain->phone), -9);
                    //     }
                    //     $sms .= " رمز التسليم ";
                    //     $sms .= $shipment->pincode;
                    //     if ($shipment->cash_on_delivery) {
                    //         $sms .= " ومبلغ الطلب ";
                    //         $sms .= $shipment->cash_on_delivery;
                    //         $sms .= " ريال";
                    //     }
                    //     $sms .= " مع تحيات ساعي";
                    // }
                    // if($order->company_id != '952') {
                    //     Log::info($sms);
                    //     $destinations1 = "966";
                    //     $destinations1 .= substr(str_replace(' ', '', $order->receiver_phone), -9);
                    //     $curl = new Curl();
                    //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";
                    //     $urlDiv = explode("?", $url);
                    //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                    //     Log::info("SMSSERVICELOG" . $result);
                    //     if ($order->receiver_phone2 != '' && $order->receiver_phone2 != $order->receiver_phone) {
                    //         $destinations2 = "966";
                    //         $destinations2 .= substr(str_replace(' ', '', $order->receiver_phone2), -9);
                    //         $curl = new Curl();
                    //         $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";
                    //         $urlDiv = explode("?", $url);
                    //         $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                    //         Log::info("SMSSERVICELOG" . $result);
                    //     }
                    // }
                }
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'message' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    private function status_5() {
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == 4) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $captainid = $captainid == 0 ? $shipment->confirmed_walker : $captainid;
                    $shipment->status = $new_status;
                    $shipment->confirmed_walker = $captainid;
                    $shipment->dropoff_timestamp = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->walker_id = $captainid;
                    $order_history->save();
                    Walker::where('id', '=', $captainid)->increment('total_delivered');
                    $ticket = new Ticket($shipment->jollychic, 66);
                    $ticket->close(array(), "Ticket Closed On Delivery"); 
                });
            }
            else if($old_status == -1) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $captainid = $captainid == 0 ? $shipment->confirmed_walker : $captainid;
                    $shipment->status = $new_status;
                    $shipment->confirmed_walker = $captainid;
                    $shipment->dropoff_timestamp = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->walker_id = $captainid;
                    $order_history->save();
                    Walker::where('id', '=', $captainid)->increment('total_delivered');
                    Walker::where('id', '=', $captainid)->decrement('picked_up');
                    $ticket = new Ticket($shipment->jollychic, 66);
                    $ticket->close(array(), "Ticket Closed On Delivery"); 
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    private function status_6() { // message - put reason id in orders history
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            $undelivered_reason = $this->undelivered_reason;
            if($old_status == 4) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $now_timestamp, $now_date, $now_time, $undelivered_reason) {
                    $delivery_failed_because = UndeliveredReason::where('id', '=', $undelivered_reason)->first();
                    $shipment->status = $new_status;
                    $shipment->undelivered_reason = $undelivered_reason;
                    $shipment->num_faild_delivery_attempts = $shipment->num_faild_delivery_attempts + 1;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->walker_id = $captainid;
                    $order_history->notes = $delivery_failed_because->english;
                    $order_history->reason_id = $undelivered_reason;
                    $order_history->save();
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response;
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    private function status_7() {
        try {
            $shipment = $this->shipment;
            $order_history = $this->order_history;
            $new_status = $this->new_status;
            $old_status = $this->old_status;
            $captainid = $this->captainid;
            $admin_id = $this->admin_id;
            $hub_id = $this->hub_id;
            $now_timestamp = $this->now_timestamp;
            $now_date = $this->now_date;
            $now_time = $this->now_time;
            if($old_status == 3 && $shipment->undelivered_reason != 0) {
                DB::transaction(function() use($shipment, $order_history, $new_status, $old_status, $captainid, $admin_id, $hub_id, $now_timestamp, $now_date, $now_time) {
                    $shipment->status = $new_status;
                    $shipment->planned_walker = '0';
                    $shipment->confirmed_walker = '0';
                    $shipment->return_to_supplier_date = $now_timestamp;
                    $shipment->save();
                    $order_history->status = $new_status;
                    $order_history->admin_id = $admin_id;
                    $order_history->save();
                });
            }
            else {
                $old_status_name = $this->all_statuses[$old_status];
                $new_status_name = $this->all_statuses[$new_status];
                $error = "Transition from '$old_status_name' status to '$new_status_name' status is not allowed";
                if($old_status == $new_status)
                    $error = "Shipment status is already '$new_status_name'";
                $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '3', 'error' => $error);
                goto response; 
            }
            $response = array('success' => true, 'message' => "Status updated successfully ($old_status => $new_status)", 'shipment' => $shipment);
        }
        catch (Exception $e) {
            $response = array('success' => false, 'waybill' => $shipment->jollychic, 'error_code' => '4', 'error' => "status ($old_status => $new_status) error: " . $e->getMessage());
        }
        response:
        return $response;
    }

    public function updatecaptainid($captainid) {
        if($captainid == 0 || $captainid == '')
            return false;
        $delivered = DB::select("select count(dr.id) 'delivered' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 5 and dr.is_money_received = 0")[0]->delivered;
        $with_captain = DB::select("select count(delivery_request.id) 'with_captain' from delivery_request where confirmed_walker = $captainid and status = 4")[0]->with_captain;
        $returned = DB::select("select count(dr.id) 'returned' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 6")[0]->returned;
        $lastscheduledshipmentdate = DB::select("select ifnull(max(scheduled_shipment_date), '') 'lastscheduledshipmentdate' from delivery_request where confirmed_walker = $captainid and is_money_received = 0")[0]->lastscheduledshipmentdate;
        $last_payment_date = DB::select("SELECT max(date) 'last_payment_date' FROM captain_account_history as cahd WHERE cahd.captain_id = $captainid")[0]->last_payment_date;
        $balance = DB::select("select sum(cah.captain_amount) 'balance' from captain_account_history cah where cah.captain_id = $captainid and cah.paid_to_captain = 0")[0]->balance;
        $pending_since = "select min(PendingSince) 'pendingsince' from (
            select min(scheduled_shipment_date) 'PendingSince' from delivery_request where status in (4,6)  and confirmed_walker = $captainid
            union 
            select min(dropoff_timestamp) 'PendingSince' from delivery_request where status = 5 and is_money_received = 0  and confirmed_walker = $captainid
            ) A";
        $pending_since = DB::select($pending_since)[0]->pendingsince;
        if($pending_since == NULL)
            $pending_since = '0000-00-00 00:00:00';
        $delayedcountquery = "select waybill_number 'DelayedShipments' from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) union select waybill_number 'DelayedShipments' from orders_history where status = 5 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 5 and is_money_received = 0 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 union select waybill_number 'DelayedShipments' from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) ";
        $delayedcount = DB::select($delayedcountquery);
        $delayedcountarray = '(';
        for($i = 0; $i < count($delayedcount); ++$i)
            if($i == 0)
                $delayedcountarray .= "'".$delayedcount[$i]->DelayedShipments."'";
            else
                $delayedcountarray .= ','."'".$delayedcount[$i]->DelayedShipments."'";
        $delayedcountarray .= ')';
        if(count($delayedcount) > 0)
            $delayedamount = DB::select("select sum(cash_on_delivery) 'delayedamount' from delivery_request where jollychic in $delayedcountarray")[0]->delayedamount;
        else
            $delayedamount = 0;
        DB::update("update walker set delivered = ? , with_captain = ? , returned = ? , last_scheduled_shipment_date = ? , last_payment_date = ? , balance = ? , pending_since = ? , delayed_count = ? , delayed_amount = ? where id = ?", [$delivered, $with_captain, $returned, $lastscheduledshipmentdate, $last_payment_date, $balance, $pending_since, count($delayedcount), $delayedamount, $captainid]);
        return true;
    }

    public function hello() {
        
        echo 'hello ';
        return 'world';
        
    }
    
}

