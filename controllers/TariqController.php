<?php


use App\Classes\Curl;

use GuzzleHttp\Client;
 
class TariqController extends WarehouseController
{
    
   public function __construct() {
       parent::__construct();
    }
    
    public function home()
    {
        $rawQ = "select count(*) as count, 
CASE 
WHEN status = 5 THEN 5 
WHEN status = 1 and num_faild_delivery_attempts = 0 THEN 1 
WHEN status = 2 and num_faild_delivery_attempts = 0 THEN 2 
WHEN status = 2 and num_faild_delivery_attempts <> 0 THEN 6
WHEN status in (4,6) THEN 4 
WHEN status = 7 THEN 7 

END as 'Status' 
FROM delivery_request where company_id = 911 and order_number <> '' GROUP by 2";

        //echo "hello";
       // echo $rawQ;
       
        $responce = DB::select($rawQ);
        //print_r( array_values( $responce ));
        
        $statusArr = array();
        $allitems = 0;
        $successrate = 0;
        $onwayitems = 0;
        $arriveditems = 0;
        $outfordeliveryitems = 0;
        $delivreditems = 0;
        $undelivreditems = 0;
        $returnedToJC = 0;
        $returnedToJC = 0;
        
        foreach ($responce as $order) 
        {
            $statusSrt = '';
            $allitems +=$order->count;
            
            switch ($order->Status)
            {
                
                case 1:
                    $statusSrt = 'not scanned yet';
                    $onwayitems +=$order->count;
                    break;
                case 2:
                    $statusSrt = 'in warehouse';
                    $arriveditems +=$order->count;
                    break;
                case 3:
                    $statusSrt = 'in warehouse';
                    $arriveditems +=$order->count;
                    break;
                case 4:
                    $statusSrt = 'with captains';
                    $outfordeliveryitems +=$order->count;
                    break;
                case 5:
                    $statusSrt = 'delivered';
                    $delivreditems +=$order->count;
                    break;
                case 6:
                    $statusSrt = 'failed to deliver';
                    $undelivreditems +=$order->count;
                    break;
                case 7:
                    $statusSrt = 'returned to JC';
                    $returnedToJC +=$order->count;
                    break;
                
            }
            
            array_push($statusArr, array(
                'status' => $statusSrt,
                'count' => $order->count
                ));
        }
                    
        $response_array = array(
                        'success' => true,
                        'details' => $statusArr);
         
        $successrate =  $delivreditems/  $allitems * 100;         
        return View::make('tariq.home')
            ->with('allitems', $allitems)
            ->with('successrate', $successrate)
            ->with('onwayitems', $onwayitems)
            ->with('arriveditems', $arriveditems)
            ->with('outfordeliveryitems', $outfordeliveryitems)
            ->with('delivreditems', $delivreditems)
            ->with('undelivreditems', $undelivreditems)
            ->with('returnedToJC',$returnedToJC);
        //return $response_array;                

    }

    public function tracking()
    {

        if (Request::has('trackingnum')) {
            try {
                $waybillnumber = Request::get('trackingnum');
                $searchcol = 'delivery_request.waybill';
                if(preg_match('/^JC[0-9]{8}KS/',$waybillnumber) == 1)
                {
                    $searchcol = 'delivery_request.jollychic';
                }

                $order_all = OrdersHistory::select('orders_history.id', 'orders_history.created_at', 'orders_history.status as statusnum',
                    'delivery_request.receiver_name', 'delivery_request.d_city as city', 'order_statuses.english as statustxt', 'notes')
                    ->leftJoin('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->leftJoin('delivery_request', $searchcol, '=', 'orders_history.waybill_number')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();


                if (count($order_all)) {

                    $orderArr = array();

                    foreach ($order_all as $order) {
                        if ($order->statusnum) {
                            $order->city = $order->city;
                        } else {
                            $order->city = 'Riyadh';
                        }
                        if ($order->statusnum==6) {
                            $order->statustxt = $order->notes;
                        }
                        array_push($orderArr, array(
                            'id' => $order->id,
                            'city' => $order->city,
                            'status' => $order->statusnum,
                            'notes' => $order->statustxt,
                            'updated_at' => $order->created_at
                        ));
                    }

                    $response_array = array(
                        'success' => true,
                        'details' => $orderArr);
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'waybill [' . $waybillnumber . '] not found');
                }

            } catch (Exception $e) {

                return $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            }

        } else {
            $response_array = array(
                'success' => false,
                'error' => 'no tracking number');
        }


        $response = Response::json($response_array, 200);
        return $response;

    }

    public function trackingpage()
    {

         if (!Session::has('language')) {
            Session::put('language', 'ar');
            $language = 'ar';
        }else{
            $language = Session::get('language');
            Log::info($language);
        }
        $page = 'warehouse.trackingar';
        if($language=='en'){
            $page = 'warehouse.tracking';
            $finalstaustxt = 'Not Found!';
        }else{

            $finalstaustxt = 'غير موجود';
        }

        if (Request::has('trackingnum')) {
            try {
                $waybillnumber = Request::get('trackingnum');
                $finalstausnum = 0;

                $orderDetail = DeliveryRequest::select('jollychic', 'D_latitude', 'D_longitude')->where('jollychic', '=', $waybillnumber)->get();

                // $order_all = OrdersHistory::where('waybill_number', '=', $waybillnumber)->orderBy('id', 'asc')->get();;

                $captainData = DeliveryRequest::select('delivery_request.confirmed_walker', 'walker.first_name', 'walker.last_name', 'walker.phone')
                    ->join('walker', 'walker.id', '=', 'delivery_request.confirmed_walker')
                    ->where('jollychic', '=', $waybillnumber)->orderBy('delivery_request.id', 'desc')->get();

                $order_all = OrdersHistory::select('created_at', 'status as statusnum', 'english as statustxt', 'arabic as statusarabic', 'notes')
                    ->join('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();

                    $orderHistory = 'select CONCAT(w.first_name, " " ,w.last_name),w.phone,o.updated_at,s.english,s.arabic, 
CASE
   WHEN o.status in (0,1,7) THEN c.city
   WHEN o.status in (2,3,4,5,6) THEN d.d_city 
END as "city", o.notes
from delivery_request d 
left join  orders_history o
 on d.jollychic = o.waybill_number 
left join  walker w
 on d.confirmed_walker = w.id 
left join  companies c
on d.company_id = c.id 
left join  order_statuses s
 on o.status = s.id 
where d.jollychic = "'.$waybillnumber.'"
order by o.updated_at desc';

$orderHistory = DB::select($orderHistory);

                    if(count($captainData)){
                        $captainName = $captainData[0]->first_name.' '.$captainData[0]->last_name;
                        $captainPhone = $captainData[0]->phone;
                    }
                    else{
                        $captainName = '';
                        $captainPhone ='';
                    }

                if (count($order_all)) {
                    $finalstausnum = $order_all[0]->statusnum;
                    if($language=='en'){
                    $finalstaustxt = $order_all[0]->statustxt;
                }else{
                    $finalstaustxt = $order_all[0]->statusarabic;
                }

                } else {
                    $finalstausnum = 0;
                    if($language=='en'){
                    $waybillnumber = 'Not Found!';
                    }else{
                       $waybillnumber = 'غير موجود';
                    }
                    $finalstaustxt = '#';
                    $order_all = array();
                }
                foreach ($order_all as $order) {

                    if ($order->statusnum) {
                        $order->city = 'Jeddah';
                    } else {
                        $order->city = 'Riyadh';
                    }

                }

                if ($finalstausnum == 0) {
                    $finalstausnum = 5;
                } else if ($finalstausnum == 1) {
                    $finalstausnum = 25;
                } else if ($finalstausnum == 2 || $finalstausnum == 3) {
                    $finalstausnum = 50;
                } else if ($finalstausnum == 4 || $finalstausnum == 6) {
                    $finalstausnum = 75;
                } else {
                    $finalstausnum = 100;
                }


            } catch (Exception $e) {

                return $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            }

        } else {
            return View::make($page)
                ->with('waybill', '')
                ->with('finalstausnum', '')
                ->with('finalstaustxt', $finalstaustxt)
                ->with('captainName', '')
                ->with('orderHistory', [])
                ->with('captainPhone', '')
                ->with('history', [])
                ->with('orderDetail', []);
        }
        return View::make($page)
            ->with('waybill', $waybillnumber)
            ->with('orderHistory', $orderHistory)
            ->with('finalstausnum', $finalstausnum)
            ->with('finalstaustxt', $finalstaustxt)
            ->with('captainName', $captainName)
            ->with('captainPhone', $captainPhone)
            ->with('history', $order_all)
            ->with('orderDetail', $orderDetail);
    }


    

}