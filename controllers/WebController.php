<?php
use JD\Cloudder\Facades\Cloudder;
class WebController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     *
     * @return Response
     */
    public function __construct() {
        if (Config::get('app.production')) {
            echo "Something cool is going to be here soon.";
            die();
        }
    }

    private function _braintreeConfigure() {
        Braintree_Configuration::environment(Config::get('app.braintree_environment'));
        Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
        Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
        Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
    }

    public function index() {
        return View::make('website.index');
    }
    public function newweb() {

        if (!Session::has('language')) {
            Session::put('language', 'ar');
            $language = 'ar';
        }else{
            $language = Session::get('language');
            Log::info($language);
        }
        $page = 'website.newwebar';
        if($language=='en'){
            $page = 'website.newweb1';
            $finalstaustxt = 'Not Found!';
        }else{

            $finalstaustxt = 'غير موجود';
        }
        return View::make($page);
    }
    public function newwebar() {
        return View::make('website.newwebar');
    }
    public function sendemailpost(){

        // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $name = strip_tags(trim($_POST["name"]));
                $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["msg"]);
        $phone =   trim($_POST["phone"]);

        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "danish.j@kasper-cab.com";

        // Set the email subject.
        $subject = "New contact from $name";

        // Build the email content.
        $email_content = "Name: $name\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Phone:\n$phone\n";
        $email_content .= "Message:\n$message\n";

        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Thank You! Your message has been sent.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

    }


    public function termsncondition() {
        $theme = Theme::all();
        $logo = '/image/logo.png';
        $favicon = '/image/favicon.ico';
        foreach ($theme as $themes) {
            $favicon = '/uploads/' . $themes->favicon;
            $logo = '/uploads/' . $themes->logo;
        }
        if ($logo == '/uploads/') {
            $logo = '/image/logo.png';
        }
        if ($favicon == '/uploads/') {
            $favicon = '/image/favicon.ico';
        }
        $app_name = Config::get('app.website_title');
        return View::make('website.termsandconditions')
                        ->with('title', 'Terms and Conditions')
                        ->with('logo', $logo)
                        ->with('favicon', $favicon)
                        ->with('app_name', $app_name);
    }

    public function banking_provider_mobile() {
        $id = Request::segment(2);
        $provider = Walker::where('id', $id)->first();

        $theme = Theme::all();
        $logo = '/image/logo.png';
        $favicon = '/image/favicon.ico';
        foreach ($theme as $themes) {
            $favicon = '/uploads/' . $themes->favicon;
            $logo = '/uploads/' . $themes->logo;
        }
        if ($logo == '/uploads/') {
            $logo = '/image/logo.png';
        }
        if ($favicon == '/uploads/') {
            $favicon = '/image/favicon.ico';
        }
        $provider_first_name = $provider->first_name;
        $provider_last_name = $provider->last_name;
        $provider_email = $provider->email;
        $app_name = Config::get('app.website_title');
        if ($provider->merchant_id == NULL) {
            if (Config::get('app.default_payment') == 'stripe') {
                return View::make('website.banking_provider_stripe')
                                ->with('title', 'Banking Provider')
                                ->with('logo', $logo)
                                ->with('favicon', $favicon)
                                ->with('app_name', $app_name)
                                ->with('provider', $provider)
                                ->with('provider_id', $id)
                                ->with('provider_first_name', $provider_first_name)
                                ->with('provider_last_name', $provider_last_name)
                                ->with('provider_email', $provider_email);
            } else {
                return View::make('website.banking_provider_braintree')
                                ->with('title', 'Banking Provider')
                                ->with('logo', $logo)
                                ->with('favicon', $favicon)
                                ->with('app_name', $app_name)
                                ->with('provider', $provider)
                                ->with('provider_id', $id);
            }
        } else {
            return View::make('website.banking_done')
                            ->with('title', 'Banking Provider')
                            ->with('logo', $logo)
                            ->with('favicon', $favicon)
                            ->with('app_name', $app_name)
                            ->with('provider', $provider);
        }
    }

    public function providerB_bankingSubmit() {
        $this->_braintreeConfigure();
        $result = new stdClass();
        $result = Braintree_MerchantAccount::create(
                        array(
                            'individual' => array(
                                'firstName' => Input::get('first_name'),
                                'lastName' => Input::get('last_name'),
                                'email' => Input::get('email'),
                                'phone' => Input::get('phone'),
                                'dateOfBirth' => date('Y-m-d', strtotime(Input::get('dob'))),
                                'ssn' => Input::get('ssn'),
                                'address' => array(
                                    'streetAddress' => Input::get('streetAddress'),
                                    'locality' => Input::get('locality'),
                                    'region' => Input::get('region'),
                                    'postalCode' => Input::get('postalCode')
                                )
                            ),
                            'funding' => array(
                                'descriptor' => 'UberForX',
                                'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
                                'email' => Input::get('bankemail'),
                                'mobilePhone' => Input::get('bankphone'),
                                'accountNumber' => Input::get('accountNumber'),
                                'routingNumber' => Input::get('routingNumber')
                            ),
                            'tosAccepted' => true,
                            'masterMerchantAccountId' => Config::get('app.masterMerchantAccountId'),
                            'id' => "taxinow" . Input::get('id')
                        )
        );

        Log::info('res = ' . print_r($result, true));
        if ($result->success) {
            $pro = Walker::where('id', Input::get('id'))->first();
            $pro->merchant_id = $result->merchantAccount->id;
            $pro->save();
            Log::info(print_r($pro, true));
            Log::info('Adding banking details to provider from Admin = ' . print_r($result, true));
            return Redirect::to("/");
        } else {
            Log::info('Error in adding banking details: ' . $result->message);
            return Redirect::to("banking_provider_mobile");
        }
    }

    public function providerS_bankingSubmit() {
        $id = Input::get('id');
        Log::info('id = ' . print_r($id, true));
        Stripe::setApiKey(Config::get('app.stripe_secret_key'));
        $token_id = Input::get('stripeToken');
        Log::info('token_id = ' . print_r($token_id, true));
        // Create a Recipient
        try {
            $recipient = Stripe_Recipient::create(array(
                        "name" => Input::get('first_name') . " " . Input::get('last_name'),
                        "type" => Input::get('type'),
                        "bank_account" => $token_id,
                        "email" => Input::get('email')
                            )
            );

            Log::info('recipient = ' . print_r($recipient, true));

            $pro = Walker::where('id', Input::get('id'))->first();
            $pro->merchant_id = $recipient->id;
            $pro->account_id = $recipient->active_account->id;
            $pro->last_4 = $recipient->active_account->last4;
            $pro->save();

            Log::info('recipient added = ' . print_r($recipient, true));
        } catch (Exception $e) {
            //Log::info('Error in Stripe = ' . print_r($e, true));
            return Redirect::route("banking_provider_mobile", $id);
        }
        return Redirect::to("/");
    }

    public function page($title) {

        $theme = Theme::all();
        $logo = '/image/logo.png';
        $favicon = '/image/favicon.ico';
        foreach ($theme as $themes) {
            $favicon = '/uploads/' . $themes->favicon;
            $logo = '/uploads/' . $themes->logo;
        }
        if ($logo == '/uploads/') {
            $logo = '/image/logo.png';
        }
        if ($favicon == '/uploads/') {
            $favicon = '/image/favicon.ico';
        }
        $app_name = Config::get('app.website_title');
        return View::make('website.' . $title)
                        ->with('title', $title)
                        ->with('logo', $logo)
                        ->with('favicon', $favicon)
                        ->with('app_name', $app_name);
    }

    public function track_ride() {
        $id = Request::segment(2);
        $request = Requests::where('security_key', $id)->where('is_started', 1)->where('is_completed', 0)->first();
        if ($request) {
            $owner = Owner::where('id', $request->owner_id)->first();
            $user_name = $owner->first_name . " " . $owner->last_name;
            $theme = Theme::all();
            $logo = '/image/logo.png';
            $favicon = '/image/favicon.ico';
            foreach ($theme as $themes) {
                $favicon = '/uploads/' . $themes->favicon;
                $logo = '/uploads/' . $themes->logo;
            }
            if ($logo == '/uploads/') {
                $logo = '/image/logo.png';
            }
            if ($favicon == '/uploads/') {
                $favicon = '/image/favicon.ico';
            }
            $app_name = Config::get('app.website_title');
            // walk location
            $reqloc = WalkLocation::where('request_id', $request->id)->first();
            return View::make('website.track_ride')
                            ->with('title', 'Track ' . $user_name)
                            ->with('user_name', $user_name)
                            ->with('logo', $logo)
                            ->with('favicon', $favicon)
                            ->with('app_name', $app_name)
                            ->with('cur_lat', $reqloc->latitude)
                            ->with('cur_lon', $reqloc->longitude)
                            ->with('track_id', $id);
        } else {
            return Redirect::to('/');
        }
    }

    // Ajax for auto updating location for tracking
    public function get_track_loc($id) {
        $request = Requests::where('security_key', $id)->where('is_started', 1)->where('is_completed', 0)->first();
        if ($request) {
            $owner = Owner::where('id', $request->owner_id)->first();
            $user_name = $owner->first_name . " " . $owner->last_name;
            $theme = Theme::all();
            $logo = '/image/logo.png';
            $favicon = '/image/favicon.ico';
            foreach ($theme as $themes) {
                $favicon = '/uploads/' . $themes->favicon;
                $logo = '/uploads/' . $themes->logo;
            }
            if ($logo == '/uploads/') {
                $logo = '/image/logo.png';
            }
            if ($favicon == '/uploads/') {
                $favicon = '/image/favicon.ico';
            }
            $app_name = Config::get('app.website_title');
            // walk location
            $start_loc = WalkLocation::where('request_id', $request->id)->first();
            $reqloc = WalkLocation::where('request_id', $request->id)->orderBy('id', 'desc')->first();
            $title = 'Track ' . $user_name;

            return Response::json(array('success' => true,
                        'titl' => $title,
                        'logo' => $logo,
                        'favicon' => $favicon,
                        'app_name' => $app_name,
                        'cur_lat' => $reqloc->latitude,
                        'cur_lon' => $reqloc->longitude,
                        'prev_lat' => $start_loc->latitude,
                        'prev_lon' => $start_loc->longitude,
                        'track_id' => $id));
        } else {
            return Redirect::to('/');
        }
    }

    public function isUsername($username)
    {
        $company = Companies::where('username', '=', $username)->first();


        if ($company == null) {
            return false;
        } else {
            return true;
        }
    }

     public function registercompany()
    {
        if (!Session::has('language')) {
            Session::put('language', 'ar');
            $language = 'ar';
        }else{
            $language = Session::get('language');
            Log::info($language);
        }
        $page = 'website.company_registrationar';
        if($language=='en'){
            $page = 'website.company_registration';
            $finalstaustxt = 'Not Found!';
        }else{

            $finalstaustxt = 'غير موجود';
        }
        return View::make($page);
    }

     public function registercompanypost()
    {

        try {


            //Get the params from the ajax request
            $company_name = Request::get('company_name');


            $owner_name = Request::get('owner_name');
            $owner_id   = Input::file('owner_id')->getRealPath();

            $company_cr   = Input::file('company_cr')->getRealPath();
            $phone = Request::get('phone');
            $email = Request::get('email');

            $username = Request::get('username');
            $password = Request::get('password');
            $customer_support_phone = Request::get('customer_support_phone');


            $customer_support_email = Request::get('customer_support_email');
            $address = Request::get('address');
            $country = Request::get('country');

            // getting all country codes with full names

            $allCountryCodes = json_decode(file_get_contents("http://country.io/names.json"), true);
            // by default input field has no value.. so if no value set it to SA as SA is default Country
            //input value is set on change

            if(empty($country)){

                    $country = 'SA';

                    }


            // getting full country name from unicode SA = Saudi Arabia etc
             $finalCountry = $allCountryCodes[$country];
            
            $city = Request::get('city');
            $bank_name = Request::get('bank_name');
            $iban = Request::get('iban');

            $last4digits = substr($phone, -4); // last 4 digits of phone


            //is !(the given email already in the database)?
            if (!$this->isUsername($username)) {

                //check if email is not already registered
                if(Companies::where('email', $email)->count() == 0){

                Cloudder::upload($owner_id, 'owner_id'.$last4digits.'');

                $owner_id_url = Cloudder::getResult();

                Cloudder::upload($company_cr, 'company_cr'.$last4digits.'');

                $company_cr_url = Cloudder::getResult();

                $owner_id_url = $owner_id_url['url'];

                $company_cr_url       = $company_cr_url['url'];

                try {
                    //Create company object.....

                    $newCompany = new Companies();

                    $newCompany->company_name = $company_name;
                    $newCompany->owner_name = $owner_name;
                    $newCompany->customer_support_email = $customer_support_email;
                    $newCompany->customer_support_phone = $customer_support_phone;
                    $newCompany->owner_id = $owner_id_url;
                    $newCompany->company_cr = $company_cr_url;
                    $newCompany->bank_name = $bank_name;
                    $newCompany->iban = $iban;
                    $newCompany->username = $username;
                    $newCompany->email = $email;
                    $newCompany->phone = $phone;
                    $newCompany->password = Hash::make($password);
                    $newCompany->address = $address;
                    $newCompany->city = $city;
                    $newCompany->country = $finalCountry;
                    
                    $newCompany->secret = Hash::make($company_name);


                    //save!!
                    $newCompany->save();

                    $control_panel = ControlPanel::where('city', '=', 'All')->first();
                    $cities = City::all()->except(1);
                    foreach($cities as $city) {
                        $record = new ControlPanel;
                        $record->company_id = $newCompany->id;
                        $record->city = $city->name;
                        $record->to_be_pickedup = $control_panel->to_be_pickedup;
                        $record->reserved_to_pickup = $control_panel->reserved_to_pickup;
                        $record->pickedup = $control_panel->pickedup;
                        $record->delayed_created_by_supplier = $control_panel->delayed_created_by_supplier;
                        $record->in_route = $control_panel->in_route;
                        $record->in_warehouse = $control_panel->in_warehouse;
                        $record->max_delivery_attempts = $control_panel->max_delivery_attempts;
                        $record->delivery_attempt = $control_panel->delivery_attempt;
                        $record->cod_reception_money = $control_panel->cod_reception_money;
                        $record->reception_money_in_branches = $control_panel->reception_money_in_branches;
                        $record->reception_money_from_branches = $control_panel->reception_money_from_branches;
                        $record->cod_paid_to_supplier = $control_panel->cod_paid_to_supplier;
                        $record->returned_to_supplier = $control_panel->returned_to_supplier;
                        $record->opened_tickets = $control_panel->opened_tickets;
                        $record->opened_urgent_tickets = $control_panel->opened_urgent_tickets;
                        $record->check_to_be_pickedup = $control_panel->check_to_be_pickedup;
                        $record->check_reserved_to_pickup = $control_panel->check_reserved_to_pickup;
                        $record->check_pickedup = $control_panel->check_pickedup;
                        $record->check_created_by_supplier = $control_panel->check_created_by_supplier;
                        $record->check_delayed_created_by_supplier = $control_panel->check_delayed_created_by_supplier;
                        $record->check_in_route = $control_panel->check_in_route;
                        $record->check_in_warehouse = $control_panel->check_in_warehouse;
                        $record->check_delivery_attempt = $control_panel->check_delivery_attempt;
                        $record->check_cod_reception_money = $control_panel->check_cod_reception_money;
                        $record->check_reception_money_in_branches = $control_panel->check_reception_money_in_branches;
                        $record->check_reception_money_from_branches = $control_panel->check_reception_money_from_branches;
                        $record->check_cod_paid_to_supplier = $control_panel->check_cod_paid_to_supplier;
                        $record->check_returned_to_supplier = $control_panel->check_returned_to_supplier;
                        $record->save();
                    }


                    //done get and return the id to use as a session when user loggedin
                    return Redirect::to('/company/login')->with('success', 'Company Created Successfully !');
                } catch (Exception $e) {
                    Log::info($e->getMessage());
                    return Redirect::to('/registercompany')->with('error', 'Failed Due to error, Please try again.')->with('company_name', $company_name)->with('owner_name', $owner_name)->with('owner_id', $owner_id)->with('company_cr', $company_cr)->with('phone', $phone)->with('email', $email)->with('username', $username)->with('customer_support_phone', $customer_support_phone)->with('customer_support_email', $customer_support_email)->with('address', $address)->with('country', $country)->with('city', $city)->with('bank_name', $bank_name)->with('iban', $iban);
                }
            }else {

                 Log::info('dub');
                return Redirect::to('/registercompany')->with('error', 'Email already exists, Please try again.')->with('company_name', $company_name)->with('owner_name', $owner_name)->with('owner_id', $owner_id)->with('company_cr', $company_cr)->with('phone', $phone)->with('email', $email)->with('username', $username)->with('customer_support_phone', $customer_support_phone)->with('customer_support_email', $customer_support_email)->with('address', $address)->with('country', $country)->with('city', $city)->with('bank_name', $bank_name)->with('iban', $iban);

            }

            } else {
                //email already taken or exist in the system, forgot your password?
                Log::info('dub');
                return Redirect::to('/registercompany')->with('error', 'Username already exists, Please try again.')->with('company_name', $company_name)->with('owner_name', $owner_name)->with('owner_id', $owner_id)->with('company_cr', $company_cr)->with('phone', $phone)->with('email', $email)->with('username', $username)->with('customer_support_phone', $customer_support_phone)->with('customer_support_email', $customer_support_email)->with('address', $address)->with('country', $country)->with('city', $city)->with('bank_name', $bank_name)->with('iban', $iban);
            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
             return Redirect::to('/registercompany')->with('error', 'Failed Due to error, Please try again.')->with('company_name', $company_name)->with('owner_name', $owner_name)->with('owner_id', $owner_id)->with('company_cr', $company_cr)->with('phone', $phone)->with('email', $email)->with('username', $username)->with('customer_support_phone', $customer_support_phone)->with('customer_support_email', $customer_support_email)->with('address', $address)->with('country', $country)->with('city', $city)->with('bank_name', $bank_name)->with('iban', $iban);

        }

    }

    public function create_order() {
        $lang = Request::segment(2);
        if($lang == 'en')
            $page = "website.create_order";
        else 
            $page = "website.create_order_ar";
        return View::make($page);
    }

}
