<?php

use App\Classes\Curl;
use JD\Cloudder\Facades\Cloudder;

class HubAPIController extends \BaseController {

    public function undelivered_reasons() {
        if(Request::has('secret')) {
            $secret = Request::get('secret');
            $hub = Hub::where('secret', '=', $secret)->first();
            if(empty($hub)) {
                $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                goto response;
            }
        }
        else {
            $response_array = array('success' => false, 'error' => 'Secret required');
            goto response;
        }
        $reasons = UndeliveredReason::select(['id', 'english', 'arabic'])->where('id', '<>', '0')->get();
        $response_array = array('success' => true, 'reasons' => $reasons);
        response:
        return $response_array;
    }

    public function order_statuses() {
        if(Request::has('secret')) {
            $secret = Request::get('secret');
            $hub = Hub::where('secret', '=', $secret)->first();
            if(empty($hub)) {
                $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                goto response;
            }
        }
        else {
            $response_array = array('success' => false, 'error' => 'Secret required');
            goto response;
        }
        $reasons = OrderStatuses::select(['id', 'english', 'arabic'])->where('id', '<>', '0')->get();
        $response_array = array('success' => true, 'reasons' => $reasons);
        response:
        return $response_array;
    }

    public function companies() {
        if(Request::has('secret')) {
            $secret = Request::get('secret');
            $hub = Hub::where('secret', '=', $secret)->first();
            if(empty($hub)) {
                $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                goto response;
            }
        }
        else {
            $response_array = array('success' => false, 'error' => 'Secret required');
            goto response;
        }
        $reasons = Companies::select(['id', 'company_name', 'name_ar'])->get();
        $response_array = array('success' => true, 'reasons' => $reasons);
        response:
        return $response_array;
    }

    public function cities() {
        if(Request::has('secret')) {
            $secret = Request::get('secret');
            $hub = Hub::where('secret', '=', $secret)->first();
            if(empty($hub)) {
                $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                goto response;
            }
        }
        else {
            $response_array = array('success' => false, 'error' => 'Secret required');
            goto response;
        }
        $reasons = City::select(['id', 'name', 'name_ar'])->where('id', '<>', '1')->get();
        $response_array = array('success' => true, 'reasons' => $reasons);
        response:
        return $response_array;
    }

    public function update_status() {
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            if(Request::has('admin_id')) {
                $admin_id = Request::get('admin_id');
                $admin = Admin::where('id', '=', $admin_id)->first();
                if(empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Admin id required');
                goto response;
            }
            if(Request::has('waybill'))
                $waybill = Request::get('waybill');
            else {
                $response_array = array('success' => false, 'error' => 'Waybill required');
                goto response;
            }
            if(Request::has('new_status'))
                $to = Request::get('new_status');
            else {
                $response_array = array('success' => false, 'error' => 'New status required');
                goto response;
            }
            $company_id = -1;
            if(Request::has('company_id')) {
                $company_id = Request::get('company_id');
                if(Companies::where('id', '=', $company_id)->count() == 0) {
                    $response_array = array('success' => false, 'error' => 'Wrong company id required');
                    goto response;
                }
            }
            $captainid = 0;
            if(Request::has('captain_id')) {
                $captainid = Request::get('captain_id');
                $captain = Walker::where('id', '=', $captainid)->first();
                if(empty($captain)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }
            }
            $undelivered_reason_id = 0; 
            if(Request::has('undelivered_reason_id')) {
                $undelivered_reason_id = Request::get('undelivered_reason_id');
                $undelivered_reason = UndeliveredReasons::where('id', '=', $undelivered_reason_id)->first();
                if(empty($undelivered_reason_id)) {
                    $response_array = array('success' => false, 'error' => "undelivered_reason_id does not exist");
                    goto response;
                }
            }
            $update_status = new UpdateStatus($waybill, $to, $captainid, $admin_id, $company_id, 0, $undelivered_reason_id, $undelivered_reason_id);
            $response_array = $update_status->execute();
            if($response_array['success']) 
                $response_array['shipment'] = DeliveryRequest::where('jollychic', '=', $waybill)->select([
                    'jollychic as waybill',
                    'order_number', 
                    'order_reference',
                    'status',
                    'd_quantity as quantity',
                    'd_weight as weight',
                    'd_description as description',
                    'cash_on_delivery',
                    'receiver_name', 
                    'receiver_phone',
                    'receiver_phone2',
                    'receiver_email',
                    'pincode',
                    'd_state as state',
                    'd_city as city',
                    'd_district as district',
                    'planned_walker',
                    'confirmed_walker',
                    'created_at as created_by_supplier_date',
                    DB::raw('CASE WHEN sort_date = "0000-00-00 00:00:00" THEN null ELSE sort_date END as sort_date'),
                    'new_shipment_date as in_route_date',
                    'signin_date',
                    'scheduled_shipment_date',
                    'dropoff_timestamp as delivery_date',
                    DB::raw('CASE WHEN return_to_supplier_date = "0000-00-00 00:00:00" THEN null ELSE return_to_supplier_date END as return_to_supplier_date'),
                ])->first();
        }
        catch (Exception $e) {
            return $e->getLine() . ' <br/> ' . $e->getMessage();
            $response_array = array('success' => false, 'erorr' => 'Failed due to an error, please try again.');
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }
    
    public function captain_registration() {
        try {
            // mandatory
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            // mandatory
            if(Request::has('first_name'))
                $first_name = Request::get('first_name');
            else {
                $response_array = array('success' => false, 'error' => 'First name required');
                goto response;
            }
            // mandatory
            if(Request::has('last_name'))
                $last_name = Request::get('last_name');
            else {
                $response_array = array('success' => false, 'error' => 'Last name required');
                goto response;
            }
            // mandatory
            if(Request::has('gender')) {
                $gender = Request::get('gender');
                if(!in_array(strtolower($gender), array('male', 'female'))) {
                    $response_array = array('success' => false, 'error' => 'Gender is not valid');
                    goto response;
                }
                if(strtolower($gender) == 'male') $gender = 'Male';
                else $gender = 'Female';
            }
            else {
                $response_array = array('success' => false, 'error' => 'Gender required');
                goto response;
            }
            // mandatory
            if(Request::has('phone')) {
                $phone = Request::get('phone');
                $saudi_numbers = '/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/';
                if(!preg_match($saudi_numbers, $phone)) {
                    $response_array = array('success' => false, 'error' => 'Phone is not valid');
                    goto response;
                }
                $phone = '966' . substr($phone, -9);
            }
            else {
                $response_array = array('success' => false, 'error' => 'Phone required');
                goto response;
            }
            // mandatory
            if(Request::has('email')) {
                if(!filter_var(Request::get('email'), FILTER_VALIDATE_EMAIL)) {
                    $response_array = array('success' => false, 'error' => 'Email is not valid');
                    goto response;
                }
                $email = Request::get('email');
            }
            else {
                $response_array = array('success' => false, 'error' => 'Email required');
                goto response;
            }
            // mandatory
            if(Input::hasFile('vehicle_registration_scan')) 
                $vehicle_registration_scan = Input::file('vehicle_registration_scan')->getRealPath();
            else {
                $response_array = array('success' => false, 'error' => 'Vehicle registration scan required');
                goto response;
            }
            // mandatory
            if(Input::hasFile('vehicle_front_scan')) 
                $vehicle_front_scan = Input::file('vehicle_front_scan')->getRealPath();
            else { 
                $response_array = array('success' => false, 'error' => 'Vehicle front scan required');
                goto response;
            }
            // mandatory
            if(Input::hasFile('captain_iqama_scan'))
                $captain_iqama_scan = Input::file('captain_iqama_scan')->getRealPath();
            else {
                $response_array = array('success' => false, 'error' => 'Captain iqama scan required');
                goto response;
            }
            // mandatory
            if(Input::hasFile('captain_license_scan'))
                $captain_license_scan = Input::file('captain_license_scan')->getRealPath();
            else {
                $response_array = array('success' => false, 'error' => 'Captain license scan required');
                goto response;
            }
            $validatorImage = Validator::make(
                array(
                    'vehicle_registration_scan' => Input::file('vehicle_registration_scan'),
                    'vehicle_front_scan' => Input::file('vehicle_front_scan'),
                    'captain_iqama_scan' => Input::file('captain_iqama_scan'),
                    'captain_license_scan' => Input::file('captain_license_scan'),
                ),
                array(
                    'vehicle_registration_scan' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
                    'vehicle_front_scan' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
                    'captain_iqama_scan' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
                    'captain_license_scan' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
                )
            );
            if($validatorImage->fails()) {
                $response_array = array('success' => false, 'error' => 'Error in uploading images', 'errors' => $validatorImage->messages());
                goto response;
            }
            if(Request::has('vehicle_registration_expiry'))
                $vehicle_registration_expiry = Request::get('vehicle_registration_expiry');
            else {
                $response_array = array('success' => false, 'error' => 'Vehicle registration expiry required');
                goto response;
            }
            // mandatory
            if(Request::has('license_expiry_date'))
                $license_expiry_date = Request::get('license_expiry_date');
            else { 
                $response_array = array('success' => false, 'error' => 'License expiry date required');
                goto response;
            }
            // mandatory
            if(Request::has('nationality'))
                $nationality = Request::get('nationality');
            else {
                $response_array = array('success' => false, 'error' => 'Nationality required');
                goto response;
            }
            // mandatory
            if(Request::has('city'))
                $city = Request::get('city');
            else {
                $response_array = array('success' => false, 'error' => 'City required');
                goto response;
            }
            // mandatory
            if(Request::has('district'))
                $district = Request::get('district');
            else {
                $response_array = array('success' => false, 'error' => 'District required');
                goto response;
            }
            // mandatory
            if(Request::has('address'))
                $address = Request::get('address');
            else {
                $response_array = array('success' => false, 'error' => 'Address required');
                goto response;
            }
            // mandatory
            if(Request::has('latitude'))
                $latitude = Request::get('latitude');
            else {
                $response_array = array('success' => false, 'error' => 'Latitude required');
                goto response;
            } 
            // mandatory
            if(Request::has('longitude'))
                $longitude = Request::get('longitude');
            else {
                $response_array = array('success' => false, 'error' => 'Longitude required');
                goto response;
            }
            // mandatory
            if(Request::has('password'))
                $password = Request::get('password');
            else {
                $response_array = array('success' => false, 'error' => 'Password required');
                goto response;
            }

            return DB::transaction(function () use ($first_name, $last_name, $gender,  $phone, $email, $vehicle_registration_scan, $vehicle_front_scan, $captain_iqama_scan, $captain_license_scan, $vehicle_registration_expiry, $license_expiry_date, $nationality, $city, $district, $address, $latitude, $longitude, $password) {
                $activation_code = uniqid();
                $walker = new Walker;
                $walker->coming_from = 'Barq';
                $walker->first_name = $first_name;
                $walker->last_name = $last_name;
                $walker->email = $email;
                $walker->phone = $phone;
                $walker->city = $city;
                $walker->bb_token = generate_token();
                $walker->nationality = $nationality;
                $walker->activation_code = $activation_code;
                $walker->email_activation = 0;
                $walker->sex = $gender;
                $walker->district = $district;
                $walker->address = $address;
                $walker->address_latitude = $latitude;
                $walker->address_longitude = $longitude;
                $walker->car_registration_expiry = $vehicle_registration_expiry;
                $walker->license_expiry_date = $license_expiry_date;
                $walker->login_by = 'API';
                $walker->device_type = 'API';
                $walker->device_token = '1234';
                $walker->is_available = 1;
                $walker->password = Hash::make($password);
                $walker->token = generate_token();
                $walker->token_expiry = generate_expiry();
                $walker->type = 5;
                $walker->save();
    
    
                Cloudder::upload($vehicle_registration_scan, 'vehicle_registration_' . $walker->id);
                $vehicle_registration_url = Cloudder::getResult();
                
                Cloudder::upload($vehicle_front_scan, 'car_front_'.$walker->id.'');
                $vehicle_front_url = Cloudder::getResult();
                
                Cloudder::upload($captain_iqama_scan, 'id_'.$walker->id.'');
                $captain_iqama_url = Cloudder::getResult();
                
                Cloudder::upload($captain_license_scan, 'driving_license_'.$walker->id.'');
                $captain_license_url = Cloudder::getResult();
    
                $vehicle_registration_url = $vehicle_registration_url['url'];
                $vehicle_front_url        = $vehicle_front_url['url'];
                $captain_iqama_url        = $captain_iqama_url['url'];
                $captain_license_url      = $captain_license_url['url'];
    
                $walker->vehicle_registration_scan = $vehicle_registration_url; 
                $walker->vehicle_front_scan = $vehicle_front_url; 
                $walker->captain_iqama_scan = $captain_iqama_url; 
                $walker->captain_license_scan = $captain_license_url; 
                $walker->save();
    
                // if (Input::has('type') != NULL) {
                //     $ke = Input::get('type');
                //     $proviserv = ProviderServices::where('provider_id', $walker->id)->first();
                //     if ($proviserv != NULL) {
                //         DB::delete("delete from walker_services where provider_id = '" . $walker->id . "';");
                //     }
                //     $base_price = Input::get('service_base_price');
                //     $service_price_distance = Input::get('service_price_distance');
                //     $service_price_time = Input::get('service_price_time');
                //     Log::info('type = ' . print_r(Input::get('type'), true));
                //     $cnkey = count(Input::get('type'));
                //     Log::info('cnkey = ' . print_r($cnkey, true));
                //     for ($i = 1; $i <= $cnkey; $i++) {
                //         $key = Input::get('type');
                //         $prserv = new ProviderServices;
                //         $prserv->provider_id = $walker->id;
                //         $prserv->type = $key;
                //         Log::info('key = ' . print_r($key, true));
                //         if (Input::has('service_base_price')) {
                //             $prserv->base_price = $base_price[$i - 1];
                //         } else {
                //             $prserv->base_price = 0;
                //         }
                //         if (Input::has('service_price_distance')) {
                //             $prserv->price_per_unit_distance = $service_price_distance[$i - 1];
                //         } else {
                //             $prserv->price_per_unit_distance = 0;
                //         }
                //         if (Input::has('service_price_distance')) {
                //             $prserv->price_per_unit_time = $service_price_time[$i - 1];
                //         } else {
                //             $prserv->price_per_unit_distance = 0;
                //         }
                //         $prserv->save();
                //     }
                // }
                $settings = Settings::where('key', 'admin_email_address')->first();
                $admin_email = $settings->value;
                $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($walker->first_name . " " . $walker->last_name), 'web_url' => web_url());
                $subject = "Welcome to " . ucwords(Config::get('app.website_title')) . ", " . ucwords($walker->first_name . " " . $walker->last_name) . "";
                email_notification($walker->id, 'walker', $pattern, $subject, 'walker_register', "imp");
                $response_array = array('success' => true, 'message' => 'Captain created successfully');
            });
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function get_all_admins() { // return all admins for one hub
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            $admins = Admin::where('hub_id', '=', $hub->id)
            ->select([
                'id',
                'username',
                'email',
                'default_city as city'
            ])
            ->get();
            $response_array = array('success' => true, 'admins' => $admins);
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response: 
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function get_all_hubs() { // return all hubs that admin have privileges for
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            if(Request::has('admin_id')) {
                $admin_id = Request::get('admin_id');
                $admin = Admin::where('id', '=', $admin_id)->first();
                if(empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Admin id required');
                goto response;
            }
            $hubs = Hub::leftJoin('hub_privilege', 'hub.id', '=', 'hub_privilege.hub_id')
            ->where('hub_privilege.admin_id', '=', $admin->id)
            ->where('hub.id', '<>', '0')
            ->select([
                'hub.id',
                'name',
                'name_ar',
                'manager_id',
                'country',
                'city',
                'district',
                'address',
                'phone',
                'latitude',
                'longitude',
            ])
            ->get();
            $response_array = array('success' => true, 'hubs' => $hubs);
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response: 
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function schedule_shipments() {
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            if(Request::has('admin_id')) {
                $admin_id = Request::get('admin_id');
                $admin = Admin::where('id', '=', $admin_id)->first();
                if(empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Admin id required');
                goto response;
            }
            if(Request::has('waybills')) { 
                $waybills = explode(',', Request::get('waybills'));
                $waybills_str = str_replace(',', "','", Request::get('waybills'));
            }
            else {
                $response_array = array('success' => false, 'error' => 'Date required');
                goto response;
            }
            if(Request::has('date')) {
                $date = Request::get('date');
                if(date('Y-m-d', strtotime($date)) != $date) {
                    $response_array = array('success' => false, 'error' => 'Date format is \'YYYY-MM-DD\'');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Date required');
                goto response;
            }
            $response_array = DB::transaction(function() use ($hub, $waybills, $waybills_str, $date, $admin_id) {
                $query = "
                    update `delivery_request` 
                        left join `control_panel` on `delivery_request`.`company_id` = `control_panel`.`company_id` and (delivery_request.d_city = control_panel.city or delivery_request.d_city in (select distinct SUBSTRING_INDEX(district, \"+\", 1) as \"city\" from districts where districts.city = control_panel.city))   
                        left join `order_statuses` on `delivery_request`.`status` = `order_statuses`.`id` 
                    set `scheduled_shipment_date` = '$date', `undelivered_reason` = 6 
                    where (`jollychic` in ('$waybills_str') or `order_number` in ('$waybills_str')) 
                        and (d_city = '$hub->city' or d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = '$hub->city'))
                        and `status` in (1, 2, 3) 
                        and DATEDIFF(now(), new_shipment_date) <= control_panel.returned_to_supplier 
                        and num_faild_delivery_attempts <= control_panel.max_delivery_attempts
                ";
                $will_be_updated = DeliveryRequest::leftJoin('control_panel', function($join) {
                    $join->on('delivery_request.company_id', '=', 'control_panel.company_id');
                    $join->on(DB::raw('(delivery_request.d_city = control_panel.city or delivery_request.d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) as "city" from districts where districts.city = control_panel.city))'),DB::raw(''),DB::raw(''));
                })
                ->whereRaw("(jollychic in ('$waybills_str') or order_number in ('$waybills_str')) ")
                ->whereIn('status', array(1,2,3))
                ->whereRaw("(d_city = '$hub->city' or d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = '$hub->city'))")
                ->whereRaw("DATEDIFF(now(), new_shipment_date) <= control_panel.returned_to_supplier")
                ->whereRaw("num_faild_delivery_attempts <= control_panel.max_delivery_attempts")
                ->where(function($query) use($date) {
                    $query->where('scheduled_shipment_date', '<>', $date)->orWhereNull('scheduled_shipment_date');
                })
                ->select(['jollychic', 'order_number', 'status', 'delivery_request.company_id', 'd_city'])
                ->get();
                foreach($will_be_updated as $one_record) {
                    $order_history = new OrdersHistory;
                    $order_history->waybill_number = $one_record->jollychic;
                    $order_history->order_number = $one_record->order_number;
                    $order_history->status = $one_record->status;
                    $order_history->company_id = $one_record->company_id;
                    $order_history->admin_id = $admin_id;
                    $order_history->notes = "Consignee asked to reschedule the shipment in $date";
                    $order_history->save();
                }
                $updated_items_count = DB::update($query);
                $success_items = DeliveryRequest::leftJoin('companies', 'delivery_request.company_id', '=', 'companies.id')
                ->leftJoin('hub', 'delivery_request.hub_id', '=', 'hub.id')
                ->leftJoin('order_statuses', 'delivery_request.status', '=', 'order_statuses.id')
                ->where('scheduled_shipment_date', '=', $date)
                ->where(function($query) use($waybills) {
                    $query->whereIn('jollychic', $waybills)->orWhereIn('order_number', $waybills);
                })
                ->select([
                    'jollychic as waybill',
                    'order_number',
                    'order_reference',
                    'company_name',
                    'd_city',
                    'hub.name as hub_name',
                    'receiver_name',
                    'receiver_phone',
                    'receiver_phone2',
                    'd_address',
                    'pincode',
                    'cash_on_delivery',
                    'scheduled_shipment_date',
                    'undelivered_reason',
                    'num_faild_delivery_attempts',
                    'order_statuses.english as status',
                    DB::raw("DATEDIFF(now(), new_shipment_date) 'age'")
                ])
                ->get();
                $failed_items = DeliveryRequest::leftJoin('control_panel', function($join) {
                    $join->on('delivery_request.company_id', '=', 'control_panel.company_id');
                    $join->on(DB::raw('(delivery_request.d_city = control_panel.city or delivery_request.d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) as "city" from districts where districts.city = control_panel.city))'),DB::raw(''),DB::raw(''));
                })
                ->leftJoin('order_statuses', 'delivery_request.status', '=', 'order_statuses.id')
                ->where(function($query) use($date) {
                    $query->where('scheduled_shipment_date', '<>', $date)->orWhereNull('scheduled_shipment_date');
                })
                ->where(function($query) use($waybills) {
                    $query->whereIn('jollychic', $waybills)->orWhereIn('order_number', $waybills);
                })
                ->select([
                    'jollychic as waybill', 
                    DB::raw(
                        "
                        CASE
                            WHEN d_city <> '$hub->city' and d_city not in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = '$hub->city') THEN 'You are not allowed schedule shipments not in $hub->city'
                            WHEN status not in (1, 2, 3) THEN CONCAT('You can not reschedule ', order_statuses.english, ' shipments')
                            WHEN DATEDIFF(now(), new_shipment_date) > control_panel.returned_to_supplier and num_faild_delivery_attempts > control_panel.max_delivery_attempts THEN 'Exceeded allowed age and maximum delivery attempts'
                            WHEN DATEDIFF(now(), new_shipment_date) > control_panel.returned_to_supplier THEN 'Exceeded allowed age'
                            WHEN num_faild_delivery_attempts > control_panel.max_delivery_attempts THEN 'Exceeded allowed delivery attempts'
                        END as 'error'
                        "
                    )
                ])
                ->get();
                return $response_array = array(
                    'success' => true,
                    // 'updated_items_count' => $updated_items_count,
                    'success_items' => $success_items,
                    'failed_items' => $failed_items
                );
            });
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response: 
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function get_captain_shipments() {
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            if(Request::has('captain_id')) 
                $captain_id = Request::get('captain_id');
            else {
                $response_array = array('success' => false, 'error' => 'Captain id required');
                goto response;
            }
            $shipments = DeliveryRequest::where('planned_walker', '=', $captain_id)
                ->where('confirmed_walker', '=', $captain_id)
                ->where('status', '=', '4')
                ->get();
            $response_array = array('success' => true, 'shipments' => $shipments);
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response: 
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function shipmentsExport() {
        try {
            if(Request::has('secret')) {
                $secret = Request::get('secret');
                $hub = Hub::where('secret', '=', $secret)->first();
                if(empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'Wrong secret. Please contact admin');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Secret required');
                goto response;
            }
            if(Request::has('admin_id')) {
                $admin_id = Request::get('admin_id');
                $admin = Admin::where('id', '=', $admin_id)->first();
                if(empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
            }
            else {
                $response_array = array('success' => false, 'error' => 'Admin id required');
                goto response;
            }
            $flag = 0;
            $companies = 'all';
            if(Request::has('companies')) {
                $companies = explode(',', Request::get('companies'));
                $flag = 1;
            }
            $cities = 'all';
            if(Request::has('cities')) {
                $cities = explode(',', Request::get('cities'));
                $flag = 1;
            }
            $hubs = 'all';
            if(Request::has('hub')) {
                $hubs = explode(',', Request::get('hub'));
                $flag = 1;
            }
            $statuses = 'all';
            if(Request::has('statuses')) {
                $statuses = explode(',', Request::get('statuses'));
                $flag = 1;
            }
            $date_type = 'all';
            if(Request::has('date_type')) {
                $date_type = Request::get('date_type');
                if($date_type == 'created_by_supplier_date') $date_type = 'created_at';
                else if($date_type == 'sort_date');
                else if($date_type == 'in_route_date') $date_type = 'new_shipment_date';
                else if($date_type == 'signin_date');
                else if($date_type == 'scheduled_shipment_date');
                else if($date_type == 'delivery_date') $date_type = 'dropoff_timestamp';
                else if($date_type == 'return_to_supplier_date');
                else {
                    $response_array = array('success' => false, 'error' => 'Date type is not valid');
                    goto response;
                }
                $flag = 1;
            }
            $datetype_from = 'all';
            if(Request::has('datetype_from')) {
                $datetype_from = Request::get('datetype_from');
                if(date('Y-m-d', strtotime($datetype_from)) != $datetype_from) {
                    $response_array = array('success' => false, 'error' => 'Date format is \'YYYY-MM-DD\'');
                    goto response;
                }
                $flag = 1;
            }
            $datetype_to = 'all';
            if(Request::has('datetype_to')) {
                $datetype_to = Request::get('datetype_to');
                if(date('Y-m-d', strtotime($datetype_to)) != $datetype_to) {
                    $response_array = array('success' => false, 'error' => 'Date format is \'YYYY-MM-DD\'');
                    goto response;
                }
                $flag = 1;
            }
            if($date_type == 'all' && ($datetype_from != 'all' || $datetype_to != 'all')) {
                $response_array = array('success' => false, 'error' => 'Date type is required to filter by date [from, to]');
                goto response;
            }
            if($date_type != 'all' && ($datetype_from == 'all' || $datetype_to == 'all')) {
                $response_array = array('success' => false, 'error' => '[from, to] pair must be provided with date type');
                goto response;
            }
            if($flag == 0) {
                $response_array = array('success' => false, 'error' => 'At least one filter is required');
                goto response;
            }
            $shipments = DeliveryRequest::leftJoin('subcity', 'subcity.subcity', '=', 'delivery_request.d_city');
            if($companies != 'all') {
                $shipments = $shipments->whereIn('company_id', $companies);
            }
            if($cities != 'all') {
                $shipments = $shipments->whereIn('subcity.city', $cities);
            }
            if($hubs != 'all') {
                $shipments = $shipments->whereIn('hub_id', $hubs);
            }
            if($statuses != 'all') {
                $shipments = $shipments->whereIn('status', $statuses);
            }
            if($date_type != 'all') {
                $shipments = $shipments->whereBetween(DB::raw("DATE(delivery_request.$date_type)"), array($datetype_from, $datetype_to));
            }
            $shipments = $shipments->select([
                'delivery_request.jollychic as waybill', 
                'delivery_request.order_number', 
                DB::raw("ifnull(delivery_request.order_reference, '') as order_reference"),
                'delivery_request.company_id',
                'delivery_request.sender_name',
                'delivery_request.d_city as receiver_city',
                'delivery_request.hub_id',
                'delivery_request.receiver_phone',
                'delivery_request.receiver_phone2',
                'delivery_request.d_address as street_address', 
                'delivery_request.d_district as receiver_district',
                'delivery_request.cash_on_delivery',
                'delivery_request.pincode',
                'delivery_request.confirmed_walker',
                'delivery_request.created_at as created_by_supplier_date',
                'delivery_request.sort_date as sort_date',
                DB::raw('DATE(delivery_request.new_shipment_date) as in_route_date'),
                'delivery_request.signin_date as signin_date',
                DB::raw("ifnull(delivery_request.scheduled_shipment_date, '') as scheduled_shipment_date"),
                DB::raw("ifnull(delivery_request.dropoff_timestamp, '') as delivery_date"),
                'delivery_request.return_to_supplier_date as return_to_supplier_date',
                DB::raw('DATEDIFF(now(),delivery_request.new_shipment_date) as age'),
            ])
            ->get();
            $response_array = array('success' => true, 'shipments' => $shipments);
        }
        catch (Exception $e) {
            return $e->getLine() . ' ' . $e->getMessage();
            $response_array = array('success' => false, 'error' => 'Failed due to an error. Please contact admin');
        }
        response: 
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function captainAccounting()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                if (isset($captaindata)) {
                    $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                    $captain_info .= " 0";
                    $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                    $orders = DeliveryRequest::select(['jollychic', 'cash_on_delivery', 'is_money_received', 'hub.name as hub_name'])->
                    leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')->where('confirmed_walker', '=', $captainid)
                        ->where('is_money_received', '=', 0)->where('status', '=', 5)->where('hub_id', '=', $hub->id)->orderBy('company_id', 'asc')->orderBy('hub_id', 'asc')->get();


                    $pickup_orders = DeliveryRequest::select(['jollychic', 'cash_on_pickup', 'is_pickup_cash_received', 'hub.name as hub_name'])->
                    leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')->where('pickup_walker', '=', $captainid)
                        ->where('is_pickup_cash_received', '=', 0)->where('hub_id', '=', $hub->id)->get();

                    $deliveredInfoQuery = 'select sum(cash_on_delivery) DeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 5 and is_money_received = 0 ';
                    $deliveredInfo = DB::select(DB::raw($deliveredInfoQuery));
                    $undeliveredInfoQuery = 'select sum(cash_on_delivery) UndeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 6';
                    $undeliveredInfo = DB::select(DB::raw($undeliveredInfoQuery));
                    $withCaptainQuery = 'select sum(cash_on_delivery) withCaptain,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 4';
                    $withCaptain = DB::select(DB::raw($withCaptainQuery));
                    $deliveredAmount = isset($deliveredInfo[0]->DeliveredAmount) ? round($deliveredInfo[0]->DeliveredAmount, 2) : 0;
                    $deliveredPieces = isset($deliveredInfo[0]->Pieces) ? $deliveredInfo[0]->Pieces : 0;
                    $undeliveredAmount = isset($undeliveredInfo[0]->UndeliveredAmount) ? round($undeliveredInfo[0]->UndeliveredAmount, 2) : 0;
                    $undeliveredPieces = isset($undeliveredInfo[0]->Pieces) ? $undeliveredInfo[0]->Pieces : 0;
                    $withCaptainAmount = isset($withCaptain[0]->withCaptain) ? round($withCaptain[0]->withCaptain, 2) : 0;
                    $withCaptainPieces = isset($withCaptain[0]->Pieces) ? $withCaptain[0]->Pieces : 0;
                    $pickedupPieces = DeliveryRequest::where('status', '=', '-1')->where('pickup_walker', '=', $captainid)->count();

                    /// update all captains page's info
                    $this->updatecaptainid($captainid);

                    $response_array = array('success' => true, 'captain_info' => $captain_info, 'captain_id' => $captainid, 'deliveredAmount' => $deliveredAmount,
                        'deliveredPieces' => $deliveredPieces, 'undeliveredAmount' => $undeliveredAmount, 'undeliveredPieces' => $undeliveredPieces, 'withCaptainAmount' => $withCaptainAmount,
                        'withCaptainPieces' => $withCaptainPieces, 'pickedupPieces' => $pickedupPieces,
                        'delivered_orders' => $orders, 'pickup_orders' => $pickup_orders);
                } else {
                    $response_array = array('success' => false, 'error' => "Captain does not exist");
                    goto response;
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function receivedDeliverdMoneyFromCaptain()
    {
        DB::beginTransaction();
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');
            $waybills = Request::input('waybills');
            $amountReceived = Request::get('amount_received');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                    'waybills' => $waybills,
                    'amount_received' => $amountReceived,
                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
                'waybills' => 'required',
                'amount_received' => 'required',
            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $ids = explode(',', $waybills);

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                $jollychic_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('status', '=', 5)->where('is_money_received', '=', 0)
                    ->where('hub_id', '=', $hub->id)->where('confirmed_walker', '=', $captainid)->get(['jollychic']);

                if (count($jollychic_ids) == 0) {
                    $response_array = array('success' => false, 'error' => 'Please select delivered shipments waybill forwhich amount not received');
                    goto response;
                }

                $waybills = implode('\',\'', $ids);
                $waybills = '\'' . $waybills . '\'';

                $cashondeliveryquery = 'SELECT sum(cast(cash_on_delivery as decimal(12,2)) ) as amountsumed FROM delivery_request
                where confirmed_walker = ' . $captainid . ' and jollychic in (' . $waybills . ') and hub_id= ' . $hub->id . ' and status=5 and is_money_received = 0';
                $amountsumed = DB::select(DB::raw($cashondeliveryquery));

                $amount = strval($amountsumed[0]->amountsumed);
                if ($amountReceived == $amount) {
                    $company_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('confirmed_walker', '=', $captainid)->where('status', '=', 5)
                        ->where('is_money_received', '=', 0)->where('hub_id', '=', $hub->id)->distinct()->orderBy('company_id', 'asc')->get(['company_id']);
                    $orders = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                        ->whereIn('jollychic', $ids)->where('status', '=', 5)->where('is_money_received', '=', 0)->where('hub_id', '=', $hub->id)
                        ->where('confirmed_walker', '=', $captainid)->orderBy('company_id', 'asc')
                        ->get(['jollychic', 'd_district', 'company_id', 'hub_id', 'cash_on_delivery', 'companies.delivery_fee as delivery_fee', 'companies.delivery_fee2 as delivery_fee2',
                            'companies.delivery_fee3 as delivery_fee3', 'd_city']);
                    $j = 0;
                    $length = sizeof($orders);
                    $subcities = array("Ja'araneh", "Rabigh", "Mastura", "Asfan", "Zahban", "Khulais", "Saaber",
                        "At Taniem", "Jumum", "Rania", "Khurma", "Alhada", "Turba", "Laith", "Ashayrah", "Kharj",
                        "Amaq", "Birk", "Nimra", "Qunfudah", "Nwariah");
                    foreach ($company_ids as $companyid) {
                        $amountReceivedCompany = 0;
                        $captainAccountHistory = new CaptainAccountHistory();

                        $captainAccountHistory->captain_id = $captainid;
                        $captainAccountHistory->admin_id = $admin_id;
                        $captainAccountHistory->company_id = $companyid['company_id'];
                        $captainAccountHistory->hub_id = $hub->id;
                        $captainAccountHistory->debit = 0;
                        $captainAccountHistory->save();
                        $totalCaptainAmount = 0;
                        Log::info('length access' . $length);
                        for ($i = $j; $i < $length; $i++) {
                            if ($orders[$i]->company_id != $companyid['company_id']) {
                                $j = $i;
                                Log::info('i break' . $i);
                                break;
                            }
                            $flag = DeliveryRequest::where('confirmed_walker', '=', $captainid)
                                ->where('jollychic', $orders[$i]->jollychic)
                                ->where('is_money_received', '=', 0)
                                ->update(array('is_money_received' => 1));
                            if ($flag > 0) {
                                $d_city = $orders[$i]->d_city;
                                if (in_array($d_city, $subcities)) {
                                    $delivery_fee = 10;
                                } else {
                                    $maincity = DB::select("SELECT DISTINCT city from districts where city in('Riyadh','Baha','Qunfudah','Gizan') and SUBSTRING_INDEX(district, '+', 1) = '$d_city'");
                                    If (!empty($maincity) && $maincity[0]->city == 'Riyadh') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Baha') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Qunfudah') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Gizan') {
                                        $delivery_fee = 10;
                                    } else {
                                        $d_district = $orders[$i]->d_district;
                                        $groupedcity = DB::select("SELECT DISTINCT city from districts where  SUBSTRING_INDEX(district, '+', 1) = '$d_city'");

                                        if (empty($groupedcity)) {
                                            $isRemote = Districts::select('is_remote')->where('city', '=', $d_city)->where('district', 'like', '%' . $d_district)
                                                ->orWhere('district_ar', 'like', '%' . $d_district)->first();
                                            if (!empty($isRemote) && $isRemote->is_remote == 1) {
                                                $delivery_fee = 10;
                                            } else {
                                                $delivery_fee = 9;
                                            }
                                        } else {
                                            $groupedcityName = $groupedcity[0]->city;
                                            $isRemote = Districts::select('is_remote')->where('city', '=', $groupedcityName)->where('district', 'like', '%' . $d_district)
                                                ->orWhere('district_ar', 'like', '%' . $d_district)->first();

                                            if (!empty($isRemote) && $isRemote->is_remote == 1) {
                                                $delivery_fee = 10;
                                            } else {
                                                $delivery_fee = 9;
                                            }
                                        }

                                    }
                                }
                                $captainAccount = new CaptainAccount();
                                $captainAccount->waybill = $orders[$i]->jollychic;
                                $captainAccount->captain_id = $captainid;
                                $captainAccount->delivery_fee = $delivery_fee;
                                $totalCaptainAmount += $delivery_fee;
                                $amountReceivedCompany += $orders[$i]->cash_on_delivery;
                                $captainAccount->captain_account_history_id = $captainAccountHistory->id;
                                $captainAccount->hub_id = $hub->id;
                                $captainAccount->type = 1;
                                if ($orders[$i]->cash_on_delivery > 0) {
                                    $captainAccount->payment_method = 1;
                                    $captainAccount->amount = $orders[$i]->cash_on_delivery;
                                } else {
                                    $captainAccount->payment_method = 0;
                                    $captainAccount->amount = 0;
                                }
                                $captainAccount->admin_id = $admin_id;
                                $captainAccount->cash_transfer = round(($orders[$i]->cash_on_delivery), 2);
                                $captainAccount->save();
                            }

                        }
                        $captainAccountHistory = CaptainAccountHistory::find($captainAccountHistory->id);
                        $captainAccountHistory->credit = $amountReceivedCompany;
                        $captainAccountHistory->cash_credit = round($amountReceivedCompany, 2);
                        $captainAccountHistory->captain_amount = $totalCaptainAmount;
                        $captainAccountHistory->save();
                    }

                    $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                    $captain_info .= " 0";
                    $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);

                    $response_array = array(
                        'success' => true,
                        'captaindata' => $captain_info,
                        'amountReceived' => $amount,
                        'message' => $amount . ' SAR Received from captain : ' . $captain_info);
                    $this->updatecaptainid($captainid);
                    Log::info('commited');
                    DB::commit();

                } else {
                    $this->updatecaptainid($captainid);
                    $response_array = array(
                        'success' => false,
                        'error' => 'Amount Received from Captain is not equal to total of cash on delivery of delivered orders.Please try again with ' . $amount);
                }

                $this->updatecaptainid($captainid);
            }
        } catch (Exception $e) {
            DB::rollback();
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatePickupMoneyReceivedFromCaptain()
    {
        DB::beginTransaction();
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');
            $waybills = Request::input('waybills');
            $amountReceived = Request::get('amount_received');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                    'waybills' => $waybills,
                    'amount_received' => $amountReceived,
                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
                'waybills' => 'required',
                'amount_received' => 'required',
            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $ids = explode(',', $waybills);

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                $jollychic_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('is_pickup_cash_received', '=', 0)
                    ->where('pickup_walker', '=', $captainid)->get(['jollychic']);

                if (count($jollychic_ids) == 0) {
                    $response_array = array('success' => false, 'error' => 'Please select Pickup shipments waybill forwhich Cash on Pickup not received');
                    goto response;
                }

                $waybills = implode('\',\'', $ids);
                $waybills = '\'' . $waybills . '\'';
                $cash_on_pickup = 'SELECT sum(cast(cash_on_pickup as decimal(12,2)) ) as amountsumed FROM delivery_request
                where pickup_walker = ' . $captainid . ' and jollychic in (' . $waybills . ') and is_pickup_cash_received = 0';
                $amountsumed = DB::select(DB::raw($cash_on_pickup));
                $amount = strval($amountsumed[0]->amountsumed);
                if ($amountReceived == $amount) {
                    $company_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('is_pickup_cash_received', '=', 0)
                        ->where('pickup_walker', '=', $captainid)->distinct()->orderBy('company_id', 'asc')->get(['company_id']);
                    $orders = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                        ->whereIn('jollychic', $ids)->where('is_pickup_cash_received', '=', 0)
                        ->where('pickup_walker', '=', $captainid)->orderBy('company_id', 'asc')->orderBy('hub_id', 'asc')
                        ->get(['jollychic', 'company_id', 'cash_on_pickup', 'companies.pickup_fee as pickup_fee', 'hub_id']);
                    $j = 0;
                    $length = sizeof($orders);
                    foreach ($company_ids as $companyid) {
                        $amountReceivedCompany = 0;
                        $captainAccountHistory = new CaptainAccountHistory();

                        $captainAccountHistory->captain_id = $captainid;
                        $captainAccountHistory->admin_id = $admin_id;
                        $captainAccountHistory->company_id = $companyid['company_id'];
                        $captainAccountHistory->hub_id = $hub->id;
                        $captainAccountHistory->banks_deposit_id = -1;
                        $captainAccountHistory->debit = 0;
                        $captainAccountHistory->save();
                        $totalCaptainAmount = 0;
                        for ($i = $j; $i < $length; $i++) {
                            if ($orders[$i]->company_id != $companyid['company_id']) {
                                $j = $i;
                                break;
                            }
                            $flag = DeliveryRequest::where('pickup_walker', '=', $captainid)
                                ->where('jollychic', $orders[$i]->jollychic)
                                ->where('is_pickup_cash_received', '=', 0)
                                ->update(array('is_pickup_cash_received' => 1));
                            if ($flag > 0) {
                                $captainAccount = new CaptainAccount();
                                $captainAccount->waybill = $orders[$i]->jollychic;
                                $captainAccount->captain_id = $captainid;
                                $captainAccount->delivery_fee = $orders[$i]->pickup_fee;
                                $captainAccount->hub_id = $hub->id;
                                $totalCaptainAmount += $orders[$i]->pickup_fee;
                                $amountReceivedCompany += $orders[$i]->cash_on_pickup;
                                $captainAccount->captain_account_history_id = $captainAccountHistory->id;
                                $captainAccount->type = 1;
                                if ($orders[$i]->cash_on_pickup > 0) {
                                    $captainAccount->payment_method = 1;
                                    $captainAccount->amount = $orders[$i]->cash_on_pickup;
                                } else {
                                    $captainAccount->payment_method = 0;
                                    $captainAccount->amount = 0;
                                }
                                $captainAccount->admin_id = $admin_id;
                                $captainAccount->cash_transfer = round($orders[$i]->cash_on_pickup, 2);
                                $captainAccount->save();
                            }

                        }

                        $captainAccountHistory = CaptainAccountHistory::find($captainAccountHistory->id);

                        $captainAccountHistory->credit = $amountReceivedCompany;
                        $captainAccountHistory->cash_credit = round($amountReceivedCompany, 2);
                        $captainAccountHistory->captain_amount = $totalCaptainAmount;
                        $captainAccountHistory->save();

                    }

                    $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                    $captain_info .= " 0";
                    $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                    $response_array = array(
                        'success' => true,
                        'captaindata' => $captain_info,
                        'amount' => $amount,
                        'message' => $amount . ' SAR Received from captain : ' . $captain_info);
                    $this->updatecaptainid($captainid);
                    DB::commit();
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'Amount Received from Captain is not equal to total of cash on pickup orders.Please try again with ' . $amount);
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function bankDepositReport()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $paidtosupplier = Request::get('paid_to_supplier');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'paid_to_supplier' => $paidtosupplier,


                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'paid_to_supplier' => 'required|integer',

            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }


                $fromdate = Request::get('fromdate');
                if (!isset($fromdate)) {
                    $fromdate = '2019-01-01';
                }
                if (date('Y-m-d', strtotime($fromdate)) != $fromdate) {
                    $response_array = array('success' => false, 'error' => 'fromdate format is \'YYYY-MM-DD\'');
                    goto response;
                }
                $todate = Request::get('todate');
                if (!isset($todate)) {
                    $todate = date('Y-m-d');
                }
                if (date('Y-m-d', strtotime($todate)) != $todate) {
                    $response_array = array('success' => false, 'error' => 'todate format is \'YYYY-MM-DD\'');
                    goto response;
                }

                $transfer_code = Request::get('transferCode');
                if (!isset($transfer_code)) {
                    $transfer_code = 'All';
                }

                $bankdepositsquery = BanksDeposit::select(
                    'banks_deposit.id',
                    'banks_deposit.deposit_date',
                    'banks_deposit.bank_name',
                    'companies.company_name as company_name',
                    'banks_deposit.city',
                    'hub.name as hub_name',
                    'admin.username as adminuser',
                    'banks_deposit.amount',
                    DB::raw('IF (banks_deposit.paid_to_supplier = 0  ,"Not Paid","Paid") As "paid_to_supplier"'),
                    DB::raw('IF (banks_deposit.paid_to_supplier = 0  ,"Not Paid",paid_admin.username) As "paidToSupplier_admin_name"'),
                    DB::raw('IF (banks_deposit.paid_to_supplier = 0  ,"Not Paid",banks_deposit.updated_at) As "paidToSupplier_date"'),
                    'banks_deposit.transfer_code')
                    ->leftJoin('companies', 'companies.id', '=', 'banks_deposit.company_id')->leftJoin('admin', 'banks_deposit.admin_id', '=', 'admin.id')
                    ->leftJoin('admin as paid_admin', 'banks_deposit.supplier_admin_id', '=', 'paid_admin.id')
                    ->leftJoin('hub', 'hub.id', '=', 'banks_deposit.hub_id')
                    ->whereDate('deposit_date', '>=', $fromdate)->whereDate('deposit_date', '<=', $todate)
                    ->where('banks_deposit.hub_id', '=', $hub->id)->where('banks_deposit.city', '=', $hub->city)
                    ->where('paid_to_supplier', '=', $paidtosupplier)->orderBy('deposit_date', 'desc');


                if ($transfer_code != 'All') {
                    $bankdepositsquery = $bankdepositsquery->where('banks_deposit.transfer_code', '=', $transfer_code);
                }
                $bankdeposits = $bankdepositsquery->get();
                $response_array = array(
                    'success' => true,
                    'bankDepositItems' => $bankdeposits);

            }

        } catch (Exception $e) {
            DB::rollback();
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function paytosupplier()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $ids = Request::input('ids');
            $amountPayed = Request::get('amountPayed');
            $date = Request::get('pay_to_supplier_date');
            $transfer_code = Request::get('transfer_code');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'ids' => $ids,
                    'amountPayed' => $amountPayed,
                    'pay_to_supplier_date' => $date,
                    'transfer_code' => $transfer_code,

                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'ids' => 'required',
                'amountPayed' => 'required',
                'pay_to_supplier_date' => 'required',
                'transfer_code' => 'required',
            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }

                $ids = explode(',', $ids);
                $BanksDeposit_ids = BanksDeposit::whereIn('id', $ids)->where('paid_to_supplier', '=', 0)->where('hub_id', '=', $hub->id)
                    ->get(['id']);
                if (count($BanksDeposit_ids) == 0) {
                    $response_array = array('success' => false, 'error' => 'Please select bank deposit ids forwhich amount not Payed to Supplier');
                    goto response;
                }

                $amount = BanksDeposit::whereIn('id', $ids)->where('paid_to_supplier', '=', 0)
                    ->where('hub_id', '=', $hub->id)->sum('amount');

                if ($amountPayed == round($amount, 2)) {
                    $flag = BanksDeposit::whereIn('id', $ids)->where('paid_to_supplier', '=', 0)->where('hub_id', '=', $hub->id)
                        ->update(array('paid_to_supplier' => 1, 'supplier_admin_id' => $admin_id, 'updated_at' => $date, 'transfer_code' => $transfer_code));
                    if ($flag > 0) {
                        $response_array = array(
                            'success' => true,
                            'message' => round($amount, 2) . ' SAR  is payed to supplier');
                    }
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'Payment to supplier update failed due to mismatch of total amount, Please try again with ' . round($amount, 2));
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function captainPaymentReport()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');
            $city = Request::get('city');
            $paidtocaptain = Request::get('paid_to_captain');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                    'city' => $city,
                    'paid_to_captain' => $paidtocaptain,


                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
                'city' => 'required',
                'paid_to_captain' => 'required|integer',

            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                $fromdate = Request::get('fromdate');
                if (!isset($fromdate)) {
                    $fromdate = '2019-01-01';
                }
                if (date('Y-m-d', strtotime($fromdate)) != $fromdate) {
                    $response_array = array('success' => false, 'error' => 'fromdate format is \'YYYY-MM-DD\'');
                    goto response;
                }
                $todate = Request::get('todate');
                if (!isset($todate)) {
                    $todate = date('Y-m-d');
                }
                if (date('Y-m-d', strtotime($todate)) != $todate) {
                    $response_array = array('success' => false, 'error' => 'todate format is \'YYYY-MM-DD\'');
                    goto response;
                }
                $captainpaymenthistoryquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
                leftJoin('banks_deposit', 'banks_deposit.id', '=', 'captain_account_history.banks_deposit_id')->
                leftJoin('companies', 'captain_account_history.company_id', '=', 'companies.id')->
                leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
                leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
                leftJoin('hub', 'hub.id', '=', 'captain_account_history.hub_id')->
                select
                ([
                    'captain_account_history.id',
                    'captain_account_history.captain_id',
                    'captain_account_history.company_id',
                    'companies.company_name',
                    'captain_account_history.hub_id',
                    'hub.name as hub_name',
                    'captain_account_history.captain_amount',
                    DB::raw('IF (captain_account_history.paid_to_captain = 0  ,"Not Paid","Paid") As "status"'),
                    DB::raw('IF (paid_admin.username <> ""  ,paid_admin.username,"Not Paid to Captain") As "paid_admin_name"'),
                    'walker.city as city',
                    'banks_deposit.deposit_date as bank_deposit_date',
                    'admin.username as adminuser'])
                    ->whereDate('banks_deposit.deposit_date', '>=', $fromdate)->whereDate('banks_deposit.deposit_date', '<=', $todate)
                    ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)->where('walker.city', '=', $city)
                    ->where('banks_deposit_id', '<>', 0)->where('captain_account_history.captain_id', '=', $captainid)->where('captain_account_history.hub_id', '=', $hub->id);
                $captain_pickup_paymentquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
                leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
                leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
                select
                ([
                    'captain_account_history.id',
                    'walker.id as captain_id',
                    'walker.city as city',
                    'captain_account_history.captain_amount as captain_pickup_payment',
                    DB::raw('IF (captain_account_history.paid_to_captain = 0  ,"Not Paid","Paid") As "status"'),
                    'admin.username as adminuser',
                    DB::raw('IF (paid_admin.username <> ""  ,paid_admin.username,"Not Paid to Captain") As "paid_admin_name"')])
                    ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)->where('captain_account_history.captain_id', '=', $captainid)
                    ->where('banks_deposit_id', '=', -1)->where('walker.city', '=', $city)->where('captain_account_history.hub_id', '=', $hub->id);

                if ($admin_id != 'All') {
                    $captainpaymenthistoryquery = $captainpaymenthistoryquery->where(function ($q) use ($admin_id) {
                        $q->where('captain_account_history.admin_id', 'like', '%' . $admin_id . '%')->orWhere('captain_account_history.captain_admin_id', 'like', '%' . $admin_id . '%');
                    });
                    $captain_pickup_paymentquery = $captain_pickup_paymentquery->where(function ($q) use ($admin_id) {
                        $q->where('captain_account_history.admin_id', 'like', '%' . $admin_id . '%')->orWhere('captain_account_history.captain_admin_id', 'like', '%' . $admin_id . '%');
                    });
                }
                $captainpaymenthistory = $captainpaymenthistoryquery->get();
                $captain_pickup_items = $captain_pickup_paymentquery->get();

                $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                $captain_info .= " 0";
                $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);

                $response_array = array(
                    'success' => true,
                    'captainInfo' => $captain_info,
                    'captainDeliveredItemsPayment' => $captainpaymenthistory,
                    'captainPickupItems' => $captain_pickup_items);
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updateCaptainPayment()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');
            $ids = Request::input('ids');
            $amountPayed = Request::get('amountPayed');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                    'ids' => $ids,
                    'amountPayed' => $amountPayed,

                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
                'ids' => 'required',
                'amountPayed' => 'required',

            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                $ids = explode(',', $ids);
                $CaptainAccountHistory_ids = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captainid)->where('hub_id', '=', $hub->id)->where('paid_to_captain', '=', 0)
                    ->where('banks_deposit_id', '<>', 0)->get(['id']);
                if (count($CaptainAccountHistory_ids) == 0) {
                    $response_array = array('success' => false, 'error' => 'Please select captain account history ids forwhich amount not Payed to Captain');
                    goto response;
                }

                $amount = CaptainAccountHistory::whereIn('id', $ids)->where('paid_to_captain', '=', 0)->where('captain_id', '=', $captainid)->where('hub_id', '=', $hub->id)->where('banks_deposit_id', '<>', 0)->sum('captain_amount');
                if (round($amount, 2) == $amountPayed) {
                    $flag = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captainid)->where('hub_id', '=', $hub->id)->where('banks_deposit_id', '<>', 0)
                        ->where('paid_to_captain', '=', 0)->update(array('paid_to_captain' => 1, 'captain_admin_id' => $admin_id));
                    $this->updatecaptainid($captainid);
                    if ($flag > 0) {
                        $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                        $captain_info .= " 0";
                        $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                        $response_array = array(
                            'success' => true,
                            'captainInfo' => $captain_info,
                            'amount' => $amount,
                            'message' => $amount . ' SAR payed to captain : ' . $captaindata->first_name . ' ' . $captaindata->last_name);
                        $response = Response::json($response_array, 200);
                        return $response;
                    }
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'No Records Updated. Amount payed to Captain is not equal to total of Captain deliverd items delivery fee.Please try again with ' . round($amount, 2));
                }

            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updateCaptainPickupPayment()
    {
        try {
            $admin_id = Request::get('admin_id');
            $secret = Request::get('secret');
            $captainid = Request::get('captain_id');
            $ids = Request::input('ids');
            $amountPayed = Request::get('amountPayed');

            $validator = Validator::make(
                array(
                    'admin_id' => $admin_id,
                    'secret' => $secret,
                    'captain_id' => $captainid,
                    'ids' => $ids,
                    'amountPayed' => $amountPayed,

                ), array(
                'admin_id' => 'required|integer',
                'secret' => 'required',
                'captain_id' => 'required|integer',
                'ids' => 'required',
                'amountPayed' => 'required',

            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $hub = Hub::where('secret', '=', $secret)->first();
                if (empty($hub)) {
                    $response_array = array('success' => false, 'error' => 'invalid secret please contact admin');
                    goto response;
                }
                $admin = Admin::where('id', '=', $admin_id)->first();
                if (empty($admin) || $admin->hub_id != $hub->id) {
                    $response_array = array('success' => false, 'error' => 'Admin id is not valid');
                    goto response;
                }
                $captaindata = Walker::select('first_name', 'last_name', 'phone')->where('id', '=', $captainid)->first();
                if (empty($captaindata)) {
                    $response_array = array('success' => false, 'error' => "Captain id does not exist");
                    goto response;
                }

                $ids = explode(',', $ids);
                $CaptainAccountHistory_ids = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captainid)->where('paid_to_captain', '=', 0)
                    ->where('banks_deposit_id', '=', -1)->get(['id']);
                if (count($CaptainAccountHistory_ids) == 0) {
                    $response_array = array('success' => false, 'error' => 'Please select captain account history ids of pickup shipments forwhich amount not Payed to Captain');
                    goto response;
                }


                $amount = CaptainAccountHistory::whereIn('id', $ids)->where('paid_to_captain', '=', 0)->where('captain_id', '=', $captainid)->where('banks_deposit_id', '=', -1)->sum('captain_amount');
                if (round($amount, 2) == $amountPayed) {
                    $flag = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captainid)->where('banks_deposit_id', '=', -1)
                        ->where('paid_to_captain', '=', 0)->update(array('paid_to_captain' => 1, 'captain_admin_id' => $admin_id));
                    $this->updatecaptainid($captainid);
                    if ($flag > 0) {
                        $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                        $captain_info .= " 0";
                        $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                        $response_array = array(
                            'success' => true,
                            'captainInfo' => $captain_info,
                            'amount' => $amount,
                            'message' => $amount . ' SAR payed to captain : ' . $captaindata->first_name . ' ' . $captaindata->last_name);
                        $response = Response::json($response_array, 200);
                        return $response;
                    }
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'No Records Updated. Amount payed to Captain is not equal to total of Captain pickup payment.Please try again with ' . $amount);
                }
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatecaptainid($captainid)
    {
        if ($captainid == 0 || $captainid == '')
            return false;
        $delivered = DB::select("select count(dr.id) 'delivered' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 5 and dr.is_money_received = 0")[0]->delivered;
        $with_captain = DB::select("select count(delivery_request.id) 'with_captain' from delivery_request where confirmed_walker = $captainid and status = 4")[0]->with_captain;
        $returned = DB::select("select count(dr.id) 'returned' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 6")[0]->returned;
        $lastscheduledshipmentdate = DB::select("select ifnull(max(scheduled_shipment_date), '') 'lastscheduledshipmentdate' from delivery_request where confirmed_walker = $captainid and is_money_received = 0")[0]->lastscheduledshipmentdate;
        $last_payment_date = DB::select("SELECT max(date) 'last_payment_date' FROM captain_account_history as cahd WHERE cahd.captain_id = $captainid")[0]->last_payment_date;
        $balance = DB::select("select sum(cah.captain_amount) 'balance' from captain_account_history cah where cah.captain_id = $captainid and cah.paid_to_captain = 0")[0]->balance;
        $pending_since = "select min(PendingSince) 'pendingsince' from (
            select min(scheduled_shipment_date) 'PendingSince' from delivery_request where status in (4,6)  and confirmed_walker = $captainid
            union
            select min(dropoff_timestamp) 'PendingSince' from delivery_request where status = 5 and is_money_received = 0  and confirmed_walker = $captainid
            ) A";
        $pending_since = DB::select($pending_since)[0]->pendingsince;
        if ($pending_since == NULL)
            $pending_since = '0000-00-00 00:00:00';
        $delayedcountquery = "select waybill_number 'DelayedShipments' from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) union select waybill_number 'DelayedShipments' from orders_history where status = 5 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 5 and is_money_received = 0 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 union select waybill_number 'DelayedShipments' from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) ";
        $delayedcount = DB::select($delayedcountquery);
        $delayedcountarray = '(';
        for ($i = 0; $i < count($delayedcount); ++$i)
            if ($i == 0)
                $delayedcountarray .= "'" . $delayedcount[$i]->DelayedShipments . "'";
            else
                $delayedcountarray .= ',' . "'" . $delayedcount[$i]->DelayedShipments . "'";
        $delayedcountarray .= ')';
        if (count($delayedcount) > 0)
            $delayedamount = DB::select("select sum(cash_on_delivery) 'delayedamount' from delivery_request where jollychic in $delayedcountarray")[0]->delayedamount;
        else
            $delayedamount = 0;
        DB::update("update walker set delivered = ? , with_captain = ? , returned = ? , last_scheduled_shipment_date = ? , last_payment_date = ? , balance = ? , pending_since = ? , delayed_count = ? , delayed_amount = ? where id = ?", [$delivered, $with_captain, $returned, $lastscheduledshipmentdate, $last_payment_date, $balance, $pending_since, count($delayedcount), $delayedamount, $captainid]);
        return true;
    }
}