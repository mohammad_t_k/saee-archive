<?php


use App\Classes\Curl;

use GuzzleHttp\Client;

class TrackingController extends \BaseController
{
    public function test()
    {
        echo "hello";

    }

    public function tracking()
    {
        $Failed_Delivery_Attempt=0;
        if (Request::has('trackingnum')) {
            try {
                $waybillnumber = Request::get('trackingnum');
                $searchcol = 'delivery_request.waybill';
                if(preg_match('/^JC[0-9]{8}KS/',$waybillnumber) == 1)
                {
                    $searchcol = 'delivery_request.jollychic';
                }

                $order_all = OrdersHistory::select('orders_history.id', 'orders_history.created_at', 'orders_history.status as statusnum',
                    'delivery_request.company_id','delivery_request.receiver_name', 'delivery_request.d_city as city', 'delivery_request.num_faild_delivery_attempts','order_statuses.english as statustxt', 'notes')
                    ->leftJoin('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->leftJoin('delivery_request', $searchcol, '=', 'orders_history.waybill_number')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();


                if (count($order_all)) {
					$company_id = $order_all[0]->company_id;

                    $orderArr = array();

                    foreach ($order_all as $order) {
                        if ($order->statusnum) {
                            $order->city = $order->city;
                        } else {
                            $order->city = 'Riyadh';
                        }
                        if ($order->statusnum==6) {
                            $order->statustxt = $order->notes;
                            $Failed_Delivery_Attempt=$order->num_faild_delivery_attempts;
                        }
                        array_push($orderArr, array(
                            'id' => $order->id,
                            'city' => $order->city,
                            'status' => $order->statusnum,
                            'notes' => $order->statustxt,
                            'updated_at' => $order->created_at
                        ));
                    }

                    $response_array = array(
                        'success' => true,
						'company_id' => $company_id,
                        'Failed_Delivery_Attempts' => $Failed_Delivery_Attempt,
                        'details' => $orderArr);
                } else {
                    $response_array = array(
                        'success' => false,
                        'error' => 'waybill [' . $waybillnumber . '] not found');
                }

            } catch (Exception $e) {

                return $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            }

        } else {
            $response_array = array(
                'success' => false,
                'error' => 'no tracking number');
        }


        $response = Response::json($response_array, 200);
        return $response;

    }

    public function trackingpage()
    {

         if (!Session::has('language')) {
            Session::put('language', 'ar');
            $language = 'ar';
        }else{
            $language = Session::get('language');
            Log::info($language);
        }
        $page = 'warehouse.trackingar';
        if($language=='en'){
            $page = 'warehouse.tracking';
            $finalstaustxt = 'Not Found!';
        }else{

            $finalstaustxt = 'غير موجود';
        }

        $google_api_key = Settings::select('value')->where("key", '=', 'google_api_key')->first();

        $google_api_key = $google_api_key->value;

        if (Request::has('trackingnum')) {
            try {
                $waybillnumber = Request::get('trackingnum');
                $finalstausnum = 0;
                
                // Captain rating
        $captainRating = CaptainRating::select('waybill', 'captain_id', 'captain_rating')->where('waybill', '=', $waybillnumber)->get();

                $orderDetail = DeliveryRequest::select('jollychic', 'D_latitude', 'D_longitude','status')->where('jollychic', '=', $waybillnumber)->get();

                // $order_all = OrdersHistory::where('waybill_number', '=', $waybillnumber)->orderBy('id', 'asc')->get();;

                $captainData = DeliveryRequest::select('delivery_request.confirmed_walker', 'walker.first_name', 'walker.last_name', 'walker.phone', 'walker.id as walkerId')
                    ->join('walker', 'walker.id', '=', 'delivery_request.confirmed_walker')
                    ->where('jollychic', '=', $waybillnumber)->orderBy('delivery_request.id', 'desc')->get();

                $order_all = OrdersHistory::select('created_at', 'status as statusnum', 'english as statustxt', 'arabic as statusarabic', 'notes')
                    ->join('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();

                    $orderHistory = 'select CONCAT(w.first_name, " " ,w.last_name),w.phone,o.updated_at,s.english,s.arabic, 
CASE
   WHEN o.status in (0,2,7) THEN c.city
   WHEN o.status in (3,4,5,6) THEN d.d_city 
END as "city", o.notes
from delivery_request d 
left join  orders_history o
 on d.jollychic = o.waybill_number 
left join  walker w
 on d.confirmed_walker = w.id 
left join  companies c
on d.company_id = c.id 
left join  order_statuses s
 on o.status = s.id 
where d.jollychic = "'.$waybillnumber.'"
order by o.created_at desc';

$orderHistory = DB::select($orderHistory);

                    if(count($captainData)){
                        $captainName = $captainData[0]->first_name.' '.$captainData[0]->last_name;
                        $captainPhone = $captainData[0]->phone;
                        $captainId = $captainData[0]->walkerId;
                    }
                    else{
                        $captainName = '';
                        $captainPhone ='';
                        $captainId = '';
                    }

                if (count($order_all)) {
                    $finalstausnum = $order_all[0]->statusnum;
                    if($language=='en'){
                    $finalstaustxt = $order_all[0]->statustxt;
                }else{
                    $finalstaustxt = $order_all[0]->statusarabic;
                }

                } else {
                    $finalstausnum = 0;
                    if($language=='en'){
                    $waybillnumber = 'Not Found!';
                    }else{
                       $waybillnumber = 'غير موجود';
                    }
                    $finalstaustxt = '#';
                    $order_all = array();
                }
                foreach ($order_all as $order) {

                    if ($order->statusnum) {
                        $order->city = 'Jeddah';
                    } else {
                        $order->city = 'Riyadh';
                    }

                }

                if ($finalstausnum == 0) {
                    $finalstausnum = 5;
                } else if ($finalstausnum == 2) {
                    $finalstausnum = 25;
                } else if ($finalstausnum == 3) {
                    $finalstausnum = 50;
                } else if ($finalstausnum == 4 || $finalstausnum == 6) {
                    $finalstausnum = 75;
                } else {
                    $finalstausnum = 100;
                }


            } catch (Exception $e) {

                return $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            }

        } else {
            return View::make($page)
                ->with('waybill', '')
                ->with('finalstausnum', '')
                ->with('finalstaustxt', $finalstaustxt)
                ->with('captainName', '')
                ->with('orderHistory', [])
                ->with('captainPhone', '')
                ->with('history', [])
                ->with('orderDetail', [])
                ->with('captainId', [])
                ->with('google_api_key', '')
                ->with('captainRating', []);
        }
        return View::make($page)
            ->with('waybill', $waybillnumber)
            ->with('orderHistory', $orderHistory)
            ->with('finalstausnum', $finalstausnum)
            ->with('finalstaustxt', $finalstaustxt)
            ->with('captainName', $captainName)
            ->with('captainPhone', $captainPhone)
            ->with('history', $order_all)
            ->with('orderDetail', $orderDetail)
            ->with('captainId', $captainId)
            ->with('google_api_key', $google_api_key)
            ->with('captainRating', $captainRating);
    }


    public function updateTrackingAddress()
    {
        
        try {
           
            $lat = number_format((float)Request::get('address_latitude'), 8, '.', '');

            $lng = number_format((float)Request::get('address_longitude'), 8, '.', '');

            //$address = Request::get('address');
            $trackingnum = Request::get('trackingnum');

            
            //$newshipmentdate = Request::get('newshipmentdate');

            $item = DeliveryRequest::select('confirmed_walker','receiver_name')->where('jollychic', '=', $trackingnum)->get();

            $walker_id = $item[0]->confirmed_walker;
            $receiver_name = $item[0]->receiver_name;


            $response_array = array(
                                    'success' => true,
                                    'unique_id' => '1001',
                                    'message'  =>  'Customer \''.$receiver_name.'\' has updated shipment address for waybill \''. $trackingnum.'\'',
                                    'receiverName'  =>  $receiver_name,
                                    'waybill' => $trackingnum
                                );
                                $title = "Shipment address updated";

                                $message = $response_array;


                $flag = DeliveryRequest::where('jollychic', '=', $trackingnum)
                    ->update(array('D_latitude' => $lat, 'D_longitude' => $lng, 'is_address_updated' => '1'));


                   
                
                if ($flag > 0) {
                    $response_array = array(
                        'success' => true);
                    $response = Response::json($response_array, 200);
                    send_notifications($walker_id, "scanner", $title, $message);
                    return $response;
                }

        } catch (Exception $e) {
            Log::info($e->getMessage());
        }
        
    }

    public function shipmenttracking()
    {
        try {
            $waybillnumber = Input::get('waybill');
            $item = DeliveryRequest::where('jollychic', '=', $waybillnumber)->first();
            if (isset($item)) {
                $captainData = DeliveryRequest::select('jollychic', 'D_latitude', 'D_longitude', 'delivery_request.d_address', 'companies.city', 'd_city', 'status','is_walker_rated')
                    ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                    ->where('jollychic', '=', $waybillnumber)->orderBy('delivery_request.id', 'desc')->get();
                $query = 'SELECT min(updated_at) updated_at from orders_history WHERE status = 3 and waybill_number = "' . $waybillnumber . '"';
                $result = DB::select($query);
                $updated_at = $result[0]->updated_at;
                $scheduleDates = array();
                $todayDate = date('Y-m-d');
                $check = 0;
                for ($i = 1; $i <= 14; $i++) {
                    $SecheduledDatesQuery = 'SELECT date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY)) scheduledDate FROM delivery_request WHERE
                                jollychic = "' . $waybillnumber . '" and
                        if (d_city in ("Jeddah"),
                    dayname(date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY))) not in ("Saturday","Monday","Wednesday","Friday"),
                dayname(date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY))) not in ("Friday"))';
                    $Dates = DB::select($SecheduledDatesQuery);
                    if (!empty($Dates)) {
                        if ($Dates[0]->scheduledDate > $todayDate) {
                            $check = 1;
                            $scheduleDates[] = $Dates[0]->scheduledDate;
                        }
                    }
                }

                if ($item->confirmed_walker > 0) {
                    $walker = Walker::where('id', '=', $item->confirmed_walker)->first();
                    if (count($walker)) {
                        $captainName = $walker->first_name . ' ' . $walker->last_name;
                        $captainPhone = $walker->phone;
                        $captain_id = $walker->id;
                    }
                } else {
                    $captainName = '';
                    $captainPhone = '';
                    $captain_id ='';
                }
                $order_all = OrdersHistory::select('created_at', 'status as status_num', 'english as status_text','arabic as status_text_ar', 'notes')
                    ->join('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                    ->where('waybill_number', '=', $waybillnumber)->orderBy('orders_history.id', 'desc')->get();
                foreach ($order_all as $order) {
                    if ($order->status_num == 0 || $order->status_num == 2 || $order->status_num == 7) {
                        $order->city = $captainData[0]->city;;
                    } else {
                        $order->city = $captainData[0]->d_city;
                    }
                }

                $response_array = array(
                    'success' => true,
                    'waybill' => $captainData[0]->jollychic,
                    'status' =>$captainData[0]->status,
                    'is_walker_rated' => $captainData[0]->is_walker_rated,
                    'captain_id' => $captain_id,
                    'captain_name' => $captainName,
                    'captain_phone' => $captainPhone,
                    'receiver_address' => $captainData[0]->d_address,
                    'receiver_lat' => $captainData[0]->D_latitude,
                    'receiver_lng' => $captainData[0]->D_longitude,
                    'schedule_dates' =>$scheduleDates,
                    'orders_history' => $order_all);
                $response = Response::json($response_array, 200);
                return $response;
            } else {
                $response_array = array('success' => false, 'error' => 'Item Not Found With ID: ' . $waybillnumber, 'error_code' => 410);
                $response = Response::json($response_array, 200);
                return $response;
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

    }

 public function shipmenttrackingv2()
    {
        try {
            $waybillnumber = Input::get('waybill');
            $phonenumber = Input::get('mobile');
            //$item = DeliveryRequest::where('jollychic', '=', $waybillnumber)->first();
            $orders = DeliveryRequest::where('jollychic', '=', $waybillnumber)->orWhere('receiver_phone', '=', $phonenumber)->get();
            if (count($orders)) {
                $items = array();
                foreach ($orders as $item) {
                    $captainData = DeliveryRequest::select('jollychic','cash_on_delivery', 'D_latitude', 'D_longitude', 'delivery_request.d_address', 'companies.city', 'd_city', 'status','is_walker_rated')
                        ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                        ->where('jollychic', '=', $item->jollychic)->orderBy('delivery_request.id', 'desc')->get();
                    $query = 'SELECT min(updated_at) updated_at from orders_history WHERE status = 3 and waybill_number = "' . $item->jollychic . '"';
                    $result = DB::select($query);
                    $updated_at = $result[0]->updated_at;
                    $scheduleDates = array();
                    $todayDate = date('Y-m-d');
                    $check = 0;
                    for ($i = 1; $i <= 14; $i++) {
                        $SecheduledDatesQuery = 'SELECT date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY)) scheduledDate FROM delivery_request WHERE
                                jollychic = "' . $waybillnumber . '" and
                        if (d_city in ("Jeddah"),
                    dayname(date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY))) not in ("Saturday","Monday","Wednesday","Friday"),
                dayname(date(DATE_ADD("' . $updated_at . '", INTERVAL "' . $i . '" DAY))) not in ("Friday"))';
                        $Dates = DB::select($SecheduledDatesQuery);
                        if (!empty($Dates)) {
                            if ($Dates[0]->scheduledDate > $todayDate) {
                                $check = 1;
                                $scheduleDates[] = $Dates[0]->scheduledDate;
                            }
                        }
                    }

                    if ($item->confirmed_walker > 0) {
                        $walker = Walker::where('id', '=', $item->confirmed_walker)->first();
                        if (count($walker)) {
                            $captainName = $walker->first_name . ' ' . $walker->last_name;
                            $captainPhone = $walker->phone;
                            $captain_id = $walker->id;
                        }
                    } else {
                        $captainName = '';
                        $captainPhone = '';
                        $captain_id =0;
                    }
                    $order_all = OrdersHistory::select('created_at', 'status as status_num', 'english as status_text','arabic as status_text_ar', 'notes')
                        ->join('order_statuses', 'order_statuses.id', '=', 'orders_history.status')
                        ->where('waybill_number', '=', $item->jollychic)->orderBy('orders_history.id', 'desc')->get();
                    foreach ($order_all as $order) {
                        if ($order->status_num == 0 || $order->status_num == 1 || $order->status_num == 2 || $order->status_num == 7) {
                            $order->city = $captainData[0]->city;;
                        } else {
                            $order->city = $captainData[0]->d_city;
                        }
                    }

                    array_push($items, array(
                        'waybill' => $captainData[0]->jollychic,
                        'status' =>$captainData[0]->status,
                        'cash_on_delivery' =>$captainData[0]->cash_on_delivery,
                        'is_walker_rated' => $captainData[0]->is_walker_rated,
                        'captain_id' => $captain_id,
                        'captain_name' => $captainName,
                        'captain_phone' => $captainPhone,
                        'receiver_address' => $captainData[0]->d_address,
                        'receiver_lat' => $captainData[0]->D_latitude,
                        'receiver_lng' => $captainData[0]->D_longitude,
                        'schedule_dates' =>$scheduleDates,
                        'orders_history' => $order_all
                    ));

                }
                $response_array = array(
                    'success' => true,
                    'items' => $items);
                $response = Response::json($response_array, 200);
                return $response;
            } else {
                $response_array = array('success' => false, 'error' => 'Item Not Found With ID: ' . $waybillnumber, 'error_code' => 410);
                $response = Response::json($response_array, 200);
                return $response;
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

    }
    public function trackingAddressupdate()
    {

        try {
            $waybillnumber = Input::get('waybill');
            $latitude = Input::get('address_latitude');
            $longitude = Input::get('address_longitude');
            //$newshipmentdate = Request::get('newshipmentdate');

            $item = DeliveryRequest::where('jollychic', '=', $waybillnumber)->first();
            if (isset($item)) {
                //$newshipmentdate=date("Y-m-d", strtotime($newshipmentdate));
                /*if($newshipmentdate == ''){
                    $response_array = array('success' => false, 'error' => 'Shipment Date Can not be null', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                } */
                if($latitude == '' || $longitude==''){
                    $response_array = array('success' => false, 'error' => 'address_latitude Or address_longitude Can not be empty', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
                $item = DeliveryRequest::select('confirmed_walker','receiver_name')->where('jollychic', '=', $waybillnumber)->get();
                $walker_id = $item[0]->confirmed_walker;
                $receiver_name = $item[0]->receiver_name;

                $response_array = array(
                    'success' => true,
                    'unique_id' => '1001',
                    'message'  =>  'Customer \''.$receiver_name.'\' has updated shipment address for waybill \''. $waybillnumber.'\'',
                    'receiverName'  =>  $receiver_name,
                    'waybill' => $waybillnumber
                );
                $title = "Shipment address updated";
                $message = $response_array;

                $flag = DeliveryRequest::where('jollychic', '=', $waybillnumber)
                    ->update(array('D_latitude' => $latitude, 'D_longitude' => $longitude, 'is_address_updated' => '1'));
                if ($flag > 0) {
                    $response_array = array(
                        'success' => true,
                    'message'=>'Address has been Updated For waybill ' . $waybillnumber,);
                    $response = Response::json($response_array, 200);
                    send_notifications($walker_id, "scanner", $title, $message);
                    return $response;
                }

            } else {
                $response_array = array('success' => false, 'error' => 'Item Not Found With ID: ' . $waybillnumber, 'error_code' => 410);
                $response = Response::json($response_array, 200);
                return $response;
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

    }
    
   public function rate_captain(){
     
   try {

       $captain_id = Input::get('captain_id');
       $waybill =  Input::get('waybill');
       $rating = Input::get('rating');
       $comments = Input::get('comments');
       $count = 0;

        // to not allow update rating if its already rated
       $checkRatingQuery = "SELECT COUNT(*) AS cntwaybill FROM captain_rating WHERE waybill='".$waybill."' and captain_rating IS NOT NULL";

       $checkRating = DB::select($checkRatingQuery);

       if (count($checkRating)) {

        $count = $checkRating[0]->cntwaybill;

       }

       $query = "SELECT * FROM captain_rating WHERE waybill='".$waybill."' ";


       $checkwaybill = DB::select($query);

       if(empty($checkwaybill)){
        $empty = true;
       }else{
        $empty = false;
       }
        

       if($empty == false){ // if there is any entry of this waybill by customer or by captain... 


            if($count == 0){  // if waybill not already rated then update

        DB::table('captain_rating')
        ->where('waybill', $waybill)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('captain_rating' => $rating, 'captain_comments' => $comments)); 


         $averageRatingQuery = 'select w.id, (SELECT Sum(captain_rating) / COUNT(DISTINCT captain_rating.id) FROM captain_rating WHERE captain_rating IS NOT NULL AND w.id = captain_rating.captain_id ) AS "captain_avg_rating", (SELECT COUNT(captain_rating.id) FROM captain_rating WHERE w.id = captain_rating.captain_id ) AS "totalcount", (SELECT COUNT(captain_rating.id) FROM captain_rating WHERE captain_rating IS NOT NULL AND w.id = captain_rating.captain_id ) AS "ratedcount" from walker w where w.id="'.$captain_id.'"';


        $averageRating = DB::select($averageRatingQuery);

        $captainAvgRating  = $averageRating[0]->captain_avg_rating;
        $captainTotalCount = $averageRating[0]->totalcount;
        $captainRatedCount = $averageRating[0]->ratedcount;

        // updating average rating and count to walker table
        DB::table('walker')
        ->where('id', $captain_id)
        ->limit(1)
        ->update(array('rate' => $captainAvgRating, 'rate_count' => $captainRatedCount));

        DB::table('delivery_request')
        ->where('jollychic', $waybill)
        ->limit(1)
        ->update(array('is_walker_rated' => '1'));

        $response_array = array(
                        'success' => true,
                        'message' => 'Rating added Successfuly', 
                    );

                    return $response = Response::json($response_array, 200);

                }
                else{

                    $response_array = array(
                        'success' => false,
                        'error' => 'You cannot modify rating',
                        'error_code' => 408
                    );

                    return $response = Response::json($response_array, 200);

                }
       }

       else{

        $insertdata = array(
         array('waybill' => $waybill, 'captain_id' => $captain_id, 'captain_rating' => $rating, 'captain_comments' => $comments),
        );



        // inserting into captain_rating table// 
        CaptainRating::insert($insertdata);

        // insert is_walker_rated to ture//
         DB::table('delivery_request')
        ->where('jollychic', $waybill)
        ->limit(1)
        ->update(array('is_walker_rated' => '1'));

        // Average Rating Code to update average into walker table

        $averageRatingQuery = 'select w.id, (SELECT Sum(captain_rating) / COUNT(DISTINCT captain_rating.id) FROM captain_rating WHERE captain_rating IS NOT NULL AND w.id = captain_rating.captain_id ) AS "captain_avg_rating", (SELECT COUNT(captain_rating.id) FROM captain_rating WHERE w.id = captain_rating.captain_id ) AS "totalcount", (SELECT COUNT(captain_rating.id) FROM captain_rating WHERE captain_rating IS NOT NULL AND w.id = captain_rating.captain_id ) AS "ratedcount" from walker w where w.id="'.$captain_id.'"';


        $averageRating = DB::select($averageRatingQuery);


        $captainAvgRating  = $averageRating[0]->captain_avg_rating;
        $captainTotalCount = $averageRating[0]->totalcount;
        $captainRatedCount = $averageRating[0]->ratedcount;

        // updating average rating and count to walker table
         DB::table('walker')
        ->where('id', $captain_id)
        ->limit(1)
        ->update(array('rate' => $captainAvgRating, 'rate_count' => $captainRatedCount));

        $response_array = array(
                        'success' => true,
                        'message' => 'Rating added Successfuly',  
                    );
        
                     return $response = Response::json($response_array, 200);
       }

    }
    catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
    

}

public function rate_customer(){

     
    try {

       $waybill =  Input::get('waybill');
       $rating = Input::get('rating');
       $comments = Input::get('comments');
       $captain_id = Input::get('captain_id');
       $token = Input::get('token');

       $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first(); // check if valid walker id

       
       $count = 0;


       $checkRatingQuery = "SELECT COUNT(*) AS cntwaybill FROM captain_rating WHERE waybill='".$waybill."' and customer_rating IS NOT NULL";

       $checkRating = DB::select($checkRatingQuery);

       if (count($checkRating)) {

        $count = $checkRating[0]->cntwaybill;

       }

       //check if waybill already has rating or not
       $query = "SELECT * FROM captain_rating WHERE waybill='".$waybill."' ";


       $checkwaybill = DB::select($query);

       if(empty($checkwaybill)){
        $empty = true;
       }else{
        $empty = false;
       }
        
if($walker){
       if($empty == false){

            if($count == 0){

        DB::table('captain_rating')
        ->where('waybill', $waybill)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('customer_rating' => $rating, 'customer_comments' => $comments)); 

         DB::table('delivery_request')
        ->where('jollychic', $waybill)
        ->limit(1)
        ->update(array('is_dog_rated' => '1'));

        $response_array = array(
                        'success' => true,
                        'message' => 'Rating added Successfuly', 
                    );

                    return $response = Response::json($response_array, 200);

                }
                else{



                    $response_array = array(
                        'success' => false,
                        'error' => 'You cannot modify rating', 
                        'error_code' => 408
                    );

                    return $response = Response::json($response_array, 200);

                }
       }

       else{


        $insertdata = array(
         array('waybill' => $waybill, 'captain_id' =>$captain_id, 'customer_rating' => $rating, 'customer_comments' => $comments),
        );

        CaptainRating::insert($insertdata);

         DB::table('delivery_request')
        ->where('jollychic', $waybill)
        ->limit(1)
        ->update(array('is_dog_rated' => '1'));

        $response_array = array(
                        'success' => true,
                        'message' => 'Rating added Successfuly',  
                    );
        
                     return $response = Response::json($response_array, 200);
       }

   } // end if $walker

   else{
        $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                       'error_code' => 406
                    );

                    return $response = Response::json($response_array, 200);
       } // else if walker

    }
    catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
    

}



}