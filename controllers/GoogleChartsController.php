<?php

/*
 * Company related data manipulation Controller
  */
class GoogleChartsController extends BaseController {

 
public function __construct() {
        $this->beforeFilter(function () {
            if (!Auth::check()) {
                $url = URL::current();
                $routeName = Route::currentRouteName();

                /* Log::info('current route =' . print_r(Route::currentRouteName(), true)); */

                if ($routeName != "AdminLogin" && $routeName != 'admin') {
                    Session::put('pre_admin_login_url', $url);
                }
                return Redirect::to('/warehouse/login');
            }
        }, array('except' => array('login', 'verify', 'add', 'walker_xml')));
    }

public function index(){

     return View::make('warehouse.googleCharts');
}


    public function overall_chart(){

        $overallQuery = 'select
sum(status = 3 and undelivered_reason = 0) "InWarehouse",
round((sum(status = 3 and undelivered_reason = 0) / count(*))*100,2) "InWarehouseRate",
sum(status = 4) "WithCaptains",
round((sum(status = 4) / count(*))*100,2) "WithCaptainsRate",
sum(status = 6) Undelivered,
round((sum(status = 6) / count(*))*100,2) "UndeliveredRate",
sum(status = 3 and undelivered_reason <> 0) "Returend",
round((sum(status = 3 and undelivered_reason <> 0) / count(*))*100,2) "ReturendRate",
sum(status = 7) "ReturnedtoSupplier",
round((sum(status = 7) / count(*))*100,2) "ReturnedtoSupplierRate",
sum(status = 5) "Delivered",
round((sum(status = 5) / count(*))*100,2) "SuccessRate", 
count(*) "TotalShipments"
from delivery_request where
status not in (0,2)';

$data = DB::select($overallQuery);
$result = array();

//$result[] = ['In Warehouse','In Warehouse Rate','With Captains'];

        foreach ($data as $key => $value) {

            $result[++$key] = [(int)$value->InWarehouse, (float)$value->InWarehouseRate, (int)$value->WithCaptains,(float)$value->WithCaptainsRate, (int)$value->Undelivered, (float)$value->UndeliveredRate, (int)$value->Returend , (float)$value->ReturendRate, (int)$value->ReturnedtoSupplier, (float)$value->ReturnedtoSupplierRate, (int)$value->Delivered, (float)$value->SuccessRate];


        }

//print_r($result[1][5]);exit;


    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
 
    

    $array['rows'][] = array('c' => array( array('v'=>'In Warehouse '.$result[1][1].'%- '.$result[1][0].''), array('v'=>$result[1][0])) );
   
    $array['rows'][] = array('c' => array( array('v'=>'With Captains '.$result[1][3].'%- '.$result[1][2].'' ), array('v'=>$result[1][2])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Undelivered '.$result[1][5].'%- '.$result[1][4].''), array('v'=>$result[1][4])));
   
    $array['rows'][] = array('c' => array( array('v'=>'Returend '.$result[1][7].'%- '.$result[1][6].''), array('v'=>$result[1][6])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Returend to Supplier  '.$result[1][9].'%- '.$result[1][8].''), array('v'=>$result[1][8])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Delivered '.$result[1][11].'%- '.$result[1][10].''), array('v'=>$result[1][10])));


  echo json_encode($array);
//$response_array = array('success' => true, 'data' => $result);
//$response = Response::json($response_array, 200);
//return $response;

    }



public function overall_last_shipments(){

        $overallLastShipmentQuery = 'select scheduled_shipment_date,
sum(status = 3 and undelivered_reason = 0) "InWarehouse",
round((sum(status = 3 and undelivered_reason = 0) / count(*))*100,2) "InWarehouseRate",
sum(status = 4) "WithCaptains",
round((sum(status = 4) / count(*))*100,2) "WithCaptainsRate",
sum(status = 6) Undelivered,
round((sum(status = 6) / count(*))*100,2) "UndeliveredRate",
sum(status = 3 and undelivered_reason <> 0) "Returend",
round((sum(status = 3 and undelivered_reason <> 0) / count(*))*100,2) "ReturendRate",
sum(status = 7) "ReturnedtoSupplier",
round((sum(status = 7) / count(*))*100,2) "ReturnedtoSupplierRate",
sum(status = 5) "Delivered",
round((sum(status = 5) / count(*))*100,2) "SuccessRate", 
count(*) "Total Shipments"
from delivery_request where
status not in (0,2) and scheduled_shipment_date in (select ScheduledCityDates from
(select max(scheduled_shipment_date) ScheduledCityDates, d_city from delivery_request where scheduled_shipment_date is not null
group by d_city) a) order by scheduled_shipment_date desc limit 3';

$data = DB::select($overallLastShipmentQuery);
$result = array();

//$result[] = ['In Warehouse','In Warehouse Rate','With Captains'];

        foreach ($data as $key => $value) {

            $result[++$key] = [(int)$value->InWarehouse, (float)$value->InWarehouseRate, (int)$value->WithCaptains,(float)$value->WithCaptainsRate, (int)$value->Undelivered, (float)$value->UndeliveredRate, (int)$value->Returend, (float)$value->ReturendRate, (int)$value->ReturnedtoSupplier, (float)$value->ReturnedtoSupplierRate, (int)$value->Delivered, (float)$value->SuccessRate];


        }

//print_r($result[1][5]);exit;


    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
    $array['cols'][] = array('type' => 'string');
   

     $array['rows'][] = array('c' => array( array('v'=>'In Warehouse '.$result[1][1].'%- '.$result[1][0].''), array('v'=>$result[1][0])) );
   
    $array['rows'][] = array('c' => array( array('v'=>'With Captains '.$result[1][3].'%- '.$result[1][2].'' ), array('v'=>$result[1][2])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Undelivered '.$result[1][5].'%- '.$result[1][4].''), array('v'=>$result[1][4])));
   
    $array['rows'][] = array('c' => array( array('v'=>'Returend '.$result[1][7].'%- '.$result[1][6].''), array('v'=>$result[1][6])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Returend to Supplier  '.$result[1][9].'%- '.$result[1][8].''), array('v'=>$result[1][8])));
    
    $array['rows'][] = array('c' => array( array('v'=>'Delivered '.$result[1][11].'%- '.$result[1][10].''), array('v'=>$result[1][10])));

  echo json_encode($array);
//$response_array = array('success' => true, 'data' => $result);
//$response = Response::json($response_array, 200);
//return $response;

    }



    public function city_last_shipments(){

    $admin_id = Session::get('admin_id');
    $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '!=', 'All')->orderBy('city', 'asc')->get();
    
    $data = array();
    $perCityQuery = array();
    $cityNames = array();   
$citiesCount = count($adminCities);


foreach($adminCities as $cities){

        if($cities->city == 'All'){
            continue;
        }
$cityNames[] = $cities->city;

        $CityQuery = 'select scheduled_shipment_date,
sum(status = 3 and undelivered_reason = 0) "InWarehouse",
round((sum(status = 3 and undelivered_reason = 0) / count(*))*100,2) "InWarehouseRate",
sum(status = 4) "WithCaptains",
round((sum(status = 4) / count(*))*100,2) "WithCaptainsRate",
sum(status = 6) "Undelivered",
round((sum(status = 6) / count(*))*100,2) "UndeliveredRate",
sum(status = 3 and undelivered_reason <> 0) "Returend",
round((sum(status = 3 and undelivered_reason <> 0) / count(*))*100,2) "ReturendRate",
sum(status = 7) "ReturnedtoSupplier",
round((sum(status = 7) / count(*))*100,2) "ReturnedtoSupplierRate",
sum(status = 5) "Delivered",
round((sum(status = 5) / count(*))*100,2) "SuccessRate", 
count(*) "Total Shipments"
from delivery_request where
d_city = "' . $cities->city . '" and status not in (0,2) and scheduled_shipment_date = (select max(scheduled_shipment_date) from delivery_request where
d_city = "' . $cities->city . '" and scheduled_shipment_date is not null) order by scheduled_shipment_date desc limit 3';


$mydata[] = DB::select($CityQuery);

   }

$result = array();
$final = array();
$totalCities = array();
$LastFinal = array();
$abc = array();

//$result[] = ['In Warehouse','In Warehouse Rate','With Captains'];

foreach($mydata as $data){

        foreach ($data as $key => $value) {

            $result[++$key] = [(int)$value->InWarehouse, (float)$value->InWarehouseRate, (int)$value->WithCaptains,(float)$value->WithCaptainsRate, (int)$value->Undelivered, (float)$value->UndeliveredRate, (int)$value->Returend, (float)$value->ReturendRate, (int)$value->ReturnedtoSupplier, (float)$value->ReturnedtoSupplierRate, (int)$value->Delivered, (float)$value->SuccessRate];

            $final[] = $result[$key];
        }

    }
    
for($count = 0; $count<$citiesCount; $count++){
    $odd = 1; 

    for($newcount=0; $newcount<=5; $newcount++){

    $LastFinal[$count][$newcount] = $final[$count][$newcount+$odd] . '% -' . $final[$count][$newcount*2];

    $values[$count][$newcount] = $final[$count][$newcount*2];
$odd++;
}

}


    $labels = array("In Warehouse", "With Captains", "Undelivered", "Returend", "Returend to Supplier", "Delivered");

    for($i=0; $i<=5; $i++){

        for($j=0; $j<$citiesCount; $j++){
           
        ${'array'.$j}['cols'][] = array('type' => 'string');
        ${'array'.$j}['rows'][] = array('c' => array( array('v'=> $labels[$i].$LastFinal[$j][$i]), array('v'=>$values[$j][$i])) );

        }

       
    }

for($k = 0; $k < $citiesCount; $k++){

$totalCities[]   =  ${'array'.$k} ;

}

//print_r($result[1][5]);exit;

// ${'array'.$j} = $array0, $array1 etc 

    
//return Response::json($totalCities, $cityNames);

return Response::json(array(
    'cities' => $totalCities,
    'citynames' => $cityNames,
));
//$response_array = array('success' => true, 'data' => $result);
//$response = Response::json($response_array, 200);
//return $response;

    }


public function per_city(){

    
    $admin_id = Session::get('admin_id');
    $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '!=', 'All')->orderBy('city', 'asc')->get();
    
    $data = array();
    $perCityQuery = array();
    $cityNames = array();   
$citiesCount = count($adminCities);


foreach($adminCities as $cities){

        if($cities->city == 'All'){
            continue;
        }
$cityNames[] = $cities->city;

        $CityQuery = 'select
sum(status = 3 and undelivered_reason = 0) "InWarehouse",
round((sum(status = 3 and undelivered_reason = 0) / count(*))*100,2) "InWarehouseRate",
sum(status = 4) "WithCaptains",
round((sum(status = 4) / count(*))*100,2) "WithCaptainsRate",
sum(status = 6) Undelivered,
round((sum(status = 6) / count(*))*100,2) "UndeliveredRate",
sum(status = 3 and undelivered_reason <> 0) Returend,
round((sum(status = 3 and undelivered_reason <> 0) / count(*))*100,2) "ReturendRate",
sum(status = 7) "ReturnedtoSupplier",
round((sum(status = 7) / count(*))*100,2) "ReturnedtoSupplierRate",
sum(status = 5) "Delivered",
round((sum(status = 5) / count(*))*100,2) "SuccessRate", 
count(*) "Total Shipments"
from delivery_request where
d_city = "' . $cities->city . '" and status not in (0,2)';


$mydata[] = DB::select($CityQuery);

   }

$result = array();
$final = array();
$totalCities = array();
$LastFinal = array();
$abc = array();

//$result[] = ['In Warehouse','In Warehouse Rate','With Captains'];

foreach($mydata as $data){

        foreach ($data as $key => $value) {

            $result[++$key] = [(int)$value->InWarehouse, (float)$value->InWarehouseRate, (int)$value->WithCaptains,(float)$value->WithCaptainsRate, (int)$value->Undelivered, (float)$value->UndeliveredRate, (int)$value->Returend, (float)$value->ReturendRate, (int)$value->ReturnedtoSupplier, (float)$value->ReturnedtoSupplierRate, (int)$value->Delivered, (float)$value->SuccessRate];

            $final[] = $result[$key];
        }

    }
    
for($count = 0; $count<$citiesCount; $count++){
    $odd = 1; 

    for($newcount=0; $newcount<=5; $newcount++){

    $LastFinal[$count][$newcount] = $final[$count][$newcount+$odd] . '% -' . $final[$count][$newcount*2];

    $values[$count][$newcount] = $final[$count][$newcount*2];
$odd++;
}

}


    $labels = array("In Warehouse", "With Captains", "Undelivered", "Returend", "Returend to Supplier", "Delivered");

    for($i=0; $i<=5; $i++){

        for($j=0; $j<$citiesCount; $j++){
           
        ${'array'.$j}['cols'][] = array('type' => 'string');
        ${'array'.$j}['rows'][] = array('c' => array( array('v'=> $labels[$i].$LastFinal[$j][$i]), array('v'=>$values[$j][$i])) );

        }

       
    }

for($k = 0; $k < $citiesCount; $k++){

$totalCities[]   =  ${'array'.$k} ;

}

//print_r($result[1][5]);exit;

// ${'array'.$j} = $array0, $array1 etc 

    
//return Response::json($totalCities, $cityNames);

return Response::json(array(
    'cities' => $totalCities,
    'citynames' => $cityNames,
));
//$response_array = array('success' => true, 'data' => $result);
//$response = Response::json($response_array, 200);
//return $response;

    }



} // End Main Class 


?>