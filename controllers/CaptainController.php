<?php

class CaptainController extends \BaseController
{
    public function registercaptain()
    {
        try {
            $first_name = Input::get('first_name');
            $last_name = Input::get('last_name');
            $email = Input::get('email');
            $phone = Input::get('phone');
            $sex = Input::get('sex');
            $nationality = Input::get('nationality');
            $district = Input::get('district');
            $city = Input::get('city');
            //$address = Input::get('address');
            $address_latitude = Input::get('address_latitude');
            $address_longitude = Input::get('address_longitude');

            $password = Input::get('password');
            $car_registration_expiry = Input::get('vehicle_registration_expiry');
            $license_expiry_date = Input::get('license_expiry_date');

            /*$vehicle_registration_scan = Input::get('vehicle_registration_scan');
            $vehicle_front_scan = Input::get('vehicle_front_scan');
            $captain_iqama_scan = Input::get('captain_iqama_scan');
            $captain_license_scan = trim(Input::get('captain_license_scan'));*/

            $login_by = Input::get('login_by');
            $device_type = Input::get('device_type');
            $device_token = Input::get('device_token');

            $validator = Validator::make(
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'phone' => $phone,
                    'password' => $password,
                    /*'sex' => $sex,
                    'nationality' => $nationality,*/
                    'district' => $district,
                    'city' => $city,
                    //'address' => $address,
                    /*'address_latitude' => $address_latitude,
                    'address_longitude' => $address_longitude,*/
                    'vehicle_registration_expiry' => $car_registration_expiry,
                    'license_expiry_date' => $license_expiry_date,
                    /*'vehicle_registration_scan' => $vehicle_registration_scan,
                    'vehicle_front_scan' => $vehicle_front_scan,
                    'captain_iqama_scan' => $captain_iqama_scan,
                    'captain_license_scan' => $captain_license_scan,*/
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'password' => 'required',
                    /*'sex' => 'required',
                    'nationality' => 'required',*/
                    'district' => 'required',
                    'city' => 'required',
                    //'address' => 'required',
                    /*'address_latitude' => 'required',
                    'address_longitude' => 'required',*/
                    'vehicle_registration_expiry' => 'required',
                    'license_expiry_date' => 'required',
                    /*'vehicle_registration_scan' => 'required',
                    'vehicle_front_scan' => 'required',
                    'captain_iqama_scan' => 'required',
                    'captain_license_scan' => 'required',*/
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error while during captain registration = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                if (Walker::where('email', '=', $email)->first()) {
                    $response_array = array('success' => false, 'error' => 'Email ID already Registred', 'error_code' => 402);
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                } else {
                    $captain = new Walker();

                    $captain->first_name = $first_name;
                    $captain->last_name = $last_name;
                    $captain->email = $email;
                    $captain->phone = $phone;
                    if (isset($sex)) {
                        $captain->sex = $sex;
                    }
                    $captain->city = $city;
                    if (isset($nationality)) {
                        $captain->nationality = $nationality;
                    }
                    $captain->district = $district;
					if (isset($address)) {
                        $captain->address = $address;
                    }
                    if (isset($address_latitude)) {
                        $captain->address_latitude = $address_latitude;
                    }
                    if (isset($address_longitude)) {
                        $captain->address_longitude = $address_longitude;
                    }

                    $captain->password = Hash::make($password);
                    $captain->car_registration_expiry = $car_registration_expiry;
                    $captain->license_expiry_date = $license_expiry_date;

                    //vehicle_registration_scan Optional
                    if (Request::has('vehicle_registration_scan')) {
                        $vehicle_registration_scan = Input::get('vehicle_registration_scan');
                        $captain->vehicle_registration_scan = $vehicle_registration_scan;
                    }
                    // vehicle_front_scan  Optional
                    if (Request::has('vehicle_front_scan')) {
                        $vehicle_front_scan = Input::get('vehicle_front_scan');
                        $captain->vehicle_front_scan = $vehicle_front_scan;
                    }
                    // captain_license_scan Optional
                    if (Request::has('captain_license_scan')) {
                        $walker_license_scan = trim(Input::get('captain_license_scan'));
                        $captain->captain_license_scan = $walker_license_scan;
                    }
                    //captain_iqama_scan Optional
                    if (Request::has('captain_iqama_scan')) {
                        $walker_iqama_scan = Input::get('captain_iqama_scan');
                        $captain->captain_iqama_scan = $walker_iqama_scan;
                    }


                    $captain->token = generate_token();
                    $captain->token_expiry = generate_expiry();

                    $captain->login_by = $login_by;
                    $captain->device_type = $device_type;
                    $captain->device_token = $device_token;

                    $captain->save();
                    $captain_email = Walker::where('email', '=', $email)->first();
                    if (isset($captain_email)) {
                        $captain_id = $captain_email->id;
                        $captain_token = $captain_email->token;
                        $created_at = date('Y-m-d H:i:s',strtotime($captain_email->created_at));
                    } else {
                        $response_array = array('success' => false, 'error' => 'Captain not found with email ' . $email, 'error_code' => 410);
                        $response_code = 200;
                        $response = Response::json($response_array, $response_code);
                        return $response;
                    }
                    $settings = Settings::where('key', 'admin_email_address')->first();
                    $admin_email = $settings->value;
                    $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($captain->first_name . " " . $captain->last_name), 'web_url' => web_url());
                    $subject = "Welcome to Saee, " . ucwords($captain->first_name . " " . $captain->last_name) . "";
                    email_notification($captain->id, 'walker', $pattern, $subject, 'walker_register', null);

                    $typequery = "select updated_at,created_at,planned_walker as captain_id , IF (status= 4,'Delivered','Assigned') As 'status',cash_on_delivery as'cod' from delivery_request where status in ( 3,4 )and planned_walker <> 0 order by updated_at desc limit 10 ";
                    $delivery_requests = DB::select($typequery);
                    $response_array = array(
                        'success' => true,
                        'captain_id' => $captain_id,
                        'token' => $captain_token,
                        'delivery_requests' => $delivery_requests,
                        'created_at'  => $created_at
                    );
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                }
            }
        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
    public function updatecaptain()
    {
        try {
            $captain_id = Input::get('id');
            $city = Input::get('city');
            $first_name = Input::get('first_name');
            $last_name = Input::get('last_name');
            $email = Input::get('email');
            $phone = Input::get('phone');
            $sex = Input::get('sex');
            $nationality = Input::get('nationality');
            $district = Input::get('district');
            $address = Input::get('address');
            $address_latitude = Input::get('address_latitude');
            $address_longitude = Input::get('address_longitude');
            $car_registration_expiry = Input::get('vehicle_registration_expiry');
            $license_expiry_date = trim(Input::get('license_expiry_date '));

            $vehicle_registration_scan = Input::get('vehicle_registration_scan');
            $vehicle_front_scan = Input::get('vehicle_front_scan');
            $captain_iqama_scan = Input::get('captain_iqama_scan');
            $captain_license_scan = trim(Input::get('captain_license_scan'));

            $login_by = Input::get('login_by');
            $device_type = Input::get('device_type');
            $device_token = Input::get('device_token');
            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'phone' => $phone,
                    'sex' => $sex,
                    'nationality' => $nationality,
                    'district' => $district,
                    'city' => $city,
                    'address' => $address,
                    'address_latitude' => $address_latitude,
                    'address_longitude' => $address_longitude,
                    'car_registration_expiry' => $car_registration_expiry,
                    'license_expiry_date' => $license_expiry_date,
                    'vehicle_registration_scan' => $vehicle_registration_scan,
                    'vehicle_front_scan' => $vehicle_front_scan,
                    'captain_iqama_scan' => $captain_iqama_scan,
                    'captain_license_scan' => $captain_license_scan,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'captain_id' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'sex' => 'required',
                    'nationality' => 'required',
                    'district' => 'required',
                    'city' => 'required',
                    'address' => 'required',
                    'address_latitude' => 'required',
                    'address_longitude' => 'required',
                    'car_registration_expiry' => 'required',
                    'license_expiry_date' => 'required',
                    'vehicle_registration_scan' => 'required',
                    'vehicle_front_scan' => 'required',
                    'captain_iqama_scan' => 'required',
                    'captain_license_scan' => 'required',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error while during captain registration = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $captain = Walker::find($captain_id);
                $captain->first_name = $first_name;
                $captain->last_name = $last_name;
                $captain->email = $email;
                $captain->phone = $phone;
                $captain->sex = $sex;
                $captain->city = $city;
                $captain->nationality = $nationality;
                $captain->district = $district;
                $captain->address = $address;
                $captain->address_latitude = $address_latitude;
                $captain->address_longitude = $address_longitude;
                $captain->car_registration_expiry = $car_registration_expiry;
                $captain->license_expiry_date = $license_expiry_date;

                $captain->vehicle_registration_scan = $vehicle_registration_scan;
                $captain->vehicle_front_scan = $vehicle_front_scan;
                $captain->captain_license_scan = $captain_license_scan;
                $captain->captain_iqama_scan = $captain_iqama_scan;

                $captain->login_by = $login_by;
                $captain->device_type = $device_type;
                $captain->device_token = $device_token;
                $captain->update();

                $response_array = array(
                    'success' => true,
                    'id' => $captain->id,
                    'first_name' => $captain->first_name,
                    'last_name' => $captain->last_name,
                    'phone' => $captain->phone,
                    'email' => $captain->email,
                    'city' => $captain->city,
                    'district' => $captain->address,
                    'nationality' => $captain->country,
                    'login_by' => $captain->login_by,
                    'token' => $captain->token,
                    'device_token' => $captain->device_token,
                    'device_type' => $captain->device_type,
                    'type' => $captain->type,
                    'is_approved' => $captain->is_approved,
                    'is_approved_txt' => 'Approved',
                    'is_available' => $captain->is_active,
                );
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;

            }


        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function getnationalities()
    {
        $nationalities = Nationality::select(['name', 'name_ar'])->get();
        $response_array = array('success' => true, 'nationalities' => $nationalities);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getlatesttransactions()
    {
        try {

            $typequery = "select updated_at,planned_walker as captain_id , IF (status= 4,'Delivered','Assigned') As 'status',cash_on_delivery as'cod' from delivery_request where status in ( 3,4 )and planned_walker <> 0 order by updated_at desc limit 10 ";
            $delivery_requests = DB::select($typequery);
            $response_array = array(
                'success' => true,
                'delivery_requests' => $delivery_requests
            );
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatecaptainhomelocation()
    {
        try {
            $captain_id = Input::get('captain_id');
            $address_latitude = Input::get('address_latitude');
            $address_longitude = Input::get('address_longitude');
            $captain = Walker::find($captain_id);
            if (isset($captain)) {
                $captain->address_latitude = $address_latitude;
                $captain->address_longitude = $address_longitude;
                $captain->isJolly = 1;
                $captain->save();
                $response_array = array(
                    'success' => true,
                    'message' => 'Home location  updated successfully for captain ' . $captain->first_name,
                    'id' => $captain->id,
                    'first_name' => $captain->first_name,
                    'last_name' => $captain->last_name,
                    'phone' => $captain->phone,
                    'city' => $captain->city,
                    'address_latitude' => $address_latitude,
                    'address_longitude' => $address_longitude,
                );
            } else {
                $response_array = array('success' => false, 'error' => 'Captain not found with id ' . $captain_id, 'error_code' => 410);
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function randomdeliveryrequests()
    {
        try {
                    $typequery = "select updated_at,planned_walker as captain_id , IF (status= 4,'Delivered','Assigned') As 'status',cash_on_delivery as'cod' from delivery_request where status in ( 3,4 )and planned_walker <> 0 order by updated_at desc limit 10 ";
                    $delivery_requests = DB::select($typequery);
                    $response_array = array(
                        'success' => true,
                        'delivery_requests' => $delivery_requests
                    );
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;


        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }


    /*public function recent_missions(){
        try {

            $captain_id = Input::get('captain_id');

            $city = Walker::select('city')->where('id', '=', $captain_id)->first()->city;

            $token = Input::get('token');

            $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();

            if ($grouped->isEmpty()) {

            $checkgroup = '0';
             } else {

            $checkgroup = '1';

            }

           
            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                    
                ), array(
                    'captain_id' => 'required',
                    'token'    => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $last15DatesQuery = 'select distinct scheduled_shipment_date from delivery_request where jollychic in (
select waybill from captain_account where captain_account_history_id in (

select id from captain_account_history where captain_id = "'.$captain_id.'" and paid_to_captain = 0

   )) and scheduled_shipment_date is not null
   order by 1 desc';

$last15DatesQuery = DB::select($last15DatesQuery);

foreach ($last15DatesQuery as $value) {
            $last15scheduled_shipment_dates[] = $value->scheduled_shipment_date;
        }
        
                 $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();

                 if($walker){

                     if ($checkgroup == 0) {

                        $recentMissionsQuery = 'select
                                                    COALESCE(RemoteDelivered,0) "remoteDelivered",
                                                    COALESCE(Delivered,0) "delivered",COALESCE(Scanned,0) "scanned",
                                                    COALESCE(round((RemoteDelivered/Scanned)*100),0) "remoteDeliveredRate",
                                                    COALESCE(round((Delivered/Scanned)*100) ,0) "successRate",scheduled_shipment_date as "scheduledShipmentDate"
                                                    from (
                                                    select (select sum(status = 5) from delivery_request c,districts d where a.scheduled_shipment_date = scheduled_shipment_date  
                                                    and confirmed_walker = "' . $captain_id . '" and is_remote = 1 
                                                    AND city = d_city and d_district = SUBSTRING_INDEX(district, "+", -1)) "remoteDelivered",
                                                    sum(status=5) "Delivered",(select sum(status = 4) from orders_history where date(created_at) = scheduled_shipment_date  
                                                    and walker_id = "' . $captain_id . '") "Scanned",scheduled_shipment_date  from delivery_request a where d_city = "' . $city . '"  
                                                    and scheduled_shipment_date is not null and confirmed_walker = "' . $captain_id . '"
                                                    and scheduled_shipment_date in ( "' . implode('", "', $last15scheduled_shipment_dates) . '" )
                                                    group by scheduled_shipment_date order by scheduled_shipment_date desc ) A';
                    } else {

                        $recentMissionsQuery = 'select
                                                      COALESCE(RemoteDelivered,0) "remoteDelivered",
                                                      COALESCE(Delivered,0) "delivered",COALESCE(Scanned,0) "scanned",
                                                      COALESCE(round((RemoteDelivered/Scanned)*100),0) "remoteDeliveredRate",
                                                      COALESCE(round((Delivered/Scanned)*100) ,0) "successRate",scheduled_shipment_date as "scheduledShipmentDate"
                                                      from (
                                                      select (select sum(status = 5) from delivery_request c,districts d where a.scheduled_shipment_date = scheduled_shipment_date  
                                                      and confirmed_walker = "' . $captain_id . '" and is_remote = 1 
                                                      AND city = "'.$city.'" and CONCAT(d_city,"+",d_district) LIKE district  ) "remoteDelivered",
                                                      sum(status=5) "Delivered",(select sum(status = 4) from orders_history where date(created_at) = scheduled_shipment_date  
                                                      and walker_id = "' . $captain_id . '") "Scanned",scheduled_shipment_date  from delivery_request a, 
                                                      (select DISTINCT SUBSTRING_INDEX(district, "+", 1) "subcity" from districts where city = "' . $city . '") b 
                                                      where d_city = subcity  and scheduled_shipment_date is not null and confirmed_walker = "' . $captain_id . '"
                                                      and scheduled_shipment_date in ( "' . implode('", "', $last15scheduled_shipment_dates) . '" )
                                                      group by scheduled_shipment_date order by scheduled_shipment_date desc ) A';
                    }

        $recent_missions = DB::select($recentMissionsQuery);

         $recentMissions = array();

         foreach ($recent_missions as $mission) {

            array_push($recentMissions, array(
                        'remoteDelivered' => (int) $mission->remoteDelivered,
                        'delivered' => (int) $mission->delivered,
                        'scanned' => (int) $mission->scanned,
                        'remoteDeliveredRate' => (int) $mission->remoteDeliveredRate,
                        'successRate' => (int) $mission->successRate,
                        'scheduledShipmentDate' => $mission->scheduledShipmentDate
                    ));
         }
                $response_array = array(
                    'success' => true,
                    'recentMissions' => $recentMissions
                );
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;

            }
            else{

                $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                        'error_code' => 406 
                    );

                 return $response = Response::json($response_array, 200);
            }

        }


        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }*/
	
	public function recent_missions()
    {
        try {

            $captain_id = Input::get('captain_id');

            $city = Walker::select('city')->where('id', '=', $captain_id)->first()->city;

            $token = Input::get('token');

            $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();

            if ($grouped->isEmpty()) {

                $checkgroup = '0';
            } else {

                $checkgroup = '1';

            }


            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,

                ), array(
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                $last15DatesQuery = 'select distinct scheduled_shipment_date from delivery_request where jollychic in (
select waybill from captain_account where captain_account_history_id in (

select id from captain_account_history where captain_id = "'.$captain_id.'" and paid_to_captain = 0

   )) and scheduled_shipment_date is not null
   order by 1 desc';

                $last15DatesQuery = DB::select($last15DatesQuery);
                $recentMissions = array();
                foreach ($last15DatesQuery as $value) {
                    //$last15scheduled_shipment_dates[] = $value->scheduled_shipment_date;
                    array_push($recentMissions, array(
                        'remoteDelivered' => 0,
                        'delivered' => 0,
                        'scanned' => 0,
                        'remoteDeliveredRate' => 0,
                        'successRate' => 0,
                        'scheduledShipmentDate' => $value->scheduled_shipment_date
                    ));
                }

                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();

                if ($walker) {

                    $response_array = array(
                        'success' => true,
                        'recentMissions' => $recentMissions
                    );
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;

                } else {

                    $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                        'error_code' => 406
                    );

                    return $response = Response::json($response_array, 200);
                }

            }


        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }



    public function accomplished_missions(){
        try {

            $captain_id = Input::get('captain_id');

            $token = Input::get('token');

            $scheduled_shipment_date  = Input::get('scheduled_shipment_date');

           
            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                    'scheduled_shipment_date' => $scheduled_shipment_date
                    
                ), array(
                    'captain_id' => 'required',
                    'token'    => 'required',
                    'scheduled_shipment_date' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {

                 $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();

                 if($walker){

            

     $accomplishedMissionsQuery = 'select jollychic as waybill, company_name as companyName, scheduled_shipment_date as scheduledShipmentDate, d_district as district, b.delivery_fee as deliveryFee from delivery_request a, captain_account b, companies c where a.jollychic = b.waybill and scheduled_shipment_date = "'.$scheduled_shipment_date.'" and confirmed_walker = "'.$captain_id.'" and confirmed_walker = b.captain_id
and a.company_id = c.id
order by 4
';


        $accomplished_missions = DB::select($accomplishedMissionsQuery);
                $response_array = array(
                    'success' => true,
                    'accomplishedMissions' => $accomplished_missions
                );
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;

            }
            else{

                $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                        'error_code' => 406 
                    );

                 return $response = Response::json($response_array, 200);
            }

        }


        } catch (Exception $e) {
            $response_array = array(
                'success' => false,
                'error' => 'Failed, Please try again',
            );
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

public function getnotifications()
    {
        try {
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    $orders = Notification::where('captain_id', '=', $captain_id)->whereIn('is_read', [0, 1])->orderBy('created_at', 'desc')->get();
                    if (count($orders)) {
                        $items = array();
                        foreach ($orders as $item) {
                            array_push($items, array(
                                'id' => $item->id,
                                'captainId' => $item->captain_id,
                                //'message' => $item->message,
								'message' =>  json_decode($item->message)?json_decode($item->message): $item->message,
                                'isRead' => $item->is_read,
                                'createdAt' => date('Y-m-d H:i:s', strtotime($item->created_at))
                            ));
                        }
                        $response_array = array(
                            'success' => true,
                            'notifications' => $items
                        );
                    } else {
                        $response_array = array('success' => false, 'error' => 'notification not found', 'error_code' => 410);
                    }
                    $response_code = 200;
                } else{
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }

            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function updatenotifications()
    {
        try {
            $id = Input::get('id');
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'id' => $id,
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'id' => 'required',
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    $order = Notification::where('id', '=', $id)->where('captain_id', '=', $captain_id)->where('is_read', '=', 0)->update(array('is_read' => 1));
                    if ($order > 0) {
                        $response_array = array(
                            'success' => true,
                            'message' => 'Read status update'
                        );
						$walker->unread_notifications = $walker->unread_notifications - 1;
                        $walker->save();

                    } else {
                        $response_array = array('success' => false, 'error' => 'Read status not update, somthing went wrong', 'error_code' => 410);
                    }
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }

            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

public function clearAllNotifications()
    {
        try {
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    $order = Notification::where('captain_id', '=', $captain_id)->whereIn('is_read', [0, 1])->update(array('is_read' => 2));
                    if ($order > 0) {
                        $response_array = array(
                            'success' => true,
                            'message' => 'status cleared successfully'
                        );

                    } else {
                        $response_array = array('success' => false, 'error' => 'status not cleared, somthing went wrong', 'error_code' => 410);
                    }
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }

            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

     public function statusRollback() {

        try {

            
            $statusTo = 4;
            $ids = explode(',', Request::input('waybills'));
            $singleid = $ids[0];
            $captainId = Request::input('captain_id');
            $token =  Request::input('token');

            $validator = Validator::make(
                array(
                   
                    'waybill' => $singleid,
                    'captain_id' => $captainId,
                    'token' => $token,
                    
                ), array(
                    'waybill'    => 'required',
                    'captain_id'  => 'required',
                    'token'      => 'required',
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            }

            $walker = Walker::where('id', '=', $captainId)->where('token', '=', $token)->first();

            if($walker){

            $success = $faild = array();
            
            foreach($ids as $id) {

                $statusFrom = DeliveryRequest::select('status')->where('jollychic', '=', $id)->first();

                $statusFrom = $statusFrom->status;

                if(!($shipment = DeliveryRequest::where('jollychic', '=', $id)->where('status', '=', $statusFrom)->first())) {
                    array_push($faild, [
                        'id' => $id,
                        'reason' => 'Wrong Status'
                    ]);
                    continue;
                }
                if($statusFrom == '6' && $statusTo == '4') { // undelivered -> with captain
                    // count number of shipment's records with status = 6 in orders_history 
                    $count = OrdersHistory::where('waybill_number', '=', $id)->where('status', '=', '6')->count();
                    if($count < 1) { // if there is no failed records in orders history
                        DB::transaction(function() use ($id, $shipment) { 
                            // update delivery request
                            DeliveryRequest::where('jollychic', '=', $id)->update(array(
                                'status' => '4',
                                'num_faild_delivery_attempts' => max(0, $shipment->num_faild_delivery_attempts - 1),
                                'undelivered_reason' => '0'
                            ));
                        });
                        array_push($success, $id);
                    }
                    else if($count == 1) { // only one failed record
                        DB::transaction(function() use ($id, $shipment) { 
                            // remove the only failed record in orders histroy
                            OrdersHistory::where('waybill_number', '=', $id)->where('status', '=', '6')->delete();
                            // update delivery request
                            DeliveryRequest::where('jollychic', '=', $id)->update(array(
                                'status' => '4',
                                'num_faild_delivery_attempts' => max(0, $shipment->num_faild_delivery_attempts - 1),
                                'undelivered_reason' => '0'
                            ));
                        });
                        array_push($success, $id);
                    }
                    else { // more than one failed records
                        DB::transaction(function() use ($id, $shipment) { 
                            // delete last failed record in orders histroy
                            DB::delete("DELETE FROM orders_history WHERE waybill_number = ? AND STATUS = 6 AND created_at = (SELECT max(created_at) FROM (SELECT * FROM orders_history) orders_history WHERE waybill_number = ? AND STATUS = 6)", [$id, $id]);
                            // get last failed record now in orders histroy
                            $last_faild = DB::select("select * from orders_history where waybill_number = '$id' and status = 6 and created_at = (select max(created_at) from orders_history where waybill_number = '$id' and status = 6)")[0];
                            // get reason of the last failed record
                            $undelivered_reason_id = UndeliveredReason::where('english', '=', $last_faild->notes)->orWhere('arabic', '=', $last_faild->notes)->first();
                            // update delivery reqeust
                            DeliveryRequest::where('jollychic', '=', $id)->update(array(
                                'status' => '4',
                                'num_faild_delivery_attempts' => max(0, $shipment->num_faild_delivery_attempts - 1),
                                'undelivered_reason' => isset($undelivered_reason_id->id) ? $undelivered_reason_id->id : '0'
                            ));
                        });
                        array_push($success, $id);
                    }
                }
                else if($statusFrom == '5' && $statusTo == '4') { // delivered -> with captain
                    if($shipment->is_money_received == '1') {
                        array_push($faild, [
                            'id' => $id,
                            'reason' => 'Money Received'
                        ]);
                        continue;
                    }

                    if($shipment->company_id == '911') {
                        array_push($faild, [
                            'id' => $id,
                            'reason' => 'Rollback not accepted by this company'
                        ]);
                        continue;
                    }
                    DB::transaction(function() use ($id, $shipment) { 
                        // delete last failed record in orders histroy
                        DB::delete("DELETE FROM orders_history WHERE waybill_number = ? AND STATUS = 5", [$id]);
                        // update delivery reqeust
                        DeliveryRequest::where('jollychic', '=', $id)->update(array(
                            'status' => '4',
                            'dropoff_timestamp' => $shipment->arrival_timestamp
                        ));
                        // decrease captain's total delivered by one
                        Walker::where('id', '=', $shipment->confirmed_walker)->decrement('total_delivered');
                    });
                    array_push($success, $id);
                }
                else {
                    array_push($faild, [
                        'id' => $id,
                        'reason' => 'Not Allowed'
                    ]);
                }

            } // end foreach
            } // end if walker

            else {

           $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                        'error_code' => 406 
                    );

                 return $response = Response::json($response_array, 200);

        }

            $response_array = array('success' => true, 'successItems' => $success, 'failedItems' => $faild);
            $response = Response::json($response_array, 200);
            return $response;
            
        } catch (Exception $e) {
            
            $response_array = array('success' => false, 'error', $e->getMessage(), 'line' => $e->getLine());
            $response = Response::json($response_array, 200);
            return $response;
            
        }
    }
	
	public function getmissionsforcaptain()
    {
        try {
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'captain_id' => 'required',
                    'token' => 'required',
                ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    //New misions for captain
                    $newMissions = Mission::where('reserve_captain', '=', 0)->where('planned_captain', '=', 0)->where('is_notify','=',1)->where('status','=',0)->where('city','=',$walker->city)->orderBy('created_at', 'desc')->get();
                    if (count($newMissions)) {
                        $new_items = array();
                        foreach ($newMissions as $item) {
                            array_push($new_items, array(
                                'missionId' => $item->id,
                                'missionName' => $item->mission_name,
                                'city' =>  $item->city,
                                'district' =>  $item->district,
                                'totalShipments' =>  $item->total_shipments,
								'estimatedDeliveryFee' =>  round($item->estimated_delivery_fee),
								 'scheduledShipmentDate' => !empty($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
								 'suggestedPickupTime' => !empty($item->suggested_pickup_time)?$item->suggested_pickup_time:'',
                                'city' => $item->city,
                                'createdAt' => date('Y-m-d H:i:s', strtotime($item->created_at))
                            ));
                        }

                    } else {
                        $new_items = array();
                    }

                    //Reserve misions for captain
                    $reserveMissions = Mission::where('reserve_captain', '=', $walker->id)->where('planned_captain', '=', 0)->where('is_notify','=',1)->where('status','=',1)->where('city','=',$walker->city)->orderBy('created_at', 'desc')->get();
                    if (count($reserveMissions)) {
                        $reserve_items = array();
                        foreach ($reserveMissions as $item) {
                            array_push($reserve_items, array(
                                'missionId' => $item->id,
                                'missionName' => $item->mission_name,
                                'city' =>  $item->city,
                                'district' =>  $item->district,
                                'totalShipments' =>  $item->total_shipments,
								'estimatedDeliveryFee' =>  round($item->estimated_delivery_fee),
								'scheduledShipmentDate' => !empty($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
								'suggestedPickupTime' => !empty($item->suggested_pickup_time)?$item->suggested_pickup_time:'',
                                'city' => $item->city,
                                'createdAt' => date('Y-m-d H:i:s', strtotime($item->created_at))
                            ));
                        }

                    } else {
                        $reserve_items = array();
                    }

                    //plan misions for captain
                    $planMissions = Mission::where('reserve_captain', '=', $walker->id)->where('planned_captain', '=', $walker->id)->where('is_notify','=',1)->where('status','=',2)
                        ->where('city','=',$walker->city)->orderBy('created_at', 'desc')->get();
                    if (count($planMissions)) {
                        $plan_items = array();
                        foreach ($planMissions as $item) {
                            array_push($plan_items, array(
                                'missionId' => $item->id,
                                'missionName' => $item->mission_name,
                                'city' =>  $item->city,
                                'district' =>  $item->district,
                                'totalShipments' =>  $item->total_shipments,
								'estimatedDeliveryFee' =>  round($item->estimated_delivery_fee),
								'scheduledShipmentDate' => !empty($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
								'suggestedPickupTime' => !empty($item->suggested_pickup_time)?$item->suggested_pickup_time:'',
                                'city' => $item->city,
                                'createdAt' => date('Y-m-d H:i:s', strtotime($item->created_at))
                            ));
                        }

                    } else {
                        $plan_items = array();
                    }

                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                }
            }
            $response_array = array('success' => true,'plannedMissions' => $plan_items, 'reservedMissions' => $reserve_items, 'newMissions' => $new_items);
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again'.$e);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
	
    public function acceptmission()
    {
        try {
            $id = Input::get('mission_id');
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'mission_id' => $id,
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'mission_id' => 'required',
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    $captain_maxShipments = $walker->max_shipments;
                    $with_captain = DeliveryRequest::where('status','<=',4)->where('planned_walker', '=', $captain_id)->count();

                    if($captain_maxShipments > $with_captain) {
                        $remaining_shipments = $captain_maxShipments - $with_captain;
                        $checkMission = Mission::where('id', '=', $id)->where('city', '=', $walker->city)->where('is_notify','=',1)
                            ->where(function ($q) use ($captain_id) {
                                $q->where('reserve_captain', '=', $captain_id)->orwhere('reserve_captain', '=', 0);
                            })->where('planned_captain', '=', 0)->first();
                        if(empty($checkMission)){
                            $response_array = array('success' => false, 'error' => 'No mission found', 'error_code' => 406);
                            $response_code = 200;
                            $response = Response::json($response_array, $response_code);
                            return $response;
                        }

                        $Mission_totalShipments = $checkMission->total_shipments;

                        if ($remaining_shipments >= $Mission_totalShipments) {
                            $order = Mission::where('id', '=', $id)->where('city', '=', $walker->city)
                                ->where(function ($q) use ($captain_id) {
                                    $q->where('reserve_captain', '=', $captain_id)->orwhere('reserve_captain', '=', 0);
                                })->where('planned_captain', '=', 0)
                                ->update(array('reserve_captain' => $walker->id, 'planned_captain' => $walker->id,'status' => 2));
                            if ($order > 0) {
                                DeliveryRequest::where('mission_id', '=', $id)->wherein('status', [1,2, 3])->update(array('planned_walker' => $walker->id));
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Mission reserve for captain \'' . $walker->first_name . ' ' . $walker->last_name . '\'');
                            } else {
                                $response_array = array('success' => false, 'error' => 'Mission not reserve, somthing went wrong', 'error_code' => 410);
                            }

                        } else {
                            $new_mission_shipments = $Mission_totalShipments - $remaining_shipments;
                            $district = $checkMission->district;
                            $isRemote = Districts::select('is_remote')->where('city', '=', $walker->city)->where('district', 'like', '%' . $district)
                                ->orWhere('district_ar', 'like', '%' . $district)->first();
                            if (!empty($isRemote) && $isRemote->is_remote == 1) {
                                $estimated_delivery_fee = 10;
                            } else {
                                $estimated_delivery_fee = 9;
                            }

                            $flag =  Mission::where('id', '=', $id)->where('city', '=', $walker->city)->where('city', '=', $walker->city)
                                ->where(function ($q) use ($captain_id) {
                                    $q->where('reserve_captain', '=', $captain_id)->orwhere('reserve_captain', '=', 0);
                                })->where('planned_captain', '=', 0)->where('is_notify','=',1)
                                ->update(array('reserve_captain' => $walker->id, 'planned_captain' => $walker->id, 'total_shipments' => $remaining_shipments,
                                    'estimated_delivery_fee' =>$estimated_delivery_fee * $remaining_shipments,
                                    'mission_name'=>$district . ' District-' . $remaining_shipments,'status' => 2));

                            $scheduled_shipment_date = $checkMission->scheduled_shipment_date;

                            $misssion_name = $district . ' District-' . $new_mission_shipments;

                            $newmission = new Mission();
                            $newmission->city = $checkMission->city;
                            $newmission->mission_name = $misssion_name;
                            $newmission->district = $checkMission->district;
                            $newmission->total_shipments = $new_mission_shipments;
                            $newmission->estimated_delivery_fee = $estimated_delivery_fee * $new_mission_shipments;
                            if ($scheduled_shipment_date != '') {
                                $newmission->scheduled_shipment_date = $scheduled_shipment_date;
                            }
                            $newmission->save();

                            $jollycic_ids_query = DeliveryRequest::select('jollychic')->where('mission_id', '=', $id)->wherein('status', [1,2,3]);
                            if ($scheduled_shipment_date != '') {
                                $jollycic_ids_query->whereDate('scheduled_shipment_date', '=', $scheduled_shipment_date);
                            }
                            $jollycic_ids = $jollycic_ids_query->get();

                            for ($a = 0; $a < count($new_mission_shipments); ++$a) {
                                $jollychic = $jollycic_ids[$a]->jollychic;
                                Log::info($jollychic);
                                $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->wherein('status', [1,2, 3])->where('mission_id', '=', $id)
                                    ->update(array('mission_id' => $newmission->id));
                            }
                            if ($flag > 0) {
                                DeliveryRequest::where('mission_id', '=', $id)->wherein('status', [1,2, 3])->update(array('planned_walker' => $captain_id));
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Mission reserve for captain \'' . $walker->first_name . ' ' . $walker->last_name . '\'');
                            } else {
                                $response_array = array('success' => false, 'error' => 'Mission not reserve, somthing went wrong', 'error_code' => 410);
                            }

                        }


                    } else {
                        $response_array = array('success' => false, 'error' => 'captain have max shipments, please release them first', 'error_code' => 410);
                    }
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }
            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
	public function rejectmission()
    {
        try {
            $id = Input::get('mission_id');
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'mission_id' => $id,
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'mission_id' => 'required',
                    'captain_id' => 'required',
                    'token' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                            $order = Mission::where('id', '=', $id)->where('city', '=', $walker->city)->where('is_notify','=',1)
                                ->where(function ($q) use ($captain_id) {
                                    $q->where('reserve_captain', '=', $captain_id)->orwhere('reserve_captain', '=', 0);
                                })->where('planned_captain', '=', 0)
                                ->update(array('reserve_captain' => 0, 'planned_captain' => 0,'status' => 0,'is_notify' =>0));
                            if ($order > 0) {
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Mission have been rejected for captain \'' . $walker->first_name . ' ' . $walker->last_name . '\'');
                            } else {
                                $response_array = array('success' => false, 'error' => 'Mission did not reject, somthing went wrong', 'error_code' => 410);
                            }

                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }
            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
	
	public function getmissionshipments()
    {
        try {
            $mission_id = Input::get('mission_id');
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');
            $validator = Validator::make(
                array(
                    'mission_id' => $mission_id,
                    'captain_id' => $captain_id,
                    'token' => $token
                ), array(
                'mission_id' => 'required',
                'captain_id' => 'required',
                    'token' => 'required'
            ));
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    $mission_shipments = DeliveryRequest::select('delivery_request.id', 'mission_id', 'jollychic', 'order_number', 'sender_name', 'company_name', 'receiver_name',
                        'receiver_phone', 'receiver_phone2', 'd_state', 'd_city', 'd_zipcode', 'd_district', 'd_address', 'd_address2', 'D_latitude', 'D_longitude',
                        'shipment_id', 'sender_name', 'sender_phone', 'cash_on_delivery', 'status', 'urgent_delivery')->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                        ->where('mission_id', '=', $mission_id)->wherein('status', [2, 3])->get();
                    if (count($mission_shipments)) {
                        $items = array();
                        foreach ($mission_shipments as $item) {
                            array_push($items, array(
                                'id' => $item->id,
                                'mission_d' => $item->mission_id,
                                'jollychic_id' => $item->jollychic,
                                'order_number' => $item->order_number,
                                'company_name' => !empty($item->sender_name)?$item->company_name.' ['.$item->sender_name.']':$item->company_name,
                                'receiver_name' => $item->receiver_name,
                                'receiver_phone' => $item->receiver_phone,
                                'receiver_phone2' => $item->receiver_phone2,
                                'receiver_state' => $item->d_state,
                                'receiver_city' => $item->d_city,
                                'receiver_zipcode' => $item->d_zipcode,
                                'receiver_district' => $item->d_district,
                                'receiver_address' => $item->d_address,
                                'receiver_address2' => $item->d_address2,
                                'receiver_address3' => $item->d_address3,
                                'receiver_lat' => $item->D_latitude,
                                'receiver_lng' => $item->D_longitude,
                                'shipment_id' => $item->shipment_id,
                                'sender_name' => $item->sender_name,
                                'sender_phone' => $item->sender_phone,
                                'cash_on_delivery' => $item->cash_on_delivery,
                                'status' => $item->status,
                                'urgent_delivery' => $item->urgent_delivery
                            ));
                        }

                        $response_array = array(
                            'success' => true, 'missionShipments' => $items);
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Item not found', 'error_code' => 410);
                        $response_code = 200;
                        $response = Response::json($response_array, $response_code);
                        return $response;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }
            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again'.$e);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
	
	public function logout()
    {
        try {
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'captain_id' => $captain_id,
                    'token' => $token,
                ), array(
                    'captain_id' => 'required|integer',
                    'token' => 'required',)
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();
                if ($walker) {
                    if (is_token_active($walker->token_expiry)) {
                        $walker->latitude = 0;
                        $walker->longitude = 0;
                        $walker->old_latitude = 0;
                        $walker->old_longitude = 0;
                        $walker->device_token = 0;
                        $walker->is_active = 0;
                        $walker->is_active_monthly = 0;
                        $walker->save();

                        $response_array = array('success' => true, 'message' => 'Successfully Log out');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid captain or token mismatch!', 'error_code' => 406);
                    $response_code = 200;
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => 'Failed, Please try again'.$e);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

}