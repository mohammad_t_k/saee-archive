<?php

use GuzzleHttp\Client;

//use GuzzleHttp\Message\Request;
//use GuzzleHttp\Message\Response;

class WalkerController extends BaseController
{

    /*public function __construct() {
        $this->afterFilter("Content-Type", ["application/json"]);
    }*/

    public function isAdmin($token)
    {
        return false;
    }

    public function getWalkerData($walker_id, $token, $is_admin)
    {

        if ($walker_data = Walker::where('token', '=', $token)->where('id', '=', $walker_id)->first()) {
            return $walker_data;
        } elseif ($is_admin) {
            $walker_data = Walker::where('id', '=', $walker_id)->first();
            if (!$walker_data) {
                return false;
            }
            return $walker_data;
        } else {
            return false;
        }
    }

    public function get_app_version()
    {
        $android_captain_app = Settings::where("key", '=', 'android_app_version_cap')->first();
        $response_array = array('success' => true, 'android' => $android_captain_app->value);
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function view_profile()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $totalrequestssent = RequestMeta::where("walker_id", '=', $walker_id)->where("status", '<>', '0')->where("is_cancelled", '=', '0')->count();
                    $totalaccepted = RequestMeta::where("walker_id", '=', $walker_id)->where("status", '=', '1')->where("is_cancelled", '=', '0')->count();
                    $totalondemand = Requests::where("confirmed_walker", '=', $walker_id)->where("status", '=', '1')->where("service_type", '=', '1')->where("is_cancelled", '=', '0')->count();
                    $totalmonthly = Requests::where("confirmed_walker", '=', $walker_id)->where("status", '=', '1')->where("service_type", '=', '2')->where("monthly_id", '=', '0')->where("is_cancelled", '=', '0')->count();

                    $walker = array();
                    $walker['rate'] = $walker_data->rate;
                    $walker['credit'] = $walker_data->credit;
                    $walker['totalrequestssent'] = $totalrequestssent;
                    $walker['totalaccepted'] = $totalaccepted;
                    $walker['totalondemand'] = $totalondemand;
                    $walker['totalmonthly'] = $totalmonthly;
                    $walker['acceptanceratio'] = round($totalaccepted / $totalrequestssent * 100);
                    $walker['available_hours'] = $walker_data->total_online_hours;
                    $walker['total_cash_recieved'] = Requests::where("confirmed_walker", '=', $walker_id)->where("status", '=', '1')->where("service_type", '=', '1')->where("is_cancelled", '=', '0')->where("is_cash_posted_cap", '=', '1')->sum('cash_paid');
                    if ($walker_data->credit > 0) {
                        $walker['payable_to_kasper'] = 0;
                        $walker['payable_to_captain'] = $walker_data->credit;
                    } else {
                        $walker['payable_to_kasper'] = ($walker_data->credit * -1);
                        $walker['payable_to_captain'] = 0;
                    }

                    $response_array = array('success' => true, 'walker_data' => $walker);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID is not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID is not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function heart_beat()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    Walker::where('id', '=', $walker_id)->update(array('heart_beat' => date("Y-m-d H:i:s")));
                    $response_array = array('success' => true, 'heart_beat' => true);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID is not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID is not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function register()
    {
        $first_name = ucwords(trim(Input::get('first_name')));
        $last_name = ucwords(trim(Input::get('last_name')));
        $email = Input::get('email');
        $phone = Input::get('phone');
        $password = Input::get('password');
        $type = Input::get('type');
        $picture = Input::file('picture');

        if (Input::hasfile('picture')) {
            $picture = Input::file('picture');
        } else {
            $picture = "";
        }

        $device_token = Input::get('device_token');
        $device_type = Input::get('device_type');
        $bio = Input::get('bio');
        $address = ucwords(trim(Input::get('address')));
        $state = ucwords(trim(Input::get('state')));
        $country = ucwords(trim(Input::get('country')));
        $zipcode = 0;
        if (Input::has('zipcode')) {
            $zipcode = Input::get('zipcode');
        }
        $login_by = Input::get('login_by');
        $car_model = 0;
        if (Input::has('car_model')) {
            $car_model = ucwords(trim(Input::get('car_model')));
        }
        $car_number = 0;
        if (Input::has('car_number')) {
            $car_number = Input::get('car_number');
        }
        $social_unique_id = Input::get('social_unique_id');

        if ($password != "" and $social_unique_id == "") {
            $validator = Validator::make(
                array(
                    'password' => $password,
                    'email' => $email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'picture' => $picture,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'zipcode' => $zipcode,
                    'login_by' => $login_by),
                array(
                    'password' => 'required',
                    'email' => 'required|email',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    /* 'picture' => 'required|mimes:jpeg,bmp,png', */
                    'picture' => '',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'zipcode' => 'integer',
                    'login_by' => 'required|in:manual,facebook,google',
                )
            );

            $validatorPhone = Validator::make(
                array(
                    'phone' => $phone,
                ), array(
                    'phone' => 'phone'
                )
            );
            $social = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                ), array(
                    'social_unique_id' => 'unique:owner'
                )
            );
        } elseif ($social_unique_id != "" and $password == "") {
            $validator = Validator::make(
                array(
                    'email' => $email,
                    'phone' => $phone,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'picture' => $picture,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'bio' => $bio,
                    'address' => $address,
                    'state' => $state,
                    'country' => $country,
                    'zipcode' => $zipcode,
                    'login_by' => $login_by,
                    'social_unique_id' => $social_unique_id
                ), array(
                    'email' => 'required|email',
                    'phone' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    /* 'picture' => 'required|mimes:jpeg,bmp,png', */
                    'picture' => '',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'bio' => '',
                    'address' => '',
                    'state' => '',
                    'country' => '',
                    'zipcode' => 'integer',
                    'login_by' => 'required|in:manual,facebook,google',
                    'social_unique_id' => 'required|unique:walker'
                )
            );

            $social = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                ), array(
                    'social_unique_id' => 'required|unique:owner'
                )
            );

            $validatorPhone = Validator::make(
                array(
                    'phone' => $phone,
                ), array(
                    'phone' => 'phone'
                )
            );
        } elseif ($social_unique_id != "" and $password != "") {
            $response_array = array('success' => false, 'error' => 'Invalid Input - either social_unique_id or password should be passed', 'error_code' => 401);
            $response_code = 200;
            goto response;
        }

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            Log::info('Error while during walker registration = ' . print_r($error_messages, true));
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else if ($validatorPhone->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Phone Number', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else if ($social->fails()) {
            $error_messages = $social->messages()->all();
            $response_array = array('success' => false, 'error' => 'The social unique id has already been taken.', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            if (Walker::where('email', '=', $email)->first()) {
                $response_array = array('success' => false, 'error' => 'Email ID already Registred', 'error_code' => 402);
                $response_code = 200;
            } else {

                if (!$type) {
                    // choose default type
                    $provider_type = ProviderType::where('is_default', 1)->first();

                    if (!$provider_type) {
                        $type = 0;
                    } else {
                        $type = $provider_type->id;
                    }
                }
                $activation_code = uniqid();

                $walker = new Walker;
                $walker->first_name = $first_name;
                $walker->last_name = $last_name;
                $walker->email = $email;
                $walker->phone = $phone;
                $walker->bb_token = generate_token();
                $walker->activation_code = $activation_code;
                $walker->email_activation = 1;
                if ($password != "") {
                    $walker->password = Hash::make($password);
                }
                $walker->token = generate_token();
                $walker->token_expiry = generate_expiry();
                // upload image
                $file_name = time();
                $file_name .= rand();
                $file_name = sha1($file_name);

                $s3_url = "";
                if (Input::hasfile('picture')) {
                    $ext = Input::file('picture')->getClientOriginalExtension();
                    Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;

                    // Upload to S3
                    if (Config::get('app.s3_bucket') != "") {
                        $s3 = App::make('aws')->get('s3');
                        $pic = $s3->putObject(array(
                            'Bucket' => Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                        ));

                        $s3->putObjectAcl(array(
                            'Bucket' => Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'ACL' => 'public-read'
                        ));

                        $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                    } else {
                        $s3_url = asset_url() . '/uploads/' . $local_url;
                    }
                }
                $walker->picture = $s3_url;
                $walker->device_token = $device_token;
                $walker->device_type = $device_type;
                $walker->bio = $bio;
                $walker->address = $address;
                $walker->state = $state;
                $walker->country = $country;
                $walker->zipcode = $zipcode;
                $walker->login_by = $login_by;
                $walker->is_available = 1;
                $walker->is_active = 0;
                $walker->is_approved = 0;
                $walker->type = $type;
                $walker->car_model = $car_model;
                $walker->car_number = $car_number;
                $password = my_random6_number();
                if ($social_unique_id != "") {
                    $walker->social_unique_id = $social_unique_id;
                    $walker->password = Hash::make($password);
                }
                $walker->timezone = "UTC";
                If (Input::has('timezone')) {
                    $walker->timezone = Input::get('timezone');
                }

                $walker->save();
                if ($social_unique_id != "") {
                    $pattern = "Hello... ! " . ucwords($first_name) . ". Your " . Config::get('app.website_title') . " Web Login Password is : " . $password;
                    sms_notification($walker->id, 'walker', $pattern);
                    $subject = "Your " . Config::get('app.website_title') . " Web Login Password";
                    email_notification($walker->id, 'walker', $pattern, $subject);
                }
                if (Input::has('type') != NULL) {
                    $ke = Input::get('type');
                    $proviserv = ProviderServices::where('provider_id', $walker->id)->first();
                    if ($proviserv != NULL) {
                        DB::delete("delete from walker_services where provider_id = '" . $walker->id . "';");
                    }
                    $base_price = Input::get('service_base_price');
                    $service_price_distance = Input::get('service_price_distance');
                    $service_price_time = Input::get('service_price_time');

                    $type = Input::get('type');
                    $myType = explode(',', $type);
                    $cnkey = count($myType);

                    if (Input::has('service_base_price')) {
                        $base_price = Input::get('service_base_price');
                        $base_price_array = explode(',', $base_price);
                    }

                    Log::info('cnkey = ' . print_r($cnkey, true));
                    for ($i = 0; $i < $cnkey; $i++) {
                        $key = $myType[$i];
                        $prserv = new ProviderServices;
                        $prserv->provider_id = $walker->id;
                        $prserv->type = $key;
                        Log::info('key = ' . print_r($key, true));

                        if (Input::has('service_base_price')) {

                            $prserv->base_price = $base_price_array[$i];
                        } else {
                            $prserv->base_price = 0;
                        }
                        if (Input::has('service_price_distance')) {
                            $prserv->price_per_unit_distance = $service_price_distance[$i];
                        } else {
                            $prserv->price_per_unit_distance = 0;
                        }
                        if (Input::has('service_price_time')) {
                            $prserv->price_per_unit_time = $service_price_time[$i];
                        } else {
                            $prserv->price_per_unit_distance = 0;
                        }
                        $prserv->save();
                    }
                }
                /* $subject = "Welcome On Board";
                  $email_data['name'] = $walker->first_name;
                  $url = URL::to('/provider/activation') . '/' . $activation_code;
                  $email_data['url'] = $url;
                  send_email($walker->id, 'walker', $email_data, $subject, 'providerregister'); */
                $settings = Settings::where('key', 'admin_email_address')->first();
                $admin_email = $settings->value;
                $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($walker->first_name . " " . $walker->last_name), 'web_url' => web_url());
                $subject = "Welcome to " . ucwords(Config::get('app.website_title')) . ", " . ucwords($walker->first_name . " " . $walker->last_name) . "";
                email_notification($walker->id, 'walker', $pattern, $subject, 'walker_register', null);
                $txt_approve = "Decline";
                if ($walker->is_approved) {
                    $txt_approve = "Approved";
                }
                $response_array = array(
                    'success' => true,
                    'id' => $walker->id,
                    'first_name' => $walker->first_name,
                    'last_name' => $walker->last_name,
                    'phone' => $walker->phone,
                    'email' => $walker->email,
                    'picture' => $walker->picture,
                    'bio' => $walker->bio,
                    'address' => $walker->address,
                    'state' => $walker->state,
                    'country' => $walker->country,
                    'zipcode' => $walker->zipcode,
                    'login_by' => $walker->login_by,
                    'social_unique_id' => $walker->social_unique_id ? $walker->social_unique_id : "",
                    'device_token' => $walker->device_token,
                    'device_type' => $walker->device_type,
                    'token' => $walker->token,
                    'timezone' => $walker->timezone,
                    'car_model' => $walker->car_model,
                    'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                    /* 'type' => $myType, */
                    'type' => $walker->type,
                    'is_approved' => $walker->is_approved,
                    'is_approved_txt' => $txt_approve,
                    'is_available' => $walker->is_active,
                );
                $response_code = 200;
            }
        }

        response:
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function login()
    {
        $login_by = Input::get('login_by');
        $device_token = Input::get('device_token');
        $device_type = Input::get('device_type');
        if (Input::has('email') && Input::has('password')) {
            $email = Input::get('email');
            $password = Input::get('password');

            $validator = Validator::make(
                array(
                    'password' => $password,
                    'email' => $email,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'password' => 'required',
                    'email' => 'required|email',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google'
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages();
                Log::error('Validation error during manual login for walker = ' . print_r($error_messages, true));
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                if ($walker = Walker::where('email', '=', $email)->first()) {
                    if (Hash::check($password, $walker->password)) {
                        if ($login_by != "manual") {
                            $response_array = array('success' => false, 'error' => 'Login by mismatch', 'error_code' => 417);
                            $response_code = 200;
                        } else {
                            if ($walker->device_type != $device_type) {
                                $walker->device_type = $device_type;
                            }
                            if ($walker->device_token != $device_token) {
                                $walker->device_token = $device_token;
                            }
                            if( empty($walker->bb_token)){
                                // if bb token is empty add token
                                $walker->bb_token = generate_token();
                            }
                            
                            $walker->token_expiry = generate_expiry();
                            $walker->save();
                            $txt_approve = "Decline";
                            if ($walker->is_approved) {
                                $txt_approve = "Approved";
                            }
                            $response_array = array(
                                'success' => true,
                                'id' => $walker->id,
                                'first_name' => $walker->first_name,
                                'last_name' => $walker->last_name,
                                'phone' => $walker->phone,
                                'email' => $walker->email,
                                'city' => $walker->city,
                                'picture' => $walker->picture,
                                'bio' => $walker->bio,
                                'address' => $walker->address,
                                'address_latitude' => $walker->address_latitude,
                                'address_longitude' => $walker->address_longitude,
                                'state' => $walker->state,
                                'country' => $walker->country,
                                'zipcode' => $walker->zipcode,
                                'login_by' => $walker->login_by,
                                'social_unique_id' => $walker->social_unique_id,
                                'device_token' => $walker->device_token,
                                'device_type' => $walker->device_type,
                                'token' => $walker->token,
                                'type' => $walker->type,
                                'bb_token'=> $walker->bb_token,
                                'timezone' => $walker->timezone,
                                'is_approved' => $walker->is_approved,
                                'car_model' => $walker->car_model,
                                'is_jolly' => $walker->isJolly,
                                'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                                'is_approved_txt' => $txt_approve,
                                'is_available' => $walker->is_active,
                                'vehicle_registration_scan' =>isset($walker->vehicle_registration_scan) ? $walker->vehicle_registration_scan : '',
                                'vehicle_front_scan' => isset($walker->vehicle_front_scan) ? $walker->vehicle_front_scan : '',
                                'captain_license_scan' => isset($walker->captain_license_scan) ? $walker->captain_license_scan : '',
                                'captain_iqama_scan' => isset($walker->captain_iqama_scan) ? $walker->captain_iqama_scan : '',
                                'captain_confession' => isset($walker->captain_confession) ? $walker->captain_confession : '',
                                'created_at' => date('Y-m-d H:i:s',strtotime($walker->created_at)),
                            );
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid Username and Password', 'error_code' => 403);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a Registered User', 'error_code' => 404);
                    $response_code = 200;
                }
            }
        } elseif (Input::has('social_unique_id')) {
            $social_unique_id = Input::get('social_unique_id');
            $socialValidator = Validator::make(
                array(
                    'social_unique_id' => $social_unique_id,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'login_by' => $login_by
                ), array(
                    'social_unique_id' => 'required|exists:walker,social_unique_id',
                    'device_token' => 'required',
                    'device_type' => 'required|in:android,ios',
                    'login_by' => 'required|in:manual,facebook,google'
                )
            );
            if ($socialValidator->fails()) {
                $error_messages = $socialValidator->messages();
                Log::error('Validation error during social login for walker = ' . print_r($error_messages, true));
                $error_messages = $socialValidator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                if ($walker = Walker::where('social_unique_id', '=', $social_unique_id)->first()) {
                    if (!in_array($login_by, array('facebook', 'google'))) {
                        $response_array = array('success' => false, 'error' => 'Login by mismatch', 'error_code' => 417);
                        $response_code = 200;
                    } else {
                        if ($walker->device_type != $device_type) {
                            $walker->device_type = $device_type;
                        }
                        if ($walker->device_token != $device_token) {
                            $walker->device_token = $device_token;
                        }
                        $walker->token_expiry = generate_expiry();
                        $walker->save();
                        $txt_approve = "Decline";
                        if ($walker->is_approved) {
                            $txt_approve = "Approved";
                        }

                        $response_array = array(
                            'success' => true,
                            'id' => $walker->id,
                            'first_name' => $walker->first_name,
                            'last_name' => $walker->last_name,
                            'phone' => $walker->phone,
                            'email' => $walker->email,
                            'picture' => $walker->picture,
                            'bio' => $walker->bio,
                            'address' => $walker->address,
                            'state' => $walker->state,
                            'country' => $walker->country,
                            'zipcode' => $walker->zipcode,
                            'login_by' => $walker->login_by,
                            'social_unique_id' => $walker->social_unique_id,
                            'device_token' => $walker->device_token,
                            'device_type' => $walker->device_type,
                            'token' => $walker->token,
                            'timezone' => $walker->timezone,
                            'type' => $walker->type,
                            'is_approved' => $walker->is_approved,
                            'car_model' => $walker->car_model,
                            'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                            'is_approved_txt' => $txt_approve,
                            'is_available' => $walker->is_active,
                        );
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid social registration User', 'error_code' => 404);
                    $response_code = 200;
                }
            }
        } else {
            $response_array = array('success' => false, 'error' => 'Invalid Input');
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Rate Dog

    public function set_dog_rating()
    {
        if (Request::isMethod('post')) {
            $comment = "";
            if (Input::has('comment')) {
                $comment = Input::get('comment');
            }
            $request_id = Input::get('request_id');
            $rating = 0;
            if (Input::has('rating')) {
                $rating = Input::get('rating');
            }
            $token = Input::get('token');
            $walker_id = Input::get('id');

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    /* 'rating' => $rating, */
                    'token' => $token,
                    'walker_id' => $walker_id,
                ), array(
                    'request_id' => 'required|integer',
                    /* 'rating' => 'required|integer', */
                    'token' => 'required',
                    'walker_id' => 'required|integer'
                )
            );
            /* $var = Keywords::where('id', 1)->first(); */
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->is_dog_rated == 0) {

                                    $owner = Owner::find($request->owner_id);

                                    $dog_review = new DogReview;
                                    $dog_review->request_id = $request_id;
                                    $dog_review->walker_id = $walker_id;
                                    $dog_review->rating = $rating;
                                    $dog_review->owner_id = $owner->id;
                                    $dog_review->comment = $comment;
                                    $dog_review->save();

                                    $request->is_dog_rated = 1;
                                    $request->save();

                                    if ($rating) {
                                        if ($owner = Owner::find($request->owner_id)) {
                                            $old_rate = $owner->rate;
                                            $old_rate_count = $owner->rate_count;
                                            $new_rate_counter = ($owner->rate_count + 1);
                                            $new_rate = (($owner->rate * $owner->rate_count) + $rating) / $new_rate_counter;
                                            $owner->rate_count = $new_rate_counter;
                                            $owner->rate = $new_rate;
                                            $owner->save();
                                        }
                                    }
                                    $walker = Walker::find($walker_id);
                                    $walker->is_available = 1;
                                    $walker->save();

                                    $response_array = array('success' => true);
                                    $response_code = 200;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Already Rated', 'error_code' => 409);
                                    $response_code = 200;
                                }
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Cancel Walk

    public function cancel_walk()
    {
        if (Request::isMethod('post')) {
            $walk_id = Input::get('walk_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');

            $validator = Validator::make(
                array(
                    'walk_id' => $walk_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                ), array(
                    'walk_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer'
                )
            );

            /* $var = Keywords::where('id', 1)->first(); */

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($walk = Walk::find($walk_id)) {
                            if ($walk->walker_id == $walker_id) {

                                if ($walk->is_walk_started == 0) {
                                    $walk->walker_id = 0;
                                    $walk->is_confirmed = 0;
                                    $walk->save();

                                    $response_array = array('success' => true);
                                    $response_code = 200;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Service Already Started', 'error_code' => 416);
                                    $response_code = 200;
                                }
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Add walker Location Data
    public function walker_location()
    {
        if (Request::isMethod('post')) {
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }

            $validator = Validator::make(
                array(
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ), array(
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {

                //Log::info($walker_id.'  lat'.$latitude.'lng'.$longitude);
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    $status_txt = "not active";
                    if ($walker_data->is_active) {
                        $status_txt = "active";
                    }
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        $walker = Walker::find($walker_id);

                        //$location = get_location($latitude, $longitude);
                        //$latitude = $location['lat'];
                        //$longitude = $location['long'];

                        if (!isset($angle)) {
                            $angle = get_angle($walker->latitude, $walker->longitude, $latitude, $longitude);
                        }
                        $walker->old_latitude = $walker->latitude;
                        $walker->old_longitude = $walker->longitude;
                        $walker->latitude = $latitude;
                        $walker->longitude = $longitude;
                        $walker->heart_beat = date("Y-m-d H:i:s");
                        $walker->bearing = $angle;
                        if ($walker->flagisact_user == 0 && $walker->is_active == 0) {
                            $walker->is_active = 1;
                            $onlinehours = new OnlineHours();
                            $onlinehours->walker_id = $walker_id;
                            $onlinehours->save();
                        }
                        if ($walker->flagisactmonthly_user == 0)
                            $walker->is_active_monthly = 1;
                        $walker->save();
                        $response_array = array(
                            'success' => true,
                            'is_active' => $walker_data->is_active,
                            'is_approved' => $walker_data->is_approved,
                            'is_active_txt' => $status_txt,
                        );
                        //Log::info($walker_id.'  '.$walker->heart_beat.'lat'.$walker->latitude.'lng'.$walker->longitude);
                    } else {
                        $response_array = array(
                            'success' => false,
                            'error' => 'Token Expired',
                            'error_code' => 412,
                            'is_active' => $walker_data->is_active,
                            'is_approved' => $walker_data->is_approved,
                            'is_active_txt' => $status_txt,
                        );
                    }
                } else {
                    if ($is_admin) {
                        /* $driver = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Profile
    public function get_requests()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
            'token' => 'required',
            'walker_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {
                    $txt_approve = "Decline";
                    if ($walker_data->is_approved) {
                        $txt_approve = "Approved";
                    }
                    $time = date("Y-m-d H:i:s");
                    $provider_timeout = Settings::where('key', 'provider_timeout')->first();
                    $timeout = $provider_timeout->value;

                    $query = "SELECT id, later, D_latitude, D_longitude, payment_mode, service_type, request_start_time , owner_id, TIMESTAMPDIFF(SECOND,updated_at, '$time') as diff from request where monthly_id = 0 and service_type!= 2 and is_cancelled = 0 and status = 0 and id in (select request_id from request_meta where walker_id = '$walker_id' and status = 0 and is_cancelled = 0) and TIMESTAMPDIFF(SECOND,updated_at, '$time') <= $timeout";

                    $requests = DB::select(DB::raw($query));
                    $all_requests = array();
                    $counter = 0;
                    foreach ($requests as $request) {
                        $counter++;
                        $data['request_id'] = $request->id;
                        $data['service_type'] = (string)$request->service_type;
                        $requestData = RequestServices::where('request_id', $request->id)->first();
                        $data['request_services'] = $requestData->type;

                        $rservc = RequestServices::where('request_id', $request->id)->get();
                        $typs = array();
                        $typi = array();
                        $typp = array();
                        $totalPrice = 0;

                        foreach ($rservc as $typ) {
                            $typ1 = ProviderType::where('id', $typ->type)->first();
                            $typ_price = ProviderServices::where('provider_id', $walker_id)->where('type', $typ->type)->first();

                            if ($typ_price->base_price > 0) {
                                $typp1 = 0.00;
                                $typp1 = $typ_price->base_price;
                            } else {
                                $typp1 = 0.00;
                            }

                            $typs['name'] = $typ1->name;
                            $typs['price'] = $typp1;
                            $totalPrice = $totalPrice + $typp1;

                            array_push($typi, $typs);
                        }
                        $data['type'] = $typi;

                        if ($request->later == 0)
                            $data['time_left_to_respond'] = $timeout - $request->diff;
                        else
                            $data['time_left_to_respond'] = $timeout - $request->diff;
                        if ($request->diff > 120) {
                            Log::info("");
                            Monthly::where('request_id', '=', $request->id)->update(array('status' => 4));
                            Requests::where('id', '=', $request->id)->where('service_type', '=', 2)->update(array('current_walker' => 0, 'status' => 0));
                            // Archiving Old Walker
                            RequestMeta::where('request_id', '=', $request->id)->where('walker_id', '=', $walker_id)->update(array('status' => 3));
                            Log::info("Time out for current captain to respond");
                            continue;
                        }

                        $owner = Owner::find($request->owner_id);
                        $user_timezone = $owner->timezone;
                        $default_timezone = Config::get('app.timezone');

                        $date_time = get_user_time($default_timezone, $user_timezone, $request->request_start_time);


                        $data['later'] = $request->later;
                        $data['datetime'] = $date_time;

                        $request_data = array();
                        $request_data['owner'] = array();
                        $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                        $request_data['owner']['picture'] = $owner->picture;
                        $request_data['owner']['phone'] = $owner->phone;
                        $request_data['owner']['address'] = $owner->address;
                        $request_data['owner']['latitude'] = $owner->latitude;
                        $request_data['owner']['longitude'] = $owner->longitude;
                        $request_data['owner']['dest_latitude'] = $request->D_latitude;
                        $request_data['owner']['dest_longitude'] = $request->D_longitude;
                        if ($request->D_latitude != NULL) {
                            /* Log::info('D_latitude = ' . print_r($request->D_latitude, true)); */
                            $request_data['owner']['d_latitude'] = $request->D_latitude;
                            $request_data['owner']['d_longitude'] = $request->D_longitude;
                        }
                        $request_data['owner']['rating'] = $owner->rate;
                        $request_data['owner']['num_rating'] = $owner->rate_count;
                        /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                          $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                        $request_data['owner']['payment_type'] = $request->payment_mode;
                        $request_data['payment_mode'] = $request->payment_mode;
                        $request_data['dog'] = array();
                        if ($dog = Dog::find($owner->dog_id)) {

                            $request_data['dog']['name'] = $dog->name;
                            $request_data['dog']['age'] = $dog->age;
                            $request_data['dog']['breed'] = $dog->breed;
                            $request_data['dog']['likes'] = $dog->likes;
                            $request_data['dog']['picture'] = $dog->image_url;
                        }
                        $data['request_data'] = $request_data;
                        array_push($all_requests, $data);
                    }

                    $query = "SELECT *, TIMESTAMPDIFF"
                        ."(SECOND,updated_at, '$time') as diff from delivery_request where is_cancelled = 0 and status = 0 and id in (select request_id from delivery_request_meta where walker_id = '$walker_id' and status = 0 and is_cancelled = 0) and"
                        ." TIMESTAMPDIFF(SECOND,updated_at, '$time') <= $timeout";

                    $requests = DB::select(DB::raw($query));
                    $all_delivery_requests = array();
                    $counter = 0;
                    foreach ($requests as $request) {
                        $counter++;
                        $data['request_id'] = $request->id;
                        $data['service_type'] = (string)$request->service_type;
                        $data['time_left_to_respond'] = $timeout - $request->diff;
                        $company = Company::find($request->company_id);

                        $msg_array = array();
                        $msg_array['unique_id'] = 51;
                        $delivery_request=$request;
                        $msg_array['delivery_request_id'] = $delivery_request->id;
                        $msg_array['request_start_time'] = $delivery_request->request_start_time;
                        $msg_array['receiver_name'] = $delivery_request->receiver_name;
                        $msg_array['receiver_phone'] = $delivery_request->receiver_phone;
                        $msg_array['small_items'] = $delivery_request->small_items;
                        $msg_array['medium_items'] = $delivery_request->medium_items;
                        $msg_array['large_items'] = $delivery_request->large_items;
                        $msg_array['cash_on_delivery'] = $delivery_request->cash_on_delivery;
                        $msg_array['transportation_fees'] = $delivery_request->transportation_fees;
                        $msg_array['transportation_fees_payed_by'] = $delivery_request->transportation_fees_payed_by;
                        $msg_array['origin_latitude'] = $delivery_request->origin_latitude;
                        $msg_array['origin_longitude'] = $delivery_request->origin_longitude;
                        $msg_array['D_latitude'] = $delivery_request->D_latitude;
                        $msg_array['D_longitude'] = $delivery_request->D_longitude;
                        $msg_array['status'] = $delivery_request->status;
                        $msg_array['is_walker_started'] = $delivery_request->status;
                        $msg_array['is_walker_arrived'] = $delivery_request->status;
                        $msg_array['is_started'] = $delivery_request->status;
                        $msg_array['is_completed'] = $delivery_request->status;
                        // add company details here
                        $msg_array['company'] = array();
                        $msg_array['company'] ['name'] = $company->name;
                        $msg_array['company'] ['phone'] = $company->phone;
                        $msg_array['company'] ['email'] = $company->email;
                        $msg_array['company'] ['products_type'] = $company->products_type;
                        $msg_array['company'] ['city'] = $company->city;
                        $msg_array['company'] ['latitude'] = $company->latitude;
                        $msg_array['company'] ['longitude'] = $company->longitude;
                        $msg_array['company'] ['address'] = $company->address;
                        $msg_array['company'] ['rating'] = $company->rate;
                        $msg_array['company'] ['num_rating'] = $company->rate_count;
                        $msg_array['company'] ['picture'] = $company->picture;

                        $data['request_data'] = $msg_array;
                        array_push($all_delivery_requests, $data);
                    }

                    //$query = "SELECT id, service_type, later, D_latitude, D_longitude, payment_mode, request_start_time , owner_id,TIMESTAMPDIFF(SECOND,updated_at, '$time') as diff from request where service_type = 2 and is_cancelled = 0 and status = 0 and current_walker=$walker_id";
                    $query = "SELECT id, service_type, later, D_latitude, D_longitude, payment_mode, total, request_start_time, request_timestamp, owner_id,TIMESTAMPDIFF(SECOND,updated_at, '$time') as diff from request where service_type = 2 and is_cancelled = 0 and status = 1 and id in
                      (select request_id from request_meta where walker_id = " . $walker_id . " and status = 0 and is_cancelled = 0 ) and monthly_id=0 order by id desc limit 1";
                    $monthly_requests = DB::select(DB::raw($query));
                    $all_monthly_requests = array();
                    $counter = 0;

                    foreach ($monthly_requests as $request) {
                        $counter++;
                        $data['request_id'] = $request->id;
                        $data['service_type'] = (string)$request->service_type;

                        $requestData = RequestServices::where('request_id', $request->id)->first();
                        $data['request_services'] = $requestData->type;

                        $rservc = RequestServices::where('request_id', $request->id)->get();
                        $typs = array();
                        $typi = array();
                        $typp = array();
                        $totalPrice = 0;

                        foreach ($rservc as $typ) {
                            $typ1 = ProviderType::where('id', $typ->type)->first();
                            $typ_price = ProviderServices::where('provider_id', $walker_id)->where('type', $typ->type)->first();

                            if ($typ_price->base_price > 0) {
                                $typp1 = 0.00;
                                $typp1 = $typ_price->base_price;
                            } else {
                                $typp1 = 0.00;
                            }

                            $typs['name'] = $typ1->name;
                            $typs['price'] = $typp1;
                            $totalPrice = $totalPrice + $typp1;

                            array_push($typi, $typs);
                        }
                        $data['type'] = $typi;

                        if ($request->later == 0)
                            //$data['time_left_to_respond'] = $timeout - $request->diff;
                            if ($request->service_type != "2") {
                                $data['time_left_to_respond'] = "60";
                            } else {

                                $provider_timeout_monthly = Settings::where('key', 'captain_monthly_timeout_seconds')->first();
                                $provider_timeout_monthly = $provider_timeout_monthly->value;
                                $data['time_left_to_respond'] = $provider_timeout_monthly - $request->diff;
                            }
                        else
                            $data['time_left_to_respond'] = $timeout - $request->diff;

                        $owner = Owner::find($request->owner_id);

                        $user_timezone = $owner->timezone;
                        $default_timezone = Config::get('app.timezone');

                        $date_time = get_user_time($default_timezone, $user_timezone, $request->request_start_time);

                        $subscription = Monthly::where('request_id', $request->id)->first();

                        $data['later'] = $request->later;
                        $data['datetime'] = $date_time;
                        $data['datetime_timestamp'] = $request->request_timestamp;
                        //$data['datetime'] = date('Y-m-d h:i:s', strtotime($date_time. ' - 1.5 seconds'));

                        $request_data = array();
                        $request_data['owner'] = array();
                        $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                        $request_data['owner']['picture'] = $owner->picture;
                        $request_data['owner']['phone'] = $owner->phone;
                        $request_data['owner']['address'] = $owner->address;
                        if (isset($subscription)) {
                            $request_data['owner']['gender'] = $subscription->gender;
                            $request_data['owner']['new_request_status'] = (string)$subscription->status;
                            $request_data['owner']['subscription_type'] = $subscription->subscription_type;
                            $request_data['owner']['trip_type'] = $subscription->trip_type;
                            $request_data['owner']['is_full_month'] = $subscription->is_full_month;

                            $request_data['owner']['trip_one_from_address'] = $subscription->trip_one_from_address;
                            $request_data['owner']['trip_one_to_address'] = $subscription->trip_one_to_address;
                            $request_data['owner']['trip_two_from_address'] = $subscription->trip_two_from_address;
                            $request_data['owner']['trip_two_to_address'] = $subscription->trip_two_to_address;

                            $request_data['owner']['trip_one_from_latitude'] = $subscription->trip_one_from_lat;
                            $request_data['owner']['trip_one_from_longitude'] = $subscription->trip_one_from_long;
                            $request_data['owner']['trip_one_to_latitude'] = $subscription->trip_one_to_lat;
                            $request_data['owner']['trip_one_to_longitude'] = $subscription->trip_one_to_long;

                            $request_data['owner']['trip_two_from_latitude'] = $subscription->trip_two_from_lat;
                            $request_data['owner']['trip_two_from_longitude'] = $subscription->trip_two_from_long;
                            $request_data['owner']['trip_two_to_latitude'] = $subscription->trip_two_to_lat;
                            $request_data['owner']['trip_two_to_longitude'] = $subscription->trip_two_to_long;

                            $request_data['owner']['trip_one_pickup_time'] = $subscription->trip_one_pickup_time;
                            $request_data['owner']['trip_two_pickup_time'] = $subscription->trip_two_pickup_time;

                            $request_data['owner']['starting_date'] = $subscription->starting_date;
                            $request_data['owner']['end_date'] = date('Y-m-d', strtotime($subscription->starting_date . ' + 29 days'));
                        } else {
                            Log::info("monthly nt found ");
                            continue;
                        }
                        $request_data['owner']['total_cost'] = $request->total . " SAR";
                        $request_data['owner']['rating'] = $owner->rate;
                        $request_data['owner']['num_rating'] = $owner->rate_count;
                        /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                          $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                        $request_data['owner']['payment_type'] = $request->payment_mode;
                        $request_data['payment_mode'] = $request->payment_mode;
                        $request_data['dog'] = array();

                        if ($dog = Dog::find($owner->dog_id)) {

                            $request_data['dog']['name'] = $dog->name;
                            $request_data['dog']['age'] = $dog->age;
                            $request_data['dog']['breed'] = $dog->breed;
                            $request_data['dog']['likes'] = $dog->likes;
                            $request_data['dog']['picture'] = $dog->image_url;
                        }
                        $data['request_data'] = $request_data;
                        array_push($all_monthly_requests, $data);
                    }
                    /* if ($counter) { */
                    $response_array = array('success' => true, 'is_approved' => $walker_data->is_approved, 'is_approved_txt' => $txt_approve, 'is_available' => $walker_data->is_active, 'incoming_requests' => $all_requests, 'monthly_requests' => $all_monthly_requests,'all_delivery_requests'=>$all_delivery_requests);
                    $response_code = 200;
                    /* } else {
                      $response_array = array('success' => false, 'error' => 'no request found', 'error_code' => 505);
                      $response_code = 200;
                      } */
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Respond To Request

    public function respond_request()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $request_id = Input::get('request_id');
        $accepted = Input::get('accepted');

        $date_time = Input::get('datetime');


        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $request_id,
                'accepted' => $accepted,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'accepted' => 'required|integer',
                'request_id' => 'required|integer'
            )
        );

        /* $driver = Keywords::where('id', 1)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrive and validate the Request
                    if ($request = Requests::find($request_id)) {
                        if ($request->confirmed_walker == 0 && $request->is_cancelled == 0) {
                            if ($accepted == 1) {
                                if ($request->later == 1) {
                                    // request ended
                                    //Requests::where('id', '=', $request_id)->where('service_type', '!=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1));
                                    Requests::where('id', '=', $request_id)->update(array('confirmed_walker' => $walker_id, 'status' => 1));
                                } else {
                                    //Requests::where('id', '=', $request_id)->where('service_type', '!=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1, 'request_start_time' => date('Y-m-d H:i:s')));
                                    Requests::where('id', '=', $request_id)->update(array('confirmed_walker' => $walker_id, 'current_walker' => $walker_id, 'status' => 1, 'request_start_time' => date('Y-m-d H:i:s')));
                                }
                                // confirm walker
                                RequestMeta::where('request_id', '=', $request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 1));

                                // Update Walker availability
                                Walker::where('id', '=', $walker_id)->update(array('is_available' => 0));
                                $exmetas = RequestMeta::where('request_id', '=', $request_id)->where('status', '=', 0)->get();
                                foreach ($exmetas as $metaa) {
                                    Walker::where('id', '=', $metaa->walker_id)->update(array('is_available' => 1));
                                    RequestMeta::where('id', '=', $metaa->id)->update(array('status' => 3));
                                    //send_walker_request_already_accepted($request_id,$metaa->walker_id);
                                }


                                // Send Notification
                                $walker = Walker::find($walker_id);
                                $walker_data = array();
                                $walker_data['first_name'] = $walker->first_name;
                                $walker_data['last_name'] = $walker->last_name;
                                $walker_data['phone'] = $walker->phone;
                                $walker_data['bio'] = $walker->bio;
                                $walker_data['picture'] = $walker->picture;
                                $walker_data['latitude'] = $walker->latitude;
                                $walker_data['longitude'] = $walker->longitude;
                                $walker_data['type'] = $walker->type;
                                $walker_data['rating'] = $walker->rate;
                                $walker_data['num_rating'] = $walker->rate_count;
                                $walker_data['car_model'] = $walker->car_model;
                                $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                  $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                $settings = Settings::where('key', 'default_distance_unit')->first();
                                $unit = $settings->value;
                                if ($unit == 0) {
                                    $unit_set = 'kms';
                                } elseif ($unit == 1) {
                                    $unit_set = 'miles';
                                }
                                $bill = array();
                                if ($request->is_completed == 1) {
                                    $bill['distance'] = (string)convert($request->distance, $unit);
                                    $bill['unit'] = $unit_set;
                                    $bill['time'] = $request->time;
                                    $bill['base_price'] = $request->base_price;
                                    $bill['distance_cost'] = $request->distance_cost;
                                    $bill['time_cost'] = $request->time_cost;
                                    $bill['total'] = $request->total;
                                    $bill['is_paid'] = $request->is_paid;
                                }


                                /* $setting = Settings::where('key', 'allow_calendar')->first();

                                  if ($request->later == 1 && $setting->value == 1) { */
                                if ($request->later == 1) {

                                    $date_time = $request->request_start_time;

                                    $datewant = new DateTime($date_time);
                                    $datetime = $datewant->format('Y-m-d H:i:s');

                                    $end_time = $datewant->add(new DateInterval('P0Y0M0DT2H0M0S'))->format('Y-m-d H:i:s');

                                    $provavail = ProviderAvail::where('provider_id', $walker_id)->where('start', '<=', $datetime)->where('end', '>=', $end_time)->first();
                                    $starttime = $provavail->start;
                                    $endtime = $provavail->end;
                                    $provavail->delete();

                                    if ($starttime == $datetime) {
                                        $provavail1 = new ProviderAvail;
                                        $provavail1->provider_id = $walker_id;
                                        $provavail1->start = $end_time;
                                        $provavail1->end = $endtime;
                                        $provavail1->save();
                                    } elseif ($endtime == $end_time) {
                                        $provavail1 = new ProviderAvail;
                                        $provavail1->provider_id = $walker_id;
                                        $provavail1->start = $starttime;
                                        $provavail1->end = $datetime;
                                        $provavail1->save();
                                    } else {
                                        $provavail1 = new ProviderAvail;
                                        $provavail1->provider_id = $walker_id;
                                        $provavail1->start = $starttime;
                                        $provavail1->end = $datetime;
                                        $provavail1->save();

                                        $provavail2 = new ProviderAvail;
                                        $provavail2->provider_id = $walker_id;
                                        $provavail2->start = $end_time;
                                        $provavail2->end = $endtime;
                                        $provavail2->save();
                                    }
                                }

                                $response_array = array(
                                    'success' => true,
                                    'request_id' => $request_id,
                                    'status' => $request->status,
                                    'confirmed_walker' => $request->confirmed_walker,
                                    'is_walker_started' => $request->is_walker_started,
                                    'is_walker_arrived' => $request->is_walker_arrived,
                                    'is_walk_started' => $request->is_started,
                                    'is_completed' => $request->is_completed,
                                    'is_walker_rated' => $request->is_walker_rated,
                                    'walker' => $walker_data,
                                    'bill' => $bill,
                                );
                                /* $driver = Keywords::where('id', 1)->first(); */
                                /* $trip = Keywords::where('id', 4)->first(); */

                                /* $title = '' . $driver->keyword . ' has accepted the ' . $trip->keyword; */
                                $title = '' . Config::get('app.generic_keywords.Provider') . ' has accepted the ' . Config::get('app.generic_keywords.Trip');

                                $message = $response_array;

                                send_notifications($request->owner_id, "owner", $title, $message);

                                // Send SMS
                                /* for speed test
                                $owner = Owner::find($request->owner_id);
                                $settings = Settings::where('key', 'sms_when_provider_accepts')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);

                                $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                sms_notification($request->owner_id, 'owner', $pattern);

                                // Send SMS
                                $owner = Owner::find($request->owner_id);
                                $src_address = get_address($owner->latitude, $owner->longitude);
                                $pattern = Config::get('app.generic_keywords.User') . " Pickup Address : " . $src_address;
                                sms_notification($walker_id, 'walker', $pattern);

                                // Send SMS
                                $settings = Settings::where('key', 'sms_request_completed')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%id%', $request->id, $pattern);
                                $pattern = str_replace('%user_mobile%', $owner->phone, $pattern);
                                sms_notification(1, 'admin', $pattern);

                                //email to client for accept the request
                                $settings = Settings::where('key', 'admin_email_address')->first();
                                $admin_email = $settings->value;
                                $pattern = array(
                                    'admin_eamil' => $admin_email,
                                    'client_name' => ucwords($owner->first_name . " " . $owner->last_name),
                                    'web_url' => web_url(),
                                    'driver_name' => ucwords($walker->first_name . " " . $walker->last_name),
                                    'driver_contact' => $walker->phone,
                                    'driver_car_model' => $walker->car_model,
                                    'driver_licence' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                                );
                                $subject = "Get Ready For Ride";
                                email_notification($owner->id, 'owner', $pattern, $subject, 'accept_request', null);
                                */
                            } else {
                                Walker::where('id', '=', $walker_id)->update(array('is_available' => 1));
                                // Archiving Old Walker
                                RequestMeta::where('request_id', '=', $request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 3));
                            }
                            $response_array = array('success' => true);
                            $response_code = 200;
                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Request ID does not matches' . $driver->keyword . ' ID', 'error_code' => 472); */
                            $response_array = array('success' => false, 'error' => 'Request already accepted by other ' . Config::get('app.generic_keywords.Provider') . ' ', 'error_code' => 472);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Request Status
    public function request_in_progress()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    $request = Requests::where('status', '=', 1)->where('is_cancelled', '=', 0)->where('service_type', '!=', 2)->where('is_dog_rated', '=', 0)->where('confirmed_walker', '=', $walker_id)->first();
                    if ($request) {
                        $request_id = $request->id;
                    } else {
                        $request_id = -1;
                    }

                    $txt_approve = "Decline";
                    if ($walker_data->is_approved) {
                        $txt_approve = "Approved";
                    }

                    $response_array = array(
                        'request_id' => $request_id,
                        'is_approved' => $walker_data->is_approved,
                        'is_available' => $walker_data->is_active,
                        'is_approved_txt' => $txt_approve,
                        'success' => true,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $driver = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Request Status
    public function get_request()
    {

        $request_id = Input::get('request_id');
        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'request_id' => $request_id,
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'request_id' => 'required|integer',
                'token' => 'required',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {
                    $txt_approve = "Decline";
                    if ($walker_data->is_approved) {
                        $txt_approve = "Approved";
                    }
                    /***
                     * For Testing I Umar Usman commented this line
                     */
                    $request = Requests::where('id', '=', $request_id)->where('service_type', '!=', 2)->first();

                    // Do necessary operations
                    if ($request) {
                        if ($request->confirmed_walker == $walker_id) {

                            $owner = Owner::find($request->owner_id);
                            $request_data = array();
                            $request_data['is_walker_started'] = $request->is_walker_started;
                            $request_data['is_walker_arrived'] = $request->is_walker_arrived;
                            $request_data['is_started'] = $request->is_started;
                            $request_data['is_completed'] = $request->is_completed;
                            $request_data['is_cash_posted_cap'] = $request->is_cash_posted_cap;
                            $request_data['is_invoice_verified_user'] = $request->is_invoice_verified_user;
                            $request_data['is_invoice_verified_cap'] = $request->is_invoice_verified_cap;
                            $request_data['is_dog_rated'] = $request->is_dog_rated;
                            $request_data['is_cancelled'] = $request->is_cancelled;
                            $request_data['dest_latitude'] = $request->D_latitude;
                            $request_data['dest_longitude'] = $request->D_longitude;

                            $user_timezone = $owner->timezone;
                            $default_timezone = Config::get('app.timezone');

                            $date_time = get_user_time($default_timezone, $user_timezone, $request->request_start_time);

                            $request_data['accepted_time'] = $date_time;
                            $request_data['payment_mode'] = $request->payment_mode;
                            $request_data['payment_type'] = $request->payment_mode;
                            if ($request->promo_code != "") {
                                if ($request->promo_code != "") {
                                    $promo_code = PromoCodes::where('id', $request->promo_id)->first();
                                    $promo_value = $promo_code->value;
                                    $promo_type = $promo_code->type;
                                    if ($promo_type == 1) {
                                        $discount = $request->total * $promo_value / 100;
                                    } elseif ($promo_type == 2) {
                                        $discount = $promo_value;
                                    }
                                    $request_data['promo_discount'] = $discount;
                                }
                            }
                            if ($request->is_started == 1) {

                                $time = DB::table('walk_location')
                                    ->where('request_id', $request_id)
                                    ->min('created_at');

                                $date_time = get_user_time($default_timezone, $user_timezone, $time);

                                $request_data['start_time'] = $date_time;

                                $settings = Settings::where('key', 'default_distance_unit')->first();
                                $unit = $settings->value;

                                $distance = DB::table('walk_location')->where('request_id', $request_id)->max('distance');
                                $request_data['distance'] = (string)convert($distance, $unit);
                                if ($unit == 0) {
                                    $unit_set = 'kms';
                                } elseif ($unit == 1) {
                                    $unit_set = 'miles';
                                }
                                $request_data['unit'] = $unit_set;

                                $loc1 = WalkLocation::where('request_id', $request->id)->first();
                                $loc2 = WalkLocation::where('request_id', $request->id)->orderBy('id', 'desc')->first();
                                if ($loc1) {
                                    $time1 = strtotime($loc2->created_at);
                                    $time2 = strtotime($loc1->created_at);
                                    $difference = intval(($time1 - $time2) / 60);
                                } else {
                                    $difference = 0;
                                }
                                $request_data['time'] = $difference;
                                $request_data['time'] = $request->time;
                            }

                            if ($request->is_completed == 1) {
                                $request_data['distance'] = (string)convert($distance, $unit);
                                if ($unit == 0) {
                                    $unit_set = 'kms';
                                } elseif ($unit == 1) {
                                    $unit_set = 'miles';
                                }
                                $request_data['unit'] = $unit_set;

                                $time = DB::table('walk_location')
                                    ->where('request_id', $request_id)
                                    ->max('created_at');

                                $end_time = get_user_time($default_timezone, $user_timezone, $time);

                                $request_data['end_time'] = $end_time;
                            }

                            $request_data['owner'] = array();
                            $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                            $request_data['owner']['picture'] = $owner->picture;
                            $request_data['owner']['phone'] = $owner->phone;
                            $request_data['owner']['address'] = $owner->address;
                            $request_data['owner']['latitude'] = $owner->latitude;
                            $request_data['owner']['longitude'] = $owner->longitude;
                            if ($request->D_latitude != NULL) {
                                $request_data['owner']['d_latitude'] = $request->D_latitude;
                                $request_data['owner']['d_longitude'] = $request->D_longitude;
                            }
                            $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                            $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                            $request_data['owner']['dest_latitude'] = $request->D_latitude;
                            $request_data['owner']['dest_longitude'] = $request->D_longitude;
                            $request_data['owner']['pickup_address'] = $request->pickup_address;
                            $request_data['owner']['drop_address'] = $request->drop_address;

                            $request_data['owner']['rating'] = $owner->rate;
                            $request_data['owner']['num_rating'] = $owner->rate_count;
                            $request_data['monthly_id'] = $request->monthly_id;
                            /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                              $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                            $request_data['dog'] = array();
                            if ($dog = Dog::find($owner->dog_id)) {

                                $request_data['dog']['name'] = $dog->name;
                                $request_data['dog']['age'] = $dog->age;
                                $request_data['dog']['breed'] = $dog->breed;
                                $request_data['dog']['likes'] = $dog->likes;
                                $request_data['dog']['picture'] = $dog->image_url;
                            }
                            $request_data['bill'] = array();
                            $bill = array();
                            $settings = Settings::where('key', 'default_distance_unit')->first();
                            $unit = $settings->value;
                            if ($unit == 0) {
                                $unit_set = 'kms';
                            } elseif ($unit == 1) {
                                $unit_set = 'miles';
                            }
                            $requestserv = RequestServices::where('request_id', $request->id)->first();

                            $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();
                            $setbase_distance = $request_typ->base_distance;
                            $base_price = $request_typ->base_price;
                            $base_time = $request_typ->base_time;
                            $price_per_unit_distance = $request_typ->price_per_unit_distance;
                            $price_per_unit_time = $request_typ->price_per_unit_time;

                            /* $currency_selected = Keywords::find(5); */
                            if ($request->is_completed == 1) {
                                if ($request->is_cash_posted_cap == 1 || $request->is_paid == 1) {
                                    $bill['current_credit'] = ($request->current_credit_cap);
                                    $bill['used_credit'] = ($request->used_credit_cap);
                                    $bill['added_to_credit'] = ($request->added_to_credit_cap);
                                    $bill['cash_paid'] = ($request->cash_paid);
                                }

                                $bill['distance'] = (string)$request->distance;
                                $bill['unit'] = $unit_set;
                                $bill['time'] = $request->time;
                                $bill['base_distance'] = $setbase_distance;
                                $bill['base_price'] = $base_price;
                                $bill['price_per_unit_distance'] = $price_per_unit_distance;
                                $bill['price_per_unit_time'] = $price_per_unit_time;
                                $bill['bill'] = $request->bill;
                                if ($requestserv->base_price != 0) {
                                    $bill['distance_cost'] = ($requestserv->distance_cost);
                                    $bill['time_cost'] = ($requestserv->time_cost);
                                } else {
                                    // $setbase_price = Settings::where('key', 'base_price')->first();
                                    // $bill['base_price'] = ($setbase_price->value);
                                    //$setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                    // $bill['distance_cost'] = ($setdistance_price->value);
                                    // $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                    // $bill['time_cost'] = ($settime_price->value);
                                    $bill['distance_cost'] = ($price_per_unit_distance);
                                    $bill['time_cost'] = ($price_per_unit_time);
                                }

                                $admins = Admin::first();
                                $walker = Walker::where('id', $walker_id)->first();
                                $bill['walker']['email'] = $walker->email;
                                $bill['admin']['email'] = $admins->username;
                                if ($request->transfer_amount != 0) {
                                    $bill['walker']['amount'] = ($request->total - $request->transfer_amount);
                                    $bill['admin']['amount'] = ($request->transfer_amount);
                                } else {
                                    $bill['walker']['amount'] = ($request->transfer_amount);
                                    $bill['admin']['amount'] = ($request->total - $request->transfer_amount);
                                }
                                $discount = 0;
                                if ($request->promo_code != "") {
                                    if ($request->promo_code != "") {
                                        $promo_code = PromoCodes::where('id', $request->promo_code)->first();
                                        if ($promo_code) {
                                            $promo_value = $promo_code->value;
                                            $promo_type = $promo_code->type;
                                            if ($promo_type == 1) {
                                                // Percent Discount
                                                $discount = $request->total * $promo_value / 100;
                                            } elseif ($promo_type == 2) {
                                                // Absolute Discount
                                                $discount = $promo_value;
                                            }
                                        }
                                    }
                                }
                                /* $bill['currency'] = $currency_selected->keyword; */
                                $bill['currency'] = Config::get('app.generic_keywords.Currency');
                                $total_amount = $request->total - $request->ledger_payment;// - $request->promo_payment;
                                /* if($total_amount < 0)
                                     $total_amount = 0;*/
                                $bill['total'] = ($total_amount);

//                                $bill['total'] = ($request->total);
                                $bill['main_total'] = ($request->total);
                                $bill['actual_total'] = ($request->total + $request->ledger_payment + $discount);
//                                $bill['total'] = ($request->total + $request->ledger_payment + $request->promo_payment);
                                $bill['referral_bonus'] = ($request->ledger_payment);
                                $bill['promo_bonus'] = ($request->promo_payment);
                                $bill['payment_type'] = $request->payment_mode;
                                $bill['is_paid'] = $request->is_paid;
                            }
                            $request_data['bill'] = $bill;

                            $cards = "";
                            $cardlist = Payment::where('owner_id', $owner->id)->where('is_default', 1)->first();
                            if (count($cardlist) >= 1) {
                                $cards = array();
                                $default = $cardlist->is_default;
                                if ($default == 1) {
                                    $cards['is_default_text'] = "default";
                                } else {
                                    $cards['is_default_text'] = "not_default";
                                }
                                $cards['card_id'] = $cardlist->id;
                                $cards['owner_id'] = $cardlist->owner_id;
                                $cards['customer_id'] = $cardlist->customer_id;
                                $cards['last_four'] = $cardlist->last_four;
                                $cards['card_token'] = $cardlist->card_token;
                                $cards['card_type'] = $cardlist->card_type;
                                $cards['is_default'] = $default;
                            }
                            $request_data['card_details'] = $cards;

                            $chagre = array();

                            /* $settings = Settings::where('key', 'default_distance_unit')->first();
                              $unit = $settings->value;
                              if ($unit == 0) {
                              $unit_set = 'kms';
                              } elseif ($unit == 1) {
                              $unit_set = 'miles';
                              } */
                            $chagre['unit'] = $unit_set;

                            $requestserv = RequestServices::where('request_id', $request->id)->first();
                            if ($requestserv->base_price != 0) {
                                $chagre['base_price'] = $requestserv->base_price;
                                $chagre['distance_price'] = $requestserv->distance_cost;
                                $chagre['price_per_unit_time'] = $requestserv->time_cost;
                            } else {
                                /* $setbase_price = Settings::where('key', 'base_price')->first();
                                  $chagre['base_price'] = $setbase_price->value;
                                  $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                  $chagre['distance_price'] = $setdistance_price->value;
                                  $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                  $chagre['price_per_unit_time'] = $settime_price->value; */
                                $chagre['base_distance'] = $setbase_distance;
                                $chagre['base_price'] = ($base_price);
                                $chagre['distance_price'] = ($price_per_unit_distance);
                                $chagre['price_per_unit_time'] = ($price_per_unit_time);
                            }
                            $chagre['total'] = $request->total;
                            $chagre['is_paid'] = $request->is_paid;


                            $request_data['charge_details'] = $chagre;

                            $response_array = array('success' => true, 'is_available' => $walker_data->is_active, 'is_approved' => $walker_data->is_approved, 'is_approved_txt' => $txt_approve, 'request' => $request_data, 'bill' => $bill);
                            $response_code = 200;
                        } else {


                            /* $driver = Keywords::where('id', 1)->first();
                              $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $driver->keyword . ' ID', 'error_code' => 407); */
                            $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'is_available' => $walker_data->is_active, 'is_approved' => $walker_data->is_approved, 'is_approved_txt' => $txt_approve, 'error_code' => 407);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'is_available' => $walker_data->is_active, 'is_approved' => $walker_data->is_approved, 'is_approved_txt' => $txt_approve, 'error_code' => 408);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $driver = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Request Status
    public function get_walk_location()
    {

        $request_id = Input::get('request_id');
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $timestamp = Input::get('ts');

        $validator = Validator::make(
            array(
                'request_id' => $request_id,
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'request_id' => 'required|integer',
                'token' => 'required',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $status_txt = "not active";
                    if ($walker_data->is_active) {
                        $status_txt = "active";
                    }
                    // Do necessary operations
                    if ($request = Requests::find($request_id)) {
                        if ($request->confirmed_walker == $walker_id) {
                            $cwalker = Walker::find($walker_id);
                            $cwalker->heart_beat = date("Y-m-d H:i:s");
                            $cwalker->save();

                            if (isset($timestamp)) {
                                $walk_locations = WalkLocation::where('request_id', '=', $request_id)->where('created_at', '>', $timestamp)->orderBy('created_at')->get();
                            } else {
                                $walk_locations = WalkLocation::where('request_id', '=', $request_id)->orderBy('created_at')->get();
                            }
                            $locations = array();
                            $settings = Settings::where('key', 'default_distance_unit')->first();
                            $unit = $settings->value;
                            foreach ($walk_locations as $walk_location) {
                                $location = array();
                                $location['latitude'] = $walk_location->latitude;
                                $location['longitude'] = $walk_location->longitude;
                                $location['distance'] = convert($walk_location->distance, $unit);
                                $location['bearing'] = $walk_location->bearing;
                                $location['timestamp'] = $walk_location->created_at;
                                array_push($locations, $location);
                            }

                            $response_array = array(
                                'success' => true,
                                'is_active' => $walker_data->is_active,
                                'is_approved' => $walker_data->is_approved,
                                'locationdata' => $locations,
                            );
                            $response_code = 200;
                        } else {
                            /* $driver = Keywords::where('id', 1)->first();
                              $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $driver->keyword . ' ID', 'error_code' => 407); */
                            $response_array = array(
                                'success' => false,
                                'is_active' => $walker_data->is_active,
                                'is_approved' => $walker_data->is_approved,
                                'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID',
                                'error_code' => 407,
                            );
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array(
                            'success' => false,
                            'is_active' => $walker_data->is_active,
                            'is_approved' => $walker_data->is_approved,
                            'error' => 'Service ID Not Found',
                            'error_code' => 408,
                        );
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $driver = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // walker started
    public function request_walker_started()
    {
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->confirmed_walker != 0) {
                                    $request->is_walker_started = 1;
                                    $request->save();
                                    Log::info('loc' . $latitude . $latitude);

                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];

                                    if (!isset($angle)) {
                                        $angle = get_angle($walker_data->latitude, $walker_data->longitude, $latitude, $longitude);
                                    }

                                    $walker_data->old_latitude = $walker_data->latitude;
                                    $walker_data->old_longitude = $walker_data->longitude;
                                    $walker_data->bearing = $angle;
                                    $walker_data->latitude = $latitude;
                                    $walker_data->longitude = $longitude;
                                    $walker_data->save();

                                    // Send Notification
                                    $msg_array = array();
                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker_data = array();
                                    $walker_data['first_name'] = $walker->first_name;
                                    $walker_data['last_name'] = $walker->last_name;
                                    $walker_data['phone'] = $walker->phone;
                                    $walker_data['bio'] = $walker->bio;
                                    $walker_data['picture'] = $walker->picture;
                                    $walker_data['latitude'] = $walker->latitude;
                                    $walker_data['longitude'] = $walker->longitude;
                                    $walker_data['type'] = $walker->type;
                                    $walker_data['rating'] = $walker->rate;
                                    $walker_data['num_rating'] = $walker->rate_count;
                                    $walker_data['car_model'] = $walker->car_model;
                                    $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                    /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                      $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $bill = array();
                                    if ($request->is_completed == 1) {
                                        $bill['distance'] = (string)convert($request->distance, $unit);
                                        $bill['unit'] = $unit_set;
                                        $bill['time'] = $request->time;
                                        $bill['base_price'] = $request->base_price;
                                        $bill['distance_cost'] = $request->distance_cost;
                                        $bill['time_cost'] = $request->time_cost;
                                        $bill['total'] = $request->total;
                                        $bill['is_paid'] = $request->is_paid;
                                    }

                                    $response_array = array(
                                        'success' => true,
                                        'request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'payment_mode' => $request->payment_data,
                                        'walker' => $walker_data,
                                        'bill' => $bill,
                                    );

                                    $message = $response_array;
                                    /* $driver = Keywords::where('id', 1)->first();
                                      $title = '' . $driver->keyword . ' has started moving towards you'; */
                                    $title = '' . Config::get('app.generic_keywords.Provider') . ' has started moving towards you';

                                    send_notifications($request->owner_id, "owner", $title, $message);


                                    $response_array = array('success' => true);
                                    $response_code = 200;
                                } else {
                                    /* $driver = Keywords::where('id', 1)->first();
                                      $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' not yet confirmed', 'error_code' => 413); */
                                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' not yet confirmed', 'error_code' => 413);
                                    $response_code = 200;
                                }
                            } else {
                                /* $driver = Keywords::where('id', 1)->first();
                                  $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $driver->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $driver = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // walked arrived
    public function request_walker_arrived()
    {
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                )
            );

            /* $driver = Keywords::where('id', 1)->first(); */

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {
                                if ($request->is_walker_started == 1) {
                                    $request->is_walker_arrived = 1;
                                    try {

                                        $date = new DateTime();
                                        $date->setTimestamp(time());
                                        $dt = $date->format('Y-m-d H:i:s');
                                        $request->arrival_timestamp = $dt;
                                        $date = new DateTime(strval($request->request_start_time));
                                        $request->customer_waiting_time = time() - $date->getTimestamp();
                                    } catch (Exception $e) {
                                        Log::info("err saving trip starting parameters");
                                        Log::info($e->getMessage());
                                    }

                                    $request->save();

                                    Log::info('loc' . $latitude . $latitude);
                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker_data->latitude, $walker_data->longitude, $latitude, $longitude);
                                    }
                                    $walker_data->old_latitude = $walker_data->latitude;
                                    $walker_data->old_longitude = $walker_data->longitude;
                                    $walker_data->bearing = $angle;
                                    $walker_data->latitude = $latitude;
                                    $walker_data->longitude = $longitude;
                                    $walker_data->save();

                                    // Send Notification
                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker_data = array();
                                    $walker_data['first_name'] = $walker->first_name;
                                    $walker_data['last_name'] = $walker->last_name;
                                    $walker_data['phone'] = $walker->phone;
                                    $walker_data['bio'] = $walker->bio;
                                    $walker_data['picture'] = $walker->picture;
                                    $walker_data['latitude'] = $walker->latitude;
                                    $walker_data['longitude'] = $walker->longitude;
                                    $walker_data['type'] = $walker->type;
                                    $walker_data['rating'] = $walker->rate;
                                    $walker_data['num_rating'] = $walker->rate_count;
                                    $walker_data['car_model'] = $walker->car_model;
                                    $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                    /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                      $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */


                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $bill = array();
                                    if ($request->is_completed == 1) {
                                        $bill['distance'] = (string)convert($request->distance, $unit);
                                        $bill['unit'] = $unit_set;
                                        $bill['time'] = $request->time;
                                        $bill['base_price'] = $request->base_price;
                                        $bill['distance_cost'] = $request->distance_cost;
                                        $bill['time_cost'] = $request->time_cost;
                                        $bill['total'] = $request->total;
                                        $bill['is_paid'] = $request->is_paid;
                                    }

                                    $response_array = array(
                                        'success' => true,
                                        'request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'walker' => $walker_data,
                                        'payment_mode' => $request->payment_data,
                                        'bill' => $bill,
                                    );
                                    /* $driver = Keywords::where('id', 1)->first();

                                      $title = '' . $driver->keyword . ' has arrived at your place'; */
                                    $title = '' . Config::get('app.generic_keywords.Provider') . ' has arrived at your place';

                                    $message = $response_array;

                                    send_notifications($request->owner_id, "owner", $title, $message);

                                    // Send SMS
                                    $owner = Owner::find($request->owner_id);
                                    $settings = Settings::where('key', 'sms_when_provider_arrives')->first();
                                    $pattern = $settings->value;
                                    $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                    $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);
                                    $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                    sms_notification($request->owner_id, 'owner', $pattern);

                                    $response_array = array('success' => true);
                                    $response_code = 200;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Service not yet started', 'error_code' => 413);
                                    $response_code = 200;
                                }
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $driver->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // walk started
    public function request_walk_started()
    {
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                )
            );

            /* $var = Keywords::where('id', 1)->first(); */

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->is_walker_arrived == 1) {
                                    $request->is_started = 1;
                                    try {

                                        $date = new DateTime();
                                        $date->setTimestamp(time());
                                        $dt = $date->format('Y-m-d H:i:s');
                                        $request->origin_latitude = $latitude;
                                        $request->origin_longitude = $longitude;
                                        $request->pickup_timestamp = $dt;
                                    } catch (Exception $e) {
                                        Log::info("err saving trip starting parameters");
                                        Log::info($e->getMessage());
                                    }
                                    $request->save();

                                    Log::info($longitude . "lat long" . $latitude);

                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker_data->latitude, $walker_data->longitude, $latitude, $longitude);
                                    }
                                    $walk_location = new WalkLocation;
                                    $walk_location->latitude = $latitude;
                                    $walk_location->longitude = $longitude;
                                    $walk_location->request_id = $request_id;
                                    $walk_location->bearing = $angle;
                                    $walk_location->save();

                                    // Send Notification
                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker->old_latitude = $walker->latitude;
                                    $walker->old_longitude = $walker->longitude;
                                    $walker->latitude = $latitude;
                                    $walker->longitude = $longitude;
                                    $walker->bearing = $angle;
                                    $walker->save();

                                    $walker_data = array();
                                    $walker_data['first_name'] = $walker->first_name;
                                    $walker_data['last_name'] = $walker->last_name;
                                    $walker_data['phone'] = $walker->phone;
                                    $walker_data['bio'] = $walker->bio;
                                    $walker_data['picture'] = $walker->picture;
                                    $walker_data['latitude'] = $walker->latitude;
                                    $walker_data['longitude'] = $walker->longitude;
                                    $walker_data['type'] = $walker->type;
                                    $walker_data['rating'] = $walker->rate;
                                    $walker_data['num_rating'] = $walker->rate_count;
                                    $walker_data['car_model'] = $walker->car_model;
                                    $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                    /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                      $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $bill = array();
                                    if ($request->is_completed == 1) {
                                        $bill['distance'] = (string)convert($request->distance, $unit);
                                        $bill['unit'] = $unit_set;
                                        $bill['time'] = $request->time;
                                        $bill['base_price'] = $request->base_price;
                                        $bill['distance_cost'] = $request->distance_cost;
                                        $bill['time_cost'] = $request->time_cost;
                                        $bill['total'] = $request->total;
                                        $bill['is_paid'] = $request->is_paid;
                                    }

                                    $response_array = array(
                                        'success' => true,
                                        'request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'start_time' => $request->pickup_timestamp,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'walker' => $walker_data,
                                        'payment_mode' => $request->payment_data,
                                        'bill' => $bill,
                                    );
                                    /* $var = Keywords::where('id', 4)->first();
                                      $title = 'Your ' . $var->keyword . ' has been started'; */
                                    $title = 'Your ' . Config::get('app.generic_keywords.Trip') . ' has been started';

                                    $message = $response_array;

                                    send_notifications($request->owner_id, "owner", $title, $message);


                                    $response_array = array('success' => true, 'start_time' => $request->pickup_timestamp);
                                    $response_code = 200;
                                } else {
                                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' not yet arrived', 'error_code' => 413); */
                                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' not yet arrived', 'error_code' => 413);
                                    $response_code = 200;
                                }
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // walk completed
    public function request_walk_completed()
    {
        $review_walker = "";
        pay_fail:
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            $distance = Input::get('distance');
            $time = Input::get('time');
            $drop_address = Input::get('drop_address');
            Log::info('drop address' . $drop_address);

            $D_latitude = Input::get('latitude');
            $D_longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }

            Log::info("walker location queue on click on trip");
            Log::info('distance input = ' . print_r($distance, true));
            Log::info('time input = ' . print_r($time, true));

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'distance' => $distance,
                    /* 'time' => $time, */
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                    'distance' => 'required',
                    /* 'time' => 'required', */
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $cash_card_user = "";
                $payment_type = "";
                $walker_payment_remaining = 0;
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        $providertype = ProviderType::where('id', $walker_data->type)->first();
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            // get current time stamp for drop off
                            $dt = new DateTime();
                            $dt->setTimestamp(time());
                            $dt = $dt->format('Y-m-d H:i:s');
                            $request->dropoff_timestamp = $dt;

                            $to_time = strtotime($dt);
                            $from_time = strtotime($request->pickup_timestamp);
                            // get time difference
                            $time = round(abs($to_time - $from_time) / 60, 2);
//                            $time = $request->time;
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->is_started == 1 && $request->is_completed == 0) {

                                    $settings = Settings::where('key', 'default_charging_method_for_users')->first();
                                    $pricing_type = $settings->value;
                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;

                                    Log::info('distance = ' . print_r($distance, true));

                                    $reqserv = RequestServices::where('request_id', $request_id)->get();
                                    $actual_total = 0;
                                    $price_per_unit_distance = 0;
                                    $price_per_unit_time = 0;
                                    $base_price = 0;
                                    foreach ($reqserv as $rse) {
                                        Log::info('type = ' . print_r($rse->type, true));
                                        $protype = ProviderType::where('id', $rse->type)->first();
                                        $pt = ProviderServices::where('provider_id', $walker_id)->where('type', $rse->type)->first();
                                        if ($pt->base_price == 0) {
                                            /* $setbase_price = Settings::where('key', 'base_price')->first();
                                              $base_price = $setbase_price->value; */
                                            $base_price = $providertype->base_price;
                                            $rse->base_price = $base_price;
                                        } else {
                                            $base_price = $pt->base_price;
                                            $rse->base_price = $base_price;
                                        }

                                        $is_multiple_service = Settings::where('key', 'allow_multiple_service')->first();
                                        if ($is_multiple_service->value == 0) {

                                            if ($pt->price_per_unit_distance == 0) {
                                                /* $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                                  $price_per_unit_distance = $setdistance_price->value * $distance;
                                                  $rse->distance_cost = $price_per_unit_distance; */
                                                if ($distance <= $providertype->base_distance) {
                                                    $price_per_unit_distance = 0;
                                                } else {
                                                    $price_per_unit_distance = $providertype->price_per_unit_distance * ($distance - $providertype->base_distance);
                                                }
                                                $rse->distance_cost = $price_per_unit_distance;
                                            } else {
                                                if ($distance <= $providertype->base_distance) {
                                                    $price_per_unit_distance = 0;
                                                } else {
                                                    $price_per_unit_distance = $pt->price_per_unit_distance * ($distance - $providertype->base_distance);
                                                }
                                                $rse->distance_cost = $price_per_unit_distance;
                                            }

                                            if ($pt->price_per_unit_time == 0) {
                                                /* $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                                  $price_per_unit_time = $settime_price->value * $time; */
                                                $price_per_unit_time = $providertype->price_per_unit_time * $time;
                                                $rse->time_cost = $price_per_unit_time;
                                            } else {
                                                $price_per_unit_time = $pt->price_per_unit_time * $time;
                                                $rse->time_cost = $price_per_unit_time;
                                            }
                                        }

                                        $total = $base_price + $price_per_unit_distance + $price_per_unit_time;
                                        $total = round($total);
                                        Log::info('total price = ' . print_r($total, true));
                                        $rse->total = $total;
                                        $rse->save();
                                        $actual_total = $actual_total + $base_price + $price_per_unit_distance + $price_per_unit_time;
                                        $actual_total = round($actual_total);
                                        Log::info('total_price = ' . print_r($actual_total, true));
                                    }

                                    $rs = RequestServices::where('request_id', $request_id)->get();
                                    $total = 0;
                                    foreach ($rs as $key) {
                                        Log::info('total = ' . print_r($key->total, true));
                                        $total = $total + $key->total;
                                    }
                                    if ($providertype->min_price > $total)
                                        $total = $providertype->min_price;
                                    $request->used_credit_cap = $total * 0.2;
                                    $request->added_to_credit_cap = $total * 0.8;
                                    $billnow = floor($total);
                                    $used_credit_user = $total - floor($total);
                                    $owner = Owner::find($request->owner_id);
                                    if ($owner->use_credit_first == 1 || $owner->credit < 0) {
                                        $total = $owner->credit - $billnow;
                                        if ($total >= 0) {
                                            $total = 0;
                                        } else {
                                            $total = $total * -1;
                                        }
                                    }

                                    Log::info('total after credit = ' . $total);
                                    Log::info('bill after credit = ' . $billnow);
                                    //$request = Requests::find($request_id);
                                    $request->used_credit_user = $used_credit_user;
                                    $request->bill = $billnow;
                                    $request->is_completed = 1;
                                    $request->distance = $distance;
                                    $request->time = $time;
                                    $request->security_key = NULL;
                                    $request->total = $total;
                                    $request->drop_address = $drop_address;
                                    $request->save();
                                    $owner_data = Owner::where('id', $request->owner_id)->first();
                                    /* GET REFERRAL & PROMO INFO */
                                    $prom_act = $prom_for_card = $prom_for_cash = $ref_act = $ref_for_card = $ref_for_cash = $ref_total = $promo_total = 0;
                                    $settings = Settings::where('key', 'promotional_code_activation')->first();
                                    $prom_act = $settings->value;

                                    $settings = Settings::where('key', 'referral_code_activation')->first();
                                    $ref_act = $settings->value;
                                    /* GET REFERRAL & PROMO INFO END */
                                    $cash_card_user = $request->payment_mode;
                                    if ($request->payment_mode == 0) {
                                        // card payment
                                        $walker_payment_remaining = $total;
                                        if ($prom_act) {
                                            $settings = Settings::where('key', 'get_promotional_profit_on_card_payment')->first();
                                            $prom_for_card = $settings->value;
                                            if ($prom_for_card) {
                                                if ($billnow > 0) {
                                                    if ($pcode = PromoCodes::where('id', $request->promo_id)->first()) {
                                                        $billnowtemp = 0;
                                                        $promo_total_added_to_credit = 0;
                                                        if ($pcode->type == 1) {
                                                            $promo_total = $billnow * (($pcode->value) / 100);
                                                            $promo_total_added_to_credit = $promo_total - floor($promo_total);
                                                            $promo_total = floor($promo_total);
                                                            $billnowtemp = $billnow - $promo_total;
                                                        } else {
                                                            $promo_total = $pcode->value;
                                                            $promo_total_added_to_credit = $promo_total - floor($promo_total);
                                                            $promo_total = floor($promo_total);
                                                            $billnowtemp = $billnow - $promo_total;
                                                        }
                                                        if ($billnowtemp < 0) {
                                                            $total = $total - $billnow;
                                                            $billnow = 0;
                                                        } else {
                                                            $billnow = $billnowtemp;
                                                            $total = $total - $promo_total;
                                                        }
                                                        if ($total < 0) {
                                                            $total = 0;
                                                        }
                                                        $request = Requests::find($request_id);
                                                        $request->added_to_credit_user = $promo_total_added_to_credit;
                                                        $request->bill = $billnow;
                                                        $request->total = $total;
                                                        $request->promo_payment = $promo_total;
                                                        Log::info("promo_total" . $promo_total);
                                                        Log::info("billnow" . $billnow);
                                                        Log::info("total" . $total);
                                                        $request->save();
                                                    }
                                                }
                                            }
                                        }
                                        if ($ref_act) {
                                            $settings = Settings::where('key', 'get_referral_profit_on_card_payment')->first();
                                            $ref_for_card = $settings->value;

                                            if ($ref_for_card) {
                                                // charge client
                                                $ledger = Ledger::where('owner_id', $request->owner_id)->first();
                                                if ($ledger) {
                                                    $balance = $ledger->amount_earned - $ledger->amount_spent;
                                                    Log::info('ledger balance = ' . print_r($balance, true));
                                                    if ($balance > 0) {
                                                        if ($total > 0) {
                                                            if ($total > $balance) {
                                                                $ref_total = $balance;
                                                                $ledger_temp = Ledger::find($ledger->id);
                                                                $ledger_temp->amount_spent = $ledger_temp->amount_spent + $balance;
                                                                $ledger_temp->save();
                                                                $total = $total - $balance;
                                                            } else {
                                                                $ref_total = $total;
                                                                $ledger_temp = Ledger::find($ledger->id);
                                                                $ledger_temp->amount_spent = $ledger_temp->amount_spent + $total;
                                                                $ledger_temp->save();
                                                                $total = 0;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($request->payment_mode == 1) {
                                        // cash
                                        $walker_payment_remaining = 0;
                                        if ($prom_act) {
                                            $settings = Settings::where('key', 'get_promotional_profit_on_cash_payment')->first();
                                            $prom_for_cash = $settings->value;
                                            if ($prom_for_cash) {
                                                if ($billnow > 0) {
                                                    if ($pcode = PromoCodes::where('id', $request->promo_id)->first()) {
                                                        $billnowtemp = 0;
                                                        $promo_total_added_to_credit = 0;
                                                        if ($pcode->type == 1) {
                                                            $promo_total = $billnow * (($pcode->value) / 100);
                                                            $promo_total_added_to_credit = $promo_total - floor($promo_total);
                                                            $promo_total = floor($promo_total);
                                                            $billnowtemp = $billnow - $promo_total;
                                                        } else {
                                                            $promo_total = $pcode->value;
                                                            $promo_total_added_to_credit = $promo_total - floor($promo_total);
                                                            $promo_total = floor($promo_total);
                                                            $billnowtemp = $billnow - $promo_total;
                                                        }
                                                        if ($billnowtemp < 0) {
                                                            $total = $total - $billnow;
                                                            $billnow = 0;
                                                        } else {
                                                            $billnow = $billnowtemp;
                                                            $total = $total - $promo_total;
                                                        }
                                                        if ($total < 0) {
                                                            $total = 0;
                                                        }
                                                        $request = Requests::find($request_id);
                                                        $request->added_to_credit_user = $promo_total_added_to_credit;
                                                        $request->bill = $billnow;
                                                        $request->total = $total;
                                                        $request->promo_payment = $promo_total;
                                                        Log::info("promo_total" . $promo_total);
                                                        Log::info("billnow" . $billnow);
                                                        Log::info("total" . $total);
                                                        $request->save();
                                                    }
                                                }
                                            }
                                        }
                                        if ($ref_act) {
                                            $settings = Settings::where('key', 'get_referral_profit_on_cash_payment')->first();
                                            $ref_for_cash = $settings->value;

                                            if ($ref_for_cash) {
                                                // charge client
                                                $ledger = Ledger::where('owner_id', $request->owner_id)->first();
                                                if ($ledger) {
                                                    $balance = $ledger->amount_earned - $ledger->amount_spent;
                                                    Log::info('ledger balance = ' . print_r($balance, true));
                                                    if ($balance > 0) {
                                                        if ($total > 0) {
                                                            if ($total > $balance) {
                                                                $ref_total = $balance;
                                                                $ledger_temp = Ledger::find($ledger->id);
                                                                $ledger_temp->amount_spent = $ledger_temp->amount_spent + $balance;
                                                                $ledger_temp->save();
                                                                $total = $total - $balance;
                                                            } else {
                                                                $ref_total = $total;
                                                                $ledger_temp = Ledger::find($ledger->id);
                                                                $ledger_temp->amount_spent = $ledger_temp->amount_spent + $total;
                                                                $ledger_temp->save();
                                                                $total = 0;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /* $promo_discount = 0;
                                      if ($pcode = PromoCodes::where('id', $request->promo_code)->where('type', 1)->first()) {
                                      $discount = ($pcode->value) / 100;
                                      $promo_discount = $total * $discount;
                                      $total = $total - $promo_discount;
                                      if ($total < 0) {
                                      $total = 0;
                                      }
                                      }

                                      $request->total = $total; */

                                    Log::info('final total = ' . print_r($total, true));

                                    $cod_sett = Settings::where('key', 'cod')->first();
                                    $allow_cod = $cod_sett->value;
                                    if ($request->payment_mode == 1 and $allow_cod == 1) {
                                        $request->is_paid = 1;
                                        $payment_type = 'Payment By cash';
                                        Log::info('allow_cod');
                                    } else if ($request->payment_mode == 0) {

                                        Log::info('normal payment. Stored cards');
                                        if ($total == 0) {
                                            $request->is_paid = 1;
                                        } else {

                                            $request = Requests::find($request_id);
                                            $payment_data = Payment::where('owner_id', $request->owner_id)->where('is_default', 1)->first();
                                            if (!$payment_data)
                                                $payment_data = Payment::where('owner_id', $request->owner_id)->first();

                                            if ($payment_data) {
                                                $setransfer = Settings::where('key', 'transfer')->first();
                                                $transfer_allow = $setransfer->value;
                                                if (Config::get('app.default_payment') == 'payfort') {
                                                    //dd($customer_id);
                                                    try {
                                                        $merchant_reference = 'kcondm' . $owner_data->id . '-' . ($request->total * 100) . '_' . microtime(true);
                                                        $requestparameterstring = 'asdaredcsreaccess_code=Z6ouzQeUwoNwpTxcqRkzamount=' . ($request->total * 100) . 'command=PURCHASEcurrency=SARcustomer_email=' . $owner->email .
                                                            'eci=MOTOlanguage=enmerchant_identifier=fLPmDVuxmerchant_reference=' . $merchant_reference . 'token_name=' . $payment_data->card_token . 'asdaredcsre';
                                                        $signature = hash("sha256", $requestparameterstring, false);
                                                        $client = new GuzzleHttp\Client();
                                                        $response = $client->post("https://sbpaymentservices.payfort.com/FortAPI/paymentApi",
                                                            ['json' => [
                                                                'access_code' => 'Z6ouzQeUwoNwpTxcqRkz',
                                                                'amount' => ($request->total * 100),
                                                                'command' => 'PURCHASE',
                                                                'currency' => 'SAR',
                                                                'customer_email' => $owner->email,
                                                                'eci' => 'MOTO',
                                                                'language' => 'en',
                                                                'merchant_identifier' => 'fLPmDVux',
                                                                'merchant_reference' => $merchant_reference,
                                                                'signature' => $signature,
                                                                'token_name' => $payment_data->card_token
                                                            ]]);
                                                        Log::info("loooog payfort");
                                                        $resobjPayfort = $response->getBody(true);
                                                        $objPayfort = json_decode($resobjPayfort, true);
                                                        Log::info('$resobjPayfort  = ' . print_r($objPayfort, true));
                                                        $response_code = $objPayfort['response_code'];
                                                        $response_message = $objPayfort['response_message'];
                                                        $fort_id = $objPayfort['fort_id'];
                                                        $status = $objPayfort['status'];
                                                        /*$transaction_history = new TransactionHistory();
                                                        $transaction_history->request_id = $request_id;
                                                        $transaction_history->fort_id = $fort_id;
                                                        $transaction_history->amount = $request->total;
                                                        $transaction_history->response_message = $response_message;
                                                        $transaction_history->response_code = $response_code;
                                                        $transaction_history->status = $status;
                                                        $transaction_history->save();*/
                                                        Log::info('$response_code  = ' . print_r($response_code, true));
                                                        if ($response_code == '14000') {
                                                            Log::info('payement succeded ');
                                                            $request->is_paid = 1;

                                                            $payment_type = "Creditcard Card Payment Successfully";
                                                            Log::info('card payment ' . $payment_type);
                                                            $request->used_credit_cap = 0;
                                                            $request->current_credit_user = $owner->credit;
                                                            $request->current_credit_cap = $walker_data->credit;
                                                            if ($owner->credit < 0) {
                                                                $request->added_to_credit_user = ($owner->credit * -1);
                                                                Log::info('added_to_credit_user  = ' . print_r($request->added_to_credit_user, true));
                                                            } else
                                                                $request->added_to_credit_user = 0;
                                                            $request->is_completed = 1;
                                                            $request->save();
                                                        } else {
                                                            Log::info('payement not succeded ');
                                                            $request->is_paid = 0;
                                                            $request->is_completed = 0;
                                                            $request->save();
                                                            $payment_type = "Creditcard Card Payment Fail";
                                                            $ledger = Ledger:: where('owner_id', $request->owner_id)->first();
                                                            if ($ledger) {
                                                                $ledger_temp = Ledger::find($ledger->id);
                                                                $ledger_temp->amount_spent = $ledger_temp->amount_spent - $ref_total;
                                                                $ledger_temp->save();
                                                            }
                                                            $change_to_cash = Requests::find($request_id);
                                                            $change_to_cash->payment_mode = 1;
                                                            $change_to_cash->save();

                                                            /* Client Side Push */
                                                            $title = 'Your card is declined, please pay cash to ' . Config::get('app.generic_keywords.Provider') . ' for your ' . Config::get('app.generic_keywords.Trip') . '.';
                                                            $response_array = array(
                                                                'success' => true, 'message' => $title,);
                                                            $message = $response_array;
                                                            send_notifications($request->owner_id, "owner", $title, $message);
                                                            /* Client Side Push END */
                                                            /* Driver Side Push */
                                                            $title = 'Please collect cash from ' . Config::get('app.generic_keywords.User') . ' for your ' . Config::get('app.generic_keywords.Trip') . '.';
                                                            $response_array = array('success' => true,
                                                                'message' => $title,
                                                            );
                                                            $message = $response_array;
                                                            send_notifications($walker_id, "walker", $title, $message);
                                                            /* Driver Side Push END */
                                                            goto pay_fail;
                                                        }


                                                        //var_dump($objPayfort);
                                                    } catch (Exception $e) {
                                                        Log::info($e->getMessage());
                                                        $request->is_paid = 0;
                                                        // Invalid parameters were supplied to Stripe's API
                                                        $ownr = Owner::find($request->owner_id);
                                                        $ownr->debt = $total;
                                                        $ownr->save();
                                                        $response_array = array('error' => $e->getMessage());
                                                        $response_code = 200;
                                                        $response = Response::json($response_array, $response_code);
                                                        return $response;
                                                    }
                                                    $settng = Settings::where('key', 'service_fee')->first();
                                                    if ($transfer_allow == 1 && $walker_data->merchant_id != "" && Config::get('app.generic_keywords.Currency') == '$') {

                                                        $transfer = Stripe_Transfer::create(array(
                                                                "amount" => floor($total - ($settng->value * $total / 100)) * 100, // amount in cents
                                                                "currency" => "usd",
                                                                "recipient" => $walker_data->merchant_id)
                                                        );
                                                        $request->transfer_amount = floor($total - $settng->value * $total / 100);
                                                    }
                                                }
                                                $request->card_payment = $total;
                                                $request->ledger_payment = $request->total - $total;
                                            }
                                        }
                                    }
                                    $request->card_payment = $total;
                                    $request->ledger_payment = $ref_total;
                                    $request->promo_payment = $promo_total;
                                    $request->payment_mode = $cash_card_user;
                                    $request->D_latitude = $D_latitude;
                                    $request->D_longitude = $D_longitude;


                                    $request->save();
                                    if ($request->is_paid == 1) {

                                        $owner = Owner::find($request->owner_id);
                                        $settings = Settings::where('key', 'sms_payment_generated')->first();
                                        $pattern = $settings->value;
                                        $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                        $pattern = str_replace('%id%', $request->id, $pattern);
                                        $pattern = str_replace('%user_mobile%', $owner->phone, $pattern);
                                        sms_notification(1, 'admin', $pattern);
                                    }

                                    $walker = Walker::find($walker_id);

                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker->latitude, $walker->longitude, $latitude, $longitude);
                                    }
                                    $walker->old_latitude = $walker->latitude;
                                    $walker->old_longitude = $walker->longitude;
                                    $walker->latitude = $latitude;
                                    $walker->longitude = $longitude;
                                    $walker->bearing = $angle;
                                    $walker->save();
                                    Log::info('distance walk location = ' . print_r($distance, true));
                                    $walk_location = new WalkLocation;
                                    $walk_location->latitude = $latitude;
                                    $walk_location->longitude = $longitude;
                                    $walk_location->request_id = $request_id;
                                    $walk_location->distance = $distance;
                                    $walk_location->bearing = $angle;
                                    $walk_location->save();

                                    // Send Notification
                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker_data = array();
                                    $walker_data['first_name'] = $walker->first_name;
                                    $walker_data['last_name'] = $walker->last_name;
                                    $walker_data['phone'] = $walker->phone;
                                    $walker_data['bio'] = $walker->bio;
                                    $walker_data['picture'] = $walker->picture;
                                    $walker_data['latitude'] = $walker->latitude;
                                    $walker_data['longitude'] = $walker->longitude;
                                    $walker_data['type'] = $walker->type;
                                    $walker_data['rating'] = $walker->rate;
                                    $walker_data['num_rating'] = $walker->rate_count;
                                    $walker_data['car_model'] = $walker->car_model;
                                    $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                    /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                      $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                    $requestserv = RequestServices::where('request_id', $request->id)->first();
                                    $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();
                                    $bill = array();
                                    /* $currency_selected = Keywords::find(5); */
                                    if ($request->is_completed == 1) {
                                        $settings = Settings::where('key', 'default_distance_unit')->first();
                                        $unit = $settings->value;
                                        $bill['payment_mode'] = $request->payment_mode;
                                        $bill['distance'] = (string)$distance;
                                        if ($unit == 0) {
                                            $unit_set = 'kms';
                                        } elseif ($unit == 1) {
                                            $unit_set = 'miles';
                                        }
                                        $bill['unit'] = $unit_set;
                                        $bill['time'] = floatval(sprintf2($request->time, 2));
                                        $bill['base_distance'] = $request_typ->base_distance;
                                        $bill['base_price'] = $request_typ->base_price;
                                        $bill['price_per_unit_distance'] = $request_typ->price_per_unit_distance;
                                        $bill['price_per_unit_time'] = $request_typ->price_per_unit_time;
                                        if ($requestserv->base_price != 0) {
                                            $bill['distance_cost'] = ($requestserv->distance_cost);
                                            $bill['time_cost'] = (floatval(sprintf2($requestserv->time_cost, 2)));
                                        } else {
                                            /* $setbase_price = Settings::where('key', 'base_price')->first();
                                              $bill['base_price'] = ($setbase_price->value); */
                                            /* $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                              $bill['distance_cost'] = ($setdistance_price->value); */
                                            $bill['distance_cost'] = ($providertype->price_per_unit_distance);
                                            /* $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                              $bill['time_cost'] = (floatval(sprintf2($settime_price->value, 2))); */
                                            $bill['time_cost'] = (floatval(sprintf2($providertype->price_per_unit_time, 2)));
                                        }
                                        $admins = Admin::first();
                                        $bill['walker']['email'] = $walker->email;
                                        $bill['admin']['email'] = $admins->username;
                                        if ($request->transfer_amount != 0) {
                                            $bill['walker']['amount'] = ($request->total - $request->transfer_amount);
                                            $bill['admin']['amount'] = ($request->transfer_amount);
                                        } else {
                                            $bill['walker']['amount'] = ($request->transfer_amount);
                                            $bill['admin']['amount'] = ($request->total - $request->transfer_amount);
                                        }
                                        /* $bill['currency'] = $currency_selected->keyword; */
                                        $bill['currency'] = Config::get('app.generic_keywords.Currency');
                                        $bill['actual_total'] = ($actual_total);
                                        $bill['total'] = ($request->total);
                                        $bill['bill'] = $request->bill;
                                        $bill['is_paid'] = $request->is_paid;
                                        if ($request->is_paid == 1) {
                                            $bill['current_credit'] = ($request->current_credit_cap);
                                            $bill['used_credit'] = ($request->used_credit_cap);
                                            $bill['added_to_credit'] = ($request->added_to_credit_cap);
                                        }
                                        $bill['promo_discount'] = ($promo_total);
                                        $bill['main_total'] = ($request->total);
                                        $total_amount = $request->total - $request->ledger_payment;// - $request->promo_payment;
                                        /*if($total_amount < 0)
                                            $total_amount = 0;*/
                                        $bill['total'] = ($total_amount);
                                        $bill['referral_bonus'] = ($request->ledger_payment);
                                        $bill['promo_bonus'] = ($request->promo_payment);
                                        $bill['payment_type'] = $request->payment_mode;
                                        $bill['is_paid'] = $request->is_paid;
                                    }

                                    $rservc = RequestServices::where('request_id', $request->id)->get();
                                    $typs = array();
                                    $typi = array();
                                    $typp = array();
                                    foreach ($rservc as $typ) {
                                        $typ1 = ProviderType::where('id', $typ->type)->first();
                                        $typ_price = ProviderServices::where('provider_id', $request->confirmed_walker)->where('type', $typ->type)->first();

                                        if ($typ_price->base_price > 0) {
                                            $typp1 = 0.00;
                                            $typp1 = $typ_price->base_price;
                                        } elseif ($typ_price->price_per_unit_distance > 0) {
                                            $typp1 = 0.00;
                                            foreach ($rservc as $key) {
                                                $typp1 = $typp1 + $key->distance_cost;
                                            }
                                        } else
                                            $typp1 = 0.00;

                                        $typs['name'] = $typ1->name;
                                        // $typs['icon']=$typ1->icon;
                                        $typs['price'] = $typp1;

                                        array_push($typi, $typs);
                                    }
                                    $bill['type'] = $typi;
                                    $rserv = RequestServices::where('request_id', $request_id)->get();
                                    $typs = array();
                                    foreach ($rserv as $typ) {
                                        $typ1 = ProviderType::where('id', $typ->type)->first();
                                        array_push($typs, $typ1->name);
                                    }

                                    $response_array = array(
                                        'success' => true,
                                        'request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'origin_latitude' => $request->origin_latitude,
                                        'origin_longitude' => $request->origin_longitude,
                                        'D_latitude' => $request->D_latitude,
                                        'D_longitude' => $request->D_longitude,
                                        'pickup_address' => $request->pickup_address,
                                        'drop_address' => $request->drop_address,
                                        'walker' => $walker_data,
                                        'monthly_id' => $request->monthly_id,
                                        'payment_mode' => $request->payment_mode,
                                        'bill' => $bill,
                                        'payment_option' => $request->payment_mode,
                                        'is_paid' => $request->is_paid,
                                    );
                                    $owner_data1 = array();
                                    $owner_data1['name'] = $owner_data->first_name . " " . $owner_data->last_name;
                                    $owner_data1['picture'] = $owner_data->picture;
                                    $owner_data1['phone'] = $owner_data->phone;
                                    $owner_data1['address'] = $owner_data->address;
                                    $owner_data1['bio'] = $owner_data->bio;
                                    $owner_data1['latitude'] = $owner_data->latitude;
                                    $owner_data1['longitude'] = $owner_data->longitude;
                                    $owner_data1['owner_dist_lat'] = $request->D_latitude;
                                    $owner_data1['owner_dist_long'] = $request->D_longitude;
                                    $owner_data1['dest_latitude'] = $request->D_latitude;
                                    $owner_data1['dest_longitude'] = $request->D_longitude;
                                    $owner_data1['payment_type'] = $request->payment_mode;
                                    $owner_data1['rating'] = $owner_data->rate;
                                    $owner_data1['num_rating'] = $owner_data->rate_count;
                                    $title = "Trip Completed";
                                    $dog1 = array();
                                    if ($dog = Dog::find($owner_data->dog_id)) {
                                        $dog1['name'] = $dog->name;
                                        $dog1['age'] = $dog->age;
                                        $dog1['breed'] = $dog->breed;
                                        $dog1['likes'] = $dog->likes;
                                        $dog1['picture'] = $dog->image_url;
                                    }
                                    $cards = "";
                                    /* $cards['none'] = ""; */
                                    $cardlist = Payment::where('owner_id', $owner_data->id)->where('is_default', 1)->first();
                                    if (count($cardlist) >= 1) {
                                        $cards = array();
                                        $default = $cardlist->is_default;
                                        if ($default == 1) {
                                            $cards['is_default_text'] = "default";
                                        } else {
                                            $cards['is_default_text'] = "not_default";
                                        }
                                        $cards['card_id'] = $cardlist->id;
                                        $cards['owner_id'] = $cardlist->owner_id;
                                        $cards['customer_id'] = $cardlist->customer_id;
                                        $cards['last_four'] = $cardlist->last_four;
                                        $cards['card_token'] = $cardlist->card_token;
                                        $cards['card_type'] = $cardlist->card_type;
                                        $cards['is_default'] = $default;
                                    }

                                    $chagre = array();
                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $chagre['unit'] = $unit_set;
                                    $requestserv = RequestServices::where('request_id', $request->id)->first();
                                    if ($requestserv->base_price != 0) {
                                        $chagre['base_price'] = ($requestserv->base_price);
                                        $chagre['distance_price'] = ($requestserv->distance_cost);
                                        $chagre['price_per_unit_time'] = ($requestserv->time_cost);
                                    } else {
                                        /* $setbase_price = Settings::where('key', 'base_price')->first();
                                          $chagre['base_price'] = ($setbase_price->value); */
                                        $chagre['base_price'] = ($providertype->base_price);
                                        /* $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                          $chagre['distance_price'] = ($setdistance_price->value); */
                                        $chagre['distance_price'] = ($providertype->price_per_unit_distance);
                                        /* $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                          $chagre['price_per_unit_time'] = ($settime_price->value); */
                                        $chagre['price_per_unit_time'] = ($providertype->price_per_unit_time);
                                    }
                                    $chagre['total'] = ($request->total);
                                    $chagre['is_paid'] = $request->is_paid;
                                    /* $var = Keywords::where('id', 4)->first(); */
                                    $title = 'Your ' . Config::get('app.generic_keywords.Trip') . ' is completed';

                                    $message = $response_array;

                                    send_notifications($request->owner_id, "owner", $title, $message);

                                    // Send SMS
                                    $owner = Owner::find($request->owner_id);
                                    $settings = Settings::where('key', 'sms_when_provider_completes_job')->first();
                                    $pattern = $settings->value;
                                    $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                    $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);
                                    $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                    $pattern = str_replace('%amount%', $request->total, $pattern);
                                    sms_notification($request->owner_id, 'owner', $pattern);
                                    $id = $request->id;
                                    // send email
                                    /* $settings = Settings::where('key', 'email_request_finished')->first();
                                      $pattern = $settings->value;
                                      $pattern = str_replace('%id%', $request->id, $pattern);
                                      $pattern = str_replace('%url%', web_url() . "/admin/request/map/" . $request->id, $pattern);
                                      $subject = "Request Completed";
                                      email_notification(2, 'admin', $pattern, $subject); */
                                    // $settings = Settings::where('key','email_invoice_generated_user')->first();
                                    // $pattern = $settings->value;
                                    // $pattern = str_replace('%id%', $request->id, $pattern);
                                    // $pattern = str_replace('%amount%', $request->total, $pattern);

                                    $email_data = array();

                                    $email_data['name'] = $owner->first_name;
                                    $email_data['emailType'] = 'user';
                                    $email_data['base_price'] = $bill['base_price'];
                                    $email_data['distance'] = $bill['distance'];
                                    $email_data['time'] = $bill['time'];
                                    $email_data['unit'] = $bill['unit'];
                                    $email_data['total'] = $bill['total'];
                                    $email_data['payment_mode'] = $bill['payment_mode'];
                                    $email_data['actual_total'] = ($actual_total);
                                    $email_data['is_paid'] = $request->is_paid;
                                    $email_data['promo_discount'] = ($promo_total);

                                    $request_services = RequestServices::where('request_id', $request->id)->first();

                                    $locations = WalkLocation::where('request_id', $request->id)
                                        ->orderBy('id')
                                        ->get();
                                    $count = round(count($locations) / 50);
                                    $start = WalkLocation::where('request_id', $request->id)
                                        ->orderBy('id')
                                        ->first();
                                    $end = WalkLocation::where('request_id', $request->id)
                                        ->orderBy('id', 'desc')
                                        ->first();

                                    $map = "https://maps-api-ssl.google.com/maps/api/staticmap?size=249x249&style=feature:landscape|visibility:off&style=feature:poi|visibility:off&style=feature:transit|visibility:off&style=feature:road.highway|element:geometry|lightness:39&style=feature:road.local|element:geometry|gamma:1.45&style=feature:road|element:labels|gamma:1.22&style=feature:administrative|visibility:off&style=feature:administrative.locality|visibility:on&style=feature:landscape.natural|visibility:on&scale=2&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-start@2x.png|$start->latitude,$start->longitude&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-finish@2x.png|$end->latitude,$end->longitude&path=color:0x2dbae4ff|weight:4";
                                    $skip = 0;
                                    foreach ($locations as $location) {
                                        if ($skip == $count) {
                                            $map .= "|$location->latitude,$location->longitude";
                                            $skip = 0;
                                        }
                                        $skip++;
                                    }

                                    $start_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$start->latitude,$start->longitude"), TRUE);
                                    $start_address = "Address not found";
                                    if (isset($start_location['results'][0]['formatted_address'])) {
                                        $start_address = $start_location['results'][0]['formatted_address'];
                                    }
                                    $end_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$end->latitude,$end->longitude"), TRUE);
                                    $end_address = "Address not found";
                                    if (isset($end_location['results'][0]['formatted_address'])) {
                                        $end_address = $end_location['results'][0]['formatted_address'];
                                    }

                                    $email_data['start_location'] = $start_location;
                                    $email_data['end_location'] = $end_location;

                                    $walker = Walker::find($request->confirmed_walker);
                                    $walker_review = WalkerReview::where('request_id', $id)->first();
                                    if ($walker_review) {
                                        $rating = round($walker_review->rating);
                                    } else {
                                        $rating = 0;
                                    }

                                    $email_data['map'] = $map;
                                    $settings = Settings::where('key', 'admin_email_address')->first();
                                    $admin_email = $settings->value;
                                    $requestserv = RequestServices::where('request_id', $request->id)->orderBy('id', 'DESC')->first();
                                    $get_type_name = ProviderType::where('id', $requestserv->type)->first();
                                    $detail = array(
                                        'admin_eamil' => $admin_email,
                                        'request' => $request,
                                        'start_address' => $start_address,
                                        'end_address' => $end_address,
                                        'start' => $start,
                                        'end' => $end,
                                        'map_url' => $map,
                                        'walker' => $walker,
                                        'rating' => $rating,
                                        'base_price' => $requestserv->base_price,
                                        'price_per_time' => $price_per_unit_time,
                                        'price_per_dist' => $price_per_unit_distance,
                                        'ref_bonus' => $request->ledger_payment,
                                        'promo_bonus' => $request->promo_payment,
                                        'dist_cost' => $requestserv->distance_cost,
                                        'time_cost' => $requestserv->time_cost,
                                        'type_name' => ucwords($get_type_name->name)
                                    );
                                    //send email to owner
                                    /* $subject = "Invoice Generated";
                                      send_email($request->owner_id, 'owner', $email_data, $subject, 'invoice'); */

                                    $subject = "Invoice Generated";
                                    //Log::info('email detail'. print_r($detail,true));
                                    //email_notification($request->owner_id, 'owner', $detail, $subject, 'invoice');

                                    //$subject = "Request Completed";
                                    //email_notification(1, 'admin', $detail, $subject, 'invoice');

                                    //send email to walker
                                    /* $subject = "Invoice Generated";
                                      $email_data['emailType'] = 'walker';
                                      send_email($request->confirmed_walker, 'walker', $email_data, $subject, 'invoice'); */
                                    //$subject = "Invoice Generated";
                                    // email_notification($request->confirmed_walker, 'walker', $detail, $subject, 'invoice');

                                    /* for speed test

                                     if ($request->is_paid == 1) {
                                        // send email
                                        $settings = Settings::where('key', 'admin_email_address')->first();
                                        $admin_email = $settings->value;
                                        $pattern = array('admin_eamil' => $admin_email, 'name' => 'Administrator', 'amount' => $request->total, 'req_id' => $request_id, 'web_url' => web_url());
                                        $subject = "Payment Done With " . $request_id . "";
                                        email_notification(1, 'admin', $pattern, $subject, 'pay_charged', null);
                                    } else {
                                        // send email
                                        $pattern = "Payment Failed for the request id " . $request->id . ".";

                                          $subject = "Payment Failed";
                                          email_notification(1, 'admin', $pattern, $subject);
                                    }*/
                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $distance = DB::table('walk_location')->where('request_id', $request_id)->max('distance');

                                    $end_time = DB::table('walk_location')
                                        ->where('request_id', $request_id)
                                        ->max('created_at');
                                    $request_data_1 = array('request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_started' => $request->is_started,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_dog_rated' => $request->is_dog_rated,
                                        'is_cancelled' => $request->is_cancelled,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'dest_latitude' => $request->D_latitude,
                                        'dest_longitude' => $request->D_longitude,
                                        'pickup_address' => $request->pickup_address,
                                        'drop_address' => $request->drop_address,
                                        'accepted_time' => $request->request_start_time,
                                        'payment_type' => $request->payment_mode,
                                        'distance' => (string)convert($distance, $unit),
                                        'unit' => $unit_set,
                                        'end_time' => $end_time,
                                        'owner' => $owner_data1,
                                        'dog' => $dog1,
                                        'bill' => $bill,
                                        'card_details' => $cards,
                                        'charge_details' => $chagre,
                                        'payment_option' => $request->is_paid);
                                    $response_array = array(
                                        'success' => true,
                                        'total' => ($total),
                                        'error' => $payment_type,
                                        /* 'currency' => $currency_selected->keyword, */
                                        'currency' => Config::get('app.generic_keywords.Currency'),
                                        'is_paid' => $request->is_paid,
                                        'request_id' => $request_id,
                                        'status' => $request->status,
                                        'confirmed_walker' => $request->confirmed_walker,
                                        'is_walker_started' => $request->is_walker_started,
                                        'is_walker_arrived' => $request->is_walker_arrived,
                                        'is_walk_started' => $request->is_started,
                                        'is_completed' => $request->is_completed,
                                        'is_cash_posted_cap' => $request->is_cash_posted_cap,
                                        'is_invoice_verified_user' => $request->is_invoice_verified_user,
                                        'is_invoice_verified_cap' => $request->is_invoice_verified_cap,
                                        'is_walker_rated' => $request->is_walker_rated,
                                        'pickup_address' => $request->pickup_address,
                                        'drop_address' => $request->drop_address,
                                        'walker' => $walker_data,
                                        'payment_mode' => $request->payment_mode,
                                        'bill' => $bill,
                                        'owner' => $owner_data1,
                                        'payment_option' => $request->is_paid,
                                        'request' => $request_data_1,
                                    );
                                    $response_code = 200;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Service not yet started', 'error_code' => 413);
                                    $response_code = 200;
                                }
                            } else {
                                /* $var = Keywords::where('id', 1)->first();
                                  $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Captian Post Cash on trip complete
    public function post_cash_amount()
    {
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $cash_paid = Input::get('cash_paid');
            $is_different = Input::get('is_different');

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'cash_paid' => $cash_paid,
                    'is_different' => $is_different,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'cash_paid' => 'required',
                    'is_different' => 'required',
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {
                                $request->cash_paid = $cash_paid;
                                $request->is_cash_posted_cap = 1;
                                $request->is_different_cap = $is_different;
                                $owner = Owner::find($request->owner_id);
                                $request->current_credit_user = ($owner->credit);
                                $request->current_credit_cap = $walker_data->credit;
                                $request->is_invoice_verified_user = 0;
                                if ($request->total != 0) {
                                    if ($cash_paid < $request->total) {

                                        if ($request->bill > $request->total) {
                                            $request->added_to_credit_user = $cash_paid - $request->total;
                                            $request->used_credit_user += $request->current_credit_user;

                                        } else {
                                            $request->added_to_credit_user = $cash_paid - $request->bill;
                                            $request->used_credit_user += 0.00;
                                        }
                                        if ($request->bill > $cash_paid) {
                                            $request->used_credit_cap = 0.00;
                                            $request->added_to_credit_cap = $request->added_to_credit_cap - $cash_paid;
                                        } else {
                                            $request->used_credit_cap = $cash_paid - $request->added_to_credit_cap;
                                            $request->added_to_credit_cap = 0.00;
                                        }
                                    } else {
                                        if ($request->current_credit_user > 0) {
                                            $request->used_credit_user += $request->current_credit_user;
                                            $request->added_to_credit_user += $cash_paid - $request->total;
                                        } else {
                                            $request->used_credit_user += 0;
                                            $request->added_to_credit_user += $cash_paid - $request->bill;
                                        }
                                        if ($request->bill > $cash_paid) {
                                            $request->used_credit_cap = 0.00;
                                            $request->added_to_credit_cap = $request->added_to_credit_cap - $cash_paid;
                                        } else {
                                            $request->used_credit_cap = $cash_paid - $request->added_to_credit_cap;
                                            $request->added_to_credit_cap = 0.00;
                                        }
                                    }
                                } else {
                                    $request->used_credit_user = $request->bill;
                                    $request->added_to_credit_user = $cash_paid;
                                    $request->used_credit_cap = $cash_paid;
                                    $request->added_to_credit_cap = $request->bill;
                                }

                                $request->save();
                                $response_array = array(
                                    'success' => true,
                                    'request_id' => $request_id,
                                    'unique_id' => 27
                                );
                                $title = 'Your cash payment has been received.';
                                $message = $response_array;
                                send_notifications($request->owner_id, "owner", $title, $message);
                                $response_code = 200;
                            } else {
                                /* $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function confirm_invoice()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $request_id = Input::get('request_id');
        $accepted = Input::get('accepted');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $request_id
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                if (is_token_active($walker_data->token_expiry)) {
                    $request = Requests::where('id', $request_id)->first();

                    if (ISSET($request)) {
                        if ($request->confirmed_walker = $walker_data->id) {
                            if ($accepted == 1) {
                                Requests::where('id', '=', $request_id)->update(array('is_invoice_verified_cap' => 1));
                                $confirmed_walker = Walker::find($request->confirmed_walker);
                                $confirmed_walker->credit = $confirmed_walker->credit + $request->added_to_credit_cap - $request->used_credit_cap;
                                $confirmed_walker->save();
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Captain confirmed invoice  successfully',
                                    'request_id' => $request_id
                                );


                                // Send SMS and EMAIL
                                $actual_total = 0;
                                $price_per_unit_distance = 0;
                                $price_per_unit_time = 0;
                                $base_price = 0;
                                $walker = Walker::find($request->confirmed_walker);
                                $owner = Owner::find($request->owner_id);
                                $requestserv = RequestServices::where('request_id', $request->id)->first();
                                $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();
                                $providertype = ProviderType::where('id', $walker->type)->first();
                                $bill = array();
                                /* $currency_selected = Keywords::find(5); */
                                if ($request->is_completed == 1) {
                                    $settings = Settings::where('key', 'default_distance_unit')->first();
                                    $unit = $settings->value;
                                    $bill['payment_mode'] = $request->payment_mode;
                                    $bill['distance'] = (string)$request->distance;
                                    if ($unit == 0) {
                                        $unit_set = 'kms';
                                    } elseif ($unit == 1) {
                                        $unit_set = 'miles';
                                    }
                                    $bill['unit'] = $unit_set;
                                    $bill['time'] = floatval(sprintf2($request->time, 2));
                                    $bill['base_distance'] = $request_typ->base_distance;
                                    $bill['base_price'] = $request_typ->base_price;
                                    $bill['price_per_unit_distance'] = $request_typ->price_per_unit_distance;
                                    $bill['price_per_unit_time'] = $request_typ->price_per_unit_time;
                                    if ($requestserv->base_price != 0) {
                                        $bill['distance_cost'] = ($requestserv->distance_cost);
                                        $bill['time_cost'] = (floatval(sprintf2($requestserv->time_cost, 2)));
                                    } else {
                                        /* $setbase_price = Settings::where('key', 'base_price')->first();
                                          $bill['base_price'] = ($setbase_price->value); */
                                        /* $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                          $bill['distance_cost'] = ($setdistance_price->value); */
                                        $bill['distance_cost'] = ($providertype->price_per_unit_distance);
                                        /* $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                          $bill['time_cost'] = (floatval(sprintf2($settime_price->value, 2))); */
                                        $bill['time_cost'] = (floatval(sprintf2($providertype->price_per_unit_time, 2)));
                                    }
                                    $admins = Admin::first();
                                    $bill['walker']['email'] = $walker->email;
                                    $bill['admin']['email'] = $admins->username;
                                    if ($request->transfer_amount != 0) {
                                        $bill['walker']['amount'] = ($request->total - $request->transfer_amount);
                                        $bill['admin']['amount'] = ($request->transfer_amount);
                                    } else {
                                        $bill['walker']['amount'] = ($request->transfer_amount);
                                        $bill['admin']['amount'] = ($request->total - $request->transfer_amount);
                                    }
                                    /* $bill['currency'] = $currency_selected->keyword; */
                                    $bill['currency'] = Config::get('app.generic_keywords.Currency');
                                    $bill['actual_total'] = $request->total;
                                    $bill['total'] = ($request->total);
                                    $bill['is_paid'] = $request->is_paid;
                                    $bill['promo_discount'] = $request->promo_payment;
                                    $bill['main_total'] = ($request->total);
                                    $total_amount = $request->total - $request->ledger_payment - $request->promo_payment;
                                    /*if($total_amount < 0)
                                        $total_amount = 0;*/
                                    $bill['total'] = ($total_amount);
                                    $bill['referral_bonus'] = ($request->ledger_payment);
                                    $bill['promo_bonus'] = ($request->promo_payment);
                                    $bill['payment_type'] = $request->payment_mode;
                                    $bill['is_paid'] = $request->is_paid;
                                }

                                $rservc = RequestServices::where('request_id', $request->id)->get();
                                $typs = array();
                                $typi = array();
                                $typp = array();
                                foreach ($rservc as $typ) {
                                    $typ1 = ProviderType::where('id', $typ->type)->first();
                                    $typ_price = ProviderServices::where('provider_id', $request->confirmed_walker)->where('type', $typ->type)->first();

                                    if ($typ_price->base_price > 0) {
                                        $typp1 = 0.00;
                                        $typp1 = $typ_price->base_price;
                                    } elseif ($typ_price->price_per_unit_distance > 0) {
                                        $typp1 = 0.00;
                                        foreach ($rservc as $key) {
                                            $typp1 = $typp1 + $key->distance_cost;
                                        }
                                    } else
                                        $typp1 = 0.00;

                                    $typs['name'] = $typ1->name;
                                    // $typs['icon']=$typ1->icon;
                                    $typs['price'] = $typp1;

                                    array_push($typi, $typs);
                                }
                                $bill['type'] = $typi;
                                $rserv = RequestServices::where('request_id', $request_id)->get();
                                $typs = array();
                                foreach ($rserv as $typ) {
                                    $typ1 = ProviderType::where('id', $typ->type)->first();
                                    array_push($typs, $typ1->name);
                                }
                                $settings = Settings::where('key', 'sms_when_provider_completes_job')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);
                                $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                $pattern = str_replace('%amount%', $request->total, $pattern);
                                sms_notification($request->owner_id, 'owner', $pattern);
                                $id = $request->id;
                                // send email
                                /* $settings = Settings::where('key', 'email_request_finished')->first();
                                  $pattern = $settings->value;
                                  $pattern = str_replace('%id%', $request->id, $pattern);
                                  $pattern = str_replace('%url%', web_url() . "/admin/request/map/" . $request->id, $pattern);
                                  $subject = "Request Completed";
                                  email_notification(2, 'admin', $pattern, $subject); */
                                // $settings = Settings::where('key','email_invoice_generated_user')->first();
                                // $pattern = $settings->value;
                                // $pattern = str_replace('%id%', $request->id, $pattern);
                                // $pattern = str_replace('%amount%', $request->total, $pattern);

                                $email_data = array();

                                $email_data['name'] = $owner->first_name;
                                $email_data['emailType'] = 'walker';
                                $email_data['base_price'] = $bill['base_price'];
                                $email_data['distance'] = $bill['distance'];
                                $email_data['time'] = $bill['time'];
                                $email_data['unit'] = $bill['unit'];
                                $email_data['total'] = $bill['total'];
                                $email_data['payment_mode'] = $bill['payment_mode'];
                                $email_data['actual_total'] = $request->total;
                                $email_data['is_paid'] = $request->is_paid;
                                $email_data['promo_discount'] = $request->promo_payment;

                                $request_services = RequestServices::where('request_id', $request->id)->first();

                                $locations = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id')
                                    ->get();
                                $count = round(count($locations) / 50);
                                $start = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id')
                                    ->first();
                                $end = WalkLocation::where('request_id', $request->id)
                                    ->orderBy('id', 'desc')
                                    ->first();

                                $map = "https://maps-api-ssl.google.com/maps/api/staticmap?size=249x249&style=feature:landscape|visibility:off&style=feature:poi|visibility:off&style=feature:transit|visibility:off&style=feature:road.highway|element:geometry|lightness:39&style=feature:road.local|element:geometry|gamma:1.45&style=feature:road|element:labels|gamma:1.22&style=feature:administrative|visibility:off&style=feature:administrative.locality|visibility:on&style=feature:landscape.natural|visibility:on&scale=2&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-start@2x.png|$start->latitude,$start->longitude&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-finish@2x.png|$end->latitude,$end->longitude&path=color:0x2dbae4ff|weight:4";
                                $skip = 0;
                                foreach ($locations as $location) {
                                    if ($skip == $count) {
                                        $map .= "|$location->latitude,$location->longitude";
                                        $skip = 0;
                                    }
                                    $skip++;
                                }

                                $start_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$start->latitude,$start->longitude"), TRUE);
                                $start_address = "Address not found";
                                if (isset($start_location['results'][0]['formatted_address'])) {
                                    $start_address = $start_location['results'][0]['formatted_address'];
                                }
                                $end_location = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$end->latitude,$end->longitude"), TRUE);
                                $end_address = "Address not found";
                                if (isset($end_location['results'][0]['formatted_address'])) {
                                    $end_address = $end_location['results'][0]['formatted_address'];
                                }

                                $email_data['start_location'] = $start_location;
                                $email_data['end_location'] = $end_location;

                                $walker_review = WalkerReview::where('request_id', $id)->first();
                                if ($walker_review) {
                                    $rating = round($walker_review->rating);
                                } else {
                                    $rating = 0;
                                }

                                $email_data['map'] = $map;
                                $settings = Settings::where('key', 'admin_email_address')->first();
                                $admin_email = $settings->value;
                                $requestserv = RequestServices::where('request_id', $request->id)->orderBy('id', 'DESC')->first();
                                $get_type_name = ProviderType::where('id', $requestserv->type)->first();
                                $detail = array(
                                    'admin_eamil' => $admin_email,
                                    'email_type' => 'walker',
                                    'request' => $request,
                                    'start_address' => $start_address,
                                    'end_address' => $end_address,
                                    'start' => $start,
                                    'end' => $end,
                                    'map_url' => $map,
                                    'walker' => $walker,
                                    'rating' => $rating,
                                    'base_price' => $requestserv->base_price,
                                    'price_per_time' => $request_services->time_cost,
                                    'price_per_dist' => $request_services->distance_cost,
                                    'ref_bonus' => $request->ledger_payment,
                                    'promo_bonus' => $request->promo_payment,
                                    'dist_cost' => $requestserv->distance_cost,
                                    'time_cost' => $requestserv->time_cost,
                                    'type_name' => ucwords($get_type_name->name)
                                );
                                //send email to owner
                                /* $subject = "Invoice Generated";
                                  send_email($request->owner_id, 'owner', $email_data, $subject, 'invoice'); */

                                //$subject = "On Demand Invoice Generated";
                                //$email_data['emailType'] = 'walker';
                                //email_notification($request->confirmed_walker, 'walker', $detail, $subject, 'invoice');
                            } else {
                                Requests::where('id', '=', $request_id)->update(array('is_invoice_verified_cap' => -1));
                                $response_array = array(
                                    'success' => true,
                                    'message' => 'Captain did not confirmed invoice.',
                                    'request_id' => $request_id
                                );
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Owner Id not match to service id', 'error_code' => 407);
                        }

                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    //Payment before starting
    public function pre_payment()
    {
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $time = Input::get('time');

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'time' => $time,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'time' => 'required',
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {
                                $request_service = RequestServices::find($request_id);
                                $request_typ = ProviderType::where('id', '=', $request_service->type)->first();

                                if (!$walker_data->type) {
                                    /* $settings = Settings::where('key', 'price_per_unit_distance')->first();
                                      $price_per_unit_distance = $settings->value;
                                      $settings = Settings::where('key', 'price_per_unit_time')->first();
                                      $price_per_unit_time = $settings->value;
                                      $settings = Settings::where('key', 'base_price')->first();
                                      $base_price = $settings->value; */
                                    $price_per_unit_distance = $request_typ->price_per_unit_distance;
                                    $price_per_unit_time = $request_typ->price_per_unit_time;
                                    $base_price = $request_typ->base_price;
                                } else {
                                    $provider_type = ProviderServices::find($walker_data->type);
                                    $base_price = $provider_type->base_price;
                                    $price_per_unit_distance = $provider_type->price_per_unit_distance;
                                    $price_per_unit_time = $provider_type->price_per_unit_time;
                                }

                                $settings = Settings::where('key', 'default_charging_method_for_users')->first();
                                $pricing_type = $settings->value;
                                $settings = Settings::where('key', 'default_distance_unit')->first();
                                $unit = $settings->value;
                                if ($pricing_type == 1) {
                                    $distance_cost = $price_per_unit_distance;
                                    $time_cost = $price_per_unit_time;
                                    $total = $base_price + $distance_cost + $time_cost;
                                } else {
                                    $distance_cost = 0;
                                    $time_cost = 0;
                                    $total = $base_price;
                                }

                                Log::info('req');
                                $request_service = RequestServices::find($request_id);
                                $request_service->base_price = $base_price;
                                $request_service->distance_cost = $distance_cost;
                                $request_service->time_cost = $time_cost;
                                $request_service->total = $total;
                                $request_service->save();
                                $request->distance = $distance_cost;
                                $request->time = $time_cost;
                                $request->total = $total;

                                Log::info('in ');

                                // charge client
                                $ledger = Ledger::where('owner_id', $request->owner_id)->first();

                                if ($ledger) {
                                    $balance = $ledger->amount_earned - $ledger->amount_spent;
                                    if ($balance > 0) {
                                        if ($total > $balance) {
                                            $ledger_temp = Ledger::find($ledger->id);
                                            $ledger_temp->amount_spent = $ledger_temp->amount_spent + $balance;
                                            $ledger_temp->save();
                                            $total = $total - $balance;
                                        } else {
                                            $ledger_temp = Ledger::find($ledger->id);
                                            $ledger_temp->amount_spent = $ledger_temp->amount_spent + $total;
                                            $ledger_temp->save();
                                            $total = 0;
                                        }
                                    }
                                }

                                Log::info('out');
                                if ($total == 0) {
                                    $request->is_paid = 1;
                                } else {

                                    $payment_data = Payment::where('owner_id', $request->owner_id)->where('is_default', 1)->first();
                                    if (!$payment_data)
                                        $payment_data = Payment::where('owner_id', $request->owner_id)->first();

                                    if ($payment_data) {
                                        $customer_id = $payment_data->customer_id;
                                        try {
                                            if (Config::get('app.default_payment') == 'stripe') {
                                                Stripe::setApiKey(Config::get('app.stripe_secret_key'));

                                                try {
                                                    Stripe_Charge::create(array(
                                                            "amount" => floor($total) * 100,
                                                            "currency" => "usd",
                                                            "customer" => $customer_id)
                                                    );
                                                } catch (Stripe_InvalidRequestError $e) {
                                                    // Invalid parameters were supplied to Stripe's API
                                                    $ownr = Owner::find($request->owner_id);
                                                    $ownr->debt = $total;
                                                    $ownr->save();
                                                    $response_array = array('error' => $e->getMessage());
                                                    $response_code = 200;
                                                    $response = Response::json($response_array, $response_code);
                                                    return $response;
                                                }
                                                $request->is_paid = 1;

                                                $setting = Settings::where('key', 'paypal')->first();
                                                $settng1 = Settings::where('key', 'service_fee')->first();
                                                if ($setting->value == 2 && $walker_data->merchant_id != NULL) {
                                                    // dd($amount$request->transfer_amount);
                                                    $transfer = Stripe_Transfer::create(array(
                                                            "amount" => ($total - $settng1->value) * 100, // amount in cents
                                                            "currency" => "usd",
                                                            "recipient" => $walker_data->merchant_id)
                                                    );
                                                }
                                            } else {
                                                $amount = $total;
                                                Braintree_Configuration::environment(Config::get('app.braintree_environment'));
                                                Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
                                                Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
                                                Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
                                                $card_id = $payment_data->card_token;
                                                $setting = Settings::where('key', 'paypal')->first();
                                                $settng1 = Settings::where('key', 'service_fee')->first();
                                                if ($setting->value == 2 && $walker_data->merchant_id != NULL) {
                                                    // escrow
                                                    $result = Braintree_Transaction::sale(array(
                                                        'amount' => $amount,
                                                        'paymentMethodToken' => $card_id
                                                    ));
                                                } else {
                                                    $result = Braintree_Transaction::sale(array(
                                                        'amount' => $amount,
                                                        'paymentMethodToken' => $card_id
                                                    ));
                                                }
                                                Log::info('result = ' . print_r($result, true));
                                                if ($result->success) {
                                                    $request->is_paid = 1;
                                                } else {
                                                    $request->is_paid = 0;
                                                }
                                            }
                                        } catch (Exception $e) {
                                            $response_array = array('success' => false, 'error' => $e, 'error_code' => 405);
                                            $response_code = 200;
                                            $response = Response::json($response_array, $response_code);
                                            return $response;
                                        }
                                    }
                                }

                                $request->card_payment = $total;
                                $request->ledger_payment = $request->total - $total;

                                $request->save();
                                Log::info('Request = ' . print_r($request, true));

                                if ($request->is_paid == 1) {
                                    $owner = Owner::find($request->owner_id);
                                    $settings = Settings::where('key', 'sms_payment_generated')->first();
                                    $pattern = $settings->value;
                                    $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                    $pattern = str_replace('%id%', $request->id, $pattern);
                                    $pattern = str_replace('%user_mobile%', $owner->phone, $pattern);
                                    sms_notification(1, 'admin', $pattern);
                                }

                                $walker = Walker::find($walker_id);
                                $walker->is_available = 1;
                                $walker->save();

                                // Send Notification
                                $walker = Walker::find($request->confirmed_walker);
                                $walker_data = array();
                                $walker_data['first_name'] = $walker->first_name;
                                $walker_data['last_name'] = $walker->last_name;
                                $walker_data['phone'] = $walker->phone;
                                $walker_data['bio'] = $walker->bio;
                                $walker_data['picture'] = $walker->picture;
                                $walker_data['type'] = $walker->type;
                                $walker_data['rating'] = $walker->rate;
                                $walker_data['num_rating'] = $walker->rate_count;
                                $walker_data['car_model'] = $walker->car_model;
                                $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                  $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                $settings = Settings::where('key', 'default_distance_unit')->first();
                                $unit = $settings->value;
                                if ($unit == 0) {
                                    $unit_set = 'kms';
                                } elseif ($unit == 1) {
                                    $unit_set = 'miles';
                                }
                                $bill = array();
                                if ($request->is_paid == 1) {
                                    $bill['distance'] = (string)convert($request->distance, $unit);
                                    $bill['unit'] = $unit_set;
                                    $bill['time'] = $request->time;
                                    $bill['base_price'] = ($base_price);
                                    $bill['distance_cost'] = ($distance_cost);
                                    $bill['time_cost'] = ($time_cost);
                                    $bill['total'] = ($request->total);
                                    $bill['is_paid'] = $request->is_paid;
                                }

                                $response_array = array(
                                    'success' => true,
                                    'request_id' => $request_id,
                                    'status' => $request->status,
                                    'confirmed_walker' => $request->confirmed_walker,
                                    'walker' => $walker_data,
                                    'bill' => $bill,
                                );
                                $title = "Payment Has Made";

                                $message = $response_array;

                                send_notifications($walker->id, "walker", $title, $message);


                                $settings = Settings::where('key', 'email_notification')->first();
                                $condition = $settings->value;
                                if ($condition == 1) {
                                    /* $settings = Settings::where('key', 'payment_made_client')->first();
                                      $pattern = $settings->value;

                                      $pattern = str_replace('%id%', $request->id, $pattern);
                                      $pattern = str_replace('%amount%', $request->total, $pattern);

                                      $subject = "Payment Charged";
                                      email_notification($walker->id, 'walker', $pattern, $subject); */
                                    $settings = Settings::where('key', 'admin_email_address')->first();
                                    $admin_email = $settings->value;
                                    $pattern = array('admin_eamil' => $admin_email, 'name' => ucwords($walker->first_name . " " . $walker->last_name), 'amount' => $total, 'req_id' => $request_id, 'web_url' => web_url());
                                    $subject = "Payment Done With " . $request_id . "";
                                    email_notification($walker->id, 'walker', $pattern, $subject, 'pre_payment', null);
                                }

                                // Send SMS
                                $owner = Owner::find($request->owner_id);
                                $settings = Settings::where('key', 'sms_when_provider_completes_job')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);
                                $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                $pattern = str_replace('%amount%', $request->total, $pattern);
                                sms_notification($request->owner_id, 'owner', $pattern);

                                $email_data = array();

                                $email_data['name'] = $owner->first_name;
                                $email_data['emailType'] = 'user';
                                $email_data['base_price'] = $bill['base_price'];
                                $email_data['distance'] = $bill['distance'];
                                $email_data['time'] = $bill['time'];
                                $email_data['unit'] = $bill['unit'];
                                $email_data['total'] = $bill['total'];

                                if ($bill['payment_mode']) {
                                    $email_data['payment_mode'] = $bill['payment_mode'];
                                } else {
                                    $email_data['payment_mode'] = '---';
                                }

                                /* $subject = "Invoice Generated";
                                  send_email($request->owner_id, 'owner', $email_data, $subject, 'invoice');

                                  $subject = "Invoice Generated";
                                  $email_data['emailType'] = 'walker';
                                  send_email($request->confirmed_walker, 'walker', $email_data, $subject, 'invoice');
                                 */
                                if ($request->is_paid == 1) {
                                    // send email
                                    /* $settings = Settings::where('key', 'email_payment_charged')->first();
                                      $pattern = $settings->value;

                                      $pattern = str_replace('%id%', $request->id, $pattern);
                                      $pattern = str_replace('%url%', web_url() . "/admin/request/" . $request->id, $pattern);

                                      $subject = "Payment Charged";
                                      email_notification(1, 'admin', $pattern, $subject); */
                                    $settings = Settings::where('key', 'admin_email_address')->first();
                                    $admin_email = $settings->value;
                                    $pattern = array('admin_eamil' => $admin_email, 'name' => 'Administrator', 'amount' => $total, 'req_id' => $request_id, 'web_url' => web_url());
                                    $subject = "Payment Done With " . $request_id . "";
                                    email_notification(1, 'admin', $pattern, $subject, 'pay_charged', null);
                                }

                                $response_array = array(
                                    'success' => true,
                                    'base_fare' => ($base_price),
                                    'distance_cost' => ($distance_cost),
                                    'time_cost' => ($time_cost),
                                    'total' => ($total),
                                    'is_paid' => $request->is_paid,
                                );
                                $response_code = 200;
                            } else {
                                /* $var = Keywords::where('id', 1)->first();
                                  $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array('success' => false, 'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    //process walk location
    public function walk_location_queue()
    {
        $currentLatitude = null;
        $currentLongitude = null;
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $locations = Input::get('locations');

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'locations' => $locations,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'locations' => 'required',
                )
            );
            Log::info("walker location queue on click on trip");
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $unit = $unit_set = -1;
                $settings = Settings::where('key', 'default_distance_unit')->first();
                $unit = $settings->value;
                if ($unit == 0) {
                    $unit_set = 'kms';
                } elseif ($unit == 1) {
                    $unit_set = 'miles';
                }
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->is_started == 1) {
                                    $locationsarr = json_decode($locations, true);
                                    $walk_location_last = WalkLocation::where('request_id', $request_id)->orderBy('created_at', 'desc')->first();
                                    $lastest_walk_location = $walk_location_last;
                                    $numoflocations = sizeof($locationsarr);
                                    if ($numoflocations == 0) {
                                        $response_array = array(
                                            'success' => false,
                                            'error' => 'Zero location received.',
                                            'locations' => $locations
                                        );
                                        $response_code = 200;
                                        $response = Response::json($response_array, $response_code);
                                        return $response;
                                    }
                                    for ($i = 0; $i < $numoflocations; $i++) {
                                        if ($request->is_completed != 1) {

                                            $walk_location = new WalkLocation;
                                            $walk_location->request_id = $request_id;
                                            $walk_location->latitude = $locationsarr[$i]['latitude'];
                                            $walk_location->longitude = $locationsarr[$i]['longitude'];
                                            $walk_location->distance = $locationsarr[$i]['distance'];
                                            $walk_location->bearing = $locationsarr[$i]['bearing'];
                                            $walk_location->created_at = $locationsarr[$i]['timestamp'];
                                            $walk_location->save();
                                            $lastest_walk_location = $walk_location;
                                        }

                                    }
                                    if (!$walk_location_last) {
                                        $walk_location = new WalkLocation;
                                        $walk_location->request_id = $request_id;
                                        $walk_location->latitude = $locationsarr[0]['latitude'];
                                        $walk_location->longitude = $locationsarr[0]['longitude'];
                                        $walk_location->distance = $locationsarr[0]['distance'];
                                        $walk_location->bearing = $locationsarr[0]['bearing'];
                                        $walk_location->created_at = $locationsarr[0]['timestamp'];
                                        $walk_location_last = $walk_location;
                                    }
                                    /*$distance_old = $walk_location_last->distance;
                                        $distance_new = distanceGeoPoints($walk_location_last->latitude, $walk_location_last->longitude, $lastest_walk_location->latitude, $lastest_walk_location->longitude);
                                        $distance = $distance_old + $distance_new;
                                        $settings = Settings::where('key', 'default_distance_unit')->first();
                                        $unit = $settings->value;
                                        if ($unit == 0) {
                                            $unit_set = 'kms';
                                        } elseif ($unit == 1) {
                                            $unit_set = 'miles';
                                        }
                                        $distancecon = convert($distance, $unit);*/
                                    $time1 = strtotime($lastest_walk_location->created_at);
                                    $time2 = strtotime($walk_location_last->created_at);
                                    $difference = ($time1 - $time2) / 60;
                                    $request->time = $request->time + $difference;
                                    $request->save();
                                    //updating walker location
                                    $walker = Walker::find($walker_id);

                                    $location = get_location($lastest_walk_location->latitude, $lastest_walk_location->longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker->latitude, $walker->longitude, $latitude, $longitude);
                                    }
                                    $walker->old_latitude = $walker->latitude;
                                    $walker->old_longitude = $walker->longitude;
                                    $walker->latitude = $latitude;
                                    $walker->longitude = $longitude;
                                    $walker->bearing = $angle;
                                    $walker->heart_beat = date("Y-m-d H:i:s");
                                    $walker->is_active = 1;
                                    $walker->save();
                                    /*try {
                                        $walker = Walker::find($walker_id);
                                        $vehicleReferenceNumber = strval($walker->vehicleReferenceNumber);
                                        $client = new Client();
                                        $response = $client->post("https://wasl.elm.sa/WaslPortalWeb/rest/LocationRegistration/send",
                                            ['json' => ['apiKey' => '24609908-5A75-409B-A9A1-B801128E5878',
                                                'vehicleReferenceNumber' => $vehicleReferenceNumber,
                                                'currentLatitude' => $walker->latitude,
                                                'currentLongitude' => $walker->longitude,
                                                'hasCustomer' => '1']]);
                                        $resobj = $response->getBody(true);
                                    } catch (Exception $e) {
                                        Log::info("WASL Location API catch" . $e->getMessage());
                                    }*/
                                    $response_array = array(
                                        'success' => true,
                                        'message' => 'Locations save successfully.',
                                        'locations' => $locations
                                    );
                                    $response_code = 200;
                                } else {
                                    $response_array = array(
                                        'success' => false,
                                        'error' => 'Trip not started yet.',
                                        'error_code' => 415
                                    );
                                    $response_code = 200;
                                }

                            } else {
                                /* $var = Keywords::where('id', 1)->first();
                                  $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array(
                                    'success' => false,
                                    'dest_latitude' => $request->D_latitude,
                                    'dest_longitude' => $request->D_longitude,
                                    'payment_type' => $request->payment_mode,
                                    'is_cancelled' => $request->is_cancelled,
                                    'unit' => $unit_set,
                                    'error' => 'Request ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID',
                                    'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Add Location Data
    public function walk_location()
    {
        $currentLatitude = null;
        $currentLongitude = null;
        if (Request::isMethod('post')) {
            $request_id = Input::get('request_id');
            $token = Input::get('token');
            $walker_id = Input::get('id');
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            if (Input::has('bearing')) {
                $angle = Input::get('bearing');
            }
            /*https://wasl.elm.sa/WaslPortalWeb/rest/LocationRegistration/send d
            '{"apiKey":" 246099085A75409BA9A1B801128E5878
            ",
            "vehicleReferenceNumber": "6703",
            "currentLatitude":"24.723437",
            "currentLongitude":"46.117452",
            "hasCustomer":"0"}'*/
            $hasCustomer = "0";
            $currentLatitude = strval($latitude);
            $currentLongitude = strval($longitude);

            $validator = Validator::make(
                array(
                    'request_id' => $request_id,
                    'token' => $token,
                    'walker_id' => $walker_id,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ), array(
                    'request_id' => 'required|integer',
                    'token' => 'required',
                    'walker_id' => 'required|integer',
                    'latitude' => 'required',
                    'longitude' => 'required',
                )
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                Log::info('walk_locationlng' . $latitude . 'lng' . $longitude);
                $unit = $unit_set = -1;
                $settings = Settings::where('key', 'default_distance_unit')->first();
                $unit = $settings->value;
                if ($unit == 0) {
                    $unit_set = 'kms';
                } elseif ($unit == 1) {
                    $unit_set = 'miles';
                }
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    // check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {
                        // Do necessary operations
                        if ($request = Requests::find($request_id)) {
                            if ($request->confirmed_walker == $walker_id) {

                                if ($request->is_started == 1) {
                                    $hasCustomer = "1";

                                    $walk_location_last = WalkLocation::where('request_id', $request_id)->orderBy('created_at', 'desc')->first();

                                    if ($walk_location_last) {
                                        $distance_old = $walk_location_last->distance;
                                        $distance_new = distanceGeoPoints($walk_location_last->latitude, $walk_location_last->longitude, $latitude, $longitude);
                                        $distance = $distance_old + $distance_new;
                                        $settings = Settings::where('key', 'default_distance_unit')->first();
                                        $unit = $settings->value;
                                        if ($unit == 0) {
                                            $unit_set = 'kms';
                                        } elseif ($unit == 1) {
                                            $unit_set = 'miles';
                                        }
                                        $distancecon = convert($distance, $unit);
                                    } else {
                                        $distance = 0;
                                    }

                                    $walker = Walker::find($walker_id);

                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker->latitude, $walker->longitude, $latitude, $longitude);
                                    }
                                    $walker->old_latitude = $walker->latitude;
                                    $walker->old_longitude = $walker->longitude;
                                    $walker->latitude = $latitude;
                                    $walker->longitude = $longitude;
                                    $walker->bearing = $angle;
                                    $walker->heart_beat = date("Y-m-d H:i:s");
                                    $walker->is_active = 1;
                                    $walker->save();

                                    /* GET SECOND LAST ENTY FOR TIME */
                                    $loc1 = WalkLocation::where('request_id', $request->id)->orderBy('id', 'desc')->first();
                                    /* GET SECOND LAST ENTY FOR TIME END */
                                    if ($request->is_completed != 1) {
                                        $walk_location = new WalkLocation;
                                        $walk_location->request_id = $request_id;
                                        $walk_location->latitude = $latitude;
                                        $walk_location->longitude = $longitude;
                                        $walk_location->distance = $distance;
                                        $walk_location->bearing = $angle;
                                        $walk_location->save();
                                    }
                                    $one_minut_old_time = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")) - 60);
                                    /* $loc1 = WalkLocation::where('request_id', $request->id)->first(); */
                                    /* print $loc1; */
                                    $loc2 = WalkLocation::where('request_id', $request->id)->orderBy('id', 'desc')->first();
                                    if ($loc1) {
                                        $time1 = strtotime($loc2->created_at);
                                        $time2 = strtotime($loc1->created_at);
                                        /* echo $difference = intval(($time1 - $time2) / 60); */
                                        $difference = ($time1 - $time2) / 60;
                                        $loc1min = WalkLocation::where('request_id', $request->id)->where('created_at', '<=', $one_minut_old_time)->orderBy('id', 'desc')->first();
                                        Log::info($loc1min);
                                        if (isset($loc1min)) {
                                            $distence = distanceGeoPoints($loc1min->latitude, $loc1min->longitude, $latitude, $longitude);
                                            if ($request->is_completed != 1) {
                                                if ($distence <= 50) {
                                                    $request->time = $request->time + $difference;
                                                } else {
                                                    $request->time = $request->time;
                                                }
                                            }
                                        } else
                                            $request->time = $request->time;
                                    } else {
                                        $request->time = 0;
                                    }
                                    $request->save();

                                    $response_array = array(
                                        'success' => true,
                                        'dest_latitude' => $request->D_latitude,
                                        'dest_longitude' => $request->D_longitude,
                                        'payment_type' => $request->payment_mode,
                                        'is_cancelled' => $request->is_cancelled,
                                        'distance' => $distancecon,
                                        'unit' => $unit_set,
                                        'time' => $difference,
                                    );
                                    $response_code = 200;
                                } else {
                                    $walker = Walker::find($walker_id);

                                    $location = get_location($latitude, $longitude);
                                    $latitude = $location['lat'];
                                    $longitude = $location['long'];
                                    if (!isset($angle)) {
                                        $angle = get_angle($walker->latitude, $walker->longitude, $latitude, $longitude);
                                    }
                                    $walker->old_latitude = $walker->latitude;
                                    $walker->old_longitude = $walker->longitude;
                                    $walker->latitude = $latitude;
                                    $walker->longitude = $longitude;
                                    $walker->bearing = $angle;
                                    $walker->heart_beat = date("Y-m-d H:i:s");
                                    $walker->is_active = 1;
                                    $walker->save();
                                    $response_array = array(
                                        'success' => false,
                                        'dest_latitude' => $request->D_latitude,
                                        'dest_longitude' => $request->D_longitude,
                                        'payment_type' => $request->payment_mode,
                                        'is_cancelled' => $request->is_cancelled,
                                        'unit' => $unit_set,
                                        'error' => 'Service not yet started',
                                        'error_code' => 414,
                                    );
                                    $response_code = 200;
                                }
                            } else {
                                /* $var = Keywords::where('id', 1)->first();
                                  $response_array = array('success' => false, 'error' => 'Request ID doesnot matches with ' . $var->keyword . ' ID', 'error_code' => 407); */
                                $response_array = array(
                                    'success' => false,
                                    'dest_latitude' => $request->D_latitude,
                                    'dest_longitude' => $request->D_longitude,
                                    'payment_type' => $request->payment_mode,
                                    'is_cancelled' => $request->is_cancelled,
                                    'unit' => $unit_set,
                                    'error' => 'Service ID doesnot matches with ' . Config::get('app.generic_keywords.Provider') . ' ID',
                                    'error_code' => 407);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Service ID Not Found', 'error_code' => 408);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        /* $var = Keywords::where('id', 1)->first();
                          $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        try {

            $walker = Walker::find($walker_id);
            $vehicleReferenceNumber = strval($walker->vehicleReferenceNumber);
            $client = new Client();
            $response = $client->post("https://wasl.elm.sa/WaslPortalWeb/rest/LocationRegistration/send",
                ['json' => ['apiKey' => '24609908-5A75-409B-A9A1-B801128E5878',
                    'vehicleReferenceNumber' => $vehicleReferenceNumber,
                    'currentLatitude' => $currentLatitude,
                    'currentLongitude' => $currentLongitude,
                    'hasCustomer' => $hasCustomer]]);
            $resobj = $response->getBody(true);
        } catch (Exception $e) {
            Log::info("WASL Location API catch" . $e->getMessage());
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Add Location Data
    public function check_state()
    {

        $walker_id = Input::get('id');
        $token = Input::get('token');

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    $response_array = array('success' => true, 'is_active' => $walker_data->is_active, 'is_active_monthly' => $walker_data->is_active_monthly);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Add Location Data
    public function toggle_state()
    {

        $walker_id = Input::get('id');
        $token = Input::get('token');
        $is_active = Input::get('is_active');
        $flagisact_user = Input::get('flagisact_user');


        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
                'is_active' => $is_active,
                'flagisact_user' => $flagisact_user
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
                'is_active' => 'required|integer',
                'flagisact_user' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    Log::info("Captain toggle input " . $walker_id . "state" . $is_active . date("Y-m-d H:i:s"));
                    $walker = Walker::find($walker_id);
                    $walker->is_active = $is_active;
                    $walker->flagisact_user = $flagisact_user;
                    if ($is_active == 1 && $walker->is_available == 0) {
                        $request = Requests::where('status', '=', 1)->where('is_cancelled', '=', 0)->where('service_type', '!=', 2)->where('is_dog_rated', '=', 0)->where('confirmed_walker', '=', $walker_id)->first();
                        if ($request) {
                            $request_id = $request->id;
                        } else {
                            $walker->is_available = 1;
                        }
                    }
                    if ($is_active == 1) {
                        $onlinehours = OnlineHours::where('walker_id', $walker_id)->where('status', 0)->orderBy('id', 'desc')->first();
                        if ($onlinehours) {
                            $difference = 0;
                            $time1 = strtotime($onlinehours->online);
                            $time2 = time();
                            $difference = floatval(($time2 - $time1) / 3600);
                            $onlinehours = OnlineHours::find($onlinehours->id);
                            if (isset($onlinehours)) {
                                $onlinehours->hours = $difference;
                                $date = new DateTime();
                                $date->setTimestamp(time());
                                $dt = $date->format('Y-m-d H:i:s');
                                $onlinehours->offline = $dt;
                                $onlinehours->status = 1;
                                $onlinehours->save();
                                $walker->total_online_hours = $walker->total_online_hours + $difference;
                            }
                        }
                        $onlinehours = new OnlineHours();
                        $onlinehours->walker_id = $walker_id;
                        $onlinehours->save();
                    } else {
                        $onlinehours = OnlineHours::where('walker_id', $walker_id)->where('status', 0)->orderBy('id', 'desc')->first();
                        if ($onlinehours) {
                            $time1 = strtotime($onlinehours->online);
                            $time2 = time();
                            $difference = floatval(($time2 - $time1) / 3600);
                            $onlinehours = OnlineHours::find($onlinehours->id);
                            if (isset($onlinehours)) {
                                $onlinehours->hours = $difference;
                                $date = new DateTime();
                                $date->setTimestamp(time());
                                $dt = $date->format('Y-m-d H:i:s');
                                $onlinehours->offline = $dt;
                                $onlinehours->status = 1;
                                $onlinehours->save();
                                $walker->total_online_hours = $walker->total_online_hours + $difference;
                            }
                        } else {
                            $difference = 0;
                        }
                    }
                    $walker->save();
                    $walker = Walker::find($walker_id);
                    $response_array = array('success' => true, 'is_active' => $walker->is_active);
                    Log::info("Captain new state is  " . $walker_id . "state" . $walker->is_active);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Add Location Data
    public function toggle_state_monthly()
    {

        $walker_id = Input::get('id');
        $token = Input::get('token');
        $is_active_monthly = Input::get('is_active_monthly');
        $flagisactmonthly_user = Input::get('flagisactmonthly_user');

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
                'is_active_monthly' => $is_active_monthly,
                'flagisactmonthly_user' => $flagisactmonthly_user
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
                'is_active_monthly' => 'required|integer',
                'flagisactmonthly_user' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $walker = Walker::find($walker_id);
                    $walker->is_active_monthly = $is_active_monthly;
                    $walker->flagisactmonthly_user = $flagisactmonthly_user;
                    $walker->save();
                    $response_array = array('success' => true, 'is_active_monthly' => $walker->is_active_monthly);
                    Log::info("Captain is active toggle. " . $walker_id . "state" . $walker->is_active_monthly);
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Update Profile

    public function update_profile()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $phone = Input::get('phone');
        $password = Input::get('password');
        $new_password = Input::get('new_password');
        $old_password = Input::get('old_password');
        $picture = Input::file('picture');
        $bio = Input::get('bio');
        $address = Input::get('address');
        $state = Input::get('state');
        $country = Input::get('country');
        $zipcode = Input::get('zipcode');
        $car_model = $car_number = "";
        if (Input::has('car_model')) {
            $car_model = trim(Input::get('car_model'));
        }
        if (Input::has('car_number')) {
            $car_number = trim(Input::get('car_number'));
        }

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'picture' => $picture,
                'zipcode' => $zipcode
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'picture' => 'mimes:jpeg,bmp,png',
                'zipcode' => 'integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    if ($new_password != "" || $new_password != NULL) {
                        if ($old_password != "" || $old_password != NULL) {
                            if (Hash::check($old_password, $walker_data->password)) {

                                $walker = Walker::find($walker_id);
                                if ($first_name) {
                                    $walker->first_name = $first_name;
                                }
                                if ($last_name) {
                                    $walker->last_name = $last_name;
                                }
                                if ($phone) {
                                    $walker->phone = $phone;
                                }
                                if ($bio) {
                                    $walker->bio = $bio;
                                }
                                if ($address) {
                                    $walker->address = $address;
                                }
                                if ($state) {
                                    $walker->state = $state;
                                }
                                if ($country) {
                                    $walker->country = $country;
                                }
                                if ($zipcode) {
                                    $walker->zipcode = $zipcode;
                                }
                                if ($new_password) {
                                    $walker->password = Hash::make($new_password);
                                }
                                if ($car_model != "") {
                                    $walker->car_model;
                                }
                                if ($car_number != "") {
                                    $walker->car_number;
                                }

                                if (Input::hasFile('picture')) {
                                    if ($walker->picture != "") {
                                        $path = $walker->picture;
                                        Log::info($path);
                                        $filename = basename($path);
                                        Log::info($filename);
                                        if (file_exists($path)) {
                                            unlink(public_path() . "/uploads/" . $filename);
                                        }
                                    }
                                    // upload image
                                    $file_name = time();
                                    $file_name .= rand();
                                    $file_name = sha1($file_name);

                                    $ext = Input::file('picture')->getClientOriginalExtension();
                                    Log::info('ext = ' . print_r($ext, true));
                                    Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                                    $local_url = $file_name . "." . $ext;

                                    // Upload to S3

                                    if (Config::get('app.s3_bucket') != "") {
                                        $s3 = App::make('aws')->get('s3');
                                        $pic = $s3->putObject(array(
                                            'Bucket' => Config::get('app.s3_bucket'),
                                            'Key' => $file_name,
                                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                                        ));

                                        $s3->putObjectAcl(array(
                                            'Bucket' => Config::get('app.s3_bucket'),
                                            'Key' => $file_name,
                                            'ACL' => 'public-read'
                                        ));

                                        $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                                    } else {
                                        $s3_url = asset_url() . '/uploads/' . $local_url;
                                    }
                                    try {
                                        if (isset($walker->picture)) {
                                            if ($walker->picture != "") {
                                                $icon = $walker->picture;
                                                unlink_image($icon);
                                            }
                                        }
                                    } catch (Exception $e) {
                                        Log::info($e->getMessage());
                                    }
                                    $walker->picture = $s3_url;
                                }
                                If (Input::has('timezone')) {
                                    $walker->timezone = Input::get('timezone');
                                }

                                $walker->save();

                                $response_array = array(
                                    'success' => true,
                                    'id' => $walker->id,
                                    'first_name' => $walker->first_name,
                                    'last_name' => $walker->last_name,
                                    'phone' => $walker->phone,
                                    'email' => $walker->email,
                                    'picture' => $walker->picture,
                                    'bio' => $walker->bio,
                                    'address' => $walker->address,
                                    'state' => $walker->state,
                                    'country' => $walker->country,
                                    'zipcode' => $walker->zipcode,
                                    'login_by' => $walker->login_by,
                                    'social_unique_id' => $walker->social_unique_id,
                                    'device_token' => $walker->device_token,
                                    'device_type' => $walker->device_type,
                                    'token' => $walker->token,
                                    'timezone' => $walker->timezone,
                                    'type' => $walker->type,
                                    'car_model' => $walker->car_model,
                                    'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                                );
                                $response_code = 200;
                            } else {
                                $response_array = array('success' => false, 'error' => 'Invalid Old Password', 'error_code' => 501);
                                $response_code = 200;
                            }
                        } else {
                            $response_array = array('success' => false, 'error' => 'Old Password must not be blank', 'error_code' => 502);
                            $response_code = 200;
                        }
                    } else {

                        $walker = Walker::find($walker_id);
                        if ($first_name) {
                            $walker->first_name = $first_name;
                        }
                        if ($last_name) {
                            $walker->last_name = $last_name;
                        }
                        if ($phone) {
                            $walker->phone = $phone;
                        }
                        if ($bio) {
                            $walker->bio = $bio;
                        }
                        if ($address) {
                            $walker->address = $address;
                        }
                        if ($state) {
                            $walker->state = $state;
                        }
                        if ($country) {
                            $walker->country = $country;
                        }
                        if ($zipcode) {
                            $walker->zipcode = $zipcode;
                        }
                        if ($car_model != "") {
                            $walker->car_model;
                        }
                        if ($car_number != "") {
                            $walker->car_number;
                        }

                        if (Input::hasFile('picture')) {
                            if ($walker->picture != "") {
                                $path = $walker->picture;
                                Log::info($path);
                                $filename = basename($path);
                                Log::info($filename);
                                if (file_exists($path)) {
                                    unlink(public_path() . "/uploads/" . $filename);
                                }
                            }
                            // upload image
                            $file_name = time();
                            $file_name .= rand();
                            $file_name = sha1($file_name);

                            $ext = Input::file('picture')->getClientOriginalExtension();
                            Log::info('ext = ' . print_r($ext, true));
                            Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                            $local_url = $file_name . "." . $ext;

                            // Upload to S3

                            if (Config::get('app.s3_bucket') != "") {
                                $s3 = App::make('aws')->get('s3');
                                $pic = $s3->putObject(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'SourceFile' => public_path() . "/uploads/" . $local_url,
                                ));

                                $s3->putObjectAcl(array(
                                    'Bucket' => Config::get('app.s3_bucket'),
                                    'Key' => $file_name,
                                    'ACL' => 'public-read'
                                ));

                                $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                            } else {
                                $s3_url = asset_url() . '/uploads/' . $local_url;
                            }

                            if (isset($walker->picture)) {
                                if ($walker->picture != "") {
                                    $icon = $walker->picture;
                                    unlink_image($icon);
                                }
                            }

                            $walker->picture = $s3_url;
                        }
                        If (Input::has('timezone')) {
                            $walker->timezone = Input::get('timezone');
                        }

                        $walker->save();

                        $response_array = array(
                            'success' => true,
                            'id' => $walker->id,
                            'first_name' => $walker->first_name,
                            'last_name' => $walker->last_name,
                            'phone' => $walker->phone,
                            'email' => $walker->email,
                            'picture' => $walker->picture,
                            'bio' => $walker->bio,
                            'address' => $walker->address,
                            'state' => $walker->state,
                            'country' => $walker->country,
                            'zipcode' => $walker->zipcode,
                            'login_by' => $walker->login_by,
                            'social_unique_id' => $walker->social_unique_id,
                            'device_token' => $walker->device_token,
                            'device_type' => $walker->device_type,
                            'token' => $walker->token,
                            'timezone' => $walker->timezone,
                            'type' => $walker->type,
                            'car_model' => $walker->car_model,
                            'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                        );
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_completed_requests()
    {
        $walker_id = Input::get('id');
        $token = Input::get('token');
        $from = Input::get('from_date'); // 2015-03-11 07:45:01
        $to_date = Input::get('to_date'); //2015-03-11 07:45:01

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    if ($from != "" && $to_date != "") {
                        $request_data = DB::table('request')
                            ->where('request.confirmed_walker', $walker_id)
                            ->where('request.is_completed', 1)
                            ->where('request.monthly_id', 0)
                            ->where('request_start_time', '>', $from)
                            ->where('request_start_time', '<', $to_date)
                            ->leftJoin('owner', 'request.owner_id', '=', 'owner.id')
                            ->leftJoin('request_services', 'request_services.request_id', '=', 'request.id')
                            ->select('request.*', 'request.request_start_time', 'request.transfer_amount', 'owner.first_name', 'owner.last_name', 'owner.phone', 'owner.email', 'owner.picture', 'request.distance', 'request.time', 'request.promo_code', 'request_services.base_price', 'request_services.distance_cost', 'request_services.time_cost', 'request.total')
                            ->groupBy('request.id')
                            ->get();
                    } else {
                        $request_data = DB::table('request')
                            ->where('request.confirmed_walker', $walker_id)
                            ->where('request.is_completed', 1)
                            ->where('request.monthly_id', 0)
                            ->leftJoin('owner', 'request.owner_id', '=', 'owner.id')
                            ->leftJoin('request_services', 'request_services.request_id', '=', 'request.id')
                            ->select('request.*', 'request.request_start_time', 'request.transfer_amount', 'owner.first_name', 'owner.last_name', 'owner.phone', 'owner.email', 'owner.picture', 'request.distance', 'request.time', 'request.promo_code', 'request_services.base_price', 'request_services.distance_cost', 'request_services.time_cost', 'request.total')
                            ->groupBy('request.id')
                            ->get();
                    }
                    $requests = array();
                    $settings = Settings::where('key', 'default_distance_unit')->first();

                    /* $setbase_price = Settings::where('key', 'base_price')->first();
                      $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                      $settime_price = Settings::where('key', 'price_per_unit_time')->first(); */

                    $unit = $settings->value;
                    if ($unit == 0) {
                        $unit_set = 'kms';
                    } elseif ($unit == 1) {
                        $unit_set = 'miles';
                    }
                    $walker = Walker::where('id', $walker_id)->first();
                    foreach ($request_data as $data) {
                        $discount = 0;
                        if ($data->promo_id != "") {
                            $promo_code = PromoCodes::where('id', $data->promo_id)->first();
                            if (isset($promo_code->id)) {
                                $promo_value = $promo_code->value;
                                $promo_type = $promo_code->type;
                                if ($promo_type == 1) {
                                    // Percent Discount
                                    $discount = $data->total * $promo_value / 100;
                                } elseif ($promo_type == 2) {
                                    // Absolute Discount
                                    $discount = $promo_value;
                                }
                            }
                        }
                        $is_multiple_service = Settings::where('key', 'allow_multiple_service')->first();
                        if ($is_multiple_service->value == 0) {

                            $requestserv = RequestServices::where('request_id', $data->id)->first();

                            $request_typ = ProviderType::where('id', '=', $requestserv->type)->first();

                            $request['id'] = $data->id;
                            $request['date'] = $data->request_start_time;
                            $request['distance'] = (string)$data->distance;
                            $request['unit'] = $unit_set;
                            $request['time'] = $data->time;
                            $request['base_distance'] = $request_typ->base_distance;
                            $request['price_per_unit_distance'] = ($request_typ->price_per_unit_distance);
                            $request['price_per_unit_time'] = ($request_typ->price_per_unit_time);
                            /* $currency = Keywords::where('alias', 'Currency')->first();
                              $request['currency'] = $currency->keyword; */
                            $request['currency'] = Config::get('app.generic_keywords.Currency');
                            if ($requestserv->base_price != 0) {
                                $request['base_price'] = ($requestserv->base_price);
                                $request['distance_cost'] = ($requestserv->distance_cost);
                                $request['time_cost'] = ($requestserv->time_cost);
                            } else {
                                /* $setbase_price = Settings::where('key', 'base_price')->first();
                                  $request['base_price'] = ($setbase_price->value);
                                  $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
                                  $request['distance_cost'] = ($setdistance_price->value);
                                  $settime_price = Settings::where('key', 'price_per_unit_time')->first();
                                  $request['time_cost'] = ($settime_price->value); */
                                $request['base_price'] = ($request_typ->base_price);
                                $request['distance_cost'] = ($request_typ->price_per_unit_distance);
                                $request['time_cost'] = ($request_typ->price_per_unit_time);
                            }

                            $admins = Admin::first();
                            $request['walker']['email'] = $walker->email;
                            $request['admin']['email'] = $admins->username;
                            if ($data->transfer_amount != 0) {
                                $request['walker']['amount'] = ($data->total - $data->transfer_amount);
                                $request['admin']['amount'] = ($data->transfer_amount);
                            } else {
                                $request['walker']['amount'] = ($data->transfer_amount);
                                $request['admin']['amount'] = ($data->total - $data->transfer_amount);
                            }

                            $request['total'] = ($data->total + $data->ledger_payment + $discount);
                        } else {

                            $request['id'] = $data->id;
                            $request['date'] = $data->request_start_time;
                            $request['distance'] = (string)$data->distance;
                            $request['unit'] = $unit_set;
                            $request['time'] = $data->time;
                            /* $currency = Keywords::where('alias', 'Currency')->first();
                              $request['currency'] = $currency->keyword; */
                            $request['currency'] = Config::get('app.generic_keywords.Currency');

                            $rserv = RequestServices::where('request_id', $data->id)->get();
                            $typs = array();
                            $typi = array();
                            $typp = array();
                            $total_price = 0;
                            foreach ($rserv as $typ) {
                                $typ1 = ProviderType::where('id', $typ->type)->first();
                                $typ_price = ProviderServices::where('provider_id', $data->confirmed_walker)->where('type', $typ->type)->first();

                                if ($typ_price->base_price > 0) {
                                    $typp1 = 0.00;
                                    $typp1 = $typ_price->base_price;
                                } elseif ($typ_price->price_per_unit_distance > 0) {
                                    $typp1 = 0.00;
                                    foreach ($rserv as $key) {
                                        $typp1 = $typp1 + $key->distance_cost;
                                    }
                                } else {
                                    $typp1 = 0.00;
                                }
                                $typs['name'] = $typ1->name;
                                $typs['price'] = ($typp1);
                                $total_price = $total_price + $typp1;
                                array_push($typi, $typs);
                            }
                            $request['type'] = $typi;

                            $base_price = 0;
                            $distance_cost = 0;
                            $time_cost = 0;
                            foreach ($rserv as $key) {
                                $base_price = $base_price + $key->base_price;
                                $distance_cost = $distance_cost + $key->distance_cost;
                                $time_cost = $time_cost + $key->time_cost;
                            }
                            $request['base_price'] = ($base_price);
                            $request['distance_cost'] = ($distance_cost);
                            $request['time_cost'] = ($time_cost);
                            $request['total'] = ($total_price);
                        }
                        /* path */
                        $id = $data->id;
                        $locations = WalkLocation::where('request_id', $data->id)->orderBy('id')->get();
                        $count = round(count($locations) / 50);
                        $start = $end = $map = "";
                        if (count($locations) >= 1) {
                            $start = WalkLocation::where('request_id', $id)
                                ->orderBy('id')
                                ->first();
                            $end = WalkLocation::where('request_id', $id)
                                ->orderBy('id', 'desc')
                                ->first();
                            $map = "https://maps-api-ssl.google.com/maps/api/staticmap?size=249x249&scale=2&markers=shadow:true|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-start@2x.png|$start->latitude,$start->longitude&markers=shadow:false|scale:2|icon:http://d1a3f4spazzrp4.cloudfront.net/receipt-new/marker-finish@2x.png|$end->latitude,$end->longitude&path=color:0x2dbae4ff|weight:4";
                            $skip = 0;
                            foreach ($locations as $location) {
                                if ($skip == $count) {
                                    $map .= "|$location->latitude,$location->longitude";
                                    $skip = 0;
                                }
                                $skip++;
                            }
                            /* $map.="&key=" . Config::get('app.gcm_browser_key'); */
                        }
                        $request['start_lat'] = "";
                        if (isset($start->latitude)) {
                            $request['start_lat'] = $start->latitude;
                        }
                        $request['start_long'] = "";
                        if (isset($start->longitude)) {
                            $request['start_long'] = $start->longitude;
                        }
                        $request['end_lat'] = "";
                        if (isset($end->latitude)) {
                            $request['end_lat'] = $end->latitude;
                        }
                        $request['end_long'] = "";
                        if (isset($end->longitude)) {
                            $request['end_long'] = $end->longitude;
                        }
                        $request['map_url'] = $map;
                        /* path END */
                        /* $request['owner']['first_name'] = $data->first_name;
                          $request['owner']['last_name'] = $data->last_name;
                          $request['owner']['phone'] = $data->phone;
                          $request['owner']['email'] = $data->email;
                          $request['owner']['picture'] = $data->picture;
                          $request['owner']['bio'] = $data->bio;
                          $request['owner']['payment_opt'] = $data->payment_mode; */


                        $request['base_price'] = ($data->base_price);
                        $request['distance_cost'] = ($data->distance_cost);
                        $request['time_cost'] = ($data->time_cost);
                        $request['total'] = ($data->total - $data->ledger_payment - $data->promo_payment);
                        $request['main_total'] = ($data->total);
                        $request['referral_bonus'] = ($data->ledger_payment);
                        $request['promo_bonus'] = ($data->promo_payment);
                        $request['payment_type'] = $data->payment_mode;
                        $request['is_paid'] = $data->is_paid;
                        $request['promo_id'] = $data->promo_id;
                        $request['promo_code'] = $data->promo_code;
                        $request['bill'] = $data->bill;
                        $request['current_credit'] = ($data->current_credit_cap);
                        $request['used_credit'] = ($data->used_credit_cap);
                        $request['added_to_credit'] = ($data->added_to_credit_cap);
                        $request['cash_paid'] = ($data->cash_paid);
                        $request['owner']['first_name'] = $data->first_name;
                        $request['owner']['last_name'] = $data->last_name;
                        $request['owner']['phone'] = $data->phone;
                        $request['owner']['email'] = $data->email;
                        $request['owner']['picture'] = $data->picture;
                        $request['owner']['payment_opt'] = $data->payment_mode;
                        array_push($requests, $request);
                    }

                    $response_array = array(
                        'success' => true,
                        'requests' => $requests
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function provider_services_update()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            Log::info('validation error =' . print_r($response_array, true));
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    foreach (Input::get('service') as $key) {
                        $serv = ProviderType::where('id', $key)->first();
                        $pserv[] = $serv->name;
                    }
                    foreach (Input::get('service') as $ke) {
                        $proviserv = ProviderServices::where('provider_id', $walker_id)->first();
                        if ($proviserv != NULL) {
                            DB::delete("delete from walker_services where provider_id = '" . $walker_id . "';");
                        }
                    }
                    $base_price = Input::get('service_base_price');
                    $service_price_distance = Input::get('service_price_distance');
                    $service_price_time = Input::get('service_price_time');
                    foreach (Input::get('service') as $key) {
                        $prserv = new ProviderServices;
                        $prserv->provider_id = $walker_id;
                        $prserv->type = $key;
                        $prserv->base_price = $base_price[$key - 1];
                        $prserv->price_per_unit_distance = $service_price_distance[$key - 1];
                        $prserv->price_per_unit_time = $service_price_time[$key - 1];
                        $prserv->save();
                    }
                    $response_array = array(
                        'success' => true,
                    );
                    $response_code = 200;
                    Log::info('success = ' . print_r($response_array, true));
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        Log::info('repsonse final = ' . print_r($response, true));
        return $response;
    }

    public function services_details()
    {
        $walker_id = Input::get('id');
        $token = Input::get('token');

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $provserv = ProviderServices::where('provider_id', $walker_id)->get();
                    foreach ($provserv as $key) {
                        $type = ProviderType::where('id', $key->type)->first();
                        $serv_name[] = $type->name;
                        $serv_base_price[] = $key->base_price;
                        $serv_per_distance[] = $key->price_per_unit_distance;
                        $serv_per_time[] = $key->price_per_unit_time;
                    }
                    $response_array = array(
                        'success' => true,
                        'serv_name' => $serv_name,
                        'serv_base_price' => $serv_base_price,
                        'serv_per_distance' => $serv_per_distance,
                        'serv_per_time' => $serv_per_time
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function panic()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $is_admin = $this->isAdmin($token);
        if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
            // check for token validity
            if (is_token_active($walker_data->token_expiry) || $is_admin) {
                $lat = Input::get('latitude');
                $long = Input::get('longitude');
                $location = 'http://maps.google.com/maps?z=12&t=m&q=loc:lat+long';
                $location = str_replace('lat', $lat, $location);
                $location = str_replace('long', $long, $location);

                /* $var = Keywords::where('id', 1)->first(); */

                /* $email_body = '' . $var->keyword . ' id = ' . $walker_id . '. And my current location is:  <br/>' . $location; */
                $email_body = '' . Config::get('app.generic_keywords.Provider') . ' id = ' . $walker_id . '. And my current location is:  <br/>' . $location;
                $subject = 'Panic Alert';
                email_notification($walker_id, 'admin', $email_body, $subject);
                $response_array = array('success' => true, 'is_active' => $walker_data->is_active);
                $response_code = 200;
            } else {
                $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                $response_code = 200;
            }
        } else {
            if ($is_admin) {
                /* $var = Keywords::where('id', 1)->first();
                  $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
            } else {
                $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
            }
            $response_code = 200;
        }
    }

    public function check_banking()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $is_admin = $this->isAdmin($token);
        if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
            // check for token validity
            if (is_token_active($walker_data->token_expiry) || $is_admin) {
                // do
                $default_banking = Config::get('app.default_payment');
                $resp = array();
                $resp['default_banking'] = $default_banking;
                $walker = Walker::where('id', $walker_id)->first();
                if ($walker->merchant_id != NULL) {
                    $resp['walker']['merchant_id'] = $walker->merchant_id;
                }
                $response_array = array('success' => true, 'details' => $resp);
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function logout()
    {
        if (Request::isMethod('post')) {
            $walker_id = Input::get('id');
            $token = Input::get('token');

            $validator = Validator::make(
                array(
                    'walker_id' => $walker_id,
                    'token' => $token,
                ), array(
                    'walker_id' => 'required|integer',
                    'token' => 'required',)
            );

            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
// check for token validity
                    if (is_token_active($walker_data->token_expiry) || $is_admin) {

                        //$walker = Walker::find($walker_id);
                        $walker_data->latitude = 0;
                        $walker_data->longitude = 0;
                        $walker_data->old_latitude = 0;
                        $walker_data->old_longitude = 0;
                        $walker_data->device_token = 0;
                        $walker_data->is_active = 0;
                        $walker_data->is_active_monthly = 0;
                        /* $walker_data->is_login = 0; */
                        $walker_data->save();

                        $response_array = array('success' => true, 'error' => 'Successfully Log-Out');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    if ($is_admin) {
                        $response_array = array('success' => false, 'error' => 'Walker ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function monthly_respond_request()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $request_id = Input::get('request_id');
        $accepted = Input::get('accepted');
        $date_time = Input::get('datetime');


        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $request_id,
                'accepted' => $accepted,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'accepted' => 'required|integer',
                'request_id' => 'required|integer')
        );

        /* $driver = Keywords::where('id', 1)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Retrive and validate the Request

                    $monthly = Monthly::where('request_id','=',$request_id)->first();
                    Log::info("re".$request_id);
                    if($monthly->status!=1){
                        $response_array = array('success' => false, 'error' => 'Request is not available', 'error_code' => 472);
                        $response_code = 200;
                        $response = Response::json($response_array, $response_code);
                        return $response;
                    }

                    if ($request = Requests::find($request_id)) {
                        $request_meta = RequestMeta::where('request_id', $request_id)->where('walker_id', $walker_id)->first();
                        if (isset($request_meta)) {
                            if ($accepted == 1) {
                                /*if ($request->later == 1) {
                                    // request ended
                                    Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1));
                                } else {
                                    Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1, 'request_start_time' => date('Y-m-d H:i:s'), 'request_timestamp' => time()));
                                }*/

                                // confirm walker
                                RequestMeta::where('request_id', '=', $request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 1));

                                // Update Walker availability
                                //Walker::where('id', '=', $walker_id)->update(array('is_available' => 0));

                                // remove other schedule_meta
                                //RequestMeta::where('request_id', '=', $request_id)->where('status', '=', 0)->delete();

                                //Monthly::where('request_id', '=', $request_id)->update(array('status' => 3));

                                $monthly = Monthly::where('request_id', '=', $request_id)->first();
                                $trip_address = array();
                                $trip_address['trip_one_from_address'] = $monthly->trip_one_from_address;
                                $trip_address['trip_one_to_address'] = $monthly->trip_one_to_address;
                                $trip_address['trip_one_pickup_time'] = $monthly->trip_one_pickup_time;
                                $trip_address['trip_two_pickup_time'] = $monthly->trip_two_pickup_time;
                                $trip_address['trip_two_to_address'] = $monthly->trip_two_to_address;
                                $trip_address['trip_two_from_address'] = $monthly->trip_two_from_address;
                                $trip_address['starting_date'] = $monthly->starting_date;
                                $trip_address['gender'] = $monthly->gender;

                                $requests = Requests::where('id', $request_id)->first();

                                // Send Notification
                                $walker = Walker::find($walker_id);
                                $walker_data = array();
                                $walker_data['walker_id'] = $walker->id;
                                $walker_data['first_name'] = $walker->first_name;
                                $walker_data['last_name'] = $walker->last_name;
                                $walker_data['phone'] = $walker->phone;
                                $walker_data['bio'] = $walker->bio;
                                $walker_data['picture'] = $walker->picture;
                                $walker_data['latitude'] = $walker->latitude;
                                $walker_data['longitude'] = $walker->longitude;
                                $walker_data['type'] = $walker->type;
                                $walker_data['rate'] = $walker->rate;
                                $walker_data['num_rating'] = $walker->rate_count;
                                $walker_data['car_model'] = $walker->car_model;
                                $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                                  $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                                $settings = Settings::where('key', 'default_distance_unit')->first();
                                $unit = $settings->value;
                                if ($unit == 0) {
                                    $unit_set = 'kms';
                                } elseif ($unit == 1) {
                                    $unit_set = 'miles';
                                }
                                $bill = array();

                                if ($request->is_completed == 1 || $request->is_paid) {
                                    $bill['distance'] = (string)convert($request->distance, $unit);
                                    $bill['unit'] = $unit_set;
                                    $bill['time'] = $request->time;
                                    //$bill['base_price'] = $request->base_price;
                                    //$bill['distance_cost'] = $request->distance_cost;
                                    //$bill['time_cost'] = $request->time;
                                    $bill['total'] = $request->total;
                                    $bill['is_paid'] = $request->is_paid;

                                    $bill['bill'] = $request->bill;
                                    $bill['current_credit'] = ($request->current_credit_cap);
                                    $bill['used_credit'] = ($request->used_credit_cap);
                                    $bill['added_to_credit'] = ($request->added_to_credit_cap);
                                    $bill['cash_paid'] = ($request->cash_paid);
                                }


                                $getMonthlyDetails = Monthly::where('request_id', $request_id)->first();
                                //echo "<pre>";print_r($getMonthlyDetails);echo "</pre>";die;
                                /* $setting = Settings::where('key', 'allow_calendar')->first();

                                if ($request->later == 1 && $setting->value == 1) { */
                                $response_array = array(
                                    'success' => true,
                                    'request_id' => $request_id,
                                    'status' => $request->status,
                                    'confirmed_walker' => $request->confirmed_walker,
                                    'is_walker_started' => $request->is_walker_started,
                                    'is_walker_arrived' => $request->is_walker_arrived,
                                    'is_walk_started' => $request->is_started,
                                    'is_completed' => $request->is_completed,
                                    'is_walker_rated' => $request->is_walker_rated,
                                    'walker' => $walker_data,
                                    'bill' => $bill,
                                    'monthly_id' => $getMonthlyDetails->id,
                                    'new_request_status' => (string)$getMonthlyDetails->status,
                                    'montlhy_or_daily' => (string)$request->service_type
                                );
                                /* $driver = Keywords::where('id', 1)->first(); */
                                /* $trip = Keywords::where('id', 4)->first(); */

                                /* $title = '' . $driver->keyword . ' has accepted the ' . $trip->keyword; */

                                /* saravanan */

                                //$title = '' . Config::get('app.generic_keywords.Provider') . ' has accepted the ' . Config::get('app.generic_keywords.Trip');


                                $walkerFName = $walker->first_name;
                                $walkerLName = $walker->last_name;

                                $title = "Captain " . $walkerFName . " " . $walkerLName . " has accepted your monthly contract request, Please confirm the ride";

                                $message = $response_array;

                                send_notifications($request->owner_id, "owner", $title, $message);
                                /* saravanan */

                                // Send SMS
                                $owner = Owner::find($request->owner_id);
                                $settings = Settings::where('key', 'sms_when_provider_accepts')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%driver%', $walker->first_name . " " . $walker->last_name, $pattern);

                                $pattern = str_replace('%driver_mobile%', $walker->phone, $pattern);
                                sms_notification($request->owner_id, 'owner', $pattern);

                                // Send SMS
                                $owner = Owner::find($request->owner_id);
                                $src_address = get_address($owner->latitude, $owner->longitude);
                                $pattern = Config::get('app.generic_keywords.User') . " Pickup Address : " . $src_address;
                                sms_notification($walker_id, 'walker', $pattern);

                                // Send SMS
                                $settings = Settings::where('key', 'sms_request_completed')->first();
                                $pattern = $settings->value;
                                $pattern = str_replace('%user%', $owner->first_name . " " . $owner->last_name, $pattern);
                                $pattern = str_replace('%id%', $request->id, $pattern);
                                $pattern = str_replace('%user_mobile%', $owner->phone, $pattern);
                                sms_notification(1, 'admin', $pattern);

                                //email to client for accept the request
                                $settings = Settings::where('key', 'admin_email_address')->first();
                                $admin_email = $settings->value;

                                $subject_cfrm = "Confirm your ride";
                                $driver = array(
                                    'owner_name' => ucwords($owner->first_name . " " . $owner->last_name),
                                    'admin_eamil' => $admin_email,
                                    'driver_name' => ucwords($walker->first_name . " " . $walker->last_name),
                                    'driver_contact' => $walker->phone,
                                    'driver_car_model' => $walker->car_model,
                                    'driver_licence' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                                    'journey_date' => date('dS - F - Y', strtotime($monthly->starting_date)) . ' - ' . date('dS - F - Y', strtotime($monthly->starting_date . ' + 29 days')),
                                    'trip_address' => $trip_address,
                                    'cost' => number_format((float)$requests->total, 2) . " SAR"
                                );

                                /* Confirm user ride */
                                email_notification($owner->id, 'owner', $driver, $subject_cfrm, 'confirm_ride', null);


                                $response_array = array('success' => true, 'message' => 'Request ID ' . $request_id . ' successfull accepted');
                                $response_code = 200;
                            } else {

                                Monthly::where('request_id', '=', $request_id)->update(array('status' => 4));
                                Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->update(array('current_walker' => 0, 'status' => 0));

                                $time = date("Y-m-d H:i:s");
                                $query = "SELECT id, owner_id, current_walker, TIMESTAMPDIFF(SECOND,request_start_time, '$time') as diff from request where id = '$request_id'";
                                $results = DB::select(DB::raw($query));
                                $settings = Settings::where('key', 'provider_timeout')->first();
                                $timeout = $settings->value;

                                // Archiving Old Walker
                                RequestMeta::where('request_id', '=', $request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 3));
                                $request_meta = RequestMeta::where('request_id', '=', $request_id)->where('status', '=', 0)->orderBy('created_at')->first();

                                // update request
                                if (isset($request_meta->walker_id)) {

                                    $monthly = Monthly::where('request_id', '=', $request_id)->first();

                                    // Cost Calculation to update in requests
                                    $trip_info['trip_type'] = $monthly->trip_type;
                                    $trip_info['cab_type'] = $monthly->cab_type;
                                    $trip_info['walker_id'] = $request_meta->walker_id;

                                    $trip_info['trip_one_from_lat'] = $monthly->trip_one_from_lat;
                                    $trip_info['trip_one_from_long'] = $monthly->trip_one_from_long;
                                    $trip_info['trip_one_to_lat'] = $monthly->trip_one_to_lat;
                                    $trip_info['trip_one_to_long'] = $monthly->trip_one_to_long;

                                    $trip_info['trip_two_from_lat'] = $monthly->trip_two_from_lat;
                                    $trip_info['trip_two_from_long'] = $monthly->trip_two_from_long;
                                    $trip_info['trip_two_to_lat'] = $monthly->trip_two_to_lat;
                                    $trip_info['trip_two_to_long'] = $monthly->trip_two_to_long;

                                    $trip_details = cost_calculation($trip_info);

                                    $cost = '';
                                    $distance = '';
                                    $duration = '';

                                    if ($trip_details) {
                                        $cost = $trip_details['total_cost'];
                                        $distance = $trip_details['distance_km'];
                                        $duration = $trip_details['duration_min'];
                                    }

                                    Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->update(array('total' => $cost, 'time' => $duration, 'distance' => $distance, 'current_walker' => $request_meta->walker_id, 'status' => 1, 'request_start_time' => date("Y-m-d H:i:s"), 'request_timestamp' => time()));
                                    Monthly::where('request_id', '=', $request_id)->update(array('status' => 1));

                                    //Send Notification
                                    $walker = Walker::find($request_meta->walker_id);
                                    $settings = Settings::where('key', 'provider_timeout')->first();
                                    $time_left = $settings->value;

                                    $owner = Owner::find($request->owner_id);
                                    $msg_array = array();
                                    $msg_array['unique_id'] = 1;
                                    $msg_array['request_id'] = $request->id;
                                    $msg_array['id'] = $request_meta->walker_id;
                                    if ($walker) {
                                        $msg_array['token'] = $walker->token;
                                    }
                                    $msg_array['time_left_to_respond'] = $time_left;
                                    $msg_array['payment_mode'] = $request->payment_mode;
                                    $msg_array['payment_type'] = $request->payment_mode;
                                    $msg_array['time_left_to_respond'] = $timeout;
                                    $msg_array['client_profile'] = array();
                                    $msg_array['client_profile']['name'] = $owner->first_name . " " . $owner->last_name;
                                    $msg_array['client_profile']['picture'] = $owner->picture;
                                    $msg_array['client_profile']['bio'] = $owner->bio;
                                    $msg_array['client_profile']['address'] = $owner->address;
                                    $msg_array['client_profile']['phone'] = $owner->phone;

                                    $request_data = array();
                                    $request_data['owner'] = array();
                                    $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                                    $request_data['owner']['picture'] = $owner->picture;
                                    $request_data['owner']['phone'] = $owner->phone;
                                    $request_data['owner']['address'] = $owner->address;
                                    $request_data['owner']['latitude'] = $owner->latitude;
                                    $request_data['owner']['longitude'] = $owner->longitude;
                                    if ($request->d_latitude != NULL) {
                                        $request_data['owner']['d_latitude'] = $request->D_latitude;
                                        $request_data['owner']['d_longitude'] = $request->D_longitude;
                                    }
                                    $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                                    $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                                    $request_data['owner']['dest_latitude'] = $request->D_latitude;
                                    $request_data['owner']['dest_longitude'] = $request->D_longitude;
                                    $request_data['owner']['payment_type'] = $request->payment_mode;
                                    $request_data['owner']['rating'] = $owner->rate;
                                    $request_data['owner']['num_rating'] = $owner->rate_count;
                                    /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                                      $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                                    $request_data['dog'] = array();
                                    if ($dog = Dog::find($owner->dog_id)) {

                                        $request_data['dog']['name'] = $dog->name;
                                        $request_data['dog']['age'] = $dog->age;
                                        $request_data['dog']['breed'] = $dog->breed;
                                        $request_data['dog']['likes'] = $dog->likes;
                                        $request_data['dog']['picture'] = $dog->image_url;
                                    }

                                    /* saravanan */
                                    $msg_array['monthly_id'] = $monthly->id;
                                    $msg_array['new_request_status'] = (string)$monthly->status;
                                    $msg_array['montlhy_or_daily'] = (string)$request->service_type;
                                    $msg_array['request_id'] = $monthly->request_id;


                                    $msg_array['request_data'] = $request_data;


                                    $sDate = date('d/m/Y', strtotime($monthly['starting_date']));
                                    $title = "Customer " . $owner->first_name . " has requested for a new monthly private contract starting " . $sDate;

                                    //$title = "New Request";

                                    $message = $msg_array;

                                    send_notifications($request_meta->walker_id, "walker", $title, $message);
                                    /* saravanan */

                                } else {
                                    // request ended
                                    Requests::where('id', '=', $request_id)->update(array('current_walker' => 0, 'status' => 1));
                                    Monthly::where('request_id', '=', $request_id)->update(array('status' => 2));

                                    /* saravanan */
                                    $driver_keyword = Config::get('app.generic_keywords.Provider');
                                    $msg_array = array();
                                    $monthlyDetails = Monthly::where('request_id', '=', $request_id)->first();
                                    $msg_array['monthly_id'] = $monthlyDetails->id;
                                    $msg_array['request_id'] = $request_id;
                                    $msg_array['new_request_status'] = (string)$monthlyDetails->status;
                                    $msg_array['montlhy_or_daily'] = (string)$request->service_type;
                                    $msg_array['message'] = 'Captain did not accepted your request. Kindly try again.';
                                    $messagess = $msg_array;

                                    /* $driver = Keywords::where('id', 1)->first(); */
                                    $owne = Owner::where('id', $request->owner_id)->first();
                                    /* $driver_keyword = $driver->keyword; */

                                    $fromAdder = $monthlyDetails->trip_one_from_address;
                                    $toAdder = $monthlyDetails->trip_one_to_address;


                                    $titlee = "Captain did not accepted your request. Kindly try again";
                                    send_notifications($owne->id, "owner", $titlee, $messagess);

                                    /* saravanan */
                                }
                                $response_array = array('success' => true, 'message' => 'Request ID ' . $request_id . ' successfull rejected');
                                $response_code = 200;
                            }

                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Request ID does not matches' . $driver->keyword . ' ID', 'error_code' => 472); */
                            $response_array = array('success' => false, 'error' => 'Request ID does not matches' . Config::get('app.generic_keywords.Provider') . ' ID', 'error_code' => 472);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Request ID Not Found', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function ongoing_subscription()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id
            ), array(
            'token' => 'required',
            'walker_id' => 'required|integer'));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    $ongoing_service = DB::table('request')
                        ->where('request.confirmed_walker', $walker_id)
                        ->where('request.is_completed', 0)
                        ->where('request.service_type', 2)
                        ->where('request.is_cancelled', 0)
                        ->whereIn('monthly.status', array(5, 7, 10, 11))
                        ->leftJoin('monthly', 'request.id', '=', 'monthly.request_id')
                        ->select('monthly.*', 'monthly.id as monthly_id', 'request.id as request_id', 'request.payment_mode', 'request.total as total')
                        ->orderBy('request.created_at', 'desc')
                        ->get();

                    $all_requests = array();

                    foreach ($ongoing_service as $ongoing) {
                        $owner = Owner::find($ongoing->owner_id);

                        $request_data = array();
                        $request_data['request_id'] = $ongoing->request_id;
                        $request_data['monthly_id'] = $ongoing->monthly_id;
                        $request_data['id'] = $ongoing->owner_id;

                        $request_data['owner'] = array();
                        $request_data['owner']['new_request_status'] = (string)$ongoing->status;
                        $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                        $request_data['owner']['picture'] = $owner->picture;
                        $request_data['owner']['phone'] = $owner->phone;
                        $request_data['owner']['gender'] = $ongoing->gender;
                        $request_data['owner']['address'] = $owner->address;

                        $request_data['owner']['subscription_type'] = $ongoing->subscription_type;
                        $request_data['owner']['trip_type'] = $ongoing->trip_type;
                        $request_data['owner']['is_full_month'] = $ongoing->is_full_month;
                        $request_data['owner']['specific_days'] = array();
                        if ($ongoing->is_full_month == 2) {
                            $specifi_days = array();

                            $monthly_days = DB::table('specific_days')->where('monthly_id', $ongoing->monthly_id)->get();

                            foreach ($monthly_days as $monthly_day) {
                                $specifi_days[] = $monthly_day->days;
                            }

                            $request_data['owner']['specific_days'] = $specifi_days;
                        }

                        $request_data['owner']['trip_one_from_address'] = $ongoing->trip_one_from_address;
                        $request_data['owner']['trip_one_to_address'] = $ongoing->trip_one_to_address;
                        $request_data['owner']['trip_two_from_address'] = $ongoing->trip_two_from_address;
                        $request_data['owner']['trip_two_to_address'] = $ongoing->trip_two_to_address;

                        $request_data['owner']['trip_one_from_latitude'] = $ongoing->trip_one_from_lat;
                        $request_data['owner']['trip_one_from_longitude'] = $ongoing->trip_one_from_long;
                        $request_data['owner']['trip_one_to_latitude'] = $ongoing->trip_one_to_lat;
                        $request_data['owner']['trip_one_to_longitude'] = $ongoing->trip_one_to_long;

                        $request_data['owner']['trip_two_from_latitude'] = $ongoing->trip_two_from_lat;
                        $request_data['owner']['trip_two_from_longitude'] = $ongoing->trip_two_from_long;
                        $request_data['owner']['trip_two_to_latitude'] = $ongoing->trip_two_to_lat;
                        $request_data['owner']['trip_two_to_longitude'] = $ongoing->trip_two_to_long;

                        $request_data['owner']['trip_one_pickup_time'] = $ongoing->trip_one_pickup_time;
                        $request_data['owner']['trip_two_pickup_time'] = $ongoing->trip_two_pickup_time;

                        $request_data['owner']['starting_date'] = $ongoing->starting_date;
                        $request_data['owner']['end_date'] = date('Y-m-d', strtotime($ongoing->starting_date . ' + 29 days'));
                        $request_data['owner']['total_cost'] = $ongoing->total . " SAR";

                        $request_data['owner']['rating'] = $owner->rate;
                        $request_data['owner']['num_rating'] = $owner->rate_count;
                        /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                          $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                        $request_data['owner']['payment_type'] = $ongoing->payment_mode;
                        $request_data['payment_mode'] = $ongoing->payment_mode;
                        $request_data['dog'] = array();

                        if ($dog = Dog::find($owner->dog_id)) {

                            $request_data['dog']['name'] = $dog->name;
                            $request_data['dog']['age'] = $dog->age;
                            $request_data['dog']['breed'] = $dog->breed;
                            $request_data['dog']['likes'] = $dog->likes;
                            $request_data['dog']['picture'] = $dog->image_url;
                        }
                        $data['request_data'] = $request_data;
                        array_push($all_requests, $data);


                    }

                    $response_array = array('success' => true, 'ongoing' => $all_requests);
                    $response_code = 200;

                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function ongoing_subscription_single()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        $requestID = Input::get('request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $requestID
            ), array(
            'token' => 'required',
            'walker_id' => 'required',
            'request_id' => 'required|integer'));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    $ongoing_service = DB::table('request')
                        ->where('request.confirmed_walker', $walker_id)
                        ->where('request.id', $requestID)
                        ->leftJoin('monthly', 'request.id', '=', 'monthly.request_id')
                        ->select('monthly.*', 'monthly.id as monthly_id', 'request.id as request_id', 'request.payment_mode', 'request.total as total')
                        ->orderBy('request.created_at', 'desc')
                        ->get();

                    $all_requests = array();
                    if (!empty($ongoing_service)) {
                        foreach ($ongoing_service as $ongoing) {
                            $owner = Owner::find($ongoing->owner_id);

                            $request_data = array();
                            $request_data['request_id'] = $ongoing->request_id;
                            $request_data['id'] = $ongoing->owner_id;

                            $request_data['owner'] = array();
                            $request_data['owner']['new_request_status'] = (string)$ongoing->status;
                            $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                            $request_data['owner']['picture'] = $owner->picture;
                            $request_data['owner']['phone'] = $owner->phone;
                            $request_data['owner']['gender'] = $ongoing->gender;
                            $request_data['owner']['address'] = $owner->address;

                            $request_data['owner']['subscription_type'] = $ongoing->subscription_type;
                            $request_data['owner']['trip_type'] = $ongoing->trip_type;
                            $request_data['owner']['is_full_month'] = $ongoing->is_full_month;
                            $request_data['owner']['specific_days'] = array();
                            if ($ongoing->is_full_month == 2) {
                                $specifi_days = array();

                                $monthly_days = DB::table('specific_days')->where('monthly_id', $ongoing->monthly_id)->get();

                                foreach ($monthly_days as $monthly_day) {
                                    $specifi_days[] = $monthly_day->days;
                                }

                                $request_data['owner']['specific_days'] = $specifi_days;
                            }

                            $request_data['owner']['trip_one_from_address'] = $ongoing->trip_one_from_address;
                            $request_data['owner']['trip_one_to_address'] = $ongoing->trip_one_to_address;
                            $request_data['owner']['trip_two_from_address'] = $ongoing->trip_two_from_address;
                            $request_data['owner']['trip_two_to_address'] = $ongoing->trip_two_to_address;

                            $request_data['owner']['trip_one_from_latitude'] = $ongoing->trip_one_from_lat;
                            $request_data['owner']['trip_one_from_longitude'] = $ongoing->trip_one_from_long;
                            $request_data['owner']['trip_one_to_latitude'] = $ongoing->trip_one_to_lat;
                            $request_data['owner']['trip_one_to_longitude'] = $ongoing->trip_one_to_long;

                            $request_data['owner']['trip_two_from_latitude'] = $ongoing->trip_two_from_lat;
                            $request_data['owner']['trip_two_from_longitude'] = $ongoing->trip_two_from_long;
                            $request_data['owner']['trip_two_to_latitude'] = $ongoing->trip_two_to_lat;
                            $request_data['owner']['trip_two_to_longitude'] = $ongoing->trip_two_to_long;

                            $request_data['owner']['trip_one_pickup_time'] = $ongoing->trip_one_pickup_time;
                            $request_data['owner']['trip_two_pickup_time'] = $ongoing->trip_two_pickup_time;

                            $request_data['owner']['starting_date'] = $ongoing->starting_date;
                            $request_data['owner']['end_date'] = date('Y-m-d', strtotime($ongoing->starting_date . ' + 29 days'));

                            $request_data['owner']['total_cost'] = $ongoing->total . " SAR";
                            $request_data['owner']['rating'] = $owner->rate;
                            $request_data['owner']['num_rating'] = $owner->rate_count;
                            /* $request_data['owner']['rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->avg('rating') ? : 0;
                              $request_data['owner']['num_rating'] = DB::table('review_dog')->where('owner_id', '=', $owner->id)->count(); */
                            $request_data['owner']['payment_type'] = $ongoing->payment_mode;
                            $request_data['payment_mode'] = $ongoing->payment_mode;
                            if ($ongoing->subscription_type == 2) {
                                $submonthlies = DB::table('submonthly')->where('submonthly.monthly_id', '=', $ongoing->id)->where('submonthly.status', '=', 1)->
                                join('owner', 'owner.id', '=', 'submonthly.owner_id')->get(['submonthly.*', 'owner.first_name', 'owner.last_name', 'owner.picture', 'owner.rate']);
                                $pusharr = array();
                                foreach ($submonthlies as $submonthly) {
                                    $data['sub_monthly_id'] = $submonthly->id;
                                    $data['owner_id'] = $submonthly->owner_id;
                                    $data['trip_one_from_address'] = $submonthly->trip_one_from_address;
                                    $data['trip_one_to_address'] = $submonthly->trip_one_to_address;
                                    $data['trip_one_from_lat'] = $submonthly->trip_one_from_lat;
                                    $data['trip_one_from_long'] = $submonthly->trip_one_from_long;
                                    $data['trip_one_to_lat'] = $submonthly->trip_one_to_lat;
                                    $data['trip_one_to_long'] = $submonthly->trip_one_to_long;
                                    $data['trip_one_pickup_time'] = $submonthly->trip_one_pickup_time;
                                    $data['trip_two_from_address'] = $submonthly->trip_two_from_address;
                                    $data['trip_two_to_address'] = $submonthly->trip_two_to_address;
                                    $data['trip_two_from_lat'] = $submonthly->trip_two_from_lat;
                                    $data['trip_two_from_long'] = $submonthly->trip_two_from_long;
                                    $data['trip_two_to_lat'] = $submonthly->trip_two_to_lat;
                                    $data['trip_two_to_long'] = $submonthly->trip_two_to_long;
                                    $data['trip_two_pickup_time'] = $submonthly->trip_two_pickup_time;
                                    $data['trip_type'] = $submonthly->trip_type;
                                    $data['status'] = $submonthly->status;
                                    $data['owner_name'] = $submonthly->first_name . " " . $submonthly->last_name;
                                    $data['rating'] = $submonthly->rate;
                                    $data['picture'] = $submonthly->picture;
                                    array_push($pusharr, $data);
                                }
                                $request_data['sub_monthies'] = $pusharr;
                            }
                            $request_data['dog'] = array();

                            if ($dog = Dog::find($owner->dog_id)) {

                                $request_data['dog']['name'] = $dog->name;
                                $request_data['dog']['age'] = $dog->age;
                                $request_data['dog']['breed'] = $dog->breed;
                                $request_data['dog']['likes'] = $dog->likes;
                                $request_data['dog']['picture'] = $dog->image_url;
                            }
                            $data['request_data'] = $request_data;
                            array_push($all_requests, $data);
                        }

                        $response_array = array('success' => true, 'ongoing' => $all_requests);
                        $response_code = 200;
                    } else {

                        $response_array = array('success' => false, 'error' => 'There is no data found.');
                        $response_code = 200;
                    }


                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function monthly_payment_confirm()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $request_id = Input::get('request_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $request_id
            ), array(
            'token' => 'required',
            'walker_id' => 'required|integer',
            'request_id' => 'required|integer'));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    $request = Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->first();

                    if ($request) {
                        Requests::where('id', '=', $request_id)->where('service_type', '=', 2)->update(array('status' => 0));
                        Monthly::where('request_id', $request_id)->update(array('status' => 7));

                        /* By saravanan */
                        $monthly = Monthly::where('request_id', $request_id)->first();
                        $msg_array['monthly_id'] = $monthly->id;
                        $msg_array['request_id'] = $request_id;
                        $msg_array['new_request_status'] = (string)$monthly->status;
                        $msg_array['montlhy_or_daily'] = (string)$request->service_type;

                        //$title = "Payment Has Made";
                        $title = "Thank you for paying " . $request->total . " SAR, Now you can enjoy your ride for the whole month";

                        $message = $msg_array;


                        $owne = Owner::where('id', $request->owner_id)->first();
                        $owner_data_id = $owne->id;
                        send_notifications($owner_data_id, "owner", $title, $message);

                        /* By saravanan */

                        $response_array = array('success' => true, 'message' => 'Driver Confirmed Cash Payment');
                        $response_code = 200;
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid Request ID', 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $driver->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function confirm_availability_Of_subscription()
    {
        $monthly_id = Input::get('monthly_id');
        $monthly = Monthly::find($monthly_id);
        $resultsRequest = Requests::where('id', $monthly->request_id)->first();
        $owner_id = $resultsRequest->owner_id;
        $owner = Owner::find($owner_id);
        $walker_id = $resultsRequest->confirmed_walker;
        $walker = Walker::find($walker_id);

        $requestServices = RequestServices::where('request_id', $monthly->request_id)->first();
        $request = new Requests;

        $request->monthly_id = $monthly->id;
        $request->owner_id = $resultsRequest->owner_id;
        $request->service_type = 1;
        $request->status = 0;
        $request->confirmed_walker = $resultsRequest->confirmed_walker;
        $request->current_walker = 0;
        $request->request_start_time = time();//curent time
        $request->request_timestamp = time();
        $request->created_at = time();
        $request->updated_at = time();
        $request->is_walker_started = 0;
        $request->is_walker_arrived = 0;
        $request->is_started = 0;
        $request->is_completed = 0;
        $request->is_dog_rated = 0;
        $request->is_walker_rated = 0;
        $walker = Walker::where('id', $resultsRequest->confirmed_walker)->first();
        $distance = distanceGeoPoints($monthly->trip_one_to_lat, $monthly->trip_one_to_long, $walker->latitude, $walker->longitude);
        $request->distance = $distance / 1000;
        $request->time = 0;
        $request->total = 0;
        $request->is_paid = 0;
        $request->card_payment = 0;
        $request->ledger_payment = 0.0;
        $request->is_cancelled = 0;
        $request->refund = 0;
        $request->transfer_amount = 0;
        $request->later = 0;
        $request->origin_latitude = $monthly->trip_one_from_lat;
        $request->origin_longitude = $monthly->trip_one_from_long;
        $request->D_latitude = $monthly->trip_one_to_lat;
        $request->D_longitude = $monthly->trip_one_to_long;
        /*$request->security_monthly=0;
        $request->payment_mode=0;
        $request->payment_id="";
        $request->promo_payment=0.0;
        $request->promo_code="";
        $request->promo_id=0;
        $request->customer_waiting_time=0;*/
        /*$request->arrival_timestamp=0;
        $request->pickup_timestamp=0;*/
        $request->save();
        $requestId = $request->id;

        $requestId = $request->id;

        $requestMeta = new RequestMeta();
        $requestMeta->request_id = $request->id;
        $requestMeta->walker_id = $request->confirmed_walker;
        $requestMeta->status = 0;
        $requestMeta->save();

        $reqserv = new RequestServices;
        $reqserv->request_id = $request->id;
        $reqserv->type = $requestServices->type;
        $reqserv->save();


        $msg_array = array();
        $msg_array['id'] = $walker_id;
        $msg_array['unique_id'] = 19;
        $msg_array['time_left_to_respond'] = 10;
        $request_data = array();
        $request_data['owner'] = array();
        $request_data['request_id'] = $requestId;
        $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
        $request_data['owner']['picture'] = $owner->picture;
        $request_data['owner']['phone'] = $owner->phone;
        $request_data['owner']['address'] = $owner->address;
        $request_data['owner']['latitude'] = $owner->latitude;
        $request_data['owner']['longitude'] = $owner->longitude;
        $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
        $request_data['owner']['owner_dist_long'] = $request->D_longitude;
        $request_data['owner']['payment_type'] = $request->payment_mode;
        $request_data['owner']['rating'] = $owner->rate;
        $request_data['owner']['num_rating'] = $owner->rate_count;
        $request_data['dog'] = array();
        $request_data['owner']["starting_date"] = "2016-11-07";
        $request_data['owner']["subscription_type"] = $request->service_type;
        $request_data['owner']["total_cost"] = $request->total;
        $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
        $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
        $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
        $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
        $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
        $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
        $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
        $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
        $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
        $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
        $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
        $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
        $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
        $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
        $request_data['owner']["trip_type"] = $monthly->cab_type;

        $msg_array['payment_mode'] = $request->payment_mode;
        $msg_array['monthly_id'] = $monthly->id;
        $msg_array['request_data'] = $request_data;
        $title = "Customer " . $owner->first_name . "'s subscription will be in an hour today. Please confirm your availability?";
        $message = $msg_array;
        Log::info('response = ' . print_r($message, true));
        Log::info('New request = ' . print_r($message, true));
        send_notifications($walker_id, "walker", $title, $message);
        $response_code = 200;
        $response_array = array('success' => true, 'success' => 'Push Notification Send');
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function sendNotificationToCustomerAssignedCaptainNotAvailable($request, $monthly)
    {
        $walker = Walker::where('id', $request->confirmed_walker)->first();
        //$response_array = array('success' => true, 'message' => 'Your today monthly subscription is canceled');
        $msg_array = array('success' => true);
        $msg_array['monthly_id'] = $monthly->id;
        $msg_array['request_id'] = $request->id;
        $msg_array['id'] = $request->owner_id;
        $msg_array['unique_id'] = 18;

        if (ISSET($walker)) {
            $title = "Seems that your assigned Captain " . $walker->first_name . " has an issue today,we will transfer your appointment to one of our stand by Captain.";
        } else {
            $title = "Seems that your assigned Captain has an issue today,we will transfer your appointment to one of our stand by Captain.";

        }

        $message = $msg_array;
        Log::info('sendNotificationToCustomerAssignedCaptainNotAvailable = ' . print_r($message, true));
        send_notifications($request->owner_id, "owner", $title, $message);
        //send_notifications($walker->id, "walker", $title, $message);
    }

    public function sendNotificationToCustomerNoCaptainAvailableForTodaySubscription($request, $monthly)
    {
        $walker = Walker::where('id', $request->confirmed_walker)->first();
        //$response_array = array('success' => true, 'message' => 'Your today monthly subscription is canceled');
        $msg_array = array('success' => true);
        $msg_array['monthly_id'] = $monthly->id;
        $msg_array['request_id'] = $request->id;
        $msg_array['id'] = $request->owner_id;
        $msg_array['unique_id'] = 25;
        $title = "Sorry seems like no captain at your area. Please use another provider and Saee  will embrace you the cost.";
        $message = $msg_array;
        Log::info('sendNotificationToCustomerNoCaptainAvailableForTodaySubscription = ' . print_r($message, true));
        send_notifications($request->owner_id, "owner", $title, $message);
        //send_notifications($walker->id, "walker", $title, $message);

    }

    public function respond_availability_Of_subscription()
    {
        Log::info('respond_availability_Of_subscription');
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $request_id = Input::get('request_id');
        $monthly_id = Input::get('monthly_id');
        $accepted = Input::get('accepted');
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'request_id' => $request_id,
                'accepted' => $accepted,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'accepted' => 'required|integer',
                'request_id' => 'required|integer'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($request = Requests::find($request_id)) {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                    if (is_token_active($walker_data->token_expiry)) {
                        if ($accepted == 0) {
                            $monthly = Monthly::where('id', $monthly_id)->first();
                            if (ISSET($monthly)) {
                                $request = Requests::where('id', $request_id)->first();
                                if (ISSET($request)) {
                                    $requestMeta = RequestMeta::where('request_id', $request->id)->where('walker_id', $request->confirmed_walker)->first();
                                    if (ISSET($requestMeta)) {
                                        $requestMeta->status = 3;
                                        $requestMeta->save();
                                    }
                                    $this->sendNotificationToCustomerAssignedCaptainNotAvailable($request, $monthly);
                                    //Dear Fahed here you have to write logic for generating an ondemand request automatically and look for driver
                                    //please write a method and simple call method here
                                    $response = $this->selectAnotherCaptain($walker_id, $request, $monthly);
                                    return $response;
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Invalid request ID', 'error_code' => 402);
                                }
                            } else {
                                $response_array = array('success' => false, 'error' => 'Invalid monthly ID', 'error_code' => 402);
                            }

                        } else {
                            // request accepted, send notification to customer for confirmation
                            $monthly = Monthly::where('id', $monthly_id)->first();
                            if (ISSET($monthly)) {
                                $request = Requests::where('id', $request_id)->first();
                                if (ISSET($request)) {
                                    $requestMeta = RequestMeta::where('request_id', $request->id)->where('walker_id', $request->confirmed_walker)->first();
                                    if (ISSET($requestMeta)) {
                                        $requestMeta->status = 1;
                                        $requestMeta->save();
                                    }
                                    if ($request->later == 1) {
                                        // request ended
                                        //Requests::where('id', '=', $request_id)->where('service_type', '!=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1));
                                        Requests::where('id', '=', $request_id)->update(array('current_walker' => $walker_id, 'confirmed_walker' => $walker_id, 'status' => 0));
                                    } else {
                                        //Requests::where('id', '=', $request_id)->where('service_type', '!=', 2)->update(array('confirmed_walker' => $walker_id, 'status' => 1, 'request_start_time' => date('Y-m-d H:i:s')));
                                        Requests::where('id', '=', $request_id)->update(array('current_walker' => $walker_id, 'confirmed_walker' => $walker_id, 'status' => 0, 'request_start_time' => date('Y-m-d H:i:s')));
                                    }
                                    // confirm walker
                                    RequestMeta::where('request_id', '=', $request_id)->where('walker_id', '=', $walker_id)->update(array('status' => 1));

                                    // Update Walker availability
                                    Walker::where('id', '=', $walker_id)->update(array('is_available' => 0));

                                    // remove other schedule_meta
                                    RequestMeta::where('request_id', '=', $request_id)->where('status', '=', 0)->delete();


                                    $walker = Walker::where('id', $request->confirmed_walker)->first();

                                    if (ISSET($walker)) {

                                        $walker_cab_photo = WalkerDocument::where('walker_id', $request->confirmed_walker)->where('document_id', 4)->first();

                                        $request_data = array();
                                        $request_data['request_id'] = $request->id;
                                        $request_data['id'] = $monthly->owner_id;

                                        $request_data['subscription_type'] = $monthly->subscription_type;
                                        $request_data['trip_type'] = $monthly->trip_type;
                                        $request_data['is_full_month'] = $monthly->is_full_month;

                                        $request_data['trip_one_from_address'] = $monthly->trip_one_from_address;
                                        $request_data['trip_one_to_address'] = $monthly->trip_one_to_address;
                                        $request_data['trip_two_from_address'] = $monthly->trip_two_from_address;
                                        $request_data['trip_two_to_address'] = $monthly->trip_two_to_address;

                                        $request_data['trip_one_from_latitude'] = $monthly->trip_one_from_lat;
                                        $request_data['trip_one_from_longitude'] = $monthly->trip_one_from_long;
                                        $request_data['trip_one_to_latitude'] = $monthly->trip_one_to_lat;
                                        $request_data['trip_one_to_longitude'] = $monthly->trip_one_to_long;

                                        $request_data['trip_two_from_latitude'] = $monthly->trip_two_from_lat;
                                        $request_data['trip_two_from_longitude'] = $monthly->trip_two_from_long;
                                        $request_data['trip_two_to_latitude'] = $monthly->trip_two_to_lat;
                                        $request_data['trip_two_to_longitude'] = $monthly->trip_two_to_long;

                                        $request_data['trip_one_pickup_time'] = $monthly->trip_one_pickup_time;
                                        $request_data['trip_two_pickup_time'] = $monthly->trip_two_pickup_time;

                                        $request_data['starting_date'] = $monthly->starting_date;
                                        $request_data['end_date'] = date('Y-m-d', strtotime($monthly->starting_date . ' + 29 days'));

                                        $request_data['total_cost'] = $request->total . " SAR";
                                        $request_data['distance'] = $request->distance;
                                        $request_data['duration'] = $request->time;

                                        $request_data['walker'] = array();
                                        $request_data['walker']['name'] = $walker->first_name . " " . $walker->last_name;
                                        $request_data['walker']['picture'] = $walker->picture;
                                        if (isset($walker_cab_photo))
                                            $request_data['walker']['cab_picture'] = $walker_cab_photo->url;
                                        else
                                            $request_data['walker']['cab_picture'] = "";
                                        $request_data['walker']['phone'] = $walker->phone;
                                        $request_data['walker']['email'] = $walker->email;
                                        $request_data['walker']['address'] = $walker->address;
                                        $request_data['walker']['car_model'] = $walker->car_model;
                                        $request_data['walker']['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                                        $request_data['walker']['rating'] = $walker->rate;
                                        $request_data['success'] = 1;
                                        $request_data['payment_mode'] = $request->payment_mode;
                                        $request_data['monthly_id'] = $monthly->id;

                                        $msg_array = array('success' => true, 'subscription' => $request_data);
                                        $msg_array['id'] = $monthly->owner_id;
                                        $msg_array['unique_id'] = 20;
                                        $msg_array['time_left_to_respond'] = 10;
                                        //$title = "New Monthly Subscription Request";
                                        $title = "Confirm Captain " . $walker->first_name . "'s today monthly subscription?";
                                        $message = $msg_array;
                                        Log::info('response = ' . print_r($message, true));
                                        Log::info('New request = ' . print_r($message, true));
                                        send_notifications($request->owner_id, "owner", $title, $message);
                                        $response_array = array('success' => true, 'message' => 'Pust Sent to Customer for verification');

                                    } else {
                                        $response_array = array('success' => false, 'error' => 'Walker not assigned for the subscription', 'error_code' => 402);
                                    }
                                } else {
                                    $response_array = array('success' => false, 'error' => 'Invalid request ID', 'error_code' => 402);
                                }
                            } else {
                                $response_array = array('success' => false, 'error' => 'Invalid monthly ID', 'error_code' => 402);
                            }
                        } // end of else - request accepted

                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function selectAnotherCaptain($walker_id, $request, $monthly)
    {
        $owner_id = $request->owner_id;
        $owner = Owner::find($owner_id);

        $longitude = $request->origin_latitude;
        $latitude = $request->origin_longitude;

        $settings = Settings::where('key', 'default_search_radius')->first();
        $distance = $settings->value;
        $settings = Settings::where('key', 'default_distance_unit')->first();
        $unit = $settings->value;
        if ($unit == 0) {
            $multiply = 1.609344;
        } elseif ($unit == 1) {
            $multiply = 1;
        }

        $walkers = array();
        if ($request->type) {
            // if request has type associated with it
            Log::info('out');
            $type = $request->type;
            if (!$type) {
                // choose default type
                $provider_type = ProviderType::where('is_default', 1)->first();

                if (!$provider_type) {
                    $type = 1;
                } else {
                    $type = $provider_type->id;
                }
            }

            $typequery = "SELECT distinct provider_id from walker_services where type IN($type)";
            $typewalkers = DB::select(DB::raw($typequery));

            Log::info('typewalkers = ' . print_r($typewalkers, true));

            if (count($typewalkers) > 0) {

                foreach ($typewalkers as $key) {
                    $types[] = $key->provider_id;
                }

                $typestring = implode(",", $types);
                //Log::info('typestring = ' . print_r($typestring, true));

                $query = "SELECT walker.*, "
                    . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                    . "cos( radians(latitude) ) * "
                    . "cos( radians(longitude) - radians('$longitude') ) + "
                    . "sin( radians('$latitude') ) * "
                    . "sin( radians(latitude) ) ) ,8) as distance "
                    . "FROM walker "
                    . "where is_available = 1 and "
                    . "is_active = 1 and "
                    . "is_approved = 1 and "
                    . "id != " . $walker_id . " and "
                    . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                    . "cos( radians(latitude) ) * "
                    . "cos( radians(longitude) - radians('$longitude') ) + "
                    . "sin( radians('$latitude') ) * "
                    . "sin( radians(latitude) ) ) ) ,8) <= $distance and "
                    . "walker.deleted_at IS NULL and "
                    . "walker.id IN($typestring) "
                    . "order by distance";
                $walkers = DB::select(DB::raw($query));

            }


//            $owner->latitude = $latitude;
//            $owner->longitude = $longitude;
//            $owner->save();

        } else {
            Log::info('in');

            $query = "SELECT walker.*, "
                . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                . "cos( radians(latitude) ) * "
                . "cos( radians(longitude) - radians('$longitude') ) + "
                . "sin( radians('$latitude') ) * "
                . "sin( radians(latitude) ) ) ,8) as distance "
                . "FROM walker "
                . "where is_available = 1 and "
                . "is_active = 1 and "
                . "is_approved = 1 and "
                . "id != " . $walker_id . " and "
                . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                . "cos( radians(latitude) ) * "
                . "cos( radians(longitude) - radians('$longitude') ) + "
                . "sin( radians('$latitude') ) * "
                . "sin( radians(latitude) ) ) ) ,8) <= $distance and "
                . "walker.deleted_at IS NULL "
                . "order by distance";
            $walkers = DB::select(DB::raw($query));
        }

        // no captains available
        if (count($walkers) == 0) {
            $this->sendNotificationToCustomerNoCaptainAvailableForTodaySubscription($request, $monthly);
            $request->is_cancelled = 1;
            $request->status = 0;
            $request->confirmed_walker = 0;
            $request->save();
            $response_array = array('success' => true, 'success' => 'No Captain found. Push sent to customer');
        } else {
            // there are some captains available
            $i = 0;
            $first_walker_id = 0;
            foreach ($walkers as $walker) {
                $request_meta = new RequestMeta;
                $request_meta->request_id = $request->id;
                $request_meta->walker_id = $walker->id;
                if ($i == 0) {
                    $first_walker_id = $walker->id;
                    $i++;
                }
                $request_meta->save();
            }

            $request->current_walker = $first_walker_id;
            $request->save();

            // Archiving Old Walker
            RequestMeta::where('request_id', '=', $request->id)->where('walker_id', '=', $walker_id)->update(array('status' => 3));

            Log::info('response  RequestMeta::where(request_id');
            // update request and send request to new captain
            if (isset($first_walker_id)) {

                //Requests::where('id', '=', $request_id)->update(array('current_walker' => $request_meta->walker_id, 'request_start_time' => date("Y-m-d H:i:s")));
                // $walker = Walker::find($request_meta->walker_id);
                // $settings = Settings::where('key', 'provider_timeout')->first();
                // $time_left = $settings->value;
                // Send Notification
                $settings = Settings::where('key', 'provider_timeout')->first();
                $time_left = $settings->value;

                // Send Notification
                $walker = Walker::find($first_walker_id);
                if ($walker) {
                    Walker::where('id', '=', $request_meta->walker_id)->update(array('is_available' => 0));
                    $msg_array = array();
                    $msg_array['unique_id'] = 1;
                    $msg_array['request_id'] = $request->id;
                    $msg_array['time_left_to_respond'] = $time_left;
                    $msg_array['payment_mode'] = 0;
                    $owner = Owner::find($owner_id);
                    $request_data = array();
                    $request_data['owner'] = array();
                    $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                    $request_data['owner']['picture'] = $owner->picture;
                    $request_data['owner']['phone'] = $owner->phone;
                    $request_data['owner']['address'] = $owner->address;
                    $request_data['owner']['latitude'] = $request->origin_latitude;
                    $request_data['owner']['longitude'] = $request->origin_longitude;
                    $request_data['owner']['d_latitude'] = $request->D_latitude;
                    $request_data['owner']['d_longitude'] = $request->D_longitude;
                    $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                    $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                    $request_data['owner']['payment_type'] = 0;
                    $request_data['owner']['rating'] = $owner->rate;
                    $request_data['owner']['num_rating'] = $owner->rate_count;

                    $msg_array['request_data'] = $request_data;
                    $title = "New High Priority Request";
                    $message = $msg_array;
                    Log::info('response = ' . print_r($message, true));
                    Log::info('first_walker_id = ' . print_r($first_walker_id, true));
                    Log::info('New High Priority Request = ' . print_r($message, true));
                    /* don't do json_encode in above line because if */
                    send_notifications($first_walker_id, "walker", $title, $message);
                }
                $response_array = array('success' => true, 'success' => 'Push Notification for on demand Sent to Captain with unique id 1');
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function call_scheduler()
    {
        try {
            $query = "SELECT m.* from monthly m WHERE TIME_TO_SEC(TIMEDIFF(trip_one_pickup_time,CURTIME()))/60 >=55 and TIME_TO_SEC(TIMEDIFF(trip_one_pickup_time,CURTIME()))/60 <=61 and starting_date <= NOW() and is_full_month = 1";
            Log::info('Query' . $query);
            $resultsMonthly = DB::select(DB::raw($query));
            // Log::info('$resultsMonthly = ' . print_r($resultsMonthly, true));
            if (count($resultsMonthly) > 0) {

                Log::info('Count of monthly' . count($resultsMonthly));
                foreach ($resultsMonthly as $key) {
                    $requestId = $key->request_id;
                    $resultsRequest = Requests::where('id', $requestId)->first();
                    $requestServices = RequestServices::where('request_id', $requestId)->first();

                    $request = new Requests;

                    $request->monthly_id = $key->id;
                    $request->owner_id = $resultsRequest->owner_id;
                    $request->service_type = 1;
                    $request->status = 0;
                    $request->confirmed_walker = $resultsRequest->confirmed_walker;
                    $request->current_walker = 0;
                    $request->request_start_time = time();//curent time
                    $request->request_timestamp = time();
                    $request->created_at = time();
                    $request->updated_at = time();
                    $request->is_walker_started = 0;
                    $request->is_walker_arrived = 0;
                    $request->is_started = 0;
                    $request->is_completed = 0;
                    $request->is_dog_rated = 0;
                    $request->is_walker_rated = 0;
                    $walker = Walker::where('id', $resultsRequest->confirmed_walker)->first();
                    $distance = distanceGeoPoints($key->trip_one_to_lat, $key->trip_one_to_long, $walker->latitude, $walker->longitude);
                    $request->distance = $distance / 1000;;
                    $request->time = 0;
                    $request->total = 0;
                    $request->is_paid = 0;
                    $request->card_payment = 0;
                    $request->ledger_payment = 0.0;
                    $request->is_cancelled = 0;
                    $request->refund = 0;
                    $request->transfer_amount = 0;
                    $request->later = 0;
                    $request->origin_latitude = $resultsMonthly->trip_one_from_lat;
                    $request->origin_longitude = $resultsMonthly->trip_one_from_long;
                    $request->D_latitude = $key->trip_one_to_lat;
                    $request->D_longitude = $key->trip_one_to_long;

                    /*$request->security_key=0;
                    $request->payment_mode=0;
                    $request->payment_id="";
                    $request->promo_payment=0.0;
                    $request->promo_code="";
                    $request->promo_id=0;
                    $request->customer_waiting_time=0;*/
                    /*$request->arrival_timestamp=0;
                    $request->pickup_timestamp=0;*/
                    $request->save();

                    $requestMeta = new RequestMeta();
                    $requestMeta->request_id = $request->id;
                    $requestMeta->walker_id = $request->confirmed_walker;
                    $requestMeta->status = 0;
                    $requestMeta->save();

                    $requestId = $request->id;
                    $reqserv = new RequestServices;
                    $reqserv->request_id = $request->id;
                    $reqserv->type = $requestServices->type;
                    $reqserv->save();

                    $request = Requests::find($requestId);
                    $monthly = Monthly::find($key->id);
                    $owner_id = $request->owner_id;
                    $owner = Owner::find($owner_id);
                    $walker_id = $request->confirmed_walker;
                    $walker = Walker::find($walker_id);
                    $msg_array = array();
                    $msg_array['id'] = $walker_id;
                    $msg_array['unique_id'] = 19;
                    $msg_array['time_left_to_respond'] = 10;
                    $request_data = array();
                    $request_data['owner'] = array();
                    $request_data['request_id'] = $request->id;
                    $request_data['owner']['name'] = $owner->first_name . " " . $owner->last_name;
                    $request_data['owner']['picture'] = $owner->picture;
                    $request_data['owner']['phone'] = $owner->phone;
                    $request_data['owner']['address'] = $owner->address;
                    $request_data['owner']['latitude'] = $owner->latitude;
                    $request_data['owner']['longitude'] = $owner->longitude;
                    $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                    $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                    $request_data['owner']['payment_type'] = $request->payment_mode;
                    $request_data['owner']['rating'] = $owner->rate;
                    $request_data['owner']['num_rating'] = $owner->rate_count;
                    $request_data['dog'] = array();
                    $request_data['owner']["starting_date"] = "2016-11-07";
                    $request_data['owner']["subscription_type"] = $request->service_type;
                    $request_data['owner']["total_cost"] = $request->total;
                    $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                    $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                    $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                    $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                    $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                    $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                    $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                    $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                    $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                    $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                    $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                    $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                    $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                    $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                    $request_data['owner']["trip_type"] = $monthly->cab_type;

                    $msg_array['payment_mode'] = $request->payment_mode;
                    $msg_array['monthly_id'] = $monthly->id;
                    $msg_array['request_data'] = $request_data;
                    //$title = "New Monthly Subscription Request";
                    $title = "Customer " . $owner->first_name . "'s subscription will be in an hour today. Please confirm your availability?";
                    $message = $msg_array;
                    Log::info('response = ' . print_r($message, true));
                    Log::info('New request = ' . print_r($message, true));
                    send_notifications($walker_id, "walker", $title, $message);
                }

            }
        } catch (Exception $e) {
            Log::info('try = ' . $e->getMessage());
        }
    }


    // respond Join by Captain
    public function respond_join()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $join_id = Input::get('join_id');
        $accepted = Input::get('accepted');
        Log::info($walker_id);

        $validator = Validator::make(
            array(
                'token' => $token,
                'owner_id' => $walker_id,
                'join_id' => $join_id
            ), array(
                'token' => 'required',
                'owner_id' => 'required|integer',
                'join_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {
                    /* $currency_selected = Keywords::find(5); */
                    $join = Join::find($join_id);
                    if (ISSET($join)) {
                        $monthly = Monthly::find($join->base_monthly_id);
                        if($monthly->status!=1){
                            $response_array = array('success' => false, 'error' => 'Request is not available', 'error_code' => 472);
                            $response_code = 200;
                            $response = Response::json($response_array, $response_code);
                            return $response;
                        }
                        if ($accepted == 1) {
                            $monthly = Monthly::find($join->base_monthly_id);
                            $request_meta = new RequestMeta;
                            $request_meta->request_id = $monthly->request_id;
                            $request_meta->walker_id = $walker_id;
                            $request_meta->status = 1;
                            $request_meta->save();
                            $join->status = 1;
                            $join->save();
                            $request_id = $monthly->request_id;


                            $trip_address = array();
                            $trip_address['trip_one_from_address'] = $monthly->trip_one_from_address;
                            $trip_address['trip_one_to_address'] = $monthly->trip_one_to_address;
                            $trip_address['trip_one_pickup_time'] = $monthly->trip_one_pickup_time;
                            $trip_address['trip_two_pickup_time'] = $monthly->trip_two_pickup_time;
                            $trip_address['trip_two_to_address'] = $monthly->trip_two_to_address;
                            $trip_address['trip_two_from_address'] = $monthly->trip_two_from_address;
                            $trip_address['starting_date'] = $monthly->starting_date;
                            $trip_address['gender'] = $monthly->gender;

                            $request = Requests::where('id', $request_id)->first();

                            // Send Notification
                            $walker = Walker::find($walker_id);
                            $walker_data = array();
                            $walker_data['walker_id'] = $walker->walker_id;
                            $walker_data['first_name'] = $walker->first_name;
                            $walker_data['last_name'] = $walker->last_name;
                            $walker_data['phone'] = $walker->phone;
                            $walker_data['bio'] = $walker->bio;
                            $walker_data['picture'] = $walker->picture;
                            $walker_data['latitude'] = $walker->latitude;
                            $walker_data['longitude'] = $walker->longitude;
                            $walker_data['type'] = $walker->type;
                            $walker_data['rate'] = $walker->rate;
                            $walker_data['num_rating'] = $walker->rate_count;
                            $walker_data['car_model'] = $walker->car_model;
                            $walker_data['car_number'] = $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number;
                            /* $walker_data['rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->avg('rating') ? : 0;
                              $walker_data['num_rating'] = DB::table('review_walker')->where('walker_id', '=', $walker->id)->count(); */

                            $settings = Settings::where('key', 'default_distance_unit')->first();
                            $unit = $settings->value;
                            if ($unit == 0) {
                                $unit_set = 'kms';
                            } elseif ($unit == 1) {
                                $unit_set = 'miles';
                            }
                            $bill = array();

                            if ($request->is_completed == 1) {
                                $bill['distance'] = (string)convert($request->distance, $unit);
                                $bill['unit'] = $unit_set;
                                $bill['time'] = $request->time;
                                //$bill['base_price'] = $request->base_price;
                                //$bill['distance_cost'] = $request->distance_cost;
                                //$bill['time_cost'] = $request->time;
                                $bill['total'] = $request->total;
                                $bill['is_paid'] = $request->is_paid;
                            }

                            $response_array = array(
                                'success' => true,
                                'request_id' => $request_id,
                                'status' => $request->status,
                                'confirmed_walker' => $request->confirmed_walker,
                                'is_walker_started' => $request->is_walker_started,
                                'is_walker_arrived' => $request->is_walker_arrived,
                                'is_walk_started' => $request->is_started,
                                'is_completed' => $request->is_completed,
                                'is_walker_rated' => $request->is_walker_rated,
                                'walker' => $walker_data,
                                'bill' => $bill,
                                'monthly_id' => $monthly->id,
                                'new_request_status' => (string)$monthly->status,
                                'montlhy_or_daily' => (string)$request->service_type
                            );
                            $walkerFName = $walker->first_name;
                            $walkerLName = $walker->last_name;

                            $title = "Captain " . $walkerFName . " " . $walkerLName . " has accepted your monthly contract request, Please confirm the ride";

                            $message = $response_array;

                            send_notifications($request->owner_id, "owner", $title, $message);
                            $response_array = array('success' => true, 'message' => 'join Confirmed Successfully',
                                'join_id' => $join->id,
                                'monthly_id' => $join->monthly_id,
                                'status' => $accepted);

                            $response_code = 200;
                        } else {
                            Join::where('id', '=', $join_id)->update(array('is_cancelled' => 1));
                            $response_array = array('success' => true, 'message' => 'Invitation Cancelled Successfully');
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Invalid subscription ID', 'error_code' => 402);
                    }

                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'User Not Found', 'error_code' => 402);
            }
            $response_code = 200;
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    // Get Joins
    public function join_in_progress()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
            'token' => 'required',
            'walker_id' => 'required|integer'
        ));

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);


            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry)) {
                    $joins = DB::table('joins')->where('joins.walker_id', '=', $walker_id)->where('joins.status', '=', 0)->where('joins.is_cancelled', '=', 0)->join('owner', 'owner.id', '=', 'joins.owner_id')->orderBy('joins.id', 'desc')->get(['joins.*', 'owner.first_name', 'owner.last_name']);
                    $pusharr = array();
                    foreach ($joins as $join) {
                        $data['join_id'] = $join->id;
                        $data['monthly_id'] = $join->monthly_id;
                        $data['owner_id'] = $join->owner_id;
                        $data['walker_id'] = $join->walker_id;
                        $data['status'] = $join->status;
                        $data['is_cancelled'] = $join->is_cancelled;
                        $data['owner_name'] = $join->first_name . " " . $join->last_name;
                        $monthly = Monthly::find($join->monthly_id);
                        Log::info('$join->monthly_id' . $join->monthly_id);
                        $request = Requests::find($monthly->request_id);
                        $inviter = Owner::find($join->owner_id);
                        $request_data = array();
                        $request_data['owner'] = array();
                        if (isset($request)) {
                            $request_data['request_id'] = $request->id;
                            $request_data['owner']["subscription_type"] = $request->service_type;
                            $request_data['payment_mode'] = $request->payment_mode;
                            $request_data['confirmed_walker'] = $request->confirmed_walker;
                            $request_data['owner']['owner_dist_lat'] = $request->D_latitude;
                            $request_data['owner']['owner_dist_long'] = $request->D_longitude;
                            $request_data['owner']['payment_type'] = $request->payment_mode;
                        }
                        $timenow = time();
                        $jio = strtotime($join->created_at);
                        $diff = $timenow - $jio;
                        $request_data['time_left_to_respond'] = 120 - $diff;
                        if ($request_data['time_left_to_respond'] < 5) {
                            continue;
                        }
                        $request_data['owner']['name'] = $inviter->first_name . " " . $inviter->last_name;
                        $request_data['owner']['picture'] = $inviter->picture;
                        $request_data['owner']['phone'] = $inviter->phone;
                        $request_data['owner']['address'] = $inviter->address;
                        $request_data['owner']['latitude'] = $inviter->latitude;
                        $request_data['owner']['longitude'] = $inviter->longitude;
                        $request_data['owner']['rating'] = $inviter->rate;
                        $request_data['owner']['num_rating'] = $inviter->rate_count;
                        $request_data['dog'] = array();
                        $request_data['owner']["starting_date"] = "2016-11-07";
                        $request_data['owner']["total_cost"] = $monthly->cost;
                        $request_data['owner']["trip_one_from_address"] = $monthly->trip_one_from_address;
                        $request_data['owner']["trip_one_from_latitude"] = $monthly->trip_one_from_lat;
                        $request_data['owner']["trip_one_from_longitude"] = $monthly->trip_one_from_long;
                        $request_data['owner']["trip_one_pickup_time"] = $monthly->trip_one_pickup_time;
                        $request_data['owner']["trip_one_to_address"] = $monthly->trip_one_to_address;
                        $request_data['owner']["trip_one_to_latitude"] = $monthly->trip_one_to_lat;
                        $request_data['owner']["trip_one_to_longitude"] = $monthly->trip_one_to_long;
                        $request_data['owner']["trip_two_from_address"] = $monthly->trip_two_from_address;
                        $request_data['owner']["trip_two_from_latitude"] = $monthly->trip_two_from_lat;
                        $request_data['owner']["trip_two_from_longitude"] = $monthly->trip_two_from_long;
                        $request_data['owner']["trip_two_pickup_time"] = $monthly->trip_two_pickup_time;
                        $request_data['owner']["trip_two_to_address"] = $monthly->trip_two_to_address;
                        $request_data['owner']["trip_two_to_latitude"] = $monthly->trip_two_to_lat;
                        $request_data['owner']["trip_two_to_longitude"] = $monthly->trip_two_to_long;
                        $request_data['owner']["trip_type"] = $monthly->cab_type;
                        $request_data['success'] = 1;
                        $request_data['join_id'] = $join->id;
                        $data['request_data'] = $request_data;
                        array_push($pusharr, $data);
                    }
                    $response_array = array('success' => true, 'joins' => $pusharr, 'success_code' => 200);
                } else {
                    if ($is_admin) {
                        /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                        $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                }

            } else {
                $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
            }
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function add_feedback()
    {
        Log::info('add feedback');
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $device_type = Input::get('device_type');
        $message = Input::get('message');
        $version = Input::get('version');
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'message' => $message,
                'version' => $version,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'message' => 'required',
                'version' => 'required'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        } else {
            $is_admin = $this->isAdmin($token);

            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    $feedback = new Feedback();
                    $feedback->user_id = $walker_id;
                    $feedback->customer_type = 'walker';
                    $feedback->message = $message;
                    $feedback->device_type = $device_type;
                    $feedback->version = $version;
                    $feedback->save();
                    $response_array = array('success' => true, 'feedback_id' => $feedback->id, 'label' => $message, 'message' => 'Feedback saved');
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);

                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
    public function get_completed_delivery_requests()
    {
        $walker_id = Input::get('id');
        $token = Input::get('token');
        $from = Input::get('from_date'); // 2015-03-11 07:45:01
        $to_date = Input::get('to_date'); //2015-03-11 07:45:01

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    if ($from != "" && $to_date != "") {
                        $request_data = DB::table('delivery_request')
                            ->where('delivery_request.confirmed_walker', $walker_id)
                            ->where('delivery_request.is_completed', 1)
                            ->where('delivery_request.request_start_time', '>', $from)
                            ->where('delivery_request.request_start_time', '<', $to_date)
                            ->leftJoin('company', 'delivery_request.company_id', '=', 'company.id')
                            ->select('delivery_request.*','delivery_request.id as devid', 'delivery_request.request_start_time', 'company.*')
                            ->groupBy('delivery_request.id')
                            ->get();
                    } else {
                        $request_data = DB::table('delivery_request')
                            ->where('delivery_request.confirmed_walker', $walker_id)
                            ->where('delivery_request.is_completed', 1)
                            ->leftJoin('company', 'delivery_request.company_id', '=', 'company.id')
                            ->select('delivery_request.*','delivery_request.id as devid', 'company.*')
                            ->groupBy('delivery_request.id')
                            ->get();
                    }
                    $requests = array();
                    foreach ($request_data as $data) {
                        $msg_array = array();
                        $msg_array['unique_id'] = 51;
                        $msg_array['delivery_request_id'] = $data->devid;
                        $msg_array['request_start_time'] = $data->request_start_time;
                        $msg_array['receiver_name'] = $data->receiver_name;
                        $msg_array['receiver_phone'] = $data->receiver_phone;
                        $msg_array['small_items'] = $data->small_items;
                        $msg_array['medium_items'] = $data->medium_items;
                        $msg_array['large_items'] = $data->large_items;
                        $msg_array['cash_on_delivery'] = $data->cash_on_delivery;
                        $msg_array['transportation_fees'] = $data->transportation_fees;
                        $msg_array['transportation_fees_payed_by'] = $data->transportation_fees_payed_by;
                        $msg_array['origin_latitude'] = $data->origin_latitude;
                        $msg_array['origin_longitude'] = $data->origin_longitude;
                        $msg_array['D_latitude'] = $data->D_latitude;
                        $msg_array['D_longitude'] = $data->D_longitude;
                        $msg_array['status'] = $data->status;
                        $msg_array['is_walker_started'] = $data->status;
                        $msg_array['is_walker_arrived'] = $data->status;
                        $msg_array['is_started'] = $data->status;
                        $msg_array['is_completed'] = $data->status;
                        // add company details here
                        $msg_array['company'] = array();
                        $msg_array['company'] ['name'] = $data->name;
                        $msg_array['company'] ['phone'] = $data->phone;
                        $msg_array['company'] ['email'] = $data->email;
                        $msg_array['company'] ['products_type'] = $data->products_type;
                        $msg_array['company'] ['city'] = $data->city;
                        $msg_array['company'] ['latitude'] = $data->latitude;
                        $msg_array['company'] ['longitude'] = $data->longitude;
                        $msg_array['company'] ['address'] = $data->address;
                        $msg_array['company'] ['rating'] = $data->rate;
                        $msg_array['company'] ['num_rating'] = $data->rate_count;
                        $msg_array['company'] ['picture'] = $data->picture;
                        array_push($requests, $msg_array);
                    }

                    $response_array = array(
                        'success' => true,
                        'delivery_requests' => $requests
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }
    
    public function get_delivery_request()
    {
        $walker_id = Input::get('id');
        $token = Input::get('token');
        $delivery_request_id = Input::get('delivery_request_id');

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
                'delivery_request_id' => $delivery_request_id
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
                'delivery_request_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getWalkerData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $request_data = DB::table('delivery_request')
                        ->where('delivery_request.confirmed_walker', $walker_id)
                        ->where('delivery_request.id', $delivery_request_id)
                        ->leftJoin('company', 'delivery_request.company_id', '=', 'company.id')
                        ->select('delivery_request.*','delivery_request.id as devid', 'company.*')
                        ->get();
                    $msg_array = array();
                    foreach ($request_data as $data) {
                        $msg_array['delivery_request_id'] = $data->devid;
                        $msg_array['request_start_time'] = $data->request_start_time;
                        $msg_array['receiver_name'] = $data->receiver_name;
                        $msg_array['receiver_phone'] = $data->receiver_phone;
                        $msg_array['small_items'] = $data->small_items;
                        $msg_array['medium_items'] = $data->medium_items;
                        $msg_array['large_items'] = $data->large_items;
                        $msg_array['cash_on_delivery'] = $data->cash_on_delivery;
                        $msg_array['transportation_fees'] = $data->transportation_fees;
                        $msg_array['transportation_fees_payed_by'] = $data->transportation_fees_payed_by;
                        $msg_array['origin_latitude'] = $data->origin_latitude;
                        $msg_array['origin_longitude'] = $data->origin_longitude;
                        $msg_array['D_latitude'] = $data->D_latitude;
                        $msg_array['D_longitude'] = $data->D_longitude;
                        $msg_array['status'] = $data->status;
                        $msg_array['is_walker_started'] = $data->is_walker_started;
                        $msg_array['is_walker_arrived'] = $data->is_walker_arrived;
                        $msg_array['is_started'] = $data->is_started;
                        $msg_array['is_completed'] = $data->is_completed;
                        // add company details here
                        $msg_array['company'] = array();
                        $msg_array['company'] ['name'] = $data->name;
                        $msg_array['company'] ['phone'] = $data->phone;
                        $msg_array['company'] ['email'] = $data->email;
                        $msg_array['company'] ['products_type'] = $data->products_type;
                        $msg_array['company'] ['city'] = $data->city;
                        $msg_array['company'] ['latitude'] = $data->latitude;
                        $msg_array['company'] ['longitude'] = $data->longitude;
                        $msg_array['company'] ['address'] = $data->address;
                        $msg_array['company'] ['rating'] = $data->rate;
                        $msg_array['company'] ['num_rating'] = $data->rate_count;
                        $msg_array['company'] ['picture'] = $data->picture;
                    }
                    $response_array = array(
                        'success' => true,
                        'delivery_request' => $msg_array
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 1)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.Provider') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function setdefaultcard()
    {
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $card_token = Input::get('card_id');

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'card' => $card_token
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'card' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    Payment::where('walker_id', $walker_id)->update(array('is_default' => 0));
                    Payment::where('walker_id', $walker_id)->where('id', $card_token)->update(array('is_default' => 1));

                    $payments = array();
                    $card_count = DB::table('payment')->where('walker_id', '=', $walker_id)->count();
                    if ($card_count) {
                        $paymnt = Payment::where('walker_id', $walker_id)->orderBy('is_default', 'DESC')->get();
                        foreach ($paymnt as $data1) {
                            $default = $data1->is_default;
                            if ($default == 1) {
                                $data['is_default_text'] = "default";
                            } else {
                                $data['is_default_text'] = "not_default";
                            }
                            $data['walker_id'] = $data1->walker_id;
                            $data['last_four'] = $data1->last_four;
                            $data['card_token'] = $data1->card_token;
                            $data['card_type'] = $data1->card_type;
                            $data['card_id'] = $data1->id;
                            $data['is_default'] = $default;
                            array_push($payments, $data);
                        }
                        $response_array = array(
                            'success' => true,
                            'payments' => $payments
                        );
                    } else {
                        $response_array = array(
                            'success' => false,
                            'error' => 'No Card Found'
                        );
                    }
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function getCaptainData($walker_id, $token, $is_admin)
    {

        if ($walker_data = Walker::where('token', '=', $token)->where('id', '=', $walker_id)->first()) {
            return $walker_data;
        } elseif ($is_admin) {
            $walker_data = Walker::where('id', '=', $walker_id)->first();
            if (!$walker_data) {
                return false;
            }
            return $walker_data;
        } else {
            return false;
        }

    }

    public function deleteCardTokenCaptain()
    {
        $card_id = Input::get('card_id');
        $token = Input::get('token');
        $walker_id = Input::get('id');

        $validator = Validator::make(
            array(
                'card_id' => $card_id,
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'card_id' => 'required',
                'token' => 'required',
                'walker_id' => 'required|integer'
            )
        );

        /* $var = Keywords::where('id', 2)->first(); */

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    if ($payment = Payment::find($card_id)) {
                        if ($payment->walker_id == $walker_id) {

                            $pdn = Payment::where('id', $card_id)->first();
                            $check = trim($pdn->is_default);
                            Payment::find($card_id)->delete();
                            if ($check == 1) {
                                $card_count = DB::table('payment')->where('walker_id', '=', $walker_id)->count();
                                if ($card_count) {
                                    $paymnt = Payment::where('walker_id', $walker_id)->first();
                                    $paymnt->is_default = 1;
                                    $paymnt->save();
                                }
                            }

                            $payments = array();
                            $card_count = DB::table('payment')->where('walker_id', '=', $walker_id)->count();
                            if ($card_count) {
                                $paymnt = Payment::where('walker_id', $walker_id)->orderBy('is_default', 'DESC')->get();
                                foreach ($paymnt as $data1) {
                                    $default = $data1->is_default;
                                    if ($default == 1) {
                                        $data['is_default_text'] = "default";
                                    } else {
                                        $data['is_default_text'] = "not_default";
                                    }
                                    $data['walker_id'] = $data1->walker_id;
                                    $data['last_four'] = $data1->last_four;
                                    $data['card_token'] = $data1->card_token;
                                    $data['card_type'] = $data1->card_type;
                                    $data['card_id'] = $data1->id;
                                    $data['is_default'] = $default;
                                    array_push($payments, $data);
                                }
                                $response_array = array(
                                    'success' => true,
                                    'payments' => $payments,
                                );
                                $response_code = 200;
                            } else {
                                $response_code = 200;
                                $response_array = array(
                                    'success' => true,
                                    'error' => 'No Card Found',
                                    'error_code' => 541,
                                );
                            }
                        } else {
                            /* $response_array = array('success' => false, 'error' => 'Card ID and ' . $var->keyword . ' ID Doesnot matches', 'error_code' => 440); */
                            $response_array = array('success' => false, 'error' => 'Card ID and Captain ID does not matches', 'error_code' => 440);
                            $response_code = 200;
                        }
                    } else {
                        $response_array = array('success' => false, 'error' => 'Card not found', 'error_code' => 441);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function update_credit()
    {
        //handles get request
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $amount = Input::get('amount');
        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
                'amount' => $amount,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer',
                'amount' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    $walker = walker::find($walker_id);
                    $walker->credit = $walker->credit + $amount;
                    $walker->save();
                    $walker = walker::find($walker_id);

                    $response_array = array(
                        'success' => true,
                        'credit' => $walker->credit,
                    );
                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => '' . Config::get('app.generic_keywords.User') . ' ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function addCardTokenCaptain()
    {
        Log::info('addcardtoken');
        $payment_token = Input::get('payment_token');
        $last_four = Input::get('last_four');
        $token = Input::get('token');
        $walker_id = Input::get('id');
        $payfort_ref_id = Input::get('payfort_ref_id');
        if (Input::has('card_type')) {
            $card_type = strtoupper(Input::get('card_type'));
        } else {
            $card_type = strtoupper("VISA");
        }
        $validator = Validator::make(
            array(
                'last_four' => $last_four,
                'payment_token' => $payment_token,
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'last_four' => 'required',
                'payment_token' => 'required',
                'token' => 'required',
                'walker_id' => 'required|integer'
            )
        );
        $payments = array();

        if ($validator->fails()) {
            $error_messages = $validator->messages();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'payments' => $payments);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {
                // check for token validity
                /*if ($walker_data->email != "payfort@kasper-cab.com" && $walker_data->email != "fahed@kc.com" && $walker_data->email != "umar@kasper-cab.com" && $walker_data->email != "zeeshan@kasper-cab.com") {
                    $response_array = array('success' => false, 'error' => 'Card Payments are not allowed Yet. Please use cash.', 'error_code' => 411);
                    $response_code = 200;
                    $response = Response::json($response_array, $response_code);
                    return $response;
                }*/
                if (is_token_active($walker_data->token_expiry) || $is_admin) {

                    try {

                        if (Config::get('app.default_payment') == 'payfort') {
                            $card_count = DB::table('payment')->where('walker_id', '=', $walker_id)->count();

                            $payment = new Payment;
                            $payment->walker_id = $walker_id;
                            $payment->customer_id = $payfort_ref_id;
                            $payment->last_four = $last_four;
                            $payment->card_type = $card_type;
                            $payment->card_token = $payment_token;
                            if ($card_count > 0) {
                                $payment->is_default = 0;
                            } else {
                                $payment->is_default = 1;
                            }
                            $payment->save();

                            $payment_data = Payment::where('walker_id', $walker_id)->orderBy('is_default', 'DESC')->get();
                            foreach ($payment_data as $data1) {
                                $default = $data1->is_default;
                                if ($default == 1) {
                                    $data['is_default_text'] = "default";
                                } else {
                                    $data['is_default_text'] = "not_default";
                                }
                                $data['id'] = $data1->id;
                                $data['walker_id'] = $data1->walker_id;
                                $data['customer_id'] = $data1->customer_id;
                                $data['last_four'] = $data1->last_four;
                                $data['card_token'] = $data1->card_token;
                                $data['card_type'] = $data1->card_type;
                                $data['card_id'] = $data1->card_token;
                                $data['is_default'] = $default;
                                array_push($payments, $data);
                            }
                            $response_array = array(
                                'success' => true,
                                'payments' => $payments,
                            );
                            $response_code = 200;
                        }
                    } catch (Exception $e) {
                        $response_array = array('success' => false, 'error' => $e, 'error_code' => 405);
                        $response_code = 200;
                    }
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    /* $var = Keywords::where('id', 2)->first();
                      $response_array = array('success' => false, 'error' => '' . $var->keyword . ' ID not Found', 'error_code' => 410); */
                    $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function getCardsCaptain()
    {

        $token = Input::get('token');
        $walker_id = Input::get('id');
        if (Input::has('card_id')) {
            $card_id = Input::get('card_id');
            Payment::where('walker_id', $walker_id)->update(array('is_default' => 0));
            Payment::where('walker_id', $walker_id)->where('id', $card_id)->update(array('is_default' => 1));
        }

        $validator = Validator::make(
            array(
                'token' => $token,
                'walker_id' => $walker_id,
            ), array(
                'token' => 'required',
                'walker_id' => 'required|integer'
            )
        );

        if ($validator->fails()) {
            $error_messages = $validator->messages()->all();
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
            $response_code = 200;
        } else {
            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {
                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    // Do necessary operations
                    $payments = array();
                    $card_count = DB::table('payment')->where('walker_id', '=', $walker_id)->count();
                    if ($card_count) {
                        $paymnt = Payment::where('walker_id', $walker_id)->orderBy('is_default', 'DESC')->get();
                        foreach ($paymnt as $data1) {
                            $default = $data1->is_default;
                            if ($default == 1) {
                                $data['is_default_text'] = "default";
                            } else {
                                $data['is_default_text'] = "not_default";
                            }
                            $data['walker_id'] = $data1->walker_id;
                            $data['last_four'] = $data1->last_four;
                            $data['card_token'] = $data1->card_token;
                            $data['card_type'] = $data1->card_type;
                            $data['card_id'] = $data1->id;
                            $data['is_default'] = $default;
                            array_push($payments, $data);
                        }
                        $response_array = array(
                            'success' => true,
                            'payments' => $payments
                        );
                    } else {
                        $response_array = array(
                            'success' => true,
                            'error' => 'No Card Found'
                        );
                    }


                    $response_code = 200;
                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function getcaptainshipmentsforstatus()
    {
        $walker_id = Input::get('id');
        $token = Input::get('token');
        $status = Input::get('status');

        $validator = Validator::make(
            array(
                'walker_id' => $walker_id,
                'token' => $token,
                'status' => $status
            ), array(
                'walker_id' => 'required|integer',
                'token' => 'required',
                'status' => 'required'
            )
        );


        if ($validator->fails()) {
            $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401);
            $response_code = 200;
        } else {

            $is_admin = $this->isAdmin($token);
            if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {

                // check for token validity
                if (is_token_active($walker_data->token_expiry) || $is_admin) {
                    if ($status == 5) {
                        $d_reqs = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->
                        where('confirmed_walker', '=', $walker_id)->where('is_money_received', '=', 0)->where('status', '=', $status)
                            ->get(['delivery_request.*', 'companies.company_name', 'companies.company_type']);
                    } else if ($status == 6) {
                        $d_reqs = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')->
                        where('confirmed_walker', '=', $walker_id)->where('status', '=', $status)
                            ->get(['delivery_request.*', 'companies.company_name', 'companies.company_type']);
                    }
                    if (count($d_reqs)) {
                        $items = array();
                        foreach ($d_reqs as $item) {
                            array_push($items, array(
                                'id' => $item->id,
                                'jollychic_id' => $item->jollychic,
                                'order_number' => isset($item->order_number) ? $item->order_number : '',
                                'cash_on_pickup' => $item->cash_on_pickup,
                                'pickup_city' => $item->pickup_city,
                                'pickup_district' => $item->pickup_district,
                                'pickup_address' => $item->pickup_address,
                                'pickup_latitude' => $item->pickup_latitude,
                                'pickup_longitude' => $item->pickup_longitude,
                                'sender_name' => $item->sender_name,
                                'sender_phone' => $item->sender_phone,
                                'sender_email' => $item->sender_email,
                                'sender_city' => $item->sender_city,
                                'cash_on_delivery' => $item->cash_on_delivery,
                                'receiver_name' => $item->receiver_name,
                                'receiver_phone' => $item->receiver_phone,
                                'receiver_phone2' => $item->receiver_phone2,
                                'receiver_state' => $item->d_state,
                                'receiver_city' => $item->d_city,
                                'receiver_zipcode' => $item->d_zipcode,
                                'receiver_district' => $item->d_district,
                                'receiver_address' => $item->d_address,
                                'receiver_address2' => isset($item->d_address2) ? $item->d_address2 : '',
                                'receiver_address3' => isset($item->d_address3) ? $item->d_address3 : '',
                                'receiver_lat' => $item->D_latitude,
                                'receiver_lng' => $item->D_longitude,
                                'company_name' => $item->company_name,
                                'weight' => $item->d_weight,
                                'quantity' => $item->d_quantity,
                                'shipment_description' => $item->d_description,
                                'scheduled_shipment_date' => isset($item->scheduled_shipment_date) ? $item->scheduled_shipment_date : '',
                                'type' => $item->company_type,
                                'timeslot' => isset($item->timeslot) ? $item->timeslot : '',
                                'status' => $item->status
                            ));
                        }

                        $response_array = array(
                            'success' => true,
                            'delivery_requests' => $items
                        );
                        $response_code = 200;

                    } else {
                        $response_array = array('success' => false, 'error' => 'Shipments not found', 'error_code' => 410);
                        $response_code = 200;
                    }


                } else {
                    $response_array = array('success' => false, 'error' => 'Token Expired', 'error_code' => 405);
                    $response_code = 200;
                }
            } else {
                if ($is_admin) {
                    $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                } else {
                    $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                }
                $response_code = 200;
            }
        }

        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function rank_captain(){


        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
       

        $city = $adminCities[0]->city;
        $from = Request::get('from');
        $to = Request::get('to');


       return View::make('rank_walker')
       ->with('city', $city)
        ->with('from', $from)
       ->with('to', $to)
       ->with('adminCities', $adminCities)
       ->with('eachCaptainRankingData', []);

   }

   public function rank_captain_post(){

         $admin_id = Session::get('admin_id');

        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
         $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $city = Request::get('city');
        $from = Request::get('from');
        $to = Request::get('to');

        $totalCaptainPoints = 0;

        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();

        if ($grouped->isEmpty()) {

            $checkgroup = '0';
        } else {

            $checkgroup = '1';

        }

        if($checkgroup == 0){
    
        // getting average frequency in specified city with specified period... 
        $frequencyQuery = 'select avg(frequency) as avgfrequency from (
select confirmed_walker,count(*) frequency from (select confirmed_walker,scheduled_shipment_date,count(*) from delivery_request where confirmed_walker <> 0 and scheduled_shipment_date is not null
and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city = "'.$city.'"
group by confirmed_walker,scheduled_shipment_date) a
group by confirmed_walker ) b';

}else{

     $frequencyQuery = 'select avg(frequency) as avgfrequency from (
select confirmed_walker,count(*) frequency from (select confirmed_walker,scheduled_shipment_date,count(*) from delivery_request where confirmed_walker <> 0 and scheduled_shipment_date is not null
and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")
group by confirmed_walker,scheduled_shipment_date) a
group by confirmed_walker ) b';
}

$averageFrequency = DB::select($frequencyQuery);

$averageFrequency = $averageFrequency[0]->avgfrequency;

// average total delivered shipments in specificied city and dates
if($checkgroup ==0){
$averageDeliveredShipmentsQuery = 'select avg(TotalDeliveredShipments) as avgdelivered from (
select confirmed_walker,count(*) TotalDeliveredShipments
from delivery_request
where confirmed_walker <> 0 and scheduled_shipment_date is not null
and new_shipment_date between  "'.$from.'" and  "'.$to.'" and d_city = "'.$city.'" and status = 5
group by confirmed_walker ) a';
}
else{
    $averageDeliveredShipmentsQuery = 'select avg(TotalDeliveredShipments) as avgdelivered from (
select confirmed_walker,count(*) TotalDeliveredShipments
from delivery_request
where confirmed_walker <> 0 and scheduled_shipment_date is not null
and new_shipment_date between  "'.$from.'" and  "'.$to.'" and d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '") and status = 5
group by confirmed_walker ) a';
}

$averageDelivered = DB::select($averageDeliveredShipmentsQuery);

$averageDelivered = $averageDelivered[0]->avgdelivered;


//getting active captains to get their frequecy and calculate their total frequency points
$activeCaptaindata = DB::select("select DISTINCT ActiveCaptains,w.frequency,w.success_ratio,w.nationality, w.first_name,w.last_name,w.phone,w.email from ( select DISTINCT planned_walker ActiveCaptains from delivery_request UNION select DISTINCT confirmed_walker ActiveCaptains from delivery_request UNION select DISTINCT current_walker ActiveCaptains from delivery_request ) A join walker w on w.id = ActiveCaptains where ActiveCaptains <> 0");

        foreach ($activeCaptaindata as $value) {

            $captainFreqPoints = 0;
            $saudiNationalPoint = 0;
            $totalCaptainPoints = 0;
            $captainNoSPoints = 0;
            $totalDeliveredIncreasePercent = 0;
            $howMuchPercent = 0;
            $captainRemoteFrequency = 0;
            $captainRemoteDeliveredPoints = 0;
            $rank = 'No Rank';

            if($checkgroup==0){

            $captainTotalDeliveredQuery = 'select count(*) CaptainTotalDeliveredShipments from delivery_request where confirmed_walker = "'.$value->ActiveCaptains.'" and scheduled_shipment_date is not null and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city = "'.$city.'" and status = 5';

             $captainTotalRemoteDeliveredQuery = 'select count(*) CaptainTotalRemoteDeliveredShipments from delivery_request, districts d where confirmed_walker = "'.$value->ActiveCaptains.'" and scheduled_shipment_date is not null and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city = "'.$city.'" and status = 5 and d.is_remote = 1';

            }
            else{

                $captainTotalDeliveredQuery = 'select count(*) CaptainTotalDeliveredShipments from delivery_request where confirmed_walker = "'.$value->ActiveCaptains.'" and scheduled_shipment_date is not null and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '") and status = 5';

                $captainTotalRemoteDeliveredQuery = 'select count(*) CaptainTotalRemoteDeliveredShipments from delivery_request, districts d where confirmed_walker = "'.$value->ActiveCaptains.'" and scheduled_shipment_date is not null and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '") and status = 5 and d.is_remote = 1';
            }


            $captainDelivered = DB::select($captainTotalDeliveredQuery);

            $captainDelivered = $captainDelivered[0]->CaptainTotalDeliveredShipments;

            if($captainDelivered != 0){ // if its zero division by zero error

            $numberOfDeliveredShipments = $captainDelivered - $averageDelivered;

            $totalDeliveredIncreasePercent = $numberOfDeliveredShipments/$averageDelivered*100;

                }


            // Number of Shipments Points
            if($totalDeliveredIncreasePercent >= 75){

                $captainNoSPoints = 0.05*$captainDelivered;
            }
            else if($totalDeliveredIncreasePercent<=74.99 && $totalDeliveredIncreasePercent>=50 ){
                $captainNoSPoints = 0.03*$captainDelivered;
            }
             else if(($totalDeliveredIncreasePercent <= 49.99) && ($totalDeliveredIncreasePercent >= 25)){
                $captainNoSPoints = 0.01*$captainDelivered;
            }
            else {
                $captainNoSPoints = 0;
            }


            $totalCaptainPoints += $captainNoSPoints;



            //Remote Points
            $captainRemoteDelivered = DB::select($captainTotalRemoteDeliveredQuery);

            $captainRemoteDelivered = $captainRemoteDelivered[0]->CaptainTotalRemoteDeliveredShipments;


             if($captainRemoteDelivered != 0){ // if its zero division by zero error

                $captainRemoteFrequency = $captainRemoteDelivered/$captainDelivered*100;

                }

                if($captainRemoteFrequency >= 75){

                $captainRemoteDeliveredPoints = 0.33*$captainRemoteDelivered;
            }
            else if($captainRemoteFrequency<=74.99 && $captainRemoteFrequency>=50 ){
                $captainRemoteDeliveredPoints = 0.22*$captainRemoteDelivered;
            }
             else if(($captainRemoteFrequency <= 49.99) && ($captainRemoteFrequency >= 25)){
                $captainRemoteDeliveredPoints = 0.11*$captainRemoteDelivered;
            }
            else {
                $captainRemoteDeliveredPoints = 0;
            }

            $totalCaptainPoints += $captainRemoteDeliveredPoints;
           
            // if captain is saudi national he got 9 points.
            if($value->nationality == 'Saudi'){
                $totalCaptainPoints += 9;

            }else{

                $totalCaptainPoints+=0;
            }

            $captainFreq = $value->frequency;

            if($captainFreq !=0 && $averageFrequency != NULL){
            $howMuchIncreased = $captainFreq - $averageFrequency;

            $howMuchPercent = $howMuchIncreased/$averageFrequency*100;

            }
             
             $successRateQuery = 'select (count(distinct (case when status = 5 then waybill_number end))/count(distinct (case when status = 4 then waybill_number end)))*100 "CaptainSuccessRate" from orders_history
where walker_id = "'.$value->ActiveCaptains.'" and waybill_number   in (select jollychic from delivery_request where scheduled_shipment_date is not null and new_shipment_date between "'.$from.'" and  "'.$to.'" and d_city = "'.$city.'") group by walker_id having count(distinct (case when status = 5 then waybill_number end)) > 0 ' ;

             $successRatePercent = DB::select($successRateQuery);
             
             if(!empty($successRatePercent)){
             $successRatePercent = $successRatePercent[0]->CaptainSuccessRate;
             }
             else{
                 
                 $successRatePercent = 0;
             }

            if($successRatePercent >= 75){

                $captainSuccessRatePoints = 0.2*$successRatePercent;
            }
            else if($successRatePercent<=74.99 && $successRatePercent>=50 ){
                $captainSuccessRatePoints = 0.1*$successRatePercent;
            }
             else if(($successRatePercent <= 49.99) && ($successRatePercent >= 25)){
                $captainSuccessRatePoints = 0.05*$successRatePercent;
            }
            else {
                $captainSuccessRatePoints = 0;
            }

            $totalCaptainPoints += $captainSuccessRatePoints;

            // Captain Frequency Points
            if($howMuchPercent >= 75){

                $captainFreqPoints = 0.33*$captainFreq;
            }
            else if($howMuchPercent<=74.99 && $howMuchPercent>=50 ){
                $captainFreqPoints = 0.22*$captainFreq;
            }
             else if(($howMuchPercent <= 49.99) && ($howMuchPercent >= 25)){
                $captainFreqPoints = 0.11*$captainFreq;
            }
            else {
                $captainFreqPoints = 0;
            }

             $totalCaptainPoints += $captainFreqPoints;


             // Set rank gol,silver etc depending upon total captain points

            if($totalCaptainPoints >= 90){

                $rank = 'Gold';
            }
            else if($totalCaptainPoints<=89.99 && $totalCaptainPoints>=80 ){
                $rank = 'Silver';
            }
             else if(($totalCaptainPoints <= 79.99) && ($totalCaptainPoints >= 70)){
                $rank = 'Bronze';
            }
            else {
                $rank = 'No Rank';
            }

            $eachCaptainFrequencyPoints[] = array('captainId' => $value->ActiveCaptains, 'first_name' => $value->first_name, 'last_name' => $value->last_name, 'phone' => $value->phone, 'email' => $value->email, 'totalpoints' => $totalCaptainPoints, 'rank' => $rank);

           
        }


        return View::make('rank_walker')
       ->with('city', $city)
       ->with('from', $from)
       ->with('to', $to)
       ->with('adminCities', $adminCities)
       ->with('eachCaptainRankingData', $eachCaptainFrequencyPoints);


   }

   public function captain_rating_comments(){



    try {
            $captain_id = Input::get('captain_id');
            $token = Input::get('token');
            $offset = Input::get('offset');

            if(!isset($offset)){

                $offset = 0;
            }

            if(is_numeric($offset) == false){

                $response_array = array(
                        'success' => false,
                        'message' => 'Please send offset as numeric value !', 
                    );
        
                     return $response = Response::json($response_array, 200);

            }

            // number of rows to show per page
            $rowsperpage = 25;

            $walker = Walker::where('id', '=', $captain_id)->where('token', '=', $token)->first();

            if($walker){

                $getWalkerRatingsQuery = 'SELECT DISTINCT c.waybill, d.order_number, c.captain_rating, c.captain_comments, d.receiver_name, c.created_at from captain_rating c, delivery_request d where captain_comments is not null and captain_id = '.$captain_id.' and c.waybill = d.jollychic limit '.$offset.', 25 ';

                $totalCountQuery = 'SELECT count(DISTINCT c.waybill) totalcount from captain_rating c, delivery_request d where captain_comments is not null and captain_id = '.$captain_id.' and c.waybill = d.jollychic';

                $totalCount = DB::select($totalCountQuery);

                

                $getWalkerRatings = DB::select($getWalkerRatingsQuery);

               
                 
                $response_array = array(
                        'success' => true,
                        'captain_ratings' => $getWalkerRatings,
                        'total_count' => $totalCount[0]->totalcount,
                    );
        
                     return $response = Response::json($response_array, 200);

            }

            else{
                 $response_array = array(
                        'success' => false,
                        'error' => 'Not a valid captain or token mismatch!',
                        'error_code' => 406 
                    );

                 return $response = Response::json($response_array, 200);
            }

    }
    catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
   }

    public function updatecaptain()
    {
        try {
            $walker_id = Input::get('id');
            $token = Input::get('token');
            $picture = Input::file('picture');
            $zipcode = Input::get('zipcode');


            $validator = Validator::make(
                array(
                    'walker_id' => $walker_id,
                    'token' => $token,
                    'picture' => $picture,
                    'zipcode' => $zipcode
                ), array(
                    'walker_id' => 'required',
                    'token' => 'required',
                    'picture' => 'mimes:jpeg,bmp,png',
                    'zipcode' => 'integer'
                )
            );
            if ($validator->fails()) {
                $error_messages = $validator->messages()->all();
                Log::info('Error while during captain registration = ' . print_r($error_messages, true));
                $response_array = array('success' => false, 'error' => 'Invalid Input', 'error_code' => 401, 'error_messages' => $error_messages);
                $response_code = 200;
            } else {
                $is_admin = $this->isAdmin($token);
                if ($walker_data = $this->getCaptainData($walker_id, $token, $is_admin)) {

                    $walker = Walker::find($walker_id);
                    //first_name Optional
                    if (Request::has('first_name')) {
                        $first_name = Input::get('first_name');
                        $walker->first_name = $first_name;
                    }
                    //last_name Optional
                    if (Request::has('last_name')) {
                        $last_name = Input::get('last_name');
                        $walker->last_name = $last_name;
                    }
                    /*//email Optional
                    if (Request::has('email')) {
                        $email = Input::get('email');
                        $walker->email = $email;
                    }*/
                    //phone Optional
                    if (Request::has('phone')) {
                        $phone = Input::get('phone');
                        if (Walker::where('phone', '=', $phone)->first()) {
                            //phone did not update
                        } else {
                            $walker->phone = $phone;
                        }
                    }
                    //sex Optional
                    if (Request::has('sex')) {
                        $sex = Input::get('sex');
                        $walker->sex = $sex;
                    }
                    //city Optional
                    if (Request::has('city')) {
                        $city = Input::get('city');
                        $walker->city = $city;
                    }
                    //nationality Optional
                    if (Request::has('nationality')) {
                        $nationality = Input::get('nationality');
                        $walker->nationality = $nationality;
                    }
                    //district Optional
                    if (Request::has('district')) {
                        $district = Input::get('district');
                        $walker->district = $district;
                    }
                    //address  Optional
                    if (Request::has('address')) {
                        $address = Input::get('address');
                        $address_latitude = Input::get('address_latitude');
                        $address_longitude = Input::get('address_longitude');

                        if (isset($address_latitude)) {
                            if (isset($address_longitude)) {
                                $walker->address = $address;
                                $walker->address_latitude = $address_latitude;
                                $walker->address_longitude = $address_longitude;
                            }
                        }

                    }
                    //picture optional
                    if (Input::hasFile('picture')) {
                        if ($walker->picture != "") {
                            $path = $walker->picture;
                            Log::info($path);
                            $filename = basename($path);
                            Log::info($filename);
                            if (file_exists($path)) {
                                unlink(public_path() . "/uploads/" . $filename);
                            }
                        }
                        // upload image
                        $file_name = time();
                        $file_name .= rand();
                        $file_name = sha1($file_name);

                        $ext = Input::file('picture')->getClientOriginalExtension();
                        Log::info('ext = ' . print_r($ext, true));
                        Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                        $local_url = $file_name . "." . $ext;

                        // Upload to S3

                        if (Config::get('app.s3_bucket') != "") {
                            $s3 = App::make('aws')->get('s3');
                            $pic = $s3->putObject(array(
                                'Bucket' => Config::get('app.s3_bucket'),
                                'Key' => $file_name,
                                'SourceFile' => public_path() . "/uploads/" . $local_url,
                            ));

                            $s3->putObjectAcl(array(
                                'Bucket' => Config::get('app.s3_bucket'),
                                'Key' => $file_name,
                                'ACL' => 'public-read'
                            ));

                            $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                        } else {
                            $s3_url = asset_url() . '/uploads/' . $local_url;
                        }

                        if (isset($walker->picture)) {
                            if ($walker->picture != "") {
                                $icon = $walker->picture;
                                unlink_image($icon);
                            }
                        }

                        $walker->picture = $s3_url;
                    }


                    //bio optional
                    if (Request::has('bio')) {
                        $bio = Input::get('bio');
                        $walker->bio = $bio;
                    }
                    //state optional
                    if (Request::has('state')) {
                        $state = Input::get('state');
                        $walker->state = $state;
                    }
                    //country optional
                    if (Request::has('country')) {
                        $country = Input::get('country');
                        $walker->country = $country;
                    }


                    //new_password optional
                    if (Request::has('new_password')) {
                        $new_password = Input::get('new_password');
                        if (Request::has('old_password')) {
                            $old_password = Input::get('old_password');
                            if (Hash::check($old_password, $walker->password)) {
                                $walker->password = Hash::make($new_password);
                            } else {
                                //incoorect old password
                            }
                        }
                    }

                    //zipcode optional
                    if (Request::has('zipcode')) {
                        $zipcode = Input::get('zipcode');
                        $walker->zipcode = $zipcode;
                    }



                    //car_model optional
                    if (Request::has('car_model')) {
                        $car_model = trim(Input::get('car_model'));
                        $walker->car_model = $car_model;
                    }
                    //car_number optional
                    if (Request::has('car_number')) {
                        $car_number = trim(Input::get('car_number'));
                        $walker->car_number = $car_number;
                    }

                    //car_registration_expiry Optional
                    if (Request::has('vehicle_registration_expiry')) {
                        $car_registration_expiry = Input::get('vehicle_registration_expiry');
                        $walker->car_registration_expiry = $car_registration_expiry;
                    }
                    //license_expiry_date Optional
                    if (Request::has('license_expiry_date')) {
                        $license_expiry_date = trim(Input::get('license_expiry_date'));
                        $walker->license_expiry_date = $license_expiry_date;
                    }
                    //vehicle_registration_scan Optional
                    if (Request::has('vehicle_registration_scan')) {
                        $vehicle_registration_scan = Input::get('vehicle_registration_scan');
                        $walker->vehicle_registration_scan = $vehicle_registration_scan;
                    }
                    // vehicle_front_scan  Optional
                    if (Request::has('vehicle_front_scan')) {
                        $vehicle_front_scan = Input::get('vehicle_front_scan');
                        $walker->vehicle_front_scan = $vehicle_front_scan;
                    }
                    // captain_license_scan Optional
                    if (Request::has('captain_license_scan')) {
                        $walker_license_scan = trim(Input::get('captain_license_scan'));
                        $walker->captain_license_scan = $walker_license_scan;
                    }
                    //captain_iqama_scan Optional
                    if (Request::has('captain_iqama_scan')) {
                        $walker_iqama_scan = Input::get('captain_iqama_scan');
                        $walker->captain_iqama_scan = $walker_iqama_scan;
                    }

                    //captain_confession Optional
                    if (Request::has('captain_confession')) {
                        $walker_confession = Input::get('captain_confession');
                        $walker->captain_confession = $walker_confession;
                    }


                    $walker->update();

                    $response_array = array(
                        'success' => true,
                        'id' => $walker->id,
                        'first_name' => $walker->first_name,
                        'last_name' => $walker->last_name,
                        'phone' => $walker->phone,
                        'email' => $walker->email,
                        'city' => $walker->city,
                        'address' => $walker->address,
                        'state' => $walker->state,
                        'country' => $walker->country,
                        'zipcode' => $walker->zipcode,
                        'bio' => $walker->bio,
                        'picture' => $walker->picture,
                        'social_unique_id' => $walker->social_unique_id,
                        'login_by' => $walker->login_by,
                        'token' => $walker->token,
                        'type' => $walker->type,
                        'is_approved' => $walker->is_approved,
                        'is_approved_txt' => 'Approved',
                        'is_available' => $walker->is_active,
                        'car_model' => $walker->car_model,
                        'car_number' => $walker->car_number_plate_letter_1 . " " . $walker->car_number_plate_letter_2 . " " . $walker->car_number_plate_letter_3 . " " . $walker->car_number_plate_number,
                        'vehicle_registration_scan' =>isset($walker->vehicle_registration_scan) ? $walker->vehicle_registration_scan : '',
                        'vehicle_front_scan' => isset($walker->vehicle_front_scan) ? $walker->vehicle_front_scan : '',
                        'captain_license_scan' => isset($walker->captain_license_scan) ? $walker->captain_license_scan : '',
                        'captain_iqama_scan' => isset($walker->captain_iqama_scan) ? $walker->captain_iqama_scan : '',
                        'captain_confession' => isset($walker->captain_confession) ? $walker->captain_confession : '',
                        'created_at' => date('Y-m-d H:i:s',strtotime($walker->created_at))
                    );
                    $response_code = 200;
                } else {
                    if ($is_admin) {
                        $response_array = array('success' => false, 'error' => 'Captain ID not Found', 'error_code' => 410);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Not a valid token', 'error_code' => 406);
                    }
                    $response_code = 200;
                }

            }
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {
            $response_array = array('success' => false,'error' => 'Failed, Please try again');
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }
   
}