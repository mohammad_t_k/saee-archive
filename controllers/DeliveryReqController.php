<?php

/*
 * handles delivery requests tracking and data manipulations
 */

use App\Classes\Curl;


class DeliveryReqController extends \BaseController
{
    public function saveitem()
    {
        try {
            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    return '0';
                }
            } else {
                return '0';
            }

            // is allowed to create shipments
            if (!$company->allow_creation) {
                return 'You are not allowed to create orders. Please contact admin.';
            }

            $company_id = $company->id;

            $delivery_request = new DeliveryRequest;

            //  company id Optional
            if (Request::has('company_id')) {
                $company_id = Input::get('company_id');
                $found = Companies::where('id', '=', $company_id)->first();
                if(!$found || $found->parent_id != $company->id)
                    return '0';
            }


            //  order number string Optional 
            if (Request::has('ordernumber')) {
                $ordernumber = Input::get('ordernumber');
                if (count(DeliveryRequest::where('order_number', '=', $ordernumber)->where('company_id', '=', $company_id)->get())) {
                    return ' order number ' . $ordernumber . ' already exists, Please try again.';
                }
                $delivery_request->order_number = $ordernumber;
            }

            $delivery_request->request_start_time = date("Y-m-d H:i:s");


            //  cashondelivery float Mandatory
            if (Request::has('cashondelivery')) {
                $cash_on_delivery = Input::get('cashondelivery');
            } else {
                return '0';
            }

            $delivery_request->cash_on_delivery = $cash_on_delivery;

            //  name string Mandatory
            if (Request::has('name')) {
                $dropoff_name = Input::get('name');
            } else {
                return '0';
            }

            $delivery_request->receiver_name = $dropoff_name;


            //  mobile ints Mandatory
            if (Request::has('mobile')) {
                $dropoff_mobile = Input::get('mobile');
		$dropoff_mobile = format_phone($dropoff_mobile);
                /*$mobilelength = strlen((string)$dropoff_mobile);
                if($mobilelength <= 11){
                    $dropoff_mobile = preg_replace('/^0/','966',$dropoff_mobile);
                }*/
            } else {
                return '0';
            }

            $delivery_request->receiver_phone = $dropoff_mobile;

            //  Email ints Mandatory
            $dropoff_email = "";
            if (Request::has('email')) {
                $dropoff_email = str_replace(PHP_EOL,' ',Input::get('email'));
            }
            $delivery_request->receiver_email = $dropoff_email;


            // mobile2 ints Optional
            if (Request::has('mobile2')) {
                $dropoff_mobile2 = Input::get('mobile2');
                $dropoff_mobile2 = format_phone($dropoff_mobile2);
                /*$mobile2length = strlen((string)$dropoff_mobile2);
                if($mobile2length <= 11){
                    $dropoff_mobile2 = preg_replace('/^0/','966',$dropoff_mobile2);
                }*/                $delivery_request->receiver_phone2 = $dropoff_mobile2;
            }


            //   streetaddress string Mandatory
            if (Request::has('streetaddress')) {
                $d_address = str_replace(PHP_EOL,' ',Input::get('streetaddress'));
                $d_address = trim(preg_replace('/\s+/',' ', $d_address));
                $delivery_request->d_address = $d_address;
            } else {
                return '0';
            }

            //  streetaddress2 string Optional
            if (Request::has('streetaddress2')) {
                $d_address2 = str_replace(PHP_EOL,'',Input::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/',' ', $d_address2));
                $delivery_request->d_address2 = $d_address2;
            }

            //  streetaddress3 string Optional
            if (Request::has('streetaddress3')) {
                $d_address3 = str_replace(PHP_EOL,'',Input::get('streetaddress3'));
                $d_address3 = trim(preg_replace('/\s+/',' ', $d_address3));
                $delivery_request->d_address3 = $d_address3;
            }


            //  city  string Mandatory
            if (Request::has('city')) {
                $d_city = Input::get('city');
                $map = CityMap::where('input', '=', $d_city)->first();
                if(isset($map))
                    $d_city = $map->output;
                $delivery_request->d_city = $d_city;
            } else {
                return '0';
            }

            //   district  string Optional
            if (Request::has('district')) {
                $d_district = Input::get('district');
                if (strpos($d_district, '+') !== false) {

                    $d_city = strtok($d_district, '+');

                    $d_district = strtok('');
                    $delivery_request->d_city = $d_city;
                }
                $delivery_request->d_district = $d_district;
            }

            //  state  string Optional
            if (Request::has('state')) {
                $d_state = Input::get('state');
                if($d_state == 'Makkah' || $d_state == 'Jeddah' || $d_state =='Taif'){

                    $d_state = 'Makkah{Mecca}';
                }

                if($d_state =='Baha'){

                    $d_state = 'Al Baha';
                }

                if($d_state =='Dammam'){

                    $d_state = 'Eastern Region {Ash-Sharqiyah}';
                }
                $delivery_request->d_state = $d_state;
            }



            //  zipcode ints Optional
            if (Request::has('zipcode')) {
                $d_zipcode = Input::get('zipcode');
                $delivery_request->d_zipcode = $d_zipcode;
            }


            //   latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Input::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            //  longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Input::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }


            //   weight float Mandatory
            if (Request::has('weight')) {
                $d_weight = Input::get('weight');
            } else {
                return '0';
            }

            $delivery_request->d_weight = $d_weight;

            // quantity ints Mandatory
            if (Request::has('quantity')) {
                $d_quantity = Input::get('quantity');
            } else {
                return '0';
            }

            $delivery_request->d_quantity = $d_quantity;

            //   description strings Optional
            if (Request::has('description')) {
                $d_description = Input::get('description');
                $delivery_request->d_description = $d_description;
            }

            //   sender name strings Optional
            if (Request::has('sendername')) {
                $sender_name = Input::get('sendername');
                $delivery_request->sender_name = $sender_name;
            } else {
                $delivery_request->sender_name = $company->company_name;
            }

            //   sender email strings Optional
            if (Request::has('senderemail')) {
                $sender_email = Input::get('senderemail');
                $delivery_request->sender_email = $sender_email;
            } else {
                $delivery_request->sender_email = $company->email;
            }

            //   sender phone strings Optional
            if (Request::has('senderphone')) {
                $sender_phone = Input::get('senderphone');
                $delivery_request->sender_phone = $sender_phone;
            } else {
                $delivery_request->sender_phone = $company->phone;
            }

            //   sender address strings Optional
            if (Request::has('senderaddress')) {
                $o_address = Input::get('senderaddress');
                $delivery_request->o_address = $o_address;
            } else {
                $delivery_request->o_address = $company->address;
            }

            //   sender city strings Optional
            if (Request::has('sendercity')) {
                $sender_city = Input::get('sendercity');
                $delivery_request->sender_city = $sender_city;
            } else {
                $delivery_request->sender_city = $company->city;
            }

            //   sender country strings Optional
            if (Request::has('sendercountry')) {
                $sender_country = Input::get('sendercountry');
                $delivery_request->sender_country = $sender_country;
            } else {
                $delivery_request->sender_country = $company->country;
            }

            $delivery_request->request_start_time = date("Y-m-d H:i:s");
            $delivery_request->company_id = $company_id;
            $delivery_request->service_type = 0; // regular order 
            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->origin_latitude = $company->latitude;
            $delivery_request->origin_longitude = $company->longitude;
            $delivery_request->pickup_city = $company->city;
            $delivery_request->pickup_district = $company->district;
            $delivery_request->pickup_address = $company->address;
            $delivery_request->pickup_latitude = $company->latitude;
            $delivery_request->pickup_longitude = $company->longitude;
            $delivery_request->status = 0;
            if($company_id == '952') {
                $delivery_request->pincode = "0000";
                $delivery_request->pincode2 = "0000";
            }
            else { 
                $delivery_request->pincode = mt_rand(1000, 9999);
                $delivery_request->pincode2 = mt_rand(1000, 9999);
            }
            $delivery_request->save();
            $waybill = 'OS' . str_pad($delivery_request->id,  8, "0", STR_PAD_LEFT).'KS';

            $dr = DeliveryRequest::find($delivery_request->id);

            $dr->waybill = $waybill;
            $dr->jollychic = $waybill;
            $dr->customer_tracking_number = $dropoff_mobile . '-' . $dr->id;
            $dr->save();

            $OrdersHistory = new OrdersHistory;

            $OrdersHistory->waybill_number = $waybill;
            $OrdersHistory->order_number = $dr->order_number;
            $OrdersHistory->status = 0;

            $OrdersHistory->company_id = $company_id;

            $OrdersHistory->save();

            return $waybill;


        } catch (Exception $e) {

            Log::info($e->getMessage());
            return '0';

        }
    }

    public function testm() {
        $waybill = 'OS00117464KS'; // dokkan 913
        $dr = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        $comapny = Companies::where('id', '=', 913)->first();
        $dropoff_mobile = "0504777517";
        
        $curl = new Curl();
        $sms = 'عزيزي عميل ';
        $company = Companies::find($dr->company_id);
        if (isset($company)) {
            if(!empty($dr->sender_name))
                $sender_name = $dr->sender_name;
            else
                $sender_name = $company->name_ar;
            $sms .= $sender_name . " ";
        }

 //        $sms .= "طلبكم في طريقه إلى ";
 //        $sms .= $dr->d_city . ' ';
 //        $sms .= "وسيتواصل معكم مندوب التوزيع في حيكم في أقرب وقت. ليصلكم الطلب أسرع فضلاً ادخل على الرابط ";
 //        $sms .= "http://saee.sa/trackingpage%3Ftrackingnum=$dr->jollychic ";
 //        $sms .= "لتسجيل موقع التوصيل ووقت التسليم مع تحيات ساعي";

 //        $destinations1 = "966";
 //        $destinations1 .= substr(str_replace(' ', '', $dropoff_mobile), -9);

 //        $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";
	// //return $url;

 //        $urlDiv = explode("?", $url);
 //        $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
 //        Log::info("SMSSERVICELOG" . $result);

 //        if (Request::has('mobile2') && $dropoff_mobile != $dropoff_mobile2) {
 //            $destinations2 = "966";
 //            $destinations2 .= substr(str_replace(' ', '', $dropoff_mobile2), -9);
 //            $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

 //            $urlDiv = explode("?", $url);
 //            $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
 //            Log::info("SMSSERVICELOG" . $result);
 //        }
        return '1';
    }

    public function shootaramex() {
        $results = DeliveryRequest::leftJoin(DB::raw('(select waybill_number, max(created_at) "created_at" from orders_history where status = 5 group by waybill_number) orders_history'), 'delivery_request.jollychic', '=', 'orders_history.waybill_number')
            // ->where('delivery_request.status', '=', '5')
            ->where('delivery_request.company_id', '=', 917)
            ->whereRaw('date(delivery_request.created_at) = "2018-12-04"')
            ->select(['delivery_request.company_id','delivery_request.status','delivery_request.jollychic','orders_history.created_at', 'delivery_request.order_number'])
            ->get();
        $timestamp = date('Y-m-d') . 'T' . date('H:i:s');
        $xml_data = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
        <InfoLinkDocument>
            <AccessRequest>
                <DocumentType>3</DocumentType>
                <Version>1.20</Version>
                <EntityID>28</EntityID>
                <EntityPIN>JED879</EntityPIN>
                <TimeStamp>$timestamp</TimeStamp>
                <Reference1 />
                <Reference2 />
                <Reference3 />
                <Reference4 />
                <Reference5 />
                <ReplyEmailAddress>info@saee.sa</ReplyEmailAddress>
                <NotifyOnSuccess>False</NotifyOnSuccess>
            </AccessRequest>";
            // foreach($results as $result) {
            //     $text = $result->created_at . '';
            //     $text = str_replace(' ', 'T', $text);
            //     $xml_data .= "<HAWBUpdate>
            //                 <HAWBNumber>$result->order_number</HAWBNumber>
            //                 <HAWBOriginEntity>JED</HAWBOriginEntity>
            //                 <UpdateEntity>28</UpdateEntity>
            //                 <PINumber>SH005</PINumber>
            //                 <ProblemCode>SHDL</ProblemCode>
            //                 <Pieces>1</Pieces>
            //                 <ActionDate>$text</ActionDate>
            //                 <Comment1 />
            //                 <Comment2 />
            //                 <SourceID>$result->source_id</SourceID>
            //                 </HAWBUpdate>";
            // }
            $xml_data .= "<HAWBUpdate>
            <HAWBNumber>32178365612</HAWBNumber>
            <HAWBOriginEntity>HKG</HAWBOriginEntity>
            <UpdateEntity>28</UpdateEntity>
            <PINumber>SH005</PINumber>
            <ProblemCode></ProblemCode>
            <Pieces>1</Pieces>
            <ActionDate>2018-12-12T14:23:53</ActionDate>
            <Comment1>روان أمين</Comment1>
            <Comment2 />
	        <BranchID>14208</BranchID>
            <SourceID>2</SourceID>
        </HAWBUpdate>";
        $xml_data .= "</InfoLinkDocument>";
        $login = 'Saee';
        $password = 'JaxDm679';
        $url = 'https://infolink.aramex.net/post.aspx';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        $result = curl_exec($ch);
        echo($result);
        curl_close($ch);
        return '';
    }

    public function newaramex() {

        $request_start_time = date('Y-m-d H:i:s');
        $xml = file_get_contents('php://input');
        $company = Companies::where('id', '=', '917')->first();

        // $secret = '';
        // if($cur = strpos($xml, $str = '<secret>')) {
        //     $curpos = $cur + strlen($str);
        //     while($curorder[$curpos] != '<')
        //         $secret .= $curorder[$curpos++];
        //     if($secret != $company->secret) {
        //         $response_array = array('success' => false, 'erorr' => 'Wrong credentials, please contact admin!');
        //         goto response;
        //     }
        // } 
        // else {
        //     $response_array = array('success' => false, 'error' => 'Secret required, please contact admin!');
        //     goto response;
        // }

        $size = strlen($xml);
        $pos = 0;
        $counter = 0;
        $items = array();
        $duplicate = array();
        while($pos < $size) {
            $curorder = '';
            $start = strpos($xml, $str = '<HAWB>', $pos);
            if(!$start)
                break;
            ++$pos;
            $end = strpos($xml, $str = '</HAWB>', $pos) + strlen($str);
            if(!$end)
                break;
            ++$pos;
            $end = min($end, $size);
            for($i = $start; $i < $end; ++$i) 
                $curorder .= $xml[$i];
            $curpos = 0;

            ++$pos;
            
            $order_number = '';
            if($cur = strpos($curorder, $str = '<HAWBNumber>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $order_number .= $curorder[$curpos++];
            } 

            $origin_entity = '';
            if($cur = strpos($curorder, $str = '<HAWBOriginEntity>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $origin_entity .= $curorder[$curpos++];
            } 

            $quantity = '';
            if($cur = strpos($curorder, $str = '<Pieces>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $quantity .= $curorder[$curpos++];
            } 

            $weight = '';
            if($cur = strpos($curorder, $str = '<HAWBWeight>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $weight .= $curorder[$curpos++];
            } 

            $order_reference = '';
            if($cur = strpos($curorder, $str = '<ShipperReference>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $order_reference .= $curorder[$curpos++];
            } 

            $sender_name = '';
            if($cur = strpos($curorder, $str = '<ShipperName>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $sender_name .= $curorder[$curpos++];
            } 

            $sender_address = '';
            if($cur = strpos($curorder, $str = '<ShipperAddress>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $sender_address .= $curorder[$curpos++];
            } 

            $sender_phone = '';
            if($cur = strpos($curorder, $str = '<ShipperTelephone>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $sender_phone .= $curorder[$curpos++];
            } 

            $sender_city = '';
            if($cur = strpos($curorder, $str = '<ShipperCity>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $sender_city .= $curorder[$curpos++];
            } 

            $receiver_name = '';
            if($cur = strpos($curorder, $str = '<ConsigneeName>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_name .= $curorder[$curpos++];
            } 

            $receiver_address = '';
            if($cur = strpos($curorder, $str = '<ConsigneeAddress>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_address .= $curorder[$curpos++];
            } 

            $receiver_phone = '';
            if($cur = strpos($curorder, $str = '<ConsigneeTelephone>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_phone .= $curorder[$curpos++];
            } 

            $receiver_phone2 = '';
            if($cur = strpos($curorder, $str = '<ConsigneeTelephone2>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_phone2 .= $curorder[$curpos++];
            } 

            $receiver_city = '';
            if($cur = strpos($curorder, $str = '<ConsigneeCity>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_city .= $curorder[$curpos++];
            } 

            $zip_code = '';
            if($cur = strpos($curorder, $str = '<ConsigneeZipCode>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $zip_code .= $curorder[$curpos++];
            } 

            $receiver_longitude = '';
            if($cur = strpos($curorder, $str = '<Longitude>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_longitude .= $curorder[$curpos++];
            } 

            $receiver_latitude = '';
            if($cur = strpos($curorder, $str = '<Latitude>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $receiver_latitude .= $curorder[$curpos++];
            } 

            $cod = '';
            if($cur = strpos($curorder, $str = '<MoneyCollection>', $curpos)) {
                $curpos = $cur + strlen($str);
                $line = '';
                while($curorder[$curpos] != '<')
                    $line .= $curorder[$curpos++];
                $line = htmlspecialchars_decode($line);
                if($cur = strpos($line, $str = '<Amount>')) {
                    $curpos = $cur + strlen($str);
                    while($line[$curpos] != '<')
                        $cod .= $line[$curpos++];
                }
            } 

            $sender_email = '';
            if($cur = strpos($curorder, $str = '<ShipperEmail>', $curpos)) {
                $curpos = $cur + strlen($str);
                while($curorder[$curpos] != '<')
                    $sender_email .= $curorder[$curpos++];
            } 

            $found = DeliveryRequest::where('order_number', '=', $order_number)->where('company_id', '=', '917')->count();
            if($found) {
                array_push($duplicate, ['id' => $order_number, 'reason' => 'Duplicated']);
            }
            else {
                $delivery_request = new DeliveryRequest;
                $delivery_request->order_number = $order_number;
                $delivery_request->origin_entity = $origin_entity;
                $delivery_request->order_reference = $order_reference;
                $delivery_request->cash_on_delivery = $cod;
                $delivery_request->receiver_name = $receiver_name;
                $delivery_request->receiver_phone = $receiver_phone;
                $delivery_request->receiver_phone2 = $receiver_phone2;
                $delivery_request->d_address = $receiver_address;
                $delivery_request->d_city = $receiver_city;
                $delivery_request->d_zipcode = $zip_code;
                $delivery_request->d_weight = $weight;
                $delivery_request->d_quantity = $quantity;
                $delivery_request->sender_name = $sender_name;
                $delivery_request->sender_phone = $sender_phone;
                $delivery_request->sender_email = $sender_email;
                $delivery_request->o_address = $sender_address;
                $delivery_request->sender_city = $sender_city;
                $delivery_request->company_id = 917;
                $delivery_request->service_type = 0;
                $delivery_request->immediate_dropoff = 1;
                $delivery_request->scheduled_dropoff_date = 0;
                $delivery_request->scheduled_dropoff_time = 0;
                $delivery_request->transportation_fees = $company->transportation_fee;
                $delivery_request->D_latitude = $receiver_latitude;
                $delivery_request->D_longitude = $receiver_longitude;
                $delivery_request->pickup_city = $company->city;
                $delivery_request->pickup_district = $company->district;
                $delivery_request->pickup_address = $company->address;
                $delivery_request->pickup_latitude = $company->latitude;
                $delivery_request->pickup_longitude = $company->longitude;
                $delivery_request->pincode = mt_rand(1000, 9999);
                $delivery_request->status = 0;
                $delivery_request->request_start_time = $request_start_time;
                $delivery_request->save();
    
                $waybill = 'OS' . str_pad($delivery_request->id,  8, "0", STR_PAD_LEFT).'KS';
                $dr = DeliveryRequest::find($delivery_request->id);
                $dr->waybill = $waybill;
                $dr->jollychic = $waybill;
                $dr->customer_tracking_number = $receiver_phone . '-' . $dr->id;
                $dr->save();
    
                $OrdersHistory = new OrdersHistory;
                $OrdersHistory->waybill_number = $waybill;
                $order_history->order_number = $dr->order_number;
                $OrdersHistory->status = 0;
                $OrdersHistory->company_id = $company->id;
                $OrdersHistory->save();
    
                array_push($items, $order_number);
                ++$counter;
            }
            $pos = $end;
        }

        $response_array = array(
            'succes' => true, 
            'message' => 'Items inserted successfully', 
            'success_items_count' => $counter, 
            'success_items' => $items,
            'failed_items_count' => count($duplicate),
            'failed_items' => $duplicate
        );

        response:
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function sendUpdates() {

        try {

            $timestamp = date('Y-m-d') . 'T' . date('H:i:s');
            $company = Request::get('company');
            if(!isset($company) || empty($company))
                $company = '917';
            
            $orders_history = OrdersHistory::where('is_updated', '=', '0')
                ->whereRaw("(waybill_number in (select distinct jollychic from delivery_request where company_id = $company))")
                ->whereRaw("(date(created_at) > '2018-08-30')")
                ->orderBy('created_at')
                ->whereIn('orders_history.status', array(4, 5, 6))
                ->distinct()
                ->limit(100)
                ->get();
            // return $orders_history;

            $xml_data = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <InfoLinkDocument>
                <AccessRequest>
                    <DocumentType>3</DocumentType>
                    <Version>1.20</Version>
                    <EntityID>28</EntityID>
                    <EntityPIN>JED879</EntityPIN>
                    <TimeStamp>$timestamp</TimeStamp>
                    <Reference1 />
                    <Reference2 />
                    <Reference3 />
                    <Reference4 />
                    <Reference5 />
                    <ReplyEmailAddress>info@saee.sa</ReplyEmailAddress>
                    <NotifyOnSuccess>False</NotifyOnSuccess>
                </AccessRequest>";
            $items = array();
            if(count($orders_history) == 0)
                goto response;
            foreach($orders_history as $history) {
                $action_date = str_replace(' ', 'T', $history->created_at);
                $delivery_request = DeliveryRequest::where('jollychic', '=', $history->waybill_number)
                    ->select(['dropoff_timestamp', 'jollychic', 'order_number', 'origin_entity', 'd_quantity', 'dropoff_timestamp', 'receiver_name', 'undelivered_reason'])
                    ->first();
                array_push($items, $delivery_request->order_number);
                Log::info('eeree ' . $timestamp . ' ' . $delivery_request->order_number);
                if($history->status == '4') {
                    $xml_data .= "<HAWBUpdate>
                                    <HAWBNumber>$delivery_request->order_number</HAWBNumber>
                                    <HAWBOriginEntity>$delivery_request->origin_entity</HAWBOriginEntity>
                                    <UpdateEntity>28</UpdateEntity>
                                    <PINumber>SH003</PINumber>
                                    <ProblemCode></ProblemCode>
                                    <Pieces>$delivery_request->d_quantity</Pieces>
                                    <ActionDate>$action_date</ActionDate>
                                    <Comment1 />
                                    <Comment2 />
                                    <BranchID>14208</BranchID>
                                    <SourceID>2</SourceID>
                                </HAWBUpdate>";
                }
                else if($history->status == '5') {
                    $xml_data .= "<HAWBUpdate>
                                    <HAWBNumber>$delivery_request->order_number</HAWBNumber>
                                    <HAWBOriginEntity>$delivery_request->origin_entity</HAWBOriginEntity>
                                    <UpdateEntity>28</UpdateEntity>
                                    <PINumber>SH005</PINumber>
                                    <ProblemCode></ProblemCode>
                                    <Pieces>$delivery_request->d_quantity</Pieces>
                                    <ActionDate>$action_date</ActionDate>
                                    <Comment1>$delivery_request->receiver_name</Comment1>
                                    <Comment2 />
                                    <BranchID>14208</BranchID>
                                    <SourceID>2</SourceID>
                                </HAWBUpdate>";
                }
                else if($history->status == '6') {
                    if($reason = UndeliveredReason::where('id', '=', $delivery_request->undelivered_reason)->first()) {
                        $xml_data .= "<HAWBUpdate>
                                        <HAWBNumber>$delivery_request->order_number</HAWBNumber>
                                        <HAWBOriginEntity>$delivery_request->origin_entity</HAWBOriginEntity>
                                        <UpdateEntity>28</UpdateEntity>
                                        <PINumber>$reason->aramex_pi_number</PINumber>
                                        <ProblemCode>$reason->aramex_problem_code</ProblemCode>
                                        <Pieces>$delivery_request->d_quantity</Pieces>
                                        <ActionDate>$action_date</ActionDate>
                                        <Comment1 />
                                        <Comment2 />
                                        <BranchID>14208</BranchID>
                                        <SourceID>2</SourceID>
                                    </HAWBUpdate>";
                    }
                }
            }
            $xml_data .= "</InfoLinkDocument>";

            $login = 'Saee';
            $password = 'JaxDm679';
            $url = 'https://infolink.aramex.net/post.aspx';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            $result = curl_exec($ch);
            echo($result);
            curl_close($ch);
            if(empty($result)) {
                foreach($orders_history as $history) {
                    OrdersHistory::where('id', '=', $history->id)
                        ->update(array('is_updated' => '1'));
                }
                response:
                $response_array = array('success' => true, 'items_count' => count($items), 'items' => $items);
            }
            else {
                $response_array = array('success' => false, 'error' => 'Something wrong happened', 'items_count' => count($items), 'items' => $items);
            }

        } catch (Exception $e) {

            Log::info(date('Y-m-d H:i:s') . ' ' . $e->getLine() . ' ' . $e->getMessage());
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'line' => $e->getLine());

        }

        $response = Response::json($response_array, 200);
        return $response;
    }

    public function update_hub_id() {
        try {
            ini_set('max_execution_time', 70000);
            $admin_id = Session::get('admin_id');
            $hub = array(
                'Jeddah' => '1',
                'Makkah' => '2',
                'Taif' => '3',
                'Baha' => '4',
                'Aqiq' => '4',
                'Atawleh' => '4',
                'BilJurashi' => '4',
                'Gilwa' => '4',
                'Mandak' => '4',
                'Mikhwa' => '4',
                'Dammam' => '5',
                'Al Hassa' => '5',
                'Dhahran' => '5',
                'Hofuf' => '5',
                'Jubail' => '5',
                'Khobar' => '5',
                'Qatif' => '5',
                'Ras Tanura' => '5',
                'Seihat' => '5',
                'Tarut' => '5',
                'Riyadh' => '7'
            );
            $cities = array(
                'Jeddah',
                'Makkah',
                'Taif',
                'Baha',
                'Aqiq',
                'Atawleh',
                'BilJurashi',
                'Gilwa',
                'Mandak',
                'Mikhwa',
                'Dammam',
                'Al Hassa',
                'Dhahran',
                'Hofuf',
                'Jubail',
                'Khobar',
                'Qatif',
                'Ras Tanura',
                'Seihat',
                'Tarut',
                'Riyadh',
            );
            $shipments = DeliveryRequest::whereIn('d_city', $cities)
                ->where('hub_id', '=', '0')
                // ->where('d_city', '=', 'Makkah')
                // ->where('jollychic', '=', 'JC10000001KS')
                ->limit(1000)
                ->get();
            $ids = array();
            foreach($shipments as $shipment) {
                $shipment->d_city = trim($shipment->d_city);
                if($shipment->d_city == 'jeddah')
                    $shipment->d_city = 'Jeddah';
                if($shipment->d_city == 'makkah')
                    $shipment->d_city = 'Makkah';
                if($shipment->d_city == 'taif')
                    $shipment->d_city = 'Taif';
                if($shipment->d_city == 'jeddah')
                    $shipment->d_city = 'Jeddah';
                if($shipment->d_city == 'baha')
                    $shipment->d_city = 'Baha';
                if($shipment->d_city == 'riyadh')
                    $shipment->d_city = 'Riyadh';
                if($shipment->d_city == 'mikhwa')
                    $shipment->d_city = 'Mikhwa';
                
                $shipment->hub_id = $hub["$shipment->d_city"];
                $shipment->save();
                array_push($ids, array($shipment->jollychic, $shipment->d_city, $shipment->hub_id));
            }
            $response_array = array('success' => true, 'items_count' => count($ids), 'items' => $ids);
            $response = Response::json($response_array, 200);
            return $response;
        } catch (Exception $e) {
            $response_array = array('success' => false, 'line' => $e->getLine(), 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
        }
    }

    public function updateAramex() {

    	ini_set('max_execution_time', 70000);

    	return '1';
    	// $shipments = OrdersHistory::whereIn('status', array('4', '5', '6', '7'))->where('is_updated', '=', '0')->select(['waybill_number'])->distinct()->get();
    	// return $shipments;

        $shipments = DeliveryRequest::leftJoin(DB::raw('(select waybill_number, is_updated, max(created_at) "created_at" from orders_history where status = 5 group by waybill_number) orders_history'), 'delivery_request.jollychic', '=', 'orders_history.waybill_number')
            ->where('company_id', '=', '917')
            ->where('status', '=', '5')
            ->where('orders_history.is_updated', '=', '0')
            ->orderBy('new_shipment_date', 'desc')
            ->select(['jollychic', 'order_number', 'orders_history.created_at', 'delivery_request.dropoff_timestamp', 'receiver_name'])
            ->limit(10)
            // ->skip(300)
            ->get();
            $timestamp = date('Y-m-d') . 'T' . date('H:i:s');
            $xml_data = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
                        <InfoLinkDocument>
                            <AccessRequest>
                                <DocumentType>3</DocumentType>
                                <Version>1.20</Version>
                                <EntityID>28</EntityID>
                                <EntityPIN>JED879</EntityPIN>
                                <TimeStamp>$timestamp</TimeStamp>
                                <Reference1 />
                                <Reference2 />
                                <Reference3 />
                                <Reference4 />
                                <Reference5 />
                                <ReplyEmailAddress>info@saee.sa</ReplyEmailAddress>
                                <NotifyOnSuccess>False</NotifyOnSuccess>
                            </AccessRequest>";
            $waybills = array();
            foreach($shipments as $shipment) {
            	array_push($waybills, $shipment->jollychic);
                $shipment->dropoff_timestamp = str_replace(' ', 'T', $shipment->dropoff_timestamp);
                $xml_data .= "<HAWBUpdate>
                                <HAWBNumber>$shipment->order_number</HAWBNumber>
                                <HAWBOriginEntity>$shipment->origin_entity</HAWBOriginEntity>
                                <UpdateEntity>28</UpdateEntity>
                                <PINumber>SH005</PINumber>
                                <ProblemCode></ProblemCode>
                                <Pieces>$shipment->d_quantity</Pieces>
                                <ActionDate>$shipment->dropoff_timestamp</ActionDate>
                                <Comment1>$shipment->receiver_name</Comment1>
                                <Comment2 />
                                <BranchID>14208</BranchID>
                                <SourceID>2</SourceID>
                            </HAWBUpdate>";
            }
            $xml_data .= "</InfoLinkDocument>";
            $login = 'Saee';
            $password = 'JaxDm679';
            $url = 'https://infolink.aramex.net/post.aspx';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            $result = curl_exec($ch);
            echo($result);
            curl_close($ch);
            OrdersHistory::whereIn('waybill_number', $waybills)->where('status', '=', '5')->update(array(
            	'is_updated' => '1'
            ));
            return 'done';
    }

    public function updateitem()
    {
        try {
            if (Request::has('secret')) {
                $secret = Request::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    return '0';
                }
            } else {
                return '0';
            }
            if (Request::has('waybill')) {
                $waybill = Request::get('waybill');
                $delivery_request = DeliveryRequest::where('waybill', '=', $waybill)->first();
                if (!isset($delivery_request)) {
                    return '0';
                } else {
                    //  cashondelivery float Mandatory
                    if (Request::has('cashondelivery')) {
                        $cash_on_delivery = Request::get('cashondelivery');
                        $delivery_request->cash_on_delivery = $cash_on_delivery;
                    }

                    //  name string Mandatory
                    if (Request::has('name')) {
                        $dropoff_name = Request::get('name');
                        $delivery_request->receiver_name = $dropoff_name;
                    }
                    //  mobile ints Mandatory
                    if (Request::has('mobile')) {
                        $dropoff_mobile = Request::get('mobile');
			$dropoff_mobile = format_phone($dropoff_mobile);
                        $delivery_request->receiver_phone = $dropoff_mobile;
                    }
                    // mobile2 ints Optional
                    if (Request::has('mobile2')) {
                        $dropoff_mobile2 = Request::get('mobile2');
			$dropoff_mobile2 = format_phone($dropoff_mobile2);
                        $delivery_request->receiver_phone2 = $dropoff_mobile2;
                    }


                    //   streetaddress string Mandatory
                    if (Request::has('streetaddress')) {
                        $d_address = Request::get('streetaddress');
                        $delivery_request->d_address = $d_address;
                    }
                    //  streetaddress2 string Optional
                    if (Request::has('streetaddress2')) {
                        $d_address2 = Request::get('streetaddress2');
                        $delivery_request->d_address2 = $d_address2;
                    }

                    //  streetaddress3 string Optional
                    if (Request::has('streetaddress3')) {
                        $d_address3 = Request::get('streetaddress3');
                        $delivery_request->d_address3 = $d_address3;
                    }

                    //   district  string Mandatory
                    if (Request::has('district')) {
                        $d_district = Request::get('district');
                        $delivery_request->d_district = $d_district;
                    }


                    //  city  string Mandatory
                    if (Request::has('city')) {
                        $d_city = Request::get('city');
                        $delivery_request->d_city = $d_city;
                    }


                    //  state  string Optional
                    if (Request::has('state')) {
                        $d_state = Request::get('state');
                        $delivery_request->d_state = $d_state;
                    }
                    //  zipcode ints Optional
                    if (Request::has('zipcode')) {
                        $d_zipcode = Request::get('zipcode');
                        $delivery_request->d_zipcode = $d_zipcode;
                    }
                    //   latitude float Optional
                    if (Request::has('latitude')) {
                        $d_latitude = Request::get('latitude');
                        $delivery_request->D_latitude = $d_latitude;
                    }
                    //  longitude float Optional
                    if (Request::has('longitude')) {
                        $d_longitude = Request::get('longitude');
                        $delivery_request->D_longitude = $d_longitude;
                    }
                    //   weight float Mandatory
                    if (Request::has('weight')) {
                        $d_weight = Request::get('weight');
                        $delivery_request->d_weight = $d_weight;
                    }
                    // quantity ints Mandatory
                    if (Request::has('quantity')) {
                        $d_quantity = Request::get('quantity');
                        $delivery_request->d_quantity = $d_quantity;
                    }
                    //   description strings Optional
                    if (Request::has('description')) {
                        $d_description = Request::get('description');
                        $delivery_request->d_description = $d_description;
                    }
                    //   Branches Detail Optional
                    if (Request::has('branchid')) {
                        $branch_id = Input::get('branchid');
                        $branches = DB::table('branches')->where('id', $branch_id)->first();
                        if (!isset($branches)) {
                            $response_array = array('success' => false, 'error' => 'invalid branch id please contact Saee', 'error_code' => 410);
                            $response = Response::json($response_array, 200);
                            return $response;
                        } else {
                            $delivery_request->branch_id = $branches->id;
                            $delivery_request->o_address = $branches->address;
                            $delivery_request->o_city = $branches->city;
                            $delivery_request->o_district = $branches->district;
                            $delivery_request->o_name = $branches->name;
                            $delivery_request->origin_latitude = $branches->latitude;
                            $delivery_request->origin_longitude = $branches->longitude;
                        }
                    }
                    $delivery_request->save();
                    return 1;
                }
            } else {
                return '0';
            }

        } catch (Exception $e) {
            Log::info($e->getMessage());
            return 0;
        }
    }

    public function cancelitem()
    {
        //get captain ID and increase his credit by 20
        try {
            if (Request::has('secret')) {
                $secret = Request::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    return '0';
                }
            } else {
                return '0';
            }
            if (Request::has('waybill')) {
                $waybill = Request::get('waybill');
                $order = DeliveryRequest::where('jollychic', '=', $waybill)->first();
                if (isset($order) && ($order->status == 0 || $order->status == -3)) {
                    DeliveryRequest::where('jollychic', $waybill)->delete();
                    OrdersHistory::where('waybill_number', $waybill)->delete();
                    return '1';
                } else {
                    return '0';
                }
            } else {
                return '0';
            }
        } catch (Exception  $e) {
            Log::info($e->getMessage());
            return '0';
        }
    }

    public function getcities()
    {
        $res = [];
        $done = [];
        $cities = City::where('name', '<>', 'All')->where('grouped', '=', '0')->select(['name', 'name_ar'])->get();
        foreach($cities as $city)
            array_push($res, $city);
        $groupedcities = City::where('grouped', '=', '1')->select('name')->get();
        foreach($groupedcities as $gcity) {
            $name = $gcity->name;
            $query = DB::select("select distinct SUBSTRING_INDEX(district, \"+\", 1) 'name', SUBSTRING_INDEX(district_ar, \"+\", 1) 'name_ar' from districts where city = '$name' and district <> 'No District'");
            foreach($query as $qcity) {
                $qcity->name = trim($qcity->name, ' ');
                $qcity->name_ar = trim($qcity->name_ar, ' ');
                $cur = ['name'=>$qcity->name, 'name_ar'=>$qcity->name_ar];
                array_push($res, $cur);
            }
        }
        $response_array = array('success' => true, 'cities' => $res);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getcitiesbystate() {
        $state = Request::get('state');
        $cities = City::where('state','=',$state)->select(['name', 'name_ar', 'grouped'])->get();
        $res = [];
        foreach($cities as $city) {
            if($city->grouped == 0)
                array_push($res, $city);
            else {
                $query = DB::select("select distinct SUBSTRING_INDEX(district, '+', 1) 'name', SUBSTRING_INDEX(district_ar, '+', 1) 'name_ar' from districts where city = '$city->name'");
                foreach($query as $qcity) {
                    $qcity->name = ltrim($qcity->name, ' ');
                    $qcity->name = rtrim($qcity->name, ' ');
                    $qcity->name_ar = ltrim($qcity->name_ar, ' ');
                    $qcity->name_ar = rtrim($qcity->name_ar, ' ');
                    $cur = ['name'=>$qcity->name, 'name_ar'=>$qcity->name_ar];
                    array_push($res, $cur);
                }
            }
        }
        $response_array = array('success' => true, 'state' => $state, 'cities' => $res);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getalldistricts()
    {
        $res = [];
        $ng_cities = City::where('grouped', '=', '0')->where('name', '<>', 'All')->get();
        $g_cities = City::where('grouped', '=', '1')->where('name', '<>', 'All')->get();
        for($i = 0; $i < count($ng_cities); ++$i){
            $districts = Districts::select(['district', 'city', 'district_ar'])->where('city', '=', $ng_cities[$i]->name)->where('district', '<>', 'No District')->get();
            foreach($districts as $district)
                array_push($res, $district);
        }
        for($j = 0; $j < count($g_cities); ++$j) {
            $cname = $g_cities[$j]->name;
            $districts = Districts::where('city', '=', $cname)->where('district', '<>', 'No District')->get();
            for($i = 0; $i < count($districts); ++$i) {
                $district = mb_split('\\+', $districts[$i]->district);
                $district_ar = mb_split('\\+', $districts[$i]->district_ar);
                $city = $district[0];
                $district = $district[1];
                $district_ar = $district_ar[1];
                $city = ltrim($city, ' ');               $city = rtrim($city, ' ');
                $district = ltrim($district, ' ');       $district = rtrim($district, ' ');
                $district_ar = ltrim($district_ar, ' '); $district_ar = rtrim($district_ar, ' ');
                $cur = (object) array(
                    'district'=>$district,
                    'city'=>$city,
                    'district_ar'=>$district_ar
                );
                array_push($res, $cur);
            }
        }
        $response_array = array('success' => true, 'districts' => $res);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function getdistrictsbycity()
    {
        $city = Request::get('city');
        $grouped = City::where('name', '=', $city)->where('grouped', '=', '0')->count();
        if($grouped == 0) {
            $districts = Districts::whereRaw("SUBSTRING_INDEX(district, '+', 1) = \"$city\"")->where('district', '<>', 'No District')->select([
                DB::raw('SUBSTRING_INDEX(district, \'+\', -1) as "district"'),
                DB::raw('SUBSTRING_INDEX(district_ar, \'+\', -1) as "district_ar"')
            ])->get();
        }
        else {
            $districts = Districts::where('city', '=', $city)->where('district', '<>', 'No District')->select(['district', 'district_ar'])->get();
        }
        for($k = 0; $k < count($districts); ++$k)  {
            // remove spaces from beginning and end
            $districts[$k]->district = ltrim($districts[$k]->district, ' ');
            $districts[$k]->district = rtrim($districts[$k]->district, ' ');

            $districts[$k]->district_ar = ltrim($districts[$k]->district_ar, ' ');
            $districts[$k]->district_ar = rtrim($districts[$k]->district_ar, ' ');
        }
        $response_array = array('success' => true, 'city' => $city, 'districts' => $districts);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function printsticker()
    {
        try {

            $packageID = Request::segment(3);
            if($packageID == 'param')
                $packageID = Request::get('waybill');
            $package = DeliveryRequest::where('jollychic', '=', $packageID)->first();
            $companydata = Companies::find($package->company_id);

            $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="55" height="69">';
            $destination = 'JED';

            /////////////////
            $nonGroupedCities = City::where('grouped', '=', '0')->get();
            $groupedCities = City::where('grouped', '=', '1')->get();
            $flag = false;
            foreach($nonGroupedCities as $city) {
                if($city->name == $package->d_city) {
                    $cityimg = "<img src='/companiesdashboard/images/$city->img_url' width='55' height='69'>";
                    $destination = $city->c_code;
                    $flag = true;
                    break;
                }
            }
            foreach($groupedCities as $curcity) {
                if($flag)
                    break;
                $city = $curcity->name;
                $query = DB::select("select jollychic from delivery_request where jollychic = '$packageID' and d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) as 'a' from districts where city = '$city') limit 1");
                if(count($query) == 0)
                    continue;
                $cityimg = "<img src='/companiesdashboard/images/$curcity->img_url' width='55' height='69'>";
                $destination = $curcity->c_code;
                break;
            }
            ////////////////
            
            $flag = true;
            if(!isset($package->order_number) || $package->order_number == '')
                $flag = false;

            $data = array(
                'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->jollychic, "C128", 1, 33) . '" alt="barcode"   style=\'width: 6.4cm;height: 1.3cm;padding: 1px;\'/>',
                'barcode90' => $flag ? '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->order_number, "C128", 1, 33) . '" alt="barcode" style=\'width: 6.4cm;height: 1.3cm;padding:1px; \'/>' : '',
                'city' => $package->d_city,
                'cityimg' => $cityimg,
                'cod' => round($package->cash_on_delivery, 2),
                'pieces' => $package->d_quantity,
                'weight' => $package->d_weight,
                'company_name' => $companydata->company_type == 1 ? $package->sender_name : $companydata->company_name,
                'company_address' => $companydata->company_type == 1 ? $package->o_address : $companydata->address,
                'company_city' => $companydata->company_type == 1 ? $package->sender_city : $companydata->city,
                'company_country' => $companydata->company_type == 1 ? $package->sender_country : $companydata->country,
                'company_phone' => $companydata->company_type == 1 ? $package->sender_phone : $companydata->phone,
                'customer_name' => $package->receiver_name,
                'customer_address' => $package->d_address,
				'customer_address2' => $package->d_address2,
                'customer_city' => $package->d_city,
                'customer_district' => $package->d_district,
                'customer_phone' => $package->receiver_phone,
                'customer_phone2' => isset($package->receiver_phone2) ? $package->receiver_phone2 : "",
                'waybill' => $package->jollychic,
                'order_number' => $package->order_number,
                'description' => $package->d_description,
                'destination' => $destination,
                'date' => date("Y-m-d")
            );
            return View::make('companies.stickerpdf', $data);
            /*$view = View::make('companies.sticker', $data);
            $contents = $view->render();
            $pdf = App::make( 'dompdf' );
            $pdf->loadHTML( $contents );
            return $pdf->stream();*/
            //$pdf = PDF::loadView('companies.sticker', $data);
            //return $pdf->download($packageID.'.pdf');

        } catch(Exception $e) {

            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';

        }
    }

    public function generatestickerpdf() {
        
        try {

            $packageID = Request::segment(3);
            $package = DeliveryRequest::where('jollychic', '=', $packageID)->first();
            $companydata = Companies::find($package->company_id);

            $cityimg = '<img src="/companiesdashboard/images/jeddah.png" width="55" height="69">';
            $destination = 'JED';

            /////////////////
            $nonGroupedCities = City::where('grouped', '=', '0')->get();
            $groupedCities = City::where('grouped', '=', '1')->get();
            $flag = false;
            foreach($nonGroupedCities as $city) {
                if($city->name == $package->d_city) {
                    $cityimg = "<img src='/companiesdashboard/images/$city->img_url' width='55' height='69'>";
                    $destination = $city->c_code;
                    $flag = true;
                    break;
                }
            }
            foreach($groupedCities as $curcity) {
                if($flag)
                    break;
                $city = $curcity->name;
                $query = DB::select("select jollychic from delivery_request where jollychic = '$packageID' and d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) as 'a' from districts where city = '$city') limit 1");
                if(count($query) == 0)
                    continue;
                $cityimg = "<img src='/companiesdashboard/images/$curcity->img_url' width='55' height='69'>";
                $destination = $curcity->c_code;
                break;
            }
            ////////////////
            
            $flag = true;
            if(!isset($package->order_number) || $package->order_number == '')
                $flag = false;

            $data = array(
                'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->jollychic, "C128", 1, 33) . '" alt="barcode" style=\'width: 13cm;height: 3cm;padding: 1px;\'/>',
                'barcode90' => $flag ? '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->order_number, "C128", 1, 33) . '" alt="barcode" style=\'width: 15cm;height: 3cm;padding:1px; \'/>' : '',
                'city' => $package->d_city,
                'cityimg' => $cityimg,
                'cod' => round($package->cash_on_delivery, 2),
                'pieces' => $package->d_quantity,
                'weight' => $package->d_weight,
                'company_name' => $companydata->company_type == 1 ? $package->sender_name : $companydata->company_name,
                'company_address' => $companydata->company_type == 1 ? $package->o_address : $companydata->address,
                'company_city' => $companydata->company_type == 1 ? $package->sender_city : $companydata->city,
                'company_country' => $companydata->company_type == 1 ? $package->sender_country : $companydata->country,
                'company_phone' => $companydata->company_type == 1 ? $package->sender_phone : $companydata->phone,
                'customer_name' => $package->receiver_name,
                'customer_address' => $package->d_address,
                'customer_address2' => $package->d_address2,
                'customer_city' => $package->d_city,
                'customer_district' => $package->d_district,
                'customer_phone' => $package->receiver_phone,
                'customer_phone2' => isset($package->receiver_phone2) ? $package->receiver_phone2 : "",
                'waybill' => $package->jollychic,
                'order_number' => $package->order_number,
                'description' => $package->d_description,
                'destination' => $destination,
                'date' => date("Y-m-d")
            );

            return View::make('companies.stickerpdf2', $data);

        } catch(Exception $e) {
            
            Log::info($e->getMessage());
            return $e->getMessage();

        }
    }
    
    public function printstickerpdf()
    {
        try {
            
            $packageID = Request::segment(4);
            if($packageID == 'param')
                $packageID = Request::get('waybill');
            shell_exec('/usr/local/bin/wkhtmltopdf https://saee.sa/deliveryrequest/generatestickerpdf/' . $packageID . ' /var/www/html/saee/public/waybills/' . $packageID . '.pdf 2>&1');
            
            return Response::download(public_path() . '/waybills/' . $packageID . '.pdf');

        } catch(Exception $e) {

            Log::info($e);

            Log::info('hello');
            return 'Failed due to an error, please try again.';

        }
    }

    public function getsubcompanies() {
        try {
            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    $response_array = array('success' => false, 'error' => 'invalid api secret, please contact Saee', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Field secret is required, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $subcompanies = DB::table('companies')->select(['secret','id','username','company_name','name_ar','email','phone','city','address','latitude','longitude'])
                ->where('parent_id', $company->id)->get();
            $response_array = array('success' => true, 'branches' => $subcompanies);
            $response = Response::json($response_array, 200);
            return $response;

        } catch (Exception $e) {

            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';
        }
    }

    public function getbranches()
    {
        try {
            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    $response_array = array('success' => false, 'error' => 'invalid api secret, please contact Saee', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Field secret is required, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $branches = DB::table('branches')->select(['id', 'name', 'name_ar', 'email', 'phone1', 'phone2', 'address', 'district', 'city', 'zip_code', 'state', 'country', 'latitude', 'longitude'])
                ->where('company_id', $company->id)->get();
            $response_array = array('success' => true, 'branches' => $branches);
            $response = Response::json($response_array, 200);
            return $response;

        } catch (Exception $e) {

            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';
        }
    }

    public function pickupnewshipment()
    {
        try {
            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    $response_array = array('success' => false, 'error' => 'invalid api secret, please contact Saee', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Field secret not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            if (Request::has('ordernumber')) {
                $ordernumber = Input::get('ordernumber');
                if (count(DeliveryRequest::where('order_number', '=', $ordernumber)->where('company_id', '=', $company->id)->get())) {
                    $response_array = array('success' => false, 'error' => 'order number ' . $ordernumber . ' already exists, Please try again.', 'error_code' => 401);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Field ordernumber not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            //  company_id int Optional
            if (Request::has('company_id')) {
                $company_id = Input::get('company_id');
            } else {
                $company_id = $company->id;
            }
            $found = Companies::where('id', '=', $company_id)->count();
            if(!$found) {
                $response_array = array('success' => false, 'error' => 'wrong company id, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $delivery_request = new DeliveryRequest;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            //  cashondelivery float Mandatory
            if (Request::has('cashondelivery')) {
                $cash_on_delivery = Input::get('cashondelivery');
            } else {
                $response_array = array('success' => false, 'error' => 'Field cashondelivery not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $delivery_request->cash_on_delivery = $cash_on_delivery;

            //  name string Mandatory
            if (Request::has('name')) {
                $dropoff_name = Input::get('name');
            } else {
                $response_array = array('success' => false, 'error' => 'Field name not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $delivery_request->receiver_name = $dropoff_name;


            //  mobile ints Mandatory
            if (Request::has('mobile')) {
                $dropoff_mobile = Input::get('mobile');
                $dropoff_mobile = format_phone($dropoff_mobile);
                /*$mobilelength = strlen((string)$dropoff_mobile);
                if ($mobilelength <= 11) {
                    $dropoff_mobile = preg_replace('/^0/', '966', $dropoff_mobile);
                }*/            } else {
                $response_array = array('success' => false, 'error' => 'Field mobile not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $delivery_request->receiver_phone = $dropoff_mobile;

            //  Email ints Mandatory
            $dropoff_email = "";
            if (Request::has('email')) {
                $dropoff_email = str_replace(PHP_EOL, ' ', Input::get('email'));
            }
            $delivery_request->receiver_email = $dropoff_email;


            // mobile2 ints Optional
            if (Request::has('mobile2')) {
                $dropoff_mobile2 = Input::get('mobile2');
               $dropoff_mobile2 = format_phone($dropoff_mobile2);
                /*$mobile2length = strlen((string)$dropoff_mobile2);
                if ($mobile2length <= 11) {
                    $dropoff_mobile2 = preg_replace('/^0/', '966', $dropoff_mobile2);
                }*/                $delivery_request->receiver_phone2 = $dropoff_mobile2;
            }



            // pickup_city string Optional
            if(Request::has('pickup_city')) {
                $pickup_city = Input::get('pickup_city');
                $delivery_request->pickup_city = $pickup_city;
            }
            else {
                $delivery_request->pickup_city = Companies::where('id', '=', $company_id)->select('address')->first()->city;
            }

            // pickup_district string Optional
            if(Request::has('pickup_district')) {
                $pickup_district = Input::get('pickup_district');
                $delivery_request->pickup_district = $pickup_district;
            }
            else {
                $delivery_request->pickup_district = Companies::where('id', '=', $company_id)->select('address')->first()->district;
            }

            // pickup_address string Optional
            if(Request::has('pickup_address')) {
                $pickup_address = Input::get('pickup_address');
                $delivery_request->pickup_address = $pickup_address;
            }
            else {
                $delivery_request->pickup_address = Companies::where('id', '=', $company_id)->select('address')->first()->address;
            }


            // pickup_latitude double Optional
            if(Request::has('pickup_latitude')) {
                $pickup_latitude = Input::get('pickup_latitude');
                $delivery_request->pickup_latitude = $pickup_latitude;
            }
            else {
                $delivery_request->pickup_latitude = Companies::where('id', '=', $company_id)->select('latitude')->first->latitude;
            }

            // pickup_longitude double Optional
            if(Request::has('pickup_longitude')) {
                $pickup_longitude = Input::get('pickup_longitude');
                $delivery_request->pickup_longitude = $pickup_longitude;
            }
            else {
                $delivery_request->pickup_longitude = Companies::where('id', '=', $company_id)->select('longitude')->first->longitude;
            }

            //   streetaddress string Mandatory
            if (Request::has('streetaddress')) {
                $d_address = str_replace(PHP_EOL, ' ', Input::get('streetaddress'));
                $d_address = trim(preg_replace('/\s+/', ' ', $d_address));
                $delivery_request->d_address = $d_address;
            } else {
                $response_array = array('success' => false, 'error' => 'Field streetaddress not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            //  streetaddress2 string Optional
            if (Request::has('streetaddress2')) {
                $d_address2 = str_replace(PHP_EOL, '', Input::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/', ' ', $d_address2));
                $delivery_request->d_address2 = $d_address2;
            }

            //  streetaddress3 string Optional
            if (Request::has('streetaddress3')) {
                $d_address3 = str_replace(PHP_EOL, '', Input::get('streetaddress3'));
                $d_address3 = trim(preg_replace('/\s+/', ' ', $d_address3));
                $delivery_request->d_address3 = $d_address3;
            }


            //  city  string Mandatory
            if (Request::has('city')) {
                $d_city = Input::get('city');
                $delivery_request->d_city = $d_city;
            } else {
                $response_array = array('success' => false, 'error' => 'Field city not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            //   district  string Mandatory
            if (Request::has('district')) {
                $d_district = Input::get('district');
                if (strpos($d_district, '+') !== false) {

                    $d_city = strtok($d_district, '+');

                    $d_district = strtok('');
                    $delivery_request->d_city = $d_city;
                }
                $delivery_request->d_district = $d_district;
            }

            //  state  string Optional
            if (Request::has('state')) {
                $d_state = Input::get('state');
                if ($d_state == 'Makkah' || $d_state == 'Jeddah' || $d_state == 'Taif') {
                    $d_state = 'Makkah{Mecca}';
                }
                if ($d_state == 'Baha') {

                    $d_state = 'Al Baha';
                }
                if ($d_state == 'Dammam') {
                    $d_state = 'Eastern Region {Ash-Sharqiyah}';
                }
                $delivery_request->d_state = $d_state;
            }

            //  zipcode ints Optional
            if (Request::has('zipcode')) {
                $d_zipcode = Input::get('zipcode');
                $delivery_request->d_zipcode = $d_zipcode;
            }

            //   latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Input::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            //  longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Input::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }


            //   weight float Mandatory
            if (Request::has('weight')) {
                $d_weight = Input::get('weight');
            } else {
                $response_array = array('success' => false, 'error' => 'Field weight not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $delivery_request->d_weight = $d_weight;

            // quantity ints Mandatory
            if (Request::has('quantity')) {
                $d_quantity = Input::get('quantity');
            } else {
                $response_array = array('success' => false, 'error' => 'Field quantity not found, please try again.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $delivery_request->d_quantity = $d_quantity;

            //   description strings Optional
            if (Request::has('description')) {
                $d_description = Input::get('description');
                $delivery_request->d_description = $d_description;
            }

            //   Branches Detail Optional - Madatory for parent companies
            if (Request::has('branchid')) {
                $branch_id = Input::get('branchid');
                $branches = DB::table('branches')->where('id', $branch_id)->first();
                if (!isset($branches)) {
                    $response_array = array('success' => false, 'error' => 'invalid branch id, please contact Saee', 'error_code' => 410);
                    $response = Response::json($response_array, 200);
                    return $response;
                } else {
                    $delivery_request->branch_id = $branches->id;
                    $delivery_request->o_address = $branches->address;
                    $delivery_request->o_city = $branches->city;
                    $delivery_request->o_district = $branches->district;
                    $delivery_request->o_name = $branches->name;
                    $delivery_request->origin_latitude = $branches->latitude;
                    $delivery_request->origin_longitude = $branches->longitude;
                }
            } else if($company->parent_id == 0) {
                $response_array = array('success' => false, 'error' => 'Please Enter Branch Id.', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            } else {
                $delivery_request->branch_id = 0;
                $delivery_request->o_address = $company->address;
                $delivery_request->o_city = $company->city;
                $delivery_request->o_name = $company->company_name;
                $delivery_request->origin_latitude = $company->latitude;
                $delivery_request->origin_longitude = $company->longitude;
            }

            $delivery_request->request_start_time = date("Y-m-d H:i:s");
            $delivery_request->order_number = $ordernumber;
            $delivery_request->company_id = $company->id;
            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->status = -3;

            $delivery_request->save();
            $waybill = 'OS' . str_pad($delivery_request->id, 8, "0", STR_PAD_LEFT) . 'KS';

            $dr = DeliveryRequest::find($delivery_request->id);

            $dr->waybill = $waybill;
            $dr->jollychic = $waybill;
            $dr->customer_tracking_number = $dropoff_mobile . '-' . $dr->id;
            $dr->save();

            $OrdersHistory = new OrdersHistory;

            $OrdersHistory->waybill_number = $waybill;
            $order_history->order_number = $dr->order_number;
            $OrdersHistory->status = -3;

            $OrdersHistory->company_id = $company->id;

            $OrdersHistory->save();

            // $curl = new Curl();
            // $sms = 'عزيزي/زتي ';
            // $sms .= $dropoff_name;
            // $sms .= "،";
            // $sms .= "طلبكم من ";
            // $company = Companies::find($dr->company_id);
            // if (isset($company)) {
            //     $sms .= " " . $company->name_ar . " ";
            // }

            // $sms .= " في طريقه إلى ";
            // $sms .= $d_city;
            // $sms .= " وسيتواصل معكم مندوب التوزيع في حيكم في أقرب ";
            // $sms .= "وقت مع تحيات ساعي";

            // $destinations1 = "966";
            // $destinations1 .= substr(str_replace(' ', '', $dropoff_mobile), -9);

            // $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            // $urlDiv = explode("?", $url);
            // $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            // Log::info("SMSSERVICELOG" . $result);

            // if (Request::has('mobile2') && $dropoff_mobile != $dropoff_mobile2) {
            //     $destinations2 = "966";
            //     $destinations2 .= substr(str_replace(' ', '', $dropoff_mobile2), -9);
            //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

            //     $urlDiv = explode("?", $url);
            //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            //     Log::info("SMSSERVICELOG" . $result);
            // }

            return $waybill;

        } catch (Exception $e) {
            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';
        }
    }

    public function activatecompany() {
        try {
            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    $response_array = array(
                        'success' => false,
                        'error' => 'invalid api secret please contact Saee',
                        'error_code' => 410
                    );
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter secret', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $newcompany = new companies;

            //  username is mandatory
            if (Request::has('username')) {
                $usernmae = Input::get('username');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter username', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->username = $usernmae;


            //  password is mandatory
            if (Request::has('password')) {
                $password = Input::get('password');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter password', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->password = Hash::make($password);


            //  email is mandatory
            if (Request::has('email')) {
                $email = Input::get('email');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter email', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->email = $email;


            //  company_name is mandatory
            if (Request::has('company_name')) {
                $company_name = Input::get('company_name');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter company name', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->company_name = $company_name;


            //  name_ar is mandatory
            if (Request::has('name_ar')) {
                $name_ar = Input::get('name_ar');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter arabic name', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->name_ar = $name_ar;


            //  phone is mandatory
            if (Request::has('phone')) {
                $phone = Input::get('phone');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter phone', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->phone = $phone;


            //  address is mandatory
            if (Request::has('address')) {
                $address = Input::get('address');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter address', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->address = $address;


            //  city is mandatory
            if (Request::has('city')) {
                $city = Input::get('city');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter city', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->city = $city;


            //  zipcode is optional
            if (Request::has('zipcode')) {
                $zipcode = Input::get('zipcode');
                $newcompany->zipcode = $zipcode;
            }


            //  country is mandatory
            if (Request::has('country')) {
                $country = Input::get('country');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter country', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->country = $country;


            //  latitude is mandatory
            if (Request::has('latitude')) {
                $latitude = Input::get('latitude');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter latitude', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->latitude = $latitude;


            //  longitude is mandatory
            if (Request::has('longitude')) {
                $longitude = Input::get('longitude');
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter longitude', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $newcompany->longitude = $longitude;


            //  owner_id_number is optionally
            if (Request::has('owner_id_number')) {
                $owner_id_number = Input::get('owner_id_number');
                $newcompany->owner_id = $owner_id_number;
            }


            //  cr_number is optionally
            if (Request::has('cr_number')) {
                $cr_number = Input::get('cr_number');
                $newcompany->company_cr = $cr_number;
            }


            //  bank_name is optional
            if (Request::has('bank_name')) {
                $bank_name = Input::get('bank_name');
                $newcompany->bank_name = $bank_name;
            }

            //  iban is optional
            if (Request::has('iban')) {
                $iban = Input::get('iban');
                $newcompany->iban = $iban;
            }


            $newcompany->parent_id = $company->id;
            $newcompany->secret = Hash::make(str_random(8));
            $newcompany->company_type = 2;
            $newcompany->is_api = 1;
            $newcompany->transportation_fee = 25;
            $newcompany->pickup_fee = 10;
            $newcompany->max_regular_weight = 15;
            $newcompany->excess_weight_fees = 1;
            $newcompany->save();

            $response_array = array(
                'success' => true,
                'id'=>$newcompany->id
            );
            $response = Response::json($response_array, 200);
            return $response;

        } catch (Exception $e) {
            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';
        }
    }

    public function pickupshipment() {

        try {

            if (Request::has('secret')) {
                $secret = Input::get('secret');
                $company = Companies::where('secret', '=', $secret)->first();
                if (!isset($company)) {
                    $response_array = array(
                        'success' => false,
                        'error' => 'invalid api secret please contact Saee',
                        'error_code' => 410
                    );
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Please Enter secret', 'error_code' => 401);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $delivery_request = new DeliveryRequest;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");

            // merchant name string Mandatory
            if(Request::has('merchantname')) {
                $merchant_name = Input::get('merchantname');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant name required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->sender_name = $merchant_name;

            // merchant email string Mandatory
            if(Request::has('merchantemail')) {
                $merchant_email = Input::get('merchantemail');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant email required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->sender_email = $merchant_email;

            // merchant phone string Mandatory
            if(Request::has('merchantphone')) {
                $merchant_phone = Input::get('merchantphone');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant phone required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->sender_phone = $merchant_phone;

            // merchant city string Mandatory
            if(Request::has('merchantcity')) {
                $merchant_city = Input::get('merchantcity');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant city required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->sender_city = $merchant_city;

            // merchant address string Mandatory
            if(Request::has('merchantaddress')) {
                $merchant_address = Input::get('merchantaddress');
            } else {
                $response_array = array('success' => false, 'error' => 'Merchant address required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->o_address = $merchant_address;

            // merchant country string Optional
            $merchant_country = 'Saudi Arabia';
            if(Request::has('merchantcountry')) {
                $merchant_country = Input::get('merchantcountry');
            }
            $delivery_request->sender_country = $merchant_country;

            // ordernumber string Optional
            $ordernumber = '';
            if (Request::has('ordernumber')) {
                $ordernumber = Input::get('ordernumber');
                if (count(DeliveryRequest::where('order_number', '=', $ordernumber)->where('company_id', '=', $company->id)->get())) {
                    $response_array = array('success' => false, 'error' => 'order number ' . $ordernumber . ' already exists, Please try again.');
                    $response_code = 200;
                    goto response;
                }
            }
            $delivery_request->order_number = $ordernumber;

            // cashondelivery float Mandatory
            if (Request::has('cashondelivery')) {
                $cash_on_delivery = Input::get('cashondelivery');
            } else {
                $response_array = array('success' => false, 'error' => 'cash on delivery required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->cash_on_delivery = $cash_on_delivery;

            // receiver name string Mandatory
            if (Request::has('dropoffname')) {
                $dropoff_name = Input::get('dropoffname');
            } else {
                $response_array = array('success' => false, 'error' => 'Dropoff name required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->receiver_name = $dropoff_name;

            // mobile string Mandatory
            if (Request::has('mobile')) {
                $dropoff_mobile = Input::get('mobile');
                $mobilelength = strlen((string)$dropoff_mobile);
                if($mobilelength <= 11){
                    $dropoff_mobile = preg_replace('/^0/','966',$dropoff_mobile);
                }
            } else {
                $response_array = array('success' => false, 'error' => 'Mobile required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->receiver_phone = $dropoff_mobile;

            // mobile2 string Optional
            if (Request::has('mobile2')) {
                $dropoff_mobile2 = Input::get('mobile2');
                $mobile2length = strlen((string)$dropoff_mobile2);
                if($mobile2length <= 11){
                    $dropoff_mobile2 = preg_replace('/^0/','966',$dropoff_mobile2);
                }
                $delivery_request->receiver_phone2 = $dropoff_mobile2;
            }

            // Email string Optional
            $dropoff_email = "";
            if (Request::has('email')) {
                $dropoff_email = str_replace(PHP_EOL,' ',Input::get('email'));
            }
            $delivery_request->receiver_email = $dropoff_email;

            //  streetaddress string Mandatory
            if (Request::has('streetaddress')) {
                $d_address = str_replace(PHP_EOL,' ',Input::get('streetaddress'));
                $d_address = trim(preg_replace('/\s+/',' ', $d_address));
            } else {
                $response_array = array('success' => false, 'error' => 'Street address required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->d_address = $d_address;

            // streetaddress2 string Optional
            $d_address2 = '';
            if (Request::has('streetaddress2')) {
                $d_address2 = str_replace(PHP_EOL,'',Input::get('streetaddress2'));
                $d_address2 = trim(preg_replace('/\s+/',' ', $d_address2));
            }
            $delivery_request->d_address2 = $d_address2;

            // city string Mandatory
            if (Request::has('city')) {
                $d_city = Input::get('city');
                $d_city = trim($d_city);
                $found = false;
                $done = [];
                $cities = City::where('name', '<>', 'All')->where('grouped', '=', '0')->select(['name', 'name_ar'])->get();
                foreach($cities as $city)
                    if($city->name == $d_city) {
                        $found = true;
                        break;
                    }
                $groupedcities = City::where('grouped', '=', '1')->select('name')->get();
                foreach($groupedcities as $gcity) {
                    if($found)
                        break;
                    $name = $gcity->name;
                    $query = DB::select("select distinct SUBSTRING_INDEX(district, \"+\", 1) 'name', SUBSTRING_INDEX(district_ar, \"+\", 1) 'name_ar' from districts where city = '$name'");
                    foreach($query as $qcity) {
                        $qcity->name = trim($qcity->name);
                        if($qcity->name == $d_city) {
                            $found = true;
                            break;
                        }
                    }
                }
                if(!$found) {
                    $response_array = array('success' => false, 'error' => 'City is not supported');
                    $response_code = 200;
                    goto response;
                }
            } else {
                $response_array = array('success' => false, 'error' => 'City required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->d_city = $d_city;

            // district string Optional
            $d_district = '';
            if (Request::has('district')) {
                $d_district = Input::get('district');
                $d_district = trim($d_district);
                $found = false;
                $ng_cities = City::where('grouped', '=', '0')->where('name', '<>', 'All')->get();
                $g_cities = City::where('grouped', '=', '1')->where('name', '<>', 'All')->get();
                for($i = 0; $i < count($ng_cities); ++$i){
                    $districts = Districts::select(['district', 'city', 'district_ar'])->where('city', '=', $ng_cities[$i]->name)->get();
                    foreach($districts as $district)
                        if($district->district == $d_district) {
                            $found = true;
                            break;
                        }
                }
                for($j = 0; $j < count($g_cities) && !$found; ++$j) {
                    $cname = $g_cities[$j]->name;
                    $districts = Districts::where('city', '=', $cname)->get();
                    for($i = 0; $i < count($districts); ++$i) {
                        $district = mb_split('\\+', $districts[$i]->district);
                        $district = $district[1];
                        $district = trim($district, ' ');
                        if($district == $d_district) {
                            $found = true;
                            $curcity = $g_cities[$j]->name;
                            break;
                        }
                    }
                }
                if(!$found || $curcity != $d_city) {
                    $response_array = array('success' => false, 'error' => 'District is not supported');
                    $response_code = 200;
                    goto response;
                }
            }
            $delivery_request->d_district = $d_district;

            // state string Optional
            $d_state = '';
            if (Request::has('state')) {
                $d_state = Input::get('state');
                if($d_state == 'Makkah' || $d_state == 'Jeddah' || $d_state =='Taif'){
                    $d_state = 'Makkah{Mecca}';
                }
                if($d_state =='Baha'){
                    $d_state = 'Al Baha';
                }
                if($d_state =='Dammam'){
                    $d_state = 'Eastern Region {Ash-Sharqiyah}';
                }
            }
            $delivery_request->d_state = $d_state;

            // zipcode ints Optional
            $d_zipcode = '';
            if (Request::has('zipcode')) {
                $d_zipcode = Input::get('zipcode');
            }
            $delivery_request->d_zipcode = $d_zipcode;

            //  latitude float Optional
            if (Request::has('latitude')) {
                $d_latitude = Input::get('latitude');
            } else {
                $d_latitude = 21.575723;
            }

            // longitude float Optional
            if (Request::has('longitude')) {
                $d_longitude = Input::get('longitude');
            } else {
                $d_longitude = 39.148976;
            }

            // weight float Mandatory
            if (Request::has('weight')) {
                $d_weight = Input::get('weight');
            } else {
                $response_array = array('success' => false, 'error' => 'Weight required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->d_weight = $d_weight;

            // quantity int Mandatory
            if (Request::has('quantity')) {
                $d_quantity = Input::get('quantity');
            } else {
                $response_array = array('success' => false, 'error' => 'Quantity required');
                $response_code = 200;
                goto response;
            }
            $delivery_request->d_quantity = $d_quantity;

            // description strings Optional
            $d_description = '';
            if (Request::has('description')) {
                $d_description = Input::get('description');
            }
            $delivery_request->d_description = $d_description;

            // pikcup city strings Optional
            $pickup_city = $company->city;
            if (Request::has('pickupcity')) {
                $pickup_city = Input::get('pickupcity');
            } 
            $delivery_request->pickup_city = $pickup_city;

            // pickup district strings Optional
            $pickup_district = $company->district;
            if (Request::has('pickupdistrict')) {
                $pickup_district = Input::get('pickupdistrict');
            } 
            $delivery_request->pickup_district = $pickup_district;

            // pickup address strings Optional
            $pickup_address = $company->address;
            if (Request::has('pickupaddress')) {
                $pickup_address = Input::get('pickupaddress');
            } 
            $delivery_request->pickup_address = $pickup_address;

            // pickup latitude double Optional
            $pickup_latitude = $company->latitude;
            if (Request::has('pickuplatitude')) {
                $pickup_latitude = Input::get('pickuplatitude');
            } 
            $delivery_request->pickup_latitude = $pickup_latitude;

            // pickup longitude double Optional
            $pickup_longitude = $company->longitude;
            if (Request::has('pickuplongitude')) {
                $pickup_longitude = Input::get('pickuplongitude');
            } 
            $delivery_request->pickup_longitude = $pickup_longitude;

            $delivery_request->request_start_time = date("Y-m-d H:i:s");
            $delivery_request->company_id = $company->id;
            $delivery_request->service_type = 1; // pickup to deliver 
            $delivery_request->immediate_dropoff = 1;
            $delivery_request->scheduled_dropoff_date = 0;
            $delivery_request->scheduled_dropoff_time = 0;
            $delivery_request->transportation_fees = 25;
            $delivery_request->D_latitude = $d_latitude;
            $delivery_request->D_longitude = $d_longitude;
            $delivery_request->origin_latitude = 21.575723;
            $delivery_request->origin_longitude = 39.148976;
            $delivery_request->pincode = mt_rand(1000, 9999);
            $delivery_request->status = -3;

            $delivery_request->save();
            $waybill = 'OS' . str_pad($delivery_request->id,  8, "0", STR_PAD_LEFT).'KS';

            $dr = DeliveryRequest::find($delivery_request->id);

            $dr->waybill = $waybill;
            $dr->jollychic = $waybill;
            $dr->customer_tracking_number = $dropoff_mobile . '-' . $dr->id;
            $dr->save();


            $OrdersHistory = new OrdersHistory;
            $OrdersHistory->waybill_number = $waybill;
            $order_history->order_number = $dr->order_number;
            $OrdersHistory->status = -3;
            $OrdersHistory->company_id = $company->id;
            $OrdersHistory->save();

            // $curl = new Curl();
            // $sms = 'عزيزي/زتي ';
            // $sms .= $dropoff_name;
            // $sms .= "،";
            // $sms .= "طلبكم من ";
            // $sms .= " " . $merchant_name . " ";
            // $sms .= " في طريقه إلى ";
            // $sms .= $d_city;
            // $sms .= " وسيتواصل معكم مندوب التوزيع في حيكم في أقرب ";
            // $sms .= "وقت مع تحيات ساعي";

            // $destinations1 = "966";
            // $destinations1 .= substr(str_replace(' ', '', $dropoff_mobile), -9);

            // $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            // $urlDiv = explode("?", $url);
            // $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            // Log::info("SMSSERVICELOG" . $result);

            // if (Request::has('mobile2') && $dropoff_mobile != $dropoff_mobile2) {
            //     $destinations2 = "966";
            //     $destinations2 .= substr(str_replace(' ', '', $dropoff_mobile2), -9);
            //     $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

            //     $urlDiv = explode("?", $url);
            //     $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            //     Log::info("SMSSERVICELOG" . $result);
            // }

            return $waybill;

            response:
            $response = Response::json($response_array, $response_code);
            return $response;

        } catch (Exception $e) {

            Log::info($e->getMessage());
            return 'Failed due to an error, please try again.';

        }

    }

    public function compare($s, $r){
        $save1 = $s;
        $save2 = $r;
        $ar1 = preg_split('//u', $s, -1, PREG_SPLIT_NO_EMPTY);
        $ar2 = preg_split('//u', $r, -1, PREG_SPLIT_NO_EMPTY);
        $sz1 = count($ar1);
        $sz2 = count($ar2);
        if($sz1 === 0 || $sz2 === 0)
            return false;
        for($ii = 0; $ii < $sz1; ++$ii) {
            $i = $ii;
            $j = 0;
            while($j < $sz2 && $ar2[$j] === ' ')
                ++$j;
            if( !($i == 0 || ctype_alpha($ar1[$i - 1]) || ($i > 1 && $ar1[$i - 2] === '?' && $ar1[$i - 1] === '?')) )
                continue;
            while($i < $sz1 && $j < $sz2) {
                if(($j == 0 || $ar2[$j - 1] == ' ') && $j + 1 < $sz2 && $ar2[$j] === '?' && $ar2[$j + 1] === '?') { $j += 2; continue; } // just erase '??' from our district (second string), thus you will always find a match
                if($ar1[$i] === $ar2[$j]) { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                break;
            }
            while($j < $sz2 && $ar2[$j] === ' ')
                ++$j;
            if($j == $sz2)
                return true;
        }
        $s = $save2;
        $r = $save1;
        $ar1 = preg_split('//u', $s, -1, PREG_SPLIT_NO_EMPTY);
        $ar2 = preg_split('//u', $r, -1, PREG_SPLIT_NO_EMPTY);
        $sz1 = count($ar1);
        $sz2 = count($ar2);
        if($sz1 === 0 || $sz2 === 0)
            return false;
        for($ii = 0; $ii < $sz1; ++$ii) {
            $i = $ii;
            $j = 0;
            while($j < $sz2 && $ar2[$j] === ' ')
                ++$j;
            if( !($i == 0 || ctype_alpha($ar1[$i - 1]) || ($i > 1 && $ar1[$i - 2] === '?' && $ar1[$i - 1] === '?')) )
                continue;
            while($i < $sz1 && $j < $sz2) {
                if(($j == 0 || $ar2[$j - 1] == ' ') && $j + 1 < $sz2 && $ar2[$j] === '?' && $ar2[$j + 1] === '?') { $j += 2; continue; } // just erase ?? from our district, thus you will always find a matching
                if($ar1[$i] === $ar2[$j]) { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; } if($ar1[$i] === '?' && $ar2[$j] === '?') { ++$i; ++$j; continue; }
                break;
            }
            while($j < $sz2 && $ar2[$j] === ' ')
                ++$j;
            if($j == $sz2)
                return true;
        }
        return false;
    }

    public function districtAnalysis($address, $city){
        $districts = DB::select("select city, district 'english', district_ar 'arabic', google_district 'g_english', google_district_ar 'g_arabic' from districts where district <> 'No District' and ( SUBSTRING_INDEX(district, '+', 1) = '$city' or city = '$city' )");
        if(count($districts) == 0)
            return 'No District';
        $city_ar = $districts[0]->arabic;
        $city_ar = mb_split("\\+", $city_ar)[0];
        $grouped = City::select('grouped')->where('name', '=', $districts[0]->city)->where('grouped', '=', '1')->count('grouped');
        for($i = 0; $i < count($districts); ++$i){
            $en = $districts[$i]->english;
            $ar = $districts[$i]->arabic;
            $g_en = $districts[$i]->g_english;
            $g_ar = $districts[$i]->g_arabic;
            // compare with original districts
            if($grouped == 1) {
                $en = mb_split("\\+", $en);
                $ar = mb_split("\\+", $ar);

                // ($this->compare($address, $en[0])): when name of the city is the first part of subcity, ignore comparing the first part
                if(count($en) >= 1 && $this->compare($city, $en[0]) == 0 && $this->compare($address, $en[0]) || count($ar) >= 1 && $this->compare($city_ar, $ar[0]) == 0 && $this->compare($address, $ar[0]))
                    return $en[1];
                if(count($en) >= 2 && $this->compare($address, $en[1]) || count($ar) >= 2 && $this->compare($address, $ar[1]))
                    return $en[1];
            }
            else {
                if($this->compare($address, $en) || $this->compare($address, $ar))
                    return $en;
            }
            // compare with google districts
            if($city == 'Baha') {
                $g_en = mb_split("\\+", $g_en);
                $g_ar = mb_split("\\+", $g_ar);
                if(count($g_en) >= 1 && $this->compare($address, $g_en[0]) || count($g_ar) >= 1 && $this->compare($address, $g_ar[0]))
                    return $en[1];
                if(count($g_en) >= 2 && $this->compare($address, $g_en[1]) || count($g_ar) >= 2 && $this->compare($address, $g_ar[1]))
                    return $en[1];
            }
            else {
                if($this->compare($address, $g_en) || $this->compare($address, $g_ar))
                    return $grouped == 1 ? $en[1] : $en;
            }
        }
        return 'No District';
    }

    public function convertdistrictlanguage($cur, $city){
        $saved = $cur;
        $grouped = City::where('name', '=', $city)->where('grouped', '=', '1')->count();
        $cur = preg_split('//u', $cur, -1, PREG_SPLIT_NO_EMPTY);

        // modify the string
        $res = '';
        for($j = 0; $j < count($cur); ++$j){

            // remove '??'
            if($j == 0 && $j + 2 < count($cur) && $cur[$j] === '?' && $cur[$j + 1] === '?' && $cur[$j + 2] == ' '){ ++$j; continue; }
            if($j + 3 < count($cur) && $cur[$j] == ' ' && $cur[$j + 1] == '?' && $cur[$j + 2] == '?' && $cur[$j + 3] == ' '){ $j += 2; continue; }

            // remove '????'
            if($j == 0 && $j + 4 < count($cur) && $cur[$j] === '?' && $cur[$j + 1] === '?' && $cur[$j + 2] == '?' && $cur[$j + 3] == '?' && $cur[$j + 4] == ' '){ $j += 3; continue; }
            if($j + 5 < count($cur) && $cur[$j] == ' ' && $cur[$j + 1] == '?' && $cur[$j + 2] == '?' && $cur[$j + 3] == '?' && $cur[$j + 4] == '?' && $cur[$j + 5] == ' '){ $j += 4; continue; }

            // skip special characters
            if($cur[$j] == ' ' || $cur[$j] == '\'')
                continue;

            $res .= $cur[$j];
        }

        // match with current districts
        $q = DB::select("select district, district_ar from districts where city = '$city'");
        for($i = 0; $i < count($q); ++$i){
            if($grouped == 0 && $this->compare($q[$i]->district_ar, $res))
                return $q[$i]->district;
            if($grouped == 1){
                $dis = mb_split("\\+", $q[$i]->district_ar)[1];
                if($this->compare($dis, $res))
                    return mb_split("\\+", $q[$i]->district)[1];
            }
        }

        // return the same district if cant find a match
        return $saved;
    }

    public function removenonalphachars($string) {
        for($i = 0; $i < strlen($string); ++$i){
            $c = $string[$i];
            if($c == '-' || $c == ',' || $c == '|' || $c == '\'' || $c == '"' || $c == '!' || $c == '@' || $c == '#' || $c == '$' || $c == '%' || $c == '^' || $c == '&' || $c == '*' || $c == '(' ||
                $c == ')' || $c == '[' || $c == ']' || $c == '{' || $c == '}' || $c == '.' || $c == '<' || $c == '>' || $c == '+' || $c == '/' || $c == '\\' || $c == '_' || $c == '~')
                $string[$i] = ' ';
        }
        return $string;
    }

    public function updateDistricts() {

        try {

            ini_set('max_execution_time', 70000);
    
            $company_id = Request::get('company_id');
            $city = Request::get('city');
            $waybill = Request::get('waybill');
            $created_at = Request::get('created_at');
            $from = Request::get('from');
            $to = Request::get('to');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');
            $status = Request::get('status');
            $update = Request::get('update');
            $limit = Request::get('limit');
            $count_only = Request::get('count_only');
            
            $no_districts = "(d_district != 'No District' and ";
            if(isset($city) && $city != 'All') {
                $city = str_replace("'", "''", $city);
                $no_districts .= "d_district not in (select distinct substring_index(district,'+',-1) from districts where city = \"$city\")";
            }
            else 
                $no_districts .= "d_district not in (select distinct substring_index(district,'+',-1) from districts)";
            $no_districts .= ")";

            $d_address = DeliveryRequest::select(['jollychic', 'd_city', 'd_district', 'd_address', 'd_latitude', 'd_longitude', 'created_at'])
                ->whereRaw($no_districts)
                ->whereNotIn('status', array(0, 4, 6, 5, 7))
                ->orderBy('created_at', 'desc');
            if(isset($company_id) && $company_id != 'all')
                $d_address = $d_address->where('company_id', '=', $company_id);
            if(isset($city) && $city != 'All') {
                $city = str_replace("'", "''", $city);
                $d_address = $d_address->whereRaw("(d_city = \"$city\" or d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) 'city' from districts where city = \"$city\"))");
            }
            if(isset($waybill))
                $d_address = $d_address->where('jollychic', '=', $waybill);
            if(isset($from) && isset($to) && $from != '' && $to != '')
                $d_address = $d_address->whereRaw("(date(created_at) between '$from' and '$to')");
            if(isset($created_at))
                $d_address = $d_address->whereRaw("(date(created_at) = '$created_at')");
            if(isset($scheduled_shipment_date) && $scheduled_shipment_date != '')
                $d_address = $d_address->where('scheduled_shipment_date', '=', $scheduled_shipment_date);
            if(isset($status) && $status != '')
                $d_address = $d_address->whereRaw("(status in $status)");
            if(isset($limit))
                $d_address = $d_address->limit($limit);
            if(isset($count_only) && $count_only == '1') {
                $d_address = $d_address->count();
            }
            if(!isset($update) || $update == '0') {
                $d_address = $d_address->get();
                $response_array = array('success' => true, 'count' => count($d_address), 'details' => $d_address);
                goto response;
            }
            $d_address = $d_address->get();
            
            $cities = City::where('name', '<>', 'All')->get();
            $suc = 0;
            $fail = 0;
            $apikey = '';
            for($i = 0; $i < count($d_address); ++$i){
    
                $jollychic = $d_address[$i]->jollychic;
    
                // get the address
                $city = $old_city = $d_address[$i]->d_city;
                $city = str_replace("'", "''", $city);
                $q = DB::select("select city, district 'english', district_ar 'arabic', google_district 'g_english', google_district_ar 'g_arabic' from districts where district <> 'No District' and (SUBSTRING_INDEX(district, '+', 1) = \"$city\" or city = \"$city\" )");
                if(!count($q)) 
                    goto FAILED;
                $grouped = City::select('grouped')->where('name', '=', $q[0]->city)->where('grouped', '=', '1')->count('grouped');
                $city_ar = City::select('name_ar')->where('name', '=', $q[0]->city)->get()[0]->name_ar;
                $city_en = City::select('name')->where('name', '=', $q[0]->city)->get()[0]->name;
                if($grouped == 1)
                    $city = mb_split('\\+', $q[0]->g_english)[0];
                $u_address = $d_address[$i]->d_address;
                $u_address = $this->removenonalphachars($u_address);
    
                // initialize variables
                $district = $this->districtAnalysis($u_address, $old_city);
                $found = ($district != 'No District');
                $route = '';
                $found_location = 0; // 0: no location, 1: from original record, 2: from google request
                if(abs($d_address[$i]->d_latitude) > 1e-40){
                    $lat = $d_address[$i]->d_latitude;
                    $lng = $d_address[$i]->d_longitude;
                    $found_location = 1;
                }
                else
                    $lat = $lng = '';
    
                // make request to google api
                $param = $city . '+' . $u_address;
                $conv = urlencode($param);
                $request = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $conv . "&key=AIzaSyAnXcbFPP4YdjHEMr1kTZbv9HrEKZoIZI8&language=en";
                $client = new GuzzleHttp\Client();
                $result = $client->get($request, ['auth' => ['user', 'pass']]);
                $json = $temp1 = json_decode($result->getBody(), true);
    
                $responsecity = $city;
                if (isset($json['results'][0]['address_components'])) {
                    $address_components = $json['results'][0]['address_components'];
                    foreach ($address_components as $address) {
                        if (isset($address['types'])) {
                            $types = $address['types'];
                            foreach ($types as $type) {
                                // the city
                                if ($type == 'locality' || $type == 'city') {
                                    $responsecity = $address['long_name'];
                                    if($responsecity == 'Mecca')
                                        $responsecity = 'Makkah';
                                    break;
                                }
                            }
                        }
                    }
                }
    
                // parse the body of response
                if (($this->compare($responsecity, $city_en) || $this->compare($responsecity, $city_ar)) && isset($json['results'][0]['address_components'])) {
                    $address_components = $json['results'][0]['address_components'];
                    if ($found_location == 0 && isset($json['results'][0]['geometry'])) {
                        $geometry = $json['results'][0]['geometry'];
                        if (isset($geometry['location'])) {
                            $location = $geometry['location'];
                            // location of the address
                            $lat = $location['lat'];
                            $lng = $location['lng'];
                            $found_location = 2;
                        }
                    }
                    if(!$found) {
                        foreach ($address_components as $address) {
                            if (isset($address['types'])) {
                                $types = $address['types'];
                                foreach ($types as $type) {
                                    if (strcmp($type, 'sublocality') === 0 || strcmp($type,'neighborhood') === 0 || strcmp($type,'sublocality_level_1') === 0 ) {
                                        $district = $address['long_name'];
                                        $found = true;
                                        break;
                                    }
                                    if(strcmp($type, 'route') === 0) {
                                        $route = $address['long_name'];
                                    }
                                }
                            }
                            if ($found)
                                break;
                        }
                    }
                }
    
                // if the district or location not found so far, get the district by reverse geocoding from the location
                if ($found_location == 2 && ($this->compare($responsecity, $city_en) || $this->compare($responsecity, $city_ar)) || $lat != '' && $lng != '') {
    
                    // make request
                    $u_latlng = $lat . ',' . $lng;
                    $request = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . urlencode($u_latlng) . "&key=AIzaSyAnXcbFPP4YdjHEMr1kTZbv9HrEKZoIZI8&language=en";
                    $client = new GuzzleHttp\Client();
                    $result = $client->get($request, ['auth' => ['user', 'pass']]);
                    $json = $temp2 = json_decode($result->getBody(), true);
    
                    $responsecity = $city;
                    if (isset($json['results'][0]['address_components'])) {
                        $address_components = $json['results'][0]['address_components'];
                        foreach ($address_components as $address) {
                            if (isset($address['types'])) {
                                $types = $address['types'];
                                foreach ($types as $type) {
                                    // the city
                                    if ($type == 'locality' || $type == 'city') {
                                        $responsecity = $address['long_name'];
                                        if($responsecity == 'Mecca')
                                            $responsecity = 'Makkah';
                                        break;
                                    }
                                }
                            }
                        }
                    }
    
                    // parse the response for districts
                    if (!$found && ($this->compare($responsecity, $city_en) || $this->compare($responsecity, $city_ar)) && isset($json['results'][0]['address_components'])) {
                        $address_components = $json['results'][0]['address_components'];
                        foreach ($address_components as $address) {
                            if (isset($address['types'])) {
                                $types = $address['types'];
                                foreach ($types as $type) {
                                    // the district
                                    if (strcmp($type, 'sublocality') === 0 || strcmp($type,'neighborhood') === 0 || strcmp($type,'sublocality_level_1') === 0 ) {
                                        $district = $address['long_name'];
                                        $found = true;
                                        break;
                                    }
                                }
                            }
                            if ($found)
                                break;
                        }
                    }
                }
    
                if(!$found){
                    FAILED:
                    ++$fail;
                    DB::update("update delivery_request set d_district = ? where jollychic = ?", ['No District', $jollychic]); 
    
                    // for debugging
    //                echo 'failed update<br/>';
                }
                else {
                    ++$suc;
                    $d_address[$i]->district = $district;
                    $district = $this->convertdistrictlanguage($district, $city);
                    DB::update("update delivery_request set d_district = ? , D_latitude = ? , D_longitude = ? , is_address_updated = ? where jollychic = ?", [$district, $lat, $lng, $found_location, $jollychic]);
                    // found_location: 1 -> originally there, 2-> from google request
                    
                    // for debugging
    //                echo 'success update<br/>';
                }
    
                // for debugging
    //            echo "$u_address <br/> DISTRICT: $district <br/> LOCATION: { LAT: $lat, LNG: $lng } <br/><hr/>";
    
                $temp1['ORINGIAL_ADDRESS'] = $u_address;
                $temp1['OUR_DISTRICTS_COMPARING'] = $district;
                $temp1['LATITUDE'] = $lat;
                $temp1['LONGITUDE'] = $lng;
                $temp1['FOUND_LOCATION'] = $found_location;
    
                $temp2['ORINGIAL_ADDRESS'] = $u_address;
                $temp2['OUR_DISTRICTS_COMPARING'] = $district;
                $temp2['LATITUDE'] = $lat;
                $temp2['LONGITUDE'] = $lng;
                $temp2['FOUND_LOCATION'] = $found_location;
            }
    
            // for debugging
    //        return "Sucess: $suc <br/> Fail: $fail";
            
            $response_array = array('success' => true, 'items_count' => count($d_address), 'success_items' => $suc, 'failed_items' => $fail);

            response:
            $response = Response::json($response_array, 200);
            return $response;

        } catch (Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage());
            $response = Response::json($response_array, 200);
            return $response;
            
        }
        
    }

}