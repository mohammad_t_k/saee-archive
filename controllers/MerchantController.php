<?php


use App\Classes\Curl;

class MerchantController extends BaseController
{
    public function __construct()
    {
        $this->beforeFilter(function () {

            if(!Session::has('merchant_id'))
                return Redirect::to('/merchant/login');

        }, array('except' => array('login', 'verify', 'registerMerchant', 'registerMerchantPost', 'updateStatus', 'welcome')));
    }

    public function index() {
        return Redirect::to('/merchant/login');
    }

    public function verify() {

        $username = Input::get('username');
        $password = Input::get('password');
        $merchant = Companies::where('username', '=', $username)->orWhere('email', '=', $username)->first();

        if ($merchant && Hash::check($password, $merchant->password)) {

            Session::put('merchant_id', $merchant->id);
            return Redirect::to('/merchant/dashboard');

        } else {
            return Redirect::to('/merchant/login')->with('error', 'Wrong Username or Password');
        }

    }

    public function login() {

        if(Session::has('merchant_id'))
            return Redirect::to('/merchant/dashboard');
        return View::make('merchants.login');

    }

    public function logout() {
        Session::flush();
        return Redirect::to('/merchant');
    }

    public function welcome() {

        $id = Request::get('id');
        $user = Users::where('id', '=', $id)->first();
        return View::make('merchants.welcome')
            ->with('user', $user);
        
    }

    public function dashboard() {
        
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $adminCities = City::select('name as city')->orderBy('name', 'asc')->distinct()->get();
        $city = Request::get('city');
        if(!isset($city))
            $city = 'All';

        $all_items = DeliveryRequest::where('company_id', '=', $merchant_id);
        $to_be_pickedup = DeliveryRequest::where('company_id', '=', $merchant_id)->where('status', '=', '-3');
        $reserved_to_pickup = DeliveryRequest::where('company_id', '=', $merchant_id)->where('status', '=', '-2');
        $pickedup = DeliveryRequest::where('company_id', '=', $merchant_id)->where('status', '=', '-1');
        $returned_to_supplier = DeliveryRequest::where('company_id', '=', $merchant_id)->where('status', '=', '7');
        $in_route = DeliveryRequest::where('company_id', '=', $merchant_id)->whereIn('status', (2));
        $in_warehouse = DeliveryRequest::where('company_id', '=', $merchant_id)->whereIn('status', array('3'));
        $with_captains = DeliveryRequest::where('company_id', '=', $merchant_id)->whereIn('status', array('4', '6'));
        $delivered = DeliveryRequest::where('company_id', '=', $merchant_id)->where('status', '=', '5');

        if($city != 'All') {

            $grouped = City::where('name', '=', $city)->where('grouped', '=', '1')->count();
            
            if($grouped) {

                $all_items = $all_items->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $to_be_pickedup = $to_be_pickedup->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $reserved_to_pickup = $reserved_to_pickup->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $pickedup = $pickedup->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $returned_to_supplier = $returned_to_supplier->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $in_route = $in_route->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $in_warehouse = $in_warehouse->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $with_captains = $with_captains->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                $delivered = $delivered->whereRaw("d_city in (select distinct SUBSTRING_INDEX(district, '+', 1) from districts where city = '$city')");
                
            } else {

                $all_items = $all_items->where('d_city', '=', $city);
                $to_be_pickedup = $to_be_pickedup->where('d_city', '=', $city);
                $reserved_to_pickup = $reserved_to_pickup->where('d_city', '=', $city);
                $pickedup = $pickedup->where('d_city', '=', $city);
                $returned_to_supplier = $returned_to_supplier->where('d_city', '=', $city);
                $in_route = $in_route->where('d_city', '=', $city);
                $in_warehouse = $in_warehouse->where('d_city', '=', $city);
                $with_captains = $with_captains->where('d_city', '=', $city);
                $delivered = $delivered->where('d_city', '=', $city);
                
            }
            
        }

        $all_items = $all_items->count();
        $to_be_pickedup = $to_be_pickedup->count();
        $reserved_to_pickup = $reserved_to_pickup->count();
        $pickedup = $pickedup->count();
        $returned_to_supplier = $returned_to_supplier->count();
        $in_route = $in_route->count();
        $in_warehouse = $in_warehouse->count();
        $with_captains = $with_captains->count();
        $delivered = $delivered->count();
        
        return View::make('merchants.dashboard')
            ->with('merchant', $merchant)
            ->with('adminCities', $adminCities)
            ->with('city', $city)
            ->with('allitems', $all_items)
            ->with('tobepickedup', $to_be_pickedup)
            ->with('reservedtopickup', $reserved_to_pickup)
            ->with('pickedup', $pickedup)
            ->with('returnedtocitems', $returned_to_supplier)
            ->with('onwayitems', $in_route)
            ->with('arriveditems', $in_warehouse)
            ->with('withcaptains', $with_captains)
            ->with('delivreditems', $delivered);
    }

    public function getorders() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        $shipments = DeliveryRequest::where('company_id', '=', $merchant_id)->get();
        return View::make('merchants.orders')
            ->with('merchant', $merchant)
            ->with('shipments', $shipments);
    }

    public function shipmentdetails() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        $waybill = Request::segment(3);
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->orWhere('order_number', '=', $waybill)->first();

        return View::make('merchants.shipmentdetails')
            ->with('merchant', $merchant)
            ->with('shipment', $shipment);
            
    }

    public function registerMerchant() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        $walker = DB::select('select * from walker where id = 1980');
        $cities = City::all();
        return View::make('merchants.register')
            ->with('walker', $walker)
            ->with('iscurrentlyproviding', 0)
            ->with('type', 0)
            ->with('merchant', $merchant)
            ->with('cities', $cities);
    }

    public function registerMerchantPost() {

        try {

            $name = Request::get('name');
            $phone = Request::get('phone');
            $email = Request::get('email');
            $has_cr = Request::get('has_cr');
    
            $user = new Users;
            $user->name = $name;
            $user->phone = $phone;
            $user->email = $email;
            $user->has_cr = $has_cr;
            $user->save();
            
        } catch(Exception $e) {

            $response = 'Failed due to an error, Please try again!';
            $response = $e->getMessage();
            return $response;
            
        }

        return Redirect::to('/merchant/welcome?id=' . $user->id);
        
    }

    public function registerMerchantPostOld() {

        $name = Request::get('name');
        $name_ar = Request::get('name_ar');
        $email = Request::get('email');
        $username = Request::get('username');
        $password = Request::get('password');
        $mobile = Request::get('mobile');
        $city = Request::get('city');
        $district = Request::get('district');
        $address = Request::get('address');
        $telegram = Request::get('telegram');
        $whatsapp = Request::get('whatsapp');
        $instagram = Request::get('instagram');
        $twitter = Request::get('twitter');
        $googleplus = Request::get('googleplus');
        $facebook = Request::get('facebook');
        $snapchat = Request::get('snapchat');
        $latitude = Request::get('address_latitude');
        $longitude = Request::get('address_longitude');
        $idnumber = Request::get('idnumber');
        $idphoto = Input::file('idphoto')->getRealPath();
        if($idphoto != '') {
           $last4digits = substr($mobile, -4);
           Cloudder::upload($idphoto, 'picture_' . $last4digits . '');
           $photo_url = Cloudder::getResult();
           $idscan = $photo_url['url'];
        }

        $merchant = new Merchants;
        $merchant->name = $name;
        $merchant->name_ar = $name;
        $merchant->email = $email;
        $merchant->username = $username;
        $merchant->password = Hash::make($password);
        $merchant->mobile = $mobile;
        $merchant->city = $city;
        $merchant->district = $district;
        $merchant->address = $address;
        $merchant->telegram = $telegram;
        $merchant->whatsapp = $whatsapp;
        $merchant->instagram = $instagram;
        $merchant->twitter = $twitter;
        $merchant->googleplus = $googleplus;
        $merchant->facebook = $facebook;
        $merchant->snapchat = $snapchat;
        $merchant->latitude = $latitude;
        $merchant->longitude = $longitude;
        $merchant->id_number = $idnumber;
        $merchant->id_scan = $idscan;
        $merchant->save();

        Session::put('merchant_id', $merchant->id);
        return Redirect::to('/merchant/dashboard');
    }

    public function forgetPassword() {
        return 'forgetpassword';
    }

    public function insertRecord() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        return View::make('merchants.insertrecord')
            ->with('merchant', $merchant);
    }

    public function insertRecordPost() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $customer_name = Request::get('customer_name');
        $mobile1 = Request::get('mobile1');
        $mobile2 = Request::get('mobile2');
        $email = Request::get('email');
        $city = Request::get('city');
        $district = Request::get('district');
        $address = Request::get('address');
        $street_address = Request::get('street_address');
        $shipment_description = Request::get('shipment_description');
        $qunatity = Request::get('quantity');
        $weight = Request::get('weight');
        $order_number = Request::get('order_number');
        $address_latitude = Request::get('address_latitude');
        $address_longitude = Request::get('address_longitude');

        $delivery_request = new DeliveryRequest;
        $delivery_request->order_number = $order_number;
        $delivery_request->d_description = $shipment_description;
        $delivery_request->d_quantity = $qunatity;
        $delivery_request->d_weight = $weight;
        $delivery_request->receiver_name = $customer_name;
        $delivery_request->receiver_phone = $mobile1;
        $delivery_request->receiver_phone2 = $mobile2;
        $delivery_request->receiver_email = $email;
        $delivery_request->d_city = $city;
        $delivery_request->d_district = $district;
        $delivery_request->d_address = $address;
        $delivery_request->d_address2 = $street_address;
        $delivery_request->D_latitude = $address_latitude;
        $delivery_request->D_longitude = $address_longitude;

        $delivery_request->company_id = $merchant_id;
        $delivery_request->service_type = 1;
        $delivery_request->status = -3;
        $delivery_request->pincode = mt_rand(1000, 9999);
        $delivery_request->new_shipment_date = date('Y-m-d H:i:s');
        $delivery_request->pickup_city = $merchant->city;
        $delivery_request->pickup_district = $merchant->district;
        $delivery_request->pickup_address = $merchant->address;
        $delivery_request->pickup_latitude = $merchant->latitude;
        $delivery_request->pickup_longitude = $merchant->longitude;
        $delivery_request->scheduled_pickup_date = date('Y-m-d');
        $delivery_request->save();

        $dr = DeliveryRequest::find($delivery_request->id);
        $waybill = 'ME' . str_pad($dr->id,  8, "0", STR_PAD_LEFT).'KS';
        $dr->waybill = $waybill;
        $dr->jollychic = $waybill;
        $dr->save();

        $orders_history = new OrdersHistory;
        $orders_history->waybill_number = $dr->waybill;
        $orders_history->status = $dr->status;
        $orders_history->company_id = $merchant_id;
        $orders_history->save();

        return Redirect::to('/merchant/pickupInfo/' . $waybill);
    }

    public function editshipment() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $formSubmit = Request::get('formSubmit');

        if(!isset($formSubmit)) {

            return View::make('merchants.editshipment')
                ->with('merchant', $merchant);
        }

        $waybill = Request::get('waybill');
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->orWhere('order_number', '=', $waybill)->first();
        $error = null;
        if(!$shipment) {
            $error = "shipment not exist";
        }
        else if($shipment->status != -3){
            if($shipment->status == -2)
                $error = 'Shipment is reserved by captain';
            else 
                $error = 'You can not edit this shipment';
        }
        if($error) {
            return View::make('merchants.editshipment')
                ->with('waybill', $waybill)
                ->with('error', $error)
                ->with('merchant', $merchant);
        }
        else {
            return Redirect::to('/merchant/editShipmentInfo/' . $waybill);
        }
    }

    public function editPickup() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $formSubmit = Request::get('formSubmit');

        if(!isset($formSubmit)) {

            return View::make('merchants.editpickup')
                ->with('merchant', $merchant);
        }

        $waybill = Request::get('waybill');
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->orWhere('order_number', '=', $waybill)->first();
        $error = null;
        if(!$shipment) {
            $error = "shipment not exist";
        }
        else if($shipment->status != -3){
            if($shipment->status == -2)
                $error = 'Shipment is reserved by captain';
            else 
                $error = 'You can not edit this shipment';
        }
        if($error) {
            return View::make('merchants.editpickup')
                ->with('waybill', $waybill)
                ->with('error', $error)
                ->with('merchant', $merchant);
        }
        else {
            return Redirect::to('/merchant/pickupInfo/' . $waybill);
        }
        
    }

    public function editshipmentinfo() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $waybill = Request::segment(3);
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();

        if(false && $shipment->company_id != $merchant_id) {
            return View::make('merchants.editshipment')
                ->with('error', 'You do not have access to this shipment')
                ->with('merchant', $merchant);
        }

        return View::make('merchants.editshipmentinfo')
            ->with('shipment', $shipment)
            ->with('waybill', $waybill)
            ->with('merchant', $merchant);
    }

    public function editshipmentinfopost() {

        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);

        $waybill = Request::get('waybill');
        $customer_name = Request::get('customer_name');
        $mobile1 = Request::get('mobile1');
        $mobile2 = Request::get('mobile2');
        $email = Request::get('email');
        $city = Request::get('city');
        $district = Request::get('district');
        $address = Request::get('address');
        $street_address = Request::get('street_address');
        $shipment_description = Request::get('shipment_description');
        $qunatity = Request::get('quantity');
        $weight = Request::get('weight');
        $order_number = Request::get('order_number');
        $address_latitude = Request::get('address_latitude');
        $address_longitude = Request::get('address_longitude');

        if($customer_name != '' && $customer_name != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'receiver_name' => $customer_name
            ));
        }
        if($mobile1 != '' && $mobile1 != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'receiver_phone' => $mobile1
            ));
        }
        if($mobile2 != '' && $mobile2 != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'receiver_phone2' => $mobile2
            ));
        }
        if($email != '' && $email != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'receiver_email' => $email
            ));
        }
        if($city != '' && $city != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_city' => $city
            ));
        }
        if($district != '' && $district != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_district' => $district
            ));
        }
        if($address != '' && $address != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_address' => $address
            ));
        }
        if($street_address != '' && $street_address != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_address2' => $street_address
            ));
        }
        if($shipment_description != '' && $shipment_description != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_description' => $shipment_description
            ));
        }
        if($qunatity != '' && $qunatity != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_quantity' => $qunatity
            ));
        }
        if($weight != '' && $weight != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'd_weight' => $weight
            ));
        }
        if($order_number != '' && $order_number != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'order_number' => $order_number
            ));
        }
        if($address_latitude != '' && $address_latitude != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'D_latitude' => $address_latitude
            ));
        }
        if($address_longitude != '' && $address_longitude != ' ') {
            DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
                'D_longitude' => $address_longitude
            ));
        }
        

        return Redirect::to('/merchant/viewShipment/' . $waybill);
    }

    public function pickupInfo() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        $waybill = Request::segment(3);
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        return View::make('merchants.pickupinfo')
            ->with('merchant', $merchant)
            ->with('shipment', $shipment)
            ->with('waybill', $waybill);
    }

    public function pickupInfoPost() {
        
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::where('id', '=', $merchant_id)->first();
        $waybill = Request::get('waybill');
        $city = Request::get('city');
        $district = Request::get('district');
        $address = Request::get('address');
        $address_latitude = Request::get('address_latitude');
        $address_longitude = Request::get('address_longitude');

        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
            'pickup_city' => $city
        ));
        DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
            'pickup_district' => $district
        ));
        DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
            'pickup_address' => $address
        ));
        DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
            'pickup_latitude' => $address_latitude
        ));
        DeliveryRequest::where('jollychic', '=', $waybill)->update(array(
            'pickup_longitude' => $address_longitude
        ));

        return Redirect::to('/merchant/dashboard');
    }

    public function trackShipment() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        return View::make('merchants.tracking')
            ->with('merchant', $merchant)
            ->with('shipment', [])
            ->with('waybill', '');
    }

    public function getStatus($status) {
        if($status == -3)
            return array('number'=>$status, 'text'=>'Created', 'percent'=>14);
        if($status == -2)
            return array('number'=>$status, 'text'=>'Assigned To Captain', 'percent'=>28);
        if($status == -1)
            return array('number'=>$status, 'text'=>'In route With Captain', 'percent'=>42);
        if($status == 2)
            return array('number'=>$status, 'text'=>'In route', 'percent'=>56);
        if($status == 3)
            return array('number'=>$status, 'text'=>'In Warehouse', 'percent'=>70);
        if($status == 4)
            return array('number'=>$status, 'text'=>'With Captain', 'percent'=>84);
        if($status == 5)
            return array('number'=>$status, 'text'=>'Delivered', 'percent'=>100);
        if($status == 6)
            return array('number'=>$status, 'text'=>'UnDelivered', 'percent'=>84);
        if($status == 7)
            return array('number'=>$status, 'text'=>'Returned To Merchant', 'percent'=>1);
    }

    public function trackShipmentPost() {
        $merchant_id = Session::get('merchant_id');
        $merchant = Companies::find($merchant_id);
        $waybill = Request::get('waybill');
        $shipment = DeliveryRequest::where('jollychic', '=', $waybill)->first();
        return View::make('merchants.tracking')
            ->with('merchant', $merchant)
            ->with('shipment', $shipment)
            ->with('waybill', $waybill)
            ->with('progress', $this->getStatus($shipment->status));
    }

    public function test() {
        return 'RRRR';
    }

    public function bulkTrackShipment() {
        return 'bulk';
    }

    public function bulkTrackShipmentPost() {
        return 'bulk post';
    }

    public function updateStatus() {

        try {

            if(!Request::has('merchant_id'))
                return 'Failed';
            $merchant_id = Session::get('merchant_id');
            $merchant = Companies::find($merchant_id);
            $status = Request::get('to');
            $waybill = Request::get('waybill');

            if(Request::has('captainid'))
                $captainid = Request::get('captainid');
            else
                $captainid = 0;
            
            $shipment = DeliveryRequest::find($waybill);

            if(isset($shipment)) {
                $oldWalker1 = $shipment->confirmed_walker1;
                $oldWalker2 = $shipment->confirmed_walker2;
                $oldStatus = $shipment->status;
                $merchants_order_history = new MerchantsOrdersHistory;

                if($status >= -3 && $status <= 7 && $staus != 0 && $status != 2) {

                    if($oldStatus == $status)
                        continue;
                    else {
                        $merchants_order_history->waybill = $waybill;
                        $merchants_order_history->status = $status;
                        if(Request::has('merchant_id'))
                            $merchants_order_history->merchant_id = Request::get('merchant_id');
                        else if(Request::has('admin_id'))
                            $merchants_order_history->admin_id = Request::get('admin_id');
                        else if($captainid > 0)
                            $merchants_order_history->walker_id = $captainid;
                    }

                    $statusDef = array(
                        "-3" => 'To Be Picked Up',
                        "-2" => 'Reserved To Pick Up',
                        "-1" => 'Picked Up',
                        "2" => 'In Warehouse',
                        "3" => 'In Warehouse',
                        "4" => 'With Captains',
                        "5" => 'Delivered',
                        "6" => 'UnDelivered',
                        "7" => 'Returned to Supplier'
                    );

                    if($status == -3) {

                        if($oldStatus == -2) { // if reserved by captain then he dropped his reservation
                            DeliveryRequest::where('waybill', '=', $waybill)->update(array(
                                'status' => $status
                            ));
                        }

                    }
                    // this request is coming from captain mobile to reseve an item
                    else if($status == -2 && $oldStatus == -3) {

                        DeliveryRequest::where('waybill', '=', $waybill)->update(array(
                            'status' => $status, 'confirmed_walker1' => $captainid, 'reserve_date' => date('Y-m-d H:i:s')
                        ));

                    }
                    else if($status == -1 && $oldStatus == -2) {

                        DeliveryRequest::where('waybill', '=', $waybill)->update(array(
                            'status' => $status, 'pickup_date' => date('Y-m-d H:i:s')
                        ));

                    }
                    else if($status == 3) {

                        DeliveryRequest::where('waybill', '=', $waybill)->update(array(
                            'status' => $status, 'inwarehouse_date' => date('Y-m-d H:i:s')
                        ));
                        if($oldStatus == 4) { // returned
                            DeliveryRequest::where('waybill', '=', $waybill)->update(array(
                                'confirmed_walker2' => '0', 'planned_walker2' => '0'
                            ));
                        }

                    }

                }

                if($captainid > 0){
                    $delivered = DB::select("select count(dr.id) 'delivered' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 5 and dr.is_money_received = 0")[0]->delivered;
                    $with_captain = DB::select("select count(delivery_request.id) 'with_captain' from delivery_request where confirmed_walker = $captainid and status = 4")[0]->with_captain;
                    $returned = DB::select("select count(dr.id) 'returned' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 6")[0]->returned;
                    $last_payment_date = DB::select("SELECT max(date) 'last_payment_date' FROM captain_account_history as cahd WHERE cahd.captain_id = $captainid")[0]->last_payment_date;
                    $balance = DB::select("select sum(cah.captain_amount) 'balance' from captain_account_history cah where cah.captain_id = $captainid and cah.paid_to_captain = 0")[0]->balance;
                    $pending_since = "select min(PendingSince) 'pendingsince' from (
                        select min(pickup_timestamp) 'PendingSince' from delivery_request where status in (4,6)  and confirmed_walker = $captainid
                        union 
                        select min(dropoff_timestamp) 'PendingSince' from delivery_request where status = 5 and is_money_received = 0  and confirmed_walker = $captainid
                        ) A";
                    $pending_since = DB::select($pending_since)[0]->pendingsince;
                    if($pending_since == NULL)
                        $pending_since = '0000-00-00 00:00:00';
                    $delayedcountquery = "select waybill_number 'DelayedShipments' from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 5 union select waybill_number 'DelayedShipments' from orders_history where status = 5 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 5 and is_money_received = 0 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 5 union select waybill_number 'DelayedShipments' from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 5 ";
                    $delayedcount = DB::select($delayedcountquery);
                    $delayedcountarray = '(';
                    for($i = 0; $i < count($delayedcount); ++$i)
                        if($i == 0)
                            $delayedcountarray .= "'".$delayedcount[$i]->DelayedShipments."'";
                        else
                            $delayedcountarray .= ','."'".$delayedcount[$i]->DelayedShipments."'";
                    $delayedcountarray .= ')';
                    if(count($delayedcount) > 0)
                        $delayedamount = DB::select("select sum(cash_on_delivery) 'delayedamount' from delivery_request where jollychic in $delayedcountarray")[0]->delayedamount;
                    else
                        $delayedamount = 0;
                    DB::update("update walker set delivered = ? , with_captain = ? , returned = ? , last_payment_date = ? , balance = ? , pending_since = ? , delayed_count = ? , delayed_amount = ? where id = ?", [$delivered, $with_captain, $returned, $last_payment_date, $balance, $pending_since, count($delayedcount), $delayedamount, $captainid]);
                }

                $response_array = array(
                'success' => true,
                'number_of_found_items' => $counter,
                'number_of_unfound_items' => $not_found,
                'item_not_database' => implode(",", $item_not_database));
                $response = Response::json($response_array, 200);
                return $response;
            }
        } catch (Exception $e) {
            return 'Exception';
        }
    }

}