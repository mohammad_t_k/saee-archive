<?php

use App\Classes\Curl;

class ArchiveController extends BaseController {

    // error codes: 
    // 1: missing mandatory parameter
    // 2: access denied
    // 3: api does not accept request
    // 4: exception thrown
 
    public function __construct() {
        $this->beforeFilter(function () {
            // if (!Auth::check()) {
            //     if(!Request::has('secret')) {
            //         return array('success' => false, 'error_code' => '1', 'error' => 'Missing request credentials');
            //     }
            //     $url = URL::current();
            //     $secret = Request::get('secret');
            //     $authenticated = false;
            //     $salt = date('Y-m-d');
            //     if(Hash::check("SaeeKSA $salt", $secret)) {
            //         $authenticated = true;
            //         break;
            //     }
            //     if(!$authenticated) {
            //         return array('success' => false, 'error_code' => '2', 'error' => 'Access denied');
            //     }
            // }
        }, array('except' => array()));
    }

    public function paste_shipments_delivered() {
        DB::beginTransaction();
        try {
            $delivery_request = json_decode(Request::get('delivery_request'));
            $captain_account = json_decode(Request::get('captain_account'));
            $captain_account_history = json_decode(Request::get('captain_account_history'));
            $banks_deposit = json_decode(Request::get('banks_deposit'));
            $orders_history = json_decode(Request::get('orders_history'));

            $delivery_request_sent_ids = [];
            $delivery_request_success_ids = [];
            $delivery_request_failed_ids = [];
            
            $captain_account_sent_ids = [];
            $captain_account_success_ids = [];
            $captain_account_failed_ids = [];

            $captain_account_history_sent_ids = [];
            $captain_account_history_success_ids = [];
            $captain_account_history_failed_ids = [];

            $banks_deposit_sent_ids = [];
            $banks_deposit_success_ids = [];
            $banks_deposit_failed_ids = [];

            $orders_history_sent_ids = [];
            $orders_history_success_ids = [];
            $orders_history_failed_ids = [];

            foreach($delivery_request as $one_record) {
                array_push($delivery_request_sent_ids, $one_record->id);
                try {
                    $array = (array) $one_record; // convert object to string, then string to array
                    $flag = DeliveryRequest::insert($array);
                    if($flag) {
                        array_push($delivery_request_success_ids, $one_record->id);
                    }
                    else {
                        array_push($delivery_request_failed_ids, array(
                            'id' => $one_record->id,
                            'error' => 'Unknown'
                        ));
                    }
                }
                catch (Exception $e) {
                    $line = $e->getLine();
                    $message = $e->getMessage();
                    array_push($delivery_request_failed_ids, array(
                        'id' => $one_record->id,
                        'error' => "Line: $line, Message: $message"
                    ));
                }
            }

            foreach($captain_account as $one_record) {
                array_push($captain_account_sent_ids, $one_record->id);
                try {
                    $array = (array) $one_record; // convert object to string, then string to array
                    $flag = CaptainAccount::insert($array);
                    if($flag) {
                        array_push($captain_account_success_ids, $one_record->id);
                    }
                    else {
                        array_push($captain_account_failed_ids, array(
                            'id' => $one_record->id,
                            'error' => 'Unknown'
                        ));
                    }
                }
                catch (Exception $e) {
                    $line = $e->getLine();
                    $message = $e->getMessage();
                    array_push($captain_account_failed_ids, array(
                        'id' => $one_record->id,
                        'error' => "Line: $line, Message: $message"
                    ));
                }
            }

            foreach($captain_account_history as $one_record) {
                array_push($captain_account_history_sent_ids, $one_record->id);
                try {
                    $array = (array) $one_record; // convert object to string, then string to array
                    $flag = CaptainAccountHistory::insert($array);
                    if($flag) {
                        array_push($captain_account_history_success_ids, $one_record->id);
                    }
                    else {
                        array_push($captain_account_history_failed_ids, array(
                            'id' => $one_record->id,
                            'error' => 'Unknown'
                        ));
                    }
                }
                catch (Exception $e) {
                    $line = $e->getLine();
                    $message = $e->getMessage();
                    array_push($captain_account_history_failed_ids, array(
                        'id' => $one_record->id,
                        'error' => "Line: $line, Message: $message"
                    ));
                }
            }

            foreach($banks_deposit as $one_record) {
                array_push($banks_deposit_sent_ids, $one_record->id);
                try {
                    $array = (array) $one_record; // convert object to string, then string to array
                    $flag = BanksDeposit::insert($array);
                    if($flag) {
                        array_push($banks_deposit_success_ids, $one_record->id);
                    }
                    else {
                        array_push($banks_deposit_failed_ids, array(
                            'id' => $one_record->id,
                            'error' => 'Unknown'
                        ));
                    }
                }
                catch (Exception $e) {
                    $line = $e->getLine();
                    $message = $e->getMessage();
                    array_push($banks_deposit_failed_ids, array(
                        'id' => $one_record->id,
                        'error' => "Line: $line, Message: $message"
                    ));
                }
            }

            foreach($orders_history as $one_record) {
                array_push($orders_history_sent_ids, $one_record->id);
                try {
                    $array = (array) $one_record; // convert object to string, then string to array
                    $flag = OrdersHistory::insert($array);
                    if($flag) {
                        array_push($orders_history_success_ids, $one_record->id);
                    }
                    else {
                        array_push($orders_history_failed_ids, array(
                            'id' => $one_record->id,
                            'error' => 'Unknown'
                        ));
                    }
                }
                catch (Exception $e) {
                    $line = $e->getLine();
                    $message = $e->getMessage();
                    array_push($orders_history_failed_ids, array(
                        'id' => $one_record->id,
                        'error' => "Line: $line, Message: $message"
                    ));
                }
            }

            // if error accured in any table, rollback everything
            if(count($delivery_request_failed_ids) 
                || count($captain_account_failed_ids) 
                || count($captain_account_history_failed_ids) 
                || count($banks_deposit_failed_ids) 
                || count($orders_history_failed_ids) 
            ) {

                throw new Exception('some of the records insertion failed. process aborted.');
            }

            $response_array = array(
                'success' => true,
                'delivery_request_sent_ids' => $delivery_request_sent_ids,
                'delivery_request_success_ids' => $delivery_request_success_ids,
                'delivery_request_failed_ids' => $delivery_request_failed_ids,
                'captain_account_sent_ids' => $captain_account_sent_ids,
                'captain_account_success_ids' => $captain_account_success_ids,
                'captain_account_failed_ids' => $captain_account_failed_ids,
                'captain_account_history_sent_ids' => $captain_account_history_sent_ids,
                'captain_account_history_success_ids' => $captain_account_history_success_ids,
                'captain_account_history_failed_ids' => $captain_account_history_failed_ids,
                'banks_deposit_sent_ids' => $banks_deposit_sent_ids,
                'banks_deposit_success_ids' => $banks_deposit_success_ids,
                'banks_deposit_failed_ids' => $banks_deposit_failed_ids,
                'orders_history_sent_ids' => $orders_history_sent_ids,
                'orders_history_success_ids' => $orders_history_success_ids,
                'orders_history_failed_ids' => $orders_history_failed_ids
            );

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $line = $e->getLine();
            $message = $e->getMessage();
            $response_array = array(
                'success' => false, 
                'server' => 'archive',
                'function' => 'ArchiveController@shipments_paste',
                'error' => "Line: $line, Message: $message",
                'delivery_request_sent_ids' => $delivery_request_sent_ids,
                'delivery_request_success_ids' => $delivery_request_success_ids,
                'delivery_request_failed_ids' => $delivery_request_failed_ids,
                'captain_account_sent_ids' => $captain_account_sent_ids,
                'captain_account_success_ids' => $captain_account_success_ids,
                'captain_account_failed_ids' => $captain_account_failed_ids,
                'captain_account_history_sent_ids' => $captain_account_history_sent_ids,
                'captain_account_history_success_ids' => $captain_account_history_success_ids,
                'captain_account_history_failed_ids' => $captain_account_history_failed_ids,
                'banks_deposit_sent_ids' => $banks_deposit_sent_ids,
                'banks_deposit_success_ids' => $banks_deposit_success_ids,
                'banks_deposit_failed_ids' => $banks_deposit_failed_ids,
                'orders_history_sent_ids' => $orders_history_sent_ids,
                'orders_history_success_ids' => $orders_history_success_ids,
                'orders_history_failed_ids' => $orders_history_failed_ids
            );
        }
        return $response_array;
    }

    public function delete_check_shipments_delivered() {

        try {
            
            $delivery_request_sent_ids = json_decode(Request::get('delivery_request_ids'));
            $captain_account_sent_ids = json_decode(Request::get('captain_account_ids'));
            $captain_account_history_sent_ids = json_decode(Request::get('captain_account_history_ids'));
            $banks_deposit_sent_ids = json_decode(Request::get('banks_deposit_ids'));
            $orders_history_sent_ids = json_decode(Request::get('orders_history_ids'));

            $delivery_request_archive_ids_query = DeliveryRequest::whereIn('id', $delivery_request_sent_ids)->select(['id'])->get();
            $captain_account_archive_ids_query = CaptainAccount::whereIn('id', $captain_account_sent_ids)->select(['id'])->get();
            $captain_account_history_archive_ids_query = CaptainAccountHistory::whereIn('id', $captain_account_history_sent_ids)->select(['id'])->get();
            $banks_deposit_archive_ids_query = BanksDeposit::whereIn('id', $banks_deposit_sent_ids)->select(['id'])->get();
            $orders_history_archive_ids_query = OrdersHistory::whereIn('id', $orders_history_sent_ids)->select(['id'])->get();

            $delivery_request_archive_ids = array();
            foreach($delivery_request_archive_ids_query as $one_record) 
                array_push($delivery_request_archive_ids, $one_record->id);

            $captain_account_archive_ids = array();
            foreach($captain_account_archive_ids_query as $one_record) 
                array_push($captain_account_archive_ids, $one_record->id);

            $captain_account_history_archive_ids = array();
            foreach($captain_account_history_archive_ids_query as $one_record) 
                array_push($captain_account_history_archive_ids, $one_record->id);

            $banks_deposit_archive_ids = array();
            foreach($banks_deposit_archive_ids_query as $one_record) 
                array_push($banks_deposit_archive_ids, $one_record->id);

            $orders_history_archive_ids = array();
            foreach($orders_history_archive_ids_query as $one_record) 
                array_push($orders_history_archive_ids, $one_record->id);
            
            if(count($delivery_request_archive_ids) != count($delivery_request_sent_ids)
                || count($captain_account_archive_ids) != count($captain_account_sent_ids)
                || count($captain_account_history_archive_ids) != count($captain_account_history_sent_ids)
                || count($banks_deposit_archive_ids) != count($banks_deposit_sent_ids)
                || count($orders_history_archive_ids) != count($orders_history_sent_ids)
            ) {
                // put in the response what records are missing
                throw new Exception("Some of the records are missing in the archive system. process aborted.");
            }

            $response_array = array(
                'success' => true,
                'message' => 'All records are found in the archive system',
                'delivery_request_sent_ids' => $delivery_request_sent_ids,
                'captain_account_sent_ids' => $captain_account_sent_ids,
                'captain_account_history_sent_ids' => $captain_account_history_sent_ids,
                'banks_deposit_sent_ids' => $banks_deposit_sent_ids,
                'orders_history_sent_ids' => $orders_history_sent_ids
            );
            
        }
        catch(Exception $e) {
            
            $line = $e->getLine();
            $message = $e->getMessage();
            $delivery_request_missing_ids = array_diff($delivery_request_sent_ids, $delivery_request_archive_ids);
            $captain_account_missing_ids = array_diff($captain_account_sent_ids, $captain_account_archive_ids);
            $captain_account_history_missing_ids = array_diff($captain_account_history_sent_ids, $captain_account_history_archive_ids);
            $banks_deposit_missing_ids = array_diff($banks_deposit_sent_ids, $banks_deposit_archive_ids);
            $orders_history_missing_ids = array_diff($orders_history_sent_ids, $orders_history_archive_ids);
            $response_array = array(
                'success' => false, 
                'server' => 'archive',
                'function' => 'ArchiveController@delete_check_shipments_delivered',
                'error' => "Line: $line, Message: $message",
                'delivery_request_sent_ids' => $delivery_request_sent_ids,
                'delivery_request_archive_ids' => $delivery_request_archive_ids,
                'delivery_request_missing_ids' => $delivery_request_missing_ids,
                'captain_account_sent_ids' => $captain_account_sent_ids,
                'captain_account_archive_ids' => $captain_account_archive_ids,
                'captain_account_missing_ids' => $captain_account_missing_ids,
                'captain_account_history_sent_ids' => $captain_account_history_sent_ids,
                'captain_account_history_archive_ids' => $captain_account_history_archive_ids,
                'captain_account_history_missing_ids' => $captain_account_history_missing_ids,
                'banks_deposit_sent_ids' => $banks_deposit_sent_ids,
                'banks_deposit_archive_ids' => $banks_deposit_archive_ids,
                'banks_deposit_missing_ids' => $banks_deposit_missing_ids,
                'orders_history_sent_ids' => $orders_history_sent_ids,
                'orders_history_archive_ids' => $orders_history_archive_ids,
                'orders_history_missing_ids' => $orders_history_missing_ids
            );
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

}

