<?php

/**
 * Created by PhpStorm.
 * User: umar
 * Date: 1/12/2017
 * Time: 6:18 PM
 */

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

class WASLAPIController extends \BaseController{

    public function updateProviderProfile() {

        $walker_id = Session::get('walker_id');
        $address_latitude=Input::get('address_latitude');
        $address_longitude=Input::get('address_longitude');
        $phone = Input::get('phone');
        $email = Input::get('email');
        $government_id_number=Input::get('government_id_number');
        $date_of_birth=Input::get('date_of_birth');
        $license_expiry_date=Input::get('license_expiry_date');
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $picture = Input::file('picture');
        $validator = Validator::make(
            array(
                'picture' => $picture
            ), array(
                'picture' => 'mimes:jpeg,bmp,png'
            )
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            Log::info('picture type not valid. Error = ' . print_r($error_messages, true));
            return Redirect::to('/provider/profile')->with('error', 'Invalid image format (Allowed formats jpeg,bmp and png)');
        } else {
            $walker = Walker::find($walker_id);
            //WASL Captain Registration
            $waslReferenceNumber=$walker->waslReferenceNumber;
            if($waslReferenceNumber==0){
                try{
                    $client = new Client();
                    $response = $client->post("https://wasl.elm.sa/WaslPortalWeb/rest/DriverRegistration/send",  ['json'=>['apiKey'=>'24609908-5A75-409B-A9A1-B801128E5878',
                        'captainIdentityNumber'=>$government_id_number,
                        'dateOfBirth'=>$date_of_birth,
                        'emailAddress'=>$email,
                        'mobileNumber'=>$phone]]);
                    $resobj=$response->getBody(true);
                    $obj=json_decode($resobj,true);
                    //var_dump($obj);
                    //echo $response->getBody(true);
                    //echo $obj['valid'];
                    $waslReferenceNumber = $obj['referenceNumber'];
                    $walker->waslReferenceNumber=$waslReferenceNumber;
                    if($obj['resultCode']!=100||$obj['resultMessage']=='Success'){
                        $waslErrorMessage=$obj['resultMessage'];
                        $waslresultCode=$obj['resultCode'];
                        $walker->first_name = $first_name;
                        $walker->last_name = $last_name;
                        $walker->government_id_number = $government_id_number;
                        $walker->date_of_birth = $date_of_birth;
                        $walker->phone = $phone;
                        $walker->license_expiry_date = $license_expiry_date;
                        $walker->address_latitude = $address_latitude;
                        $walker->address_longitude = $address_longitude;
                        $walker->save();
                        return Redirect::to('/provider/profile')->with('error', $waslErrorMessage)->with('waslresultCode',$waslresultCode);
                    }else{
                        $walker->first_name = $first_name;
                        $walker->last_name = $last_name;
                        $walker->government_id_number = $government_id_number;
                        $walker->date_of_birth = $date_of_birth;
                        $walker->license_expiry_date = $license_expiry_date;
                        $walker->waslReferenceNumber = $waslReferenceNumber;
                        $walker->address_latitude = $address_latitude;
                        $walker->address_longitude = $address_longitude;
                        $walker->phone = $phone;
                        $walker->save();
                    }
                }
                catch(Exception $e){
                    Log::info('WASL TRY = ' .$e->getMessage());
                    $walker->first_name = $first_name;
                    $walker->last_name = $last_name;
                    $walker->government_id_number = $government_id_number;
                    $walker->date_of_birth = $date_of_birth;
                    $walker->license_expiry_date = $license_expiry_date;
                    $walker->waslReferenceNumber = $waslReferenceNumber;
                    $walker->address_latitude = $address_latitude;
                    $walker->address_longitude = $address_longitude;
                    $walker->phone = $phone;
                    $walker->save();
                }

            }

            if (Input::hasFile('picture')) {
                if ($walker->picture != "") {
                    try{
                    $path = $walker->picture;
                    Log::info($path);
                    $filename = basename($path);
                    Log::info($filename);
                    if (file_exists($path)) {
                        unlink(public_path() . "/uploads/" . $filename);
                    }
                }catch (Exception $e){
                    Log::info($e->getMessage());
                }
                }
                // upload image
                $file_name = time();
                $file_name .= rand();
                $file_name = sha1($file_name);

                $ext = Input::file('picture')->getClientOriginalExtension();
                Input::file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;

                // Upload to S3
                if (Config::get('app.s3_bucket') != "") {
                    $s3 = App::make('aws')->get('s3');
                    $pic = $s3->putObject(array(
                        'Bucket' => Config::get('app.s3_bucket'),
                        'Key' => $file_name,
                        'SourceFile' => public_path() . "/uploads/" . $local_url,
                    ));

                    $s3->putObjectAcl(array(
                        'Bucket' => Config::get('app.s3_bucket'),
                        'Key' => $file_name,
                        'ACL' => 'public-read'
                    ));

                    $s3_url = $s3->getObjectUrl(Config::get('app.s3_bucket'), $file_name);
                } else {
                    $s3_url = asset_url() . '/uploads/' . $local_url;
                }

                if (isset($walker->picture)) {
                    if ($walker->picture != "") {
                        $icon = $walker->picture;
                        unlink_image($icon);
                    }
                }

                $walker->picture = $s3_url;
            }
            $walker->first_name = $first_name;
            $walker->last_name = $last_name;
            $walker->license_expiry_date = $license_expiry_date;
            $walker->address_latitude = $address_latitude;
            $walker->address_longitude = $address_longitude;

            //$walker->government_id_number = $government_id_number;
            //$walker->date_of_birth = $date_of_birth;

            //$walker->waslReferenceNumber = $waslReferenceNumber;

            //$walker->phone = $phone;
            $walker->save();
            if (!$walker->is_approved){
                $status=-1;
                return Redirect::to('/provider/vehicle')->with('message', 'Your profile has been updated successfully. Please provide your vehcile information')->with('type', 'success')->with('status', $status);
            }
            return Redirect::to('/provider/profile')->with('message', 'Your profile has been updated successfully.')->with('type', 'success');
        }
    }

    public function updateProviderVehicle() {

        foreach (Input::get('service') as $key) {
            $serv = ProviderType::where('id', $key)->first();
            $pserv[] = $serv->name;
        }
        $walker_id = Session::get('walker_id');
        $vehicle_sequence_number=Input::get('vehicle_sequence_number');
        $car_registration_expiry=Input::get('car_registration_expiry');
        $car_insurance_expiry=Input::get('car_insurance_expiry');
        $car_number_plate_number=Input::get('car_number_plate_number');
        $car_number_plate_letter_1=Input::get('car_number_plate_letter_1');
        $car_number_plate_letter_2=Input::get('car_number_plate_letter_2');
        $car_number_plate_letter_3=Input::get('car_number_plate_letter_3');
        $plate_type=Input::get('plate_type');
        $car_model = trim(Input::get('car_model'));

        {
            $walker = Walker::find($walker_id);
            //WASL Vehicle Registration
            $vehicleReferenceNumber=$walker->vehicleReferenceNumber;
            if($vehicleReferenceNumber==""||$vehicleReferenceNumber=="0"){
                try{
                    $client = new Client();
                    $response = $client->post("https://wasl.elm.sa/WaslPortalWeb/rest/VehicleRegistration/send",
                        ['json'=>[
                            'apiKey'=>'24609908-5A75-409B-A9A1-B801128E5878',
                            'vehicleSequenceNumber'=>$vehicle_sequence_number,
                            'plateLetterRight'=>$car_number_plate_letter_1,
                            'plateLetterMiddle'=>$car_number_plate_letter_2,
                            'plateLetterLeft'=>$car_number_plate_letter_3,
                            'plateNumber'=>$car_number_plate_number,
                            'plateType'=>$plate_type
                        ]]);
                    $resobjVeh=$response->getBody(true);
                    $objVeh=json_decode($resobjVeh,true);
                    //var_dump($objVeh);
                    //echo $response->getBody(true);
                    $vehicleReferenceNumber = $objVeh['vehicleReferenceNumber'];
                    if($objVeh['resultMessage']!='Success'&&$vehicleReferenceNumber!=0){
                        $waslErrorMessage=$objVeh['resultMessage'];
                        $waslresultCode=$objVeh['resultCode'];
                        $walker->vehicle_sequence_number = $vehicle_sequence_number;
                        $walker->car_number_plate_number = $car_number_plate_number;
                        $walker->car_number_plate_letter_1 = $car_number_plate_letter_1;
                        $walker->car_number_plate_letter_2 = $car_number_plate_letter_2;
                        $walker->car_number_plate_letter_3 = $car_number_plate_letter_3;
                        $walker->plate_type = $plate_type;
                        if ($car_model != "") {
                            $walker->car_model = $car_model;
                        }
                        $walker->car_registration_expiry = $car_registration_expiry;
                        $walker->car_insurance_expiry = $car_insurance_expiry;
                        $walker->save();
                        return Redirect::to('/provider/profile')->with('error', $waslErrorMessage)->with('waslresultCode',$waslresultCode);
                    }else{
                        $walker->vehicleReferenceNumber = $vehicleReferenceNumber;
                        $walker->vehicle_sequence_number = $vehicle_sequence_number;
                        $walker->car_number_plate_number = $car_number_plate_number;
                        $walker->car_insurance_expiry = $car_insurance_expiry;
                        $walker->car_number_plate_letter_1 = $car_number_plate_letter_1;
                        $walker->car_number_plate_letter_2 = $car_number_plate_letter_2;
                        $walker->car_number_plate_letter_3 = $car_number_plate_letter_3;
                        $walker->plate_type = $plate_type;
                        $walker->save();
                    }
                }catch (Exception $e){
                    Log::info('WASL TRY = ' .$e->getMessage());
                    $walker->vehicleReferenceNumber = $vehicleReferenceNumber;
                    $walker->vehicle_sequence_number = $vehicle_sequence_number;
                    $walker->car_number_plate_number = $car_number_plate_number;
                    $walker->car_insurance_expiry = $car_insurance_expiry;
                    $walker->car_number_plate_letter_1 = $car_number_plate_letter_1;
                    $walker->car_number_plate_letter_2 = $car_number_plate_letter_2;
                    $walker->car_number_plate_letter_3 = $car_number_plate_letter_3;
                    $walker->plate_type = $plate_type;
                    $walker->save();
                }


            }
            /* if ($car_number != "") {
                 $walker->car_number = $car_number;
             }*/
            if ($car_model != "") {
                $walker->car_model = $car_model;
            }
            $walker->car_registration_expiry = $car_registration_expiry;
            $walker->car_insurance_expiry = $car_insurance_expiry;

            //$walker->vehicle_sequence_number = $vehicle_sequence_number
            //$walker->car_number_plate_number = $car_number_plate_number;
            //$walker->car_number_plate_letter_1 = $car_number_plate_letter_1;
            //$walker->car_number_plate_letter_2 = $car_number_plate_letter_2;
            //$walker->car_number_plate_letter_3 = $car_number_plate_letter_3;
            //$walker->plate_type = $plate_type;

            //$walker->vehicleReferenceNumber = $vehicleReferenceNumber;

            $walker->save();

            foreach (Input::get('service') as $ke) {
                $proviserv = ProviderServices::where('provider_id', $walker->id)->first();
                if ($proviserv != NULL) {
                    DB::delete("delete from walker_services where provider_id = '" . $walker->id . "';");
                }
            }
            $base_price = Input::get('service_base_price');
            $service_price_distance = Input::get('service_price_distance');
            $service_price_time = Input::get('service_price_time');
            Log::info('service = ' . print_r(Input::get('service'), true));

            /* $cnkey = Input::get('noOfTypes'); */
            $cnkey = count(Input::get('service'));
            $type_id = trim((Input::get('service')[0]));
            $walker->type = $type_id;
            $walker->save();

            Log::info('cnkey = ' . print_r($cnkey, true));
            Log::info('service_base_price = ' . print_r($base_price, true));
            Log::info('service_price_distance = ' . print_r($service_price_distance, true));
            Log::info('service_price_time = ' . print_r($service_price_time, true));
            $key = Input::get('service');



            $prserv = new ProviderServices;
            $prserv->provider_id = $walker->id;
            $prserv->type = $type_id;
            $walker->type = $type_id;
            $walker->save();
            /* Log::info('key = ' . print_r($key, true)); */
            if (Input::has('service_base_price')) {
                $prserv->base_price = $base_price[$type_id];
            } else {
                $prserv->base_price = 0;
            }
            if (Input::has('service_price_distance')) {

                $prserv->price_per_unit_distance = $service_price_distance[$type_id];
            } else {
                $prserv->price_per_unit_distance = 0;
            }
            if (Input::has('service_price_time')) {
                $prserv->price_per_unit_time = $service_price_time[$type_id];
            } else {
                $prserv->price_per_unit_time = 0;
            }
            $prserv->save();

            /* }
              } */
            if (!$walker->is_approved){
                $status=-1;
                return Redirect::to('/provider/documents')->with('message', 'Your profile has been updated successfully. Please upload document')->with('type', 'success')->with('status', $status);
            }
            return Redirect::to('/provider/vehicle')->with('message', 'Your Vehicle has been updated successfully.')->with('type', 'success');
        }
    }

}