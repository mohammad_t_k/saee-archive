<?php
 
/*
 * handles delivery requests tracking and data manipulations
 */

use Ejabberd\Rest\Client;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BbChatController extends \BaseController
{



public function __construct()
    {

         
    }


public function registerUser(){

		$company_id = Session::get('company_id');
        $companydata = Companies::find($company_id);

		return View::make('companies.bBregisterUser')
		->with('companydata',$companydata)
        ->with('company_id', $company_id);


}

public function bbLogin(){

        
return View::make('companies.blackBoxLogin');
       

}

public function registerUserPost(){

    $company_id = Session::get('company_id');

    $username = Request::get('username');

    $onlyUsername = $username;

    $email  = Request::get('email');

    $phone = Request::get('phone');

    $phone = format_phone($phone);

    $alias = Request::get('alias');

    $bb_token = generate_token();

    $companyType = Companies::select('company_type','company_name')->where('id','=',$company_id)->first();


    if($companyType->company_type == '0'){

    $username = 'company'.'_'.$username.'_'.$company_id;

    }
    else{

        $username = 'merchant'.'_'.$username.'_'.$company_id;
    }

    $password = Request::get('password');

$client = new Client([
    'apiUrl' => '34.196.203.66:5280/api/',
    'host' => '34.196.203.66'
    ]);

// clearing ejabberd db cache // 
$clearCache = $client->clearCache();

$createAccount = $client->createAccount($username, $bb_token);

$newUsername = str_replace(array('company_','merchant_','_'.$company_id), '',$username);// removing saee_agent_ from usrename

$setNickName  = $client->setNickName($username, $companyType->company_name.' Agent ('.$onlyUsername.')');

$data = json_decode($createAccount);

if(property_exists($data , 'status')){
// if error then send object will error details...
return Response::json($createAccount);

}
else{

// else return success message and save record to our saee db or update if its already exist

    $existingUser = BBCompanyUsers::where('jid',$username)->first();

    if(empty($existingUser)){ // if username is not already registered in saee db // 

    $newUser = new BBCompanyUsers();

    $newUser->company_id = $company_id;

    $newUser->jid = $username;

    $newUser->username = $onlyUsername;

    $newUser->phone = $phone;

    $newUser->token = generate_token();

    $newUser->bb_token = $bb_token;

    $newUser->device_type = 'web';

    $newUser->password = Hash::make($password);

    $newUser->email = $email;

    $newUser->alias_id = $alias;

    $newUser->save();

    }
    else{
            // updating existing user with is deleted 0
        $user = BBCompanyUsers::where('jid',$username)->first();

        //$user->username;

        $user->company_id = $company_id;

        $user->password = $password;

        //$user->bb_token = $bb_token;

        $user->email      = $email;

        $user->is_deleted = 0;

        $user->save();

    }

return Response::json($createAccount);

}


}

public function bbAllUsers(){

    $company_id = Session::get('company_id');

    $companydata = Companies::find($company_id);

    $users = BBCompanyUsers::where('company_id', '=', $company_id)->where('is_deleted', '!=', 1)->orderBy('id', 'DESC')->paginate(10);

    $aliases = BBAliases::where('company_id', '=', $company_id)->orderBy('id', 'DESC')->paginate(10);

    $title = ucwords(trans('customize.User') . 's'); /* 'Owners' */
        return View::make('companies.bBAllUsers')
            ->with('title', $title)
            ->with('companydata', $companydata)
            ->with('page', 'users')
            ->with('users', $users)
            ->with('aliases', $aliases);
}

 public function editUser() {
        $id = Request::segment(4);
        $company_id = Session::get('company_id');

        $companydata = Companies::find($company_id);
        $success = Input::get('success');
        $user = BBCompanyUsers::find($id);
        if ($user) {
            $title = ucwords("Edit " ); /* 'Edit User' */
            return View::make('companies.edit_user')
                ->with('title', $title)
                ->with('company_id', $company_id)
                ->with('page', 'owners')
                ->with('success', $success)
                ->with('companydata', $companydata)
                ->with('user', $user);
        } else {
            return View::make('notfound')
                ->with('title', 'Error Page Not Found')
                ->with('page', 'Error Page Not Found');
        }
    }


   public function update_user(){

    $company_id = Session::get('company_id');

    $user_id = Input::get('id');

    $username = Input::get('username');

    $email = Input::get('email');

    $password = Input::get('password');

    $alias = Input::get('alias');

    $phone = Input::get('phone');
    $phone = format_phone($phone);

    BBCompanyUsers::where('id', '=', $user_id)->update(

         array('email' => $email,
               'password' => Hash::make($password),
               'alias_id' => $alias,
               'phone'   => $phone
     ));

}


public function delete_user() {

        $id = Request::segment(4);

        $success = Input::get('success');

        $user = BBCompanyUsers::find($id);

        $username = $user->jid;

         $client = new Client([
    'apiUrl' => '34.196.203.66:5280/api/',
    'host' => '34.196.203.66'
    ]);

    $unRegister = $client->unRegisterAccount($username);

    $data = json_decode($unRegister);

if(property_exists($data , 'status')){
// if error then send object will error details...
return Redirect::to("/blackbox/allusers")->with('msg',$data->message);

}

else{

    $user->is_deleted = 1;

    $user->save();

    return Redirect::to("/blackbox/allusers")->with('msg','Successfully Deleted');

}

       
    }

    public function delete_alias() {

        $id = Request::segment(4);

        $success = Input::get('success');

        $alias = BBAliases::find($id);

        if(!empty($alias)){

        
        $alias->delete();

    return Redirect::to("/blackbox/allusers")->with('msg','Successfully Deleted');

    }
       
    }

public function blackbox(){

    return View::make('companies.bbchat');
}


public function addAliasPost(){


    try{

    $company_id = Session::get('company_id');

    $aliasId = Request::get('aliasid');

    $aliasName  = Request::get('aliasname');

    $country  = Request::get('country');

    $existingAlias = BBAliases::where('alias_id',$aliasId)->first();

    if(empty($existingAlias)){ // if alias is not already registered in saee db // 

    $newAlias = new BBAliases();

    $newAlias->company_id = $company_id;

    $newAlias->alias_id = $aliasId;

    $newAlias->alias_name = $aliasName;

    $newAlias->country = $country;

    $newAlias->save();

           $response_array = array('success' => true, 'message' => 'Alias added succesfully');
           $response_code = 200;
           $response = Response::json($response_array, $response_code);
           return $response;


    }
    else{
           
           $response_array = array('success' => false, 'message' => 'Alias already exist');
           $response_code = 200;
           $response = Response::json($response_array, $response_code);
           return $response; 
       
    }

}

 catch (Exception $e) {

            $response_array = array('success' => false, 'error' => $e->getMessage(), 'line' => $e->getLine());
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
         }



}


public function bbVerify()
    {
        $email = Input::get('username');
        $password = Input::get('password');
        $admin = Admin::where('email', '=', $email)->orWhere('username', '=', $email)->first();

        $companyAgent = BBCompanyUsers::where('email','=',$email)->orWhere('username','=',$email)->first();
        $isAdmin = 0;

        if ($admin) {
            if ($admin && Hash::check($password, $admin->password)) {
               $isAdmin = 1;
               $admin_id = $admin->id;
               if(filter_var($admin->username, FILTER_VALIDATE_EMAIL)) {
                    //username is email
                    $admin_username = strstr($admin->username, '@', true); // get first part of email before @
                }
                else{
                    $admin_username = $admin->username;
                }
               $admin_password = $admin->bb_token;
                return View::make('companies.bbchat') // autologin bbchat view
                ->with('admin_id',$admin_id)
                ->with('admin_username',$admin_username)
                ->with('admin_password', $admin_password)
                ->with('is_admin', $isAdmin);
            } else {
                return Redirect::to('blackbox/login')->with('error', 'Invalid Username and password')->with('email', $email);
            }

        }
        else if ($companyAgent) {
            if ($companyAgent && Hash::check($password, $companyAgent->password)) {
               
               $admin_id = $companyAgent->company_id;
               $admin_username = $companyAgent->username;
               $admin_password = $companyAgent->bb_token;
                return View::make('companies.bbchat') // autologin bbchat view
                ->with('admin_id',$admin_id)
                ->with('admin_username',$admin_username)
                ->with('admin_password', $admin_password)
                ->with('is_admin', $isAdmin);
            } else {
                return Redirect::to('blackbox/login')->with('error', 'Invalid Username and password')->with('email', $email);
            }

        }
         else {
            return Redirect::to('blackbox/login')->with('error', 'Invalid Username')->with('email', $email);
        }
    }


public function bbpinscreen($waybill){

return View::make('companies.bbpinscreen')->with('waybill',$waybill);

}

public function bbPinVerify()
    {
        $pin = Input::get('pin');
        $waybill = Input::get('waybill');

        $order = DeliveryRequest::select('pincode2','receiver_name')->where('jollychic', '=', $waybill)->first();

        if($order){

        if ($order->pincode2 == $pin) { // match user entered pin with db pin
        
                $allStakeHolders = app('JollyChicController')->allStackeHolders();
                $data = $allStakeHolders->getData();

                $jids = array();
                foreach ($data->participants as $participant) {
  
                 $jids[] = $participant->jid.'@34.196.203.66';  

                }
                // converting to collon seperated jids becuase send direct invites accepts collon seperated jids
                $collonJids = implode(':', $jids); 

                $client = new Client([
    'apiUrl' => '34.196.203.66:5280/api/',
    'host' => '34.196.203.66'
    ]);

$sendInvitation = $client->sendInvitation($waybill, $collonJids);
              
                return View::make('companies.bbconsigneechat') // autologin bbchat view
                ->with('waybill',$waybill)->with('receiver_name',$order->receiver_name)->with('collonJids', $collonJids);

        }

         else {
                return Redirect::to('blackbox/'.$waybill)->with('error', 'Invalid Pin');
            }

        } // if order exist
        else{

            return Redirect::to('blackbox/'.$waybill)->with('error', 'Invalid Waybill');
        }
        
    }



}