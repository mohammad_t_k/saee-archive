<?php

class ApplicationController extends BaseController {

    private function _braintreeConfigure() {
        Braintree_Configuration::environment(Config::get('app.braintree_environment'));
        Braintree_Configuration::merchantId(Config::get('app.braintree_merchant_id'));
        Braintree_Configuration::publicKey(Config::get('app.braintree_public_key'));
        Braintree_Configuration::privateKey(Config::get('app.braintree_private_key'));
    }

    public function pages() {
        $informations = Information::all();
        $informations_array = array();
        foreach ($informations as $information) {
            $data = array();
            $data['id'] = $information->id;
            $data['title'] = $information->title;
            $data['content'] = $information->content;
            $data['icon'] = $information->icon;
            array_push($informations_array, $data);
        }
        $response_array = array();
        $response_array['success'] = true;
        $response_array['informations'] = $informations_array;
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function get_page() {
        $id = Request::segment(3);
        $information = Information::find($id);
        $response_array = array();
        if ($information) {
            $response_array['success'] = true;
            $response_array['title'] = $information->title;
            $response_array['content'] = $information->content;
            $response_array['icon'] = $information->icon;
        } else {
            $response_array['success'] = false;
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function types() {
        $types = ProviderType::where('is_visible', '=', 1)->get();
        /* $setbase_price = Settings::where('key', 'base_price')->first();
          $base_price = $setbase_price->value;
          $setdistance_price = Settings::where('key', 'price_per_unit_distance')->first();
          $distance_price = $setdistance_price->value;
          $settime_price = Settings::where('key', 'price_per_unit_time')->first();
          $time_price = $settime_price->value; */
        $type_array = array();
        $settunit = Settings::where('key', 'default_distance_unit')->first();
        $unit = $settunit->value;
        if ($unit == 0) {
            $unit_set = 'kms';
        } elseif ($unit == 1) {
            $unit_set = 'miles';
        }
        /* $currency_selected = Keywords::find(5); */
        foreach ($types as $type) {
            $data = array();
            $data['id'] = $type->id;
            $data['name'] = $type->name;
            $data['min_fare'] = ($type->min_price);
            $data['max_size'] = $type->max_size;
            $data['icon'] = $type->icon;
            $data['is_default'] = $type->is_default;
            $data['price_per_unit_time'] = ($type->price_per_unit_time);
            $data['price_per_unit_distance'] = ($type->price_per_unit_distance);
            $data['base_price'] = ($type->base_price);
            $data['base_distance'] = ($type->base_distance);
            /* $data['currency'] = $currency_selected->keyword; */
            $data['currency'] = Config::get('app.generic_keywords.Currency');
            $data['unit'] = $unit_set;
            array_push($type_array, $data);
        }
        $response_array = array();
        $response_array['success'] = true;
        $response_array['types'] = $type_array;
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    public function forgot_password() {
        $type = Input::get('type');
        $email = Input::get('email');
        if ($type == 1) {
            // Walker
            $walker_data = Walker::where('email', $email)->first();
            if ($walker_data) {
                $walker = Walker::find($walker_data->id);
                $new_password = time();
                $new_password .= rand();
                $new_password = sha1($new_password);
                $new_password = substr($new_password, 0, 8);
                $walker->password = Hash::make($new_password);
                $walker->save();

                /* $subject = "Your New Password";
                  $email_data = array();
                  $email_data['password'] = $new_password;
                  send_email($walker->id, 'walker', $email_data, $subject, 'forgotpassword'); */
                $settings = Settings::where('key', 'admin_email_address')->first();
                $admin_email = $settings->value;
                $login_url = web_url() . "/provider/signin";
                $pattern = array('name' => $walker->first_name . " " . $walker->last_name, 'admin_eamil' => $admin_email, 'new_password' => $new_password, 'login_url' => $login_url);
                $subject = "Your New Password";
                email_notification($walker->id, 'walker', $pattern, $subject, 'forgot_password', "imp");

                $response_array = array();
                $response_array['success'] = true;
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $response_array = array('success' => false, 'error' => 'This Email is not Registered', 'error_code' => 425);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            }
        } else {
            $owner_data = Owner::where('email', $email)->first();
            if ($owner_data) {

                $owner = Owner::find($owner_data->id);
                $new_password = time();
                $new_password .= rand();
                $new_password = sha1($new_password);
                $new_password = substr($new_password, 0, 8);
                $owner->password = Hash::make($new_password);
                $owner->save();

                /* $subject = "Your New Password";
                  $email_data = array();
                  $email_data['password'] = $new_password;
                  send_email($owner->id, 'owner', $email_data, $subject, 'forgotpassword'); */
                $settings = Settings::where('key', 'admin_email_address')->first();
                $admin_email = $settings->value;
                $login_url = web_url() . "/user/signin";
                $pattern = array('name' => $owner->first_name . " " . $owner->last_name, 'admin_eamil' => $admin_email, 'new_password' => $new_password, 'login_url' => $login_url);
                $subject = "Your New Password";
                email_notification($owner->id, 'owner', $pattern, $subject, 'forgot_password', "imp");


                $response_array = array();
                $response_array['success'] = true;
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            } else {
                $response_array = array('success' => false, 'error' => 'This Email is not Registered', 'error_code' => 425);
                $response_code = 200;
                $response = Response::json($response_array, $response_code);
                return $response;
            }
        }
    }

    public function token_braintree() {
        $this->_braintreeConfigure();
        $clientToken = Braintree_ClientToken::generate();
        $response_array = array('success' => true, 'clientToken' => $clientToken);
        $response_code = 200;
        return Response::json($response_array, $response_code);
    }

    public function available_types() {

       // $token = Input::get('token');
       // $owner_id = Input::get('id');

        $trip_one_from_latitude = Input::get('trip_one_from_lat');
        $trip_one_from_longitude = Input::get('trip_one_from_long');

        $trip_one_to_latitude = Input::get('trip_one_to_lat');
        $trip_one_to_longitude = Input::get('trip_one_to_long');

       $trip_two_from_latitude = Input::get('trip_two_from_lat');
        $trip_two_from_longitude = Input::get('trip_two_from_long');

       $trip_two_to_latitude = Input::get('trip_two_to_lat');
       $trip_two_to_longitude = Input::get('trip_two_to_long');


        $validator = Validator::make(
                        array(
                    //'token' => $token,
                    //'owner_id' => $owner_id,
                    'trip_one_from_latitude' => $trip_one_from_latitude,
                    'trip_one_from_longitude' => $trip_one_from_longitude
                        ), array(
                    //'token' => 'required',
                   // 'owner_id' => 'required|integer',
                    'trip_one_from_latitude' => 'required',
                    'trip_one_from_longitude' => 'required'
                        )
        );

        $latitude = $trip_one_from_latitude;
        $longitude = $trip_one_from_longitude;

        $types = ProviderType::where('is_visible', '=', 1)->get();
        
        $type_array = array();
        $settunit = Settings::where('key', 'default_distance_unit')->first();
        $unit = $settunit->value;
        if ($unit == 0) {
            $unit_set = 'kms';
        } elseif ($unit == 1) {
            $unit_set = 'miles';
        }



        /* $currency_selected = Keywords::find(5); */
        foreach ($types as $type) {

            $typequery = "SELECT distinct provider_id from walker_services where type IN($type->id) AND provider_id NOT IN (SELECT current_walker FROM request WHERE status = 1 AND service_type = 2)";
            Log::info('$typequery = ' . print_r($typequery, true));
            $typewalkers = DB::select(DB::raw($typequery));

               // Log::info('typewalkers = ' . print_r($typewalkers, true));

                $types =array();
                $typestring = array();

                if (count($typewalkers) > 0) {
                    foreach ($typewalkers as $key) {
                        $types[] = $key->provider_id;
                    }

                }
                
                $typestring = implode(",", $types);
                Log::info('typestring = ' . print_r($typestring, true));

                $settings = Settings::where('key', 'radius_monthly_contract_from_captain_home_kms')->first();
                $distance = $settings->value;
                            
                $settings = Settings::where('key', 'default_distance_unit')->first();
                $unit = $settings->value;

                if ($unit == 0) {
                    $multiply = 1.609344;
                } elseif ($unit == 1) {
                    $multiply = 1;
                }
                
                $walkers = array();

                if(!empty($typestring)){
                    $query = "SELECT walker.*, "
                            . "ROUND(" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                            . "cos( radians(address_latitude) ) * "
                            . "cos( radians(address_longitude) - radians('$longitude') ) + "
                            . "sin( radians('$latitude') ) * "
                            . "sin( radians(address_latitude) ) ) ,8) as distance "
                            . "FROM walker "
                            . "where is_available = 1 and "
                            . "is_active = 1 and "
                            . "is_approved = 1 and "
                            . "ROUND((" . $multiply . " * 3956 * acos( cos( radians('$latitude') ) * "
                            . "cos( radians(address_latitude) ) * "
                            . "cos( radians(address_longitude) - radians('$longitude') ) + "
                            . "sin( radians('$latitude') ) * "
                            . "sin( radians(address_latitude) ) ) ) ,8) <= $distance and "
                            . "walker.deleted_at IS NULL and "
                            . "walker.id IN($typestring) "
                            . "order by distance";

                    Log::info('$query = ' . print_r($query, true));
                    $walkers = DB::select(DB::raw($query));
                }

                $walkersId = ''; 

                if (count($walkers) > 0) { $is_available = 1; 
                    $walkersId = $walkers[0]->id;
                }
                else{ $is_available = 0; }


                if($trip_two_from_latitude == '' && $trip_two_from_longitude == ''){ $trip_info['trip_type'] = 1; }
                else{ $trip_info['trip_type'] = 2; }
                
                $trip_info['cab_type'] = $type->id;
                $trip_info['walker_id'] = $walkersId;

                $trip_info['trip_one_from_lat'] = $trip_one_from_latitude;
                $trip_info['trip_one_from_long'] = $trip_one_from_longitude;
                $trip_info['trip_one_to_lat'] = $trip_one_to_latitude;
                $trip_info['trip_one_to_long'] = $trip_one_to_longitude;

                $trip_info['trip_two_from_lat'] = $trip_two_from_latitude;
                $trip_info['trip_two_from_long'] = $trip_two_from_longitude;
                $trip_info['trip_two_to_lat'] = $trip_two_to_latitude;
                $trip_info['trip_two_to_long'] = $trip_two_to_longitude;

                $trip_details = cost_calculation($trip_info);

                $cost = '';
                if($trip_details){
                    $cost = number_format((float)$trip_details['total_cost'], 2);
                }

            $data = array();
            $data['id'] = $type->id;
            $data['name'] = $type->name;
            $data['min_fare'] = ($type->min_price);
            $data['max_size'] = $type->max_size;
            $data['icon'] = $type->icon;
            $data['is_default'] = $type->is_default;
            $data['price_per_unit_time'] = ($type->price_per_unit_time);
            $data['price_per_unit_distance'] = ($type->price_per_unit_distance);
            $data['base_price'] = ($type->base_price);
            $data['base_distance'] = ($type->base_distance);
            /* $data['currency'] = $currency_selected->keyword; */
            $data['currency'] = Config::get('app.generic_keywords.Currency');
            $data['unit'] = $unit_set;
            $data['total_cost'] = $cost . " SAR";
            $data['is_available'] = $is_available;
            array_push($type_array, $data);
        }

        $response_array = array();
        $response_array['success'] = true;
        $response_array['types'] = $type_array;
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    /*public function cost_calculation($trip_info){

        $distance_km = '';
        $cost = '';
        $duration_min = '';
        $trip_details = array();

        //echo $map_url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$trip_info['trip_one_from_lat'].", ".$trip_info['trip_one_from_long']."&destination=".$trip_info['trip_one_to_lat'].", ".$trip_info['trip_one_to_long']."&mode=driving&key=AIzaSyAcAGKNFyPX3kLqkUOOCh01u79L3zFQauM";
        
        $location_info = array('origin'=> $trip_info['trip_one_from_lat']. ', ' . $trip_info['trip_one_from_long'],
              'destination'=> $trip_info['trip_one_to_lat']. ', ' . $trip_info['trip_one_to_long'],
              'mode'=>'driving',
              'key'=>'AIzaSyAcAGKNFyPX3kLqkUOOCh01u79L3zFQauM');

        $map_info = http_build_query($location_info);
        $maps = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/directions/json?'.$map_info), TRUE);

        if($maps){
            $distance = $maps['routes'][0]['legs'][0]['distance']['value'];
            $duration = $maps['routes'][0]['legs'][0]['duration']['value'];
            
            $distance_km = $distance * 0.001;
            $duration_min = $duration * 0.0166667;

            //echo $distance . "<br>" . $duration . "<br>";
            //echo $distance_km . " KM" . "<br>" . $duration_min . "Min";

            if($trip_info['cab_type'] == 1){
                $cost = 4+(1.4*$distance_km)+(0.35*$duration_min);
                $cost = $cost*22;
            }else if($trip_info['cab_type'] == 2){
                $cost = 8+(1.8*$distance_km)+(0.55*$duration_min);
                $cost = $cost*22;
            }else if($trip_info['cab_type'] == 3){
                $cost = 10+(2*$distance_km)+(0.9*$duration_min);
                $cost = $cost*22;
            }else if($trip_info['cab_type'] == 4){  
                $cost = 10+(2.5*$distance_km)+(1*$duration_min);
                $cost = $cost*22;
            }else if($trip_info['cab_type'] == 5){
                $cost = 500;
            }

            if($trip_info['trip_type'] == 2){
                  if($trip_info['cab_type'] != 5){
                    $cost = $cost*2;
                  }
            }

            $trip_details = array('total_cost' => (float)$cost, 'distance_km' => $distance_km, 'duration_min' => $duration_min);
        }

        return $trip_details;
    }*/

}
