<?php


/*
 *  controller for customer service panels
 *
 * handles and manipulate the functionality by the customer service requests.
 *
 */


class CustomerServiceController extends \BaseController
{
    /*
     * Important note: after fixing the bugs and adding the new function, Delete the unnecessary repetitive functions.
     */





   // haversine formula to calculate distance between two points
   public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)

    {

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;

    }

    /*
     * Function to find nearest available drivers to the location of a given monthly contract
     * note: the function does not check for _approved and _avaliable drivers it shows all drivers without any conditions.
     *
     */
    public function findDrivers()
    {

        try {

        $monthly_id = Request::get('id');
        //get lat and lang of the home location
        $monthly = Monthly::where('id', '=', $monthly_id)->first();


        $walkers = Walker::where('is_approved','=',1)->get();

        $scores = array();
        $i = 0;
         foreach ($walkers as $walker){

             $score =  array($this->haversineGreatCircleDistance($monthly->trip_one_from_lat,$monthly->trip_one_from_long,$walker->latitude ,$walker->longitude ,6371)
                              ,
                              $walker);

             $scores[$i] = $score;
            $i++;
         }

         return json_encode($scores);

        } catch (Exception $e){

            return $e->getMessage();
        }

    }


    //either call this function by its parms or get the parms from ajax request by Request::get('json_attribute');
   public function notifyDriver ( ){


        try {

            $month_id = Request::get('monthlyid');
            /*$walker_id = Request::get('walkerid');*/

            //this is for testing...
            $walker_id = "432";
            $monthly = Monthly::where('id', $month_id)->first();
            request_walker_for_private_contract($monthly,$walker_id);

        }catch (Exception $e){

            return $e->getMessage();
        }
        return "done";
   }


     //do the same as the above function but send notification to the 10 nearest drivers...
   function notify_nearest_drivers( ){

       try {
           $monthly_id = Request::get('monthlyid');
           $monthly = Monthly::where('id', '=', $monthly_id)->first();
           request_walker_monthly($monthly);

       } catch (Exception $e){
           return $e->getMessage();
       }

   }


    /*
     * insert to db driver id assigned to monthly contract id
     */
    public function setDriverToContract(){}

    /*
     *
     * set the status of the contract either one of the following but not limited to:
     * -contacted.
     * -canceled.
     * -service started.
     * -expired.
     */
    public function updateContractStatus()
    {

      try {

        Monthly::where('id',Request::get('id'))->update(array('cs_status'=>Request::get('status') ));

        }catch (Exception  $e){
            return $e->getMessage();
        }
        return "Done";

    }

          //
    public function matchTwoContracts( ){}

    /*
  returns the form for creating a new monthly contract.
     */

    public function CreateMonthlyContract()
    {
        //parm is an array from the URI, form action = post. "
           //your view mame, use "->with" to send parms.
        return View::make('apply_monthly');

    }


    public function requestStatusToString($status){

         //monthly status as documented
        $MonthlyStatus = array ( "Not approved request", //0
                                 "Waiting for driver approval", //1
                                 "No Driver Found", //2
                                 "Driver Approved", //3
                                 "Driver Rejected", //4
                                 "Confirm Payment", //5
                                 "Reject Payment", //6
                                 "Customer Paid", //7
                                 "Raid Completed", //8
                                 "CSR Declined", //9
                                 "Paid by Cash", //10
                                 "Paid by Sadad", //11
                                 "Removed Request", //12
                                 "Unknown", //13
                                 "Unknown", //14
                                 "Unknown", //15
                                 "Unknown", //16
                                 "Unknown", //17
                                 "Shared Contract Created", //18
                                 "Asked To Join ", //19
                                 "Invited", //20
                                 "Invitation Approved", //21
                                 "Asked To Join Approved", //22
                                 "Asked To Join Rejected ", //23
                                 "Invitation Rejected"); //24

        return $MonthlyStatus[$status];


    }


    public function getContractRequestStatus ($contract){
        if($contract->request_id != 0){
            $request = Requests::where('id', '=',$contract->request_id)->first();
           //meaning walker confirmed ??
           if($request->confirmed_walker != 0){
               $confirmed_walker = Walker::where('id','=',$request->confirmed_walker)->first();
               return 'Walker: '.$confirmed_walker->id." Accepted";
           }else {
               return $this->requestStatusToString($request->status);
           }
          }else if ($contract->request_id == 0){
            return "request not created";
        }
    }

    public function contracts (){

        //Extract the user associated with each contract, each user might have multiple contracts.
        //get all rows from table "Monthly".
        $contracts = Monthly::all();
        $i = 0;
        $users = array();
        $statuses = array ();
        foreach ($contracts as $contract) {
            $user = Owner::where('id', '=', $contract->owner_id)->first();
            $users[$i] = $user;
            $status = $this->getContractRequestStatus($contract);
            $statuses[$i] = $status;
            $i++;
        }
        /*
         return all contracts, each contract is associated with a user. could be optimized to return
         only "valid" or recent contracts..
         */
        return View::make('web.CustomerService.contracts')->with('users', $users)
                                                          ->with('contracts', $contracts)
                                                          ->with('statuses',$statuses);

    }

    //return contracts page view.
    public function ViewContracts()
    {

        //get all rows from table "Monthly".
        $contracts = Monthly::all();
        //$users = Owner::all();
        //Extract the user associated with each contract, each user might have multiple contracts.
        $i = 0;
        $users = array();

        foreach ($contracts as $contract) {
            $user = Owner::where('id', '=', $contract->owner_id)->first();
            $users[$i] = $user;

            $i++;
        }
        /*
         * The first parm of "with" function is used as a reference in the view.
           Return list of contracts and users.
       */
        return View::make('contracts')->with('users', $users)->with('contracts', $contracts);

    }

    //return drivers page view.
    public function ViewDrivers ( ){
         //with-> method to return list of drivers.
        return View::make('view_drivers');

    }

    //check wither the giving email is registered or not.
    public function isUser($email)
    {

        $owner_data = Owner::where('email', '=', $email)->first();

        if ($owner_data == null) {
            return false;
        } else {
            return true;
        }

    }

    /*
     * receives a post request with an array parm and populate the table
     * "monthly" with a new contract, using an existing or a new user.
     */



   
    function getOnTripDrivers(){

        try {
            $walkers = Walker::where('is_available','=',0)->get();
            $walkers_with_documents = array();
            $i = 0;
            foreach ($walkers as $walker) {

                $walker_docs = array();
                for($x=2; $x<=6; $x++ ){
                    if($x==3){
                        $walker_docs[$x-2]= $walker->picture;
                     }else{
                        if($doc = WalkerDocument::where('walker_id', '=', $walker->id)->where('document_id','=',$x)->first()){
                            $walker_docs[$x-2] = $doc->url;
                        }else {
                            $walker_docs[$x-2] = "/uploads/unavailable.jpg";
                        }
                    }

                }
                $walkers_with_documents[$i] = array($walker,$walker_docs);//number of missing docs
                $i++;
            }
            return json_encode($walkers_with_documents);
        } catch (Exception $e){
            return $e->getMessage();
        }

    }

   function getMonthlyActiveDrivers (){

       try {
           $walkers = Walker::where('is_active_monthly','=',1)->get();
           $walkers_with_documents = array();
           $i = 0;
           foreach ($walkers as $walker) {

               $walker_docs = array();
               for($x=2; $x<=6; $x++ ){
                   
                    if($x==3){
                        $walker_docs[$x-2]= $walker->picture;
                     }else{
                   if($doc = WalkerDocument::where('walker_id', '=', $walker->id)->where('document_id','=',$x)->first()){
                       $walker_docs[$x-2] = $doc->url;
                   }else {
                        $walker_docs[$x-2] = "/uploads/unavailable.jpg";
                   }
                 }
                   
               }
               $walkers_with_documents[$i] = array($walker,$walker_docs);//number of missing docs
               $i++;
           }
           return json_encode($walkers_with_documents);
       } catch (Exception $e){
           return $e->getMessage();
       }



   }



    function getOnlineDrivers (){

        try {

            $walkers = Walker::where('is_active','=',1)->get();
            $walkers_with_documents = array();
            $i = 0;
            foreach ($walkers as $walker) {

                $walker_docs = array();
                for($x=2; $x<=6; $x++ ){
                     if($x==3){
                        $walker_docs[$x-2]= $walker->picture;
                     }else{
                    if($doc = WalkerDocument::where('walker_id', '=', $walker->id)->where('document_id','=',$x)->first()){
                        $walker_docs[$x-2] = $doc->url;
                    }else {
                        $walker_docs[$x-2] = "/uploads/unavailable.jpg";
                    }
                }
                }
                $walkers_with_documents[$i] = array($walker,$walker_docs);//number of missing docs
                $i++;
            }
            return json_encode($walkers_with_documents);
        } catch (Exception $e){
            return $e->getMessage();
        }

    }



    public function insertNewContract()
    {

        try {

            //Get the params from the ajax request
            $name = Request::get('name');
            $mobile = Request::get('mobile');
            $email = Request::get('email');

            $str_lat = Request::get('str_lat');
            $str_lng = Request::get('str_lng');

            $end_lat = Request::get('end_lat');
            $end_lng = Request::get('end_lng');

            $extra_lat = Request::get('extra_lat');
            $extra_lng = Request::get('extra_lng');

            $kim = Request::get('km');
            $min = Request::get('min');
            $going_time = Request::get('going_time');
            $return_time = Request::get('return_time');

            $cost = Request::get('cost');
            $starting_date = Request::get('starting_date');
            $contract_type = Request::get('contract_type');
            $days = Request::get('days');
            $employer = Request::get('employer');


            $trip_one_address = Request::get('trip_one_address');
            $trip_two_address = Request::get('trip_two_address');
            $extra_address = Request::get('extra_address');

            $numberofdays = Request::get('numberofdays');

            //is the given email already in the database?
            if(!$this->isUser($email)){

                //Create new user with the given name, email, mobile and init universal password of 123456789.
                $newUser = new Owner();
                $names = preg_split('/\s+/', $name);

                $first_name = $names[0];
                $last_name = $names[1];

                $newUser->first_name = $first_name;
                $newUser->last_name = $last_name;
                $newUser->phone = "966" . $mobile;
                $newUser->email = $email;

                // Hash function to encrypt the password before populating the database.
                $newUser->password = Hash::make("123456789");
                $newUser->created_at = date("Y-m-d h:i:s");
                $newUser->save();

            }

            //get the id of the created user then use it to populate new contract....
            $owner_data = Owner::where('email', '=', $email)->first();

            //Create new contract for $owner_data.
            $monthly = new Monthly;

            $monthly->owner_id = $owner_data->id;
            $monthly->subscription_type = $contract_type;


            if ($return_time != "00:00:00") {
                $monthly->trip_type = "1";
            } else {
                $monthly->trip_type = "2";
            }


            //Trip one.
            $monthly->trip_one_from_lat = $str_lat;
            $monthly->trip_one_from_long = $str_lng;
            $monthly->trip_one_to_lat = $end_lat;
            $monthly->trip_one_to_long = $end_lng;

            $monthly->trip_one_from_address = $trip_one_address;
            $monthly->trip_one_to_address = $trip_two_address;


            $monthly->trip_one_pickup_time = $going_time;

            //Trip two.
            $monthly->trip_two_from_lat = $end_lat;
            $monthly->trip_two_from_long = $end_lng;
            $monthly->trip_two_to_lat = $str_lat;
            $monthly->trip_two_to_long = $str_lng;

            $monthly->trip_two_from_address = $trip_two_address;
            $monthly->trip_two_to_address = $trip_one_address;


            $monthly->trip_two_pickup_time = $return_time;

            $monthly->distance = $kim;
            $monthly->duration = $min;
            $monthly->days = $days;

            //this is for the extra location, "child school drop location".
            $monthly->extra_lat = $extra_lat;
            $monthly->extra_lan = $extra_lng;
            $monthly->extra_address = $extra_address;

            $monthly->cab_type = 5;
            //this mean a return time from location of trip_one_to__
            $monthly->starting_date = $starting_date;
            $monthly->cost = $cost;

            //requested date.
            $monthly->created_at = date("Y-m-d h:i:s");
            $monthly->save();


            //create new user
            //insert the data to monthly table.

            return "done!";

         } catch (Exception $e) {
            return $e->getMessage();

         }

    }

    //DownloadContract as .docx extension. given the contract and the choosen driver by customer service.
    public function downloadContract()
    {


        try {
            //library for generating docs files from html elements.
            include ('/var/www/html/kc/public/Dev-Server-Resources/vsword-master/vsword-master/vsword/VsWord.php');

            $data = json_decode(stripslashes(Request::get('data')));


            /*
          echo "Contract ID: ".$data[0];
          echo "Contract Type: ".$data[1];
          echo "Today Date: ".$data[2];
          echo "Name: ".$data[3];
          echo "ID: ".$data[4];
          echo "Starting Date: ".$data[5];
          echo "From Location: ".$data[6];
          echo "To Location: ".$data[7];
          echo "Going Time: ".$data[8];
          echo "Return Time: ".$data[9];
          echo "Cost: " .$data[10];
          echo "Mobile Number: ".$data[11];
          echo "driver name: ".$data[12];
          echo "driver phone: ".$data[13];
          echo "driver email: ".$data[14];
          echo "driver photo: ".$data[15];
          */


            VsWord::autoLoad();

            $doc = new VsWord();
            $parser = new HtmlParser($doc);
            $parser->parse('<img align="left" src="/opt/lampp/htdocs/kasper-cab/public/Dev-Server-Resources/ContractElements/contractlogo.png" style="width:343px;height:113px;">  شهري اشتراك عقد');


            $parser->parse('<h2></h2>
                       <table dir="rtl" lang="ar" >

         <tr><td>' . $data[1] . '</td><td dir="rtl" lang="ar">نوعه      </td><td>' . $data[0] . '</td><td>العقد رقم </td> </tr>
         <tr><td>' . $data[2] . '</td><td>التاريخ</td><td>' . $data[3] . '</td><td>العميل اسم</td> </tr>
         <tr><td>' . $data[6] . '</td><td>المغادرة موقع</td><td>' . $data[11] . '</td><td>المحمول الهاتف رقم</td> </tr>
         <tr><td>' . $data[7] . '</td><td>الوصول موقع</td><td>' . $data[5] . '</td><td>العقد بداية تاريخ</td> </tr>
         <tr><td>' . $data[9] . '</td><td>العودة وقت</td><td>' . $data[8] . '</td><td>الذهاب وقت</td> </tr>
         <tr> <td>' . $data[12] . '</td><td>السائق اسم</td> </tr>
         <tr> <td>' . $data[13] . '</td><td>السائق رقم</td> </tr>
         <tr> <td>' . $data[14] . '</td><td>السائق ايميل</td> </tr>
                     </table>');

/*           $parser->parse('<img align="left" src='.data[18].' >');*/


            $parser->parse('<img align="left" src="/opt/lampp/htdocs/kasper-cab/public/Dev-Server-Resources/ContractElements/terms.png"  >');
            $parser->parse('<img align="left" src="/opt/lampp/htdocs/kasper-cab/public/Dev-Server-Resources/ContractElements/Kasper-Cab-Stamp.png"  >');

            // $parser->parse($html);

            // echo '<pre>'.($doc->getDocument() -> getBody() -> look()).'</pre>';

            //change this directory
         $doc->saveAs('/opt/lampp/htdocs/kasper-cab/public/Dev-Server-Resources/ReadyContracts/Contract.docx');


/*
            header('Content-disposition: inline');
            header('Content-type: application/msword');
            readfile('/opt/lampp/htdocs/kasper-cab/public/Dev-Server-Resources/ReadyContracts/Contract.docx');*/


        } catch (Exception $e) {
            return $e->getMessage()." at line ".$e->getLine();

        }

        return "done";

    }


    //return main view of customer service panel
    public function main (){
        return View::make("web.CustomerService.main");
    }


    public function walkers(){

        //get me all walkers with their documents to view it in one page
        $walkers = Walker::all();
        $i = 0;
        $walkers_documents = array();

        foreach ($walkers as $walker) {

            $walker_docs = array();
        for($x=2; $x<=6; $x++ ){
            if($doc = WalkerDocument::where('walker_id', '=', $walker->id)->where('document_id','=',$x)->first()){
                $walker_docs[$x-2] = $doc->url;
            }else {
                $walker_docs[$x-2] = "/uploads/unavailable.jpg";
            }
        }
            $walkers_documents[$i] = $walker_docs;//number of missing docs
             $i++;
        }
        /*
         return two lists one for basic walker information and the other for each walker documents and informations
         */
        return View::make('web.CustomerService.walkers')->with('walkers', $walkers)
                                                        ->with('walkers_documents', $walkers_documents);
    }


    /*   -1 canceled or default
    *    0 pending searching for a walker
    *    1 walker accepted. "on the way"
    *    2 package picked from sender "package picked"  same as "arrived"
    *    3 delivered "completed cash posted or "cash taken from receiver and company credit updated "cash taken" same as completed.
    *    4 cash collected from the driver.
    */
    //return main view of customer service panel
    public function delivery_orders (){

        $orders = DeliveryRequest::all();
        $i = 0;
        $companies = array();
        $statuses = array ();
        $walkers = array();


        //main loop
        foreach ($orders as $order) {

            $company = Company::where('id', '=', $order->company_id)->first();
            $companies[$i] = $company;
            $status = "unknown";
            $walker = "pending";
            if ($order->status == -1) {
                $status = "canceled";

            } else if ($order->status == 0) {
                $status = "pending";

            } else if ($order->status == 1) {
                $status = "on the way";

            } else if ($order->status == 2) {
                $status = "package picked";

            } else if ($order->status == 3) {
                $status = "delivered";

            } else if ($order->status == 4){
                $status = "cash collected";
            }

            //case walker available
            if($order->confirmed_walker!=0 ){
                $confirmed_walker = Walker::where('id', '=' ,$order->confirmed_walker)->first();
                $walker = $confirmed_walker->first_name ." ".$confirmed_walker->phone;
            }//Else case walker not available

            $walkers[$i] = $walker;
            $statuses[$i] = $status;
            $i++;
        }

        return View::make("web.CustomerService.orders")
            ->with('companies',$companies)
            ->with('orders',$orders)
            ->with('statuses',$statuses)
            ->with('walkers',$walkers);


    }

    public function update_order_status()
    {
        try {
            DeliveryRequest::where('id', Request::get('id'))->update(array('status' => Request::get('status')));
        } catch (Exception  $e) {
            return $e->getMessage();
        }
        return "Done";

    }

}

















