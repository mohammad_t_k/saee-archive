<?php

/*
 * handles delivery requests tracking and data manipulations
 */

use App\Classes\Curl;


class AutoPlanningController extends \BaseController
{

    public function autoplanning()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $success_weight = Request::get('success_weight');
        $frequency_weight = Request::get('frequency_weight');
        $nationality_weight = Request::get('nationality_weight');
        $company = Request::get('company');
        $city = Request::get('city');
        $scheduled_shipment_date = Request::get('scheduled_date');

        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }
        if (!isset($success_weight)) {
            $success_weight = 0.5;
        }
        if (!isset($nationality_weight)) {
            $nationality_weight = 0.2;
        }
        if (!isset($frequency_weight)) {
            $frequency_weight = 0.2;
        }
        if (!isset($scheduled_shipment_date)) {
            $scheduled_shipment_date = 'all';
        }
        if (!isset($company)) {
            $company = 'all';
        }

        if ($scheduled_shipment_date != 'all') {
            $newestshipmentdate = $scheduled_shipment_date;
        } else {
            $newestshipmentdate = DeliveryRequest::max('scheduled_shipment_date');
        }
        $companies = Companies::all(array('id', 'company_name as name'));
        $scheduled_Dates = DeliveryRequest::select('scheduled_shipment_date')->where('d_city', 'like', $city)->whereIn('status', [2, 3])->orderBy('scheduled_shipment_date', 'desc')->distinct()->get();
        $activecaptains = array();
        /*$activeCaptaindata = DB::select("select DISTINCT ActiveCaptains from ( select DISTINCT planned_walker ActiveCaptains from delivery_request UNION
                                        select DISTINCT confirmed_walker ActiveCaptains from delivery_request UNION select DISTINCT current_walker ActiveCaptains from delivery_request
                                        ) A where ActiveCaptains <> 0");
        foreach ($activeCaptaindata as $value) {
            $activeArray[] = $value->ActiveCaptains;
        }*/
        if ($city == 'Jeddah') {
            $activeCaptaindata = DB::select("select w.id from walker w where address_latitude <> 0 and address_longitude <>0 and w.isJolly = 1 and w.city = '$city' and id  in  (select distinct confirmed_walker from delivery_request a, walker b where confirmed_walker = b.id and datediff(now(),b.created_at) < 30 and b.city = 'Jeddah' UNION select DISTINCT confirmed_walker from ( select confirmed_walker,max(scheduled_shipment_date) from delivery_request a, walker b where confirmed_walker = b.id and b.city = 'Jeddah' and datediff(now(),b.created_at) > 30 and scheduled_shipment_date is not null group by confirmed_walker having datediff(now(),max(scheduled_shipment_date)) < 30 ) A)");
            $allCaptaindata = DB::select("select w.id from walker w where address_latitude <> 0 and address_longitude <>0 and  w.city = '$city' and id NOT IN
 (select w.id from walker w where address_latitude <> 0 and address_longitude <>0 and w.isJolly = 1 and w.city = '$city' and id  in  (select distinct confirmed_walker from delivery_request a, walker b where confirmed_walker = b.id and datediff(now(),b.created_at) < 30 and b.city = 'Jeddah' UNION select DISTINCT confirmed_walker from ( select confirmed_walker,max(scheduled_shipment_date) from delivery_request a, walker b where confirmed_walker = b.id and b.city = 'Jeddah' and datediff(now(),b.created_at) > 30 and scheduled_shipment_date is not null group by confirmed_walker having datediff(now(),max(scheduled_shipment_date)) < 30 ) A))");
        } else {
            $activeCaptaindata = DB::select("select w.id from walker w where address_latitude <> 0 and address_longitude <>0 and w.isJolly = 1 and w.city = '$city'");
            $allCaptaindata = DB::select("select w.id from walker w where address_latitude <> 0 and address_longitude <>0 and isJolly = 0 and  w.city = '$city'");
        }
        if (count($allCaptaindata)) {
            foreach ($allCaptaindata as $value) {
                $allcaptainsArray[] = $value->id;
            }
        } else {
            $allcaptainsArray = [];
        }
        if (count($activeCaptaindata)) {
            foreach ($activeCaptaindata as $value) {
                $activeArray[] = $value->id;
            }
        } else {
            $activeArray = [];
        }
        $total_frequency = DB::select("select count(*) 'Total_Frequency' from (select scheduled_shipment_date,count(*) from delivery_request where  confirmed_walker <> 0 and scheduled_shipment_date is not null group by scheduled_shipment_date ) a");
        $total_frequency = $total_frequency[0]->Total_Frequency;
        Walker::whereIN('id', $activeArray)->where('total_scanned', '>', 'total_delivered')->update(array('success_ratio' => DB::raw('total_delivered/total_scanned')));
        Walker::whereIN('id', $activeArray)->where('isJolly', '=', '1')->update(array('weight' => DB::raw('success_ratio*' . $success_weight . '+frequency/' . $total_frequency . '*' . $frequency_weight . '+IF (nationality ="Saudi", ' . $nationality_weight . ',0 )')));

        //$activeArray = implode(',', $activeArray);

        //$captainsActivedataquery = "select w.id,w.first_name,w.last_name,w.city,w.phone,w.max_shipments,w.nationality,w.success_ratio,w.weight from walker w where w.id in($activeArray) order by weight desc ";

        $allshipmentsQuery = DeliveryRequest::where('scheduled_shipment_date', '=', $newestshipmentdate)
            ->where('planned_walker', '=', 0)
            ->whereIn('status', [2, 3])
            ->where(function ($query) {
                $query->where('D_latitude', '<>', '')
                    ->Where('D_longitude', '<>', '')
                    ->orWhere('d_district', '<>', '');
            });
        //Log::info($allshipmentsQuery->toSql());
        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($grouped->isEmpty()) {
            $allshipmentsQuery = $allshipmentsQuery->where('d_city', 'like', $city);
        } else {
            $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Admin City" from districts where city = "' . $city . '")';
            $allshipmentsQuery = $allshipmentsQuery->whereRaw($subcities);
        }
        if ($company != 'all') {
            $allshipmentsQuery = $allshipmentsQuery->where('company_id', '=', $company);
        }
        $activecaptains = Walker::select(['id', 'first_name', 'last_name', 'city', 'phone', 'max_shipments', 'nationality', 'success_ratio', 'weight'])
            ->whereIN('id', $activeArray)->orderBy('weight', 'desc')->get();
        $allshipments = $allshipmentsQuery->get();
        $allcaptains = Walker::select(['id', 'first_name', 'last_name', 'city', 'phone', 'max_shipments', 'nationality', 'success_ratio', 'weight'])
            ->whereIN('id', $allcaptainsArray)->orderBy('weight', 'desc')->get();
        return View::make('warehouse.autoplanning')
            ->with('activecaptains', $activecaptains)
            ->with('allcaptains', $allcaptains)
            ->with('allshipments', $allshipments)
            ->with('city', $city)
            ->with('adminCities', $adminCities)
            ->with('companies', $companies)
            ->with('company', $company)
            ->with('scheduled_Dates', $scheduled_Dates)
            ->with('shipmentDate', $newestshipmentdate);
    }

    function autoplanningrollback()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $city = Request::get('city');
        $scheduled_shipment_date = Request::get('scheduled_shipment_date');
        $rollbackplanningQuery = DeliveryRequest::where('scheduled_shipment_date', '=', $scheduled_shipment_date)
            ->where('planned_walker', '<>', 0)
            ->whereIn('status', [2, 3]);
        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($grouped->isEmpty()) {
            $rollbackplanningQuery = $rollbackplanningQuery->where('d_city', 'like', $city);
        } else {
            $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Admin City" from districts where city = "' . $city . '")';
            $rollbackplanningQuery = $rollbackplanningQuery->whereRaw($subcities);
        }
        $flag = $rollbackplanningQuery->update(['planned_walker' => 0]);
        $response_array = array(
            'success' => true,
            'shipments_rollbacked' => $flag
        );
        $response = Response::json($response_array, 200);
        return $response;
    }

    function autoPlanningAssign()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $company_id = Request::get('company');
        $distance_weight = Request::get('distance_weight');
        $ids = explode(',', Request::get('ids'));
        $captain_idsstr = Request::get('captains');
        $captain_ids = explode(',', Request::get('captains'));
        $city = Request::get('city');
        $start_pickuptime = Request::get('start_pickup_time');
        $scheduled_shipment_date = Request::get('scheduled_shipment_date');
        $end_pickuptime = Request::get('end_pickup_time');
        $max_distance = Request::get('max_distance');
        $count_planned = 0;
        $count_not_planned = 0;
        $captain_assigned_arr = array();
        $assigned_shipments_arr = array();
        $remaining_ids = array();
        $step_weight = $distance_weight / 3;
        $step_distance = $max_distance / 3;
        Log::info($max_distance . 'step distance' . $step_distance);
        $grouped = false;
        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($grouped->isEmpty()) {
            $grouped = true;
        }
        ////////////////////////////Auto Planning////////////////////////
        for ($distance = $step_distance; $distance < $max_distance; $distance += $step_distance, $distance_weight -= $step_weight) {

            Log::info('distance' . $distance);
            $remaining_ids = array();

            foreach ($ids as $id) {
                $item = DeliveryRequest::select(['d_latitude', 'd_longitude', 'd_district', 'd_city'])->where('jollychic', '=', $id)->first();
                if ($item->d_latitude == 0 || $item->d_longitude == 0 && isset($item->d_district)) {
                    $d_district = $item->d_district;
                    if ($grouped)
                        $d_district = $item->d_city + '+' + $item->d_district;
                    $district = Districts::where('district', 'like', $d_district)->get();
                    if (count($district)) {
                        $item->d_latitude = $district[0]->lat;
                        $item->d_longitude = $district[0]->lng;
                        Log::info('district lat lng');
                    }
                }
                $query = "SELECT id, (weight+$distance_weight)total_weight,max_shipments,min_shipments,"
                    . "ROUND(3956 * acos( cos( radians('$item->d_latitude') ) * "
                    . "cos( radians(address_latitude) ) * "
                    . "cos( radians(address_longitude) - radians('$item->d_longitude') ) + "
                    . "sin( radians('$item->d_latitude') ) * "
                    . "sin( radians(address_latitude) ) ) ,8) as distance "
                    . "FROM walker "
                    . "where id in ($captain_idsstr) and "
                    . "ROUND((3956 * acos( cos( radians('$item->d_latitude') ) * "
                    . "cos( radians(address_latitude) ) * "
                    . "cos( radians(address_longitude) - radians('$item->d_longitude') ) + "
                    . "sin( radians('$item->d_latitude') ) * "
                    . "sin( radians(address_latitude) ) ) ) ,8) <= $distance "
                    . "order by total_weight desc";
                Log::info($query);
                $captains = DB::select($query);
                if (count($captains) > 0) {
                    foreach ($captains as $captaindata) {
                        $capid = $captaindata->id;
                        if (isset($assigned_shipments_arr[$capid])) {
                            if ($assigned_shipments_arr[$capid]['max_shipments'] > count($assigned_shipments_arr[$capid]['planned_shipments'])) {
                                if (DeliveryRequest::where('jollychic', '=', $id)
                                        ->whereIn('status', [2, 3])
                                        ->where('planned_walker', '=', 0)
                                        ->update(['planned_walker' => $capid]) > 0) {
                                    array_push($assigned_shipments_arr[$capid]['planned_shipments'], $id);
                                    $count_planned++;
                                    break;
                                } else {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            $assigned_shipments_arr[$capid] = array('max_shipments' => $captaindata->max_shipments,
                                'min_shipments' => $captaindata->min_shipments, 'planned_shipments' => array());
                            if (DeliveryRequest::where('jollychic', '=', $id)
                                    ->whereIn('status', [2, 3])
                                    ->where('planned_walker', '=', 0)
                                    ->update(['planned_walker' => $capid]) > 0) {
                                array_push($assigned_shipments_arr[$capid]['planned_shipments'], $id);
                                array_push($captain_assigned_arr, $capid);
                                $count_planned++;
                                break;
                            } else {
                                $assigned_shipments_arr[$capid] = null;
                                continue;
                            }

                        }

                        if (!next($captaindata)) {
                            array_push($remaining_ids, $id);
                        }
                    }
                } else {
                    array_push($remaining_ids, $id);
                }
            }
            $ids = $remaining_ids;
            $count_not_planned = count($ids);
        }
        $i = 0;
        ///////////////////////////////////////Removing Minimum Shipment Captains////////////////////
        foreach ($captain_assigned_arr as $captain_id) {
            $planned_shipments = $assigned_shipments_arr[$captain_id]['planned_shipments'];
            $count_planned_shipments = count($planned_shipments);
            if ($assigned_shipments_arr[$captain_id]['min_shipments'] > $count_planned_shipments) {
                DeliveryRequest::whereIn('jollychic', $planned_shipments)->whereIn('status', [2, 3])
                    ->where('planned_walker', '=', $captain_id)->update(['planned_walker' => 0]);
                foreach ($planned_shipments as $planned_shipment)
                    array_push($remaining_ids, $planned_shipment);
                $count_not_planned += $count_planned_shipments;
                $count_planned -= $count_planned_shipments;
                unset($assigned_shipments_arr[$captain_id]);
                unset($captain_assigned_arr[$i]);
            }
            $i++;
        }
        $settings = Settings::where('key', 'cancel_shipment_planning_captain_timeout')->first();
        $cancel_shipment_planning_captain_timeout = $settings->value;
        $currentDate = date("m-d-Y H:i:s");
        $currentDate_timestamp = strtotime($currentDate);
        $cancel_timestamp_expiry = strtotime("+$cancel_shipment_planning_captain_timeout minutes", $currentDate_timestamp);
        /////////////////////////////////////////SEND Push to captain app/////////////////////////////////////////////
        foreach ($captain_assigned_arr as $captain_id) {
            $suggested_pickup_time = DB::select("SELECT concat('$scheduled_shipment_date ',SEC_TO_TIME(FLOOR(TIME_TO_SEC('$start_pickuptime') + RAND() * (TIME_TO_SEC(TIMEDIFF('$end_pickuptime', '$start_pickuptime')))))) 'SuggestedPickupTimestamp'");
            $captain_info = Walker::find($captain_id);
            $captain_info->suggested_pickup_time = $suggested_pickup_time[0]->SuggestedPickupTimestamp;
            $captain_info->cancel_planned_shipments_expiry_timestamp = $cancel_timestamp_expiry;
            $captain_info->save();
            $assigned_shipments_arr[$captain_id]['name'] = $captain_info->first_name . ' ' . $captain_info->last_name;
            $assigned_shipments_arr[$captain_id]['phone'] = $captain_info->phone;
            $assigned_shipments_arr[$captain_id]['city'] = $captain_info->city;
            $assigned_shipments_arr[$captain_id]['weight'] = $captain_info->weight;
            $assigned_shipments_arr[$captain_id]['success_ratio'] = $captain_info->success_ratio;
            $assigned_shipments_arr[$captain_id]['suggested_pickup_time'] = $suggested_pickup_time[0]->SuggestedPickupTimestamp;
            $max_shipments = $assigned_shipments_arr[$captain_id]['max_shipments'];
            $planned_shipments = $assigned_shipments_arr[$captain_id]['planned_shipments'];
            $count_planned_shipments = count($planned_shipments);
            $assigned_shipments_arr[$captain_id]['count_planned_shipments'] = $count_planned_shipments;
            $title = 'You have ' . $count_planned_shipments . ' planned items ready to pickup ';
            $title .= 'لديك';
            $title .= $count_planned_shipments;
            $title .= 'شحنة جاهزة للتوصيل';
            $response_array = array('success' => true,
                'message' => $title,
                'max_shipments' => $max_shipments,
                'planned_shipments' => $planned_shipments,
                'cancel_planned_shipments_expiry_timestamp' => $cancel_timestamp_expiry,
                'count_planned_shipments' => $count_planned_shipments,
                'unique_id' => 1002
            );
            $message = $response_array;
            send_notifications(226, "scanner", $title, $message);
        }

        $response_array = array(
            'success' => true,
            'count_planned' => $count_planned,
            'count_not_planned' => $count_not_planned,
            'captains_planned' => array_values($captain_assigned_arr),
            'shipments_planned' => $assigned_shipments_arr,
            'shipments_not_planned' => $remaining_ids
        );
        $response = Response::json($response_array, 200);
        return $response;
    }

    function unplanshipmentsforcaptain()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $city = Request::get('city');
        $scheduled_shipment_date = Request::get('scheduled_shipment_date');
        $captain_id = Request::get('captain_id');
        //$distance_weight = Request::get('distance_weight');
        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }
        if (!isset($scheduled_shipment_date)) {
            $scheduled_shipment_date = 'all';
        }
        if ($scheduled_shipment_date != 'all') {
            $newestshipmentdate = $scheduled_shipment_date;
        } else {
            $newestshipmentdate = DeliveryRequest::max('scheduled_shipment_date');
        }
        $allshipmentsQuery = DeliveryRequest::select('jollychic', 'd_city', 'cash_on_delivery', 'receiver_name', 'd_district')->where('scheduled_shipment_date', '=', $newestshipmentdate)
            ->where('planned_walker', '=', 0)
            ->whereIn('status', [2, 3])
            ->where(function ($query) {
                $query->where('D_latitude', '<>', '')
                    ->Where('D_longitude', '<>', '')
                    ->orWhere('d_district', '<>', '');
            });
        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($grouped->isEmpty()) {
            $allshipmentsQuery = $allshipmentsQuery->where('d_city', 'like', $city);
        } else {
            $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Admin City" from districts where city = "' . $city . '")';
            $allshipmentsQuery = $allshipmentsQuery->whereRaw($subcities);
        }
        $allshipments = $allshipmentsQuery->get();
        if ($allshipmentsQuery->count() == 0) {
            return 0;
        }
        $count = 0;
        foreach ($allshipments as $id) {
            $jollyic = $id->jollychic;
            $item = DeliveryRequest::select(['d_latitude', 'd_longitude', 'd_district', 'd_city',])->where('jollychic', '=', $jollyic)->first();
            if ($item->d_latitude == 0 || $item->d_longitude == 0 && isset($item->d_district)) {
                $d_district = $item->d_district;
                if ($grouped)
                    $d_district = $item->d_city + '+' + $item->d_district;
                $district = Districts::where('district', 'like', $d_district)->get();
                if (count($district)) {
                    $item->d_latitude = $district[0]->lat;
                    $item->d_longitude = $district[0]->lng;
                    Log::info('district lat lng');
                }
            }
            $count++;
            $query = "SELECT id,"
                . "ROUND(3956 * acos( cos( radians(address_latitude) ) * "
                . "cos( radians('$item->d_latitude') ) * "
                . "cos( radians('$item->d_longitude') - radians(address_longitude) ) + "
                . "sin( radians(address_latitude) ) * "
                . "sin( radians('$item->d_latitude') ) ) ,8) as distance "
                . "FROM walker "
                . "where id in ($captain_id) "
                . "order by distance asc";
            $distance = DB::select($query);
            $data[] = array(
                'waybill' => $id['jollychic'],
                'receiver_name' => $id['receiver_name'],
                'd_city' => $id['d_city'],
                'd_district' => $id['d_district'],
                'cod' => $id['cash_on_delivery'],
                'captain_id' => $distance[0]->id,
                'distance' => $distance[0]->distance,
            );
        }
        $captaininfo = Walker::select(['id', 'first_name', 'last_name', 'phone', 'success_ratio', 'max_shipments', 'min_shipments'])->where('id', '=', $captain_id)->first();
        $plannedforcaptain = DeliveryRequest::where('planned_walker', '=', $captain_id)->whereIn('status', [2, 3])->count();
        $response_array = array(
            'success' => true,
            'un_planned_shipments' => $data,
            'captaininfo' => $captaininfo,
            'plannedforcaptain' => $plannedforcaptain,
            'count' => $count
        );
        $response = Response::json($response_array, 200);
        return $response;


    }

    public function assignshipmenttocaptain()
    {

        try {
            $captainid = Request::get('captainid');
            $jollychic_id = Request::get('jollychic');
            $flag = DeliveryRequest::where('jollychic', '=', $jollychic_id)->where('planned_walker', '=', 0)->whereIn('status', [2, 3])
                ->update(['planned_walker' => $captainid]);
            if ($flag > 0) {
                $response_array = array('success' => true, 'msg' => 'Shipment with waybill ' . $jollychic_id . ' have been Assign to Captain');
                $response = Response::json($response_array, 200);
                return $response;
            } else {
                return 0;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function captainsforunplanshipment()
    {

        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $city = Request::get('city');
        $jollychic = Request::get('jollychic');
        //$distance_weight = Request::get('distance_weight');
        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }

        if ($city == 'Jeddah') {
            $walker_id = DB::table('orders_history')->where('status', '=', 4)->whereDate('updated_at', '=', '2018-07-01')->distinct()->orderBy('walker_id')->lists('walker_id');
            $activeCaptainQuery = Walker::select('id')->Where('address_latitude', '<>', 0)->Where('address_longitude', '<>', 0)->Where('isJolly', '=', 1)->Where('city', '=', $city)
                ->whereIn('id', $walker_id);
        } else {
            $activeCaptainQuery = Walker::select('id')->Where('address_latitude', '<>', 0)->Where('address_longitude', '<>', 0)->Where('isJolly', '=', 1)->Where('city', '=', $city);
        }
        $activeCaptain = $activeCaptainQuery->get();
        if ($activeCaptainQuery->count() == 0) {
            return 0;
        }
        $count = 0;
        $shipmentquery = DeliveryRequest::select(['jollychic', 'receiver_name', 'receiver_phone', 'd_address', 'cash_on_delivery', 'd_latitude', 'd_longitude', 'd_district', 'd_city',])
            ->where('jollychic', '=', $jollychic)->where('planned_walker', '=', 0);
        $shipment = $shipmentquery->first();
        if ($shipmentquery->count() == 0) {
            return 1;
        }
        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($shipment->d_latitude == 0 || $shipment->d_longitude == 0 && isset($shipment->d_district)) {
            $d_district = $shipment->d_district;
            if ($grouped)
                $d_district = $shipment->d_city + '+' + $shipment->d_district;
            $district = Districts::where('district', 'like', $d_district)->get();
            if (count($district)) {
                $shipment->d_latitude = $district[0]->lat;
                $shipment->d_longitude = $district[0]->lng;
            }
        }
        foreach ($activeCaptain as $id) {
            $captain_id = $id->id;
            $count++;
            $query = "SELECT id,first_name, last_name,city,phone,max_shipments,success_ratio,"
                . "ROUND(3956 * acos( cos( radians('$shipment->d_latitude') ) * "
                . "cos( radians(address_latitude) ) * "
                . "cos( radians(address_longitude) - radians('$shipment->d_longitude') ) + "
                . "sin( radians('$shipment->d_latitude') ) * "
                . "sin( radians(address_latitude) ) ) ,8) as distance "
                . "FROM walker "
                . "where id in ($captain_id)"
                . "order by distance asc";
            $distance = DB::select($query);
            $plannedforcaptain = DeliveryRequest::where('planned_walker', '=', $captain_id)->whereIn('status', [2, 3])->count();
            $data[] = array(
                'captain_id' => $distance[0]->id,
                'first_name' => $distance[0]->first_name,
                'last_name' => $distance[0]->last_name,
                'captain_city' => $distance[0]->city,
                'success_ratio' => $distance[0]->success_ratio,
                'phone' => $distance[0]->phone,
                'plannedforcaptain' => $plannedforcaptain,
                'max_shipments' => $distance[0]->max_shipments,
                'distance' => $distance[0]->distance,
            );

        }
        $response_array = array(
            'success' => true,
            'shipmentinfo' => $shipment,
            'captainsforunplanshipment' => $data,
            'count' => $count
        );
        $response = Response::json($response_array, 200);
        return $response;
    }
	
	public function missionreservation()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $city = Request::get('city');
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }
        $cityHubs = Hub::select('id as hub_id','name','enable_missions_creation')->where('city', '=', $city)->get();
        $hub = Request::get('hub');
        if (!isset($hub)) {
            if (isset($cityHubs[0]))
                $hub = $cityHubs[0]->hub_id;
            $enableMissionCreation = $cityHubs[0]->enable_missions_creation;
        } else{
            $enableMissionCreation= Hub::select('enable_missions_creation')->where('id', '=', $hub)->first()->enable_missions_creation;
        }
        $scheduled_shipment_date = Request::get('scheduled_date');
        if (!isset($scheduled_shipment_date)) {
            $scheduled_shipment_date = 'all';
        }

        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $scheduled_Dates = DeliveryRequest::select('scheduled_shipment_date')->where('d_city', 'like', $city)->whereIn('status', [2, 3])->orderBy('scheduled_shipment_date', 'desc')->distinct()->get();

            $newestshipmentdate = $scheduled_shipment_date;

        $Missions_query = Mission::leftJoin('walker', 'mission.reserve_captain', '=', 'walker.id')->leftJoin('walker as con_walker', 'mission.planned_captain', '=', 'con_walker.id')
            ->leftJoin('mission_statuses', 'mission_statuses.id', '=', 'mission.status')
            ->select(
                ['mission.id',
                    'mission.mission_waybill',
                    'mission.mission_name',
                    'mission.city',
                    'mission.district',
                    'mission.total_shipments',
                    'mission.scheduled_shipment_date',
                    'mission_statuses.english as mission_status',
                    'mission.is_notify',
                    'walker.first_name as first_name',
                    'walker.last_name as last_name',
                    'walker.phone as phone',
                    'con_walker.first_name as confirst_name',
                    'con_walker.last_name as conlast_name',
                    'con_walker.phone as conphone',
                ])->where('mission.city', '=', $city);

        $groupbyresult_query = 'SELECT count(jollychic) as shipments,scheduled_shipment_date,d_district,d_city FROM `delivery_request` WHERE status in(2,3) and d_city="' . $city . '"
                                  and d_district <> "" and mission_id = 0 and hub_id="' . $hub . '"';
        if ($scheduled_shipment_date != 'all') {
            $groupbyresult_query .= ' and scheduled_shipment_date="' . $newestshipmentdate . '"';
            $Missions_query = $Missions_query->where('mission.scheduled_shipment_date', '=', $newestshipmentdate);
        }
        $groupbyresult_query .= ' GROUP by scheduled_shipment_date,d_district order by scheduled_shipment_date DESC';
        $groupbyresult = DB::Select($groupbyresult_query);

        $Missions = $Missions_query->whereNotIn('status', ['5', '6', '7'])->get();


        return View::make('warehouse.missionreservation')
            ->with('allshipments', $groupbyresult)->with('adminCities',$adminCities)->with('city', $city)->with('cityHubs',$cityHubs)->with('hub',$hub)->with('enableMissionCreation',$enableMissionCreation)
            ->with('shipmentDate', $newestshipmentdate)->with('scheduled_Dates',$scheduled_Dates)->with('Missions',$Missions);

    }

    public function gethubsbycity()
    {
        $city = Request::get('city');
        $adminHubs = Hub::select('id as hub_id','name','enable_missions_creation')->where('city', '=', $city)->get();
        $response_array = array('success' => true, 'city' => $city, 'hubs' => $adminHubs);
        $response = Response::json($response_array, 200);
        return $response;
    }
    public function isHubMissionEnabled()
    {
        $city = Request::get('city');
        $hub = Request::get('hub');
        $isMIssionEnabled = Hub::select('id as hub_id','enable_missions_creation')->where('city', '=', $city)->where('id','=',$hub)->get();
        $response_array = array('success' => true, 'hub' => $hub, 'hub' => $isMIssionEnabled);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function generatemissions()
    {

        $responce = 0;
        try {

            $city = Request::get('city');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');
            if ($scheduled_shipment_date == 'all' || $scheduled_shipment_date == '') {
                $response_array = array('success' => true, 'error' => 'Please select scheduled_shipment_date for which you want to create mission.');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $maxShipment = Request::get('maxShipment');
            if (!isset($maxShipment)) {
                $maxShipment = 25;
            }
            $hub = Request::get('hub');
            $groupbyresult = DB::Select('SELECT count(jollychic) as total_shipments,scheduled_shipment_date,d_district,d_city FROM `delivery_request` WHERE status in(2,3) and d_city="' . $city . '"
                                  and d_district <> "" and mission_id = 0 and hub_id="'.$hub.'" and scheduled_shipment_date="' . $scheduled_shipment_date . '" GROUP by scheduled_shipment_date,d_district');
            if(empty($groupbyresult)){
                $response_array = array('success' => true, 'message' => 'No Mission found for city ' . $city . ' with scheduled_shipment_date = ' . $scheduled_shipment_date);
                $response = Response::json($response_array, 200);
                return $response;
            }
            $update_mission_id = 0;
            $shipmentsofcity = 0;
            $missionsids = array();

            for ($i = 0; $i < count($groupbyresult); ++$i) {
                $total_shipments = $groupbyresult[$i]->total_shipments;
                $scheduled_shipment_date = $groupbyresult[$i]->scheduled_shipment_date;
                $mission_city = $groupbyresult[$i]->d_city;
                $district = $groupbyresult[$i]->d_district;
                $isRemote = Districts::select('is_remote')->where('city', '=', $mission_city)->where('district', 'like', '%' . $district)
                    ->orWhere('district_ar', 'like', '%' . $district)->first();
                if (!empty($isRemote) && $isRemote->is_remote == 1) {
                    $estimated_delivery_fee = 10;
                } else {
                    $estimated_delivery_fee = 9;
                }
                $city_info = City::select('c_code')->where('name', '=', $mission_city)->first();
                if (!empty($city_info) && $city_info->c_code != '') {
                    $post_waybill = $city_info->c_code;
                } else {
                    $post_waybill = 'MIS';
                }

                if ($total_shipments > $maxShipment) {
                    $total_mission = ceil($total_shipments / $maxShipment);

                    $remaining_missions_shipments = $total_shipments;
                    for ($b = 0; $b < $total_mission; ++$b) {
                        if ($remaining_missions_shipments > $maxShipment) {
                            $missionShipments = $maxShipment;
                            $remaining_missions_shipments = $remaining_missions_shipments - $maxShipment;
                        } else {
                            $missionShipments = $remaining_missions_shipments;
                        }

                        $misssion_name = $district . ' District-' . $missionShipments;
                        $mission = new Mission();
                        $mission->city = $mission_city;
                        $mission->mission_name = $misssion_name;
                        $mission->district = $district;
                        $mission->estimated_delivery_fee = $estimated_delivery_fee * $missionShipments;
                        $mission->total_shipments = $missionShipments;
                        $mission->scheduled_shipment_date = $scheduled_shipment_date;
                        $mission->save();

                        $mission = Mission::find($mission->id);
                        $waybill = 'SM' . str_pad($mission->id, 7, "0", STR_PAD_LEFT) . $post_waybill;
                        $mission->mission_waybill = $waybill;
                        $mission->save();

                        $jollycic_ids = DeliveryRequest::select('jollychic')->where('d_city', '=', $mission_city)->where('mission_id', '=', 0)->where('d_district', 'like', $district)->wherein('status', [2, 3])->
                        whereDate('scheduled_shipment_date', '=', $scheduled_shipment_date)->get();
                        for ($a = 0; $a < $missionShipments; ++$a) {
                            $jollychic = $jollycic_ids[$a]->jollychic;
                            $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->wherein('status', [2, 3])->whereDate('scheduled_shipment_date', '=', $scheduled_shipment_date)
                                ->update(array('mission_id' => $mission->id));
                            if ($flag > 0) {
                                $update_mission_id++;
                            }
                        }
                        $shipmentsofcity += $missionShipments;

                        array_push($missionsids, $mission->id);
                    }
                } else {
                    $misssion_name = $district . ' District-' . $total_shipments;
                    $mission = new Mission();
                    $mission->city = $mission_city;
                    $mission->mission_name = $misssion_name;
                    $mission->district = $district;
                    $mission->estimated_delivery_fee = $estimated_delivery_fee * $total_shipments;
                    $mission->total_shipments = $total_shipments;
                    $mission->scheduled_shipment_date = $scheduled_shipment_date;
                    $mission->save();
                    $mission = Mission::find($mission->id);
                    $waybill = 'SM' . str_pad($mission->id, 7, "0", STR_PAD_LEFT) . $post_waybill;
                    $mission->mission_waybill = $waybill;
                    $mission->save();

                    $jollycic_ids = DeliveryRequest::select('jollychic')->where('d_city', '=', $mission_city)->where('mission_id', '=', 0)->where('d_district', 'like', $district)->wherein('status', [2, 3])->
                    whereDate('scheduled_shipment_date', '=', $scheduled_shipment_date)->get();
                    for ($a = 0; $a < count($jollycic_ids); ++$a) {
                        $jollychic = $jollycic_ids[$a]->jollychic;
                        $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->wherein('status', [2, 3])->whereDate('scheduled_shipment_date', '=', $scheduled_shipment_date)
                            ->update(array('mission_id' => $mission->id));
                        if ($flag > 0) {
                            $update_mission_id++;
                        }
                    }
                    $shipmentsofcity += $total_shipments;

                    array_push($missionsids, $mission->id);
                }
            }

            if (!empty($groupbyresult) && ($shipmentsofcity == $update_mission_id)) {
                $items = Mission::where('city', '=', $city)->wherein('id', $missionsids)->get();
                $response_array = array('success' => true, 'message' => 'Mission created Successfully', 'new_records' => $items);
                $response = Response::json($response_array, 200);
                return $response;
            } else {
                $response_array = array('success' => true, 'message' => 'No Mission found for city ' . $city . ' with scheduled_shipment_date = ' . $scheduled_shipment_date);
                $response = Response::json($response_array, 200);
                return $response;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $responce;
    }

    public function reservecaptain()
    {
        $count = 0;
        try {
            $captainid = Request::get('captainid');
            $mission_ids = explode(',', Request::get('ids'));
            $captain_city = Request::get('captain_city');
            $start_pickuptime = Request::get('start_pickup_time');
            $end_pickuptime = Request::get('end_pickup_time');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');

            $suggested_pickup_time = DB::select("SELECT concat('$scheduled_shipment_date ',SEC_TO_TIME(FLOOR(TIME_TO_SEC('$start_pickuptime') + RAND() * (TIME_TO_SEC(TIMEDIFF('$end_pickuptime', '$start_pickuptime')))))) 'SuggestedPickupTimestamp'");
            $message = 'Some missions have been reserved for you. Have a look!';

            foreach ($mission_ids as $id) {

                $flag = Mission::where('id', '=', $id)->where('reserve_captain','=',0)->where('planned_captain','=',0)->where('city', '=', $captain_city)->update(array('reserve_captain' => $captainid ,
                    'suggested_pickup_time' => $suggested_pickup_time[0]->SuggestedPickupTimestamp,'is_notify' => 1,'status' => 1));
                 Log::info('Access is and flag is '.$flag);
				 if ($flag > 0) {
                     //DeliveryRequest::where('mission_id','=',$id)->wherein('status', [2, 3])->update(array('planned_walker' => $captainid));
                     $count = $count + 1;
                 }
            }
            if($count >0) {
                if (isset($message)) {
                    if (isset($captainid)) {
                        $response_array = array(
                            'success' => true,
                            'unique_id' => '1003',
                            'message' => $message,
                        );
                        $data = $response_array;
                        $title = "Missions Reserved";
                        send_notifications($captainid, "scanner", $title, $data);

                    }
                }
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function unreservecaptain()
    {
        $count = 0;
        try {
            $captainid = Request::get('captainid');
            $mission_ids = explode(',', Request::get('ids'));
            $message = 'Admin has unreserved some of your missions. Have a look at your current ones!';
            foreach ($mission_ids as $id) {
                $flag = Mission::where('id', '=', $id)->where('reserve_captain','=',$captainid)->where('planned_captain','=',0)->update(array('reserve_captain' => 0,'suggested_pickup_time' => Null, 'is_notify' => 0,'status' => 0));
                if ($flag > 0) {
                    //DeliveryRequest::where('mission_id','=',$id)->wherein('status', [2, 3])->update(array('planned_walker' => 0));
                    $count = $count + 1;
                }
            }
            if($count >0) {
                if (isset($message)) {
                    if (isset($captainid)) {
                        $response_array = array(
                            'success' => true,
                            'unique_id' => '1004',
                            'message' => $message,
                        );
                        $data = $response_array;
                        $title = "Missions Unreserved";
                        send_notifications($captainid, "scanner", $title, $data);

                    }
                }
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function sendMissionNotificationToActiveCaptains() {

        try {
            $city = Request::get('city');
            $mission_ids = explode(',', Request::get('ids'));
            $start_pickuptime = Request::get('start_pickup_time');
            $end_pickuptime = Request::get('end_pickup_time');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');
            $suggested_pickup_time = DB::select("SELECT concat('$scheduled_shipment_date ',SEC_TO_TIME(FLOOR(TIME_TO_SEC('$start_pickuptime') + RAND() * (TIME_TO_SEC(TIMEDIFF('$end_pickuptime', '$start_pickuptime')))))) 'SuggestedPickupTimestamp'");
            $message = 'New missions have been created for you. Have a look!';

            $count=0;
            foreach ($mission_ids as $id) {
                $flag = Mission::where('id', '=', $id)->where('reserve_captain','=',0)->where('planned_captain','=',0)->where('is_notify','=',0)->update(array('suggested_pickup_time' => $suggested_pickup_time[0]->SuggestedPickupTimestamp, 'is_notify' => 1));
                if ($flag > 0) {
                    $count = $count + 1;
                }
            }

            if($count > 0) {
                $activeCaptaindata=Walker::where('is_approved','=',1)->where('city','=',$city)->get(['id']);

                foreach ($activeCaptaindata as $value) {
                    $activeArray[] = $value->id;
                }

                if (isset($message)) {
                    if (!empty($activeArray)) {
                        $response_array = array(
                            'success' => true,
                            'unique_id' => '1005',
                            'message' => $message,
                        );
                        $data = $response_array;
                        $title = "Missions Arrived";
                        $activecaptains=array_chunk($activeArray,999);
                        foreach($activecaptains as $activecaptain){
                            send_notifications($activecaptain, "scanner", $title, $data);
                        }

                    }
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function unplancaptain()
    {
        $count = 0;
        try {
            $mission_ids = explode(',', Request::get('ids'));
            $city = Request::get('city');
            $is_mission_planned = Mission::wherein('id', $mission_ids)->where('reserve_captain','<>',0)->where('planned_captain','<>',0)->count();
            if($is_mission_planned == 0){
                $response_array = array('success' => false, 'error' => 'Mission already un planned');
                $response = Response::json($response_array, 200);
                return $response;
            }

            foreach ($mission_ids as $id) {
                $flag = DeliveryRequest::where('mission_id','=',$id)->where('d_city','=',$city)->where('planned_walker','<>',0)->where('confirmed_walker', '=',0)
                    ->wherein('status', [2, 3])->update(array('planned_walker' => 0));
                if ($flag > 0) {
                    Mission::where('id', '=', $id)->where('reserve_captain','<>',0)->where('planned_captain','<>',0)
                        ->update(array('reserve_captain' => 0 , 'planned_captain' => 0, 'is_notify' => 0,'status' => 0));
                    $count = $count + 1;
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }


    public function missionsrollback()
    {
        $count = 0;
        try {
            $city = Request::get('city');
            $scheduled_shipment_date = Request::get('scheduled_shipment_date');
            $mission_ids = explode(',', Request::get('ids'));

            foreach ($mission_ids as $id) {
                $flag = Mission::select('mission_name')->where('city','=',$city)->where('reserve_captain','=',0)->where('planned_captain','=',0)
                    ->where('scheduled_shipment_date','=',$scheduled_shipment_date)->where('id','=', $id)->update(array('status' => 5));
                if ($flag > 0) {
                    DeliveryRequest::where('mission_id','=',$id)->where('d_city','=',$city)
                       ->update(array('mission_id' => 0));
                    $count = $count + 1;
                }
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function createmission()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();

        return View::make('warehouse.createMission')
            ->with('adminCities', $adminCities);
    }

    public function admincreatemission()
    {
        $city = Request::get('city');
        //$hub = Request::get('hub');
        $ids = explode(',', Request::get('ids'));

        /*$unscheduled = DeliveryRequest::whereIn('jollychic', $ids)->whereNull('scheduled_shipment_date')->count();
        if ($unscheduled > 0) {
            $response_array = array('success' => false, 'error' => 'Scheduled shipment date is empty. Please try again.');
            $response = Response::json($response_array, 200);
            return $response;
        }

        $scheduled_dates = DeliveryRequest::select('scheduled_shipment_date')->whereIn('jollychic', $ids)->distinct()->get();
        if (count($scheduled_dates) > 1) {
            $response_array = array('success' => false, 'error' => 'Scheduled shipment date must be same for all shipments in mission. Please try again.');
            $response = Response::json($response_array, 200);
            return $response;
        }

        $nohub_id = DeliveryRequest::select('hub_id')->where('hub_id', '=', 0)->whereIn('jollychic', $ids)->count();
        if ($nohub_id > 0) {
            $response_array = array('success' => false, 'error' => 'Hub must be set for all shipments in mission. Please try again.');
            $response = Response::json($response_array, 200);
            return $response;
        }
        $hub_id_mismatch = DeliveryRequest::whereIn('jollychic', $ids)->where('hub_id','<>',$hub)->count();
        if ($hub_id_mismatch > 0) {
            $response_array = array('success' => false, 'error' => 'Please eneter shipments of selected Hub.');
            $response = Response::json($response_array, 200);
            return $response;
        }

        $check_district = DeliveryRequest::whereIn('jollychic', $ids)->whereRaw("(d_district <> 'No District' and d_district in (select distinct SUBSTRING_INDEX(district,'+',-1) from districts))")->count();
        $riyadh_baha = DeliveryRequest::whereIn('jollychic', $ids)->whereRaw("(d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = 'Riyadh' or city = 'Baha'))")->count();
        if ($check_district == 0 && $riyadh_baha == 0) {
            $response_array = array('success' => false, 'error' => 'Sorry, shipments have incorrect district.Please fix it from plan distribution');
            $response = Response::json($response_array, 200);
            return $response;
        }*/


        $unplan_mission_shipments = DeliveryRequest::select('jollychic', 'mission_id')->whereIn('jollychic', $ids)->where('mission_id', '<>', 0)
            ->where('status', '<', 4)->where('planned_walker', '=', 0)->where('confirmed_walker', '=', 0)->orderBy('mission_id', 'asc')->get();
        if (count($unplan_mission_shipments)) {
            for ($mi = 0; $mi < count($unplan_mission_shipments); ++$mi) {
                $jollychic = $unplan_mission_shipments[$mi]->jollychic;
                $mission_id = $unplan_mission_shipments[$mi]->mission_id;
                $mission_flag = DeliveryRequest::where('jollychic', '=', $jollychic)->where('mission_id', '=', $mission_id)->where('status', '<', 4)->where('planned_walker', '=', 0)
                    ->update(array('mission_id' => 0));
                if ($mission_flag > 0) {
                    $mission = Mission::select('total_shipments', 'estimated_delivery_fee')->where('id', '=', $mission_id)->where('planned_captain', '=', 0)->first();
                    $new_total_shipments = $mission->total_shipments - 1;
                    $misssion_name = 'SaeeMis-' . $new_total_shipments . '';
                    $estimated_delivery_fee = $mission->estimated_delivery_fee - 9.5;
                    if ($new_total_shipments == 0) {
                        Mission::where('id', '=', $mission_id)->where('planned_captain', '=', 0)
                            ->update(array('total_shipments' => $new_total_shipments, 'status' => 7));
                    } else {
                        $d_distinct_update = DeliveryRequest::where('mission_id', '=', $mission_id)->where('status', '<', '4')->where('planned_walker', '=', 0)
                            ->where('d_district', '<>', 'No District')->where('d_district', '<>', '')->distinct()->get(['d_district']);
                        $totalupdated_districts = count($d_distinct_update);
                        if ($totalupdated_districts == 0) {
                            $updated_districts = '';
                        } else {
                            $updated_districts = $d_distinct_update[0]->d_district;
                            if ($totalupdated_districts > 1) {
                                for ($i = 1; $i < $totalupdated_districts; ++$i) {
                                    $updated_districts .= ' | ' . $d_distinct_update[$i]->d_district;
                                }
                            }
                        }
                        Mission::where('id', '=', $mission_id)->where('planned_captain', '=', 0)
                            ->update(array('total_shipments' => $new_total_shipments, 'district' => $updated_districts, 'mission_name' => $misssion_name, 'estimated_delivery_fee' => $estimated_delivery_fee));
                    }
                }
            }
        }

        $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
        if ($grouped->isEmpty()) {
            $checkgroup = '0';
        } else {
            $checkgroup = '1';
        }
        $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';

        if ($checkgroup == 1) {
            $distinct_district = DeliveryRequest::whereIn('jollychic', $ids)->whereRaw($subcities)->where('mission_id', '=', 0)->where('status', '<', '4')->where('status', '>', '0')->where('planned_walker', '=', 0)
                ->where('d_district', '<>', 'No District')->where('d_district', '<>', '')->distinct()->get(['d_district']);

            $jollycic_ids = DeliveryRequest::select('jollychic')->whereIn('jollychic', $ids)->whereRaw($subcities)->where('mission_id', '=', 0)->where('status', '<', '4')->where('status', '>', '0')->
            where('d_district', '<>', 'No District')->where('d_district', '<>', '')->where('planned_walker', '=', 0)->get();

            $city_shipments = DeliveryRequest::select('jollychic')->whereIn('jollychic', $ids)->whereRaw($subcities)->count();
        } else {
            $distinct_district = DeliveryRequest::whereIn('jollychic', $ids)->where('d_city', 'like', $city)->where('mission_id', '=', 0)->where('status', '<', '4')->where('status', '>', '0')->where('planned_walker', '=', 0)
                ->where('d_district', '<>', 'No District')->where('d_district', '<>', '')->distinct()->get(['d_district']);

            $jollycic_ids = DeliveryRequest::select('jollychic')->whereIn('jollychic', $ids)->where('d_city', '=', $city)->where('mission_id', '=', 0)->where('status', '<', '4')->where('status', '>', '0')->
            where('d_district', '<>', 'No District')->where('d_district', '<>', '')->where('planned_walker', '=', 0)->get();
            $city_shipments = DeliveryRequest::select('jollychic')->whereIn('jollychic', $ids)->where('d_city', '=', $city)->count();
        }

        if ($city_shipments == 0) {
            $response_array = array('success' => false, 'error' => 'please enter shipments of select city');
            $response = Response::json($response_array, 200);
            return $response;
        }

        if (count($distinct_district) == 0) {
            $response_array = array('success' => false, 'error' => 'Sorry, mission can not be created,please set district for these shipments and try again');
            $response = Response::json($response_array, 200);
            return $response;
        }

        $districts = $distinct_district[0]->d_district;

        $total_districts = count($distinct_district);
        if ($total_districts > 1) {
            for ($i = 1; $i < $total_districts; ++$i) {
                $districts .= ' | ' . $distinct_district[$i]->d_district;
            }
        }

        $mission = new Mission();
        $mission->city = $city;
        $mission->district = $districts;
        //$mission->scheduled_shipment_date = $scheduled_dates[0]->scheduled_shipment_date;
        $mission->save();

        $missionShipments = 0;
        for ($a = 0; $a < count($jollycic_ids); ++$a) {
            $jollychic = $jollycic_ids[$a]->jollychic;
            $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->where('status', '<', 4)->where('planned_walker', '=', 0)->where('mission_id', '=', 0)
                ->update(array('mission_id' => $mission->id));
            if ($flag > 0) {
                $missionShipments++;
            }
        }
        $city_info = City::select('c_code')->where('name', '=', $city)->first();
        if (!empty($city_info) && $city_info->c_code != '') {
            $post_waybill = $city_info->c_code;
        } else {
            $post_waybill = 'MIS';
        }

        $mission = Mission::find($mission->id);
        $waybill = 'SM' . str_pad($mission->id, 7, "0", STR_PAD_LEFT) . $post_waybill;
        $misssion_name = 'SaeeMis-' . $missionShipments . '';
        $mission->mission_name = $misssion_name;
        $mission->total_shipments = $missionShipments;
        $mission->estimated_delivery_fee = 9.5 * $missionShipments;
        $mission->mission_waybill = $waybill;
        $mission->save();
        $missioninfo = Mission::where('id', '=', $mission->id)->get();

        $response_array = array('success' => true, 'count' => $missionShipments, 'missioninfo' => $missioninfo);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function sticker()
    {
        $packageID = Request::segment(3);
        $package = Mission::select('id', 'mission_waybill', 'mission_name', 'city', 'district', 'total_shipments', 'estimated_delivery_fee', 'scheduled_shipment_date')
            ->where('mission_waybill', '=', $packageID)->first();
        $delivery_request_info = DeliveryRequest::where('mission_id', '=', $package->id);//->get();
        $main_city = Districts::select('city')->distinct()->from('districts')->where('district', 'like', $package->city . '%')->first();

        $cod = $delivery_request_info->sum('cash_on_delivery');
        $company_ids = $delivery_request_info->select('company_id')->distinct()->get();
        $waybills_info = $delivery_request_info->select('jollychic')->distinct()->get();
        $waybills = '';
        foreach ($waybills_info as $waybill) {
            $waybills .= $waybill->jollychic . ' , ';
        }

        $company_names = '';
        $company_cities = '';
        foreach ($company_ids as $company) {
            $companydata = Companies::select('company_name', 'address', 'address', 'city')->where('id', '=', $company->company_id)->first();
            $company_names .= $companydata->company_name . ' , ';
            $company_cities .= $companydata->city . ', ';
        }

        /////////////////
        $city_info = City::where('name', '=', $package->city)->first();
        $destination = $city_info->c_code;

        ////////////////

        if (empty($package->mission_waybill))
            $barcode90 = '';
        else
            $barcode90 = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->mission_waybill, "C128", 1, 33) . '" alt="barcode" style=\'width: 6.4cm;height: 1.3cm;padding:1px; \'/>';
        $data = array(
            'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($package->mission_waybill, "C128", 1, 33) . '" alt="barcode"   style=\'width: 6.4cm;height: 1.3cm;padding: 1px;\'/>',
            'barcode90' => $barcode90,
            'city' => $package->city,
            'cod' => round($cod, 2),
            'pieces' => $package->total_shipments,
            'estimatedDeliverfee' => round($package->estimated_delivery_fee, 2),
            'company_name' => $company_names,
            'waybills' => $waybills,
            'districts' => $package->district,
            'customer_city' => $package->city,
            'jollychic' => $package->mission_waybill,
            'description' => 'Mission shipments',
            'destination' => $destination,
            'order_number' => '123',
            'main_city' => $main_city,
            'date' => date("Y-m-d")
        );

        return View::make('warehouse.missionsticker', $data);
    }

    public function scanmission()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $city = Admin::where('id', '=', $admin_id)->first()->default_city;
        $adminHubs = HubPrivilege::select('hub_id')->where('admin_id', '=', $admin_id)->where('hub_id','<>',0)->orderBy('id')->get();
       $hub_ids = array();
	   foreach($adminHubs as $adminHub){
		   $hub_ids[]=$adminHub->hub_id;
	   }
		$cityHubs = Hub::select('id as hub_id', 'city', 'name', 'enable_missions_creation')->whereIn('id', $hub_ids)->get();
        $hub = Request::get('hub');
        if (!isset($hub)) {
            $hub = Hub::where('city', '=', $city)->first()->id;
        }

        return View::make('warehouse.bulkplanmission')
            ->with('adminCities', $adminCities)->with('city', $city)->with('hub',$hub)->with('cityHubs',$cityHubs);
    }

    public function planmissiontocaptain()
    {
        $city = Request::get('city');
        $captain_id = Request::get('captain_id');
        $mission_waybill = Request::get('mission_waybill');
        $hub = Request::get('hub');

        $pendingpayment = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 5)->where('is_money_received', '=', 0)->count();
        if ($pendingpayment > 0) {
            $response_array = array('success' => false, 'error' => 'sorry,captain still has ' . $pendingpayment . '  delivered/pending cash on delivery. please release them first.');
            $response = Response::json($response_array, 200);
            return $response;
        }
        $pendingcoveredamount = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 5)->where('is_amount_covered', '=', 1)->count();
        if ($pendingcoveredamount > 0) {
            $response_array = array('success' => false, 'error' => 'sorry, captain ' . $captain_id . ' still has pending covered amount. please release them first.');
            $response = Response::json($response_array, 200);
            return $response;
        }
        $pendingcount = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 6)->count();
        if ($pendingcount > 0) {
            $response_array = array('success' => false, 'error' => 'sorry, captain still has ' . $pendingcount . ' undelivered pending shipments. please release them first.');
            $response = Response::json($response_array, 200);
            return $response;
        }
        $today = date('y-m-d');
        $pendingshipments = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('scheduled_shipment_date', '<', $today)->where('status', '<>', 5)->count();
        if ($pendingshipments > 0) {
            $response_array = array('success' => false, 'error' => 'sorry, captain still has ' . $pendingshipments . ' undelivered pending shipments with expired scheduled shipment date. please release them first.');
            $response = Response::json($response_array, 200);
            return $response;
        }
        $mission = Mission::select('id', 'mission_waybill', 'mission_name', 'city', 'district', 'total_shipments', 'status', 'estimated_delivery_fee', 'reserve_captain', 'planned_captain', 'total_shipments')
            ->where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)->first();
        if (count($mission) == 0) {
            $response_array = array('success' => false, 'error' => 'mission waybill ' . $mission_waybill . ' not found for city ' . $city);
            $response = Response::json($response_array, 200);
            return $response;
        } else {
            $mission_id = $mission->id;
            $mission_status = $mission->status;
            $mission_reserve_captain = $mission->reserve_captain;
            $mission_planned_captain = $mission->planned_captain;
            $mission_shipments = $mission->total_shipments;
           $unscheduled = DeliveryRequest::where('mission_id','=', $mission_id)->whereNull('scheduled_shipment_date')->count();

       $nohub_id = DeliveryRequest::select('hub_id')->where('hub_id', '=', 0)->where('mission_id','=', $mission_id)->count();
       if ($nohub_id > 0) {
           $response_array = array('success' => false, 'error' => 'Hub must be set for all shipments in mission. Please try again.');
           $response = Response::json($response_array, 200);
           return $response;
       }
       $hub_id_mismatch = DeliveryRequest::where('mission_id','=', $mission_id)->where('hub_id','<>',$hub)->count();
       if ($hub_id_mismatch > 0) {
           $response_array = array('success' => false, 'error' => 'Mission shipments must be of selected Hub. Please try again.');
           $response = Response::json($response_array, 200);
           return $response;
       }

            $status_mismatch = DeliveryRequest::where('mission_id', '=', $mission_id)->where('status', '<>', 3)->count();
            if ($status_mismatch > 0) {
                $response_array = array('success' => false, 'error' => 'For all Mission shipments status  must be equal to 3 . Please try again.');
                $response = Response::json($response_array, 200);
                return $response;
            }

       $check_district = DeliveryRequest::where('mission_id','=', $mission_id)->whereRaw("(d_district <> 'No District' and d_district in (select distinct SUBSTRING_INDEX(district,'+',-1) from districts))")->count();
       $riyadh_baha = DeliveryRequest::where('mission_id','=', $mission_id)->whereRaw("(d_city in (select distinct SUBSTRING_INDEX(district,'+',1) from districts where city = 'Riyadh' or city = 'Baha'))")->count();
       if ($check_district == 0 && $riyadh_baha == 0) {
           $response_array = array('success' => false, 'error' => 'Sorry, shipments have incorrect district.Please fix it from plan distribution');
           $response = Response::json($response_array, 200);
           return $response;
       }

            if ($unscheduled > 0) {
                $response_array = array('success' => false, 'scheduledDate_error' => 'Scheduled shipment date is empty for mission shipments');
                $response = Response::json($response_array, 200);
                return $response;
            }

            $scheduled_dates = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id','=', $mission_id)->distinct()->get();
            if (count($scheduled_dates) > 1) {
                $response_array = array('success' => false,  'scheduledDate_error' => 'Scheduled shipment date must be same for all shipments in mission');
                $response = Response::json($response_array, 200);
                return $response;
            }


            if ($mission_status == 1 && $mission_reserve_captain != $captain_id && $mission_reserve_captain != 0) {
                $response_array = array('success' => false, 'error' => 'Mission is already reserve for captain ' . $mission_reserve_captain);

            } else if ($mission_status == 2 && $mission_planned_captain != 0) {
                $response_array = array('success' => false, 'error' => 'Mission is already planned for captain ' . $mission_planned_captain);

            } else if ($mission_status == 0 && $mission_reserve_captain == 0 && $mission_planned_captain == 0) {
                $jollycic_ids = DeliveryRequest::select('jollychic','scheduled_shipment_date')->where('mission_id', '=', $mission_id)->where('status', '=', 3)
                    ->where('planned_walker', '=', 0)->where('confirmed_walker', '=', 0)->get();

                $plan_shipments = 0;
                for ($a = 0; $a < count($jollycic_ids); ++$a) {
                    $jollychic = $jollycic_ids[$a]->jollychic;
                    $Mission_scheduledShipmentDate = $jollycic_ids[0]->scheduled_shipment_date;

                    $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->where('status', '=', 3)->where('mission_id', '=', $mission_id)
                        ->where('planned_walker', '=', 0)
                        ->update(array('planned_walker' => $captain_id));
                    if ($flag > 0) {
                        $plan_shipments++;
                    }
                }
                if ($plan_shipments == $mission_shipments) {
                    Mission::where('id', '=', $mission_id)->where('mission_waybill', '=', $mission_waybill)->where('planned_captain', '=', 0)
                        ->update(array('reserve_captain' => $captain_id, 'planned_captain' => $captain_id, 'is_notify' => 1,
                            'status' => 2, 'scheduled_shipment_date' => $Mission_scheduledShipmentDate));
                    $missioninfo = Mission::where('id', '=', $mission->id)->get();
                    $response_array = array('success' => true, 'count' => $mission_shipments, 'missioninfo' => $missioninfo);
                } else {
                    $response_array = array('success' => false, 'error' => 'Mission shipments not planned.Something went wrong!');
                }

            } else {
                $response_array = array('success' => false, 'error' => 'Mission can not be planned.Something went wrong!');
            }
            $response_code = 200;
            $response = Response::json($response_array, $response_code);
            return $response;
        }
    }

    public function updatemissionScheduledDate()
    {
        try {
            $city = Request::get('city');
            $mission_waybill = Request::get('mission_waybill');
            $dispatchdate = Request::get('dispatchdate');

            $flag = Mission::where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)
                ->update(array('scheduled_shipment_date' => $dispatchdate));
            $count=0;
            if ($flag > 0) {
                $mission_id =  Mission::select('id')->where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)->first()->id;
                 DeliveryRequest::where('mission_id','=',$mission_id)->update(array( 'scheduled_shipment_date' => $dispatchdate));
                $count = $count + 1;
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $count);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function unplanmissiontocaptain()
    {
        $city = Request::get('city');
        $captain_id = Request::get('captain_id');
        $mission_waybill = Request::get('mission_waybill');
        $mission = Mission::select('id', 'reserve_captain', 'planned_captain', 'total_shipments')
            ->where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)->first();

        if (count($mission) == 0) {
            $response_array = array('success' => false, 'error' => 'mission waybill ' . $mission_waybill . ' not found for city ' . $city);
            $response = Response::json($response_array, 200);
            return $response;

        } else {
            $mission_id = $mission->id;
            $mission_planned_captain = $mission->planned_captain;

            $flag = DeliveryRequest::where('mission_id', '=', $mission_id)->where('status', '=', 3)->where('planned_walker', '=', $captain_id)->where('confirmed_walker', '=', 0)
                ->update(array('planned_walker' => 0));
            if ($flag > 0) {
                Mission::where('id', '=', $mission_id)->where('mission_waybill', '=', $mission_waybill)
                    ->where('reserve_captain', '=', $captain_id)->where('planned_captain', '=', $captain_id)
                    ->update(array('reserve_captain' => 0, 'planned_captain' => 0, 'is_notify' => 0, 'status' => 0));
                $response_array = array('success' => true, 'unplan_message' => 'Mission of ' . $mission->total_shipments . ' shipements unplanned for captain ' . $captain_id);
            } else {
                $item = DeliveryRequest::select('confirmed_walker')->where('mission_id', '=', $mission_id)->first();
                if ($mission_planned_captain != $captain_id || $mission_planned_captain == 0) {
                    $response_array = array('success' => false, 'error' => 'Mission is not planned for captain ' . $captain_id);
                } else if ($item->confirmed_walker != 0) {
                    $response_array = array('success' => false, 'error' => 'Mission shipments scan with captain.So mission can not be unplan for captain');
                } else {
                    $response_array = array('success' => false, 'error' => 'Mission can not be Un plan.something went wrong');
                }
            }

        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }


    public function copymissionwaybill()
    {
        $city = Request::get('city');
        $mission_waybill = Request::get('mission_waybill');
        $mission = Mission::select('id')->where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)->first();

        if (count($mission) == 0) {
            return 'mission waybill ' . $mission_waybill . ' not found for city ' . $city;

        } else {
            $mission_id = $mission->id;
            $waybill_ids = DeliveryRequest::select('jollychic')->where('mission_id', '=', $mission_id)->get();
            $jollychic= array();
            foreach($waybill_ids as $waybill_id){
                array_push($jollychic, $waybill_id->jollychic);
            }

            return implode(',', $jollychic);

            //$response_array = array('success' => true, 'waybills' => $jollychic,'copy_waybills'=>$copy_waybills);
        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }
    public function missionshipmentsrollback()
    {
        $count = 0;
        try {
            $city = Request::get('city');
            $mission_waybill = Request::get('mission_waybill');


            $mission = Mission::select('id', 'reserve_captain', 'planned_captain','status')->where('mission_waybill','=',$mission_waybill)->where('city', '=', $city)->first();
            if (count($mission)) {
                $reserve_captain = $mission->reserve_captain;
                $planned_captain = $mission->planned_captain;
                $mission_id = $mission->id;
                $mission_status = $mission->status;
                if ($reserve_captain != 0 && $planned_captain != 0) {
                    if ($mission_status == 2) {
                        $response_array = array('success' => false, 'error' => 'Mission shipments Planned with captain ' . $planned_captain);
                    } else if ($mission_status == 3) {
                        $response_array = array('success' => false, 'error' => 'Mission shipments Scanned with captain   ' . $planned_captain);
                    } else {
                        $response_array = array('success' => false, 'error' => 'Mission not Rollback. something went wrong');
                    }


                } else if ($planned_captain == 0) {
                    $grouped = City::select('name', 'grouped')->where('grouped', '=', '1')->where('name', '=', $city)->get();
                    if ($grouped->isEmpty()) {
                        $checkgroup = '0';
                    } else {
                        $checkgroup = '1';
                    }
                    $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';

                    $flag = Mission::select('mission_name')->where('city','=',$city)->where('reserve_captain','=',0)->where('planned_captain','=',0)
                        ->where('id','=', $mission_id)->update(array('status' => 5));

                    if ($flag > 0) {
                        $subcities = 'd_city in (select distinct SUBSTRING_INDEX(district, "+", 1) AS "Al Baha Admin City" from districts where city = "' . $city . '")';
                        if ($checkgroup == 1) {
                            DeliveryRequest::where('mission_id', '=', $mission_id)->whereRaw($subcities)->whereRaw($subcities)
                                ->update(array('mission_id' => 0));
                            $count = $count + 1;
                        } else {
                            DeliveryRequest::where('mission_id', '=', $mission_id)->where('planned_walker', '=', 0)->where('confirmed_walker', '=', 0)
                                ->where('city', '=', $city)->where('d_city', '=', $city)
                                ->update(array('mission_id' => 0));
                            $count = $count + 1;
                        }
                    }
                    $response_array = array('success' => true, 'count' => $count);
                }

            } else {
                $response_array = array('success' => false, 'error' => 'Mission waybill '.$mission_waybill .' not found for city '.$city.'.Please try again.');
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function scanmissiontocaptain()
    {
        $city = Request::get('city');
        $captain_id = Request::get('captain_id');
        $mission_waybill = Request::get('mission_waybill');
        $mission = Mission::select('id', 'reserve_captain', 'planned_captain', 'total_shipments','scheduled_shipment_date')
            ->where('mission_waybill', '=', $mission_waybill)->where('city', '=', $city)->first();

        if (count($mission) == 0) {
            $response_array = array('success' => false, 'error' => 'mission waybill ' . $mission_waybill . ' not found for city ' . $city);
            $response = Response::json($response_array, 200);
            return $response;

        } else {
            $mission_id = $mission->id;
            $mission_planned_captain = $mission->planned_captain;

            $unscheduled = DeliveryRequest::where('mission_id','=', $mission_id)->whereNull('scheduled_shipment_date')->count();
            if ($unscheduled > 0) {
                $response_array = array('success' => false, 'scheduledDate_error' => 'Scheduled shipment date is empty for mission shipments');
                $response = Response::json($response_array, 200);
                return $response;
            }

            $scheduled_dates = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id','=', $mission_id)->distinct()->get();
            if (count($scheduled_dates) > 1) {
                $response_array = array('success' => false,  'scheduledDate_error' => 'Scheduled shipment date must be same for all shipments in mission');
                $response = Response::json($response_array, 200);
                return $response;
            }

            $mission_shipments_old_status = DeliveryRequest::select('scheduled_shipment_date')->where('mission_id', '=', $mission->id)->where('status', '<>', 3)->count();
            if ($mission_shipments_old_status > 0) {
                //Mission shipments are not ready scanning
                $response_array = array('success' => false, 'error' => 'Mission shipments old status must be equal to 3. Please try again.');
                $response = Response::json($response_array, 200);
                return $response;
            }
            if ($mission_planned_captain != $captain_id) {
                $response_array = array('success' => false, 'error' => 'Mission is not plan for captain ' . $captain_id);
            } else {
                $pendingPayment = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 5)->where('is_money_received', '=', 0)->count();
                if ($pendingPayment > 0) {
                    $response_array = array('success' => false, 'error' => 'Sorry, captain still has delivered/pending cash on delivery. Please release them first.');
                    $response = Response::json($response_array, 200);
                    return $response;
                }
                $pendingCoveredAmount = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 5)->where('is_amount_covered', '=', 1)->count();
                if ($pendingCoveredAmount > 0) {
                    $response_array = array('success' => false, 'error' => 'Sorry, captain ' . $captain_id . ' still has pending covered amount. Please release them first.');
                    $response = Response::json($response_array, 200);
                    return $response;
                }
                $pendingCount = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('status', '=', 6)->count();
                if ($pendingCount > 0) {
                    $response_array = array('success' => false, 'error' => 'Sorry, captain still has undelivered pending shipments. Please release them first.');
                    $response = Response::json($response_array, 200);
                    return $response;
                }

                $today = date('Y-m-d');
                $pendingshipments = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('scheduled_shipment_date', '<', $today)->where('status', '<>', 5)->count();
                if ($pendingshipments > 0) {
                    $response_array = array('success' => false, 'error' => 'Sorry, captain still has undelivered pending shipments with expired scheduled shipment date. Please release them first.');
                    $response = Response::json($response_array, 200);
                    return $response;
                }


                $jollycic_ids = DeliveryRequest::select('jollychic','scheduled_shipment_date')->where('mission_id', '=', $mission->id)->where('status', '=', 3)
                    ->where('planned_walker', '=', $captain_id)->where('confirmed_walker', '=', 0)->get();
                $today = date('Y-m-d');
                $mission_date =$mission->scheduled_shipment_date;
                if ($mission_date != $today) {
                    $response_array = array('success' => false, 'error' => 'Today date and mission’s scheduled_shipment_date should be same.Please scan on ' . $mission_date);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
                $scan_shipments = 0;
                for ($a = 0; $a < count($jollycic_ids); ++$a) {
                    $jollychic = $jollycic_ids[$a]->jollychic;


                    $flag = DeliveryRequest::where('jollychic', '=', $jollychic)->where('status', '=', 3)
                        ->where('mission_id', '=', $mission_id)->where('planned_walker', '=', $captain_id)
                        ->update(array('status' => 4, 'confirmed_walker' => $captain_id));
                    $item = DeliveryRequest::select('order_number', 'company_id')->where('jollychic', '=', $jollychic)->first();
                    if ($flag > 0) {
                        $scan_shipments++;
                        $order_history = new OrdersHistory;
                        $order_history->waybill_number = $jollychic;
                        $order_history->order_number = $item->order_number;
                        $order_history->company_id = $item->company_id;
                        $order_history->status = 4;
                        $order_history->walker_id = $captain_id;
                        $order_history->save();
                        DeliveryRequest::where('jollychic', '=', $jollychic)->update(array(
                            'pickup_timestamp' => date('Y-m-d H:i:s')));
                        $this->sendsmstocustomer($item, $captain_id);
                    }
                }
                if ($scan_shipments == $mission->total_shipments) {
                    $mission_in_progress = Mission::where('id', '=', $mission->id)->where('city', '=', $city)
                        ->where('reserve_captain', '=', $captain_id)->where('reserve_captain', '=', $captain_id)
                        ->update(array('status' => 3));
                }
                if ($mission_in_progress > 0) {
                    //Status updated successfully
                    $response_array = array('success' => true, 'scan_message' => 'Mission of ' . $scan_shipments . ' shipments have been scanned with captain ');
                } else {

                }
            }

        }
        $response_code = 200;
        $response = Response::json($response_array, $response_code);
        return $response;
    }

    function sendsmstocustomer($item, $captain_id)
    {
        if ($item->company_id == 917 || $item->company_id == 931) {
            $sms = "عزيزي/زتي ";
            $sms .= "يوجد لديكم شحنة عن طريق ";
            $company = Companies::find($item->company_id);
            if (isset($company)) {
                $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
            }
            $sms .= "  رقم ";
            $sms .= $item->order_number;
            $sms .= "  برقم كود ";
            $sms .= $item->pincode;

            if ($item->cash_on_delivery) {
                $sms .= " ومبلغ ";
                $sms .= $item->cash_on_delivery;
                $sms .= " ريال";
            }
            $sms .= "  مع المندوب ";

            if ($captain_id > 0) {
                $walker = Walker::find($captain_id);
                $walker->total_scanned = $walker->total_scanned + 1;
                $walker->save();
                if (count($walker)) {
                    $sms .= $walker->first_name;
                    $sms .= " 0";
                    $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                }
            }
        } else {
            $sms = "عزيزي عميل ";
            $company = Companies::find($item->company_id);
            if (isset($company)) {
                $sms .= !empty($item->sender_name) ? " " . $company->name_ar . ' [' . $item->sender_name . ']' : " " . $company->name_ar . " ";
            }

            $sms .= " طلبكم سيصلكم اليوم مع المندوب ";
            //get order:

            if ($captain_id > 0) {
                $walker = Walker::select('first_name', 'phone')->where('id', '=', $captain_id)->first();

                if (count($walker)) {
                    $sms .= $walker->first_name;
                    $sms .= " 0";
                    $sms .= substr(str_replace(' ', '', $walker->phone), -9);
                }
            }

            $sms .= " رمز التسليم ";
            $sms .= $item->pincode;
            if ($item->cash_on_delivery) {
                $sms .= " ومبلغ الطلب ";
                $sms .= $item->cash_on_delivery;
                $sms .= " ريال";
            }
            $sms .= " مع تحيات ساعي";
        }


        $destinations1 = "966";
        $destinations1 .= substr(str_replace(' ', '', $item->receiver_phone), -9);

        if (isset($company) && $company->id != '952' || !isset($company)) {

            $curl = new Curl();
            //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

            $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations1&text=$sms&msg_type=2";

            $urlDiv = explode("?", $url);
            $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
            Log::info("SMSSERVICELOG" . $result);


            if ($item->receiver_phone2 != '') {
                $destinations2 = "966";
                $destinations2 .= substr(str_replace(' ', '', $item->receiver_phone2), -9);
                $curl = new Curl();
                //$url = "http://www.jawalbsms.ws/api.php/sendsms?user=cab&pass=Kaspercab123456&to=$destinations&message=$sms&sender=Kasper";

                $url = "http://smpp.oursms.net:9090/Sendsms.aspx?username=Kasper&api_secret=718ddb29a7fc48f59ce93c54d8711044&from=Saee&to=$destinations2&text=$sms&msg_type=2";

                $urlDiv = explode("?", $url);
                $result = $curl->_simple_call("post", $urlDiv[0], $urlDiv[1], array("TIMEOUT" => 3));
                Log::info("SMSSERVICELOG" . $result);
            }

        }
    }

}