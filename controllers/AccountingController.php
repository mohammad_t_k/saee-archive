<?php


use App\Classes\Curl;

class AccountingController extends BaseController
{
    public function captainAccounting()
    {
        try {

            $admin_id = Session::get('admin_id');
            if (!isset($admin_id)) {
                return Redirect::to('/warehouse/login');
            }
            //$admin_hub_id = Admin::select('hub_id')->where('id', '=', $admin_id)->first()->hub_id;
            $captainid = Request::get('captain_id');
            $hub = Request::get('hub');
            if(!isset($hub) || $hub == '' || $hub == '0') {
                $hub = 'All';
            }

            $captaindata = Walker::select('first_name','last_name','phone')->where('id','=',$captainid)->first();

            if (isset($captaindata)) {

                $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                $captain_info .= " 0";
                $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                $ordersquery = DeliveryRequest::leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')->where('confirmed_walker', '=', $captainid)->where('is_money_received', '=', 0)->where('status', '=', 5);
                if ($hub != 'All') {
                    $ordersquery = $ordersquery->where('hub_id', '=', $hub);
                }
                $orders = $ordersquery->select(['jollychic', 'cash_on_delivery', 'is_money_received','hub.name as hub_name'])->orderBy('company_id', 'asc')->orderBy('hub_id', 'asc')->get();
                $covered_orders_query = DeliveryRequest::leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')->where('confirmed_walker', '=', $captainid)->where('is_money_received', '=', 1)->where('is_amount_covered', '=', 1)->where('status', '=', 5);
                if ($hub != 'All') {
                    $covered_orders_query = $covered_orders_query->where('hub_id', '=', $hub);
                }
                $covered_orders = $covered_orders_query->select(['jollychic', 'cash_on_delivery', 'is_money_received','hub.name as hub_name'])->get();
                $pickup_orders_query = DeliveryRequest::leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')->where('pickup_walker', '=', $captainid)->where('is_pickup_cash_received', '=', 0);
                if ($hub != 'All') {
                    $pickup_orders_query = $pickup_orders_query->where('hub_id', '=', $hub);
                }
                $pickup_orders = $pickup_orders_query->select(['jollychic','cash_on_pickup', 'is_pickup_cash_received','hub.name as hub_name'])->get();
                $deliveredInfoQuery = 'select sum(cash_on_delivery) DeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 5 and is_money_received = 0 ';
                $deliveredInfo = DB::select(DB::raw($deliveredInfoQuery));
                $coveredamountInfoquery = 'select sum(cash_on_delivery) CoveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 5 and is_money_received = 1 and is_amount_covered = 1 ';
                $coveredamountInfo = DB::select(DB::raw($coveredamountInfoquery));
                $undeliveredInfoQuery = 'select sum(cash_on_delivery) UndeliveredAmount,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 6';
                $undeliveredInfo = DB::select(DB::raw($undeliveredInfoQuery));
                $withCaptainQuery = 'select sum(cash_on_delivery) withCaptain,count(jollychic) Pieces from delivery_request where confirmed_walker = ' . $captainid . ' and status = 4';
                $withCaptain = DB::select(DB::raw($withCaptainQuery));
                $deliveredAmount = isset($deliveredInfo[0]->DeliveredAmount) ? round($deliveredInfo[0]->DeliveredAmount, 2) : 0;
                $coveredAmount = isset($coveredamountInfo[0]->CoveredAmount) ? round($coveredamountInfo[0]->CoveredAmount, 2) : 0;
                $coveredamountPieces = isset($coveredamountInfo[0]->Pieces) ? $coveredamountInfo[0]->Pieces : 0;
                $deliveredPieces = isset($deliveredInfo[0]->Pieces) ? $deliveredInfo[0]->Pieces : 0;
                $undeliveredAmount = isset($undeliveredInfo[0]->UndeliveredAmount) ? round($undeliveredInfo[0]->UndeliveredAmount, 2) : 0;
                $undeliveredPieces = isset($undeliveredInfo[0]->Pieces) ? $undeliveredInfo[0]->Pieces : 0;
                $withCaptainAmount = isset($withCaptain[0]->withCaptain) ? round($withCaptain[0]->withCaptain, 2) : 0;
                $withCaptainPieces = isset($withCaptain[0]->Pieces) ? $withCaptain[0]->Pieces : 0;
                $pickedupPieces = DeliveryRequest::where('status', '=', '-1')->where('pickup_walker', '=', $captainid)->count();

                //////////
                /// update all captains page's info
                $this->updatecaptainid($captainid);
                ///////////////
                $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();

                return View::make('warehouse.captainAccounting')
                    ->with('captain_info', $captain_info)
                    ->with('captain_id', $captainid)
                    ->with('orders', $orders)
                    ->with('covered_orders', $covered_orders)
                    ->with('pickup_orders', $pickup_orders)
                    ->with('deliveredAmount', $deliveredAmount)
                    ->with('deliveredPieces', $deliveredPieces)
                    ->with('undeliveredAmount', $undeliveredAmount)
                    ->with('undeliveredPieces', $undeliveredPieces)
                    ->with('withCaptainAmount', $withCaptainAmount)
                    ->with('coveredAmount',$coveredAmount)
                    ->with('adminHubs', $adminHubs)
                    ->with('hub', $hub)
                    ->with('coveredamountPieces', $coveredamountPieces)
                    ->with('withCaptainPieces', $withCaptainPieces)
                    ->with('pickedupPieces', $pickedupPieces);
            } else {
                return Redirect::to('/warehouse/captains');
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateMoneyReceivedFromCaptain()
    {
        DB::beginTransaction();
        try {
            $admin_id = Auth::user()->id;

            $captainid = Request::get('captain_id');
            //$ids = Request::get('ids');
            $jsonarray = Request::get('jsonarray');
            $ids = [];
            foreach($jsonarray as $array_ids)
            {
                array_push($ids, $array_ids['ids']);
            }
            $ids=implode(",", $ids);Log::info($ids);
            $amountReceived = Request::get('amountReceived');
            $cashpercentage = Request::get('cashpercentage');
            $atmpercentage = Request::get('atmpercentage');
            $approval_code = Request::get('approval_code');
            if (empty($ids)) {
                Log::info('No Records Updated');
                $response_array = array(
                    'success' => false,
                    'error' => 'Please Select Packages to submit');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $ids = explode(',', $ids);
            $waybills = implode('\',\'', $ids);
            $waybills = '\'' . $waybills . '\'';
            $cashondeliveryquery = 'SELECT sum(cast(cash_on_delivery as decimal(12,2)) ) as amountsumed FROM delivery_request
                where confirmed_walker = ' . $captainid . ' and jollychic in (' . $waybills . ') and is_money_received = 0';
            $amountsumed = DB::select(DB::raw($cashondeliveryquery));
            $amount = strval($amountsumed[0]->amountsumed);
            if ($amountReceived == $amount) {
                $company_ids = DeliveryRequest::whereIn('jollychic', $ids)->distinct()->orderBy('company_id', 'asc')->get(['company_id']);
                $orders = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                    ->whereIn('jollychic', $ids)->orderBy('company_id', 'asc')->orderBy('hub_id', 'asc')
                    ->get(['jollychic','d_district', 'company_id', 'hub_id', 'cash_on_delivery', 'companies.delivery_fee as delivery_fee','companies.delivery_fee2 as delivery_fee2',
                        'companies.delivery_fee3 as delivery_fee3','d_city']);
                $j = 0;
                $length = sizeof($orders);
                $Captain_covered_amount = 0;
                $subcities = array("Ja'araneh","Rabigh","Mastura","Asfan","Zahban","Khulais","Saaber",
                    "At Taniem","Jumum","Rania","Khurma","Alhada","Turba","Laith","Ashayrah","Kharj",
                    "Amaq","Birk","Nimra","Qunfudah","Nwariah");
                foreach ($company_ids as $companyid) {
                    $hub_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('company_id', $companyid['company_id'])->distinct()->orderBy('hub_id', 'asc')->get(['hub_id']);
                    foreach ($hub_ids as $hubid) {
                        $amountReceivedCompany = 0;
                        $captainAccountHistory = new CaptainAccountHistory();

                        $captainAccountHistory->captain_id = $captainid;
                        $captainAccountHistory->admin_id = $admin_id;
                        $captainAccountHistory->company_id = $companyid['company_id'];
                        $captainAccountHistory->hub_id = $hubid['hub_id'];
                        $captainAccountHistory->debit = 0;
                        $captainAccountHistory->save();
                        $totalCaptainAmount = 0;
                        Log::info('length access'.$length);
                        for ($i = $j; $i < $length; $i++) {
                            Log::info($orders[$i]->company_id .' com'. $companyid['company_id'] .' order hub '. $orders[$i]->hub_id  .'hub  '.  $hubid['hub_id']);
                            if ($orders[$i]->company_id != $companyid['company_id']||$orders[$i]->hub_id != $hubid['hub_id']) {
                                $j = $i;
                                Log::info('i break'.$i);
                                break;
                            }
                            Log::info('i access'.$i);
                            $flag = DeliveryRequest::where('confirmed_walker', '=', $captainid)
                                ->where('jollychic', $orders[$i]->jollychic)
                                ->where('is_money_received', '=', 0)
                                ->update(array('is_money_received' => 1,'is_amount_covered' => $jsonarray[$i]['is_amount_covered']));
                            if ($flag > 0) {
                                $jolyychic_id = $orders[$i]->jollychic;
                                $d_city = $orders[$i]->d_city;
                                if (in_array($d_city, $subcities)) {
                                    $delivery_fee = 10;
                                } else {
                                    $maincity = DB::select("SELECT DISTINCT city from districts where city in('Riyadh','Baha','Qunfudah','Gizan') and SUBSTRING_INDEX(district, '+', 1) = '$d_city'");
                                    If (!empty($maincity) && $maincity[0]->city == 'Riyadh') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Baha') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Qunfudah') {
                                        $delivery_fee = 10;
                                    } else If (!empty($maincity) && $maincity[0]->city == 'Gizan') {
                                        $delivery_fee = 10;
                                    } else {
                                        $d_district = $orders[$i]->d_district;
                                        $groupedcity = DB::select("SELECT DISTINCT city from districts where  SUBSTRING_INDEX(district, '+', 1) = '$d_city'");

                                        if (empty($groupedcity)) {
                                            $isRemote = Districts::select('is_remote')->where('city', '=', $d_city)->where('district', 'like', '%' . $d_district)
                                                ->orWhere('district_ar', 'like', '%' . $d_district)->first();
                                            if (!empty($isRemote) && $isRemote->is_remote == 1) {
                                                $delivery_fee = 10;
                                            } else {
                                                $delivery_fee = 9;
                                            }
                                        } else {
                                            $groupedcityName = $groupedcity[0]->city;
                                            $isRemote = Districts::select('is_remote')->where('city', '=', $groupedcityName)->where('district', 'like', '%' . $d_district)
                                                ->orWhere('district_ar', 'like', '%' . $d_district)->first();

                                            if (!empty($isRemote) && $isRemote->is_remote == 1) {
                                                $delivery_fee = 10;
                                            } else {
                                                $delivery_fee = 9;
                                            }
                                        }

                                        /*$RemoteDelivered = DB::select("Select RemoteDelivered,Scanned,round((RemoteDelivered/Scanned)*100) 'RemoteDeliveredRate' from
        ( select sum(status=5) 'RemoteDelivered',(select sum(status = 4) from orders_history where date(created_at) = (select scheduled_shipment_date from delivery_request
         where jollychic = '$jolyychic_id') and walker_id = $captainid) 'Scanned' from delivery_request a, districts b where d_city = city and d_city = '$d_city' and d_district like district and confirmed_walker = $captainid and is_remote = 1 and
          scheduled_shipment_date = (select scheduled_shipment_date from delivery_request where jollychic = '$jolyychic_id') ) A");

                                        $Success = DB::select("select Delivered,Scanned,round((Delivered/Scanned)*100) 'SuccessRate' from ( select sum(status=5) 'Delivered',
        (select sum(status = 4) from orders_history where date(created_at) = (select scheduled_shipment_date from delivery_request where jollychic = '$jolyychic_id') and walker_id = $captainid) 'Scanned'
         from delivery_request a, districts b where d_city = city and d_city = '$d_city' and d_district like district and confirmed_walker = $captainid and
          scheduled_shipment_date = (select scheduled_shipment_date from delivery_request where jollychic = '$jolyychic_id') ) A");

                                        $RemoteDeliveredRate = isset($RemoteDelivered[0]->RemoteDeliveredRate) ? $RemoteDelivered[0]->RemoteDeliveredRate : 0;
                                        $Success_Rate = isset($Success[0]->SuccessRate) ? $Success[0]->SuccessRate : 0;
                                        if ($RemoteDeliveredRate >= 50 && $RemoteDeliveredRate <= 69 || $Success_Rate >= 80 && $Success_Rate <= 89) {
                                            $delivery_fee = $orders[$i]->delivery_fee2;
                                        } else if ($RemoteDeliveredRate >= 70 || $Success_Rate >= 90) {
                                            $delivery_fee = $orders[$i]->delivery_fee3;
                                        } else if ($Success_Rate < 80) {
                                            $delivery_fee = $orders[$i]->delivery_fee;
                                        } else {
                                            $delivery_fee = $orders[$i]->delivery_fee;
                                        }*/
                                    }
                                }
                                Log::info('new delivery fee'.$delivery_fee);
                                $captainAccount = new CaptainAccount();
                                $captainAccount->waybill = $orders[$i]->jollychic;
                                $captainAccount->captain_id = $captainid;
                                $captainAccount->delivery_fee = $delivery_fee;
                                $totalCaptainAmount += $delivery_fee;
                                $amountReceivedCompany += $orders[$i]->cash_on_delivery;
                                $captainAccount->captain_account_history_id = $captainAccountHistory->id;
                                $captainAccount->hub_id = $hubid['hub_id'];
                                $captainAccount->type = 1;
                                if ($orders[$i]->cash_on_delivery > 0) {
                                    $captainAccount->payment_method = 1;
                                    $captainAccount->amount = $orders[$i]->cash_on_delivery;
                                } else {
                                    $captainAccount->payment_method = 0;
                                    $captainAccount->amount = 0;
                                }
                                $captainAccount->admin_id = $admin_id;
                                $captainAccount->cash_transfer = round(($cashpercentage/100)*($orders[$i]->cash_on_delivery), 2);
                                $captainAccount->atm_transfer = round(($atmpercentage/100)*($orders[$i]->cash_on_delivery), 2);
                                $captainAccount->save();
                                if ($jsonarray[$i]['is_amount_covered'] == 1) {
                                    $Captain_covered_amount += $orders[$i]->cash_on_delivery;
                                }
                            }

                        }

                        $captainAccountHistory = CaptainAccountHistory::find($captainAccountHistory->id);

                        $captainAccountHistory->credit = $amountReceivedCompany;
                        $captainAccountHistory->cash_credit = round($cashpercentage/100*$amountReceivedCompany, 2);
                        $captainAccountHistory->atm_credit = round($atmpercentage/100*$amountReceivedCompany, 2);
                        $captainAccountHistory->approval_code = $approval_code;
                        $captainAccountHistory->captain_amount = $totalCaptainAmount;
                        $captainAccountHistory->save();
                    }
                }


                //Add captain Covered amount
                if ($Captain_covered_amount > 0) {
                    $Captain = Walker::find($captainid);
                    $Captain->covered_amount = $Captain->covered_amount + $Captain_covered_amount;
                    $Captain->save();
                }
                $captaindata = Walker::find($captainid);
                $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                $captain_info .= " 0";
                $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                $response_array = array(
                    'success' => true,
                    'captaindata' => $captain_info,
                    'amount' => $amount);
                $response = Response::json($response_array, 200);
                $this->updatecaptainid($captainid);
                Log::info('commited');
                DB::commit();
                return $response;
            } else {
                $this->updatecaptainid($captainid);
                $response_array = array(
                    'success' => false,
                    'error' => 'Amount Received from Captain is not equal to total of cash on delivery of delivered orders' . "\n" . '             ' . 'Please try again with ' . $amount);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $this->updatecaptainid($captainid);
        } catch (Exception $e) {
            $this->updatecaptainid($captainid);
            DB::rollback();
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated ');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updateCaptainCoveredMoney()
    {
        try {
            $captainid = Request::get('captain_id');
            $ids = Request::get('ids');
            $amountReceived = Request::get('amountReceived');
            if (empty($ids)) {
                Log::info('No Records Updated');
                $response_array = array(
                    'success' => false,
                    'error' => 'Please Select Packages to submit');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $ids = explode(',', $ids);
            $waybills = implode('\',\'', $ids);
            $waybills = '\'' . $waybills . '\'';
            $cashondeliveryquery = 'SELECT sum(cast(cash_on_delivery as decimal(12,2)) ) as amountsumed FROM delivery_request
                where confirmed_walker = ' . $captainid . ' and jollychic in (' . $waybills . ') and is_money_received = 1 and is_amount_covered = 1';
            $amountsumed = DB::select(DB::raw($cashondeliveryquery));
            $amount = strval($amountsumed[0]->amountsumed);
            if ($amountReceived == $amount) {
                $company_ids = DeliveryRequest::whereIn('jollychic', $ids)->distinct()->orderBy('company_id', 'asc')->get(['company_id']);
                $orders = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                    ->whereIn('jollychic', $ids)->orderBy('company_id', 'asc')
                    ->get(['jollychic', 'company_id', 'cash_on_delivery', 'companies.delivery_fee as delivery_fee']);
                $j = 0;
                $length = sizeof($orders);
                foreach ($company_ids as $companyid) {
                    $Captain_covered_amount = 0;
                    $CaptainCoveredMoney = new CaptainCoveredAmount();
                    $CaptainCoveredMoney->captain_id = $captainid;
                    $CaptainCoveredMoney->company_id = $companyid['company_id'];
                    $CaptainCoveredMoney->save();
                    for ($i = $j; $i < $length; $i++) {
                        if ($orders[$i]->company_id != $companyid['company_id']) {
                            $j = $i;
                            break;
                        }
                        $flag = DeliveryRequest::where('confirmed_walker', '=', $captainid)
                            ->where('jollychic', $orders[$i]->jollychic)
                            ->where('is_money_received', '=', 1)
                            ->where('is_amount_covered', '=', 1)
                            ->update(array('is_amount_covered' => 0));
                        if ($flag > 0) {
                            $Captain_covered_amount += $orders[$i]->cash_on_delivery;
                            CaptainAccount::where('waybill', $orders[$i]->jollychic)->where('captain_id', '=', $captainid)
                                ->update(array('captain_covered_amount_history_id' => $CaptainCoveredMoney->id));
                        }

                    }
                    if ($Captain_covered_amount > 0) {
                        $CaptainCoveredMoney = CaptainCoveredAmount::find($CaptainCoveredMoney->id);
                        $CaptainCoveredMoney->covered_amount = $Captain_covered_amount;
                        $CaptainCoveredMoney->save();
//subtract captain Covered amount
                        $Captain = Walker::find($captainid);
                        $Captain->covered_amount = $Captain->covered_amount - $Captain_covered_amount;
                        $Captain->save();
                    }

                }


                $captaindata = Walker::select('first_name','last_name','phone')->where('id','=',$captainid)->first();
                $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                $captain_info .= " 0";
                $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                $response_array = array(
                    'success' => true,
                    'captaindata' => $captain_info,
                    'amount' => $amount);
                $response = Response::json($response_array, 200);
                $this->updatecaptainid($captainid);
                return $response;
            } else {
                $this->updatecaptainid($captainid);
                $response_array = array(
                    'success' => false,
                    'error' => 'Amount Received from Captain is not equal to total of cash on delivery of delivered orders' . "\n" . '             ' . 'Please try again with ' . $amount);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $this->updatecaptainid($captainid);
        } catch (Exception $e) {
            $this->updatecaptainid($captainid);
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated ');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatePickupMoneyReceivedFromCaptain()
    {
        DB::beginTransaction();
        try {
            $admin_id = Auth::user()->id;

            $captainid = Request::get('captain_id');
            $ids = Request::get('ids');
            if (empty($ids)) {
                Log::info('No Records Updated');
                $response_array = array(
                    'success' => false,
                    'error' => 'Please Select Packages to submit');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $amountReceived = Request::get('amountReceived');
            $cashpercentage = Request::get('cashpercentage');
            $atmpercentage = Request::get('atmpercentage');
            $approval_code = Request::get('approval_code');
            if (empty($ids)) {
                Log::info('No Records Updated');
                $response_array = array(
                    'success' => false,
                    'error' => 'Please Select Packages to submit');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $ids = explode(',', $ids);
            $waybills = implode('\',\'', $ids);
            $waybills = '\'' . $waybills . '\'';
            $cash_on_pickup = 'SELECT sum(cast(cash_on_pickup as decimal(12,2)) ) as amountsumed FROM delivery_request
                where pickup_walker = ' . $captainid . ' and jollychic in (' . $waybills . ') and is_pickup_cash_received = 0';
            $amountsumed = DB::select(DB::raw($cash_on_pickup));
            $amount = strval($amountsumed[0]->amountsumed);
            if ($amountReceived == $amount) {
                $company_ids = DeliveryRequest::whereIn('jollychic', $ids)->distinct()->orderBy('company_id', 'asc')->get(['company_id']);
                $orders = DeliveryRequest::leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
                    ->whereIn('jollychic', $ids)->orderBy('company_id', 'asc')->orderBy('hub_id', 'asc')
                    ->get(['jollychic', 'company_id', 'cash_on_pickup', 'companies.pickup_fee as pickup_fee','hub_id']);
                $j = 0;
                $length = sizeof($orders);
                foreach ($company_ids as $companyid) {
                    $hub_ids = DeliveryRequest::whereIn('jollychic', $ids)->where('company_id', $companyid['company_id'])->distinct()->orderBy('hub_id', 'asc')->get(['hub_id']);
                    foreach ($hub_ids as $hubid) {
                        $amountReceivedCompany = 0;
                        $captainAccountHistory = new CaptainAccountHistory();

                        $captainAccountHistory->captain_id = $captainid;
                        $captainAccountHistory->admin_id = $admin_id;
                        $captainAccountHistory->company_id = $companyid['company_id'];
                        $captainAccountHistory->hub_id = $hubid['hub_id'];
                        $captainAccountHistory->banks_deposit_id = -1;
                        $captainAccountHistory->debit = 0;
                        $captainAccountHistory->save();
                        $totalCaptainAmount = 0;
                        for ($i = $j; $i < $length; $i++) {
                            if ($orders[$i]->company_id != $companyid['company_id']||$orders[$i]->hub_id != $hubid['hub_id']) {
                                $j = $i;
                                break;
                            }
                            $flag = DeliveryRequest::where('pickup_walker', '=', $captainid)
                                ->where('jollychic', $orders[$i]->jollychic)
                                ->where('is_pickup_cash_received', '=', 0)
                                ->update(array('is_pickup_cash_received' => 1));
                            if ($flag > 0) {
                                $captainAccount = new CaptainAccount();
                                $captainAccount->waybill = $orders[$i]->jollychic;
                                $captainAccount->captain_id = $captainid;
                                $captainAccount->delivery_fee = $orders[$i]->pickup_fee;
                                $captainAccount->hub_id = $hubid['hub_id'];
                                $totalCaptainAmount += $orders[$i]->pickup_fee;
                                $amountReceivedCompany += $orders[$i]->cash_on_pickup;
                                $captainAccount->captain_account_history_id = $captainAccountHistory->id;
                                $captainAccount->type = 1;
                                if ($orders[$i]->cash_on_pickup > 0) {
                                    $captainAccount->payment_method = 1;
                                    $captainAccount->amount = $orders[$i]->cash_on_pickup;
                                } else {
                                    $captainAccount->payment_method = 0;
                                    $captainAccount->amount = 0;
                                }
                                $captainAccount->admin_id = $admin_id;
                                $captainAccount->cash_transfer = round(($cashpercentage / 100) * ($orders[$i]->cash_on_pickup), 2);
                                $captainAccount->atm_transfer = round(($atmpercentage / 100) * ($orders[$i]->cash_on_pickup), 2);
                                $captainAccount->save();
                            }

                        }

                        $captainAccountHistory = CaptainAccountHistory::find($captainAccountHistory->id);

                        $captainAccountHistory->credit = $amountReceivedCompany;
                        $captainAccountHistory->cash_credit = round($cashpercentage / 100 * $amountReceivedCompany, 2);
                        $captainAccountHistory->atm_credit = round($atmpercentage / 100 * $amountReceivedCompany, 2);
                        $captainAccountHistory->approval_code = $approval_code;
                        $captainAccountHistory->captain_amount = $totalCaptainAmount;
                        $captainAccountHistory->save();
                    }
                }


                $captaindata = Walker::select('first_name','last_name','phone')->where('id','=',$captainid)->first();
                $captain_info = $captaindata->first_name . ' ' . $captaindata->last_name;
                $captain_info .= " 0";
                $captain_info .= substr(str_replace(' ', '', $captaindata->phone), -9);
                $response_array = array(
                    'success' => true,
                    'captaindata' => $captain_info,
                    'amount' => $amount);
                $response = Response::json($response_array, 200);
                $this->updatecaptainid($captainid);
                DB::commit();
                return $response;
            } else {
                $this->updatecaptainid($captainid);
                $response_array = array(
                    'success' => false,
                    'error' => 'Amount Received from Captain is not equal to total of cash on pickup orders' . "\n" . '             ' . 'Please try again with ' . $amount);
                $response = Response::json($response_array, 200);
                return $response;
            }

            $this->updatecaptainid($captainid);
        } catch (Exception $e) {
            $this->updatecaptainid($captainid);
            DB::rollback();
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated ');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function CaptainAccountReport()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $banks = Bank::all();
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $admins = Admin::whereIn('role', [3,4,5,8])->orderBy('username', 'asc')->get();
        $admin_id = 'All';
        $city = 'Jeddah';
        $company_id = 'All';
        $fromdate = '2018-01-01';
        $todate = date('Y-m-d');
        $status = 0;

        /*  $city = Request::get('city');
          if (!isset($city)) {

              $city = 'Jeddah';
          }
          $company_id = Request::get('company');
          if (!isset($company_id)) {
              $company_id = 'All';
          }
          $fromdate = Request::get('fromdate');
          if (!isset($fromdate)) {
              $fromdate = '2018-01-01';
          }
          $todate = Request::get('todate');
          if (!isset($todate)) {
              $todate = date('Y-m-d');
          } */
        $captainaccounthistoryquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')
            ->leftJoin('companies', 'companies.id', '=', 'captain_account_history.company_id')->
            leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
            leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
            leftJoin('hub', 'hub.id', '=', 'captain_account_history.hub_id')->
            select
            ([
                'captain_account_history.*',
                'hub.name as hub_name',
                'walker.city as city',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'companies.company_name as company',
                'walker.phone as phone',
                'admin.username as adminuser',
                'paid_admin.username as paid_admin_name'])
            ->whereDate('date', '>=', $fromdate)->whereDate('date', '<=', $todate)->where('banks_deposit_id', '=', 0);
        if ($city != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('walker.city', '=', $city);
        }
        if ($company_id != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('company_id', '=', $company_id);
        }
        $captainaccounthistory = $captainaccounthistoryquery->get();

        foreach ($captainaccounthistory as $key => $captainaccount) {
            $covered_money = DB::select("select sum(cash_on_delivery) as covered_money from delivery_request where jollychic in (select waybill from captain_account ca
join captain_account_history cah on ca.captain_account_history_id =cah.id where cah.id = $captainaccount->id) and is_amount_covered = 1");
            $covered_money = round(($covered_money[0]->covered_money), 2);
            $captainaccounthistory[$key]['covered_amount'] = $covered_money;
        }
        return View::make('warehouse.captainAccountReport')
            ->with('items', $captainaccounthistory)
            ->with('company_id', $company_id)
            ->with('city', $city)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('banks', $banks)
            ->with('companies', $companies)
            ->with('admins', $admins)
            ->with('admin_id', $admin_id)
            ->with('status',$status);
    }

    public function CaptainAccountReportpost()
    {
        $admin_id = Session::get('admin_id');
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $banks = Bank::all();
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $admins = Admin::whereIn('role', [3,4,5,8])->orderBy('username', 'asc')->get();

        $city = Request::get('city');
        if (!isset($city)) {

            $city = 'Jeddah';
        }
        $company_id = Request::get('company');
        if (!isset($company_id)) {
            $company_id = 'All';
        }
        $fromdate = Request::get('fromdate');
        if (!isset($fromdate)) {
            $fromdate = '2018-01-01';
        }
        $todate = Request::get('todate');
        if (!isset($todate)) {
            $todate = date('Y-m-d');
        }
        $status = Request::get('status');
        if (!isset($status)) {
            $status = 0;
        }
        $transaction = Request::get('transaction');
        if (!isset($transaction)) {
            $transaction = 'All';
        }
        $admin_id = Request::get('admin');
        if (!isset($admin_id)) {
            $admin_id = 'All';
        }
        $hub = Request::get('hub');
        if(!isset($hub) || $hub == '' || $hub == '0') {
            $hub = 'All';
        }
        $captainaccounthistoryquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')
            ->leftJoin('companies', 'companies.id', '=', 'captain_account_history.company_id')->
            leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
            leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
            leftJoin('hub', 'hub.id', '=', 'captain_account_history.hub_id')->
            select
            ([
                'captain_account_history.*',
                'hub.name as hub_name',
                'walker.city as city',
                'walker.first_name as first_name',
                'walker.last_name as last_name',
                'companies.company_name as company',
                'walker.phone as phone',
                'admin.username as adminuser',
                'paid_admin.username as paid_admin_name'])
            ->whereDate('date', '>=', $fromdate)->whereDate('date', '<=', $todate);
        if ($status == 0) {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('banks_deposit_id', '=', 0);
        } else {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('banks_deposit_id', '<>', 0);
        }
        if ($transaction != 'All') {
            if ($transaction == 1) {
                $captainaccounthistoryquery = $captainaccounthistoryquery->where('cash_credit', '<>', 0);
            } else {
                $captainaccounthistoryquery = $captainaccounthistoryquery->where('atm_credit', '<>', 0);
            }

        }
        if ($city != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('walker.city', '=', $city);
        }
        if ($hub != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('captain_account_history.hub_id', '=', $hub);
        }
        if ($company_id != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where('company_id', '=', $company_id);
        }
        if ($admin_id != 'All') {
            $captainaccounthistoryquery = $captainaccounthistoryquery->where(function ($q) use ($admin_id) {
                $q->where('captain_account_history.admin_id', 'like', '%' . $admin_id . '%')
                    ->orWhere('captain_account_history.captain_admin_id', 'like', '%' . $admin_id . '%');
            });
        }
        $captainaccounthistory = $captainaccounthistoryquery->get();

        foreach ($captainaccounthistory as $key => $captainaccount) {
            $covered_money = DB::select("select sum(cash_on_delivery) as covered_money from delivery_request where jollychic in (select waybill from captain_account ca
join captain_account_history cah on ca.captain_account_history_id =cah.id where cah.id = $captainaccount->id) and is_amount_covered = 1");
            $covered_money = round(($covered_money[0]->covered_money), 2);
            $captainaccounthistory[$key]['covered_amount'] = $covered_money;
        }

        return View::make('warehouse.captainAccountReport')
            ->with('items', $captainaccounthistory)
            ->with('company_id', $company_id)
            ->with('city', $city)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
            ->with('hub', $hub)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('banks', $banks)
            ->with('companies', $companies)
            ->with('status',$status)
            ->with('admin_id', $admin_id)
            ->with('admins', $admins)
            ->with('transaction',$transaction);
    }

    public function updateDepositMoneytoBank()
    {
        DB::beginTransaction();
        try {
            $admin_id = Auth::user()->id;
            $ids = Request::get('ids');
            $amountDeposited = Request::get('amountDeposited');
            $bankName = Request::get('bankName');
            $city = Request::get('city');
            $company = Request::get('company');
            $ids = explode(',', $ids);
            $amountquery = CaptainAccountHistory::whereIn('id', $ids)->where('banks_deposit_id', '=', 0);
            if ($company != 'All') {
                $amountquery = $amountquery->where('company_id', '=', $company);
            }
            $amount = $amountquery->sum('credit');
            if (round($amount, 2) == $amountDeposited) {
                $company_ids = CaptainAccountHistory::whereIn('id', $ids)->distinct()->get(['company_id']);
                foreach ($company_ids as $companyid) {
                    $hub_ids = CaptainAccountHistory::whereIn('id', $ids)->where('company_id', $companyid['company_id'])->distinct()->get(['hub_id']);
                    foreach ($hub_ids as $hubid) {
                        $banksDeposit = new BanksDeposit();
                        $banksDeposit->admin_id = $admin_id;
                        $banksDeposit->amount = round(CaptainAccountHistory::whereIn('id', $ids)
                            ->where('company_id', '=', $companyid['company_id'])
                            ->where('hub_id', '=', $hubid['hub_id'])
                            ->sum('credit'), 2);
                        $banksDeposit->cash_transfer = round(CaptainAccountHistory::whereIn('id', $ids)
                            ->where('company_id', '=', $companyid['company_id'])
                            ->where('hub_id', '=', $hubid['hub_id'])
                            ->sum('cash_credit'), 2);
                        $banksDeposit->atm_transfer = round(CaptainAccountHistory::whereIn('id', $ids)
                            ->where('company_id', '=', $companyid['company_id'])
                            ->where('hub_id', '=', $hubid['hub_id'])
                            ->sum('atm_credit'), 2);
                        $banksDeposit->bank_name = $bankName;
                        $banksDeposit->city = $city;
                        $banksDeposit->company_id = $companyid['company_id'];
                        $banksDeposit->hub_id = $hubid['hub_id'];
                        $banksDeposit->save();
                        $flag = CaptainAccountHistory::whereIn('id', $ids)
                            ->where('company_id', '=', $companyid['company_id'])
                            ->where('hub_id', '=', $hubid['hub_id'])
                            ->update(array('banks_deposit_id' => $banksDeposit->id));
                    }
                }
                for($i = 0; $i < count($ids); ++$i) {
                    $captainid = CaptainAccountHistory::select('captain_id')->where('id', '=', $ids[$i])->first()->captain_id;
                    $frequency = DB::select("select count(*) 'frequency' from (select confirmed_walker,scheduled_shipment_date,count(*) from delivery_request where confirmed_walker = $captainid and scheduled_shipment_date is not null group by confirmed_walker,scheduled_shipment_date ) a");
                    $frequency = $frequency[0]->frequency;
                    Walker::where('id', '=', $captainid)->update(['frequency' => $frequency]);
                }

                $response_array = array(
                    'success' => true);
                $response = Response::json($response_array, 200);
                DB::commit();
                return $response;
            } else {
                $response_array = array(
                    'success' => false,
                    'error' => 'Amount deposited is not equal to total of records selected' . "\n" . '             ' . 'Please try again with ' . round($amount, 2));
                $response = Response::json($response_array, 200);
                return $response;
            }


        } catch (Exception $e) {
            Log::info($e->getMessage());
        }
        DB::rollback();
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated ');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function captaincodreport()
    {
        $admin_id = Session::get('admin_id');
        $items = array();
        try {
            if (Request::get('ids')) {
                $ids = explode(',', Request::get('ids'));;
                /*$captainhistories = CaptainAccountHistory::select('id')->whereIn('id', $ids)->get();
                $captainhistoryids = array_map(function ($object) {
                    return $object->id;
                }, json_decode($captainhistories));
                Log::info($captainhistoryids);
                $captainaccounts = CaptainAccount::select('waybill')->whereIn('captain_account_history_id', $captainhistoryids)->get();
                $waybills = array_map(function ($object) {
                    return $object->waybill;
                }, json_decode($captainaccounts));

                $waybills = implode('\',\'', $waybills);
                $waybills = '\'' . $waybills . '\'';
                $typequery = "select date_format (created_at,'%m/%d/%Y') as 'pick_up',order_number as 'order', (select company_name from companies where id = company_id) as 'company',
                                dropoff_timestamp as 'drop_off',jollychic as 'waybill', date_format (updated_at,'%m/%d/%Y') as 'updated_at' ,
                                 IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',cash_on_delivery as'cod', d_city as 'city',
                                 (select transfer_code from banks_deposit where paid_to_supplier=1 and id in (
                                  select banks_deposit_id from captain_account_history where id in (
                                 select captain_account_history_id from captain_account where waybill = delivery_request.jollychic))) as transfer_code,
                                 (select date_format(updated_at,'%m/%d/%Y') as 'Paid_to_Supplier_Date' from banks_deposit where paid_to_supplier=1 and id in (
                                  select banks_deposit_id from captain_account_history where id in (
                                 select captain_account_history_id from captain_account where waybill = delivery_request.jollychic))) as Paid_to_Supplier_Date
                                  from delivery_request
                                  where jollychic in ( " . $waybills . " ) order by updated_at asc ";*/
								  $captain_account_history_ids = '(';
                $flag = false;
                foreach ($ids as $id) {
                    if ($flag)
                        $captain_account_history_ids .= ',';
                    $flag = true;
                    $captain_account_history_ids .= "'$id'";
                }
                $captain_account_history_ids .= ')';

                $typequery = "select date_format (DR.created_at,'%m/%d/%Y') as pick_up,DR.order_number as 'order',company_name as company,
dropoff_timestamp as drop_off,jollychic as waybill,date_format (DR.updated_at,'%m/%d/%Y') as updated_at,IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',
cash_on_delivery as'cod', d_city as 'city',(select transfer_code from banks_deposit where paid_to_supplier=1 and banks_deposit.id= CAH.banks_deposit_id) as transfer_code,
(select date_format(updated_at,'%m/%d/%Y')  from banks_deposit where paid_to_supplier=1 and banks_deposit.id= CAH.banks_deposit_id) as Paid_to_Supplier_Date from captain_account_history as CAH
 INNER JOIN captain_account as CA ON CAH.id=CA.captain_account_history_id
INNER JOIN delivery_request as DR ON CA.waybill=DR.jollychic INNER JOIN companies ON companies.id = DR.company_id WHERE CAH.id in $captain_account_history_ids";
                $items = DB::select(DB::raw($typequery));
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

        $companies = Companies::all(array('id', 'company_name as name'));
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        return View::make('warehouse.codreport')
            ->with('adminCities', $adminCities)
            ->with('companies', $companies)
            ->with('items', $items);
    }

    public function captainPaymentReport()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        //  $paidtocaptain = Request::get('paidtocaptain');
        $captain_id = Request::get('captain_id');
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $admins = Admin::whereIn('role', [3,4,5,8])->orderBy('username', 'asc')->get();
        $admin_id = 'All';
        $city = 'Jeddah';
        $paidtocaptain = 0;
        $fromdate = '2018-01-01';
        $todate = '2018-01-01';

        /*   $city = Request::get('city');
           if (!isset($city)) {

               $city = 'Jeddah';
           }
           if (!isset($paidtocaptain)) {
               $paidtocaptain = 0;
           }
           $fromdate = Request::get('fromdate');
           if (!isset($fromdate)) {
               $fromdate = '2018-01-01';
           }
           $todate = Request::get('todate');
           if (!isset($todate)) {
               $todate = date('Y-m-d');
           } */
        $cap_id = null;
        $captainpaymenthistoryquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
        leftJoin('banks_deposit', 'banks_deposit.id', '=', 'captain_account_history.banks_deposit_id')->
        leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
        leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
        leftJoin('hub', 'hub.id', '=', 'captain_account_history.hub_id')->
        select
        ([
            'captain_account_history.*',
            'hub.name as hub_name',
            'walker.city as city',
            'walker.id as captain_id',
            'walker.first_name as first_name',
            'walker.last_name as last_name',
            'walker.phone as phone',
            'banks_deposit.deposit_date as deposit_date',
            'admin.username as adminuser',
            'paid_admin.username as paid_admin_name'])
            ->whereDate('banks_deposit.deposit_date', '>=', $fromdate)->whereDate('banks_deposit.deposit_date', '<=', $todate)
            ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)
            ->where('banks_deposit_id', '<>', 0);
        $captain_pickup_paymentquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
        leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
        leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
        select
        ([
            'captain_account_history.*',
            'walker.city as city',
            'walker.id as captain_id',
            'walker.first_name as first_name',
            'walker.last_name as last_name',
            'walker.phone as phone',
            'admin.username as adminuser',
            'paid_admin.username as paid_admin_name'])
            ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)
            ->where('banks_deposit_id', '=', -1);
        if (isset($captain_id) && !empty($captain_id)) {
            $cap_id = $captain_id;
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where('captain_account_history.captain_id', '=', $captain_id);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where('captain_account_history.captain_id', '=', $captain_id);
        }
        if ($city != 'All') {
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where('walker.city', '=', $city);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where('walker.city', '=', $city);
        }
        $captainpaymenthistory = $captainpaymenthistoryquery->get();
        $captain_pickup_paymentquery = $captain_pickup_paymentquery->get();
        return View::make('warehouse.captainPaymentReport')
            ->with('items', $captainpaymenthistory)
            ->with('captain_pickup_items', $captain_pickup_paymentquery)
            ->with('city', $city)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
            ->with('admins', $admins)
            ->with('admin_id', $admin_id)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('captain_id', $cap_id)
            ->with('paidtocaptain', $paidtocaptain);
    }

    public function captainPaymentReportpost()
    {
        $admin_id = Session::get('admin_id');
        $paidtocaptain = Request::get('paidtocaptain');
        $captain_id = Request::get('captain_id');
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->where('city', '<>', 'All')->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $admins = Admin::whereIn('role', [3,4,5,8])->orderBy('username', 'asc')->get();
        $admin_id = Request::get('admin');
        if (!isset($admin_id)) {
            $admin_id = 'All';
        }

        $city = Request::get('city');
        if (!isset($city)) {

            $city = 'Jeddah';
        }
        if (!isset($paidtocaptain)) {
            $paidtocaptain = 0;
        }
        $fromdate = Request::get('fromdate');
        if (!isset($fromdate)) {
            $fromdate = '2018-01-01';
        }
        $todate = Request::get('todate');
        if (!isset($todate)) {
            $todate = date('Y-m-d');
        }
        $hub = Request::get('hub');
        if(!isset($hub) || $hub == '' || $hub == '0') {
            $hub = 'All';
        }
        $cap_id = null;
        $captainpaymenthistoryquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
        leftJoin('banks_deposit', 'banks_deposit.id', '=', 'captain_account_history.banks_deposit_id')->
        leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
        leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
        leftJoin('hub', 'hub.id', '=', 'captain_account_history.hub_id')->
        select
        ([
            'captain_account_history.*',
            'hub.name as hub_name',
            'walker.city as city',
            'walker.id as captain_id',
            'walker.first_name as first_name',
            'walker.last_name as last_name',
            'walker.phone as phone',
            'banks_deposit.deposit_date as deposit_date',
            'admin.username as adminuser',
            'paid_admin.username as paid_admin_name'])
            ->whereDate('banks_deposit.deposit_date', '>=', $fromdate)->whereDate('banks_deposit.deposit_date', '<=', $todate)
            ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)
            ->where('banks_deposit_id', '<>', 0);
        $captain_pickup_paymentquery = CaptainAccountHistory::leftJoin('walker', 'walker.id', '=', 'captain_account_history.captain_id')->
        leftJoin('admin', 'captain_account_history.admin_id', '=', 'admin.id')->
        leftJoin('admin as paid_admin', 'captain_account_history.captain_admin_id', '=', 'paid_admin.id')->
        select
        ([
            'captain_account_history.*',
            'walker.city as city',
            'walker.id as captain_id',
            'walker.first_name as first_name',
            'walker.last_name as last_name',
            'walker.phone as phone',
            'admin.username as adminuser',
            'paid_admin.username as paid_admin_name'])
            ->where('captain_account_history.paid_to_captain', '=', $paidtocaptain)
            ->where('banks_deposit_id', '=', -1);
        if (isset($captain_id) && !empty($captain_id)) {
            $cap_id = $captain_id;
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where('captain_account_history.captain_id', '=', $captain_id);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where('captain_account_history.captain_id', '=', $captain_id);
        }
        if ($city != 'All') {
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where('walker.city', '=', $city);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where('walker.city', '=', $city);
        }
        if ($hub != 'All') {
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where('captain_account_history.hub_id', '=', $hub);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where('captain_account_history.hub_id', '=', $hub);
        }else {
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->whereNotIn('captain_account_history.hub_id',[6, 7]);
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->whereNotIn('captain_account_history.hub_id',[6, 7]);
        }
        if ($admin_id != 'All') {
            $captainpaymenthistoryquery = $captainpaymenthistoryquery->where(function ($q) use ($admin_id) {
                $q->where('captain_account_history.admin_id', 'like', '%' . $admin_id . '%')->orWhere('captain_account_history.captain_admin_id', 'like', '%' . $admin_id . '%');
            });
            $captain_pickup_paymentquery = $captain_pickup_paymentquery->where(function ($q) use ($admin_id) {
                $q->where('captain_account_history.admin_id', 'like', '%' . $admin_id . '%')->orWhere('captain_account_history.captain_admin_id', 'like', '%' . $admin_id . '%');
            });
        }
        $captainpaymenthistory = $captainpaymenthistoryquery->get();
        $captain_pickup_paymentquery = $captain_pickup_paymentquery->get();
        return View::make('warehouse.captainPaymentReport')
            ->with('items', $captainpaymenthistory)
            ->with('captain_pickup_items', $captain_pickup_paymentquery)
            ->with('city', $city)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
            ->with('hub', $hub)
            ->with('admins', $admins)
            ->with('admin_id', $admin_id)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('captain_id', $cap_id)
            ->with('paidtocaptain', $paidtocaptain);
    }

    public function updateCaptainPayment()
    {
        try {
            $admin_id = Auth::user()->id;
            $ids = Request::get('ids');
            $captain_id = Request::get('captain_id');
            $amountPayed = Request::get('amountPayed');
            $ids = explode(',', $ids);

            if (empty($captain_id)) {
                if (isset($ids[0])) {
                    $captain = CaptainAccountHistory::where('id', '=', $ids[0])->select(['captain_id'])->get();
                    $captain_id = $captain[0]->captain_id;
                }
            }
            $isMoneyCovered = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('is_amount_covered', '=', 1)->count();
            if ($isMoneyCovered > 0) {
                $response_array = array(
                    'success' => false,
                    'error' => 'Sorry  payment is not allowed due to pending covered amount');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $amount = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captain_id)->where('banks_deposit_id', '<>', 0)->sum('captain_amount');
            if (round($amount, 2) == $amountPayed) {
                $flag = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captain_id)->where('banks_deposit_id', '<>', 0)
                    ->where('paid_to_captain', '=', 0)->update(array('paid_to_captain' => 1, 'captain_admin_id' => $admin_id));
                $this->updatecaptainid($captain_id);
                if ($flag > 0) {
                    $response_array = array(
                        'success' => true);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            }
            $response_array = array(
                'success' => false,
                'error' => 'Sorry at least one shipment is not deposited into our bank account');
            $response = Response::json($response_array, 200);
            $this->updatecaptainid($captain_id);
            return $response;
        } catch (Exception $e) {
            $this->updatecaptainid($captain_id);
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated ');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatecaptain_pickup_payment()
    {
        try {
            Log::info('done this challenge 5');
            $admin_id = Auth::user()->id;
            $ids = Request::get('ids');
            $captain_id = Request::get('captain_id');
            $amountPayed = Request::get('amountPayed');
            $ids = explode(',', $ids);

            if (empty($captain_id)) {
                if (isset($ids[0])) {
                    $captain = CaptainAccountHistory::where('id', '=', $ids[0])->select(['captain_id'])->get();
                    $captain_id = $captain[0]->captain_id;
                }
            }
            $isMoneyCovered = DeliveryRequest::where('confirmed_walker', '=', $captain_id)->where('is_amount_covered', '=', 1)->count();
            if ($isMoneyCovered > 0) {
                $response_array = array(
                    'success' => false,
                    'error' => 'Sorry  payment is not allowed due to pending covered amount');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $amount = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captain_id)->where('banks_deposit_id', '=', -1)->sum('captain_amount');
            if (round($amount, 2) == $amountPayed) {
                $flag = CaptainAccountHistory::whereIn('id', $ids)->where('captain_id', '=', $captain_id)->where('banks_deposit_id', '=', -1)
                    ->where('paid_to_captain', '=', 0)->update(array('paid_to_captain' => 1, 'captain_admin_id' => $admin_id));
                $this->updatecaptainid($captain_id);
                if ($flag > 0) {
                    $response_array = array(
                        'success' => true);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            }
            $response_array = array(
                'success' => false,
                'error' => 'Sorry at least one shipment is not deposited into our bank account');
            $response = Response::json($response_array, 200);
            $this->updatecaptainid($captain_id);
            return $response;
        } catch (Exception $e) {
            $this->updatecaptainid($captain_id);
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function updatecaptainid($captainid) {
        if($captainid == 0 || $captainid == '')
            return false;
        $delivered = DB::select("select count(dr.id) 'delivered' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 5 and dr.is_money_received = 0")[0]->delivered;
        $with_captain = DB::select("select count(delivery_request.id) 'with_captain' from delivery_request where confirmed_walker = $captainid and status = 4")[0]->with_captain;
        $returned = DB::select("select count(dr.id) 'returned' from delivery_request dr where dr.confirmed_walker = $captainid and dr.status = 6")[0]->returned;
        $lastscheduledshipmentdate = DB::select("select ifnull(max(scheduled_shipment_date), '') 'lastscheduledshipmentdate' from delivery_request where confirmed_walker = $captainid and is_money_received = 0")[0]->lastscheduledshipmentdate;
        $last_payment_date = DB::select("SELECT max(date) 'last_payment_date' FROM captain_account_history as cahd WHERE cahd.captain_id = $captainid")[0]->last_payment_date;
        $balance = DB::select("select sum(cah.captain_amount) 'balance' from captain_account_history cah where cah.captain_id = $captainid and cah.paid_to_captain = 0")[0]->balance;
        $pending_since = "select min(PendingSince) 'pendingsince' from (
            select min(scheduled_shipment_date) 'PendingSince' from delivery_request where status in (4,6)  and confirmed_walker = $captainid
            union 
            select min(dropoff_timestamp) 'PendingSince' from delivery_request where status = 5 and is_money_received = 0  and confirmed_walker = $captainid
            ) A";
        $pending_since = DB::select($pending_since)[0]->pendingsince;
        if($pending_since == NULL)
            $pending_since = '0000-00-00 00:00:00';
        $delayedcountquery = "select waybill_number 'DelayedShipments' from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 4 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 4 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) union select waybill_number 'DelayedShipments' from orders_history where status = 5 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 5 and is_money_received = 0 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 union select waybill_number 'DelayedShipments' from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) > 3 and waybill_number not in ( select waybill_number  from orders_history where status = 6 and walker_id = $captainid and waybill_number in ( select jollychic from delivery_request where status = 6 and confirmed_walker = $captainid ) and DATEDIFF(now(), created_at) <= 3) ";
        $delayedcount = DB::select($delayedcountquery);
        $delayedcountarray = '(';
        for($i = 0; $i < count($delayedcount); ++$i)
            if($i == 0)
                $delayedcountarray .= "'".$delayedcount[$i]->DelayedShipments."'";
            else
                $delayedcountarray .= ','."'".$delayedcount[$i]->DelayedShipments."'";
        $delayedcountarray .= ')';
        if(count($delayedcount) > 0)
            $delayedamount = DB::select("select sum(cash_on_delivery) 'delayedamount' from delivery_request where jollychic in $delayedcountarray")[0]->delayedamount;
        else
            $delayedamount = 0;
        DB::update("update walker set delivered = ? , with_captain = ? , returned = ? , last_scheduled_shipment_date = ? , last_payment_date = ? , balance = ? , pending_since = ? , delayed_count = ? , delayed_amount = ? where id = ?", [$delivered, $with_captain, $returned, $lastscheduledshipmentdate, $last_payment_date, $balance, $pending_since, count($delayedcount), $delayedamount, $captainid]);
        return true;
    }

    public function bankDepositReport()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $city = 'All';
        $paidtosupplier = 0;
        $company_id = 'All';
        $fromdate = '2018-01-01';
        $todate = date('Y-m-d');
		$transfer_code ='All';
        $transferCodes = BanksDeposit::select('transfer_code')->where('transfer_code','<>','')->whereDate('deposit_date', '>=', $fromdate)
            ->whereDate('deposit_date', '<=', $todate)->distinct()->get();

        /*   $city = Request::get('city');
           if (!isset($city)) {
               if (isset($adminCities[0]))
                   $city = $adminCities[0]->city;
           }
          $paidtosupplier = Request::get('paidtosupplier');
           $paidtosupplier ='';
           if (!isset($paidtosupplier)) {
               $paidtosupplier = 0;
           }
           $company_id = Request::get('company');
           if (!isset($company_id)) {
               $company_id = 'All';

           }
           $fromdate = Request::get('fromdate');
           if (!isset($fromdate)) {
               $fromdate = '2018-01-01';
           }
           $todate = Request::get('todate');
           if (!isset($todate)) {
               $todate = date('Y-m-d');
           }*/
        $bankdepositsquery = BanksDeposit::select('banks_deposit.*', 'admin.username as adminuser','hub.name as hub_name',
            'paid_admin.username as paid_admin_name', 'companies.company_name as company_name')
            ->leftJoin('companies', 'companies.id', '=', 'banks_deposit.company_id')->leftJoin('admin', 'banks_deposit.admin_id', '=', 'admin.id')
            ->leftJoin('admin as paid_admin', 'banks_deposit.supplier_admin_id', '=', 'paid_admin.id')
            ->leftJoin('hub', 'hub.id', '=', 'banks_deposit.hub_id')
            ->whereDate('deposit_date', '>=', $fromdate)->whereDate('deposit_date', '<=', $todate)
            ->where('paid_to_supplier', '=', $paidtosupplier)->orderBy('deposit_date', 'desc');
        if ($city != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('banks_deposit.city', '=', $city);

        }
		if ($transfer_code != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('banks_deposit.transfer_code', '=', $transfer_code);

        }
        /*if ($company_id != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('company_id', '=', $company_id);

        }*/
        $bankdepositsquery = $bankdepositsquery->where('company_id', '<>', '952');
        $bankdeposits = $bankdepositsquery->get();

        foreach ($bankdeposits as $key => $bankdeposit) {
            $covered_money = DB::select("select sum(cash_on_delivery) as covered_money from delivery_request where jollychic in (select waybill from captain_account ca
join captain_account_history cah on ca.captain_account_history_id =cah.id where cah.banks_deposit_id = $bankdeposit->id) and is_amount_covered = 1");
            $covered_money = round(($covered_money[0]->covered_money), 2);
            $bankdeposits[$key]['covered_amount'] = $covered_money;
        }

        return View::make('warehouse.bankDepositReport')
            ->with('items', $bankdeposits)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('companies', $companies)
            ->with('paidtosupplier', $paidtosupplier)
            ->with('company_id', $company_id)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
			->with('transfer_code', $transfer_code)
            ->with('transferCodes', $transferCodes)
            ->with('city', $city);
    }

    public function bankDepositReportpost()
    {
        $admin_id = Session::get('admin_id');
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $city = Request::get('city');
        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }
        $paidtosupplier = Request::get('paidtosupplier');
        if (!isset($paidtosupplier)) {
            $paidtosupplier = 0;
        }
        $bankdepositreport = Request::get('bankdepositreport');
        if (!isset($bankdepositreport)) {
            $bankdepositreport = 'deposit_date';
        }
        $company_id = Request::get('company');
        if (!isset($company_id)) {
            $company_id = 'All';

        }
        $fromdate = Request::get('fromdate');
        if (!isset($fromdate)) {
            $fromdate = '2018-01-01';
        }
        $todate = Request::get('todate');
        if (!isset($todate)) {
            $todate = date('Y-m-d');
        }
        $transaction = Request::get('transaction');
        if (!isset($transaction)) {
            $transaction = 'All';
        }
        $hub = Request::get('hub');
        if(!isset($hub) || $hub == '' || $hub == '0') {
            $hub = 'All';
        }
		$transfer_code = Request::get('transferCode');
        if (!isset($transfer_code)) {
            $transfer_code = 'All';
        }
        $transferCodes = BanksDeposit::select('transfer_code')->where('transfer_code','<>','')->whereDate($bankdepositreport, '>=', $fromdate)
            ->whereDate($bankdepositreport, '<=', $todate)->distinct()->get();
        $bankdepositsquery = BanksDeposit::select('banks_deposit.*', 'admin.username as adminuser','hub.name as hub_name',
            'paid_admin.username as paid_admin_name', 'companies.company_name as company_name')
            ->leftJoin('companies', 'companies.id', '=', 'banks_deposit.company_id')->leftJoin('admin', 'banks_deposit.admin_id', '=', 'admin.id')
            ->leftJoin('admin as paid_admin', 'banks_deposit.supplier_admin_id', '=', 'paid_admin.id')
            ->leftJoin('hub', 'hub.id', '=', 'banks_deposit.hub_id')
            ->whereDate($bankdepositreport, '>=', $fromdate)->whereDate($bankdepositreport, '<=', $todate)
            ->where('paid_to_supplier', '=', $paidtosupplier)->orderBy('deposit_date', 'desc');
        if ($transaction != 'All') {
            if ($transaction == 1) {
                $bankdepositsquery = $bankdepositsquery->where('cash_transfer', '<>', 0);
            } else {
                $bankdepositsquery = $bankdepositsquery->where('atm_transfer', '<>', 0);
            }

        }
        if ($city != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('banks_deposit.city', '=', $city);

        }
        if ($hub != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('banks_deposit.hub_id', '=', $hub);

        }
		if ($transfer_code != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('banks_deposit.transfer_code', '=', $transfer_code);

        }
        if ($company_id != 'All') {
            $bankdepositsquery = $bankdepositsquery->where('company_id', '=', $company_id);

        }
        $bankdeposits = $bankdepositsquery->get();

        foreach ($bankdeposits as $key => $bankdeposit) {
            $covered_money = DB::select("select sum(cash_on_delivery) as covered_money from delivery_request where jollychic in (select waybill from captain_account ca
join captain_account_history cah on ca.captain_account_history_id =cah.id where cah.banks_deposit_id = $bankdeposit->id) and is_amount_covered = 1");
            $covered_money = round(($covered_money[0]->covered_money), 2);
            $bankdeposits[$key]['covered_amount'] = $covered_money;
        }

        return View::make('warehouse.bankDepositReport')
            ->with('items', $bankdeposits)
            ->with('fromdate', $fromdate)
            ->with('todate', $todate)
            ->with('companies', $companies)
            ->with('paidtosupplier', $paidtosupplier)
            ->with('bankdepositreport',$bankdepositreport)
            ->with('company_id', $company_id)
            ->with('adminCities', $adminCities)
            ->with('adminHubs', $adminHubs)
			->with('transfer_code', $transfer_code)
            ->with('transferCodes', $transferCodes)
            ->with('hub', $hub)
            ->with('city', $city)
            ->with('transaction',$transaction);
    }

    public function updatepaymentsupplier()
    {
        try {
            $admin_id = Auth::user()->id;
            $ids = Request::get('ids');
            $company_id = Request::get('company_id');
            $amountPayed = Request::get('amountPayed');
            $date = Request::get('date');
            $transfer_code = Request::get('transfer_code');
            if (empty($ids)) {
                Log::info('No Records Updated');
                $response_array = array(
                    'success' => false,
                    'error' => 'No Records Updated');
                $response = Response::json($response_array, 200);
                return $response;
            }
            $bankdepositquery = 'SELECT sum(cast(amount as decimal(12,2)) ) as amountsumed FROM `banks_deposit` where id in (' . $ids . ') and paid_to_supplier = 0';
            $amountsumed = DB::select(DB::raw($bankdepositquery));
            $ids = explode(',', $ids);
            //$amount = BanksDeposit::whereIn('id', $ids)->where('paid_to_supplier', '=', 0)->sum('amount');
            $amount = strval($amountsumed[0]->amountsumed);
            if ($amountPayed == $amount) {
                $flag = BanksDeposit::whereIn('id', $ids)->where('paid_to_supplier', '=', 0)
                    ->update(array('paid_to_supplier' => 1, 'supplier_admin_id' => $admin_id, 'updated_at' => $date, 'transfer_code' => $transfer_code));
                if ($flag > 0) {
                    $response_array = array(
                        'success' => true);
                    $response = Response::json($response_array, 200);
                    return $response;
                }
            }
            $response_array = array(
                'success' => false,
                'error' => 'Payment to supplier update failed due to mismatch of total amount,' . "\n" . '             ' . 'Please try again with ' . $amount);
            $response = Response::json($response_array, 200);
            return $response;

        } catch (Exception $e) {
            Log::info($e->getMessage());
        }
        $response_array = array(
            'success' => false,
            'error' => 'No Records Updated');
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function codReportRedirect()
    {
        $admin_id = Session::get('admin_id');
        $count = 0;

        $searchcolumn = 'id';

        $items = array();
        try {
            if (Request::get('ids')) {
                $ids = explode(',', Request::get('ids'));
                //BanksDeposit::whereIn('id', $ids)->update(['is_printed' => 1]);
                DB::table('banks_deposit')->whereIn('id', $ids)->update(['is_printed' => 1]);
                /*$captainhistories = CaptainAccountHistory::select('id')->whereIn('banks_deposit_id', $ids)->get();
                Log::info($captainhistories);
                $captainhistoryids = array_map(function ($object) {
                    return $object->id;
                }, json_decode($captainhistories));
                Log::info($captainhistoryids);
                $captainaccounts = CaptainAccount::select('waybill')->whereIn('captain_account_history_id', $captainhistoryids)->get();
                $waybills = array_map(function ($object) {
                    return $object->waybill;
                }, json_decode($captainaccounts));

                $waybills = implode('\',\'', $waybills);
                $waybills = '\'' . $waybills . '\'';
                $typequery = "select date_format (delivery_request.created_at,'%m/%d/%Y') as 'pick_up',order_number as 'order',company_name as 'company',
                              dropoff_timestamp as 'drop_off',jollychic as 'waybill', date_format (delivery_request.updated_at,'%m/%d/%Y') as 'updated_at' ,
                              IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',cash_on_delivery as'cod', d_city as 'city' ,
                              (select transfer_code from banks_deposit where paid_to_supplier=1 and id in (
                                  select banks_deposit_id from captain_account_history where id in (
                                 select captain_account_history_id from captain_account where waybill = delivery_request.jollychic))) as transfer_code,
                              (select date_format(updated_at,'%m/%d/%Y') as 'Paid_to_Supplier_Date' from banks_deposit where paid_to_supplier=1 and id in (
                              select banks_deposit_id from captain_account_history where id in (
                              select captain_account_history_id from captain_account where waybill = delivery_request.jollychic))) as Paid_to_Supplier_Date
                              from delivery_request LEFT JOIN companies ON companies.id = delivery_request.company_id where
                              jollychic in ( " . $waybills . " ) order by updated_at asc ";*/
							  $bank_deposit_ids = '(';
                $flag = false;
                foreach ($ids as $id) {
                    if ($flag)
                        $bank_deposit_ids .= ',';
                    $flag = true;
                    $bank_deposit_ids .= "'$id'";
                }
                $bank_deposit_ids .= ')';

                $typequery = "select date_format (DR.created_at,'%m/%d/%Y') as pick_up,DR.order_number as 'order',company_name as company,
dropoff_timestamp as drop_off,jollychic as waybill,date_format (DR.updated_at,'%m/%d/%Y') as updated_at,IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',
cash_on_delivery as'cod', d_city as 'city',IF (paid_to_supplier = 1,transfer_code,'') As 'transfer_code',
IF (paid_to_supplier = 1,date_format(banks_deposit.updated_at,'%m/%d/%Y'),'') As 'Paid_to_Supplier_Date' from captain_account_history as CAH
INNER JOIN banks_deposit ON banks_deposit.id = CAH.banks_deposit_id INNER JOIN captain_account as CA ON CAH.id=CA.captain_account_history_id
INNER JOIN delivery_request as DR ON CA.waybill=DR.jollychic INNER JOIN companies ON companies.id = DR.company_id WHERE banks_deposit.id in $bank_deposit_ids";
                //$ids = explode(',', Request::get('ids'));

                // var_dump($ids);
                //die();

                if (preg_match('/^JC[0-9]{8}KS/', $ids[0])) {
                    $searchcolumn = 'jollychic';
                }

                if (preg_match('/^OS[0-9]{8}KS/', $ids[0])) {
                    $searchcolumn = 'waybill';
                }

                $items = DB::select(DB::raw($typequery));
                /*foreach ($ids as $id) {
                    $anitem = DeliveryRequest::where($searchcolumn, '=', $id)->get()->first();


                    if ($anitem->cash_on_delivery) {
                        $status = 'COD';
                    } else {
                        $status = 'Prepaid';
                    }

                    //$anitem->dropoff_timestamp = $anitem->dropoff_timestamp->format('m/d/Y');
                    array_push($items, array(
                        'pick_up' => $anitem->created_at->format('m/d/Y'),
                        'drop_off' => $anitem->dropoff_timestamp,
                        'waybill' => $anitem->jollychic,
                        'status' => $status,
                        'cod' => $anitem->cash_on_delivery
                    ));
                }*/

            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

        $companies = Companies::all(array('id', 'company_name as name'));
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        return View::make('warehouse.codreport')
            ->with('adminCities', $adminCities)
            ->with('companies', $companies)
            ->with('items', $items);
    }

    function generate_bank_report()
    {
        $ids = Request::get('ids');

        $ids = explode(',', $ids);
        $bankdepositsquery = BanksDeposit::select(DB::raw('SUM(cast(banks_deposit.amount as decimal(12,2)) ) as amount'), 'companies.name_ar as company_name')
            ->leftJoin('companies', 'companies.id', '=', 'banks_deposit.company_id')->whereIn('banks_deposit.id', $ids)->groupBy('company_id')->get();
        foreach ($bankdepositsquery as $company) {
            $company_data[] = array(
                'amount' => $company->amount,
                'company_name' => $company->company_name,
            );
        }

        $cities = BanksDeposit::whereIn('banks_deposit.id', $ids)->distinct()->get(['city']);
        foreach ($cities as $city) {
            $cities_data[] = array(
                'city' => $city->city,
            );
        }
        $deposit_dates = BanksDeposit::whereIn('banks_deposit.id', $ids)->distinct()->groupBy(DB::raw('DATE(deposit_date)'))->get(['deposit_date']);
        foreach ($deposit_dates as $deposit_date) {
            $depositDates_data[] = array(
                'deposit_date' => date('d-m-Y', strtotime($deposit_date->deposit_date)),
            );
        }
        $hub_names = BanksDeposit::leftJoin('hub', 'hub.id', '=', 'banks_deposit.hub_id')->whereIn('banks_deposit.id', $ids)->distinct()->get(['name']);
        foreach ($hub_names as $hub) {
            $hubNames_data[] = array(
                'hub_name' => $hub->name,
            );
        }

        $response_array = array(
            'success' => true,
            'cities' => $cities_data,
            'deposit_dates' => $depositDates_data,
            'hub_names' => $hubNames_data,
            'company_data' => $company_data
        );
        $response = Response::json($response_array, 200);
        return $response;
    }
    public function shipmentscovered()
    {
        $admin_id = Session::get('admin_id');
        if (!isset($admin_id)) {
            return Redirect::to('/warehouse/login');
        }
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $fromdate = '2018-01-01';
        $todate = date('Y-m-d');

        $received_covered_orders = CaptainAccount::leftJoin('walker', 'captain_account.captain_id', '=', 'walker.id')
            ->leftJoin('delivery_request', 'delivery_request.jollychic', '=', 'captain_account.waybill')
            ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
            ->leftJoin('hub', 'hub.id', '=', 'captain_account.hub_id')
            ->select(
                [
                    'captain_account.updated_at as date',
                    'hub.name as hub_name',
                    'jollychic', 'cash_on_delivery','d_city',
                    'walker.id as captain_id','walker.id as captain_id','walker.first_name as first_name', 'walker.last_name as last_name', 'walker.phone as phone',
                    'companies.company_name'
                ])
            ->where('captain_covered_amount_history_id', '<>', 0)->get();

        $unreceived_covered_orders = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')
            ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
            ->leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')
            ->select(
                [
                    'jollychic', 'cash_on_delivery','d_city','hub.name as hub_name',
                    'walker.id as captain_id','walker.first_name as first_name', 'walker.last_name as last_name', 'walker.phone as phone',
                    'companies.company_name'
                ])
            ->where('is_amount_covered', '=', 1)->where('status', '=', 5)->get();
        return View::make('warehouse.coveredShipments')
            ->with('received_covered_orders', $received_covered_orders)->with('notreceived_covered_orders', $unreceived_covered_orders)
            ->with('companies', $companies)->with('adminCities', $adminCities)->with('fromdate', $fromdate)
            ->with('todate', $todate)->with('adminHubs',$adminHubs);
    }
    public function shipmentscoveredpost()
    {
        $admin_id = Session::get('admin_id');
        $companies = Companies::orderBy('company_name', 'asc')->get();
        $adminCities = CityPrivilege::where('admin_id', '=', $admin_id)->orderBy('city', 'asc')->get();
        $adminHubs = HubPrivilege::leftJoin('hub', 'hub.id', '=', 'hub_privilege.hub_id')->where('admin_id', '=', $admin_id)->get();
        $city = Request::get('city');
        if (!isset($city)) {
            if (isset($adminCities[0]))
                $city = $adminCities[0]->city;
        }

        $company_id = Request::get('company');
        if (!isset($company_id)) {
            $company_id = 'All';

        }
        $fromdate = Request::get('fromdate');
        if (!isset($fromdate)) {
            $fromdate = '2018-01-01';
        }
        $todate = Request::get('todate');
        if (!isset($todate)) {
            $todate = date('Y-m-d');
        }
        $hub = Request::get('hub');
        if(!isset($hub) || $hub == '' || $hub == '0') {
            $hub = 'All';
        }

        $received_covered_orders_query = CaptainAccount::leftJoin('walker', 'captain_account.captain_id', '=', 'walker.id')
            ->leftJoin('delivery_request', 'delivery_request.jollychic', '=', 'captain_account.waybill')
            ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
            ->leftJoin('hub', 'hub.id', '=', 'captain_account.hub_id')
            ->select(
                [
                    'captain_account.updated_at as date',
                    'hub.name as hub_name',
                    'jollychic', 'cash_on_delivery','d_city',
                    'walker.id as captain_id','walker.id as captain_id','walker.first_name as first_name', 'walker.last_name as last_name', 'walker.phone as phone',
                    'companies.company_name'
                ])
            ->where('captain_covered_amount_history_id', '<>', 0)
            ->whereDate('captain_account.updated_at', '>=', $fromdate)->whereDate('captain_account.updated_at', '<=', $todate);

        $unreceived_covered_orders_query = DeliveryRequest::leftJoin('walker', 'delivery_request.confirmed_walker', '=', 'walker.id')
            ->leftJoin('companies', 'companies.id', '=', 'delivery_request.company_id')
            ->leftJoin('hub', 'hub.id', '=', 'delivery_request.hub_id')
            ->select(
                [
                    'jollychic', 'cash_on_delivery','d_city','hub.name as hub_name',
                    'walker.id as captain_id','walker.first_name as first_name', 'walker.last_name as last_name', 'walker.phone as phone',
                    'companies.company_name'
                ])
            ->where('is_amount_covered', '=', 1)->where('status', '=', 5);

        if ($city != 'All') {
            $received_covered_orders_query = $received_covered_orders_query->where('delivery_request.d_city', '=', $city);
            $unreceived_covered_orders_query = $unreceived_covered_orders_query->where('d_city', '=', $city);

        }
        if ($hub != 'All') {
            $received_covered_orders_query = $received_covered_orders_query->where('captain_account.hub_id', '=', $hub);
            $unreceived_covered_orders_query = $unreceived_covered_orders_query->where('delivery_request.hub_id', '=', $hub);

        }
        if ($company_id != 'All') {
            $received_covered_orders_query = $received_covered_orders_query->where('delivery_request.company_id', '=', $company_id);
            $unreceived_covered_orders_query = $unreceived_covered_orders_query->where('delivery_request.company_id', '=', $company_id);

        }
        $received_covered_orders = $received_covered_orders_query->get();
        $unreceived_covered_orders = $unreceived_covered_orders_query->get();


        return View::make('warehouse.coveredShipments')
            ->with('received_covered_orders', $received_covered_orders)->with('notreceived_covered_orders', $unreceived_covered_orders)
            ->with('companies', $companies)->with('adminCities', $adminCities)->with('fromdate', $fromdate)
            ->with('adminHubs',$adminHubs)->with('hub',$hub)
            ->with('todate', $todate)->with('company_id', $company_id)->with('city', $city);
    }
	
	public function checkcodviewstatus()
    {
        $items = array();
        try {
            if (Request::get('ids')) {
                $ids = explode(',', Request::get('ids'));

                $bank_deposit_ids = '(';
                $flag = false;
                foreach ($ids as $id) {
                    if ($flag)
                        $bank_deposit_ids .= ',';
                    $flag = true;
                    $bank_deposit_ids .= "'$id'";
                }
                $bank_deposit_ids .= ')';

                $typequery = "select CA.waybill as 'jollychic' from captain_account CA
INNER JOIN captain_account_history as CAH ON CAH.id = CA.captain_account_history_id
 INNER JOIN banks_deposit ON banks_deposit.id = CAH.banks_deposit_id
  WHERE banks_deposit.id in  $bank_deposit_ids";
                $items = DB::select(DB::raw($typequery));
            }

        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }

        $response_array = array('success' => true, 'count' => count($items));
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function downloadcodReportDirect()
    {
        if (Request::get('ids')) {
            $ids = explode(',', Request::get('ids'));
            $bank_deposit_ids = '(';
            $flag = false;
            foreach ($ids as $id) {
                if ($flag)
                    $bank_deposit_ids .= ',';
                $flag = true;
                $bank_deposit_ids .= "'$id'";
            }
            $bank_deposit_ids .= ')';

            $typequery = "select date_format (DR.created_at,'%m/%d/%Y') as pick_up,DR.order_number as 'order',company_name as company,
dropoff_timestamp as drop_off,jollychic as waybill,date_format (DR.updated_at,'%m/%d/%Y') as updated_at,IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',
cash_on_delivery as'cod', d_city as 'city',IF (paid_to_supplier = 1,transfer_code,'') As 'transfer_code',
IF (paid_to_supplier = 1,date_format(banks_deposit.updated_at,'%m/%d/%Y'),'') As 'Paid_to_Supplier_Date' from captain_account_history as CAH
INNER JOIN banks_deposit ON banks_deposit.id = CAH.banks_deposit_id INNER JOIN captain_account as CA ON CAH.id=CA.captain_account_history_id
INNER JOIN delivery_request as DR ON CA.waybill=DR.jollychic INNER JOIN companies ON companies.id = DR.company_id WHERE banks_deposit.id in $bank_deposit_ids";
            $items = DB::select(DB::raw($typequery));

            $todate = date('Y-m-d');
            $filename = "COD-" . $todate . '_' . rand(10, 300) . ".csv";
            $handle = fopen(public_path() . '/import/' . $filename, 'w+');
            fputcsv($handle, array('Nu.', 'Pick Up date', 'Order No.', 'Company', 'Delivery Date', 'AWB', 'Status', 'COD Amount', 'City', 'Payment Date', 'Paid to Supplier Date', 'Transfer Code'));
            $nu = 0;
            foreach ($items as $row) {
                $nu++;
                $pickup = $row->pick_up;
                $order = addslashes($row->order);
                $company = $row->company;
                $dropoff = $row->drop_off;
                $waybill = $row->waybill;
                $status = $row->status;
                $cod = $row->cod;
                $updated_at = $row->updated_at;
                $city = $row->city;
                if ($row->Paid_to_Supplier_Date == "") {
                    $Paid_to_Supplier_Date = 'Not Paid to Supplier';
                } else {
                    $Paid_to_Supplier_Date = $row->Paid_to_Supplier_Date;
                }
                if ($row->transfer_code == "") {
                    $transfer_code = '';
                } else {
                    $transfer_code = $row->transfer_code;
                }

                fputcsv($handle, array($nu, $pickup, $order, $company, $dropoff, $waybill, $status, $cod, $city, $updated_at, $Paid_to_Supplier_Date, $transfer_code));
            }
            fclose($handle);
            $headers = array(
                'Content-Type' => 'text/csv',
            );
            if (count($items) > 0) {
                return Response::download(public_path() . '/import/' . $filename, $filename, $headers);
            }
        }
        return 'No data find';

    }
	
	public function checkcaptaincodviewstatus()
    {
        $items = array();
        try {
            if (Request::get('ids')) {
                $ids = explode(',', Request::get('ids'));
                $items = CaptainAccount::select('waybill')->whereIn('captain_account_history_id', $ids)->count();
            }
        } catch (Exception $e) {
            $response_array = array('success' => false, 'error' => $e->getMessage(), 'error_code' => 410);
            $response = Response::json($response_array, 200);
            return $response;
        }
        $response_array = array('success' => true, 'count' => $items);
        $response = Response::json($response_array, 200);
        return $response;
    }

    public function downloadcaptaincodReportDirect()
    {
        if (Request::get('ids')) {
            $ids = explode(',', Request::get('ids'));

            $captain_account_history_ids = '(';
            $flag = false;
            foreach ($ids as $id) {
                if ($flag)
                    $captain_account_history_ids .= ',';
                $flag = true;
                $captain_account_history_ids .= "'$id'";
            }
            $captain_account_history_ids .= ')';

            $typequery = "select date_format (DR.created_at,'%m/%d/%Y') as pick_up,DR.order_number as 'order',company_name as company,
dropoff_timestamp as drop_off,jollychic as waybill,date_format (DR.updated_at,'%m/%d/%Y') as updated_at,IF (cash_on_delivery > 0,'COD','Prepaid') As 'status',
cash_on_delivery as'cod', d_city as 'city',(select transfer_code from banks_deposit where paid_to_supplier=1 and banks_deposit.id= CAH.banks_deposit_id) as transfer_code,
(select date_format(updated_at,'%m/%d/%Y')  from banks_deposit where paid_to_supplier=1 and banks_deposit.id= CAH.banks_deposit_id) as Paid_to_Supplier_Date from captain_account_history as CAH
 INNER JOIN captain_account as CA ON CAH.id=CA.captain_account_history_id
INNER JOIN delivery_request as DR ON CA.waybill=DR.jollychic INNER JOIN companies ON companies.id = DR.company_id WHERE CAH.id in $captain_account_history_ids";
            $items = DB::select(DB::raw($typequery));

            $todate = date('Y-m-d');
            $filename = "COD-" . $todate . '_' . rand(10, 300) . ".csv";
            $handle = fopen(public_path() . '/import/' . $filename, 'w+');
            fputcsv($handle, array('Nu.', 'Pick Up date', 'Order No.', 'Company', 'Delivery Date', 'AWB', 'Status', 'COD Amount', 'City', 'Payment Date', 'Paid to Supplier Date', 'Transfer Code'));
            $nu = 0;
            foreach ($items as $row) {
                $nu++;
                $pickup = $row->pick_up;
                $order = addslashes($row->order);
                $company = $row->company;
                $dropoff = $row->drop_off;
                $waybill = $row->waybill;
                $status = $row->status;
                $cod = $row->cod;
                $updated_at = $row->updated_at;
                $city = $row->city;
                if ($row->Paid_to_Supplier_Date == "") {
                    $Paid_to_Supplier_Date = 'Not Paid to Supplier';
                } else {
                    $Paid_to_Supplier_Date = $row->Paid_to_Supplier_Date;
                }
                if ($row->transfer_code == "") {
                    $transfer_code = '';
                } else {
                    $transfer_code = $row->transfer_code;
                }
                fputcsv($handle, array($nu, $pickup, $order, $company, $dropoff, $waybill, $status, $cod, $city, $updated_at, $Paid_to_Supplier_Date, $transfer_code));
            }
            fclose($handle);
            $headers = array(
                'Content-Type' => 'text/csv',
            );
            if (count($items) > 0) {
                return Response::download(public_path() . '/import/' . $filename, $filename, $headers);
            }
        }
        return 'No data find';

    }
}